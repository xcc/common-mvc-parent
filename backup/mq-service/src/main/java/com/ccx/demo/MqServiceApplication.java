package com.ccx.demo;

import org.springframework.boot.SpringApplication;

/**
 * <pre>
 * 参考配置：
 *   https://docs.spring.io/spring-boot/docs/2.1.5.RELEASE/reference/htmlsingle/
 *
 * @author 谢长春 2019/1/21
 */
public class MqServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }
}
