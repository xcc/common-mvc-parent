package com.test;

import com.google.common.collect.HashBasedTable;
import com.common.util.JSON;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by 谢长春 on 2017/10/14.
 */
@Slf4j
public class Test {
    public static void main(String[] args) {

        Stream.of(
                User.builder().id(1).phone("18700000001").name("JX").value(System.currentTimeMillis()).build(),
                User.builder().id(2).phone("18700000002").name("Jack").value(System.currentTimeMillis()).build(),
                User.builder().id(3).phone("18700000003").name("Jhon").value(System.currentTimeMillis()).build(),
                User.builder().id(4).phone("18700000004").name("Jessie").value(System.currentTimeMillis()).build(),
                User.builder().id(5).phone("18700000005").name("Joe").value(System.currentTimeMillis()).build()
        ).collect(
                HashBasedTable::create,
                (t, u) -> {
                    System.out.println(t.getClass());
                    System.out.println(u.getClass());
                    System.out.println(JSON.toJsonString(Arrays.asList(t, u)));
                },
                (t, u) -> {
                    System.out.println(JSON.toJsonString(Arrays.asList(t, u)));
                }
        );

    }

    @Getter
    @Setter
    @ToString
    @Builder
    static class User {
        private int id;
        private String phone;
        private String name;
        private long value;

    }

}
