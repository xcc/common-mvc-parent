package com.ccx.demo.business.code.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCodeConvert is a Querydsl query type for TabCodeConvert
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCodeConvert extends EntityPathBase<TabCodeConvert> {

    private static final long serialVersionUID = 1513924929L;

    public static final QTabCodeConvert tabCodeConvert = new QTabCodeConvert("tabCodeConvert");

    public final ArrayPath<com.ccx.demo.enums.AppCode[], com.ccx.demo.enums.AppCode> codes = createArray("codes", com.ccx.demo.enums.AppCode[].class);

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Long> createUserId = createNumber("createUserId", Long.class);

    public final BooleanPath deleted = createBoolean("deleted");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ArrayPath<Long[], Long> ids = createArray("ids", Long[].class);

    public final ArrayPath<String[], String> images = createArray("images", String[].class);

    public final SimplePath<com.common.entity.Item> item = createSimple("item", com.common.entity.Item.class);

    public final ArrayPath<com.common.entity.Item[], com.common.entity.Item> items = createArray("items", com.common.entity.Item[].class);

    public final StringPath textBase64 = createString("textBase64");

    public final StringPath textOriginal = createString("textOriginal");

    public final StringPath updateTime = createString("updateTime");

    public final NumberPath<Long> updateUserId = createNumber("updateUserId", Long.class);

    public QTabCodeConvert(String variable) {
        super(TabCodeConvert.class, forVariable(variable));
    }

    public QTabCodeConvert(Path<? extends TabCodeConvert> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCodeConvert(PathMetadata metadata) {
        super(TabCodeConvert.class, metadata);
    }

}

