package com.ccx.demo.business.code.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCodeExample is a Querydsl query type for TabCodeExample
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCodeExample extends EntityPathBase<TabCodeExample> {

    private static final long serialVersionUID = -760636168L;

    public static final QTabCodeExample tabCodeExample = new QTabCodeExample("tabCodeExample");

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final StringPath content = createString("content");

    public final StringPath contentEncode = createString("contentEncode");

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Long> createUserId = createNumber("createUserId", Long.class);

    public final BooleanPath deleted = createBoolean("deleted");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final EnumPath<com.ccx.demo.business.code.enums.DemoStatus> status = createEnum("status", com.ccx.demo.business.code.enums.DemoStatus.class);

    public final StringPath updateTime = createString("updateTime");

    public final NumberPath<Long> updateUserId = createNumber("updateUserId", Long.class);

    public QTabCodeExample(String variable) {
        super(TabCodeExample.class, forVariable(variable));
    }

    public QTabCodeExample(Path<? extends TabCodeExample> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCodeExample(PathMetadata metadata) {
        super(TabCodeExample.class, metadata);
    }

}

