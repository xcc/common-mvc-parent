package com.ccx.demo.business.code.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCodeAuthId is a Querydsl query type for TabCodeAuthId
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCodeAuthId extends EntityPathBase<TabCodeAuthId> {

    private static final long serialVersionUID = -1388189803L;

    public static final QTabCodeAuthId tabCodeAuthId = new QTabCodeAuthId("tabCodeAuthId");

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Long> createUserId = createNumber("createUserId", Long.class);

    public final BooleanPath deleted = createBoolean("deleted");

    public final StringPath id = createString("id");

    public final StringPath name = createString("name");

    public final StringPath updateTime = createString("updateTime");

    public final NumberPath<Long> updateUserId = createNumber("updateUserId", Long.class);

    public QTabCodeAuthId(String variable) {
        super(TabCodeAuthId.class, forVariable(variable));
    }

    public QTabCodeAuthId(Path<? extends TabCodeAuthId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCodeAuthId(PathMetadata metadata) {
        super(TabCodeAuthId.class, metadata);
    }

}

