package com.ccx.demo.business.code.service;

import com.ccx.demo.business.code.dao.jpa.CodeConvertRepository;
import com.ccx.demo.business.code.dto.TabCodeConvertInsertDTO;
import com.ccx.demo.business.code.dto.TabCodeConvertUpdateDTO;
import com.ccx.demo.business.code.entity.TabCodeConvert;
import com.common.db.entity.Page;
import com.common.exception.DeleteRowsException;
import com.common.exception.UpdateRowsException;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 服务接口实现类：测试自定义 Convert 表
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class CodeConvertService {
    private final CodeConvertRepository repository;


    /**
     * 新增 测试自定义 Convert 表
     *
     * @param dto    {@link TabCodeConvertInsertDTO} 实体对象
     * @param userId {@link Long} 操作用户ID
     * @return {@link TabCodeConvert} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") TabCodeConvert insert(
            @Valid @NotNull(message = "【dto】不能为null") final TabCodeConvertInsertDTO dto
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        final TabCodeConvert obj = new TabCodeConvert();
        BeanUtils.copyProperties(dto, obj);
        return repository.insert(userId, obj);
    }

    /**
     * 批量新增 测试自定义 Convert 表
     *
     * @param list   {@link List<TabCodeConvertInsertDTO>}  实体对象集合
     * @param userId {@link Long} 操作用户ID
     * @return List<TabCodeConvert> 实体对象集合
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") List<TabCodeConvert> insert(
            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull TabCodeConvertInsertDTO> list
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        return repository.insert(userId, list.stream()
                .map(dto -> {
                    final TabCodeConvert obj = new TabCodeConvert();
                    BeanUtils.copyProperties(dto, obj);
                    return obj;
                })
                .collect(Collectors.toList())
        );
    }

    /**
     * 更新 测试自定义 Convert 表 ；
     *
     * @param id     {@link Long} 数据ID
     * @param userId {@link Long} 操作用户ID
     * @param dto    {@link TabCodeConvertUpdateDTO} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
            , @NotNull(message = "【userId】不能为null") final Long userId
            , @Valid @NotNull(message = "【dto】不能为null") final TabCodeConvertUpdateDTO dto) {
        final TabCodeConvert obj = new TabCodeConvert();
        BeanUtils.copyProperties(dto, obj);
        UpdateRowsException.asserts(repository.update(id, userId, obj));
    }

//    /**
//     * 测试自定义 Convert 表 按ID删除，物理删除
//     *
//     * @param id     {@link Long} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteById(id, userId));
//    }
//
//    /**
//     * 测试自定义 Convert 表 按 id+updateTime 删除，物理删除
//     *
//     * @param id         {@link Long} 数据ID
//     * @param updateTime {@link String} 最后一次更新时间
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
//            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
//            ,@NotNull(message = "【userId】不能为null") final Long userId
//    ) {
//        DeleteRowsException.asserts(repository.deleteById(id, updateTime, userId));
//    }
//
//    /**
//     * 测试自定义 Convert 表 按ID删除，物理删除
//     *
//     * @param ids     {@link Set<Long>} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids, userId));

//    }
//
//    /**
//     * 测试自定义 Convert 表 按 id+updateTime 删除，物理删除
//     *
//     * @param ids     {@link Set<Long>} 数据ID
//     * @param updateTimes     Set<String> 最后一次更新时间
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
//            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
//            ,@NotNull(message = "【userId】不能为null") final Long userId
//    ) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids, updateTimes, userId));

//    }
//

    /**
     * 测试自定义 Convert 表 按ID删除，逻辑删除
     *
     * @param id     {@link Long} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        DeleteRowsException.asserts(repository.markDeleteById(id, userId));
    }

    /**
     * 测试自定义 Convert 表 按 id+updateTime 删除，逻辑删除
     *
     * @param id         {@link Long} 数据ID
     * @param updateTime {@link String} 最后一次更新时间
     * @param userId     {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
            , @NotNull(message = "【userId】不能为null") final Long userId
    ) {
        DeleteRowsException.asserts(repository.markDeleteById(id, updateTime, userId));
    }

    /**
     * 测试自定义 Convert 表 按ID删除，逻辑删除
     *
     * @param ids    {@link Set<Long>} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, userId), ids.size());

    }

    /**
     * 测试自定义 Convert 表 按 id+updateTime 删除，逻辑删除
     *
     * @param ids         {@link Set<Long>} 数据ID
     * @param updateTimes Set<String> 最后一次更新时间
     * @param userId      {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
            , @NotNull(message = "【userId】不能为null") final Long userId
    ) {
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, updateTimes, userId), ids.size());

    }

    /**
     * 测试自定义 Convert 表 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link Long} 数据ID
     * @return {@link Optional<TabCodeConvert>} 实体对象
     */

    public Optional<TabCodeConvert> findById(final Long id) {
        if (Objects.isNull(id) || id < 1) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabCodeConvert::cloneObject); // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作

    }

    /**
     * 测试自定义 Convert 表 按条件分页查询列表
     *
     * @param condition {@link TabCodeConvert} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return {@link QueryResults<TabCodeConvert>} 分页对象
     */

    public @NotNull(message = "返回值不能为null") QueryResults<TabCodeConvert> page(
            @NotNull(message = "【condition】不能为null") final TabCodeConvert condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        return repository.page(condition, page);
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试自定义 Convert 表 按条件查询列表
//     *
//     * @param condition {@link TabCodeConvert} 查询条件
//     * @return {@link List<TabCodeConvert>} 结果集合
//     */
//
//    public @NotNull(message = "返回值不能为null") List<TabCodeConvert> list(
//            @NotNull(message = "【condition】不能为null") final TabCodeConvert condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 测试自定义 Convert 表 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link Long}  数据 id 集合
     * @return {@link List<TabCodeConvert>} 结果集合
     */

    public @NotNull(message = "返回值不能为null") List<TabCodeConvert> listByIds(final Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }

    /**
     * 测试自定义 Convert 表 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link Long} 数据 id 集合
     * @return {@link List<TabCodeConvert>} 结果集合
     */

    public @NotNull(message = "返回值不能为null") Map<Long, TabCodeConvert> mapByIds(final Set<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }

}
