package com.ccx.demo.business.code.dto;

import com.ccx.demo.business.code.entity.TabCodeConvert;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO：测试自定义 Convert 表新增
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义测试自定义 Convert 表新增时的扩展属性
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "测试自定义 Convert 表")
public class TabCodeConvertInsertDTO extends TabCodeConvert {
    private static final long serialVersionUID = 1L;


}
