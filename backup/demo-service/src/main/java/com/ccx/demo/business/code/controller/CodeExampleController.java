package com.ccx.demo.business.code.controller;

import com.ccx.demo.business.code.dto.TabCodeExampleInsertDTO;
import com.ccx.demo.business.code.dto.TabCodeExampleUpdateDTO;
import com.ccx.demo.business.code.entity.TabCodeExample;
import com.ccx.demo.business.code.service.CodeExampleService;
import com.ccx.demo.business.code.vo.TabCodeExampleVO;
import com.ccx.demo.config.DOEncryptConfiguration;
import com.ccx.demo.config.encrypt.DataVersion;
import com.common.db.IUser;
import com.common.db.entity.Page;
import com.common.db.entity.Result;
import com.common.enums.Code;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.common.util.Dates.Pattern.yyyyMMdd;

/**
 * 对外接口：测试案例表
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Api(tags = "测试案例表", hidden = true)
//@ApiSort() // 控制接口排序
@RequestMapping("/code-example")
@Controller
@Slf4j
@RequiredArgsConstructor
public class CodeExampleController {

    private final CodeExampleService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询测试案例表", tags = {""})
    @ApiOperationSupport(
            order = 1
            , author = "谢长春"
            , includeParameters = {"number", "limit", "orderBy"
            , "name", "status", "createTimeRange.begin", "createTimeRange.end", "amountRange.min", "amountRange.max"
            , "createUserId", "updateUserId", "deleted"}
    )
    @ResponseBody
    public Result<TabCodeExample> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
            final TabCodeExample condition) {
        return new Result<TabCodeExample>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.page(condition, Page.of(number, limit)));
        });
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询测试案例表", tags = {""})
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "name", "status", "createUserId", "updateUserId", "deleted"}
//    )
//    @ResponseBody
//    public Result<TabCodeExample> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabCodeExample condition) {
//        return new Result<TabCodeExample>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list( condition ));
//        });
//    }
//

    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询测试案例表", tags = {""})
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<TabCodeExample> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id") @PathVariable final String id
    ) {
        return new Result<TabCodeExample>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(DOEncryptConfiguration.decryptLongId(id)).orElse(null));
        });
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "新增测试案例表", tags = {""})
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            includeParameters = {
                    "body.name", "body.content", "body.contentEncode", "body.amount", "body.status"
            })
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final TabCodeExampleInsertDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.insert(body, user.getId());
        });
    }

    @PutMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
    @ApiOperation(value = "修改测试案例表", tags = {""})
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            ignoreParameters = {
                    "body.name", "body.content", "body.contentEncode", "body.amount", "body.status"
            })
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv,
            @RequestBody final TabCodeExampleUpdateDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            final DataVersion dataVersion = DOEncryptConfiguration.decryptDV(dv)
                    .orElseThrow(() -> Code.A00003.toCodeException("数据版本号无效：%s", dv));
            body.setUpdateTime(dataVersion.getUpdateTime());
            service.update(dataVersion.getLongId(), user.getId(), body);
        });
    }

    @PatchMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除测试案例表", tags = {""})
    @ApiOperationSupport(order = 6, author = "谢长春")
    @ResponseBody
    public Result<Void> markDeleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv
    ) {
        return new Result<Void>().call(() -> {
            final DataVersion dataVersion = DOEncryptConfiguration.decryptDV(dv)
                    .orElseThrow(() -> Code.A00003.toCodeException("数据版本号无效：%s", dv));
            service.markDeleteById(dataVersion.getLongId(), dataVersion.getUpdateTime(), user.getId());
        });
    }

    @PatchMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除测试案例表", tags = {""})
    @ApiOperationSupport(
            order = 7,
            author = "谢长春",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDeleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<String> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            final List<DataVersion> decryptList = DOEncryptConfiguration.decryptDV(body).collect(Collectors.toList());
            service.markDeleteByIds(
                    decryptList.stream().map(DataVersion::getLongId).collect(Collectors.toSet())
                    , decryptList.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }

    @DeleteMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "物理删除测试案例表", tags = {""})
    @ApiOperationSupport(order = 8, author = "谢长春")
    @ResponseBody
    public Result<Void> deleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv) {
        return new Result<Void>().call(() -> {
            final DataVersion dataVersion = DOEncryptConfiguration.decryptDV(dv)
                    .orElseThrow(() -> Code.A00003.toCodeException("数据版本号无效：%s", dv));
            service.deleteById(dataVersion.getLongId(), dataVersion.getUpdateTime(), user.getId());
        });
    }

    @DeleteMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "物理删除测试案例表", tags = {""})
    @ApiOperationSupport(
            order = 9,
            author = "谢长春",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> deleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<String> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            final List<DataVersion> decryptList = DOEncryptConfiguration.decryptDV(body).collect(Collectors.toList());
            service.deleteByIds(
                    decryptList.stream().map(DataVersion::getLongId).collect(Collectors.toSet())
                    , decryptList.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }

    @GetMapping("/page/vo/{number}/{limit}")
    @ApiOperation(value = "扩展查询条件和返回参数", tags = {""})
    @ApiOperationSupport(order = 11, author = "谢长春")
    @ResponseBody
    public Result<TabCodeExampleVO> pageVO(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数", example = "1") @PathVariable final int limit,
            final TabCodeExampleVO condition) {
        return new Result<TabCodeExampleVO>().execute(result ->
                result.setSuccess(service.pageVO(condition, Page.of(number, limit)))
        );
    }


    @GetMapping()
    @ApiOperation(value = "扩展查询条件和返回参数", tags = {""})
    @ApiOperationSupport(order = 12, author = "谢长春")
    @ResponseBody
    public Result<TabCodeExample> list(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            final TabCodeExample condition) {
        return new Result<TabCodeExample>().execute(result -> result.setSuccess(service.list(condition)));
    }

    @GetMapping("/vo")
    @ApiOperation(value = "扩展查询条件和返回参数", tags = {""})
    @ApiOperationSupport(order = 12, author = "谢长春")
    @ResponseBody
    public Result<TabCodeExampleVO> listVO(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            final TabCodeExampleVO condition) {
        return new Result<TabCodeExampleVO>().execute(result -> result.setSuccess(service.listVO(condition)));
    }

    @ApiIgnore
    @GetMapping("/test")
    @ResponseBody
    public Result<Void> test(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Void>().call(service::listTest);
    }

    @GetMapping("/page/simple/{number}/{limit}")
    @ApiOperation(value = "连表查询指定字段", tags = {""})
    @ApiOperationSupport(order = 13, author = "谢长春")
    @ResponseBody
    public Result<TabCodeExampleVO> pageSimpleJoinTabUser(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数", example = "1") @PathVariable final int limit,
            final TabCodeExampleVO condition) {
        return new Result<TabCodeExampleVO>().execute(result -> result.setSuccess(service.pageSimpleJoinTabUser(
                condition,
                Page.builder().number(number).limit(limit).build())
        ));
    }

    @GetMapping("/page/full/{number}/{limit}")
    @ApiOperation(value = "连表查询主表全部字段", tags = {""})
    @ApiOperationSupport(order = 14, author = "谢长春")
    @ResponseBody
    public Result<TabCodeExampleVO> pageFullJoinTabUser(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数", example = "1") @PathVariable final int limit,
            final TabCodeExampleVO condition) {
        return new Result<TabCodeExampleVO>().execute(result -> result.setSuccess(service.pageFullJoinTabUser(
                condition,
                Page.builder().number(number).limit(limit).build())
        ));
    }

    @GetMapping("/csv")
    @ApiOperation(value = "导出csv格式", tags = {""})
    @ApiOperationSupport(order = 15, author = "谢长春")
    public void csv(
            @ApiIgnore @AuthenticationPrincipal final IUser user
            , final TabCodeExampleVO condition
            , final HttpServletResponse response) {
        try {
            final String fileName = URLEncoder.encode(String.format("文件导出测试_%s.csv", yyyyMMdd.now()), StandardCharsets.UTF_8.displayName());
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);// 设定输出文件头
            response.setContentType("text/csv;charset=UTF-8");// 定义输出类型
            @Cleanup final ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF}); // 加上UTF-8文件的标识字符， 解决文件内容乱码
            final byte[] newLine = "\r\n".getBytes();
            @Cleanup final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            bufferedOutputStream.write(String
                    .join(","
                            , "id"
                            , "name"
                            , "amount"
                            , "status"
                            , "createUserId"
                            , "createTime"
                            , "updateUserId"
                            , "updateTime"
                    )
                    .getBytes(StandardCharsets.UTF_8)
            );
            bufferedOutputStream.write(newLine);
            service.forEachCsv(condition, list -> list.forEach(row -> {
                try {
                    bufferedOutputStream.write(Stream
                            .of(
                                    row.getId()
                                    , row.getName()
                                    , row.getAmount()
                                    , row.getStatus()
                                    , row.getCreateUserId()
                                    , row.getCreateTime()
                                    , row.getUpdateUserId()
                                    , row.getUpdateTime()
                            )
                            .map(val -> Objects.isNull(val) ? "" : val)
                            .map(Object::toString)
                            .map(val -> val.replaceAll(",", "，"))
                            .collect(Collectors.joining(","))
                            .getBytes(StandardCharsets.UTF_8)
                    );
                    bufferedOutputStream.write(newLine);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

}
