package com.ccx.demo.business.code.vo;

import com.ccx.demo.business.code.entity.QTabCodeExample;
import com.ccx.demo.business.code.entity.TabCodeExample;
import com.ccx.demo.business.code.enums.DemoStatus;
import com.ccx.demo.business.user.entity.QTabUser;
import com.common.util.Dates;
import com.querydsl.core.types.ExpressionUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

import static com.ccx.demo.business.code.entity.QTabCodeExample.tabCodeExample;


/**
 * <pre>
 * 实体：扩展 {@link TabCodeExample}，增强单个实体的特性，同时满足 join 表的需求
 * 缺点在于 VO 类无法使用全部的 lombok 注解， VO 类继承后必须使用以下 3 个注解
 *
 * *@Getter @Setter @ToString(callSuper = true)
 * *@EqualsAndHashCode(callSuper = true)
 * *@ToString(callSuper = true)
 * </pre>
 *
 * @author 谢长春 2020-01-17
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "扩展：测试案例表：TabCodeExample")

public class TabCodeExampleVO extends TabCodeExample {

    private static final long serialVersionUID = -7251222373656975725L;
    /**
     * 查询参数：状态集合
     */
    @ApiModelProperty(value = "查询参数：状态集合。com.ccx.demo.business.example.enums.DemoStatus")
    private Set<DemoStatus> statusArray;
    /**
     * 更新时间查询区间
     */
    @ApiModelProperty(value = "查询参数：更新时间查询区间")
    private Dates.Range updateTimeRange;
    /**
     * 扩展连表查询字段
     */
    private String[] roles;
    /**
     * 扩展连表查询字段及查询条件
     */
    private String username;

    private String createUserNickname;
    private String updateUserNickname;

    @Override
    public QdslWhere where() {
        final QTabCodeExample table = tabCodeExample;
        final QTabUser qTabUser = QTabUser.tabUser;
        // 增强扩展查询条件
        return super.where()
                .andIfNonEmpty(statusArray, () -> table.status.in(statusArray))
                .and(updateTimeRange, () -> ExpressionUtils.and(
                        updateTimeRange.rebuild().ifPresentUpdateTimeBegin(table.createTime::goe)
                        , updateTimeRange.ifPresentUpdateTimeEnd(table.createTime::loe)
                ))
                .and(username, () -> qTabUser.username.eq(username))
                ;

    }
}
