package com.ccx.demo.business.code.dto;

import com.ccx.demo.business.code.entity.TabCodeExample;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * DTO：测试案例表新增
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义测试案例表新增时的扩展属性
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "测试案例表")
public class TabCodeExampleInsertDTO extends TabCodeExample {
    private static final long serialVersionUID = 1L;


    @NotBlank
    @Override
    public String getName() {
        return super.getName();
    }

}
