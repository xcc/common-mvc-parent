package com.ccx.demo.config;

import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author 谢长春 2018-10-3
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@PropertySource(value = "classpath:demo-service.properties", encoding = "UTF-8")
public class DemoServiceConfiguration {
    @Value("${demo.service.service.name}")
    private String name;
    @Value("${demo.service.service.pkg}")
    private String pkg;
    @Value("${demo.service.service.comment}")
    private String comment;

//    /**
//     * 初始化配置信息
//     *
//     * @author 谢长春
//     */
//    @Component
//    @Slf4j
//    @RequiredArgsConstructor
//    public class DemoServiceConfigInitializer implements ApplicationListener<ContextRefreshedEvent> {
//        private final DemoServiceAppProperties properties;
//
//        @Override
//        public void onApplicationEvent(ContextRefreshedEvent event) {
//            log.info("┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬ 服务参数配置 ┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬");
//            log.info("\n{}", properties.toString());
//            log.info("┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴ 服务参数配置 ┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴");
//        }
//    }

    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , com.google.common.base.Strings.repeat(" ┳", 30)
            , com.google.common.base.Strings.repeat(" ┳", 30)
            , com.google.common.base.Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

}
