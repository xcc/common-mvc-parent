package com.ccx.demo.business.code.enums;

import com.common.entity.Item;

/**
 * 枚举：Demo数据状态
 *
 * @author 谢长春 2018/12/17
 */
public enum DemoStatus {
    _NONE("无效"),
    WATING("等待中"),
    RUNNING("执行中"),
    SUCCESS("成功"),
    FAILURE("失败"),
    ;
    /**
     * 枚举属性说明
     */
    public final String comment;
    /**
     * 是否已废弃
     */
    public final boolean deprecated;

    DemoStatus(final String comment) {
        this(comment, false);
    }

    DemoStatus(final String comment, final boolean deprecated) {
        this.comment = comment;
        this.deprecated = deprecated;
    }

    /**
     * 转换为 {@link Item} 对象
     *
     * @return {@link Item}
     */
    public Item getObject() {
        return new Item()
                .setKey(this.name())
                .setValue(this.ordinal())
                .setComment(this.comment)
                ;
    }
}
