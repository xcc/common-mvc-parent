package com.ccx.demo.business.code.service;

import com.ccx.demo.business.code.dao.jpa.CodeSearchIdRepository;
import com.ccx.demo.business.code.entity.TabCodeSearchId;
import com.common.db.entity.Page;
import com.google.common.base.Strings;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;


/**
 * 服务接口实现类：测试 SearchId 模板表
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor

public class CodeSearchIdService {
    private final CodeSearchIdRepository repository;


    /**
     * 测试 SearchId 模板表 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link String} 数据ID
     * @return {@link Optional<TabCodeSearchId>} 实体对象
     */

    public Optional<TabCodeSearchId> findById(final String id) {
        if (Strings.isNullOrEmpty(id)) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabCodeSearchId::cloneObject); // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作

    }

    /**
     * 测试 SearchId 模板表 按条件分页查询列表
     *
     * @param condition {@link TabCodeSearchId} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return {@link QueryResults<TabCodeSearchId>} 分页对象
     */

    public @NotNull(message = "返回值不能为null") QueryResults<TabCodeSearchId> page(
            @NotNull(message = "【condition】不能为null") final TabCodeSearchId condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<TabCodeSearchId> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        //this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试 SearchId 模板表 按条件查询列表
//     *
//     * @param condition {@link TabCodeSearchId} 查询条件
//     * @return {@link List<TabCodeSearchId>} 结果集合
//     */
//
//    public @NotNull(message = "返回值不能为null") List<TabCodeSearchId> list(
//            @NotNull(message = "【condition】不能为null") final TabCodeSearchId condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 测试 SearchId 模板表 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link String}  数据 id 集合
     * @return {@link List<TabCodeSearchId>} 结果集合
     */

    public @NotNull(message = "返回值不能为null") List<TabCodeSearchId> listByIds(final Collection<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }

    /**
     * 测试 SearchId 模板表 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link String} 数据 id 集合
     * @return {@link List<TabCodeSearchId>} 结果集合
     */

    public @NotNull(message = "返回值不能为null") Map<String, TabCodeSearchId> mapByIds(final Set<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }

}
