package com.ccx.demo.business.code.entity;

import com.common.IJson;
import com.common.db.ITable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

/**
 * @author 谢长春 2018/12/20
 */
@Getter
@Setter
@ToString
@Slf4j

public class TabValid implements ITable, IJson {

    private static final long serialVersionUID = 1494537060273446575L;
    @Positive
    private Long id;
    @Size(max = 10)
    private String label;
    @PositiveOrZero
    private Short value;
    @Size(max = 30)
    private String content;

    public TabValid log() {
        log.debug(this.toString());
        return this;
    }

    public static TabValid.TabValidBuilder builder() {
        return new TabValid.TabValidBuilder();
    }

    public static class TabValidBuilder {
        private Long id;
        private String label;
        private Short value;
        private String content;

        TabValidBuilder() {
        }

        public TabValid.TabValidBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public TabValid.TabValidBuilder label(String label) {
            this.label = label;
            return this;
        }

        public TabValid.TabValidBuilder value(Short value) {
            this.value = value;
            return this;
        }

        public TabValid.TabValidBuilder content(String content) {
            this.content = content;
            return this;
        }

        public TabValid build() {
            final TabValid obj = new TabValid();
            obj.setId(this.id);
            obj.setLabel(this.label);
            obj.setValue(this.value);
            obj.setContent(this.content);
            return obj;
        }

    }

}
