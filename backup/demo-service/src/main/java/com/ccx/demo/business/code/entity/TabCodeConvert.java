package com.ccx.demo.business.code.entity;

import com.ccx.demo.business.code.entity.convert.ArrayCodeJsonConvert;
import com.ccx.demo.config.DOEncryptConfiguration;
import com.ccx.demo.config.encrypt.annotations.AesIdLongJsonConverter;
import com.ccx.demo.enums.AppCode;
import com.common.db.ITable;
import com.common.db.IWhere;
import com.common.db.IWhere.QdslWhere;
import com.common.db.convert.*;
import com.common.db.entity.OrderBy;
import com.common.entity.Item;
import com.common.enums.Code;
import com.common.util.JSON;
import com.common.util.Then;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.QueryEntity;
import com.querydsl.core.annotations.QueryTransient;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAUpdateClause;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.ccx.demo.business.code.entity.QTabCodeConvert.tabCodeConvert;
import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 实体类：测试自定义 Convert 表
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Table(name = "tab_code_convert")
@Entity
@QueryEntity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@ToString
@ApiModel(description = "测试自定义 Convert 表")

public class TabCodeConvert implements
        ITable, // 所有与数据库表 - 实体类映射的表都实现该接口；方便后续一键查看所有表的实体
        // JPAUpdateClause => 需要的动态更新字段；采用 方案2 时需要实现该接口
        // QdslWhere       => 动态查询条件
        IWhere<JPAUpdateClause, QdslWhere> {
    private static final long serialVersionUID = 1L;

    /**
     * 数据ID
     */
    @AesIdLongJsonConverter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Positive
    @ApiModelProperty(value = "数据ID", position = 1)
    private Long id;
    /**
     * {@link List<Long>}
     */
    @Column(name = "ids")
    @Convert(converter = ArrayLongJsonConvert.class)
    @ApiModelProperty(value = "{@link List<Long>}", position = 2)
    private Long[] ids;
    /**
     * {@link List<String>}
     */
    @Column(name = "images")
    @Convert(converter = ArrayStringJsonConvert.class)
    @ApiModelProperty(value = "{@link List<String>}", position = 3)
    private String[] images;
    /**
     * {@link List< AppCode >}
     */
    @Column(name = "codes")
    @Convert(converter = ArrayCodeJsonConvert.class)
    @ApiModelProperty(value = "{@link List<com.support.mvc.enums.AppCode>}", position = 4)
    private AppCode[] codes;
    /**
     * {@link List< Item >}
     */
    @Column(name = "items")
    @Convert(converter = ArrayItemJsonConvert.class)
    @ApiModelProperty(value = "{@link List<com.support.mvc.entity.base.Item>}", position = 5)
    private Item[] items;
    /**
     * {@link Item}
     */
    @Column(name = "item")
    @Convert(converter = ItemJsonConvert.class)
    @ApiModelProperty(value = "{@link com.support.mvc.entity.base.Item}", position = 6)
    private Item item;
    /**
     * 文本，原文
     */
    @Column(name = "textOriginal")
    @Size(max = 100)
    @ApiModelProperty(value = "文本，原文", position = 7)
    private String textOriginal;
    /**
     * 文本，base64编码
     */
    @Column(name = "textBase64")
    @Size(max = 2500)
    @Convert(converter = Base64Convert.class)
    @ApiModelProperty(value = "文本，base64编码", position = 8)
    private String textBase64;
    /**
     * 创建时间，yyyyMMddHHmmss
     */
    @Column(name = "createTime")
    @Size(min = 14, max = 14)
    @ApiModelProperty(value = "数据新增时间，格式：yyyyMMddHHmmss", example = "20200202020202", position = 9)
    private String createTime;
    /**
     * 创建用户ID
     */
    @Column(name = "createUserId")
    @PositiveOrZero
    @ApiModelProperty(value = "新增操作人id", position = 10)
    private Long createUserId;
    /**
     * 修改时间，yyyyMMddHHmmssSSS
     */
    @Column(name = "updateTime")
    @Size(min = 17, max = 17)
    @ApiModelProperty(value = "数据最后一次更新时间，格式：yyyyMMddHHmmssSSS", example = "20200202020202002", position = 11)
    private String updateTime;
    /**
     * 修改用户ID
     */
    @Column(name = "updateUserId")
    @PositiveOrZero
    @ApiModelProperty(value = "更新操作人id", position = 12)
    private Long updateUserId;
    /**
     * 是否逻辑删除。 [0:否,1:是]
     */
    @Column(name = "deleted")
    @ApiModelProperty(value = "是否逻辑删除。 [0:否,1:是]", position = 13)
    private Boolean deleted;

    /**
     * 排序字段
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(value = "查询排序字段")
    private List<OrderBy> orderBy;

    /**
     * 防止踩坑。
     * 当 JPA 查询对象没有 clone 时，执行 setValue 操作时，会触发更新动作，这里拦截之后抛出异常
     */
    @PreUpdate
    public void onPreUpdate() {
        throw Code.A00003.toCodeException("防止踩坑，谨慎使用JPA默认的 update 方法。 如果业务逻辑确定需要使用 setValue 方式更新对象时，去掉这个异常");
    }

    /**
     * 数据版本号，用于更新和删除参数，规避脏数据更新
     *
     * @return String
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "数据版本号，用于更新和删除参数，规避脏数据更新", position = 1, accessMode = READ_ONLY)
    public String getDv() {
        return DOEncryptConfiguration.encryptDV(id, getUpdateTime());
    }

    @SneakyThrows
    public TabCodeConvert cloneObject() {
        return (TabCodeConvert) super.clone();
    }

// Enum Start **********************************************************************************************************

    /**
     * 枚举：定义排序字段
     */
    public enum OrderByColumn {
        // 按 id 排序可替代按创建时间排序
        id(tabCodeConvert.id),
//        ids(tabCodeConvert.ids),
//        images(tabCodeConvert.images),
//        codes(tabCodeConvert.codes),
//        items(tabCodeConvert.items),
//        item(tabCodeConvert.item),
//        textOriginal(tabCodeConvert.textOriginal),
//        textBase64(tabCodeConvert.textBase64),
//        createTime(tabCodeConvert.createTime),
//        createUserId(tabCodeConvert.createUserId),
//        updateTime(tabCodeConvert.updateTime),
//        updateUserId(tabCodeConvert.updateUserId),
//        deleted(tabCodeConvert.deleted),
        ;
        private final ComparableExpressionBase<?> column;

        public OrderBy asc() {
            return new OrderBy().setName(this.name());
        }

        public OrderBy desc() {
            return new OrderBy().setName(this.name()).setDirection(Sort.Direction.DESC);
        }

        /**
         * 获取所有排序字段名
         *
         * @return String[]
         */
        public static String[] names() {
            return Stream.of(OrderByColumn.values()).map(Enum::name).toArray(String[]::new);
        }

        OrderByColumn(final ComparableExpressionBase<?> column) {
            this.column = column;
        }
    }

// Enum End : DB Start *************************************************************************************************

    @Override
    public Then<JPAUpdateClause> update(final JPAUpdateClause jpaUpdateClause) {
        final QTabCodeConvert table = tabCodeConvert;
        // 动态拼接 update 语句
        // 以下案例中 只有 name 属性 为 null 时才不会加入 update 语句；
        return Then.of(jpaUpdateClause)
                .then(ids, update -> update.set(table.ids, ids))
                .then(images, update -> update.set(table.images, images))
                .then(codes, update -> update.set(table.codes, codes))
                .then(items, update -> update.set(table.items, items))
                .then(item, update -> update.set(table.item, item))
                .then(textOriginal, update -> update.set(table.textOriginal, textOriginal))
                .then(textBase64, update -> update.set(table.textBase64, textBase64))
//                .then(updateUserId, update -> update.set(table.updateUserId, updateUserId))
////                // 当 name != null 时更新 name 属性
////                .then(name, update -> update.set(table.name, name))
////                .then(update -> update.set(table.updateUserId, updateUserId))
////                // 假设数据库中 content is not null；可以在属性为null时替换为 ""
////                .then(update -> update.set(table.content, Optional.ofNullable(content).orElse("")))
////                // 数据库中 amount 可以为 null
////                .then(update -> update.set(table.amount, amount))
                ;
    }

    @Override
    public QdslWhere where() {
        final QTabCodeConvert table = tabCodeConvert;
        // 构建查询顺序规则请参考：com.support.mvc.entity.IWhere#where
        return QdslWhere.of()
                .and(id, () -> table.id.eq(id))
                .andIfNonEmpty(ids, () -> {
                    final QdslWhere.Or expressions = QdslWhere.Or.of();
                    for (Long id : ids) {
                        expressions.or(Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.ids, id));
                    }
                    return expressions.toPredicate();
                })
                .andIfNonEmpty(images, () -> {
                    final QdslWhere.Or expressions = QdslWhere.Or.of();
                    for (String img : images) {
                        expressions.or(Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.images, img));
                    }
                    return expressions.toPredicate();
                })
                .andIfNonEmpty(codes, () -> {
                    final QdslWhere.Or expressions = QdslWhere.Or.of();
                    for (AppCode code : codes) {
                        expressions.or(Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.codes, code));
                    }
                    return expressions.toPredicate();
                })
                .and(textOriginal, () -> table.textOriginal.contains(textOriginal))
                .and(textBase64, () -> table.textBase64.eq(textBase64))
//                .and(ids, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.ids, JSON.toJsonString(ids)))
//                .and(images, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.images, JSON.toJsonString(images)))
//                .and(codes, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.codes, JSON.toJsonString(codes)))
//                .and(items, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.items, JSON.toJsonString(items)))
//                .and(item, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.item, JSON.toJsonString(item)))
//                .and(textOriginal, () -> textOriginal.endsWith("%") || textOriginal.startsWith("%") ? table.textOriginal.like(textOriginal) : table.textOriginal.startsWith(textOriginal))
//                .and(textBase64, () -> textBase64.endsWith("%") || textBase64.startsWith("%") ? table.textBase64.like(textBase64) : table.textBase64.startsWith(textBase64))
//                .and(createTime, () -> createTime.endsWith("%") || createTime.startsWith("%") ? table.createTime.like(createTime) : table.createTime.eq(createTime))
//                .and(createUserId, () -> table.createUserId.eq(createUserId))
//                .and(updateTime, () -> updateTime.endsWith("%") || updateTime.startsWith("%") ? table.updateTime.like(updateTime) : table.updateTime.eq(updateTime))
//                .and(updateUserId, () -> table.updateUserId.eq(updateUserId))
                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
////                .and(phone, () -> table.phone.eq(phone))
////                .and(createUserId, () -> table.createUserId.eq(createUserId))
////                .and(updateUserId, () -> table.updateUserId.eq(updateUserId))
////                // 强制带默认值的查询字段
////                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
////                // 数字区间查询
////                .and(amountRange, () -> table.amount.between(amountRange.getMin(), amountRange.getMax()))
////                // 日期区间查询；Range.rebuild() : 先将时间区间重置到 00:00:00.000 - 23:59:59.999 ; 大多数情况都需要重置时间
////                .and(createTimeRange, () -> ExpressionUtils.and(createTimeRange.rebuild().ifPresentCreateTimeBegin(table.createTime::goe), createTimeRange.ifPresentCreateTimeEnd(table.createTime::loe)))
////                .and(updateTimeRange, () -> ExpressionUtils.and(updateTimeRange.rebuild().ifPresentUpdateTimeBegin(table.createTime::goe), updateTimeRange.ifPresentUpdateTimeEnd(table.createTime::loe)))
////                // 模糊匹配查询：后面带 % ；建议优先使用
////                .and(name, () -> table.name.startsWith(name)) // 模糊匹配查询：后面带 %
////                .and(name, () -> table.name.endsWith(name)) // 模糊匹配查询：前面带 %
////                .and(name, () -> table.name.contains(name)) // 模糊匹配查询：前后带 %,同 MessageFormat.format("%{0}%", name)
////                .and(name, () -> table.name.like(MessageFormat.format("%{0}%", name))) 模糊匹配查询：前后带 %
                ;
    }

    /**
     * 排序参数构建：QueryDSL 查询模式；QueryDSL 模式构建排序对象返回 null 则会抛出异常
     *
     * @return {@link OrderSpecifier[]}
     */
    public OrderSpecifier<?>[] qdslOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return new OrderSpecifier<?>[]{OrderByColumn.id.column.asc()}; // 指定默认排序字段
                return new OrderSpecifier<?>[]{};
            }
            return orderBy.stream().map((OrderBy by) -> {
                final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                // 自定义排序
                // new OrderSpecifier<>(Order.valueOf(by.getDirection().name()) , Expressions.stringTemplate("convert_gbk({0})", column.column));

                // return Objects.equals(orderBy.getDirection(), Sort.Direction.DESC) ? column.column.desc(): column.column.asc();
                return new OrderSpecifier<>(
                        Order.valueOf(by.getDirection().name())
                        , column.column
                );
            }).toArray(OrderSpecifier<?>[]::new);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 排序参数构建：Spring JPA 查询模式；JPA 模式构建排序对象返回 null 表示不排序
     *
     * @return {@link Sort}
     */
    public Sort jpaOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return Sort.by(Sort.Direction.ASC, OrderByColumn.id.name()); // 指定默认排序字段
                return Sort.unsorted();
            }
            return orderBy.stream()
                    .map((OrderBy by) -> {
                        final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                        // 自定义排序
                        // JpaSort.unsafe(by.getDirection(), String.format("convert_gbk(%s)", column.name()));
                        return Sort.by(by.getDirection(), column.name());
                    })
                    .reduce(Sort::and)
                    .orElseGet(Sort::unsorted);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 获取查询实体与数据库表映射的所有字段,用于投影到 VO 类
     * 支持追加扩展字段,追加扩展字段一般用于连表查询
     *
     * @param appends {@link Expression}[] 追加扩展连表查询字段
     * @return {@link Expression}[]
     */
    public static Expression<?>[] allColumnAppends(final Expression<?>... appends) {
        final List<Expression<?>> columns = Lists.newArrayList(appends);
        final Class<?> clazz = tabCodeConvert.getClass();
        try {
            for (Field field : clazz.getDeclaredFields()) {
                if (field.getType().isPrimitive()) continue;
                final Object o = field.get(tabCodeConvert);
                if (o instanceof EntityPath || o instanceof BeanPath) continue;
                if (o instanceof Path) {
                    columns.add((Path<?>) o);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("获取查询实体属性与数据库映射的字段异常", e);
        }
        return columns.toArray(new Expression<?>[0]);
    }

// DB End **************************************************************************************************************

}
