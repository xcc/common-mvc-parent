package com.ccx.demo.business.code.service;

import com.ccx.demo.business.code.entity.TabCodeExample;
import com.common.IAutoTask;
import com.common.util.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


/**
 * 服务接口实现类：测试案例表
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class CodeExampleAutoTaskService
        implements IAutoTask {
    private final CodeExampleService service;

    @Override
    public void call(String args) {
        service.forEachExample1(list -> {
            list.forEach(row -> {
                try {
                    log.info(JSON.toJsonString(row));
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            });
        });
        service.forEachExample2(
                new TabCodeExample(),
                list -> {
                    list.forEach(row -> {
                        try {
                            log.info(JSON.toJsonString(row));
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                        }
                    });
                }
        );
    }
}
