package com.ccx.demo;

import org.springframework.boot.SpringApplication;

/**
 * 组合打包所有发布模块，避免出现循环依赖
 *
 * @author 谢长春 2019/1/25
 */
public class AppDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }
}
