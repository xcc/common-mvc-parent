package com.ccx.demo.wechat.web;
//
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import com.ccx.demo.config.init.AppConfig;
//import com.jfinal.weixin.sdk.api.AccessTokenApi;
//import com.jfinal.weixin.sdk.api.ApiConfigKit;
//import com.jfinal.weixin.sdk.kit.SignatureCheckKit;
//import com.support.mvc.enums.Code;
//import com.common.util.Maps;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.Assert;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * 微信公众号配置
// *
// * @author 谢长春 2020-05-09
// */
//@Slf4j
//@Controller
//@RequiredArgsConstructor
//@RequestMapping("/open/wechat-config")
//public class WechatConfigController {
//    private final AppConfig appConfig;
//
//    /**
//     * 查看微信token
//     * URL:/admin/wechat/token
//     * 请求方式：GET
//     * 参数：
//     */
//    @GetMapping("/token")
//    @ResponseBody
//    public String token() {
//        try {
//            ApiConfigKit.setThreadLocalAppId(appConfig.getWechat().getId());
//            AccessTokenApi.getAccessToken();
//            return ApiConfigKit.getAccessTokenCache().get(ApiConfigKit.getAppId());
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            return Code.FAILURE.name();
//        } finally {
//            ApiConfigKit.removeThreadLocalAppId();
//        }
//    }
//
//    /**
//     * 微信配置接口
//     *
//     * @return String
//     */
//    @GetMapping(produces = {"text/xml"})
//    @ResponseBody
//    public String config(
//            @RequestParam(required = false) final String echostr,
//            @RequestParam final String signature,
//            @RequestParam final String timestamp,
//            @RequestParam final String nonce
//    ) {
//        try {
//            Assert.hasLength(signature, () -> "参数【signature】不能为空");
//            Assert.hasLength(timestamp, () -> "参数【timestamp】不能为空");
//            Assert.hasLength(nonce, () -> "参数【nonce】不能为空");
//            if (!SignatureCheckKit.me.checkSignature(
//                    signature,
//                    appConfig.getWechat().getToken(),
//                    timestamp,
//                    nonce)
//            ) {
//                throw new Exception("验签失败：".concat(Maps.of()
//                        .put("signature", signature)
//                        .put("timestamp", timestamp)
//                        .put("nonce", nonce)
//                        .json(SerializerFeature.PrettyFormat)
//                ));
//            }
//            if (StringUtils.isNotBlank(echostr)) {
//                return echostr;
//            }
////            // 处理微信消息
////            final String xml = service.process(Message.parse(request));
////            if (Objects.nonNull(xml)) return xml;
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//        return "";
//    }
//
//}
