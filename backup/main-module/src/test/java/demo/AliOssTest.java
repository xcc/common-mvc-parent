package demo;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.Bucket;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AliOssTest {
    @SneakyThrows
    public static void main(String[] args) {
        // 创建OSSClient实例。
        @Cleanup("shutdown") final OSS ossClient = new OSSClientBuilder().build(
                "oss-cn-shanghai.aliyuncs.com",
                "LTAI5tEZcbFLbvZsF9QmU6eI",
                "lWWWLEkD2bDi6OOrUORv98iTsTUXMP"
        );
        ossClient.listBuckets().forEach(bucket -> {
            log.info("Name={},Location={},Region={},ExtranetEndpoint={},IntranetEndpoint={},Owner={}"
                    , bucket.getName()
                    , bucket.getLocation()
                    , bucket.getRegion()
                    , bucket.getExtranetEndpoint()
                    , bucket.getIntranetEndpoint()
                    , bucket.getOwner()
            );
        });
        final String bucketName = "app-x";
        // 检查存储桶是否已经存在
        final boolean isBucketExist = ossClient.doesBucketExist(bucketName);
        System.out.println(isBucketExist);
        if (!isBucketExist) {
            final Bucket bucket = ossClient.createBucket(bucketName);
            log.info("Name={},Location={},Region={},ExtranetEndpoint={},IntranetEndpoint={},Owner={}"
                    , bucket.getName()
                    , bucket.getLocation()
                    , bucket.getRegion()
                    , bucket.getExtranetEndpoint()
                    , bucket.getIntranetEndpoint()
                    , bucket.getOwner()
            );
        }

    }
}
