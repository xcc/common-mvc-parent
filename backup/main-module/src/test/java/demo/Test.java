package demo;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.nio.file.Paths;

@Slf4j
public class Test {
    @SneakyThrows
    public static void main(String[] args) {
        System.out.println(Paths.get("").toAbsolutePath());
    }
}
