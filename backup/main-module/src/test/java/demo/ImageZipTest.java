package demo;

import com.common.enums.Image;
import com.common.util.Base64;
import lombok.Cleanup;
import lombok.SneakyThrows;
import net.coobird.thumbnailator.Thumbnails;

import java.io.ByteArrayOutputStream;

public class ImageZipTest {
    @SneakyThrows
    public static void main(String[] args) {
        // 等比缩放（设置缩放比例）
        Thumbnails
                .of("doc/vim.jpeg")
                .scale(0.5D)
                .outputQuality(0.5D)
                .toFile(".temp/vim-1.jpeg")
        ;
        // 等比缩放（限定宽高）
        Thumbnails
                .of("doc/vim.jpeg")
                .width(100)
                .height(100)
                .toFile(".temp/vim-2.jpeg")
        ;
        // 强制输出尺寸 100x100 图片会拉伸变形
        Thumbnails
                .of("doc/vim.jpeg")
                .forceSize(100, 100)
                .toFile(".temp/vim-3.jpeg");
        {
            @Cleanup final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Thumbnails
                    .of("doc/vim.jpeg")
                    .width(700)
                    .toOutputStream(byteArrayOutputStream);
            System.out.println("\n\n");
            System.out.println(Image.JPEG.base64(Base64.encoder(byteArrayOutputStream.toByteArray())));
            System.out.println("\n\n");
        }
    }
}
