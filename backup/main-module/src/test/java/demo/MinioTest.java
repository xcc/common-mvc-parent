package demo;
//
//import io.minio.*;
//import lombok.Cleanup;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.BufferedInputStream;
//import java.io.FileInputStream;
//
//@Slf4j
//public class MinioTest {
//    @SneakyThrows
//    public static void main(String[] args) {
//        // 使用MinIO服务的URL，端口，Access key和Secret key创建一个MinioClient对象
//        MinioClient minioClient = MinioClient.builder()
//                .endpoint("http://localhost:9001")
//                .credentials("minio", "11111111")
//                .build();
//        final String bucketName = "my-bucket";
//        // 检查存储桶是否已经存在
//        boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder()
//                .bucket(bucketName)
//                .build());
//        if (isExist) {
//            log.info("Bucket[{}] already exists.", bucketName);
//        } else {
//            // 创建存储桶
//            minioClient.makeBucket(MakeBucketArgs.builder()
//                    .bucket(bucketName)
//                    .build());
//            // 设置权限
//            // {"Statement": [{"Action": ["s3:GetBucketLocation", "s3:ListBucket"], "Effect": "Allow", "Principal": "*", "Resource": "arn:aws:s3:::%s"}, {"Action": "s3:GetObject", "Effect": "Allow", "Principal": "*", "Resource": "arn:aws:s3:::%s/myobject*"}], "Version": "2012-10-17"}
//            minioClient.setBucketPolicy(SetBucketPolicyArgs.builder()
//                    .bucket(bucketName)
//                    .config(String.format("{\"Version\": \"2012-10-17\", \"Statement\": [{\"Action\": \"s3:GetObject\", \"Effect\": \"Allow\", \"Principal\": \"*\", \"Resource\": \"arn:aws:s3:::%s/*\"}]}"
//                            , bucketName
//                    ))
//                    .build());
//        }
//        {
//            @Cleanup BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(
//                    "/home/ccx/git-repository/common-mvc-parent/.temp/a.log"
//            ));
//            {
//                // 上传文件到存储桶
//                ObjectWriteResponse response = minioClient.putObject(PutObjectArgs.builder()
//                        .bucket(bucketName)
//                        .object("a.log")
//                        .stream(bufferedInputStream, bufferedInputStream.available(), -1)
//                        .build());
//                log.info("headers={}, bucket={}, region={}, object={}, etag={}, versionId={}"
//                        , response.headers()
//                        , response.bucket()
//                        , response.region()
//                        , response.object()
//                        , response.etag()
//                        , response.versionId()
//                );
//            }
//            {
//                // 上传文件到存储桶
//                ObjectWriteResponse response = minioClient.putObject(PutObjectArgs.builder()
//                        .bucket(bucketName)
//                        .object("a/b.log")
//                        .stream(bufferedInputStream, bufferedInputStream.available(), -1)
//                        .build());
//                log.info("headers={}, bucket={}, region={}, object={}, etag={}, versionId={}"
//                        , response.headers()
//                        , response.bucket()
//                        , response.region()
//                        , response.object()
//                        , response.etag()
//                        , response.versionId()
//                );
//            }
//            {
//
//                @Cleanup BufferedInputStream imageInputStream = new BufferedInputStream(new FileInputStream(
//                        "/home/ccx/git-repository/common-mvc-parent/.temp/a.jpg"
//                ));
//                // 上传文件到存储桶
//                ObjectWriteResponse response = minioClient.putObject(PutObjectArgs.builder()
//                        .bucket(bucketName)
//                        .object("a.jpg")
//                        .stream(imageInputStream, imageInputStream.available(), -1)
//                        .build());
//                log.info("headers={}, bucket={}, region={}, object={}, etag={}, versionId={}"
//                        , response.headers()
//                        , response.bucket()
//                        , response.region()
//                        , response.object()
//                        , response.etag()
//                        , response.versionId()
//                );
//            }
//        }
//
//        minioClient.listBuckets().forEach(bucket -> {
//            log.info(bucket.name());
//        });
//        minioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).recursive(true).build()).forEach(obj -> {
//            try {
//                log.info(obj.get().objectName());
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//            }
//        });
//        {
//            @Cleanup final GetObjectResponse response = minioClient.getObject(GetObjectArgs.builder()
//                    .bucket(bucketName)
//                    .object("a.log")
//                    .build());
//            System.out.println(response);
//        }
//        {
//            @Cleanup final GetObjectResponse response = minioClient.getObject(GetObjectArgs.builder()
//                    .bucket(bucketName)
//                    .object("b.jpg")
//                    .build());
//            System.out.println(response.headers());
//        }
//        {
////            @Cleanup final GetObjectResponse response = minioClient.getObject(GetObjectArgs.builder()
////                    .bucket("app-demo")
////                    .object("user/17e9785be2e521e2d793a66fa5768283.jpg")
////                    .build());
////            System.out.println(response.headers());
//        }
//    }
//}
