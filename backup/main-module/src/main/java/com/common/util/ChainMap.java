package com.common.util;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * 链式 HashMap
 *
 * @param <K>
 * @param <V>
 * @author 谢长春 2022-06-12
 */
@RequiredArgsConstructor
public class ChainMap<K, V> {
    @Getter
    private final Map<K, V> mapObject;

    public static <K, V> ChainMap<K, V> empty() {
        return new ChainMap<>(new HashMap<>(0));
    }

    public static <K, V> ChainMap<K, V> create() {
        return newLinkedHashMap();
    }

    public static <K, V> ChainMap<K, V> newHashMap() {
        return new ChainMap<>(Maps.newHashMap());
    }

    public static <K, V> ChainMap<K, V> newLinkedHashMap() {
        return new ChainMap<>(Maps.newLinkedHashMap());
    }

    public static <K, V> ChainMap<K, V> create(final Map<K, V> values) {
        return newLinkedHashMap(values);
    }

    public static <K, V> ChainMap<K, V> newHashMap(final Map<K, V> values) {
        final ChainMap<K, V> map = new ChainMap<>(Maps.newHashMap());
        return map.putAll(values);
    }

    public static <K, V> ChainMap<K, V> newLinkedHashMap(final Map<K, V> values) {
        final ChainMap<K, V> map = new ChainMap<>(Maps.newLinkedHashMap());
        map.putAll(values);
        return map;
    }

    public int size() {
        return mapObject.size();
    }

    public boolean isEmpty() {
        return mapObject.isEmpty();
    }

    public boolean containsKey(K key) {
        return mapObject.containsKey(key);
    }

    public boolean containsValue(V value) {
        return mapObject.containsValue(value);
    }

    public ChainMap<K, V> put(final K key, final V value) {
        if (Objects.isNull(key) || Objects.isNull(value)) {
            return this;
        }
        mapObject.put(key, value);
        return this;
    }

    public ChainMap<K, V> clear() {
        this.mapObject.clear();
        return this;
    }

    public ChainMap<K, V> putIfAbsent(final K key, final V value) {
        if (Objects.isNull(key) || Objects.isNull(value)) {
            return this;
        }
        mapObject.putIfAbsent(key, value);
        return this;
    }

    public ChainMap<K, V> putAll(final Map<K, V> values) {
        mapObject.putAll(values);
        return this;
    }

    public V get(K key) {
        return mapObject.get(key);
    }

    public <T> Optional<T> getObject(K key, Class<T> clazz) {
        return Optional.ofNullable(mapObject.get(key)).map(Objects::toString).map(jsonText -> JSON.parseObject(jsonText, clazz));
    }

    public <T> List<T> getArray(K key, Class<T> clazz) {
        Optional<String> optional = Optional.ofNullable(mapObject.get(key)).map(Objects::toString);
        if (optional.isPresent()) {
            String jsonText = optional.get();
            return JSON.parseList(jsonText, clazz);
        }
        return Collections.emptyList();
    }

    public Optional<String> getString(K key) {
        return Optional.ofNullable(mapObject.get(key)).map(Objects::toString);
    }

    public Optional<Long> getLong(K key) {
        return Optional.ofNullable(mapObject.get(key)).map(Objects::toString).filter(StringUtils::isNoneBlank).map(Long::parseLong);
    }

    public Optional<Integer> getInteger(K key) {
        return Optional.ofNullable(mapObject.get(key)).map(Objects::toString).filter(StringUtils::isNoneBlank).map(Integer::parseInt);
    }

    public V getOrDefault(final K key, final V defaultValue) {
        return mapObject.getOrDefault(key, defaultValue);
    }

    public ChainMap<K, V> getAll(final Collection<K> keys) {
        final ChainMap<K, V> newMap = ChainMap.create();
        keys.forEach(key -> {
            if (mapObject.containsKey(key)) {
                newMap.put(key, mapObject.get(key));
            }
        });
        return newMap;
    }

    public ChainMap<K, V> remove(K key) {
        if (Objects.nonNull(key)) {
            mapObject.remove(key);
        }
        return this;
    }

    public ChainMap<K, V> remove(final Set<K> keys) {
        if (Objects.nonNull(keys)) {
            keys.forEach(mapObject::remove);
        }
        return this;
    }

    public ChainMap<K, V> forEach(final BiConsumer<? super K, ? super V> consumer) {
        if (!mapObject.isEmpty()) {
            mapObject.forEach(consumer);
        }
        return this;
    }

    public ChainMap<K, V> forEachKey(final Consumer<? super K> consumer) {
        if (!mapObject.isEmpty()) {
            mapObject.keySet().forEach(consumer);
        }
        return this;
    }

    public ChainMap<K, V> forEachValue(final Consumer<? super V> consumer) {
        if (!mapObject.isEmpty()) {
            mapObject.values().forEach(consumer);
        }
        return this;
    }

    public final Set<Map.Entry<K, V>> entrySet() {
        return mapObject.entrySet();
    }

    public final Set<K> keySet() {
        return mapObject.keySet();
    }

    public final Collection<V> values() {
        return mapObject.values();
    }

    public final List<V> valuesToList() {
        return new ArrayList<>(mapObject.values());
    }

    public Map<K, V> toMapObject() {
        return mapObject;
    }

    public String toJsonString() {
        return JSON.toJsonString(this.mapObject);
    }

    @Override
    public String toString() {
        return toJsonString();
    }
}
