package com.common;

import com.common.util.JSON;

/**
 * Json 格式化基础方法
 *
 * @author 谢长春 2017-9-26 .
 */
public interface IJson {

    /**
     * 当前类转换为Json字符串
     *
     * @return String
     */
    default String json() {
        return JSON.toJsonString(this);
    }

}
