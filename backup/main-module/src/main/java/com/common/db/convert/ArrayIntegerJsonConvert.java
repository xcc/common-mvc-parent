package com.common.db.convert;

import com.common.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

/**
 * 支撑 mysql 原生 JSON 数据类型与实体类属性的映射；
 * 需要在实体类属性上添加注解：@Convert(converter = {@link ArrayIntegerJsonConvert}.class)
 *
 * @author 谢长春 2019/2/12
 */
@Converter
public class ArrayIntegerJsonConvert implements AttributeConverter<Integer[], String> {
    @Override
    public String convertToDatabaseColumn(final Integer[] attribute) {
        if (Objects.isNull(attribute)) {
            return null;
        }
        if (attribute.length == 0) {
            return "[]";
        }
        return JSON.toJsonString(attribute);
    }

    @Override
    public Integer[] convertToEntityAttribute(final String dbData) {
        return JSON.parse(dbData, new TypeReference<Integer[]>() {
        });
    }
}
