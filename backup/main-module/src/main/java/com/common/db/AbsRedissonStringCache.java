package com.common.db;

import com.common.util.JSON;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBatch;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Redisson缓存实现此接口
 *
 * @author 谢长春 2022-06-14
 */
public abstract class AbsRedissonStringCache<K> implements ICache {

    /**
     * 缓存客户端
     *
     * @return {@link RedissonClient}
     */
    protected abstract RedissonClient getRedissonClient();

    /**
     * 缓存过期时间
     *
     * @return {@link Duration}
     */
    public abstract Duration getExpired();

    /**
     * null 值缓存过期时间， 默认 1 分钟
     *
     * @return {@link Duration}
     */
    protected Duration getNullValueExpired() {
        return NULL_VALUE_EXPIRED;
    }

    /**
     * 基于数据主键构造缓存key； 拼接前缀，一般用类名作为前缀:unionKey
     *
     * @param unionKey K
     * @return String this.getClass().getSimpleName() + ":" + unionKey
     */
    protected Optional<String> formatCacheKey(final K unionKey) {
        return Optional.ofNullable(unionKey)
                .map(Objects::toString)
                .filter(StringUtils::isNoneBlank)
                .map(val -> this.getClass().getSimpleName() + ":" + val)
                ;
    }

    /**
     * 前缀匹配表达式，匹配满足条件的所有缓存
     *
     * @return String
     */
    protected String getMatchPattern() {
        return this.getClass().getSimpleName() + ":*";
    }

    /**
     * 写缓存
     *
     * @param unionKey K
     * @param value    String
     */
    public void set(final K unionKey, final String value) {
        formatCacheKey(unionKey).ifPresent(cacheKey -> {
            final long expiredSeconds = getExpired().getSeconds();
            final long nullValueExpiredSeconds = getNullValueExpired().getSeconds();
            final RBucket<String> bucket = getRedissonClient().getBucket(cacheKey, StringCodec.INSTANCE);
            if (Strings.isNullOrEmpty(value)) {
                // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
                bucket.set("", nullValueExpiredSeconds, TimeUnit.SECONDS);
            } else {
                // 数据库查询结果写入缓存
                bucket.set(value, expiredSeconds, TimeUnit.SECONDS);
            }
        });
    }

    /**
     * 写缓存
     *
     * @param unionKey String
     * @param value    Object
     */
    public void set(final K unionKey, final Object value) {
        if (Objects.isNull(value)) {
            set(unionKey, "");
        } else {
            set(unionKey, JSON.toJsonString(value));
        }
    }

    /**
     * 批量写缓存
     *
     * @param map Map<String, String>
     */
    public void set(final Map<K, String> map) {
        if (map.isEmpty()) {
            return;
        }
        final RedissonClient redissonClient = getRedissonClient();
        final long expiredSeconds = getExpired().getSeconds();
        final long nullValueExpiredSeconds = getNullValueExpired().getSeconds();
        final RBatch batch = redissonClient.createBatch();
        map.forEach((unionKey, value) -> {
            formatCacheKey(unionKey).ifPresent(cacheKey -> {
                if (Strings.isNullOrEmpty(value)) {
                    // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
                    batch.getBucket(cacheKey, StringCodec.INSTANCE)
                            .setAsync("", nullValueExpiredSeconds, TimeUnit.SECONDS);
                } else {
                    // 数据库查询结果写入缓存
                    batch.getBucket(cacheKey, StringCodec.INSTANCE)
                            .setAsync(value, expiredSeconds, TimeUnit.SECONDS);
                }
            });
        });
        batch.execute();
    }

    /**
     * 按 unionKey 获取数据缓存
     *
     * @param unionKey String
     * @return Optional<String>
     */

    public Optional<String> get(final K unionKey) {
        return formatCacheKey(unionKey)
                .map(cacheKey -> getRedissonClient().<String>getBucket(cacheKey, StringCodec.INSTANCE).get())
                .filter(StringUtils::isNoneBlank);
    }

    /**
     * 删除缓存
     *
     * @param unionKey String
     */
    protected void delete(final K unionKey) {
        formatCacheKey(unionKey).ifPresent(cacheKey -> getRedissonClient().getBucket(cacheKey).deleteAsync());
    }

    /**
     * 批量删除缓存
     *
     * @param unionKeys Collection<String>
     */
    protected void delete(final Collection<K> unionKeys) {
        final String[] cacheKeys = unionKeys.stream()
                .map(this::formatCacheKey)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toArray(String[]::new);
        if (cacheKeys.length > 0) {
            getRedissonClient().getKeys().deleteAsync(cacheKeys);
        }
    }

    /**
     * 按前缀删除缓存
     */
    public void clear() {
        final String pattern = getMatchPattern();
        getRedissonClient()
                .getKeys()
                .deleteByPattern(pattern);
    }

    /**
     * 按前缀遍历缓存key， 500条返回一次
     *
     * @param consumer Consumer<List<String>>
     */
    protected void forEachKeys(final Consumer<List<String>> consumer) {
        final String pattern = getMatchPattern();
        final RedissonClient redissonClient = getRedissonClient();
        final List<String> cacheKeys = Lists.newArrayList();
        redissonClient
                .getKeys()
                .getKeysStreamByPattern(pattern)
                .forEach(cacheKey -> {
                    if (cacheKeys.size() == 500) {
                        consumer.accept(cacheKeys);
                        cacheKeys.clear();
                    } else {
                        cacheKeys.add(cacheKey);
                    }
                });
        if (!cacheKeys.isEmpty()) {
            consumer.accept(cacheKeys);
            cacheKeys.clear();
        }
    }

    /**
     * 按前缀遍历缓存数据， 500条返回一次
     *
     * @param consumer Consumer<List<String>>
     */
    protected void forEachValues(final Consumer<List<String>> consumer) {
        final String pattern = getMatchPattern();
        final RedissonClient redissonClient = getRedissonClient();
        final List<String> cacheKeys = Lists.newArrayList();
        redissonClient
                .getKeys()
                .getKeysStreamByPattern(pattern)
                .forEach(cacheKey -> {
                    if (cacheKeys.size() == 500) {
                        final RBatch batch = redissonClient.createBatch();
                        cacheKeys.forEach(ck -> batch.getBucket(ck, StringCodec.INSTANCE).getAsync());
                        cacheKeys.clear();
                        final List<?> responses = batch.execute().getResponses();
                        final List<String> list = responses.stream()
                                .filter(jsonText -> !Objects.equals("", jsonText)) // 规避缓存穿透，会有空字符串写入缓存
                                .map(Objects::toString)
                                .collect(Collectors.toList());
                        if (!list.isEmpty()) {
                            consumer.accept(list);
                        }
                    } else {
                        cacheKeys.add(cacheKey);
                    }
                });
        if (cacheKeys.isEmpty()) {
            final RBatch batch = redissonClient.createBatch();
            cacheKeys.forEach(ck -> batch.getBucket(ck, StringCodec.INSTANCE).getAsync());
            cacheKeys.clear();
            final List<?> responses = batch.execute().getResponses();
            final List<String> list = responses.stream()
                    .filter(jsonText -> !Objects.equals("", jsonText)) // 规避缓存穿透，会有空字符串写入缓存
                    .map(Objects::toString)
                    .collect(Collectors.toList());
            if (!list.isEmpty()) {
                consumer.accept(list);
            }
        }
    }

}
