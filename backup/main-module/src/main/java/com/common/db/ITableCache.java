package com.common.db;

import com.common.util.ChainMap;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.redisson.api.RBatch;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 所有数据库表缓存必须实现此接口
 *
 * @author 谢长春 2022-06-14
 */
public interface ITableCache<K, V> extends ICache {

    interface CacheRepository<K, V> {
        /**
         * 这个方法仅用于同步数据库数据到 redis ， 注意：这个方法查的是数据库
         * 缓存查询，过滤无效值；
         *
         * @param ids Collection<K> 数据主键id
         * @return Map<String, TabRole>
         */
        Map<K, V> mapByIds(final Collection<K> ids);
    }

    /**
     * 缓存客户端
     *
     * @return {@link RedissonClient}
     */
    RedissonClient getRedissonClient();

    /**
     * 缓存表 数据库 查询接口
     *
     * @return {@link CacheRepository}
     */
    CacheRepository<K, V> getCacheRepository();

    /**
     * 缓存实体类对象
     *
     * @return {@link Class}
     */
    Class<V> getEntityClazz();

    /**
     * 缓存 key 构造
     *
     * @return {@link String}
     */
    String cacheKey(final K id);

    /**
     * 缓存过期时间
     *
     * @return {@link Duration}
     */
    Duration getExpired();

//
//    /**
//     * 从数据库查询缓存中没有的数据
//     *
//     * @param dbIds Set<K>
//     * @return Map<K, V>
//     */
//    Map<K, V> findDBRows(final Set<K> dbIds);

    /**
     * null 值缓存过期时间， 默认 1 分钟
     *
     * @return {@link Duration}
     */
    default Duration getNullValueExpired() {
        return NULL_VALUE_EXPIRED;
    }

    /**
     * 写缓存
     *
     * @param id    K
     * @param value V
     */
    default void set(final K id, final V value) {
        final String key = Objects.toString(id);
        if (Objects.equals("0", key)
                || key.startsWith("-") // 负数
                || Strings.isNullOrEmpty(key) // 空字符穿
                || Objects.equals("null", key)

        ) {
            return;
        }
        final RedissonClient redissonClient = getRedissonClient();
        final long expiredSeconds = getExpired().getSeconds();
        final long nullValueExpiredSeconds = getNullValueExpired().getSeconds();
        final RBucket<String> bucket = redissonClient.getBucket(cacheKey(id), StringCodec.INSTANCE);

        if (Objects.equals("0", key)
                || key.startsWith("-") // 负数
                || Strings.isNullOrEmpty(key) // 空字符穿
                || Objects.equals("null", key)

        ) {
            // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
            bucket.set("", nullValueExpiredSeconds, TimeUnit.SECONDS);
        } else {
            // 数据库查询结果写入缓存
            bucket.set(JsonCache.toJsonString(value), expiredSeconds, TimeUnit.SECONDS);
        }
    }

    /**
     * 批量写缓存
     *
     * @param map Map<K,V>
     */
    default void set(final Map<K, V> map) {
        if (map.isEmpty()) {
            return;
        }
        final RedissonClient redissonClient = getRedissonClient();
        final long expiredSeconds = getExpired().getSeconds();
        final long nullValueExpiredSeconds = getNullValueExpired().getSeconds();
        final RBatch batch = redissonClient.createBatch();
        map.forEach((id, value) -> {
            final String key = Objects.toString(id);
            if (Objects.equals("0", key)
                    || key.startsWith("-") // 负数
                    || Strings.isNullOrEmpty(key) // 空字符穿
                    || Objects.equals("null", key)

            ) {
                // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
                batch.getBucket(cacheKey(id), StringCodec.INSTANCE)
                        .setAsync("", nullValueExpiredSeconds, TimeUnit.SECONDS);
            } else {
                // 数据库查询结果写入缓存
                batch.getBucket(cacheKey(id), StringCodec.INSTANCE)
                        .setAsync(JsonCache.toJsonString(value), expiredSeconds, TimeUnit.SECONDS);
            }
        });
        batch.execute();
    }

    /**
     * 按 id 获取数据缓存
     *
     * @param id K
     * @return Optional<TabUser>
     */

    default Optional<V> get(final K id) {
        final String key = Objects.toString(id);
        if (Objects.equals("0", key)
                || key.startsWith("-") // 负数
                || Strings.isNullOrEmpty(key) // 空字符穿
                || Objects.equals("null", key)

        ) {
            return Optional.empty();
        }
        final RedissonClient redissonClient = getRedissonClient();
        final Class<V> clazz = getEntityClazz();
        final CacheRepository<K, V> cacheRepository = getCacheRepository();
        final long expiredSeconds = getExpired().getSeconds();
        final long nullValueExpiredSeconds = getNullValueExpired().getSeconds();
        final RBucket<String> bucket = redissonClient.getBucket(cacheKey(id), StringCodec.INSTANCE);

        final String jsonText = bucket.get();
        if (Objects.isNull(jsonText)) { // 无缓存
            final Map<K, V> map = cacheRepository.mapByIds(ImmutableSet.of(Objects.requireNonNull(id)));
            if (map.containsKey(id)) {
                // 数据库查询结果写入缓存
                bucket.set(JsonCache.toJsonString(map.get(id)), expiredSeconds, TimeUnit.SECONDS);
            } else {
                // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
                bucket.set("", nullValueExpiredSeconds, TimeUnit.SECONDS);
            }
            return Optional.ofNullable(map.get(id));
        } else if (Objects.equals("", jsonText.trim())) { // 规避缓存穿透，会有空字符串写入缓存
            return Optional.empty();
        }
        return Optional.ofNullable(JsonCache.parseObject(jsonText, clazz));
    }

    /**
     * 按 id 批量获取数据缓存
     *
     * @param ids Set<K>
     * @return Map<K, V>
     */

    @SuppressWarnings({"unchecked"})
    default Map<K, V> mapByIds(final Set<K> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        final List<K> cacheIds = ids.stream()
                .filter(Objects::nonNull)
                .filter(val -> { // 去掉无效的id
                    final String id = Objects.toString(val);
                    return !(Objects.equals("0", id)
                            || id.startsWith("-")
                            || Strings.isNullOrEmpty(id)
                    );
                })
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(cacheIds)) { // id集合为空时，直接返回
            return Collections.emptyMap();
        }
        final RedissonClient redissonClient = getRedissonClient();
        final Class<V> clazz = getEntityClazz();
        final CacheRepository<K, V> cacheRepository = getCacheRepository();
        final long expiredSeconds = getExpired().getSeconds();
        final long nullValueExpiredSeconds = getNullValueExpired().getSeconds();
        if (1 == cacheIds.size()) { // 只有一条数据，不用批处理
            final K id = cacheIds.get(0);
            final RBucket<String> bucket = redissonClient.getBucket(cacheKey(id), StringCodec.INSTANCE);
            final String jsonText = bucket.get();
            if (Objects.isNull(jsonText)) { // 无缓存
                final Map<K, V> map = cacheRepository.mapByIds(Sets.newHashSet(id));
                if (map.containsKey(id)) {
                    // 数据库查询结果写入缓存
                    bucket.set(JsonCache.toJsonString(map.get(id)), expiredSeconds, TimeUnit.SECONDS);
                } else {
                    // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
                    bucket.set("", nullValueExpiredSeconds, TimeUnit.SECONDS);
                }
                return map;
            } else if (Objects.equals("", jsonText.trim())) { // 规避缓存穿透，会有空字符串写入缓存
                return Collections.emptyMap();
            }
            return ChainMap.<K, V>create() // 有缓存
                    .put(id, JsonCache.parseObject(jsonText, clazz))
                    .toMapObject();
        }
        final Set<K> dbIds = Sets.newHashSet(); // 记录缓存中不存在的数据id回数据库查询
        final Map<K, V> resultMap = Maps.newHashMap(); // 最终返回的数据
        { // redis 缓存查询
            final RBatch batch = redissonClient.createBatch();
            // 异步获取缓存
            cacheIds.forEach(id -> batch.getBucket(cacheKey(id), StringCodec.INSTANCE).getAsync());
            final List<String> responses = (List<String>) batch.execute().getResponses();

            for (int i = 0; i < responses.size(); i++) {
                final String jsonText = responses.get(i);
                if (Objects.isNull(jsonText)) { // 无缓存，回数据库查询
                    dbIds.add(cacheIds.get(i));
                } else if (Objects.equals("", jsonText.trim())) {
                    // 规避缓存穿透，会有空字符串写入缓存
                } else {
                    resultMap.put(cacheIds.get(i), JsonCache.parseObject(jsonText, clazz));
                }
            }
        }
        // redis 没有的数据， 从数据库查询并写入缓存
        if (!dbIds.isEmpty()) {
            final Map<K, V> rows = cacheRepository.mapByIds(dbIds);
            final RBatch batch = redissonClient.createBatch();
            dbIds.forEach(id -> {
                if (rows.containsKey(id)) {
                    batch.getBucket(cacheKey(id), StringCodec.INSTANCE)
                            // 数据库查询结果批量异步写入缓存
                            .setAsync(JsonCache.toJsonString(rows.get(id)), expiredSeconds, TimeUnit.SECONDS);
                } else {
                    batch.getBucket(cacheKey(id), StringCodec.INSTANCE)
                            // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
                            .setAsync("", nullValueExpiredSeconds, TimeUnit.SECONDS);
                }
            });
            batch.execute();
            resultMap.putAll(rows);
        }
        return resultMap;
    }

    /**
     * 按 id 批量获取数据缓存， 返回数据同传进来的数据保持一致
     *
     * @param ids Collection<K>
     * @return List<V>
     */

    default List<V> listByIds(final Collection<K> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        final Map<K, V> map = mapByIds(Sets.newHashSet(ids));
        return ids.stream().filter(map::containsKey).map(map::get).collect(Collectors.toList());
    }

    /**
     * 删除缓存
     *
     * @param id K
     */
    default void delete(final K id) {
        getRedissonClient().getBucket(cacheKey(id)).deleteAsync();
    }

    /**
     * 批量删除缓存
     *
     * @param ids Collection<K>
     */
    default void delete(final Collection<K> ids) {
        getRedissonClient()
                .getKeys()
                .deleteAsync(ids.stream().map(this::cacheKey).toArray(String[]::new));
//        final RBatch batch = getRedissonClient().createBatch();
//        ids.forEach(id -> batch.getBucket(cacheKey(id), StringCodec.INSTANCE).deleteAsync());
//        batch.execute();
    }

//    /**
//     * 按前缀计算缓存行数量
//     */
//    default long count() {
//        final String pattern = this.getClass().getSimpleName() + ":*"; // 表达式，匹配当前表所有缓存
//        return getRedissonClient()
//                .getKeys()
//                .getKeysStreamByPattern(pattern)
//                .count();
//    }

    /**
     * 按前缀删除缓存
     */
    default void clear() {
        final String pattern = this.getClass().getSimpleName() + ":*"; // 表达式，匹配当前表所有缓存
        getRedissonClient()
                .getKeys()
                .deleteByPattern(pattern);
    }

    /**
     * 按前缀遍历缓存key， 500条返回一次
     *
     * @param consumer Consumer<List<String>>
     */
    default void forEachKeys(final Consumer<List<String>> consumer) {
        final String pattern = this.getClass().getSimpleName() + ":*"; // 表达式，匹配当前表所有缓存
        final RedissonClient redissonClient = getRedissonClient();
        final List<String> cacheKeys = Lists.newArrayList();
        redissonClient
                .getKeys()
                .getKeysStreamByPattern(pattern)
                .forEach(cacheKey -> {
                    if (cacheKeys.size() == 500) {
                        consumer.accept(cacheKeys);
                        cacheKeys.clear();
                    } else {
                        cacheKeys.add(cacheKey);
                    }
                });
        if (!cacheKeys.isEmpty()) {
            consumer.accept(cacheKeys);
            cacheKeys.clear();
        }
    }

    /**
     * 按前缀遍历缓存数据， 500条返回一次
     *
     * @param consumer Consumer<List<V>>
     */
    default void forEachValues(final Consumer<List<V>> consumer) {
        final String pattern = this.getClass().getSimpleName() + ":*"; // 表达式，匹配当前表所有缓存
        final RedissonClient redissonClient = getRedissonClient();
        final List<String> cacheKeys = Lists.newArrayList();
        redissonClient
                .getKeys()
                .getKeysStreamByPattern(pattern)
                .forEach(cacheKey -> {
                    if (cacheKeys.size() == 500) {
                        final RBatch batch = redissonClient.createBatch();
                        cacheKeys.forEach(key -> batch.getBucket(key, StringCodec.INSTANCE).getAsync());
                        cacheKeys.clear();
                        final List<?> responses = batch.execute().getResponses();
                        final List<V> list = responses.stream()
                                .filter(jsonText -> !Objects.equals("", jsonText)) // 规避缓存穿透，会有空字符串写入缓存
                                .map(jsonText -> JsonCache.parseObject((String) jsonText, getEntityClazz()))
                                .collect(Collectors.toList());
                        if (!list.isEmpty()) {
                            consumer.accept(list);
                        }
                    } else {
                        cacheKeys.add(cacheKey);
                    }
                });
        if (cacheKeys.isEmpty()) {
            final RBatch batch = redissonClient.createBatch();
            cacheKeys.forEach(key -> batch.getBucket(key, StringCodec.INSTANCE).getAsync());
            cacheKeys.clear();
            final List<?> responses = batch.execute().getResponses();
            final List<V> list = responses.stream()
                    .filter(jsonText -> !Objects.equals("", jsonText)) // 规避缓存穿透，会有空字符串写入缓存
                    .map(jsonText -> JsonCache.parseObject((String) jsonText, getEntityClazz()))
                    .collect(Collectors.toList());
            if (!list.isEmpty()) {
                consumer.accept(list);
            }
        }
    }

}
