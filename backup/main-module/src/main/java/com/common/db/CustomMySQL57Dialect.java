package com.common.db;

import org.hibernate.dialect.MySQL57Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

/**
 * 注册自定义方言函数
 * {@link org.hibernate.engine.jdbc.dialect.spi.DialectResolver}
 * https://blog.csdn.net/qq_19671173/article/details/82909338
 *
 * <code>
 * Expressions.booleanTemplate("find_in_set({0},{1})>0", value, table.column)
 * Expressions.stringTemplate("group_concat({0})", table.column)
 * Expressions.stringTemplate("left_char({0},8)", table.column)
 * Expressions.stringTemplate("if_null({0},{1})", table.true_column, table.false_column)
 * Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.column, value)
 * Expressions.booleanTemplate("JSON_CONTAINS({0},JSON_ARRAY({1}))>0", table.column, value)
 * </code>
 *
 * @author 谢长春 2019-12-13
 */
public class CustomMySQL57Dialect extends MySQL57Dialect {
    public CustomMySQL57Dialect() {
        super();
        registerFunction("group_concat", new StandardSQLFunction("group_concat", StandardBasicTypes.STRING));
        registerFunction("convert_gbk", new SQLFunctionTemplate(StandardBasicTypes.STRING, "convert(?1 using gbk)"));
        registerFunction("left_char", new StandardSQLFunction("left", StandardBasicTypes.STRING));
//        registerFunction("left_char", new SQLFunctionTemplate(StandardBasicTypes.STRING, "left(?1, ?2)"));
        registerFunction("if_null", new StandardSQLFunction("ifnull"));
//        registerFunction("bitand", new SQLFunctionTemplate(IntegerType.INSTANCE, "(?1 & ?2)"));
//        registerFunction("bitor", new SQLFunctionTemplate(IntegerType.INSTANCE, "(?1 | ?2)"));
//        registerFunction("bitxor", new SQLFunctionTemplate(IntegerType.INSTANCE, "(?1 ^ ?2)"));
//        registerFunction("if", new StandardSQLFunction("if"));
//        registerFunction("countif", new SQLFunctionTemplate(StandardBasicTypes.LONG, "count(if(?1, ?2, ?3))"));
//        registerFunction("sumif", new StandardSQLFunction("ifnull"));
//        registerFunction("sumifnull", new StandardSQLFunction("ifnull"));
    }
}
