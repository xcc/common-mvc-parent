package com.common.db;

import com.common.IJson;

import java.io.Serializable;

/**
 * 所有数据库的视图必须实现此接口
 *
 * @author 谢长春 2017-9-26
 */
public interface IView extends Serializable, IJson {
}
