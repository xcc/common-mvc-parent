package com.common.db;

import com.ccx.demo.config.encrypt.annotations.AesIdJsonConverter;
import com.common.util.ChainMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 所有缓存相关必须实现此接口
 *
 * @author 谢长春 2019-12-18
 */
public interface ICache {

    /**
     * 空值缓存过期时间
     */
    Duration NULL_VALUE_EXPIRED = Duration.ofMinutes(1);

    /**
     * 封装 jackson 操作静态方法
     *
     * @author 谢长春 2022-07-05
     */
    @Slf4j
    final class JsonCache {
        private JsonCache() {
        }

        private static final ObjectMapper objectMapper;
        private static final SimpleFilterProvider simpleFilterProvider;

        static {
            simpleFilterProvider = new SimpleFilterProvider();
            objectMapper = new ObjectMapper()
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) // 反序列化忽略 java 类中不存在的字段
                    .enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES) // 反序列化 null 值忽略
                    .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS) // bean序列化为空时不会异常失败
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL) // 序列化不返回 null 值
                    .setAnnotationIntrospector(new JacksonAnnotationIntrospector() {
                        @Override
                        public boolean isAnnotationBundle(Annotation ann) {
                            final Class<? extends Annotation> type = ann.annotationType();
                            if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
                                return false;
                            }
                            return super.isAnnotationBundle(ann);
                        }
                    })
            ;
        }


        public static ObjectMapper get() {
            return objectMapper;
        }

        /**
         * 注册属性过滤器
         * SimpleBeanPropertyFilter.serializeAllExcept() // 序列化排除字段
         * SimpleBeanPropertyFilter.filterOutAllExcept() // 序列化包含字段
         * SimpleBeanPropertyFilter.serializeAll() // 默认规则
         *
         * @param id     String 过滤器名称，全局唯一
         * @param filter SimpleBeanPropertyFilter
         */
        public static void addFilter(final String id, final SimpleBeanPropertyFilter filter) {
            simpleFilterProvider.addFilter(id, filter);
            objectMapper.setFilterProvider(simpleFilterProvider);
        }

        @SneakyThrows
        public static String toJsonString(final Object value) {
            return objectMapper.writeValueAsString(value);
        }

        @SneakyThrows
        public static <V> ChainMap<String, V> parseObject(final String jsonText) {
            if (StringUtils.isBlank(jsonText)) {
                return null;
            }
            final LinkedHashMap<String, V> map = objectMapper.readValue(jsonText, new TypeReference<LinkedHashMap<String, V>>() {
            });
            return ChainMap.create(map);
        }

        @SneakyThrows
        public static <T> T parseObject(final String jsonText, Class<T> clazz) {
            if (StringUtils.isBlank(jsonText)) {
                return null;
            }
            return objectMapper.readValue(jsonText, clazz);
        }

        @SneakyThrows
        public static <T> T parseObject(final String jsonText, TypeReference<T> typeReference) {
            if (StringUtils.isBlank(jsonText)) {
                return null;
            }
            return objectMapper.readValue(jsonText, typeReference);
        }

        @SneakyThrows
        public static <T> List<T> parseArray(final String jsonText) {
            if (StringUtils.isBlank(jsonText)) {
                return null;
            }
            return objectMapper.readValue(jsonText, new TypeReference<List<T>>() {
            });
        }

    }

}
