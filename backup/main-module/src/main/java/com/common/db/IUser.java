package com.common.db;

import com.ccx.demo.business.user.vo.LoginUserVO;
import com.ccx.demo.open.auth.dto.Token;
import com.common.ICall;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;
import java.util.Optional;

/**
 * 用户信息抽象接口， 用于 Controller 层泛型获取用户信息
 *
 * @author 谢长春
 */
public interface IUser {
    /**
     * 客户端指纹
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getFingerprint() {
        return null;
    }

    /**
     * 请求头 x-token
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getToken() {
        return null;
    }

    /**
     * 用户id
     *
     * @return {@link Long}
     */
    Long getId();

    void setId(final Long id);

    /**
     * 用户登录名
     *
     * @return {@link String}
     */
    String getUsername();

    void setUsername(final String username);

    /**
     * 用户昵称
     *
     * @return {@link String}
     */
    String getNickname();

    void setNickname(final String nickname);

    /**
     * 用户手机号
     *
     * @return {@link String}
     */
    String getPhone();

    void setPhone(final String phone);

    /**
     * 判断用户id是否有效，有效则执行 call 方法
     *
     * @param call {@link ICall}
     * @return boolean true: call 方法已执行
     */
    default boolean ifUserId(final ICall call) {
        if (Objects.nonNull(getId()) && getId() > 0) {
            call.call();
            return true;
        }
        return false;
    }

    /**
     * 判断用户id是否有效，有效则执行 call 方法
     *
     * @return boolean true: call 方法已执行
     */
    default Optional<Long> optionalTokenUserId() {
        if (Objects.nonNull(getId()) && getId() > 0) {
            return Optional.ofNullable(getId());
        }
        parseToken();
        return Optional.ofNullable(getId()).filter(id -> id > 0);
    }

    /**
     * 用于开放接口用 token 换用户信息
     * 判断 token 不为空， 且没有过期，能正常换到用户信息，则执行 call 方法
     *
     * @param call {@link ICall}
     * @return boolean true: call 方法已执行
     */
    default boolean ifToken(final ICall call) {
        if (Objects.nonNull(getId()) && getId() > 0) {
            call.call();
            return true;
        }
        return parseToken();
    }

    default boolean parseToken() {
        if (StringUtils.isNoneBlank(getToken())) {
            try {
                // token 换用户信息
                final LoginUserVO loginUserVO = Token.auth(getToken()).getUser();
                setId(loginUserVO.getId());
                setUsername(loginUserVO.getUsername());
                setNickname(loginUserVO.getNickname());
                setPhone(loginUserVO.getPhone());
                return true;
            } catch (Exception e) {
                // 开放接口换不到用户信息也可以执行，所以这里不需要抛异常
            }
        }
        return false;
    }

    IUser EMPTY = new IUser() {
        @Override
        public Long getId() {
            return null;
        }

        @Override
        public void setId(Long id) {

        }

        @Override
        public String getUsername() {
            return null;
        }

        @Override
        public void setUsername(String username) {

        }

        @Override
        public String getNickname() {
            return null;
        }

        @Override
        public void setNickname(String nickname) {

        }

        @Override
        public String getPhone() {
            return null;
        }

        @Override
        public void setPhone(String phone) {

        }

        @Override
        public String toString() {
            return null;
        }
    };

    @Getter
    @Setter
    @ToString(exclude = {"anonymousKey"})
    @RequiredArgsConstructor
    class OpenUser implements IUser {
        private final String anonymousKey;
        private final String token;
        private Long id;
        private String username;
        private String nickname;
        private String phone;

        /**
         * OpenUser 转换为 匿名用户对象
         *
         * @return AnonymousAuthenticationToken
         */
        public Authentication toAnonymous() {
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (Objects.nonNull(authentication)) {
                if ((authentication.getPrincipal() instanceof IUser)) {
                    // 已登录用户，不需要处理
                    return authentication;
                    // } else if ((authentication.getPrincipal() instanceof IOpenUser.OpenUser)) {
                    // // 已生成游客对象
                    // return;
                }
            }
            // 生成默认的游客对象， controller 有需求时使用 token 换用户信息，如果 token 为空则表示用户未登录
            // 如果 token 过期，填充用户时捕获异常，避免接口报错，因为开放接口允许任何用户访问
            return new AnonymousAuthenticationToken(
                    anonymousKey,
                    this,
                    // 开放接口强制使用游客权限，避免开放接口注入权限，导致越权访问
                    AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS")
            );
        }
    }
}
