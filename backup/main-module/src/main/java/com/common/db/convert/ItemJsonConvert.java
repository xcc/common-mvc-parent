package com.common.db.convert;

import com.common.entity.Item;
import com.common.util.JSON;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

/**
 * 支撑 mysql 原生 JSON 数据类型与实体类属性的映射；
 * 需要在实体类属性上添加注解：@Convert(converter = {@link ItemJsonConvert}.class)
 *
 * @author 谢长春 2019/2/12
 */
@Converter
public class ItemJsonConvert implements AttributeConverter<Item, String> {
    @Override
    public String convertToDatabaseColumn(final Item attribute) {
        if (Objects.isNull(attribute)) {
            return null;
        }
        return JSON.toJsonString(attribute);
    }

    @Override
    public Item convertToEntityAttribute(final String dbData) {
        return JSON.parseObject(dbData, Item.class);
    }
}
