package com.common.db;

import com.common.IJson;

import java.io.Serializable;

/**
 * 所有mongodb数据库的实体类必须实现此接口
 *
 * @author 谢长春 2017-9-26
 */
public interface IMongo extends Serializable, IJson {
}
