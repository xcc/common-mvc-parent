package com.common.enums;

/**
 * 应用运行环境
 *
 * @author 谢长春 2022-05-20
 */
public enum AppEnv {
    local("本地开发环境"),
    dev("远程开发环境"),
    sit("系统测试"),
    uat("验收测试"),
    prod("生产"),
    ;
    /**
     * 枚举属性说明
     */
    public final String comment;

    AppEnv(String comment) {
        this.comment = comment;
    }
}
