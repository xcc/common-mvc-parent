package com.ccx.demo.open.auth.controller;

import com.ccx.demo.aop.AppEventRequest;
import com.ccx.demo.business.common.service.FileService;
import com.ccx.demo.business.user.service.UserService;
import com.ccx.demo.business.user.vo.LoginUserVO;
import com.ccx.demo.config.init.AppConfig;
import com.ccx.demo.enums.Role;
import com.ccx.demo.enums.TokenClient;
import com.ccx.demo.feign.SmsService;
import com.ccx.demo.open.auth.cache.PhoneCodeCache;
import com.ccx.demo.open.auth.dto.UsernamePasswordLoginDTO;
import com.ccx.demo.open.auth.service.AuthService;
import com.common.annotations.AppSwaggerGroup;
import com.common.annotations.VueSwaggerGroup;
import com.common.annotations.WechatSwaggerGroup;
import com.common.db.entity.Result;
import com.common.enums.Code;
import com.common.util.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * 操作请求处理：授权
 *
 * @author 谢长春
 */
@Api(tags = "登录")
@ApiSort(1)
@RequestMapping("/open/auth")
@Controller
@Slf4j
@RequiredArgsConstructor
public class OpenAuthController {

    private final AppConfig appConfig;
    private final AuthService authService;
    private final UserService userService;
    private final ExecutorService multiThreadExecutorService;
    private final FileService fileService;
    private final SmsService smsService;
    private final PhoneCodeCache phoneCodeCache;

    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "账号密码登录，返回token"
            , tags = {""}
            , notes = "用户名密码登录，返回token"
            + "<br>密码需要RSA加密，RSA公钥由后端提供，注意不同环境公钥不一样"
            + "<br>client 对应的枚举值参考【TokenClient】： #/全部接口/开发环境支撑/enumsUsingGET_2"
            + "<br>登录成功之后 x-token 写入本地缓存，发请求时 x-token 写入请求头； "
            + "<br>登录成功之后并不会返回用户信息，需要在请求头携带 x-token 调用 /user/current 接口获取用户信息"
            + "<br>任意接口响应【401】表示token过期，需跳转登录页重新获取token"
            + "<br>同一个账号在一个客户端只能有一个 token"
    )
    @PostMapping("/login/{client}")
    @ApiOperationSupport(order = 1, author = "谢长春", includeParameters = {"body.username", "body.password"})
    @ResponseBody
    public Result<String> login(
            @ApiParam(required = true, value = "登录客户端", example = "OM_PC") @PathVariable final TokenClient client,
            @RequestBody final UsernamePasswordLoginDTO body,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        return new Result<String>().execute(result -> {
            Code.A00001.assertNonBlank(body.getUsername(), "用户名不能为空");
            Code.A00001.assertNonBlank(body.getPassword(), "密码不能为空");
            // 登录成功之后，生成 token 回传到前端
            final LoginUserVO user = authService.login(body.getUsername(), body.getPassword());
            final List<String> authorityList = user.getAuthorityList();
            if (CollectionUtils.isEmpty(authorityList)) {
                throw Code.A00010.toCodeException("%s：authorityList未授权", body.getUsername());
            }
            if (Arrays.stream(Role.values()).anyMatch(ele -> authorityList.contains(ele.name()))) {
                // 硬编码权限可以登录
            } else if (!authorityList.contains(client.name())) {
                log.error("{}=>{}", client, JSON.toJsonString(authorityList));
                throw Code.A00010.toCodeException(body.getUsername());
            }
            // 生成token 放在响应头
            final String token = user.token(client);
            response.setHeader("x-token", token);
            result.setSuccess(token);
        });
    }

    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "2.1手机验证码登录：发送验证码"
            , tags = {""}
            , notes = "开发环境会关闭验证码发送开关（不发送验证码）"
            + "<br>"
    )
    @PostMapping("/sms-login-code/{phone}")
    @ApiOperationSupport(order = 2, author = "谢长春")
    @ResponseBody
    public Result<String> smsLoginCode(
            @ApiParam(required = true, value = "手机号", example = "18888888888") @PathVariable final String phone
    ) {
        return new Result<String>().call(() -> {
            Code.A00002.assertNonBlank(phone, "手机号必填");
            final String code = phoneCodeCache.get(phone);
            final Map<String, String> mdc = MDC.getCopyOfContextMap(); // 复制 MDC 上下文
            multiThreadExecutorService.execute(() -> {
                try {
                    MDC.setContextMap(mdc); // 设置上下文，保持链路追踪
                    smsService.sendPhoneCode(phone, code);
                } finally {
                    MDC.clear(); // 清理 MDC， 否则会导致内存泄漏
                }
            });
        });
    }

    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "2.2手机验证码登录：登录认证"
            , tags = {""}
            , notes = "开发环境验证码随便输入"
            + "<br>client 对应的枚举值参考【TokenClient】： #/全部接口/开发环境支撑/enumsUsingGET_2"
            + "<br>登录成功之后 x-token 写入本地缓存，发请求时 x-token 写入请求头； "
            + "<br>登录成功之后并不会返回用户信息，需要在请求头携带 x-token 调用 /user/current 接口获取用户信息"
            + "<br>任意接口响应【401】表示token过期，需跳转登录页重新获取token"
            + "<br>同一个账号在一个客户端只能有一个 token"
    )
    @PostMapping("/login/{client}/{phone}/{code}")
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<String> smsLogin(
            @ApiParam(required = true, value = "登录客户端", example = "OM_PC") @PathVariable final TokenClient client,
            @ApiParam(required = true, value = "手机号", example = "18717942600") @PathVariable final String phone,
            @ApiParam(required = true, value = "验证码", example = "0000") @PathVariable final String code,
            HttpServletResponse response
    ) {
        return new Result<String>().execute(result -> {
            Code.A00002.assertNonBlank(phone, "手机号必填");
            Code.A00002.assertNonBlank(code, "验证码必填");
            phoneCodeCache.asserts(phone, code);
            // 验证码验证成功之后，生成 token 回传到前端
            // final LoginUserVO user = userService.findLoginUserVO(phone)
            //         .orElseThrow(() -> AppCode.A01016.toCodeException("手机号未绑定用户"));
            final LoginUserVO user = userService.findAndInsertAppUser(
                    phone
                    , insertUserDTO -> {
                        log.info("新增用户时追加默认信息： {}", insertUserDTO);
                        return insertUserDTO;
                    }
                    , userId -> {
                        log.info("注册用户id： {}", userId);
                    }
            );
            final List<String> authorityList = user.getAuthorityList();
            if (CollectionUtils.isEmpty(authorityList)) {
                throw Code.A00010.toCodeException("%s：authorityList未授权", phone);
            }
            if (Arrays.stream(Role.values()).anyMatch(ele -> authorityList.contains(ele.name()))) {
                // 硬编码权限可以登录
            } else if (!authorityList.contains(client.name())) {
                log.error("{}=>{}", client, JSON.toJsonString(authorityList));
                throw Code.A00010.toCodeException(phone);
            }
            // 生成token 放在响应头
            final String token = user.token(client);
            response.setHeader("x-token", token);
            result.setSuccess(token);
        });
    }

    @AppSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "4.1账号销毁：发送手机验证码"
            , tags = {""}
            , notes = "2022-07-04: 开发环境不会发送验证码"
            + "<br>"
    )
    @PostMapping("/sms-clear-user/{phone}")
    @ApiOperationSupport(order = 11, author = "谢长春")
    @ResponseBody
    public Result<String> smsClearUser(
            @ApiParam(required = true, value = "手机号", example = "18717942600") @PathVariable final String phone
    ) {
        return new Result<String>().call(() -> {
            Code.A00002.assertNonBlank(phone, "手机号必填");
            final String code = phoneCodeCache.get(phone);
            final Map<String, String> mdc = MDC.getCopyOfContextMap(); // 复制 MDC 上下文
            multiThreadExecutorService.execute(() -> {
                try {
                    MDC.setContextMap(mdc); // 设置上下文，保持链路追踪
                    smsService.sendPhoneCode(phone, code);
                } finally {
                    MDC.clear(); // 清理 MDC， 否则会导致内存泄漏
                }
            });
        });
    }

    @AppSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "4.2账号销毁"
            , tags = {""}
            , notes = "账号销毁之后可以用手机号重新注册帐号"
            + "<br>"
    )
    @PostMapping("/sms-clear-user/{phone}/{code}")
    @ApiOperationSupport(order = 12, author = "谢长春")
    @ResponseBody
    public Result<Void> smsClearUser(
            @ApiParam(required = true, value = "手机号", example = "18717942600") @PathVariable final String phone,
            @ApiParam(required = true, value = "验证码", example = "0000") @PathVariable final String code
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonBlank(phone, "手机号必填");
            Code.A00002.assertNonBlank(code, "验证码必填");
            phoneCodeCache.asserts(phone, code);
            userService.clear(
                    phone
                    , Collections.singletonList((u) -> log.debug("执行账号销毁关联的其他逻辑"))
            );
        });
    }
//    @AppEventRequest
//    @AppSwaggerGroup
//    @ApiOperation(value = "3.1移动应用微信第三方登录集成：第 1 步"
//            , tags = {""}
//            , notes = "app 调用微信 sdk 获取 code 之后，通过这个接口进行认证"
//            + "<br> 移动应用微信登录开发指南： https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_Login/Development_Guide.html"
//            + "<br> iOS接入指南： https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Access_Guide/iOS.html"
//            + "<br> Android接入指南： https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Access_Guide/Android.html"
//            + "<br> 网站应用微信登录开发指南： https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Wechat_Login.html"
//            + "<br> + 微信认证接口"
//            + "<br> + openId+unionId 查询绑定记录"
//            + "<br> + 无绑定记录，返回 B00001 走 第2步"
//            + "<br> + 检查账号状态，禁用时抛出异常，不能登录"
//            + "<br> + 是否有权限使用登录客户端"
//            + "<br> + 检查必须有权限登录使用对应的客户端，否则抛异常消息"
//            + "<br> + 生成 x-token 写入 http 响应头"
//            + "<br> ------"
//            + "<br> + client 对应的枚举值参考【TokenClient】： #/全部接口/开发环境支撑/enumsUsingGET_2"
//            + "<br> + 登录成功之后 x-token 写入本地缓存，发请求时 x-token 写入请求头； "
//            + "<br> + 登录成功之后并不会返回用户信息，需要在请求头携带 x-token 调用 /user/current 接口获取用户信息"
//            + "<br> + 任意接口响应【401】表示token过期，需跳转登录页重新获取token"
//            + "<br> + 同一个账号在一个客户端只能有一个 token"
//            + "<br> ------"
//            + "<br> + 响应码：B00001 = 未绑定微信，走第2步【3.2移动应用微信第三方登录集成】跳转页面填手机号和验证码进行账号绑定"
//    )
//    @PostMapping("/wechat/oauth2/{client}/{wxCode}")
//    @ApiOperationSupport(order = 4, author = "谢长春")
//    @ResponseBody
//    public Result<String> wechatOAuth2(
//            @ApiParam(required = true, value = "登录客户端", example = "WECHAT") @PathVariable final TokenClient client,
//            @ApiParam(required = true, value = "微信code", example = "0000") @PathVariable final String wxCode,
//            HttpServletResponse response
//    ) {
//        return new Result<String>().execute(result -> {
//            final AppWechatUserInfoVO appWechatUserInfoVO = wechatService.oauth2(wxCode);
//
//            final Optional<TabUserWechat> userWechatOptional = Optional.ofNullable(userWechatService
//                    // 查询微信返回的 openId 是否已经绑定过用户，已经绑定。 生成 token 返回登录成功
//                    .findById(appWechatUserInfoVO.buildUserWechatId()) // md5(appId+openId)
//                    .orElseGet(() -> userWechatService
//                            // 查询微信返回的 unionId 是否已经绑定过用户，已经绑定。 生成 token 返回登录成功
//                            .findByUnionId(appWechatUserInfoVO.getUnionid())
//                            .orElse(null)
//                    )
//            );
//            if (userWechatOptional.isPresent()) {
//                final TabUserWechat tabUserWechat = userWechatOptional.get();
//                // 验证码验证成功之后，生成 token 回传到前端
//                final LoginUserVO userVO = userService.findById(tabUserWechat.getUserId())
//                        .orElseThrow(() -> Code.B00001.toCodeException("微信未绑定用户:%s:%s", appWechatUserInfoVO.getAppId(), appWechatUserInfoVO.getOpenid()))
//                        .toTabUserVO();
//                final List<String> authorityList = userVO.getAuthorityList();
//    if (CollectionUtils.isEmpty(authorityList)) {
//        throw Code.A00010.toCodeException("%s：authorityList未授权",userId);
//    }
//                if (Arrays.stream(Role.values()).anyMatch(ele -> authorityList.contains(ele.name()))) {
//                    // 硬编码权限可以登录
//                } else if (!authorityList.contains(client.name())) {
//                    log.error("{}=>{}", client, JSON.toJsonString(authorityList));
//                    throw AppCode.A01021.toCodeException(userVO.getPhone());
//                }
//                Code.A00002.assertHasTrue(userVO.isAllow(), "用户账号已禁止登录");
//                // 生成token 放在响应头
//                final String token = userVO.token(client);
//                response.setHeader("x-token", token);
//                result.setSuccess(token);
//            } else {
//                // 返回 B00001 响应码， 到第2步【3.2移动应用微信第三方登录集成】
//                result.setCode(Code.B00001);
//            }
//        });
//    }
//
//    @AppSwaggerGroup
//    @AppEventRequest
//    @ApiOperation(value = "3.2移动应用微信第三方登录集成：第 2 步"
//            , tags = {""}
//            , notes = "通过微信 code 绑定手机号，完成登录认证"
//            + "<br> + 验证手机短信验证码，开发环境验证码随便输入"
//            + "<br> + 获取第1步【3.1移动应用微信第三方登录集成】微信认证缓存，有效期2小时"
//            + "<br> + 检查账号状态，禁用时抛出异常，不能登录"
//            + "<br> + 检查必须有权限登录使用对应的客户端，否则抛异常消息"
//            + "<br> + 写入微信账号绑定信息"
//            + "<br> + 生成 x-token 写入 http 响应头"
//            + "<br> ------"
//            + "<br> + client 对应的枚举值参考【TokenClient】： #/全部接口/开发环境支撑/enumsUsingGET_2"
//            + "<br> + 登录成功之后 x-token 写入本地缓存，发请求时 x-token 写入请求头； "
//            + "<br> + 登录成功之后并不会返回用户信息，需要在请求头携带 x-token 调用 /user/current 接口获取用户信息"
//            + "<br> + 任意接口响应【401】表示token过期，需跳转登录页重新获取token"
//            + "<br> + 同一个账号在一个客户端只能有一个 token"
//    )
//    @PostMapping("/wechat/oauth2/bind/{client}/{wxCode}/{phone}/{smsCode}")
//    @ApiOperationSupport(order = 5, author = "谢长春")
//    @ResponseBody
//    public Result<String> wechatOAuth2BindPhone(
//            @ApiParam(required = true, value = "登录客户端", example = "WECHAT") @PathVariable final TokenClient client,
//            @ApiParam(required = true, value = "微信code", example = "0000") @PathVariable final String wxCode,
//            @ApiParam(required = true, value = "手机号", example = "18717942600") @PathVariable final String phone,
//            @ApiParam(required = true, value = "短信验证码", example = "0000") @PathVariable final String smsCode,
//            HttpServletResponse response
//    ) {
//        return new Result<String>().execute(result -> {
//            Code.A00002.assertNonBlank(phone, "手机号必填");
//            Code.A00002.assertNonBlank(smsCode, "验证码必填");
//            Code.A00003.assertNonBlank(wxCode, "微信code必填");
//            if (appConfig.isVerifyCodeEnabled()) {
//                final RMap<String, Object> loginPhoneCodeBucket = redissonClient.getMap(RedisKey.login_phone_code.key(phone));
//                Code.A00002.assertHasTrue(loginPhoneCodeBucket.isExists(), "验证码已过期");
//                if (Objects.equals(smsCode, loginPhoneCodeBucket.get("code"))) {
//                    loginPhoneCodeBucket.delete(); // 验证码校验成功
//                } else {
//                    final int count = Optional
//                            .ofNullable(loginPhoneCodeBucket.get("count"))
//                            .map(Objects::toString)
//                            .map(Integer::parseInt)
//                            .orElse(0)
//                            + 1;
//                    loginPhoneCodeBucket.put("count", count);
//                    if (count >= 3) { // 计数，避免暴力破解，验证码输入错误次数达到 3 次时，删除验证码
//                        loginPhoneCodeBucket.delete();
//                    }
//                    throw Code.A00002.toCodeException("验证码错误");
//                }
//            }
//            // 验证码验证成功之后，获取微信认证缓存
//            final AppWechatUserInfoVO appWechatUserInfoVOCache = wechatService.getCache(wxCode);
//            Code.A00003.assertNonNull(appWechatUserInfoVOCache, "微信认证已过期");
//
//            final LoginUserVO userVO = userService.findAndRegistAppUser(
//                    phone
//                    , row -> {
//                        row.setNickname(appWechatUserInfoVOCache.getNickname());
//                        if (!Strings.isNullOrEmpty(appWechatUserInfoVOCache.getHeadimgurl())) { // 从微信服务器下载头像
//                            final UserAvatarImageDTO avatarImageDTO = new UserAvatarImageDTO();
//                            avatarImageDTO.setName("wechat.jpeg");
//                            avatarImageDTO.setUname(appWechatUserInfoVOCache.getOpenid() + ".jpeg");
//                            row.setAvatar(fileService.writeFromUrl(appWechatUserInfoVOCache.getHeadimgurl(), avatarImageDTO));
//                        }
//                        return row;
//                    }
//                    , registerUserId -> {
//                    }
//            );
//            Code.A00002.assertHasTrue(userVO.isAllow(), "用户账号已禁止登录");
//            final List<String> authorityList = userVO.getAuthorityList();
//    if (CollectionUtils.isEmpty(authorityList)) {
//        throw Code.A00010.toCodeException("%s：authorityList未授权",userId);
//    }
//            if (Arrays.stream(Role.values()).anyMatch(ele -> authorityList.contains(ele.name()))) {
//                // 硬编码权限可以登录
//            } else if (!authorityList.contains(client.name())) {
//                log.error("{}=>{}", client, JSON.toJsonString(authorityList));
//                throw AppCode.A01021.toCodeException(phone);
//            }
//            { // 写入微信账号绑定信息
//                final TabUserWechatInsertDTO dto = new TabUserWechatInsertDTO();
//                dto.setAppId(appWechatUserInfoVOCache.getAppId());
//                dto.setOpenId(appWechatUserInfoVOCache.getOpenid());
//                dto.setUnionId(appWechatUserInfoVOCache.getUnionid());
//                dto.setUserId(userVO.getId());
//                // dto.setSessionKey(appWechatUserInfoVOCache.get);
//                // dto.setRefreshToken(appWechatUserInfoVOCache.get);
//                userWechatService.insert(dto);
//            }
//            // 生成token 放在响应头
//            final String token = userVO.token(client);
//            response.setHeader("x-token", token);
//            result.setSuccess(token).addExtras("x-token", token);
//        });
//    }
//
//    @AppSwaggerGroup
//    @AppEventRequest
//    @ApiOperation(value = "微信小程序登录"
//            , tags = {""}
//            , notes = ""
//            + "<br> wx.login 获取 jsCode； https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/login.html"
//            + "<br> phonenumber.getPhoneNumber 获取 phoneCode； https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/getPhoneNumber.html"
//            + "<br> + 通过 jsCode 获取用户 openId、 unionId"
//            + "<br> + 通过 phoneCode 获取手机号"
//            + "<br> + openId+unionId 查询绑定记录"
//            + "<br> + 无绑定记录，使用手机号注册绑定微信账号"
//            + "<br> + 检查账号状态，禁用时抛出异常，不能登录"
//            + "<br> +、 检查必须有权限登录使用对应的客户端，否则抛异常消息"
//            + "<br> +  生成 x-token 写入 http 响应头"
//            + "<br> ------"
//            + "<br> + client 对应的枚举值参考【TokenClient】： #/全部接口/开发环境支撑/enumsUsingGET_2"
//            + "<br> + 登录成功之后 x-token 写入本地缓存，发请求时 x-token 写入请求头； "
//            + "<br> + 登录成功之后并不会返回用户信息，需要在请求头携带 x-token 调用 /user/current 接口获取用户信息"
//            + "<br> + 任意接口响应【401】表示token过期，需跳转登录页重新获取token"
//            + "<br> + 同一个账号在一个客户端只能有一个 token"
//    )
//    @PostMapping("/wechat-mini-app/oauth2/{client}/{jsCode}/{phoneCode}")
//    @ApiOperationSupport(order = 4, author = "谢长春")
//    @ResponseBody
//    public Result<String> wechatMiniAppOAuth2(
//            @ApiParam(required = true, value = "登录客户端", example = "WECHAT") @PathVariable final TokenClient client,
//            @ApiParam(required = true, value = "微信小程序登录凭证 code", example = "0000") @PathVariable final String jsCode,
//            @ApiParam(required = true, value = "动态令牌code，用于获取手机号", example = "0000") @PathVariable final String phoneCode,
//            @RequestBody final WechatMiniAppUserDTO body,
//            HttpServletResponse response
//    ) {
//        return new Result<String>().execute(result -> {
//            // 微信小程序认证结果
//            final WechatMiniAppJsCode2SessionVO wechatMiniAppJsCode2SessionVO = wechatMiniAppService.jsCode2Session(jsCode);
////            final WechatMiniAppJsCode2SessionVO wechatMiniAppJsCode2SessionVO =JSON.parseObject("{\"appId\":\"wxa0473a8cbe349ca7\",\"openid\":\"oG9yB5HmQfcQ2sieSO02AFTWgNvY\",\"session_key\":\"Xpysn3mvQa7W0UhJSNEfZQ==\",\"unionid\":\"om-7c6uuVkbD-gSCLW_MIAUzkuCE\"}", WechatMiniAppJsCode2SessionVO.class);
//            // 查询是否有绑定微信账号
//            final Optional<TabUserWechat> userWechatOptional = Optional.ofNullable(userWechatService
//                    // 查询微信返回的 openId 是否已经绑定过用户，已经绑定。 生成 token 返回登录成功
//                    .findById(wechatMiniAppJsCode2SessionVO.buildUserWechatId()) // md5(appId+openId)
//                    .orElseGet(() -> userWechatService
//                            // 查询微信返回的 unionId 是否已经绑定过用户，已经绑定。 生成 token 返回登录成功
//                            .findByUnionId(wechatMiniAppJsCode2SessionVO.getUnionid())
//                            .orElse(null)
//                    )
//            );
//            LoginUserVO userVO;
//            if (userWechatOptional.isPresent()) {
//                final TabUserWechat tabUserWechat = userWechatOptional.get();
//                // 已绑定账号，生成 token 回传到前端
//                userVO = userService.findById(tabUserWechat.getUserId())
//                        .orElseThrow(() -> Code.B00001.toCodeException("微信未绑定用户:%s:%s", wechatMiniAppJsCode2SessionVO.getAppId(), wechatMiniAppJsCode2SessionVO.getOpenid()))
//                        .toTabUserVO();
//            } else {
//                // 微信小程序手机号
//                final WechatMiniAppPhoneVO wechatMiniAppPhoneVO = wechatMiniAppService.getPhone(phoneCode);
////                final WechatMiniAppPhoneVO wechatMiniAppPhoneVO = JSON.parseObject("{\"errcode\": 0, \"errmsg\": \"ok\", \"phone_info\": {\"countryCode\": \"86\", \"phoneNumber\": \"17500000000\", \"purePhoneNumber\": \"17500000000\", \"watermark\": {\"appid\": \"wxa0473a8cbe349ca7\", \"timestamp\": 1653385804}}}", WechatMiniAppPhoneVO.class);
//                final String phone = wechatMiniAppPhoneVO.getPhone_info().getPurePhoneNumber();
//                userVO = userService
//                        .findAndRegistAppUser(
//                                phone
//                                , row -> { // 补充用户基本信息
//                                    row.setNickname(Optional.ofNullable(body.getNickname()).orElse(""));
//                                    if (!Strings.isNullOrEmpty(body.getAvatar())) { // 从微信服务器下载头像
//                                        final UserAvatarImageDTO avatarImageDTO = new UserAvatarImageDTO();
//                                        avatarImageDTO.setName("wechat.jpeg");
//                                        avatarImageDTO.setUname(wechatMiniAppJsCode2SessionVO.getOpenid() + ".jpeg");
//                                        row.setAvatar(fileService.writeFromUrl(body.getAvatar(), avatarImageDTO));
//                                    }
//                                    return row;
//                                }
//                                , registerUserId -> {
//                                    if (StringUtils.isNoneBlank(body.getScene())) {// 新用户注册成功之后的回调，老用户不走这段逻辑
//                                        final Map<String, String> mdcContextMap = MDC.getCopyOfContextMap(); // 复制 MDC 上下文
//                                        multiThreadExecutorService.execute(() -> {
//                                            try {
//                                                MDC.setContextMap(mdcContextMap); // 设置上下文，保持链路追踪
//                                                String userName = "";
//                                                final String wechatScene = userService.decryptWechatScene(body.getScene());
//                                                userService.getUserByWechatScene(wechatScene).ifPresent(sceneUser -> {
//                                                    // 邀请
//                                                });
//                                            } finally {
//                                                MDC.clear(); // 清理 MDC， 否则会导致内存泄漏
//                                            }
//                                        });
//                                    }
//                                }
//                        )
//                        .toTabUserVO();
//                { // 写入微信账号绑定信息
//                    final TabUserWechatInsertDTO dto = new TabUserWechatInsertDTO();
//                    dto.setAppId(wechatMiniAppJsCode2SessionVO.getAppId());
//                    dto.setOpenId(wechatMiniAppJsCode2SessionVO.getOpenid());
//                    dto.setUnionId(wechatMiniAppJsCode2SessionVO.getUnionid());
//                    dto.setUserId(userVO.getId());
//                    dto.setSessionKey(wechatMiniAppJsCode2SessionVO.getSession_key());
//                    userWechatService.insert(dto);
//                }
//            }
//            final List<String> authorityList = userVO.getAuthorityList();
//    if (CollectionUtils.isEmpty(authorityList)) {
//        throw Code.A00010.toCodeException("%s：authorityList未授权",userId);
//    }
//            if (Arrays.stream(Role.values()).anyMatch(ele -> authorityList.contains(ele.name()))) {
//                // 硬编码权限可以登录
//            } else if (!authorityList.contains(client.name())) {
//                log.error("{}=>{}", client, JSON.toJsonString(authorityList));
//                throw AppCode.A01021.toCodeException(userVO.getPhone());
//            }
//            Code.A00002.assertHasTrue(userVO.isAllow(), "用户账号已禁止登录");
//            // 生成token 放在响应头
//            final String token = userVO.token(client);
//            response.setHeader("x-token", token);
//            result.setSuccess(token);
//        });
//    }
//    /**
//     * 激活账户
//     */
//    @GetMapping("/email/{uuid}")
//    public String activate(@PathVariable String uuid) {
//        return authService.activate(uuid);
//    }
//    /**
//     * 忘记密码，发送修改密码邮件
//     */
//    @PatchMapping("/forget/password")
//    @ResponseBody
//    public Result<Object> forgetPassword(@RequestBody(required = false) String body) {
//        Result<Object> result = new Result<>();
//        try {
//            authService.forgetPassword(JSON.parseObject(body).getString("email"));
//            result.setCode(SUCCESS);
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            result.setCode(e);
//        }
//        return result;
//    }

}
