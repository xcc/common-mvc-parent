package com.ccx.demo.feign.baidu.dto;

import com.ccx.demo.feign.baidu.enums.ImageType;
import com.ccx.demo.feign.baidu.enums.LivenessControl;
import com.ccx.demo.feign.baidu.enums.QualityControl;
import com.ccx.demo.feign.baidu.enums.SpoofingControl;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 公安验证 参数
 * <pre>
 * <table border>
 * <thead> <tr> <th>参数</th> <th>必选</th> <th>类型</th> <th>说明</th> </tr> </thead>
 * <tbody>
 * <tr> <td>image</td> <td>是</td> <td>string</td> <td>图片信息(<strong>总数据大小应小于10M</strong>)，图片上传方式根据image_type来判断</td> </tr>
 * <tr> <td>image_type</td> <td>是</td> <td>string</td> <td>图片类型      <br><strong>BASE64</strong>:图片的base64值，base64编码后的图片数据，编码后的图片大小不超过2M；图片尺寸不超过1920*1080 <br><strong>URL</strong>:图片的 URL地址( 可能由于网络等原因导致下载图片时间过长)<strong>； <br></strong>FACE_TOKEN**: 人脸图片的唯一标识，调用人脸检测接口时，会为每个人脸图片赋予一个唯一的FACE_TOKEN，同一张图片多次检测得到的FACE_TOKEN是同一个。</td> </tr>
 * <tr> <td>id_card_number</td> <td>是</td> <td>string</td> <td>身份证号码</td> </tr>
 * <tr> <td>name</td> <td>是</td> <td>string</td> <td>姓名（注：需要是UTF-8编码的中文）</td> </tr>
 * <tr> <td>quality_control</td> <td>否</td> <td>string</td> <td>图片质量控制  <br> <strong>NONE</strong>: 不进行控制  <br><strong>LOW</strong>:较低的质量要求  <br><strong>NORMAL</strong>: 一般的质量要求  <br><strong>HIGH</strong>: 较高的质量要求  <br><strong>默认 NONE</strong></td> </tr>
 * <tr> <td>liveness_control</td> <td>否</td> <td>string</td> <td>活体检测控制   <br><strong>NONE</strong>: 不进行控制  <br><strong>LOW</strong>:较低的活体要求(高通过率 低攻击拒绝率)  <br><strong>NORMAL</strong>: 一般的活体要求(平衡的攻击拒绝率, 通过率)  <br><strong>HIGH</strong>: 较高的活体要求(高攻击拒绝率 低通过率)  <br><strong>默认NONE</strong></td> </tr>
 * <tr> <td>spoofing_control</td> <td>否</td> <td>string</td> <td>合成图控制 <br><strong>NONE</strong>: 不进行控制 <br><strong>LOW</strong>:较低的合成图检测要求(高通过率 低攻击拒绝率) <br><strong>NORMAL</strong>: 一般的合成图检测要求(平衡的攻击拒绝率, 通过率) <br><strong>HIGH</strong>: 较高的活体要求(高攻击拒绝率 低通过率) <br>默认<strong>NONE</strong></td> </tr>
 * </tbody>
 * </table>
 * </pre>
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("公安验证-头像采集")
public class BaiduPersonVerifyDTO {

    /**
     * image	是	string	图片信息(总数据大小应小于10M)，图片上传方式根据image_type来判断
     */
    @ApiModelProperty(value = "头像base64", position = 1, required = true)
    private String image;
    /**
     * 图片类型
     */
    @JsonProperty("image_type")
    @ApiModelProperty(value = "图片类型", position = 2)
    private ImageType imageType = ImageType.BASE64;
    /**
     * 身份证号码
     */
    @JsonProperty("id_card_number")
    @ApiModelProperty(value = "身份证号码", position = 3, required = true)
    private String idCardNumber;
    /**
     * 姓名（注：需要是UTF-8编码的中文）
     */
    @ApiModelProperty(value = "姓名", position = 4, required = true)
    private String name;
    /**
     * 图片质量控制
     */
    @JsonProperty("quality_control")
    @ApiModelProperty(value = "图片质量控制", position = 5)
    private QualityControl qualityControl;
    /**
     * 活体检测控制
     */
    @JsonProperty("liveness_control")
    @ApiModelProperty(value = "活体检测控制", position = 6)
    private LivenessControl livenessControl;
    /**
     * 合成图控制
     */
    @JsonProperty("spoofing_control")
    @ApiModelProperty(value = "合成图控制", position = 7)
    private SpoofingControl spoofingControl;
}
