package com.ccx.demo.config.convert;

import com.common.util.JSON;
import com.common.util.RangeInt;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToRangeIntConverter implements Converter<String, RangeInt> {
    @Override
    public RangeInt convert(String value) {
        return JSON.parseObject(value, RangeInt.class);
    }
}
