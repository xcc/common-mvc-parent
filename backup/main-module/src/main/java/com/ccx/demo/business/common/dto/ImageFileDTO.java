package com.ccx.demo.business.common.dto;

import com.ccx.demo.config.init.AppConfig.Path;
import com.ccx.demo.config.init.AppConfig.URL;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 图片文件对象
 *
 * @author 谢长春 on 2022-06-24
 */
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "图片文件对象")
public class ImageFileDTO extends FileDTO {

    private static final long serialVersionUID = -6779893372268172821L;

    public ImageFileDTO(final String uname) {
        super(uname);
    }

    public ImageFileDTO(final String name, final String uname) {
        super(name, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = ImageFileDTO.class.getSimpleName();

    /**
     * 等比缩放的缩略图，用于列表页面优化图片加载速度，点击该图片才显示原图
     */
    @ApiModelProperty(value = "等比缩放的缩略图，用于列表页面优化图片加载速度，点击该图片才显示原图；命名规则：uname_mini.png", example = "https://oss.app.com/image_mini.png", accessMode = READ_ONLY)
    protected String mini;

    /**
     * 裁剪过的图片，比如裁减方图用于9宫格，点击该图片才显示原图
     */
    @ApiModelProperty(value = "裁剪过的图片，比如裁减方图用于9宫格，点击该图片才显示原图；命名规则：uname_crop.png", example = "https://oss.app.com/image_crop.png", accessMode = READ_ONLY)
    protected String crop;

    /**
     * 构造缩略图文件名: uname.png => uname_crop.png
     *
     * @return T
     */
    @SuppressWarnings("unchecked")
    public <T extends ImageFileDTO> T buildMini() {
        this.mini = this.uname.replaceAll("(\\.[a-zA-Z]+)$", "_mini$1");
        return (T) this;
    }

    /**
     * 构造裁剪图文件名: uname.png => uname_crop.png
     *
     * @return T
     */
    @SuppressWarnings("unchecked")
    public <T extends ImageFileDTO> T buildCorp() {
        this.crop = this.uname.replaceAll("(\\.[a-zA-Z]+)$", "_crop$1");
        return (T) this;
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMiniPath() {
        return StringUtils.isEmpty(mini) ? null : Path.TEMP.get(mini);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getCropPath() {
        return StringUtils.isEmpty(crop) ? null : Path.TEMP.get(crop);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getMiniUrl() {
        return StringUtils.isEmpty(mini) ? getUrl() : URL.TEMP.append(mini);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getCropUrl() {
        return StringUtils.isEmpty(crop) ? getUrl() : URL.TEMP.append(crop);
    }

    @SneakyThrows
    public ImageFileDTO cloneObject() {
        return (ImageFileDTO) super.clone();
    }
}
