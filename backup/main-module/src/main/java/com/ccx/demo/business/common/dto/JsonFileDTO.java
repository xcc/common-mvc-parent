package com.ccx.demo.business.common.dto;

import com.ccx.demo.config.init.AppConfig.Path;
import com.ccx.demo.config.init.AppConfig.URL;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

/**
 * 静态json文件对象
 *
 * @author 谢长春 on 2022-06-24
 */
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "静态json文件对象")
public class JsonFileDTO extends FileDTO {

    private static final long serialVersionUID = -6779893372268172821L;

    public JsonFileDTO(final String uname) {
        super(uname);
    }

    public JsonFileDTO(final String name, final String uname) {
        super(name, uname);
    }
//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = JsonFileDTO.class.getSimpleName();

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : Path.JSON.get(uname);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getUrl() {
        return StringUtils.isEmpty(uname) ? null : URL.JSON.append(uname);
    }

    @SneakyThrows
    public JsonFileDTO cloneObject() {
        return (JsonFileDTO) super.clone();
    }
}
