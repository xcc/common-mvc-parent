package com.ccx.demo.config;
//
//import io.minio.MinioClient;
//import lombok.*;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.Objects;
//
///**
// * 微信配置
// *
// * @author 谢长春 2020-11-13
// */
//@Configuration(proxyBeanMethods = false)
//@ConditionalOnProperty(value = "spring.app.minio.enabled", havingValue = "true")
//@Slf4j
//@Getter @Setter
//@ConfigurationProperties(prefix = "spring.app.minio")
//public class WechatConfiguration {
//    /**
//     * 是否开启 minio 开关。 注意：开关打开后，需要修改 files 配置
//     */
//    private boolean enabled;
//    /**
//     * 对象存储服务的URL
//     */
//    private String endpoint;
//    /**
//     * Access key就像用户ID，可以唯一标识你的账户。
//     */
//    private String accessKey;
//    /**
//     * Secret key是你账户的密码。
//     */
//    private String secretKey;
//    /**
//     * 存储桶名称
//     */
//    private String bucket;
//
//    /**
//     * 微信小程序配置
//     */
//    @Getter @Setter
//    public static class WxApp {
//        /**
//         * 微信小程序 AppID(小程序ID).
//         */
//        private String id;
//        /**
//         * 微信小程序 AppSecret(小程序密钥).
//         */
//        private String secret;
//    }
//
//    /**
//     * 微信公众号配置
//     */
//    @Getter @Setter
//    public static class Wechat {
//        /**
//         * 微信公众号 开发者ID(AppID)
//         */
//        private String id;
//        /**
//         * 开发者密码(AppSecret)
//         */
//        private String secret;
//        /**
//         * 微信 token
//         */
//        private String token;
//        /**
//         * 微信 encodingAesKey
//         */
//        private String encodingAesKey;
//        /**
//         * 微信支付商户号
//         */
//        private String mchId;
//        /**
//         * 微信支付商户号中设置的Api秘钥
//         */
//        private String apiKey;
//    }
//    if (Objects.nonNull(appConfig.getWechat())) {
//        config.append(String.format("%s【%s】 : %s\n", "spring.app.wechat.id", "微信公众号 开发者ID(AppID)", appConfig.getWechat().getId()))
//                .append(String.format("%s【%s】 : %s\n", "spring.app.wechat.secret", "开发者密码(AppSecret)", appConfig.getWechat().getSecret()))
//                .append(String.format("%s【%s】 : %s\n", "spring.app.wechat.token", "微信 token", appConfig.getWechat().getToken()))
//                .append(String.format("%s【%s】 : %s\n", "spring.app.wechat.encoding-aes-key", "微信 encodingAesKey", appConfig.getWechat().getEncodingAesKey()))
//                .append(String.format("%s【%s】 : %s\n", "spring.app.wechat.mch-id", "微信支付商户号", appConfig.getWechat().getMchId()))
//                .append(String.format("%s【%s】 : %s\n", "spring.app.wechat.api-key", "微信支付商户号中设置的Api秘钥", appConfig.getWechat().getApiKey()))
//        ;
//    }
//            if (Objects.nonNull(appConfig.getWxApp())) {
//        config.append(String.format("%s【%s】 : %s\n", "spring.app.wx-app.id", "微信小程序 AppID(小程序ID)", appConfig.getWxApp().getId()))
//                .append(String.format("%s【%s】 : %s\n", "spring.app.wx-app.secret", "微信小程序 AppSecret(小程序密钥)", appConfig.getWxApp().getSecret()))
//        ;
//
//    }
//}
