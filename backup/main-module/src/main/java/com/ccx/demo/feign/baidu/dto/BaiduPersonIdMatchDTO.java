package com.ccx.demo.feign.baidu.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 身份证与姓名比对 参数
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("身份证与姓名比对")
public class BaiduPersonIdMatchDTO {

    /**
     * 身份证号码
     */
    @JsonProperty("id_card_number")
    @ApiModelProperty("身份证号码")
    private String idCardNumber;
    /**
     * 姓名（注：需要是UTF-8编码的中文）
     */
    @ApiModelProperty("姓名")
    private String name;
}
