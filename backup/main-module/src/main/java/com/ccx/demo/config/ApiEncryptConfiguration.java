package com.ccx.demo.config;

import com.common.db.entity.Result;
import com.common.enums.Code;
import com.common.util.JSON;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import lombok.Cleanup;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * api 数据包加解密
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
// @ConditionalOnProperty(value = "spring.app.encrypt.api", havingValue = "true")
@ConfigurationProperties("spring.app.encrypt.api")
public class ApiEncryptConfiguration {
    /**
     * 加解密开关。 true：是，false：否。
     */
    @Setter
    private boolean enabled;
    /**
     * 全局 aes 密钥， 32位
     */
    @Setter
    private String secretKey;
    /**
     * 固定 IV ， 16位。 设置固定 IV 之后同样内容每次加密结果一样。 默认为 null， 表示使用动态 IV
     */
    @Setter
    private String ivParameterSpec;
    /**
     * API 接口数据包签名类型
     */
    @Setter
    private String signType;

    private static final Cipher CIPHER;
    /**
     * https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#trans
     */
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 参考文档: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
     * <pre>
     * Cipher (Encryption) Algorithms
     * Cipher Algorithm Names
     * The following names can be specified as the algorithm component in a transformation when requesting an instance of Cipher.
     *
     * <table border="5" cellpadding="5" frame="border" width="90%" summary="Cipher Algorithm Names">
     * <thead> <tr> <th>Algorithm Name</th> <th>Description</th> </tr> </thead>
     * <tbody>
     * <tr> <td>AES</td> <td>Advanced Encryption Standard as specified by NIST in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS 197</a>. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits.</td> </tr>
     * <tr> <td>AESWrap</td> <td>The AES key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3394.txt">RFC 3394</a>.</td> </tr>
     * <tr> <td>ARCFOUR</td> <td>A stream cipher believed to be fully interoperable with the RC4 cipher developed by Ron Rivest. For more information, see K. Kaukonen and R. Thayer, "A Stream Cipher Encryption Algorithm 'Arcfour'", Internet Draft (expired), <a href="http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt"> draft-kaukonen-cipher-arcfour-03.txt</a>.</td> </tr>
     * <tr> <td>Blowfish</td> <td>The <a href="http://www.schneier.com/blowfish.html">Blowfish block cipher</a> designed by Bruce Schneier.</td> </tr>
     * <tr> <td>CCM</td> <td>Counter/CBC Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38C/SP800-38C_updated-July20_2007.pdf">NIST Special Publication SP 800-38C</a>.</td> </tr>
     * <tr> <td>DES</td> <td>The Digital Encryption Standard as described in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS PUB 46-3</a>.</td> </tr>
     * <tr> <td>DESede</td> <td>Triple DES Encryption (also known as DES-EDE, 3DES, or Triple-DES). Data is encrypted using the DES algorithm three separate times. It is first encrypted using the first subkey, then decrypted with the second subkey, and encrypted with the third subkey.</td> </tr>
     * <tr> <td>DESedeWrap</td> <td>The DESede key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3217.txt">RFC 3217</a> .</td> </tr>
     * <tr> <td>ECIES</td> <td>Elliptic Curve Integrated Encryption Scheme</td> </tr>
     * <tr> <td>GCM</td> <td>Galois/Counter Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38D/SP-800-38D.pdf">NIST Special Publication SP 800-38D</a>.</td> </tr>
     * <tr> <td>PBEWith&lt;digest&gt;And&lt;encryption&gt; PBEWith&lt;prf&gt;And&lt;encryption&gt;</td> <td>The password-based encryption algorithm found in (PKCS5), using the specified message digest (&lt;digest&gt;) or pseudo-random function (&lt;prf&gt;) and encryption algorithm (&lt;encryption&gt;). Examples: <ul> <li><b>PBEWithMD5AndDES</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, Nov 1993</a>. Note that this algorithm implies <a href="#cbcMode"><i>CBC</i></a> as the cipher mode and <a href="#pkcs5Pad"><i>PKCS5Padding</i></a> as the padding scheme and cannot be used with any other cipher modes or padding schemes.</li> <li><b>PBEWithHmacSHA256AndAES_128</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Cryptography Standard," version 2.0, March 1999</a>.</li> </ul> </td> </tr>
     * <tr> <td>RC2</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RC4</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc. (See note prior for ARCFOUR.)</td> </tr>
     * <tr> <td>RC5</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RSA</td> <td>The RSA encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2125">PKCS #1</a></td> </tr>
     * </tbody>
     * </table>
     */
    private static final String CIPHER_ALGORITHM = "AES";
    private static final int ZERO = 0;
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    private static SecretKeySpec SECRET_KEY;
    /**
     * IV 长度
     */
    private static final int IV_LENGTH = 16;
    /**
     * 请求响应报文加解密：固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    private static IvParameterSpec IV;
    /**
     * 加解密开关。 true：是，false：否。
     */
    private static boolean ENABLED;

    static {
        try {
            CIPHER = Cipher.getInstance(TRANSFORMATION);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final int MAX = 70;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PostConstruct
    private void postConstruct() {
        log.info(LOG_PATTERN
                , "API 接口数据包 AES 加解密， 用于前后端数据交互密文传输关键字段"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.api.enabled: " + enabled, MAX, ' '), MAX) + " # 是否开启API 接口数据包加密"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.api.secretKey: " + secretKey, MAX, ' '), MAX) + " # 密钥"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.api.ivParameterSpec: " + ivParameterSpec, MAX, ' '), MAX) + " # 固定 IV ， 16位"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.api.signType: " + signType, MAX, ' '), MAX) + " # 用于请求数据包签名，目前只支持MD5签名"
                )
                , "API 接口数据包 AES 加解密， 用于前后端数据交互密文传输关键字段"
        );
        ENABLED = enabled;
        // 设置密钥 32 位: RandomStringUtils.randomAlphanumeric(32)
        {
            if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
                throw new IllegalArgumentException("secretKey 长度必须是 32 位");
            }
            SECRET_KEY = new SecretKeySpec(secretKey.getBytes(UTF_8), CIPHER_ALGORITHM);
        }
        // 设置 IV ， 长度必须是 16 位 RandomStringUtils.randomAlphanumeric(16)
        {
            if (StringUtils.isNotBlank(ivParameterSpec)) {
                if (ivParameterSpec.length() != 16) {
                    throw new IllegalArgumentException("ivParameterSpec 长度必须是 16 位");
                }
                IV = new IvParameterSpec(ivParameterSpec.getBytes(UTF_8));
            }
        }
    }

    /**
     * 响应结果加密
     */
    @Component
    @ConditionalOnProperty(value = "spring.app.encrypt.api.enabled", havingValue = "true")
    @ControllerAdvice(basePackages = "com.ccx.demo")
    public static class AesApiEncryptResponseBodyAop implements ResponseBodyAdvice<Object> {

        @Override
        public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
            // 是否执行 beforeBodyWrite
            return true;
        }

        @Override
        public Object beforeBodyWrite(Object body,
                                      MethodParameter returnType,
                                      MediaType selectedContentType,
                                      Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                      ServerHttpRequest request,
                                      ServerHttpResponse response) {
            if (body instanceof Result) {
                return encryptBody(JSON.toJsonString(body));
            }
            return body;
        }
    }

    /**
     * 请求参数解密， 只有 POST PUT DELETE PATCH 请求，且 body 不为空的情况才需要解密。 上传文件也不会走解密逻辑
     */
    @Component
    @ConditionalOnProperty(value = "spring.app.encrypt.api.enabled", havingValue = "true")
    @ControllerAdvice(basePackages = "com.ccx.demo")
    public static class AesApiDecryptRequestBodyAop implements RequestBodyAdvice {

        @Override
        public boolean supports(MethodParameter methodParameter, Type targetType,
                                Class<? extends HttpMessageConverter<?>> converterType) {
            // 是否执行 beforeBodyRead 、 afterBodyRead 、 handleEmptyBody
            return true;
        }

        @Override
        public HttpInputMessage beforeBodyRead(HttpInputMessage request,
                                               MethodParameter parameter,
                                               Type targetType,
                                               Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
            // 请求数据
            @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(request.getBody());
            final String body = CharStreams.toString(inputStreamReader);
            if (log.isDebugEnabled()) {
                log.debug("密文:{}", body);
            }
            return new HttpInputMessage() {
                @SneakyThrows
                @Override
                public InputStream getBody() {
                    @Cleanup final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                            Optional.of(body)
                                    .map(val -> {
                                        final String decrypt = decryptBody(val);
                                        if (log.isInfoEnabled()) {
                                            log.info("body: {}", decrypt);
                                        }
                                        return decrypt;
                                    })
                                    .map(val -> val.getBytes(UTF_8))
                                    .orElse(new byte[0])
                    );
                    return byteArrayInputStream;
                }

                @Override
                public HttpHeaders getHeaders() {
                    return request.getHeaders();
                }
            };
        }

        @Override
        public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                    Class<? extends HttpMessageConverter<?>> converterType) {
            if (log.isTraceEnabled()) log.trace("# afterBodyRead");
            return body;
        }

        @Override
        public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter,
                                      Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
            if (log.isTraceEnabled()) log.trace("# handleEmptyBody");
            return body;
        }

    }

    /**
     * 请求参数解密， 只有 POST PUT DELETE PATCH 请求，且 body 不为空的情况才需要解密。 上传文件也不会走解密逻辑
     */
    @Component
    @ConditionalOnProperty(value = "spring.app.encrypt.api.enabled", havingValue = "false")
    @ControllerAdvice(basePackages = "com.ccx.demo")
    public static class LogRequestBodyAop implements RequestBodyAdvice {

        @Override
        public boolean supports(MethodParameter methodParameter, Type targetType,
                                Class<? extends HttpMessageConverter<?>> converterType) {
            // 是否执行 beforeBodyRead 、 afterBodyRead 、 handleEmptyBody
            return true;
        }

        @Override
        public HttpInputMessage beforeBodyRead(HttpInputMessage request,
                                               MethodParameter parameter,
                                               Type targetType,
                                               Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
            // 请求数据
            @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(request.getBody());
            final String body = CharStreams.toString(inputStreamReader);
            if (log.isInfoEnabled()) {
                log.info("body: {}", body);
            }
            return new HttpInputMessage() {
                @SneakyThrows
                @Override
                public InputStream getBody() {
                    return new ByteArrayInputStream(body.getBytes(UTF_8));
                }

                @Override
                public HttpHeaders getHeaders() {
                    return request.getHeaders();
                }
            };
        }

        @Override
        public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                    Class<? extends HttpMessageConverter<?>> converterType) {
            if (log.isTraceEnabled()) log.trace("# afterBodyRead");
            return body;
        }

        @Override
        public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter,
                                      Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
            if (log.isTraceEnabled()) log.trace("# handleEmptyBody");
            return body;
        }

    }

    private static final Function<byte[], String> hexEncode = Hex::encodeHexString;
    private static final Function<String, byte[]> hexDecode = (String hexString) -> {
        try {
            return Hex.decodeHex(hexString);
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    };
    private static final Function<byte[], String> base64Encode = Base64::encodeBase64String;
    private static final Function<String, byte[]> base64Decode = Base64::decodeBase64;

    /**
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 setIV(String) ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param body {@link String} 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    private static String encryptBody(final String body) {
        try {
            if (Strings.isNullOrEmpty(body)) {
                return null;
            }
            if (!ENABLED) {
                return body;
            }
            if (Objects.isNull(IV)) { // 未指定 IV 使用动态 IV 加密
                // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
                final byte[] ivBytes = RandomStringUtils.randomAlphanumeric(IV_LENGTH).getBytes();
                // 初始化加密参数，设置为加密模式，指定密钥，设置IV
                CIPHER.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
                final byte[] dataBytes = CIPHER.doFinal(body.getBytes(UTF_8));
                final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
                System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
                System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
                // final String result = hexEncode.apply(bytes);
                final String result = base64Encode.apply(bytes);
                if (log.isDebugEnabled()) {
                    log.debug("{} => {}", body, result);
                }
                return result;
            }
            // 使用固定的 IV 加密
            CIPHER.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV);
            // final String result = hexEncode.apply(cipher.doFinal(data.getBytes(UTF_8)));
            final String result = base64Encode.apply(CIPHER.doFinal(body.getBytes(UTF_8)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", body, result);
            }
            return result;
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%s", body);
        }
    }

    /**
     * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
     *
     * @param body {@link String} 密文
     * @return 明文
     */
    @SneakyThrows
    private static String decryptBody(final String body) {
        try {
            if (Strings.isNullOrEmpty(body)) {
                return null;
            }
            if (!ENABLED) {
                return body;
            }
            if (Objects.isNull(IV)) { // 未指定 iv 使用动态 iv 解密
                // 将 data 分割成 IV 和密文
                // final byte[] bytes = hexDecode.apply(data);
                final byte[] bytes = base64Decode.apply(body);
                final byte[] ivBytes = new byte[IV_LENGTH];
                final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
//                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);

                CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
                final String result = new String(CIPHER.doFinal(dataBytes));
                if (log.isDebugEnabled()) {
                    log.debug("{}({}) => {}", body, new String(ivBytes), result);
                }
                return result;
            }
            // 使用固定的 IV 解密
            CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV);
            // final String result = new String(cipher.doFinal(hexDecode.apply(data)));
            final String result = new String(CIPHER.doFinal(base64Decode.apply(body)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", body, result);
            }
            return result;
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", body);
        }
    }

    /**
     * api请求数据验签
     * post|put|patch|delete 数据接口验证请求 body
     * post 文件上传接口验证 request uri ， 需要排除 sign 参数
     * get 接口验证 request uri ， 需要排除 sign 参数
     *
     * @param timeMillis 当前时间戳精确到毫秒
     * @param random     随机字符串
     * @param aesBody    aes加密后的请求体
     * @param sign       客户端签名
     */
    public static void assertHttpSign(
            final String timeMillis
            , final String random
            , final String aesBody
            , final String sign
    ) {
        if (!ENABLED) {
            return;
        }
//            String signature = "";
//            try {
//                Code.A00003.assertNonBlank(timeMillis, "签名时间戳无效");
//                Code.A00003.assertNonBlank(random, "签名随机字符串无效");
//                Code.A00003.assertNonBlank(sign, "签名无效");
//                Code.A00003.assertHasTrue(
//                        time_millis.parse(timeMillis).before(Dates.now().addMinute(5))
//                        , "签名时间戳有效期为5分钟"
//                );
//                signature = DigestUtils.md5Hex(
//                        String.join("",
//                                Stream.of(
//                                                timeMillis, // 当前时间戳精确到毫秒
//                                                random, // 随机字符串长度32-128位
//                                                SK, // AES固定加密密钥32位
//                                                Optional.ofNullable(aesBody).orElse("") // AES+Base64加密的数据包
//                                        )
//                                        .collect(Collectors.toCollection(TreeSet::new))
//                        )
//                );
//                Code.A00003.assertHasTrue(Objects.equals(sign, signature), "验签失败");
//            } catch (Exception e) {
//                log.error("sign:{}, signature:{}, timeMillis:{}, random:{}, aesBody:{}", sign, signature, timeMillis, random, aesBody);
//                throw e;
//            }
    }

    @SneakyThrows
    public static void main(String[] args) {
        final ApiEncryptConfiguration enc = new ApiEncryptConfiguration();

        String env = "local";
//        env = "dev";
//        env = "sit";
//        env = "uat";
//        env = "prod";
        if (Objects.equals(env, "local") || Objects.equals(env, "dev")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/dev/spring.app.encrypt.api.secretKey")));
            enc.setSecretKey(secretKey);
            enc.setEnabled(true);
            enc.postConstruct();
            System.out.println(env + ": " + encryptBody("{\"id\":1000}"));
            System.out.println(env + ": " + encryptBody("{\"username\":\"admin-test\",\"password\":\"wnQd7TXKhjEaxMEwa9Rbxcui1GrU9GJqrMUrkLonRgjjMJgRQiZm9Kc1N+sx6TjlaRdAtCTczfR7d8e1SSzhFTFUepsbTlrArd4kkml+tnJGKjKQ7fl35XV9sV+6vimjy+YPgnu0FRGBlxgpPdh634EQqmtJ+TcInFvavZoXXN7b6IQR0Mp97f9Pv74oiwGz8kZuOMJN2f0INNQIEsoyL0YN01sW1pGHQ+OXCA6OhNkIH9Opg1r8Cgzz+6ORcPdo9der7Ez59bHErQpU7QoeEQi0dDjskCZblApjL7TCdX6wzX/lVm5VkSlfrYM3R46FeYANCJAhebGQyg/He4DR8Q==\"}"));
            System.out.println(env + ": " + decryptBody("eXl6UGJaTVpZeW5nMFFFSUVQGifjDIy01AHgQsRswmHoI0cScVyYH8VJFr0HZ4d2PLpDiYcxDXQnP96ylxCanEheSAETnj9Ewqvr2g+tyP3or4cj+glosHAnInIK8arUzxnLtD3TqkXpBDD3rzyCy97Ru5roP4+Gv1EwkOX8zC9Lhmq33TP6D/5FzeCN/KCcl0ZssRDRT/5qznl8WbejKoZIibD3rzLsi5ksQD66wWjCg0Sq6ek4W3l+3GEReHgy4SEQnyjfH1ZqMiQXCfUcG+FNwONlmOLLYQvdZ1JDuBmpLGxOe6Ip0z7bvbYaqNlA+7nnLoFBXQCLUwwB5UdqV3WSR74guj1gGH9yrXr4+J/ZpHDHXhPWeAbt8fVnzH0s1devUbW7sSjyMENLwZXoinJB6NfCMGNu3BnmAee7Re8vx2HkU6goeZOhW6lTeHFyX3mzW3cbTP+QvQRMxQ9DpDe7rB3fdv8fNWK5zCg9RANtbwMIDgTMp0drtsQS8pH5yxHZBvTLJaoV9TYtjNxQLFzosUuv5eHxXThrUL4CGqjzMt1ew3FR9SGPNfr6iLKC+duu8p2FSRJofcW1UJUj+fu1pTPPcO7ql7sVXWwb/vQ3xV0l7dskqrmAYLPHwkYcwUtjrnEADhMHDNMsKrwcVpOcYM0UXkf0o/y2WjTXnXWHlQrRNusWkyLhTBJkVOLDrwZYI8Wu03TPLgB/Yambdnzc4z9gLrUeaQMjGJK2kW6Q8f71sNECa62zVu27Qb68S/VAZpnt0E52RC4pFdSJc4meLuA9NscI8dYsczvTAwXLjvjff4Wrxy+VjyocVuWjslh20sBxioi3zEYp57XD4A=="));
        }
        if (Objects.equals(env, "sit")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/spring.app.encrypt.api.secretKey")));
            enc.setSecretKey(secretKey);
            enc.setEnabled(true);
            enc.postConstruct();
            System.out.println(env + ": " + encryptBody("{\"id\":1000}"));
            System.out.println(env + ": " + decryptBody("MDAwMTY1NTk1NzcyNTc2NvvWAr2mK94jNw8yxdiDUF4="));
        }
        if (Objects.equals(env, "uat")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/spring.app.encrypt.api.secretKey")));
            enc.setSecretKey(secretKey);
            enc.setEnabled(true);
            enc.postConstruct();
            System.out.println(env + ": " + encryptBody("{\"id\":1000}"));
            System.out.println(env + ": " + decryptBody("MDAwMTY1NTk1NzcyNTc2N0+y5G71194onGrK4n2WSU4="));
        }
        if (Objects.equals(env, "prod")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/spring.app.encrypt.api.secretKey")));
            enc.setSecretKey(secretKey);
            enc.setEnabled(true);
            enc.postConstruct();
            System.out.println(env + ": " + encryptBody("{\"id\":1000}"));
            System.out.println(env + ": " + decryptBody("MDAwMTY1NTk1NzcyNTc2N53QIHaKOllHs9Fp5pgEsnA="));
        }
        // api请求数据签名测试
        {
//            final String aesBody = encrypt("明文，使用aes加密");
//
//            final long timeMillis = System.currentTimeMillis();
//            final String random = RandomStringUtils.randomAlphanumeric(32);
//            final String sign = DigestUtils.md5Hex(
//                    String.join("",
//                            Sets.newTreeSet(Arrays.asList(
//                                    Objects.toString(timeMillis), // 当前时间戳精确到毫秒
//                                    random, // 随机字符串长度32-128位
//                                    secretKey, // AES固定加密密钥32位
//                                    aesBody // AES+Base64加密的数据包
//                            ))
//                    )
//            );
//            log.info("\n- api请求数据签名测试 ------------------------" +
//                            "\n\t前后端约定的密钥：{}" +
//                            "\n\t当前时间戳精确到毫秒：{}" +
//                            "\n\t随机字符串：{}" +
//                            "\n\taes加密后的请求体：{}" +
//                            "\n\t签名：{}" +
//                            "\napi请求数据验签测试 : {}"
//                    , secretKey
//                    , timeMillis
//                    , random
//                    , aesBody
//                    , sign
//                    , Objects.equals(sign, DigestUtils.md5Hex(
//                            String.join("", Stream
//                                    .of(
//                                            Objects.toString(timeMillis), // 当前时间戳精确到毫秒
//                                            random, // 随机字符串长度32-128位
//                                            secretKey, // AES固定加密密钥32位
//                                            aesBody // AES+Base64加密的数据包
//                                    )
//                                    .filter(Objects::nonNull)
//                                    .collect(Collectors.toCollection(TreeSet::new))
//                            )
//                    ))
//            );
//            log.info("测试签名数据排序规则：{}", JSON.toJsonString(
//                    Stream.of(
//                                    System.currentTimeMillis() + ":当前时间戳精确到毫秒",
//                                    RandomStringUtils.randomAlphanumeric(32) + "：随机字符串长度32-128位",
//                                    secretKey + "：AES固定加密密钥32位",
//                                    aesBody // AES+Base64加密的数据包
//                            )
//                            .collect(Collectors.toCollection(TreeSet::new))
//            ));
//            log.info("测试签名数据排序规则：{}", JSON.toJsonString(Stream.of("B", "9", "0", "a", "1", "b", "A", "Z", "z").collect(Collectors.toCollection(TreeSet::new))));
        }
    }

}
