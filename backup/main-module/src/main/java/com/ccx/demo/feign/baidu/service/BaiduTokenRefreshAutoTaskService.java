package com.ccx.demo.feign.baidu.service;

import com.common.IAutoTask;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * 定时任务: 刷新百度 token
 *
 * @author 谢长春 2022-04-09
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.auto-task.services.baiduTokenRefreshAutoTaskService.enabled", havingValue = "true")
public class BaiduTokenRefreshAutoTaskService implements IAutoTask {
    @Autowired(required = false) // spring.app.feign.baidu.face.enabled = true 时，这个 bean 才会注入
    private BaiduFaceVerifyService baiduFaceVerifyService;
    @Autowired(required = false) // spring.app.feign.baidu.audit.enabled = true 时，这个 bean 才会注入
    private BaiduAuditService baiduAuditService;

    @Override
    public void call(final String date) {
        if (Objects.nonNull(baiduFaceVerifyService)) {
            try {
                baiduFaceVerifyService.refreshToken();
            } catch (Exception e) {
                log.error("百度人脸识别 token 刷新失败", e);
            }
        }
        if (Objects.nonNull(baiduAuditService)) {
            try {
                baiduAuditService.refreshToken();
            } catch (Exception e) {
                log.error("百度内容审核 token 刷新失败");
            }
        }
    }
}
