package com.ccx.demo.feign.baidu.service;

import com.ccx.demo.feign.baidu.api.BaiduAuditAudioApiService;
import com.ccx.demo.feign.baidu.api.BaiduAuditImageApiService;
import com.ccx.demo.feign.baidu.api.BaiduAuditTextApiService;
import com.ccx.demo.feign.baidu.api.BaiduTokenApiService;
import com.ccx.demo.feign.baidu.cache.BaiduAuditTokenCache;
import com.ccx.demo.feign.baidu.dto.BaiduAuditAudioDTO;
import com.ccx.demo.feign.baidu.dto.BaiduAuditImageDTO;
import com.ccx.demo.feign.baidu.vo.BaiduAuditAudioVO;
import com.ccx.demo.feign.baidu.vo.BaiduAuditVO;
import com.ccx.demo.feign.baidu.vo.BaiduTokenVO;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 百度内容审核
 *
 * @author 谢长春 2022-04-09
 */
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.feign.baidu.audit.enabled", havingValue = "true")
@ConfigurationProperties("spring.app.feign.baidu.audit")
public class BaiduAuditService {
    private static final String GRANT_TYPE = "client_credentials";
    private final BaiduAuditTokenCache baiduAuditTokenCache;
    private final BaiduTokenApiService baiduTokenApiService;
    private final BaiduAuditImageApiService baiduAuditImageApiService;
    private final BaiduAuditTextApiService baiduAuditTextApiService;
    private final BaiduAuditAudioApiService baiduAuditAudioApiService;

    /**
     * 百度内容审核应用配置: 应用id
     */
    @Setter
    private String appId;
    /**
     * 百度内容审核应用配置: api调用钥匙
     */
    @Setter
    private String apiKey;
    /**
     * 百度内容审核应用配置: api调用密钥
     */
    @Setter
    private String secretKey;

    /**
     * 定义百度内容审核失败的异常说明，这里比较严格，如果希望放宽审核内容，可以注释一部分
     * 参考地址： https://ai.baidu.com/ai-doc/ANTIPORN/Vk42xcpu1
     * type，subType 字段说明
     */
    private static final Table<Integer, Integer, String> table = HashBasedTable.create();
    private static final Map<Integer, String> errMessage = Maps.newHashMap();

    static {
        // table.put(type, subType, "type说明：subType说明");
        table.put(0, 0, "百度官方违禁图库：百度官方违禁图");
        table.put(1, 0, "色情识别：一般色情");
        table.put(1, 1, "色情识别：卡通色情");
        table.put(1, 2, "色情识别：SM");
        table.put(1, 3, "色情识别：低俗");
        table.put(1, 4, "色情识别：儿童裸露");
        table.put(1, 5, "色情识别：艺术品色情");
        table.put(1, 6, "色情识别：性玩具");
        table.put(1, 7, "色情识别：男性性感");
        table.put(1, 8, "色情识别：自然男性裸露");
        table.put(1, 9, "色情识别：女性性感");
        table.put(1, 10, "色情识别：卡通女性性感");
        table.put(1, 11, "色情识别：特殊类");
        table.put(1, 12, "色情识别：亲密行为");
        table.put(1, 13, "色情识别：卡通亲密行为");
        table.put(1, 14, "色情识别：孕肚裸露");
        table.put(1, 15, "色情识别：臀部特写");
        table.put(1, 16, "色情识别：脚部特写");
        table.put(1, 17, "色情识别：裆部特写");
        table.put(2, 0, "暴恐识别：警察部队");
        table.put(2, 1, "暴恐识别：血腥");
        table.put(2, 2, "暴恐识别：尸体");
        table.put(2, 3, "暴恐识别：爆炸火灾");
        table.put(2, 4, "暴恐识别：杀人");
        table.put(2, 5, "暴恐识别：暴乱");
        table.put(2, 6, "暴恐识别：暴恐人物");
        table.put(2, 7, "暴恐识别：军事武器");
        table.put(2, 8, "暴恐识别：暴恐旗帜");
        table.put(2, 9, "暴恐识别：血腥动物或动物尸体");
        table.put(2, 10, "暴恐识别：车祸");
        table.put(2, 11, "暴恐识别：枪械");
        table.put(2, 12, "暴恐识别：弹药");
        table.put(2, 13, "暴恐识别：刀具");
        table.put(2, 14, "暴恐识别：武装人员");
        table.put(2, 15, "暴恐识别：特殊服饰");
        table.put(3, 0, "恶心图识别：恶心图");
        table.put(3, 1, "恶心图识别：性器官特写");
        table.put(3, 2, "恶心图识别：脏器");
        table.put(3, 3, "恶心图识别：疾病表症");
        table.put(3, 4, "恶心图识别：密集恐惧症");
        table.put(3, 5, "恶心图识别：腐烂食物");
        table.put(3, 6, "恶心图识别：排泄物");
        table.put(3, 7, "恶心图识别：恶心动物");
        table.put(3, 8, "恶心图识别：人体血腥和尸体");
        table.put(3, 9, "恶心图识别：动物血腥及尸体");
        table.put(4, 0, "广告检测：水印");
        table.put(4, 1, "广告检测：二维码");
        table.put(4, 2, "广告检测：条形码");
        table.put(4, 3, "广告检测：识别二维码中内容");
        table.put(4, 4, "广告检测：识别条形码中内容");
        table.put(4, 5, "广告检测：不过滤字幕");
        table.put(4, 6, "广告检测：小程序码");
        table.put(4, 7, "广告检测：水印自定义黑名单");
        table.put(4, 8, "广告检测：水印自定义白名单");
        table.put(5, 0, "政治敏感识别：政治敏感");
        table.put(5, 1, "政治敏感识别：公众人物");
        table.put(5, 2, "政治敏感识别：自定义敏感人物");
        table.put(6, 0, "图像质量检测：图像清晰度");
        table.put(6, 1, "图像质量检测：图像美观度");
        table.put(7, 0, "用户图像黑名单：用户自定义图像黑名单");
        table.put(8, 0, "用户图像白名单：用户自定义图像白名单");
        table.put(10, 0, "用户头像审核：图像中必须是真人脸");
        table.put(10, 1, "用户头像审核：人脸必须为正脸");
        table.put(10, 2, "用户头像审核：左右旋转角度");
        table.put(10, 3, "用户头像审核：俯仰角度");
        table.put(10, 4, "用户头像审核：歪头角度");
        table.put(10, 5, "用户头像审核：人脸不能有遮挡");
        table.put(10, 6, "用户头像审核：不能遮挡眼睛");
        table.put(10, 7, "用户头像审核：不能遮挡鼻子");
        table.put(10, 8, "用户头像审核：不能遮挡嘴");
        table.put(10, 9, "用户头像审核：不能遮挡下巴");
        table.put(10, 10, "用户头像审核：不能遮挡脸颊");
        table.put(10, 11, "用户头像审核：人脸不能佩戴墨镜");
        table.put(10, 12, "用户头像审核：人脸占比");
        table.put(10, 13, "用户头像审核：人脸必须清晰");
        table.put(10, 15, "用户头像审核：人脸为女性");
        table.put(10, 16, "用户头像审核：人脸为男性");
        table.put(10, 17, "用户头像审核：人脸为未成年");
        table.put(11, 0, "百度官方违禁词库：百度官方默认违禁词库");
        table.put(12, 0, "图文审核：低质灌水");
        table.put(12, 1, "图文审核：暴恐违禁");
        table.put(12, 2, "图文审核：文本色情");
        table.put(12, 3, "图文审核：政治敏感");
        table.put(12, 4, "图文审核：恶意推广");
        table.put(12, 5, "图文审核：低俗辱骂");
        table.put(12, 6, "图文审核：恶意推广 - 联系方式");
        table.put(12, 7, "图文审核：恶意推广 - 软文推广");
        table.put(12, 8, "图文审核：广告法审核");
        table.put(13, 0, "自定义文本黑名单：自定义文本黑名单");
        table.put(14, 0, "自定义文本白名单：自定义文本白名单");
        table.put(15, 0, "EasyDL自定义模型:EasyDL自定义模型");
        table.put(16, 0, "敏感旗帜标志识别：中国国旗及类似图形");
        table.put(16, 1, "敏感旗帜标志识别：中国地图及类似图形");
        table.put(16, 2, "敏感旗帜标志识别：党旗、军旗、党徽及类似图形");
        table.put(16, 3, "敏感旗帜标志识别：警徽及类似图形");
        table.put(16, 4, "敏感旗帜标志识别：各类臂章");
        table.put(16, 5, "敏感旗帜标志识别：反动组织旗帜、徽章、标志");
        table.put(21, 1, "不良场景识别：真人吸烟");
        table.put(21, 2, "不良场景识别：卡通吸烟");
        table.put(21, 3, "不良场景识别：毒品");
        table.put(21, 4, "不良场景识别：真人饮酒");
        table.put(21, 5, "不良场景识别：卡通饮酒");
        table.put(21, 6, "不良场景识别：赌博");
        table.put(21, 7, "不良场景识别：纹身");
        table.put(21, 9, "不良场景识别：竖中指");
        table.put(24, 0, "直播场景审核：卫生间场景");
        table.put(24, 1, "直播场景审核：车内场景");
        table.put(24, 2, "直播场景审核：卧室场景");
        table.put(24, 3, "直播场景审核：无意义场景");
        table.put(24, 4, "直播场景审核：普通场景");
    }

    // https://ai.baidu.com/ai-doc/ANTIPORN/Iki010pnh
    static {
        errMessage.put(216100, "非法请求,请检查参数后重新尝试");
        errMessage.put(216101, "缺少必要参数");
        errMessage.put(216102, "请求了不支持的服务，请检查调用的url");
        errMessage.put(216103, "请求中某些参数过长，请检查后重新尝试");
        errMessage.put(216110, "appId不存在");
        errMessage.put(216200, "图片不能为空");
        errMessage.put(216201, "图片格式错误仅支持:PNG、JPG、JPEG、BMP");
        errMessage.put(216202, "图片过大或者过小");
        errMessage.put(216630, "API服务发生异常");
        errMessage.put(216631, "图片无法识别");
        errMessage.put(216633, "图片无法识别");
        errMessage.put(216634, "API服务发生异常");
        errMessage.put(282000, "API服务发生异常");
        errMessage.put(282003, "请求参数缺失");
        errMessage.put(282005, "处理批量任务时发生部分或全部错误");
        errMessage.put(282006, "处理批量任务超过限制");
        errMessage.put(282100, "图片转换错误");
        errMessage.put(282102, "图片模糊");
        errMessage.put(282103, "图片模糊");
        errMessage.put(282810, "API服务发生异常");
    }

    /**
     * 获取百度 token
     *
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO getToken() {
        return baiduAuditTokenCache.optional().orElseGet(this::refreshToken);
    }

    /**
     * 刷新百度 token 并写入缓存
     *
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO refreshToken() {
        final ResponseEntity<BaiduTokenVO> responseEntity = baiduTokenApiService
                .call(
                        GRANT_TYPE,
                        apiKey,
                        secretKey
                );
        Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
        final BaiduTokenVO token = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为 null")
                .initCacheTime();
        token.assertErrorCode();
        baiduAuditTokenCache.set(token);
        if (log.isInfoEnabled()) {
            log.info("百度 token： {}", token);
        }
        return token;
    }

    /**
     * 百度文本内容审核
     *
     * @param text {@link String}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditText(final String text) {
        try {
            final ResponseEntity<BaiduAuditVO> responseEntity = baiduAuditTextApiService.call(
                    getToken().getAccess_token(),
                    new LinkedMultiValueMap<String, String>() {{
                        put("text", Lists.newArrayList(text));
                    }}
            );
            if (log.isDebugEnabled()) {
                log.debug("{}", responseEntity.getBody());
            }
            Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                    () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
            final BaiduAuditVO vo = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为null");
            if (vo.success()) {
                if (vo.auditSuccess()) {
                    return "";
                }
                final String errMsg = Optional.ofNullable(vo.getData())
                        .orElseGet(Lists::newArrayList)
                        .stream()
                        .map(row -> {
                            final String message = Optional
                                    .ofNullable(row.getHits())
                                    .map(hits -> hits.stream()
                                            .map(BaiduAuditVO.DataBean.HitsBean::getWords)
                                            .flatMap(Stream::of)
                                            .collect(Collectors.joining("、"))
                                    ).orElse("");
                            return String.format("%s：%s：%s",
                                    Objects.toString(table.get(row.getType(), row.getSubType()), ""),
                                    row.getMsg(),
                                    message
                            );
                        })
                        .collect(Collectors.joining("；"));
                log.error(errMsg);
                return errMsg;
            } else {
                final String errMsg = String.format("百度内容审核，%d：%s", vo.getErrorCode(), vo.getErrorMsg());
                log.error(errMsg);
                return errMsg;
            }
        } catch (Exception e) {
            final String errMsg = String.format("审核异常，等待人工审核：%s", e.getMessage());
            log.error(errMsg, e);
            return errMsg;
        }
    }

    /**
     * 百度图片内容审核
     *
     * @param dto {@link BaiduAuditImageDTO}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditImage(final BaiduAuditImageDTO dto) {
        try {
            final ResponseEntity<BaiduAuditVO> responseEntity = baiduAuditImageApiService.call(
                    getToken().getAccess_token(),
                    new LinkedMultiValueMap<String, Object>() {{
                        put("imgType", Lists.newArrayList(0));
                        put("image", Lists.newArrayList(dto.getImage()));
                        put("imgUrl", Lists.newArrayList(dto.getImgUrl()));
                    }}
            );
            if (log.isDebugEnabled()) {
                log.debug("{}", responseEntity.getBody());
            }
            Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                    () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
            final BaiduAuditVO vo = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为null");
            if (vo.success()) {
                if (vo.auditSuccess()) {
                    return "";
                }
                final String errMsg = Optional.ofNullable(vo.getData())
                        .orElseGet(Lists::newArrayList)
                        .stream()
                        .map(row -> {
                            final String message = Optional
                                    .ofNullable(row.getHits())
                                    .map(hits -> hits.stream()
                                            .map(BaiduAuditVO.DataBean.HitsBean::getWords)
                                            .flatMap(Stream::of)
                                            .collect(Collectors.joining("、"))
                                    ).orElse("");
                            return String.format("%s：%s：%s",
                                    Objects.toString(table.get(row.getType(), row.getSubType()), ""),
                                    row.getMsg(),
                                    message
                            );
                        })
                        .collect(Collectors.joining("；"));
                log.error(errMsg);
                return errMessage.getOrDefault(vo.getErrorCode(), errMsg);
            } else {
                final String errMsg = String.format("百度内容审核，%d：%s", vo.getErrorCode(), vo.getErrorMsg());
                log.error(errMsg);
                return errMessage.getOrDefault(vo.getErrorCode(), errMsg);
            }
        } catch (Exception e) {
            final String errMsg = String.format("审核异常，等待人工审核：%s", e.getMessage());
            log.error(errMsg, e);
            return errMsg;
        }
    }

    /**
     * 百度音频内容审核
     *
     * @param dto {@link BaiduAuditAudioDTO}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditAudio(final BaiduAuditAudioDTO dto) {
        try {
            final ResponseEntity<BaiduAuditAudioVO> responseEntity = baiduAuditAudioApiService.call(
                    getToken().getAccess_token(),
                    new LinkedMultiValueMap<String, Object>() {{
                        put("url", Lists.newArrayList(dto.getUrl()));
                        put("base64", Lists.newArrayList(dto.getBase64()));
                        put("fmt", Lists.newArrayList(dto.getFmt()));
                        put("rate", Lists.newArrayList(dto.getRate()));
                    }}
            );
            if (log.isDebugEnabled()) {
                log.debug("{}", responseEntity.getBody());
            }
            Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                    () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
            final BaiduAuditAudioVO vo = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为null");
            if (vo.success()) {
                if (vo.auditSuccess()) {
                    return "";
                }
                final String errMsg = Optional.ofNullable(vo.getData())
                        .orElseGet(Lists::newArrayList)
                        .stream()
                        .filter(row -> CollectionUtils.isNotEmpty(row.getAuditData()))
                        .flatMap(row -> row.getAuditData().stream())
                        .map(row -> {
                            final String message = Optional
                                    .ofNullable(row.getHits())
                                    .map(hits -> hits.stream()
                                            .map(BaiduAuditVO.DataBean.HitsBean::getWords)
                                            .flatMap(Stream::of)
                                            .collect(Collectors.joining("、"))
                                    ).orElse("");
                            return String.format("%s：%s：%s",
                                    Objects.toString(table.get(row.getType(), row.getSubType()), ""),
                                    row.getMsg(),
                                    message
                            );
                        })
                        .collect(Collectors.joining("；"));
                log.error(errMsg);
                return errMessage.getOrDefault(vo.getErrorCode(), errMsg);
            } else {
                final String errMsg = String.format("百度内容审核，%d：%s", vo.getErrorCode(), vo.getErrorMsg());
                log.error(errMsg);
                return errMessage.getOrDefault(vo.getErrorCode(), errMsg);
            }
        } catch (Exception e) {
            final String errMsg = String.format("审核异常，等待人工审核：%s", e.getMessage());
            log.error(errMsg, e);
            return errMsg;
        }
    }
}
