package com.ccx.demo.config.encrypt.convert;

import com.ccx.demo.config.DOEncryptConfiguration;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *    @JsonSerialize(converter = AesIdLongListJsonConvert.Serializer.class)
 *    @JsonDeserialize(converter = AesIdLongListJsonConvert.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesIdLongListJsonConvert {

    /**
     * id 反序列化
     */
    public static class Serializer extends StdConverter<List<Long>, List<String>> {

        @Override
        public List<String> convert(List<Long> value) {
            if (CollectionUtils.isEmpty(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", value);
            }
            return DOEncryptConfiguration
                    .encryptIds(value)
                    .collect(Collectors.toList())
                    ;
        }
    }

    /**
     * id 反序列化
     */
    public static class Deserializer extends StdConverter<List<String>, List<Long>> {

        @Override
        public List<Long> convert(List<String> value) {
            if (CollectionUtils.isEmpty(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", value);
            }
            return DOEncryptConfiguration
                    .decryptLongId(value)
                    .collect(Collectors.toList())
                    ;
        }
    }

}
