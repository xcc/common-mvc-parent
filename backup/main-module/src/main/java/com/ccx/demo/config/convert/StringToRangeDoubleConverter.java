package com.ccx.demo.config.convert;

import com.common.util.JSON;
import com.common.util.Range;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToRangeDoubleConverter implements Converter<String, Range<Double>> {
    @Override
    public Range<Double> convert(String value) {
        return JSON.parse(value, new TypeReference<Range<Double>>() {
        });
    }
}
