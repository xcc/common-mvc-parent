package com.ccx.demo.aop;

import java.lang.annotation.*;

/**
 * 请求埋点注解
 *
 * @author 谢长春 2022-06-28
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AppEventRequest {

    /**
     * 是否跳过该接口
     *
     * @return boolean
     */
    boolean skip() default false;

    /**
     * 是否忽略用户id
     *
     * @return boolean
     */
    boolean ignoreUserId() default true;

}
