package com.ccx.demo.open.auth.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 实体：登录参数
 *
 * @author 谢长春
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
@ApiModel(description = "手机验证码登录参数")
public class PhoneCodeLoginDTO implements Serializable {
    /**
     * 手机号，CODE 模式手机号和验证码必填
     */
    @ApiModelProperty(position = 4, value = "手机号，CODE 模式手机号和验证码必填", example = "18700000000")
    private String phone;
    /**
     * 验证码，CODE 模式手机号和验证码必填
     */
    @ApiModelProperty(position = 5, value = "验证码，CODE 模式手机号和验证码必填", example = "1234")
    private String code;
//
//    public Method getMethod() {
//        if (Objects.isNull(method)) {
//            return Method.SESSION; // 若未指定会话模式，则默认为SESSION模式
//        }
//        return method;
//    }
}
