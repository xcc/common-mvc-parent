package com.ccx.demo.business.common.dto;

import com.ccx.demo.business.common.entity.TabErrorLog;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * DTO：异常日志记录新增
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义异常日志记录新增时的扩展属性
 *
 * @author 谢长春 on 2022-02-16 V20220709
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "异常日志记录")
public class TabErrorLogInsertDTO extends TabErrorLog {
    private static final long serialVersionUID = 1L;


    @NotBlank
    @Override
    public String getTraceId() {
        return super.getTraceId();
    }
}
