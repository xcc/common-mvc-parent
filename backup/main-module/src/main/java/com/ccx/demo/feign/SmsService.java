package com.ccx.demo.feign;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Set;

/**
 * 服务接口：短信发送
 *
 * @author 谢长春 2022-04-10
 */
public interface SmsService {
    /**
     * 发送短信
     *
     * @param phone   String 手机号
     * @param content String 发送内容
     */
    void send(final String phone, final String content);

    /**
     * 发送短信
     *
     * @param phoneList String 手机号
     * @param content   String 发送内容
     */
    default void send(final Set<String> phoneList, final String content) {
        Lists.partition(new ArrayList<>(phoneList), 99)
                .forEach(list -> send(String.join(",", list), content));
    }

    String SEND_PHONE_CODE = "尊敬的用户，您正在使用短信验证服务，您的验证码是： %s ，请在5分钟内完成验证";

    /**
     * 发送短信验证码
     *
     * @param phone String 手机号
     * @param code  String 验证码
     */
    default void sendPhoneCode(final String phone, final String code) {
        send(phone, String.format(SEND_PHONE_CODE, code));
    }

}
