package com.ccx.demo.config;

import io.swagger.annotations.Api;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;

/**
 * <pre>
 * swagger 增强版配置
 * 官方文档： https://doc.xiaominfo.com
 * demo： https://gitee.com/xiaoym/swagger-bootstrap-ui-demo.git
 * </pre>
 */
@ConditionalOnProperty(value = "spring.app.swaggerEnabled", havingValue = "true")
@Configuration(proxyBeanMethods = false)
@EnableOpenApi
//@Import(BeanValidatorPluginsConfiguration.class)
@AutoConfigureAfter(AppConfiguration.class)
//@RequiredArgsConstructor
public class SwaggerConfiguration {
    //    private final ServletContext servletContext;
//@Bean(value = "defaultApi1")
//public Docket defaultApi1() {
//    //List<SecurityScheme> securitySchemes=Arrays.asList(new ApiKey("Authorization", "Authorization", "header"));
//    List<SecurityScheme> securitySchemes=new ArrayList<>();
//
//    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
//    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//    authorizationScopes[0] = authorizationScope;
//
//    List<SecurityContext> securityContexts= Arrays.asList(SecurityContext.builder()
//            .securityReferences(CollectionUtil.newArrayList(new SecurityReference("Authorization", authorizationScopes)))
//            .forPaths(PathSelectors.regex("/.*"))
//            .build());
//    HttpAuthenticationScheme httpAuthenticationScheme = HttpAuthenticationScheme.JWT_BEARER_BUILDER
//            .name(HttpHeaders.AUTHORIZATION)
//            .description("Bearer Token")
//            .build();
//    securitySchemes.add(httpAuthenticationScheme);
//
//    //默认全局参数
//    List<RequestParameter> requestParameters=new ArrayList<>();
//    requestParameters.add(new RequestParameterBuilder().name("test").description("测试").in(ParameterType.QUERY).required(true).build());
//
//    Docket docket=new Docket(DocumentationType.OAS_30)
//            .apiInfo(apiInfo())
//            //分组名称
//            .groupName("2")
//            .select()
//            //这里指定Controller扫描包路径
//            .apis(RequestHandlerSelectors.basePackage("com.xiaominfo.knife4j.new2"))
//            .paths(PathSelectors.any())
//            .build()
//            //.globalRequestParameters(requestParameters)
//            //.extensions(openApiExtensionResolver.buildExtensions("1.2.x"))
//            //.extensions(openApiExtensionResolver.buildSettingExtensions())
//            .securityContexts(securityContexts).securitySchemes(securitySchemes);
//    return docket;
//}
    @Bean(value = "allApi")
    public Docket allApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("全部接口")
//                .host("https://app-demo.cccc6666.com/")
//                .pathProvider(new RelativePathProvider(servletContext) {
//                    @Override
//                    public String getApplicationBasePath() {
//                        return "/api";
//                    }
//                })
                .apiInfo(new ApiInfoBuilder()
                        .version("1.0")
                        .title("全部接口")
                        .description("### Swagger RESTful APIs 增强版， 集成指南：doc.xiaominfo.com")
                        .termsOfServiceUrl("https://app-demo.cccc6666.com")
                        .contact(new Contact("谢长春", "https://app-demo.cccc6666.com", "403368945@qq.com"))
                        .build()
                )
                .select()
                // 生成所有API接口
//                .apis(RequestHandlerSelectors.basePackage("com.ccx.demo.open.common.web"))
                // 只生成被Api这个注解注解过的类接口
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                // 只生成被ApiOperation这个注解注解过的api接口
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .directModelSubstitute(Timestamp.class, String.class)
                .directModelSubstitute(Date.class, String.class)
                .globalRequestParameters(Collections.singletonList(new RequestParameterBuilder()
                        .name("x-token")
                        .description("用户认证token，登录之后从响应头获取")
                        .in(ParameterType.HEADER)
                        .required(true)
                        .build()
                ))
//                .ignoredParameterTypes()
//                .globalOperationParameters(Lists.newArrayList(parameterBuilder
//                                .name("token")
//                                .description("token 令牌")
//                                .modelRef(new ModelRef("String"))
//                                .parameterType("header")
//                                .required(true)
//                                .build()
//                        )
//                )
//                .securityContexts(Lists.newArrayList(
//                        SecurityContext.builder()
//                                .securityReferences(defaultAuth())
//                                .forPaths(PathSelectors.regex("/.*"))
//                                .build()
//                ))
//                .securitySchemes(Lists.<SecurityScheme>newArrayList(new ApiKey("BearerToken", "Authorization", "header")))
                ;
    }

//    @Bean(value = "allApi")
//    public Docket allApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("全部接口")
////                .host("https://app-demo.cccc6666.com/")
////                .pathProvider(new RelativePathProvider(servletContext) {
////                    @Override
////                    public String getApplicationBasePath() {
////                        return "/api";
////                    }
////                })
//                .apiInfo(new ApiInfoBuilder()
//                        .version("1.0")
//                        .title("全部接口")
//                        .description("### Swagger RESTful APIs 增强版， 集成指南：doc.xiaominfo.com")
//                        .termsOfServiceUrl("https://app-demo.cccc6666.com")
//                        .contact(new Contact("谢长春", "https://app-demo.cccc6666.com", "403368945@qq.com"))
//                        .build()
//                )
//                .select()
//                // 生成所有API接口
////                .apis(RequestHandlerSelectors.basePackage("com.ccx.demo.open.common.web"))
//                // 只生成被Api这个注解注解过的类接口
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//                // 只生成被ApiOperation这个注解注解过的api接口
//                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
//                .paths(PathSelectors.any())
//                .build()
//                .directModelSubstitute(Timestamp.class, String.class)
//                .directModelSubstitute(Date.class, String.class)
////                .ignoredParameterTypes()
////                .globalOperationParameters(Lists.newArrayList(parameterBuilder
////                                .name("token")
////                                .description("token 令牌")
////                                .modelRef(new ModelRef("String"))
////                                .parameterType("header")
////                                .required(true)
////                                .build()
////                        )
////                )
////                .securityContexts(Lists.newArrayList(
////                        SecurityContext.builder()
////                                .securityReferences(defaultAuth())
////                                .forPaths(PathSelectors.regex("/.*"))
////                                .build()
////                ))
////                .securitySchemes(Lists.<SecurityScheme>newArrayList(new ApiKey("BearerToken", "Authorization", "header")))
//                ;
//    }
//
//    @Bean(value = "openApi")
//    public Docket openApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("开放接口")
//                .apiInfo(new ApiInfoBuilder()
//                        .version("1.0")
//                        .title("开放接口")
//                        .description("### Swagger RESTful APIs 增强版， 集成指南：doc.xiaominfo.com")
//                        .termsOfServiceUrl("https://app-demo.cccc6666.com/")
//                        .contact(new Contact("谢长春", "app-demo.cccc6666.com", "403368945@qq.com"))
//                        .build()
//                )
//                .select()
//                // 生成所有API接口
//                .apis(RequestHandlerSelectors.basePackage("com.ccx.demo.open")) // 这里不支持通配符
//                // 只生成被Api这个注解注解过的类接口
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//                // 只生成被ApiOperation这个注解注解过的api接口
//                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
//                .paths(PathSelectors.any())
//                .build()
//                .directModelSubstitute(Timestamp.class, String.class)
//                .directModelSubstitute(Date.class, String.class)
//                ;
//    }
//
//    @Bean(value = "appApi")
//    public Docket appApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("IOS Android")
//                .apiInfo(new ApiInfoBuilder()
//                        .version("1.0")
//                        .title("IOS Android 接口")
//                        .description("### Swagger RESTful APIs 增强版， 集成指南：doc.xiaominfo.com")
//                        .termsOfServiceUrl("https://app-demo.cccc6666.com/")
//                        .contact(new Contact("谢长春", "app-demo.cccc6666.com", "403368945@qq.com"))
//                        .build()
//                )
//                .select()
//                // 生成所有API接口
//                .apis(RequestHandlerSelectors.basePackage("com.ccx.demo")) // 这里不支持通配符
//                // 只生成被Api这个注解注解过的类接口
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//                // 只生成被AppSwaggerGroup这个注解注解过的api接口
//                .apis(RequestHandlerSelectors.withMethodAnnotation(AppSwaggerGroup.class))
//                .paths(PathSelectors.any())
//                .build()
//                .directModelSubstitute(Timestamp.class, String.class)
//                .directModelSubstitute(Date.class, String.class)
//                ;
//    }
//
//    @Bean(value = "vueApi")
//    public Docket vueApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("PC web 端接口")
//                .apiInfo(new ApiInfoBuilder()
//                        .version("1.0")
//                        .title("PC web 端接口")
//                        .description("### Swagger RESTful APIs 增强版， 集成指南：doc.xiaominfo.com")
//                        .termsOfServiceUrl("https://app-demo.cccc6666.com/")
//                        .contact(new Contact("谢长春", "app-demo.cccc6666.com", "403368945@qq.com"))
//                        .build()
//                )
//                .select()
//                // 生成所有API接口
//                .apis(RequestHandlerSelectors.basePackage("com.ccx.demo")) // 这里不支持通配符
//                // 只生成被Api这个注解注解过的类接口
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//                // 只生成被AppSwaggerGroup这个注解注解过的api接口
//                .apis(RequestHandlerSelectors.withMethodAnnotation(VueSwaggerGroup.class))
//                .paths(PathSelectors.any())
//                .build()
//                .directModelSubstitute(Timestamp.class, String.class)
//                .directModelSubstitute(Date.class, String.class)
//                ;
//    }
//
//    @Bean(value = "wechatApi")
//    public Docket wechatApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("微信端接口")
//                .apiInfo(new ApiInfoBuilder()
//                        .version("1.0")
//                        .title("微信端接口")
//                        .description("### Swagger RESTful APIs 增强版， 集成指南：doc.xiaominfo.com")
//                        .termsOfServiceUrl("https://app-demo.cccc6666.com/")
//                        .contact(new Contact("谢长春", "app-demo.cccc6666.com", "403368945@qq.com"))
//                        .build()
//                )
//                .select()
//                // 生成所有API接口
//                .apis(RequestHandlerSelectors.basePackage("com.ccx.demo")) // 这里不支持通配符
//                // 只生成被Api这个注解注解过的类接口
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//                // 只生成被AppSwaggerGroup这个注解注解过的api接口
//                .apis(RequestHandlerSelectors.withMethodAnnotation(WechatSwaggerGroup.class))
//                .paths(PathSelectors.any())
//                .build()
//                .directModelSubstitute(Timestamp.class, String.class)
//                .directModelSubstitute(Date.class, String.class)
//                ;
//    }

}
