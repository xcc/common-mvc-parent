package com.ccx.demo.feign.baidu.api;

import com.ccx.demo.feign.baidu.vo.BaiduTokenVO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 百度 api token 获取
 * https://ai.baidu.com/ai-doc/REFERENCE/Ck3dwjhhu
 *
 * @author 谢长春 2022-04-09
 */
@ConditionalOnProperty(value = "spring.app.feign.baidu.token-api")
@FeignClient(name = "baiduTokenApiService", url = "${spring.app.feign.baidu.token-api}")
public interface BaiduTokenApiService {
    /**
     * 百度 api token 获取参数
     * <pre>
     * {
     *   "refresh_token": "25.b55fe1d287227ca97aab219bb249b8ab.315360000.1798284651.282335-8574074",
     *   "expires_in": 2592000,
     *   "scope": "public wise_adapt",
     *   "session_key": "9mzdDZXu3dENdFZQurfg0Vz8slgSgvvOAUebNFzyzcpQ5EnbxbF+hfG9DQkpUVQdh4p6HbQcAiz5RmuBAja1JJGgIdJI",
     *   "access_token": "24.6c5e1ff107f0e8bcef8c46d3424a0e78.2592000.1485516651.282335-8574074",
     *   "session_secret": "dfac94a3489fe9fca7c3221cbf7525ff"
     * }
     * </pre>
     *
     * @param grantType    {@link String} grant_type： 必须参数，固定为client_credentials；
     * @param clientId     {@link String} client_id： 必须参数，应用的API Key；
     * @param clientSecret {@link String} client_secret： 必须参数，应用的Secret Key；
     * @return {@link BaiduTokenVO}
     */
    @PostMapping("/token")
    ResponseEntity<BaiduTokenVO> call(@RequestParam("grant_type") String grantType, @RequestParam("client_id") String clientId, @RequestParam("client_secret") String clientSecret);
}
