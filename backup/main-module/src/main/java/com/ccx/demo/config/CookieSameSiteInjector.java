package com.ccx.demo.config;

//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.context.event.EventListener;
//import org.springframework.session.web.http.DefaultCookieSerializer;
//import org.springframework.stereotype.Component;
//
///**
// * https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/Set-Cookie/SameSite
// * https://stackoverflow.com/questions/58386069/how-to-set-same-site-cookie-flag-in-spring-boot/58487733#58487733
// * https://docs.spring.io/spring-security/site/docs/5.2.8.RELEASE/reference/html/features.html
// */
//@Slf4j
//@Component
//@AllArgsConstructor
//public class CookieSameSiteInjector {
//
//    private final ApplicationContext applicationContext;
//
//    @EventListener
//    public void onApplicationEvent(ContextRefreshedEvent event) {
//        DefaultCookieSerializer cookieSerializer = applicationContext.getBean(DefaultCookieSerializer.class);
//        cookieSerializer.setSameSite("none");
//    }
//
//    @Bean
//    public CookieSerializer httpSessionIdResolver() {
//        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
//        cookieSerializer.setSameSite(null);
//        return cookieSerializer;
//    }
//    @Bean
//    @ConfigurationProperties("spring.session.cookie")
//    public DefaultCookieSerializer cookieSerializer()  {
//        return new DefaultCookieSerializer();
//    }
//}
