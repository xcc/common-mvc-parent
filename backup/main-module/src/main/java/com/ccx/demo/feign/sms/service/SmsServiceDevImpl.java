package com.ccx.demo.feign.sms.service;

import com.ccx.demo.feign.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 服务接口实现类：短信发送
 *
 * @author 谢长春 2022-04-10
 */
@Slf4j
@Service
public class SmsServiceDevImpl implements SmsService {
    @Override
    public void send(String phone, String content) {
        log.warn("spring.app.verifyCodeEnabled => spring.app.feign.sms.enabled ： 短信验证码开关未打开");
    }
}
