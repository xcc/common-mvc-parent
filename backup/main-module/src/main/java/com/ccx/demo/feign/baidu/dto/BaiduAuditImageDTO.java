package com.ccx.demo.feign.baidu.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 内容审核平台-图像接口 参数
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("内容审核平台-图像接口")
public class BaiduAuditImageDTO {
    /**
     * 图片类型0:静态图片（PNG、JPG、JPEG、BMP、GIF（仅对首帧进行审核）、Webp、TIFF），1:GIF动态图片
     */
    @ApiModelProperty(value = "图片类型", position = 1)
    private int imgType;
    /**
     * image
     * string
     * N（和imgUrl二选一）
     * 待审核图像Base64编码字符串， 以图像文件形式请求时必填， 图像要求base64后 大于等于5kb，小于等于4M， 最短边大于等于128像素， 小于等于4096像素， 支持的图片格式：PNG、JPG、JPEG、BMP、GIF（仅对首帧进行审核）、Webp、TIFF
     */
    @ApiModelProperty(value = "图片 base64", position = 2)
    private String image;
    /**
     * imgUrl
     * string
     * N （和image二选一）
     * 图像URL地址， 以URL形式请求， 图像Url需要做UrlEncode， 图像要求base64后大于等于5kb， 小于等于4M， 最短边大于等于128像素， 小于等于4096像素 支持的图片格式：PNG、JPG、JPEG、BMP、GIF（仅对首帧进行审核）、Webp、TIFF
     */
    @ApiModelProperty(value = "图片地址", position = 3)
    private String imgUrl;
}
