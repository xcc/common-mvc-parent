package com.ccx.demo.feign.baidu.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.List;
import java.util.Objects;

/**
 * 百度内容审核
 * {"conclusion": "不合规", "log_id": 16045435522725277, "data": [{"msg": "存在低俗辱骂不合规", "conclusion": "不合规", "hits": [{"probability": 1.0, "datasetName": "百度默认文本反作弊库", "words": ["草泥马"]}], "subType": 5, "conclusionType": 2, "type": 12}], "conclusionType": 2}
 *
 * @author 谢长春 2022-04-09
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel("百度内容审核")
public class BaiduAuditVO extends BaiduBaseVO {

    /**
     * 日志id
     */
    @JsonProperty("log_id")
    private String logId;
    /**
     * 审核结果，可取值描述：合规、不合规、疑似、审核失败
     */
    private String conclusion;
    /**
     * 审核结果类型，可取值1、2、3、4，分别代表1：合规，2：不合规，3：疑似，4：审核失败
     */
    private Integer conclusionType;
    /**
     * 不合规/疑似/命中白名单项详细信息。响应成功并且conclusion为疑似或不合规或命中白名单时才返回，响应失败或conclusion为合规且未命中白名单时不返回。
     */
    private List<DataBean> data;

    @Override
    public boolean success() {
        return Objects.isNull(getErrorCode());
    }

    public boolean auditSuccess() {
        return Objects.equals(1, conclusionType);
    }

    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class DataBean {
        /**
         * 内层错误提示码，底层服务失败才返回，成功不返回
         */
        @JsonProperty("error_code")
        private Integer errorCode;
        /**
         * 内层错误提示信息，底层服务失败才返回，成功不返回
         */
        @JsonProperty("error_msg")
        private String errorMsg;
        /**
         * 结果具体命中的模型：0:百度官方违禁图库、1：色情识别、2：暴恐识别、3：恶心图识别、4:广告检测、5：政治敏感识别、6：图像质量检测、7：用户图像黑名单、8：用户图像白名单、10：用户头像审核、11：百度官方违禁词库、12：图文审核、13:自定义文本黑名单、14:自定义文本白名单、15:EasyDL自定义模型、16：敏感旗帜标志识别、21：不良场景识别
         */
        private Integer type;
        /**
         * 审核子类型，此字段需参照type主类型字段决定其含义：
         */
        private int subType;
        /**
         * 不合规项描述信息
         */
        private String msg;
        /**
         * 不合规项置信度
         */
        private Float probability;
        /**
         * 违规项目所属数据集名称
         */
        private Float datasetName;
        /**
         * 审核结果，可取值描述：合规、不合规、疑似、审核失败
         */
        private String conclusion;
        /**
         * 审核结果类型，可取值1、2、3、4，分别代表1：合规，2：不合规，3：疑似，4：审核失败
         */
        private Integer conclusionType;
        /**
         * 每个type可能会调用多个底层服务，此处可能有多个结果
         */
        private String results;
        /**
         * 命中关键词信息
         */
        private List<HitsBean> hits;

        @NoArgsConstructor
        @Getter
        @Setter
        @ToString
        public static class HitsBean {
            private Float score;
            private Float probability;
            private String datasetName;
            private String[] words;
        }
    }
}
