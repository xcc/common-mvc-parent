package com.ccx.demo.config.convert;

import com.common.util.Dates;
import com.common.util.JSON;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToDateRangeConverter implements Converter<String, Dates.Range> {
    @Override
    public Dates.Range convert(String value) {
        return JSON.parseObject(value, Dates.Range.class);
    }
}
