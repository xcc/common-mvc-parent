package com.ccx.demo.feign.baidu.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 公安验证 结果
 * {"error_code":0,"error_msg":"SUCCESS","log_id":6575001650575,"timestamp":1603857179,"cached":0,"result":{"score":100}}
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel("百度人脸识别")
public class BaiduPersonVerifyVO extends BaiduBaseVO {

    /**
     * 日志id
     */
    @JsonProperty("log_id")
    private String logId;
    /**
     * 与公安小图相似度可能性，用于验证生活照与公安小图是否为同一人，有正常分数时为[0~100]，推荐阈值80，超过即判断为同一人
     */
    @ApiModelProperty(value = "与公安小图相似度可能性，用于验证生活照与公安小图是否为同一人，有正常分数时为[0~100]，推荐阈值80，超过即判断为同一人")
    private Map<String, Object> result;

    public Optional<Float> getScore() {
        return Optional.ofNullable(result).filter(map -> map.containsKey("score")).map(res -> Float.parseFloat(Objects.toString(res.get("score"))));
    }

    public boolean scoreSuccess() {
        return getScore().map(score -> score >= 80f).orElse(false);
    }
}
