package com.ccx.demo.feign.baidu.service;

import com.ccx.demo.feign.AuditTextService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * 文字审核
 *
 * @author 谢长春 2022-04-11
 */
@Primary
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.feign.baidu.audit.enabled", havingValue = "true")
public class AuditTextServiceProdImpl implements AuditTextService {
    private final BaiduAuditService baiduAuditService;

    /**
     * 文本审核
     *
     * @param text {@link String}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditText(final String text) {
        return baiduAuditService.auditText(text);
    }
}
