package com.ccx.demo.feign.baidu.api;

import com.ccx.demo.feign.baidu.vo.BaiduAuditVO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 百度内容审核平台：文本审核
 * https://ai.baidu.com/ai-doc/ANTIPORN/Vk3h6xaga
 * 计费：https://ai.baidu.com/ai-doc/ANTIPORN/lk3h6x7if
 *
 * @author 谢长春 2022-04-09
 */
@ConditionalOnProperty(value = "spring.app.feign.baidu.audit.enabled", havingValue = "true")
@FeignClient(name = "baiduAuditTextApiService", url = "${spring.app.feign.baidu.audit.api}")
public interface BaiduAuditTextApiService {

    /**
     * 百度内容审核平台：文本审核
     * https://ai.baidu.com/ai-doc/ANTIPORN/Rk3h6xb3i
     *
     * @param accessToken {@link String}
     * @param dto         {@link MultiValueMap<String, String>}
     * @return {@link ResponseEntity<BaiduAuditVO>}
     */
    @PostMapping(value = "/text_censor/v2/user_defined", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<BaiduAuditVO> call(
            @RequestParam("access_token") final String accessToken,
            MultiValueMap<String, String> dto
    );
}
