package com.ccx.demo.business.common.controller;

import com.ccx.demo.config.init.AppConfig;
import com.ccx.demo.config.init.AppConfig.Path;
import com.ccx.demo.config.init.AppConfig.URL;
import com.common.db.IUser;
import com.common.db.entity.Result;
import com.common.entity.Item;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Objects;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 参数配置
 *
 * @author 谢长春 2017年7月21日 上午10:19:28
 */
@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/admin-config")
@Slf4j
@RequiredArgsConstructor
public class AdminConfigController {
    private final ApplicationContext applicationContext;
    private final ExecutorService multiThreadExecutorService;
    private final AppConfig appConfig;

    @ApiIgnore
    @GetMapping("/system")
    @ApiOperation(value = "系统环境变量")
    @ApiOperationSupport(order = 2, author = "谢长春")
    @ResponseBody
    public Result<SortedMap<String, Object>> system() {
        return new Result<SortedMap<String, Object>>().execute(result -> {
            final Properties properties = System.getProperties();
            final SortedMap<String, Object> map = new TreeMap<>();
            properties.forEach((key, value) -> map.put(Objects.toString(key), value));
            result.setSuccess(map);
        });
    }

    @GetMapping("/app")
    @ResponseBody
    public Result<AppConfig> getApp(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<AppConfig>().execute(result -> result.setSuccess(appConfig));
    }

    @GetMapping("/path")
    @ResponseBody
    public Result<Item> getPath(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Item>().execute(result -> result
                .setSuccess(Stream.of(Path.values())
                        .map(key -> new Item().setKey(key.name()).setValue(key.get()).setComment(key.comment))
                        .collect(Collectors.toList())
                )
        );
    }

    @GetMapping("/url")
    @ResponseBody
    public Result<Item> getUrl(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Item>().execute(result -> result
                .setSuccess(Stream.of(URL.values())
                        .map(key -> new Item().setKey(key.name()).setValue(key.value()).setComment(key.comment))
                        .collect(Collectors.toList())
                )
        );
    }

//    @PostMapping(value = "/api/encrypt", produces = "text/plain", consumes = "text/plain")
//    @ResponseBody
//    public String apiAesEncrypt(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final String body) {
//        return AesApi.encrypt(body);
//    }
//
//    @PostMapping(value = "/api/decrypt", produces = "text/plain", consumes = "text/plain")
//    @ResponseBody
//    public String apiAesDecrypt(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final String body) {
//        return AesApi.decrypt(body);
//    }
}
