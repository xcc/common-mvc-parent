package com.ccx.demo.business.common.convert;

import com.ccx.demo.business.common.dto.UserAvatarImageDTO;
import com.common.util.JSON;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

/**
 * 支撑 mysql 原生 JSON 数据类型与实体类属性的映射；
 * 需要在实体类属性上添加注解：@Convert(converter = {@link UserAvatarImageDTOJsonConvert}.class)
 *
 * @author 谢长春 2019/2/12
 */
@Converter
public class UserAvatarImageDTOJsonConvert implements AttributeConverter<UserAvatarImageDTO, String> {
    @Override
    public String convertToDatabaseColumn(final UserAvatarImageDTO attribute) {
        if (Objects.isNull(attribute)) {
            return null;
        }
        return JSON.toJsonString(attribute);
    }

    @Override
    public UserAvatarImageDTO convertToEntityAttribute(final String dbData) {
        return JSON.parseObject(dbData, UserAvatarImageDTO.class);
    }
}
