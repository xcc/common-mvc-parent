package com.ccx.demo.business.user.cache;

import com.ccx.demo.business.user.entity.TabUser;
import com.ccx.demo.business.user.vo.LoginUserVO;
import com.common.db.AbsRedissonStringCache;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBatch;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static com.ccx.demo.config.init.AppInit.getBean;


/**
 * 缓存：登录账号对应的用户id
 *
 * @author 谢长春 2022-06-16
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.cache.login")
public class LoginCache extends AbsRedissonStringCache<String> {
    public interface CacheRepository {
        /**
         * 按【登录账户、手机号、邮箱】查找用户
         *
         * @param username {@link String} 登录账户、手机号、邮箱
         * @return Optional<TabUser>
         */
        Optional<TabUser> findLoginUser(final String username);
    }

    private static final String CACHE_KEY = LoginCache.class.getSimpleName() + ":%s";
    private static volatile LoginCache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    private final TabUserCache tabUserCache;

    private final CacheRepository cacheRepository;

    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration expired;
    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;

    /**
     * 获取 {@link LoginCache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link LoginCache}
     */
    public static LoginCache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (LoginCache.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = getBean(LoginCache.class);
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.login.expired: {}", expired);
        log.info("spring.app.cache.login.nullValueExpired: {}", nullValueExpired);
    }


    /**
     * 基于数据主键构造缓存key； 拼接前缀，一般用类名作为前缀:unionKey
     *
     * @param unionKey String username or phone or email 【登录账户、手机号、邮箱】
     * @return String this.getClass().getSimpleName() + ":" + unionKey
     */
    @Override
    protected Optional<String> formatCacheKey(final String unionKey) {
        return Optional.ofNullable(unionKey)
                .filter(StringUtils::isNoneBlank)
                .map(val -> String.format(CACHE_KEY, unionKey))
                ;
    }

    /**
     * 获取用户缓存
     *
     * @param username String {@link TabUser#getUsername()} {@link TabUser#getPhone()}  {@link TabUser#getEmail()}
     * @return Optional<TabUser>
     */

    public Optional<TabUser> getUser(final String username) {
        final RBucket<String> bucket = redissonClient.getBucket(
                formatCacheKey(username).orElseThrow(NullPointerException::new)
                , StringCodec.INSTANCE
        );
        final String userId = bucket.get();
        log.info("getUserCache: {}", userId);
        if (StringUtils.isNoneBlank(userId)) {
            // 查到缓存中登录名对应的用户id，通过用户id获取用户缓存
            return tabUserCache.get(Long.parseLong(userId));
        } else if (Objects.equals("", userId)) {
            // 规避缓存穿透，会有 空字符串 写入缓存
            return Optional.empty();
        }
        // 从数据库查询登录名对应的用户信息
        final Optional<TabUser> optional = cacheRepository.findLoginUser(username);
        if (optional.isPresent()) {
            // 数据库查询结果写入缓存
            final TabUser user = optional.get();

            tabUserCache.set(user.getId(), user); // 用户缓存也写一下

            setUser(user);
        } else {
            // 规避缓存穿透，数据库不存在的记录，写空字符串，缓存 1 分钟
            bucket.set("", nullValueExpired.getSeconds(), TimeUnit.SECONDS);
        }
        return optional;
    }

    /**
     * 删除登录缓存
     *
     * @param key String
     */
    public void delete(final String key) {
        super.delete(key);
    }

    /**
     * 写缓存
     *
     * @param user {@link TabUser}
     */
    public void setUser(final TabUser user) {
        if (Objects.isNull(user)) {
            return;
        }
        // 所有可以作为登录凭证的字段都做一次用户 id 映射，写到缓存
        final RBatch batch = redissonClient.createBatch();
        Stream.of(user.getUsername(), user.getPhone(), user.getEmail())
                .filter(StringUtils::isNoneBlank)
                .map(this::formatCacheKey)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(cacheKey -> {
                    batch.getBucket(cacheKey, StringCodec.INSTANCE)
                            .setAsync(Objects.toString(user.getId()), expired.getSeconds(), TimeUnit.SECONDS);
                });
        batch.execute();
    }
    /**
     * 清理缓存
     *
     * @param user {@link LoginUserVO}
     */
    public void clear(final LoginUserVO user) {
        if (Objects.isNull(user)) {
            return;
        }
        // 所有可以作为登录凭证的字段都做一次用户 id 映射，写到缓存
        final RBatch batch = getRedissonClient().createBatch();
        Stream.of(user.getUsername(), user.getPhone(), user.getEmail())
                .filter(StringUtils::isNoneBlank)
                .map(this::formatCacheKey)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(cacheKey -> {
                    batch.getBucket(cacheKey, StringCodec.INSTANCE).deleteAsync();
                });
        batch.execute();
    }
//
//    /**
//     * 删缓存
//     *
//     * @param user {@link TabUser}
//     */
//    public void deleteUser(final TabUser user) {
//        if (Objects.isNull(user)) {
//            return;
//        }
//        Stream.of(user.getUsername(), user.getPhone(), user.getEmail())
//                .map(this::formatCacheKey)
//                .filter(Optional::isPresent)
//                .map(Optional::get)
//                .forEach(cacheKey -> redissonClient.getBucket(cacheKey, StringCodec.INSTANCE).delete());
//    }

}
