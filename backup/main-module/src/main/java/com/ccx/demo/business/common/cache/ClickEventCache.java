package com.ccx.demo.business.common.cache;

import com.ccx.demo.enums.AppEvent;
import com.common.db.ICache;
import com.common.util.JSON;
import com.google.common.collect.Lists;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RBatch;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.LongCodec;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 点击事件埋点计数
 *
 * @author 谢长春 2022-05-16
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ClickEventCache implements ICache {
    private final String CACHE_KEY = ClickEventCache.class.getSimpleName() + ":%s:%s";
    private final RedissonClient redissonClient;
//    private static volatile ClickEventCache INSTANCE = null;
//
//    /**
//     * 获取 {@link ClickEventCache} 实例，用于静态类或实体类获取配置参数
//     *
//     * @return {@link ClickEventCache}
//     */
//    public static ClickEventCache instance() {
//        if (Objects.isNull(INSTANCE)) {
//            synchronized (ClickEventCache.class) {
//                if (Objects.isNull(INSTANCE)) {
//                    INSTANCE = AppInit.getAppContext().getBean(ClickEventCache.class);
//                }
//            }
//        }
//        return INSTANCE;
//    }

    private RAtomicLong getBucket(final AppEvent event, final String id) {
        return redissonClient.getAtomicLong(String.format(CACHE_KEY, event.name(), id));
    }

    /**
     * 点击事件埋点计数增加
     *
     * @param event  {@link AppEvent} 埋点事件
     * @param id     {@link String} 数据id
     * @param userId {@link Long} 登录用户id，用于限制周期内只能有一次有效点击
     */
    public void increment(final AppEvent event, final String id, final Long userId) {
        increment(event, id, Objects.toString(userId));
    }

    /**
     * 点击事件埋点计数增加
     *
     * @param event     {@link AppEvent} 埋点事件
     * @param id        {@link String} 数据id
     * @param clientKey {@link String} 客户端唯一标记，用于限制周期内只能有一次有效点击
     */
    public void increment(final AppEvent event, final String id, final String clientKey) {
        if (!event.isClick()) {
            log.warn("【{}：{}】没有定义为点击事件埋点", event.name(), event.comment);
            return;
        }
        // FIXME: 2022/5/16 这里可以基于 clientKey ， 限制 15 分钟之内重复点击无效，不计数
        getBucket(event, id).incrementAndGet();
    }

    /**
     * 初始化计数
     *
     * @param event {@link AppEvent} 埋点事件
     * @param id    {@link String} 数据id
     * @param count {@link Long} 总数
     */
    public void set(final AppEvent event, final Long id, final long count) {
        set(event, Objects.toString(id), count);
    }

    /**
     * 初始化计数
     *
     * @param event {@link AppEvent} 埋点事件
     * @param id    {@link String} 数据id
     * @param count {@link Long} 总数
     */
    public void set(final AppEvent event, final String id, final long count) {
        getBucket(event, id).set(count);
    }

    /**
     * 初始化计数
     *
     * @param event {@link AppEvent} 埋点事件
     * @param map   Map<Long, Long> key: 数据ID， value: 计数
     */
    public void set(final AppEvent event, final Map<Long, Long> map) {
        final RBatch batch = redissonClient.createBatch();
        map.forEach((key, value) -> batch.getAtomicLong(String.format(CACHE_KEY, event.name(), key)).setAsync(value));
        log.info("操作数据行数：{}", batch.execute().getResponses().size());
    }

    /**
     * 初始化计数
     *
     * @param event {@link AppEvent} 埋点事件
     * @param map   Map<String, Long> key: 数据ID， value: 计数
     */
    public void setStringKey(final AppEvent event, final Map<String, Long> map) {
        final RBatch batch = redissonClient.createBatch();
        map.forEach((key, value) -> batch.getAtomicLong(String.format(CACHE_KEY, event.name(), key)).setAsync(value));
        log.info("操作数据行数：{}", batch.execute().getResponses().size());
    }

    /**
     * 获取总数
     *
     * @param event {@link AppEvent} 埋点事件
     * @param id    {@link Long} 数据id
     * @return long 总数
     */
    public long get(final AppEvent event, final Long id) {
        return get(event, Objects.toString(id));
    }

    /**
     * 获取总数
     *
     * @param event {@link AppEvent} 埋点事件
     * @param id    {@link String} 数据id
     * @return long 总数
     */
    public long get(final AppEvent event, final String id) {
        return getBucket(event, id).get();
    }

    /**
     * 批量获取埋点计数缓存
     *
     * @param event {@link AppEvent} 埋点事件
     * @param ids   List<Long> 数据id
     * @return long 总数
     */
    @SuppressWarnings("unchecked")
    public Map<Long, Long> mapByIds(final AppEvent event, final List<Long> ids) {
        final RBatch batch = redissonClient.createBatch();
        ids.forEach(id -> batch.getBucket(String.format(CACHE_KEY, event.name(), id), LongCodec.INSTANCE).getAsync());
        final List<Long> responses = (List<Long>) batch.execute().getResponses();
        return Stream.iterate(0, i -> ++i)
                .limit(ids.size())
                .collect(Collectors.toMap(
                        ids::get,
                        idx -> Optional.ofNullable(responses.get(idx)).orElse(0L))
                );
    }

    /**
     * 批量获取埋点计数缓存
     *
     * @param event {@link AppEvent} 埋点事件
     * @param ids   List<String> 数据id
     * @return long 总数
     */
    @SuppressWarnings("unchecked")
    public Map<String, Long> mapByStringIds(final AppEvent event, final List<String> ids) {
        final RBatch batch = redissonClient.createBatch();
        ids.forEach(id -> batch.getBucket(String.format(CACHE_KEY, event.name(), id), LongCodec.INSTANCE).getAsync());
        final List<Long> responses = (List<Long>) batch.execute().getResponses();
        return Stream.iterate(0, i -> ++i)
                .limit(ids.size())
                .collect(Collectors.toMap(
                        ids::get,
                        idx -> Optional.ofNullable(responses.get(idx)).orElse(0L))
                );
    }

    public static void main(String[] args) {
        Config config = new Config();
        config
                .useSingleServer()
                .setTimeout(1000000)
                .setAddress("redis://localhost:6379")
                .setPassword("111111");
        @Cleanup("shutdown") RedissonClient redissonClient = Redisson.create(config);
        ClickEventCache cache = new ClickEventCache(redissonClient);
        cache.set(AppEvent.CLICK_3, 1L, 10L); // 单个初始化值
        System.out.println("单个取值【1】：" + cache.get(AppEvent.CLICK_3, 1L));
        System.out.println("单个取值【2】：" + cache.get(AppEvent.CLICK_3, 2L));
        System.out.println("批量取值【1L, 2L, 3L】：" + JSON.toJsonString(cache.mapByIds(AppEvent.CLICK_3, Lists.newArrayList())));
        cache.set(AppEvent.CLICK_3, new HashMap<Long, Long>() {{ // 批量初始化值
            put(5L, 555L);
            put(6L, 666L);
        }});
        System.out.println("批量取值【4, 5, 1】：" + JSON.toJsonString(cache.mapByStringIds(AppEvent.CLICK_3, Lists.newArrayList("4", "5", "1"))));
    }
}
