package com.ccx.demo.business.user.entity;

import com.ccx.demo.business.user.vo.Authority;
import com.ccx.demo.config.DOEncryptConfiguration;
import com.ccx.demo.config.encrypt.annotations.AesIdStringJsonConverter;
import com.common.db.ITable;
import com.common.db.IWhere;
import com.common.db.IWhere.QdslWhere;
import com.common.db.convert.ArrayStringJsonConvert;
import com.common.db.entity.OrderBy;
import com.common.enums.Code;
import com.common.util.Dates;
import com.common.util.JSON;
import com.common.util.Then;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.QueryEntity;
import com.querydsl.core.annotations.QueryTransient;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.jpa.impl.JPAUpdateClause;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static com.ccx.demo.business.user.entity.QTabRole.tabRole;
import static com.common.util.Dates.Pattern.*;
import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 实体类：角色
 *
 * @author 谢长春 on 2019-08-29
 */
@Table(name = "tab_role")
@Entity
@QueryEntity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@ToString
@ApiModel(description = "角色")
public class TabRole implements
        ITable, // 所有与数据库表 - 实体类映射的表都实现该接口；方便后续一键查看所有表的实体
        // JPAUpdateClause => 需要的动态更新字段；采用 方案2 时需要实现该接口
        // QdslWhere       => 动态查询条件
        IWhere<JPAUpdateClause, QdslWhere> {

    private static final long serialVersionUID = -3752479204599023144L;
    /**
     * 数据ID
     */
    @AesIdStringJsonConverter // id 使用 aes 加密
    @Id
    @GeneratedValue(generator = "base36")
    @GenericGenerator(name = "base36", strategy = "com.common.db.generator.Base36Generator")
    @Size(min = 18, max = 18)
    @ApiModelProperty(value = "数据ID", position = 1)
    private String id;
    /**
     * 名称
     */
    @Size(max = 50)
    @ApiModelProperty(value = "名称", required = true, position = 3)
    private String name;
    /**
     * 备注
     */
    @Size(max = 200)
    @ApiModelProperty(value = "备注", position = 3)
    private String remark;
    /**
     * 权限指令集合，{@link String}[]
     */
    @Convert(converter = ArrayStringJsonConvert.class)
    @ApiModelProperty(value = "权限指令集合，{@link String}[]", position = 4)
    private String[] authorities;
    /**
     * 创建时间
     */
    @Size(min = 14, max = 14)
    @ApiModelProperty(value = "数据新增时间，格式:yyyyMMddHHmmss", example = "20200202020202", position = 5)
    private String createTime;
    /**
     * 创建用户ID
     */
    @PositiveOrZero
    @ApiModelProperty(value = "新增操作人id", position = 6)
    private Long createUserId;
    /**
     * 修改时间
     */
    @Size(min = 17, max = 17)
    @ApiModelProperty(value = "数据最后一次更新时间，格式:yyyyMMddHHmmssSSS", example = "20200202020202002", position = 7)
    private String updateTime;
    /**
     * 修改用户ID
     */
    @PositiveOrZero
    @ApiModelProperty(value = "更新操作人id", position = 8)
    private Long updateUserId;
    /**
     * 是否为隐藏角色，角色隐藏之后用户端看不到，一般用于禁止用户端修改或赋予用户管理员角色。 [0:否,1:是]',
     */
    @Column(updatable = false)
    @ApiModelProperty(hidden = true)
    private Boolean hidden;
    /**
     * 是否逻辑删除（1、已删除， 0、未删除）。 [0:否,1:是]
     */
    @ApiModelProperty(value = "是否逻辑删除。 [0:否,1:是]", position = 10)
    private Boolean deleted;

    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Set<String> ids;
    /**
     * 排序字段
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(value = "查询排序字段", position = 11)
    private List<OrderBy> orderBy;
    /**
     * 查询参数：排除 id
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(hidden = true)
    private Set<String> idExcludes;
    /**
     * 前端配置的权限树
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(value = "前端配置的权限树", required = true, position = 13)
    private List<Authority> authorityTree;
    /**
     * 新增操作人昵称
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "新增操作人昵称", position = 14, accessMode = READ_ONLY)
    private String createUserNickname;
    /**
     * 更新操作人昵称
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "更新操作人昵称", position = 14, accessMode = READ_ONLY)
    private String updateUserNickname;

    /**
     * 防止踩坑。
     * 当 JPA 查询对象没有 clone 时，执行 setValue 操作时，会触发更新动作，这里拦截之后抛出异常
     */
    @PreUpdate
    public void onPreUpdate() {
        throw Code.A00003.toCodeException("防止踩坑，谨慎使用JPA默认的 update 方法。 如果业务逻辑确定需要使用 setValue 方式更新对象时，去掉这个异常");
    }

    /**
     * 数据版本号，用于更新和删除参数，规避脏数据更新
     *
     * @return String
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "数据版本号，用于更新和删除参数，规避脏数据更新", position = 1, accessMode = READ_ONLY)
    public String getDv() {
        return DOEncryptConfiguration.encryptDV(id, getUpdateTime());
    }

    /**
     * 创建时间格式化，仅用于前端展示
     *
     * @return String
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "创建时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
    public String getCreateTimeFormat() {
        return yyyyMMddHHmmss.parseOfNullable(createTime).map(yyyy_MM_dd_HH_mm_ss::format).orElse(null);
    }

    /**
     * 修改时间格式化，仅用于前端展示
     *
     * @return String
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "修改时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
    public String getUpdateTimeFormat() {
        return yyyyMMddHHmmssSSS.parseOfNullable(updateTime).map(yyyy_MM_dd_HH_mm_ss_SSS::format).orElse(null);
    }


    @SneakyThrows
    public TabRole cloneObject() {
        return (TabRole) super.clone();
    }

    // Enum Start **********************************************************************************************************

    /**
     * 枚举：定义排序字段
     */
    public enum OrderByColumn {
        // 按 id 排序可替代按创建时间排序
        id(tabRole.id),
//        name(tabRole.name),
//        authorities(tabRole.authorities),
//        createTime(tabRole.createTime),
//        createUserId(tabRole.createUserId),
//        updateTime(tabRole.updateTime),
//        updateUserId(tabRole.updateUserId),
//        hidden(tabRole.hidden),
//        deleted(tabRole.deleted),
        ;
        private final ComparableExpressionBase<?> column;

        public OrderBy asc() {
            return new OrderBy().setName(this.name());
        }

        public OrderBy desc() {
            return new OrderBy().setName(this.name()).setDirection(Sort.Direction.DESC);
        }

        /**
         * 获取所有排序字段名
         *
         * @return String[]
         */
        public static String[] names() {
            return Stream.of(OrderByColumn.values()).map(Enum::name).toArray(String[]::new);
        }

        OrderByColumn(final ComparableExpressionBase<?> column) {
            this.column = column;
        }
    }

// Enum End : DB Start *************************************************************************************************

    /**
     * DAO层新增数据时，设置字段默认值
     *
     * @param now    当前时间
     * @param userId 当前操作用户id
     */
    public void insert(final Dates now, final Long userId) {
        this.id = null;
        this.hidden = Optional.ofNullable(this.hidden).orElse(false);
        this.deleted = Optional.ofNullable(this.deleted).orElse(false);
        this.createUserId = userId;
        this.createTime = now.format(yyyyMMddHHmmss);
        this.updateUserId = userId;
        this.updateTime = now.format(yyyyMMddHHmmssSSS);
    }

    @Override
    public Then<JPAUpdateClause> update(final JPAUpdateClause jpaUpdateClause) {
        final QTabRole table = tabRole;
        // 动态拼接 update 语句
        // 以下案例中 只有 name 属性 为 null 时才不会加入 update 语句；
        return Then.of(jpaUpdateClause)
                .then(name, update -> update.set(table.name, name))
                .then(remark, update -> update.set(table.remark, remark))
                .then(authorities, update -> update.set(table.authorities, authorities))
//                .then(update -> update.set(table.updateUserId, updateUserId))
//                // 假设数据库中 content is not null；可以在属性为null时替换为 ""
//                .then(update -> update.set(table.content, Optional.ofNullable(content).orElse("")))
//                // 数据库中 amount 可以为 null
//                .then(update -> update.set(table.amount, amount))
                ;
    }

    @Override
    public QdslWhere where() {
        final QTabRole table = tabRole;
        // 构建查询顺序规则请参考：com.support.mvc.entity.IWhere#where
        return QdslWhere.of()
                .andIfNonEmpty(ids, () -> table.id.in(ids))
                .and(id, () -> table.id.eq(id))
                .and(idExcludes, () -> table.id.notIn(idExcludes))
                .and(name, () -> table.name.contains(name))
//                .and(authorities, () -> table.authorities.eq(authorities))
//                .and(createTimeRange, () -> table.createTime.between(createTimeRange.rebuild().getBegin(), createTimeRange.getEnd()))
                .and(createUserId, () -> table.createUserId.eq(createUserId))
//                .and(updateTimeRange, () -> table.updateTime.between(updateTimeRange.rebuild().getBegin(), updateTimeRange.getEnd()))
                .and(updateUserId, () -> table.updateUserId.eq(updateUserId))
                .and(hidden, () -> table.hidden.eq(hidden))
                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
//                .and(phone, () -> table.phone.eq(phone))
//                .and(createUserId, () -> table.createUserId.eq(createUserId))
//                .and(updateUserId, () -> table.updateUserId.eq(updateUserId))
//                // 强制带默认值的查询字段
//                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
//                // 数字区间查询
//                .and(amountRange, () -> table.amount.between(amountRange.getMin(), amountRange.getMax()))
//                // 日期区间查询；Range.rebuild() : 先将时间区间重置到 00:00:00.000 - 23:59:59.999 ; 大多数情况都需要重置时间
//                .and(createTimeRange, () -> table.createTime.between(createTimeRange.rebuild().getBegin(), createTimeRange.getEnd()))
//                // 模糊匹配查询：后面带 % ；建议优先使用
//                .and(name, () -> table.name.startsWith(name)) // 模糊匹配查询：后面带 %
//                .and(name, () -> table.name.endsWith(name)) // 模糊匹配查询：前面带 %
//                .and(name, () -> table.name.like(MessageFormat.format("%{0}%", name)))
//                .and(name, () -> table.name.contains(name)) // 模糊匹配查询：前后带 %,同 MessageFormat.format("%{0}%", name)
                ;
    }

    /**
     * 排序参数构建：QueryDSL 查询模式；QueryDSL 模式构建排序对象返回 null 则会抛出异常
     *
     * @return {@link OrderSpecifier[]}
     */
    public OrderSpecifier<?>[] qdslOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return new OrderSpecifier<?>[]{OrderByColumn.id.column.asc()}; // 指定默认排序字段
                return new OrderSpecifier<?>[]{};
            }
            return orderBy.stream().map((OrderBy by) -> {
                final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                // 自定义排序
                // new OrderSpecifier<>(Order.valueOf(by.getDirection().name()) , Expressions.stringTemplate("convert_gbk({0})", column.column));

                // return Objects.equals(orderBy.getDirection(), Sort.Direction.DESC) ? column.column.desc(): column.column.asc();
                return new OrderSpecifier<>(Order.valueOf(by.getDirection().name()), column.column);
            }).toArray(OrderSpecifier<?>[]::new);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 排序参数构建：Spring JPA 查询模式；JPA 模式构建排序对象返回 null 表示不排序
     *
     * @return {@link Sort}
     */
    public Sort jpaOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return Sort.by(Sort.Direction.ASC, OrderByColumn.id.name()); // 指定默认排序字段
                return Sort.unsorted();
            }
            return orderBy.stream()
                    .map((OrderBy by) -> {
                        final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                        // 自定义排序
                        // JpaSort.unsafe(by.getDirection(), String.format("convert_gbk(%s)", column.name()));
                        return Sort.by(by.getDirection(), column.name());
                    })
                    .reduce(Sort::and)
                    .orElseGet(Sort::unsorted);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 获取查询实体与数据库表映射的所有字段,用于投影到 VO 类
     * 支持追加扩展字段,追加扩展字段一般用于连表查询
     *
     * @param appends {@link Expression}[] 追加扩展连表查询字段
     * @return {@link Expression}[]
     */
    public static Expression<?>[] allColumnAppends(final Expression<?>... appends) {
        final List<Expression<?>> columns = Lists.newArrayList(appends);
        final Class<?> clazz = tabRole.getClass();
        try {
            for (Field field : clazz.getDeclaredFields()) {
                if (field.getType().isPrimitive()) continue;
                final Object o = field.get(tabRole);
                if (o instanceof EntityPath || o instanceof BeanPath) continue;
                if (o instanceof Path) {
                    columns.add((Path<?>) o);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("获取查询实体属性与数据库映射的字段异常", e);
        }
        return columns.toArray(new Expression<?>[0]);
    }

// DB End **************************************************************************************************************

}
