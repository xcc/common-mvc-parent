package com.ccx.demo.business.user.service;

import com.ccx.demo.business.common.dto.UserAvatarImageDTO;
import com.ccx.demo.business.user.cache.LoginCache;
import com.ccx.demo.business.user.cache.TabRoleCache;
import com.ccx.demo.business.user.cache.TabUserCache;
import com.ccx.demo.business.user.dao.jpa.UserRepository;
import com.ccx.demo.business.user.dto.TabUserInsertDTO;
import com.ccx.demo.business.user.dto.TabUserUpdateDTO;
import com.ccx.demo.business.user.entity.QTabUser;
import com.ccx.demo.business.user.entity.TabRole;
import com.ccx.demo.business.user.entity.TabUser;
import com.ccx.demo.business.user.vo.LoginUserVO;
import com.ccx.demo.business.user.vo.TabUserVO;
import com.ccx.demo.config.init.AppConfig;
import com.ccx.demo.enums.AppCode;
import com.ccx.demo.enums.TokenClient;
import com.ccx.demo.open.auth.cache.TokenCache;
import com.common.db.entity.Page;
import com.common.enums.Code;
import com.common.exception.DeleteRowsException;
import com.common.exception.UpdateRowsException;
import com.common.util.Rsa;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 服务接口实现类：用户
 *
 * @author 谢长春 on 2017/10/12.
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConfigurationProperties("spring.app.user")
public class UserService {

    private final RoleService roleService;
    private final TabUserCache tabUserCache;
    private final LoginCache loginCache;
    private final TokenCache tokenCache;
    private final TabRoleCache tabRoleCache;
    private final AppConfig appConfig;
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    /**
     * 运营管理端普通用户默认角色id
     */
    @Setter
    private String omRoleId;
    /**
     * 用户默认角色id
     */
    @Setter
    private String appRoleId;

    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final SecretKeySpec SK;
    private static final IvParameterSpec IV;

    static {
        try {
            final String sk = DigestUtils.md5Hex(UserService.class.getName());
            SK = new SecretKeySpec(sk.getBytes(), "AES");
            IV = new IvParameterSpec(StringUtils.right(sk, 16).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 保存
     *
     * @param dto    TabUser 实体对象
     * @param userId {@link Long} 操作用户ID
     * @return TabUserInsertDTO 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") TabUser insert(
            @Valid @NotNull(message = "【dto】不能为null") final TabUserInsertDTO dto,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        repository.assertUserExist(dto.getUsername(), dto.getPhone(), dto.getEmail());
        final TabUser obj = new TabUser();
        BeanUtils.copyProperties(dto, obj);
        obj.setRoles(Optional.ofNullable(dto.getRoles())
                .filter(ArrayUtils::isNotEmpty)
                .map(roleService::matchValidRoleIds)
                .orElseGet(() -> new String[]{})
        );
        obj.setPassword(passwordEncoder.encode(obj.getPassword()));
        return repository.insert(userId, obj);
    }

    /**
     * 更新数据
     *
     * @param id     {@link Long} 数据ID
     * @param userId {@link Long} 操作用户ID
     * @param dto    TabUserUpdateDTO 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId,
            @Valid @NotNull(message = "【dto】不能为null") final TabUserUpdateDTO dto) {
        if (!Strings.isNullOrEmpty(dto.getEmail())) {
            AppCode.B01006.assertHasFalse(repository.exists(
                    QTabUser.tabUser.email.eq(dto.getEmail())
                            .and(QTabUser.tabUser.id.ne(id))
            ));
        }
        if (!Strings.isNullOrEmpty(dto.getPhone())) {
            AppCode.B01007.assertHasFalse(repository.exists(
                    QTabUser.tabUser.phone.eq(dto.getPhone())
                            .and(QTabUser.tabUser.id.ne(id))
            ));
        }

        final TabUser tabUser = findById(id).orElseThrow(NullPointerException::new);
        Code.A00002.assertHasFalse(
                // 管理员角色权限不能编辑, 只能通过编码的方式授权管理员权限： @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
                Objects.equals(true, tabUser.getHidden()),
                "禁止编辑该用户"
        );

        final TabUser obj = new TabUser();
        BeanUtils.copyProperties(dto, obj);
        obj.setRoles(Optional.ofNullable(dto.getRoles())
                .filter(ArrayUtils::isNotEmpty)
                .map(roleService::matchValidRoleIds)
                .orElseGet(() -> new String[]{})
        );
        UpdateRowsException.asserts(repository.update(id, userId, obj));

        tabUserCache.delete(id);
    }

    /**
     * 用户 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link Long} 数据ID
     * @return {@link Optional<TabUser>} 实体对象
     */

    public Optional<TabUser> findById(final Long id) {
        if (Objects.isNull(id) || id <= 0) {
            return Optional.empty();
        }
//        return repository.findById(id)
//                .map(TabUser::cloneObject); // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
        return tabUserCache.get(id)
//                .map(row -> {
//                    return row;
//                })
                ;
    }

    /**
     * 按 id 删除，逻辑删除
     *
     * @param id     {@link Long} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        final TabUser tabUser = repository.findById(id)
                .orElseThrow(NullPointerException::new);
        Code.A00002.assertHasFalse(
                // 管理员角色权限不能编辑, 只能通过编码的方式授权管理员权限： @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
                Objects.equals(true, tabUser.getHidden()),
                "禁止删除该用户"
        );
        UpdateRowsException.asserts(repository.markDeleteById(id, userId));
        tabUserCache.delete(id);
    }

    /**
     * 按 id+updateTime 删除，逻辑删除
     *
     * @param id         {@link Long} 数据ID
     * @param updateTime {@link String} 最后一次更新时间
     * @param userId     {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        final TabUser tabUser = repository.findById(id)
                .orElseThrow(NullPointerException::new);
        Code.A00002.assertHasFalse(
                // 管理员角色权限不能编辑, 只能通过编码的方式授权管理员权限： @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
                Objects.equals(true, tabUser.getHidden()),
                "禁止删除该用户"
        );
        UpdateRowsException.asserts(repository.markDeleteById(id, updateTime, userId));
        tabUserCache.delete(id);
    }

    /**
     * 批量操作按 id 删除，批量逻辑删除
     *
     * @param ids    {@link List<Long>} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@NotNull @Positive Long> ids,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        if (repository.exists(QTabUser.tabUser.id.in(ids)
                .and(QTabUser.tabUser.hidden.eq(true)))
        ) {
            throw new RuntimeException("禁止删除该用户");
        }
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, userId), ids.size());
        tabUserCache.delete(ids);
    }

    /**
     * 按 id+updateTime 删除，批量逻辑删除
     *
     * @param ids    {@link Set<Long>} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDelete(
            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
            , final Set<String> updateTimes
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId
    ) {
        if (repository.exists(QTabUser.tabUser.id.in(ids)
                .and(QTabUser.tabUser.hidden.eq(true)))
        ) {
            throw new RuntimeException("禁止删除该用户");
        }
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, updateTimes, userId), ids.size());
        tabUserCache.delete(ids);
    }

    /**
     * 按条件分页查询列表
     *
     * @param condition TabUser 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return {@link QueryResults<TabUser>} 分页对象
     */

    public @NotNull(message = "返回值不能为null") QueryResults<TabUser> page(
            @NotNull(message = "【condition】不能为null") final TabUser condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        condition.setHidden(false); // 禁止前端查看隐藏角色
        final QueryResults<TabUser> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        final Map<Long, TabUser> userMap = this.mapByIds(queryResults.getResults().stream()
                .flatMap(row -> Stream.of(row.getCreateUserId(), row.getUpdateUserId()))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet())
        );
        queryResults.getResults().forEach(row -> {
            Optional.ofNullable(userMap.get(row.getCreateUserId())).ifPresent(user -> {
                row.setCreateUserNickname(user.getNickname());
            });
            Optional.ofNullable(userMap.get(row.getUpdateUserId())).ifPresent(user -> {
                row.setUpdateUserNickname(user.getNickname());
            });
        });
        return queryResults;
    }

    /**
     * 用户 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link Long}  数据 id 集合
     * @return {@link List<TabUser>} 结果集合
     */

    public @NotNull(message = "返回值不能为null") List<TabUser> listByIds(final Collection<Long> ids) {
        if (org.springframework.util.CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return tabUserCache.listByIds(ids);
    }

    /**
     * 用户 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link Long} 数据 id 集合
     * @return {@link List<TabUser>} 结果集合
     */

    public @NotNull(message = "返回值不能为null") Map<Long, TabUser> mapByIds(final Set<Long> ids) {
        return tabUserCache.mapByIds(ids);
    }


    public QueryResults<TabUserVO> pageVO(
            @NotNull(message = "【condition】不能为null") final TabUserVO condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        condition.setHidden(false); // 禁止前端查看隐藏角色
        final QueryResults<TabUserVO> queryResults = repository.page(condition, page, TabUserVO.class);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        final Map<Long, TabUser> userMap = this.mapByIds(queryResults.getResults().stream()
                .flatMap(row -> Stream.of(row.getCreateUserId(), row.getUpdateUserId()))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet())
        );
        queryResults.getResults().forEach(row -> {
            Optional.ofNullable(userMap.get(row.getCreateUserId())).ifPresent(user -> {
                row.setCreateUserNickname(user.getNickname());
            });
            Optional.ofNullable(userMap.get(row.getUpdateUserId())).ifPresent(user -> {
                row.setUpdateUserNickname(user.getNickname());
            });
        });
        return queryResults;
    }

    /**
     * 按用户名查找用户信息，（查询范围：username|phone|email）
     *
     * @param username String 用户名
     * @return Optional<LoginUserVO>
     */

    public Optional<LoginUserVO> findLoginUserVO(final String username) {
        return loginCache.getUser(username)
                .map(user -> {
                    final LoginUserVO vo = new LoginUserVO();
                    BeanUtils.copyProperties(user, vo);
                    final List<TabRole> tabRoles = tabRoleCache.listByIds(Sets.newHashSet(user.getRoles())).stream()
                            .filter(row -> Objects.equals(false, row.getDeleted()))
                            .collect(Collectors.toList());
                    if (!tabRoles.isEmpty()) {
                        vo.setAuthorityList(tabRoles.stream()
                                .map(TabRole::getAuthorities)
                                .filter(Objects::nonNull)
                                .flatMap(Arrays::stream)
                                .distinct()
                                .collect(Collectors.toList())
                        );
                        vo.setRoleNames(tabRoles.stream()
                                .map(TabRole::getName)
                                .filter(StringUtils::isNoneBlank)
                                .collect(Collectors.toList()));
                    }
                    return vo;
                });
    }
//    /**
//     * 启用账户
//     *
//     * @param id     Long 用户ID
//     * @param userId Long 修改者ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void enable(final Long id, final Long userId) {
//        UpdateRowsException.asserts(repository.enable(id, userId));
//    }
    /**
     * 更新数据
     *
     * @param id       {@link Long} 数据ID
     * @param userId   {@link Long} 操作用户ID
     * @param nickname String 昵称
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateNickname(@NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
                               @NotBlank(message = "【nickname】不能为null") @Size(min = 1, max = 32, message = "【nickname】字符长度不能超过32位") final String nickname,
                               @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        UpdateRowsException.asserts(repository.updateNickname(id, nickname, userId));
        tabUserCache.delete(id);
    }

    /**
     * 修改密码
     *
     * @param id       Long 用户ID
     * @param password String 新密码
     * @param userId   Long 修改者ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void updatePassword(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotBlank(message = "【password】不能为空") @Size(min = 6, max = 15, message = "【password】必须是 6-15 位") final String password,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        UpdateRowsException.asserts(repository.updatePassword(id, passwordEncoder.encode(password), userId));
        tabUserCache.delete(id);
    }

    /**
     * 修改禁用启用状态
     *
     * @param id       Long 用户ID
     * @param disabled Boolean 是否禁用
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateDisabled(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotNull(message = "【disabled】不能为null") final Boolean disabled,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        UpdateRowsException.asserts(repository.updateDisabled(id, disabled, userId));
        tabUserCache.delete(id);
    }

    /**
     * 重置密码。  这里先返回新密码，基于安全考虑，正确的逻辑应该是发送短信或者邮件给用户，而不是返回新密码
     *
     * @param id     Long 用户ID
     * @param userId Long 修改者ID
     */
    @Transactional(rollbackFor = Exception.class)
    public String resetPassword(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        final String password = RandomStringUtils.randomAlphanumeric(8);
        updatePassword(id, password, userId);
        return password;
    }

    /**
     * 当前登录用户修改密码
     *
     * @param id          {@link Long} 当前登录用户ID
     * @param passwordOld {@link String} 原密码
     * @param passwordNew {@link String} 新密码
     */
    @Transactional(rollbackFor = Exception.class)
    public void changePassword(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotBlank(message = "【passwordOld】不能为空") final String passwordOld,
            @NotBlank(message = "【passwordNew】不能为空") final String passwordNew
    ) {
        final TabUser user = tabUserCache.get(id).orElseThrow(() -> Code.A00002.toCodeException("用户不存在"));
        if (appConfig.getEncrypt().getPwd().isEnabled()) {
            AppCode.B01015.assertHasTrue(
                    passwordEncoder.matches(passwordOld, user.getPassword())
                    , "原密码错误"
            );
            final String passwordNewDecrypt = Rsa.decryptByPrivateKey(
                    passwordNew,
                    appConfig.getEncrypt().getPwd().getPrivateKey()
            );
            updatePassword(id, passwordNewDecrypt, id);
        } else {
            AppCode.B01015.assertHasTrue(
                    passwordEncoder.matches(passwordOld, user.getPassword())
                    , "原密码错误"
            );
            updatePassword(id, passwordNew, id);
        }
    }

    /**
     * 当前登录用户修改头像
     *
     * @param id     {@link Long} 当前登录用户ID
     * @param avatar {@link UserAvatarImageDTO} 新头像
     */
    @Transactional(rollbackFor = Exception.class)
    public void changeAvatar(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id,
            @NotNull(message = "【avatar】不能为null") final UserAvatarImageDTO avatar
    ) {
        UpdateRowsException.asserts(repository.updateAvatar(id, avatar, id));
        tabUserCache.delete(id);
    }

    /**
     * 初始化用户
     *
     * @param phone            String 手机号
     * @param chain            Function<TabUser, TabUser> 补充默认值
     * @param registerConsumer Consumer<Long> 走注册逻辑时回调注册用户id
     * @return TabUser
     */
    @Transactional(rollbackFor = Exception.class)
    public LoginUserVO findAndInsertAppUser(
            final String phone
            , final Function<TabUser, TabUser> chain
            , final Consumer<Long> registerConsumer
    ) {
        return findLoginUserVO(phone)
                .orElseGet(() -> { // 没查到用户，开始注册账号
                    loginCache.delete(phone);
                    final TabUser insertUserDTO = new TabUser();
                    insertUserDTO.setUsername(phone);
                    insertUserDTO.setPhone(phone);
                    insertUserDTO.setNickname(phone.replaceAll("^(\\d{3})\\d{4}(\\d{4})$", "$1****$2"));
                    insertUserDTO.setPassword(passwordEncoder.encode(RandomStringUtils.randomAlphanumeric(10)));
                    insertUserDTO.setRoles(new String[]{appRoleId});
                    final TabUser insertUserVO = repository.insert(0L, chain.apply(insertUserDTO));
                    registerConsumer.accept(insertUserVO.getId());
                    final LoginUserVO vo = new LoginUserVO();
                    BeanUtils.copyProperties(insertUserVO, vo);
                    final List<TabRole> tabRoles = tabRoleCache.listByIds(Sets.newHashSet(appRoleId)).stream()
                            .filter(row -> Objects.equals(false, row.getDeleted()))
                            .collect(Collectors.toList());
                    if (!tabRoles.isEmpty()) {
                        vo.setAuthorityList(tabRoles.stream()
                                .map(TabRole::getAuthorities)
                                .filter(Objects::nonNull)
                                .flatMap(Arrays::stream)
                                .distinct()
                                .collect(Collectors.toList())
                        );
                        vo.setRoleNames(tabRoles.stream()
                                .map(TabRole::getName)
                                .filter(StringUtils::isNoneBlank)
                                .collect(Collectors.toList()));
                    }
                    return vo;
                });
    }

    /**
     * aes 加密手机号， 用于微信小程序邀请二维码唯一标记
     *
     * @param phone {@link String} 手机号
     * @return {@link String} 密文
     */
    @SneakyThrows
    public String encryptWechatScene(final String phone) {
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, SK, IV);
        final String result = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(cipher.doFinal(phone.getBytes()));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", phone, result);
        }
        return result;
    }

    /**
     * aes解密微信scene
     *
     * @param scene {@link String} 密文
     * @return 明文
     */
    @SneakyThrows
    public String decryptWechatScene(final String scene) {
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, SK, IV);
        return new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(scene)));
    }

    /**
     * 基于aes解密后的微信scene，查询用户信息
     *
     * @param decryptScene {@link String} 明文
     * @return 明文
     */
    @SneakyThrows
    public Optional<LoginUserVO> getUserByWechatScene(final String decryptScene) {
        final String phone = decryptScene;
        return findLoginUserVO(phone).map(row -> {
            LoginUserVO userVO = new LoginUserVO();
            userVO.setId(row.getId());
            userVO.setNickname(row.getNickname());
            userVO.setAvatar(row.getAvatar());
            userVO.setPhone(phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));
            userVO.setUpdateTime(row.getUpdateTime());
            return userVO;
        });
    }

    /**
     * 销毁账号，重新注册
     *
     * @param phone {@link String} {@link TabUser#getPhone()}
     */
    @Transactional(rollbackFor = Exception.class)
    public void clear(
            @NotBlank(message = "【phone】不能为空") final String phone,
            List<Consumer<TabUser>> chain
    ) {
        findLoginUserVO(phone).ifPresent(user -> {
            UpdateRowsException.asserts(repository.clear(user.getId()));
            chain.forEach(consumer -> consumer.accept(user));
            tabUserCache.delete(user.getId());
            loginCache.clear(user);
            tokenCache.clear(user.getId());
        });
    }
}
