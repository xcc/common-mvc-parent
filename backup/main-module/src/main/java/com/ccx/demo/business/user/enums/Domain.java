package com.ccx.demo.business.user.enums;

/**
 * <pre>
 * 枚举：角色域，多平台角色可以通过角色域划分权限
 *
 * @author 谢长春 2021-03-27
 */
public enum Domain {
    www("默认平台"),
    test("测试平台"),
    ;
    public final String comment;

    Domain(final String comment) {
        this.comment = comment;
    }
}
