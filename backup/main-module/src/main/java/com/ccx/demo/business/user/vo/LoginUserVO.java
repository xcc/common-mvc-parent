package com.ccx.demo.business.user.vo;

import com.ccx.demo.business.user.entity.TabRole;
import com.ccx.demo.business.user.entity.TabUser;
import com.ccx.demo.enums.TokenClient;
import com.common.db.IUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 登录用户对象
 *
 * @author 谢长春 2022-06-18
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "登录用户对象")
public class LoginUserVO extends TabUser implements IUser, UserDetails {
    /**
     * 登录客户端
     */
    @ApiModelProperty(value = "登录客户端", example = "OM_PC")
    private TokenClient client;
    /**
     * 权限代码
     */
    @ApiModelProperty(value = "权限代码", example = "user_list_load")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<String> authorityList;

    /**
     * 角色名称集合 {@link TabRole#getName()}
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "角色名称集合", accessMode = READ_ONLY, position = 23)
    private List<String> roleNames;

    @JsonIgnore
    @Override
    public String getPassword() {
        return super.getPassword();
    }

    /**
     * 权限
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (CollectionUtils.isEmpty(authorityList)) {
            return Collections.emptyList();
        }
        return authorityList.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isAccountNonExpired() {
        return true;
//        return Objects.equals(false, user.getExpired());
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return !(getDisabled() || getDeleted());
    }

    /**
     * 检查是否允许登录
     *
     * @return boolean true：是， false：否
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public boolean isAllow() {
        return isEnabled() && isCredentialsNonExpired() && isAccountNonLocked() && isAccountNonExpired();
    }
}
