package com.ccx.demo.config;

import com.ccx.demo.business.user.vo.LoginUserVO;
import com.ccx.demo.open.auth.dto.Token;
import com.common.db.IUser;
import com.common.enums.Code;
import com.common.exception.CodeException;
import com.common.util.Rsa;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * Spring Security 权限控制
 * <pre>
 *     中文文档：https://springcloud.cc/spring-security-zhcn.html
 *     官方文档：https://docs.spring.io/spring-security/reference/servlet/configuration/java.html
 *             https://docs.spring.ioio/spring-security/reference/getting-spring-security.html
 *             https://docs.spring.io/spring-security/reference/servlet/authentication/events.html
 *
 * @author 谢长春 2022-06-19
 */
@Configuration(proxyBeanMethods = false)
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true)
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
// 使用 @EnableGlobalMethodSecurity 注解来启用基于注解的安全性
// @EnableGlobalMethodSecurity(securedEnabled = true) // 启用注解：@Secured；[@Secured("ROLE_USER"), @Secured("IS_AUTHENTICATED_ANONYMOUSLY")]
// @EnableGlobalMethodSecurity(prePostEnabled = true) // 启用注解：@PreAuthorize；[@PreAuthorize("hasAuthority('ROLE_USER')"), @PreAuthorize("isAnonymous()")]
@Slf4j
public class SecurityConfiguration {
//    /**
//     * 自定义校验规则
//     * https://docs.spring.io/spring-security/reference/servlet/authorization/method-security.html
//     */
//    @Configuration
//    public static class GlobalMethodSecurity extends GlobalMethodSecurityConfiguration {
//        @Override
//        protected MethodSecurityExpressionHandler createExpressionHandler() {
//            final DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
//            expressionHandler.setPermissionEvaluator(null);
//            return expressionHandler;
//        }
//    }

    private static final Pattern REG = Pattern.compile("^[\\w-]+$");

    private static boolean tokenRegexp(final String token) {
        return REG.matcher(token).matches();
    }

    /**
     * 请求url打印
     */
    private static final OncePerRequestFilter logUriFilter = new OncePerRequestFilter() {

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
            log.info("{} {}", request.getMethod(), request.getRequestURI());
            filterChain.doFilter(request, response);
        }
    };

    /**
     * 跨域配置。
     * 注意 allowedHeaders 会影响跨域条件
     * org.springframework.web.cors.DefaultCorsProcessor#handleInternal(ServerHttpRequest, ServerHttpResponse, CorsConfiguration, boolean)
     *
     * @return CorsConfigurationSource
     */
    @ConditionalOnProperty(value = "spring.app.corsEnabled", havingValue = "true")
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final List<String> allowedHeaders = Arrays.asList(
                "content-type"
                , "x-token"
                , "x-version"
                , "X-Trace-Id"
        );
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOriginPatterns(Collections.singletonList("*"));
//        configuration.setAllowedOrigins(Arrays.asList("http://localhost:9528"));
        configuration.setAllowedMethods(Collections.singletonList("*"));
//        configuration.setAllowedMethods(Arrays.asList("GET","POST"));
        configuration.setAllowedHeaders(allowedHeaders);
        configuration.setExposedHeaders(allowedHeaders);
        configuration.setAllowCredentials(true);
        configuration.setMaxAge(TimeUnit.HOURS.toSeconds(1));
//        configuration.setMaxAge(10L);

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /**
     * 静态文件映射配置
     */
    @Configuration(proxyBeanMethods = false)
    public static class StaticAssetConfiguration
//            extends WebMvcConfigurationSupport
            implements WebMvcConfigurer {
        @Setter
        @Value("${spring.app.nfs.disk.directory:-''}")
        private String nfsDiskDirectory;

        @Override
        public void addResourceHandlers(final ResourceHandlerRegistry registry) {
            // 添加静态资源过滤
            registry.addResourceHandler("favicon.ico")
                    .addResourceLocations("classpath:/static/")
                    .setCacheControl(CacheControl.maxAge(30, TimeUnit.DAYS).cachePublic());
            // 需要在 Spring Security 中配置忽略静态资源 WebSecurity.ignoring().antMatchers("/static/**");
            registry.addResourceHandler("/static/**")
                    // Locations 这里应该是编译后的静态文件目录
                    .addResourceLocations("classpath:/static/")
                    .setCacheControl(CacheControl.maxAge(1, TimeUnit.MINUTES).cachePublic());

            // knife4j 增强 swagger 配置 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            registry.addResourceHandler("doc.html")
                    .addResourceLocations("classpath:/META-INF/resources/")
                    .setCacheControl(CacheControl.maxAge(1, TimeUnit.DAYS).cachePublic());
            registry.addResourceHandler("/webjars/**")
                    .addResourceLocations("classpath:/META-INF/resources/webjars/")
                    .setCacheControl(CacheControl.maxAge(30, TimeUnit.DAYS).cachePublic());
            // knife4j 增强 swagger 配置 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            // 添加静态资源过滤
            // 需要在 com.ccx.demo.config.SecurityConfiguration#staticSecurityFilterChain 中配置静态资源路径不走权限校验
            if (StringUtils.isNoneBlank(nfsDiskDirectory)) {
                registry.addResourceHandler("/nfs/**")
                        // TODO 采坑记录结尾带 / 和不带 / 的区别
                        //   假设请求url为：{{domain}}/files/temp/a.txt
                        //   addResourceLocations 指定绝对路径
                        //   d:/files => d:/temp/a.txt
                        //   d:/files/ => d:/files/temp/a.txt
                        .addResourceLocations(String.format("file:%s/", nfsDiskDirectory))
                ;
            }
        }
    }
//
//    @Bean
//    public WebSecurityCustomizer webSecurityCustomizer() {
//        return web -> {
//            web.ignoring().antMatchers( // resources 下的静态资源
//                    "/favicon.ico"
//                    , "/nfs/**"
//                    , "/static/**"
//                    , "/druid/**"
//            );
//            if (swaggerEnabled) {
//                // knife4j 增强 swagger 页面
//                web.ignoring().antMatchers(
//                        "/doc.html"
//                        , "/webjars/**"
//                        , "/swagger-resources/**"
//                        , "/v2/api-docs/**"
//                        , "/v3/api-docs/**"
//                );
//            }
//        };
//    }

    @Setter
    @Value("${spring.app.swaggerEnabled:-false}")
    private boolean swaggerEnabled;

    /**
     * 静态资源，无需登录授权
     */
    @Order(1)
    @Bean
    public SecurityFilterChain staticSecurityFilterChain(HttpSecurity http) throws Exception {
        http
                .cors(withDefaults()) // 跨域配置
                // csrf 令牌 <<<<<<
                .csrf(AbstractHttpConfigurer::disable)
                .requestCache().disable()
                .securityContext().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                )
                .requestMatchers(configurer -> {
                    configurer
                            .antMatchers(
                                    "/"
                                    , "/version"
                                    // 健康检查接口
                                    , "/actuator/health/**"
                                    // druid 监控
                                    , "/druid/**"
                                    // resources 下的静态资源
                                    , "/favicon.ico"
                                    , "/nfs/**"
                                    , "/static/**"
                            );
                    if (swaggerEnabled) {
                        // knife4j 增强 swagger 页面
                        configurer.antMatchers(
                                "/doc.html"
                                , "/webjars/**"
                                , "/swagger-resources/**"
                                , "/v2/api-docs/**"
                                , "/v3/api-docs/**"
                        );
                    }
                })
                .headers(headers -> headers
                        .addHeaderWriter((req, res) -> res.addHeader("X-Trace-Id", MDC.get("traceId")))
                        .xssProtection()
                )
                .authorizeHttpRequests(authorize -> authorize
                        .anyRequest().permitAll()
                )
//                .anonymous()
//                .and().servletApi()
//                .and().authorizeRequests()
//                .anyRequest().permitAll()
        ;
        return http.build();
    }

    /**
     * 开放接口，无需登录授权
     */
    @Order(2)
    @Bean
    public SecurityFilterChain openSecurityFilterChain(HttpSecurity http) throws Exception {
        final String anonymousKey = UUID.randomUUID().toString().replace("-", "");
        final Authentication anonymous = new IUser.OpenUser(anonymousKey, null)
                // 没有 token 统一用这个对象作为匿名用户， 避免重复初始化空的匿名用户
                .toAnonymous();
        http
                .requestMatchers(configurer -> {
                    configurer
                            // open 前缀的 url 不需要登录
                            .antMatchers("/open/**")
                    ;
                })
                .cors(withDefaults()) // 跨域配置
                .requestCache().disable()
                //.sessionManagement().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                )
                // csrf 令牌 <<<<<<
                .csrf(csrf -> {
                    csrf.disable();
//                        if (appConfig.isDev()) {
//                            csrf.disable();
//                        } else {
//                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
//                        }
                })
                .headers(headers -> headers
                        .addHeaderWriter((req, res) -> res.addHeader("X-Trace-Id", MDC.get("traceId")))
                        .xssProtection()
                )
                .authorizeHttpRequests(authorize -> authorize
                        .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                        .anyRequest().permitAll()
                )
                .addFilterBefore(
                        // 自定义游客认证逻辑，如果用户已登录，访问开放接口时，使用会话中的用户。
                        // 当使用 token 模式登录是，需要在 controller 主动调用 OpenUser.fillUserByToken() 方法换用户信息
                        new AnonymousAuthenticationFilter(anonymousKey) {
                            @SneakyThrows
                            public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) {
                                String token = Optional.ofNullable(((HttpServletRequest) request).getHeader("x-token"))
                                        .orElseGet(() -> request.getParameter("x-token"));
                                if (StringUtils.isNotBlank(token)) {
                                    if (!tokenRegexp(token)) {
                                        log.error("token格式错误:{}", token);
                                        token = null;
                                    }
                                }
                                final Authentication anonymousAuthentication = Optional.ofNullable(token)
                                        .map(val -> new IUser.OpenUser(anonymousKey, val).toAnonymous())
                                        .orElse(anonymous);
                                SecurityContextHolder.getContext().setAuthentication(anonymousAuthentication);
                                chain.doFilter(request, response);
                            }
                        }
                        , AnonymousAuthenticationFilter.class
                )
                .addFilterBefore(logUriFilter, ChannelProcessingFilter.class)
        ;
        return http.build();
    }

    /**
     * 封闭接口，需登录授权
     */
    @Bean
    public SecurityFilterChain loginSecurityFilterChain(HttpSecurity http) throws Exception {
        http
//                .requestMatchers(configurer -> configurer.antMatchers("/**"))
                .cors(withDefaults()) // 跨域配置
                .requestCache().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                // csrf 令牌 <<<<<<
                .csrf(csrf -> { // https://docs.spring.io/spring-security/reference/servlet/exploits/csrf.html
                    csrf.disable();
//                        if (appConfig.isDev()) {
//                            csrf.disable();
//                        } else {
//                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
//                        }
                })
                // 响应头
                .headers(headers -> headers // https://docs.spring.io/spring-security/reference/servlet/exploits/headers.html
                        // http 响应头链路追踪 id
                        .addHeaderWriter((req, res) -> res.addHeader("X-Trace-Id", MDC.get("traceId")))
                        .xssProtection()
                        // .and().cacheControl() // 启用缓存
                        .and().httpStrictTransportSecurity(hsts -> hsts
                                .includeSubDomains(true)
                                .preload(true)
                                .maxAgeInSeconds(TimeUnit.DAYS.toSeconds(30))
                        )
                )
                // 异常
                //.exceptionHandling(configurer -> configurer
                //        .authenticationEntryPoint((request, response, e) -> {
                //            // 未登录，返回错误码 401
                //            if (log.isErrorEnabled()) {
                //                log.error("authenticationEntryPoint： {}", e.getMessage(), e);
                //            }
                //            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "未登录");
                //        })
                //        .accessDeniedHandler((request, response, e) -> {
                //            // 已登录，无权限，返回错误码 403
                //            if (log.isErrorEnabled()) {
                //                log.error("accessDeniedHandler： {}", e.getMessage(), e);
                //            }
                //            throw Code.A00006.toCodeException("无权限");
                //        })
                //)
                // 请求拦截规则
                .authorizeHttpRequests(authorize -> authorize
                                .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                                .anyRequest().authenticated() // 所有方法都需要登录认证
                        // *************************************************************************************
                        // 建议在类头部或方法头上加权限注解，这里配置太多的权限容易混淆
                        // *************************************************************************************
                        // .antMatchers("/").permitAll()
                        // .antMatchers("/error").permitAll() // HomeController 中需要异常处理方法
                        // *************************************************************************************
                )
                .httpBasic(withDefaults()) // https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/basic.html
                // 登录认证回调
                //.formLogin(configurer -> configurer // https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/form.html
                //        .successHandler((request, response, authentication) -> {
                //            if (log.isDebugEnabled()) {
                //                log.debug("successHandler: {}", JSON.toJsonString(authentication));
                //            }
                //            response.setStatus(HttpServletResponse.SC_OK);
                //        })
                //        .failureHandler((request, response, e) -> {
                //            if (log.isErrorEnabled()) {
                //                log.error("failureHandler: {}", e.getMessage(), e);
                //            }
                //            throw Code.A00002.toCodeException("登录失败");
                //        })
                //)
                // 登出动作
                .logout(configurer -> configurer // https://docs.spring.io/spring-security/reference/servlet/authentication/logout.html
                        .invalidateHttpSession(true) // 指定是否在注销时让HttpSession无效, 默认设置为 true
                        .deleteCookies("JSESSIONID") // 删除 JSESSIONID
                        .addLogoutHandler(new SecurityContextLogoutHandler()) // 添加一个LogoutHandler, 清除 session
                        // 向客户端发送 清除 “cookie、storage、缓存” 消息
                        //.addLogoutHandler(new HeaderWriterLogoutHandler(new ClearSiteDataHeaderWriter(ClearSiteDataHeaderWriter.Directive.ALL)))
                        .addLogoutHandler((request, response, authentication) -> { // 添加一个LogoutHandler, 退出操作需要删除 redis token 缓存
                            final String token = Optional.ofNullable((request).getHeader("x-token"))
                                    .orElseGet(() -> request.getParameter("x-token"));
                            if (StringUtils.isNotBlank(token) && tokenRegexp(token)) {
                                try {
                                    Token.auth(token).destroy();
                                } catch (Exception e) {
                                    log.error(e.getMessage(), e);
                                    // 有可能 token 已经过期了， 所以需要捕获异常，不向前端抛出
                                }
                            }
                        })
                        .logoutSuccessHandler((request, response, authentication) -> response.setStatus(HttpServletResponse.SC_OK))
                )
                // token 模式登录认证
                .addFilterBefore(
                        new OncePerRequestFilter() {
                            @SneakyThrows
                            @Override
                            protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) {
                                final String token = Optional.ofNullable(request.getHeader("x-token"))
                                        .orElseGet(() -> request.getParameter("x-token"));
                                if (StringUtils.isNotBlank(token)) {
                                    try {
                                        if (!tokenRegexp(token)) {
                                            throw new IllegalArgumentException("token格式错误");
                                        }
                                        final LoginUserVO user = Token.auth(token).getUser();
                                        Code.A00001.assertHasTrue(user.isAllow()
                                                , "用户状态异常；isEnabled： %s ，isCredentialsNonExpired： %s ，isAccountNonLocked： %s ，isAccountNonExpired： %s"
                                                , user.isEnabled()
                                                , user.isCredentialsNonExpired()
                                                , user.isAccountNonLocked()
                                                , user.isAccountNonExpired()
                                        );
                                        SecurityContextHolder.getContext().setAuthentication(new LoginUserAuthenticationToken(user));
                                    } catch (Exception e) {
                                        if (e instanceof CodeException) {
                                            log.warn("{}:{}", token, e.getMessage());
                                        } else {
                                            log.error("{}", token, e);
                                        }
                                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "会话超时");
                                        return;
                                    }
                                }
                                filterChain.doFilter(request, response);
                            }
                        }
                        , BasicAuthenticationFilter.class
                )
                .addFilterBefore(logUriFilter, ChannelProcessingFilter.class)
        ;
        // http.authenticationProvider(new AuthenticationProvider()); // 自定义登录验证适配器验证
        return http.build();
    }

    /**
     * 自定义 token 认证对象
     */
    public static class LoginUserAuthenticationToken extends AbstractAuthenticationToken {

        private final LoginUserVO user;

        LoginUserAuthenticationToken(final LoginUserVO user) {
            super(user.getAuthorities());
            super.setAuthenticated(true);
            this.user = user;
        }

        @Override
        public Object getCredentials() {
            return null;
        }

        @Override
        public Object getPrincipal() {
            return user;
        }

        @Override
        public String getName() {
            return user.getUsername();
        }

        @Override
        public boolean isAuthenticated() {
            return true;
        }

        @Override
        public Object getDetails() {
            return user;
        }
    }

    @Setter
    @Value("${spring.app.encrypt.pwd.enabled:-false}")
    private boolean encryptEnabled;
    @Setter
    @Value("${spring.app.encrypt.pwd.type:-rsa}")
    private String encryptType;
    @Setter
    @Value("${spring.app.encrypt.pwd.privateKey:-''}")
    private String encryptPrivateKey;

    /**
     * 开启密码加密传输
     *
     * @return {@link PasswordEncoder}
     */
    // @ConditionalOnExpression("'true'.equals('${spring.app.encrypt.pwd.enabled}') && 'rsa'.equals('${spring.app.encrypt.pwd.type}')")
    @Bean
    public PasswordEncoder rsaBCryptPasswordEncoder() {
        return new BCryptPasswordEncoder() {
            @Override
            public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
                if (encryptEnabled && Objects.equals("rsa", encryptType)) {
                    // rsa 解密之后再验证密码
                    final String decryptPassword = Rsa.decryptByPrivateKey(rawPassword.toString(), encryptPrivateKey);
                    return super.matches(decryptPassword, encodedPassword);
                }
                return super.matches(rawPassword, encodedPassword);
            }
        };
    }

//    @SneakyThrows
//    @Bean
//    public UserDetailsService userDetailsService(final PasswordEncoder passwordEncoder) {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User
//                .withUsername("user")
//                .roles("USER")
//                .password(passwordEncoder.encode("password"))
//                .build()
//        );
//        manager.createUser(User
//                .withUsername("admin")
//                .roles("ADMIN", "USER")
//                .password(passwordEncoder.encode("password"))
//                .build()
//        );
//        return manager;
//    }
//    @Bean
//    public AuthenticationManager myAuthenticationManager(final AuthenticationManagerBuilder builder) throws Exception {
//        builder.inMemoryAuthentication();
//        final DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
//        provider.setHideUserNotFoundExceptions(false);
//        provider.setUserDetailsService(this.userDetailsService);
//        provider.setPasswordEncoder(passwordEncoder());
//        builder.authenticationProvider(provider);
//        builder.userDetailsService(this.userDetailsService).passwordEncoder(passwordEncoder());
//        return builder.build();
//    }
}
//
//import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
//import org.springframework.security.web.csrf.CsrfToken;
//import org.springframework.security.web.csrf.CsrfTokenRepository;
//import org.springframework.security.web.csrf.DefaultCsrfToken;
//import org.springframework.util.Assert;
//import org.springframework.util.StringUtils;
//import org.springframework.web.util.WebUtils;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.UUID;
//
//public final class HeaderCsrfTokenRepository implements CsrfTokenRepository {
//    static final String DEFAULT_CSRF_COOKIE_NAME = "XSRF-TOKEN";
//    static final String DEFAULT_CSRF_PARAMETER_NAME = "_csrf";
//    static final String DEFAULT_CSRF_HEADER_NAME = "X-XSRF-TOKEN";
//    private String parameterName = DEFAULT_CSRF_PARAMETER_NAME;
//    private String headerName = DEFAULT_CSRF_HEADER_NAME;
//    private String cookieName = DEFAULT_CSRF_COOKIE_NAME;
//    private boolean cookieHttpOnly = true;
//    private String cookiePath;
//    private String cookieDomain;
//
//    public HeaderCsrfTokenRepository() {
//    }
//
//    public CsrfToken generateToken(HttpServletRequest request) {
//        return new DefaultCsrfToken(this.headerName, this.parameterName, this.createNewToken());
//    }
//
//    public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
//        String tokenValue = token == null ? "" : token.getToken();
//        Cookie cookie = new Cookie(this.cookieName, tokenValue);
//        cookie.setSecure(request.isSecure());
//        if (this.cookiePath != null && !this.cookiePath.isEmpty()) {
//            cookie.setPath(this.cookiePath);
//        } else {
//            cookie.setPath(this.getRequestContext(request));
//        }
//
//        if (token == null) {
//            cookie.setMaxAge(0);
//        } else {
//            cookie.setMaxAge(-1);
//        }
//
//        cookie.setHttpOnly(this.cookieHttpOnly);
//        if (this.cookieDomain != null && !this.cookieDomain.isEmpty()) {
//            cookie.setDomain(this.cookieDomain);
//        }
//
//        response.addCookie(cookie);
//    }
//
//    public CsrfToken loadToken(HttpServletRequest request) {
//        Cookie cookie = WebUtils.getCookie(request, this.cookieName);
//        if (cookie == null) {
//            return null;
//        } else {
//            String token = cookie.getValue();
//            return !StringUtils.hasLength(token) ? null : new DefaultCsrfToken(this.headerName, this.parameterName, token);
//        }
//    }
//
//    public void setParameterName(String parameterName) {
//        Assert.notNull(parameterName, "parameterName is not null");
//        this.parameterName = parameterName;
//    }
//
//    public void setHeaderName(String headerName) {
//        Assert.notNull(headerName, "headerName is not null");
//        this.headerName = headerName;
//    }
//
//    public void setCookieName(String cookieName) {
//        Assert.notNull(cookieName, "cookieName is not null");
//        this.cookieName = cookieName;
//    }
//
//    public void setCookieHttpOnly(boolean cookieHttpOnly) {
//        this.cookieHttpOnly = cookieHttpOnly;
//    }
//
//    private String getRequestContext(HttpServletRequest request) {
//        String contextPath = request.getContextPath();
//        return contextPath.length() > 0 ? contextPath : "/";
//    }
//
//    public static CookieCsrfTokenRepository withHttpOnlyFalse() {
//        CookieCsrfTokenRepository result = new CookieCsrfTokenRepository();
//        result.setCookieHttpOnly(false);
//        return result;
//    }
//
//    private String createNewToken() {
//        return UUID.randomUUID().toString();
//    }
//
//    public void setCookiePath(String path) {
//        this.cookiePath = path;
//    }
//
//    public String getCookiePath() {
//        return this.cookiePath;
//    }
//
//    public void setCookieDomain(String cookieDomain) {
//        this.cookieDomain = cookieDomain;
//    }
//}
//
