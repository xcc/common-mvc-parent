package com.ccx.demo.feign.baidu.enums;

/**
 * 活体检测控制
 * 默认 NONE
 *
 * @author 谢长春 2022-04-09
 */
public enum LivenessControl {
    NONE("不进行控制"),
    LOW("较低的活体要求(高通过率 低攻击拒绝率)"),
    NORMAL("一般的活体要求(平衡的攻击拒绝率, 通过率)"),
    HIGH("较高的活体要求(高攻击拒绝率 低通过率)"),
    ;
    public final String comment;


    LivenessControl(final String comment) {
        this.comment = comment;
    }
}
