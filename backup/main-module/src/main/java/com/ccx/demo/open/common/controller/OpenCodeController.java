package com.ccx.demo.open.common.controller;

import com.ccx.demo.aop.AppEventRequest;
import com.ccx.demo.config.init.AppConfig;
import com.ccx.demo.open.auth.cache.ImageCodeCache;
import com.ccx.demo.open.auth.vo.ImageCodeVO;
import com.common.annotations.AppSwaggerGroup;
import com.common.annotations.VueSwaggerGroup;
import com.common.annotations.WechatSwaggerGroup;
import com.common.db.entity.Result;
import com.common.util.Util;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Objects;

/**
 * 验证码接口
 *
 * @author 谢长春 on 2017-9-18
 */
@Api(tags = "图片验证码")
@ApiSort(2)
@RequestMapping("/open/code")
@Controller
@Slf4j
@RequiredArgsConstructor
public class OpenCodeController {
    private final AppConfig appConfig;
    private final ImageCodeCache imageCodeCache;

    /**
     * 绘制验证码图形， 返回 base64 图片
     *
     * @param chars char[] 验证码
     * @return String
     */
    @SneakyThrows
    private String draw(final char[] chars) {
        final int width = 80;
        final int height = 30;

        final BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = (Graphics2D) bi.getGraphics();
        // 填充背景
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);
        // 抗锯齿
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        for (int i = 0; i < 2; i++) {
            // 画干扰圆
            g2d.setColor(getRandColor());
            final int w = RandomUtils.nextInt(3, 10);
            g2d.drawOval(RandomUtils.nextInt(0, width / 2), RandomUtils.nextInt(0, height / 2), w, w);

            // 画干扰线
            g2d.setColor(getRandColor());
            final int x = RandomUtils.nextInt(0, width);
            final int y = RandomUtils.nextInt(0, height);
            final int xl = RandomUtils.nextInt(0, width / 4 + 12);
            final int yl = RandomUtils.nextInt(0, height / 4 + 12);
            g2d.drawLine(x, y, x + xl, y + yl);
        }
        // 画字符串
        g2d.setFont(new Font("Arial", Font.BOLD, height / 4 + 12));
        final FontMetrics fontMetrics = g2d.getFontMetrics();
        final int fW = width / chars.length;  // 每一个字符所占的宽度
        final int fSp = (fW - (int) fontMetrics.getStringBounds("8", g2d).getWidth()) / 2;  // 字符的左右边距
        for (int i = 0; i < chars.length; i++) {
            g2d.setColor(getRandColor());
            int fY = height - ((height - (int) fontMetrics.getStringBounds(String.valueOf(chars[i]), g2d).getHeight()) >> 1);  // 文字的纵坐标
            g2d.drawString(String.valueOf(chars[i]), i * fW + fSp + 3, fY - 3);
        }
        g2d.dispose();

        @Cleanup final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bi, "png", byteArrayOutputStream);
        return "data:image/png;base64," + Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
    }

    /*
     * 给定范围获得随机颜色
     */
    private Color getRandColor() {
        int fc = 100, bc = 200;
        final int r = fc + RandomUtils.nextInt(0, bc - fc);
        final int g = fc + RandomUtils.nextInt(0, bc - fc);
        final int b = fc + RandomUtils.nextInt(0, bc - fc);
        return new Color(r, g, b);
    }

    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @GetMapping
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ApiOperation(value = "获取图片验证码"
            , tags = {""}
            , notes = "验证码图片以 base64 格式放在 data 数组中返回"
    )
    @ResponseBody
    public Result<ImageCodeVO> getImageCode() {
        return new Result<ImageCodeVO>().execute(result -> {
            final int a = RandomUtils.nextInt(0, RandomUtils.nextBoolean() ? 10 : 50);
            final int b = RandomUtils.nextInt(0, RandomUtils.nextBoolean() ? 10 : 50);
            String code;
            String exp;
            if (RandomUtils.nextBoolean()) {
                code = Objects.toString(a + b);
                exp = String.format(String.format("%d+%d=?", a, b));
            } else {
                code = Objects.toString(a - b);
                exp = String.format(String.format("%d-%d=?", a, b));
            }
            final ImageCodeVO vo = new ImageCodeVO();
            vo.setUuid(Util.uuid32());
            vo.setBase64Image(draw(exp.toCharArray()));
            imageCodeCache.set(vo.getUuid(), code);
            if (!appConfig.isVerifyCodeEnabled()) {
                vo.setCode(code);
            }
            result.setSuccess(vo);
        });
    }

    @AppEventRequest
    @ApiOperation(value = "校验图片验证码"
            , tags = {""}
            , notes = "仅用于开发环境测试图片验证码输入"
    )
    @GetMapping("/check/{uuid}/{code}")
    @ResponseBody
    @Deprecated
    public Result<Void> checkImageCode(
            @ApiParam(required = true, value = "唯一值", example = "uuid") @PathVariable String uuid
            , @ApiParam(required = true, value = "验证码", example = "a1b2") @PathVariable String code
    ) {
        return new Result<Void>().call(() -> {
            if (appConfig.isVerifyCodeEnabled()) {
                imageCodeCache.asserts(uuid, code);
            }
        });
    }

}
