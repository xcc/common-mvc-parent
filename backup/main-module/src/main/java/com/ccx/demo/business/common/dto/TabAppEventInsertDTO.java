package com.ccx.demo.business.common.dto;

import com.ccx.demo.business.common.entity.TabAppEvent;
import com.ccx.demo.enums.AppEvent;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * DTO：埋点表新增 V20220709
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义埋点表新增时的扩展属性
 *
 * @author 谢长春 on 2022-06-27
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "埋点表")
public class TabAppEventInsertDTO extends TabAppEvent {
    private static final long serialVersionUID = 1L;


    @NotNull
    @Override
    public AppEvent getEvent() {
        return super.getEvent();
    }

    @NotBlank
    @Override
    public String getEventTime() {
        return super.getEventTime();
    }

    @NotNull
    @Override
    public Long getUserId() {
        return super.getUserId();
    }

}
