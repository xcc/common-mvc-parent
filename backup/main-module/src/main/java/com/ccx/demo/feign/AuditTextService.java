package com.ccx.demo.feign;

/**
 * 文字审核
 *
 * @author 谢长春 2022-04-10
 */
public interface AuditTextService {
    /**
     * 文本审核
     *
     * @param text {@link String}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    String auditText(final String text);
}
