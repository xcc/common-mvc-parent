package com.ccx.demo.aop;

import com.ccx.demo.business.common.dto.TabAppEventInsertDTO;
import com.ccx.demo.business.common.service.AppEventService;
import com.ccx.demo.enums.AppEvent;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * \@After Aop 拦截所有 AppEventAfter 记录埋点； 用于没有返回值的方法埋点
 * SPEL： ```http://itmyhome.com/spring/expressions.html```
 *
 * @author 谢长春 2022-04-22
 * 用法：
 * <pre>
 * {@code
 *   @AppEventAfter(comment = "埋点说明文字" // 说明一下这个埋点是干啥的
 *   , condition = "1 == 1 and 'a' == 'a'" // 满足条件才会埋点
 *   , name = "AppEvent.name"              // 枚举参考： com.ccx.demo.business.event.enums.AppEvent
 *   , userId = "#user.id"                 // 用户 id , spEL取值表达式
 *   , longId = "#result.id"               // 业务longId , spEL取值表达式
 *   , stringId = "#result.id"             // 业务stringId , spEL取值表达式
 *   , data = "{\"userId\":\"#user.id\",\"dataId\":\"#body.id\",\"dataName\":\"#body.name\"}" // data : spEL取值表达式，构造新对象
 * )
 * }
 * <pre/>
 */
@Component
@Aspect
@Slf4j
@Order(9999)
@RequiredArgsConstructor
public class AppEventAfterAspect {
    private static final SpelExpressionParser SPEL_EXPRESSION_PARSER;
    private static final DefaultParameterNameDiscoverer DEFAULT_PARAMETER_NAME_DISCOVERER;

    static {
        SPEL_EXPRESSION_PARSER = new SpelExpressionParser();
        DEFAULT_PARAMETER_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();
    }

    private final AppEventService appEventService;

    @After("@annotation(annotation)")
    public void appEventAfter(final JoinPoint joinPoint, AppEventAfter annotation) {
        try {
            final Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
            //final AppEventAfter annotation = method.getAnnotation(AppEventAfter.class);

            final EvaluationContext context = new StandardEvaluationContext();
            if (ArrayUtils.isNotEmpty(joinPoint.getArgs())) { // 检查埋点方法是否有参数
                final Object[] args = joinPoint.getArgs();
                final String[] argNames = Objects.requireNonNull(DEFAULT_PARAMETER_NAME_DISCOVERER.getParameterNames(method), "未获取到方法参数名");
                for (int i = 0; i < args.length; i++) {
                    context.setVariable(argNames[i], args[i]);
                }
            }
            final Boolean isCondition = SPEL_EXPRESSION_PARSER.parseExpression(annotation.condition()).getValue(context, Boolean.class);
            if (Objects.equals(isCondition, Boolean.FALSE)) {
                if (log.isInfoEnabled()) {
                    log.info("不满足埋点条件：{}", annotation);
                }
                return;
            }
            final TabAppEventInsertDTO event = new TabAppEventInsertDTO();
            event.setEvent(AppEvent.valueOf(annotation.name()));
            if (!Strings.isNullOrEmpty(annotation.userId())) { // 处理当前操作用户 id 表达式
                try {
                    final Long userId = SPEL_EXPRESSION_PARSER.parseExpression(annotation.userId()).getValue(context, Long.class);
                    if (log.isDebugEnabled()) {
                        log.debug("userId: {}", userId);
                    }
                    event.setUserId(userId);
                } catch (Exception e) {
                    log.error("埋点用户id获取异常，请检查埋点配置是否正确", e);
                }
            }
            if (!Strings.isNullOrEmpty(annotation.longId())) { // 处理业务 id 表达式
                try {
                    final Long longId = SPEL_EXPRESSION_PARSER.parseExpression(annotation.longId()).getValue(context, Long.class);
                    if (log.isDebugEnabled()) {
                        log.debug("longId: {}", longId);
                    }
                    event.setRefLongId(longId);
                } catch (Exception e) {
                    log.error("埋点关联业务longId获取异常，请检查埋点配置是否正确", e);
                }
            }
            if (!Strings.isNullOrEmpty(annotation.stringId())) { // 处理业务 id 表达式
                try {
                    final String stringId = SPEL_EXPRESSION_PARSER.parseExpression(annotation.stringId()).getValue(context, String.class);
                    if (log.isDebugEnabled()) {
                        log.debug("stringId: {}", stringId);
                    }
                    event.setRefStringId(stringId);
                } catch (Exception e) {
                    log.error("埋点关联业务stringId获取异常，请检查埋点配置是否正确", e);
                }
            }
            if (!Strings.isNullOrEmpty(annotation.data())) { // 处理埋点内容
                try {
                    final String value = SPEL_EXPRESSION_PARSER.parseExpression(annotation.data()).getValue(context, String.class);
                    if (log.isDebugEnabled()) {
                        log.debug("data: {}", value);
                    }
                    if (Objects.nonNull(value)) {
                        event.setData(value);
                    }
                } catch (Exception e) {
                    log.error("埋点data获取异常，请检查埋点配置是否正确", e);
                }
            }
            appEventService.insert(event, event.getUserId());
        } catch (Exception e) {
            log.error("埋点异常：{}", joinPoint, e);
        }
    }
}
