package com.ccx.demo.config.convert;

import com.common.util.Dates;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.common.util.Dates.Pattern.yyyy_MM_dd_HH_mm_ss_SSS;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToDateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String value) {
        return yyyy_MM_dd_HH_mm_ss_SSS.parseOfNullable(value).map(Dates::date).orElse(null);
    }
}
