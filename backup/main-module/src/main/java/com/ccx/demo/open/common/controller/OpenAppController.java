package com.ccx.demo.open.common.controller;

import com.ccx.demo.aop.AppEventRequest;
import com.ccx.demo.business.common.dto.JsonFileDTO;
import com.ccx.demo.business.common.service.FileService;
import com.ccx.demo.config.init.AppConfig;
import com.ccx.demo.open.common.vo.AppStaticVO;
import com.common.annotations.AppSwaggerGroup;
import com.common.annotations.VueSwaggerGroup;
import com.common.annotations.WechatSwaggerGroup;
import com.common.db.entity.Result;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.OPTIONS;

/**
 * @author 谢长春 2022-06-28
 */
@RequiredArgsConstructor
@Controller
public class OpenAppController {

    private static final JsonFileDTO openAppStaticJsonFileDTO;

    static {
        openAppStaticJsonFileDTO = new JsonFileDTO("static.json", "app/static.json");
    }

    private final AppConfig appConfig;
    private final FileService fileService;

    /**
     * swagger 文档
     *
     * @return {@link String}
     */
    @ApiIgnore
    @AppEventRequest
    @GetMapping(value = "/")
    public String doc() {
        return "redirect:/doc.html";
    }

    /**
     * 用于获取 csrf token
     *
     * @return {@link String}
     */
    @ApiIgnore
    @AppEventRequest
    @RequestMapping(value = "/", method = {HEAD, OPTIONS})
    @ResponseBody
    public String csrf() {
        return "ok";
    }

    /**
     * 查看应用版本号
     *
     * @return {@link Result}
     */
    @AppEventRequest(skip = true)
    @ApiIgnore
    @GetMapping("/version")
    @ResponseBody
    public Result<String[]> version() {
        return new Result<String[]>().execute(result -> {
            result.setSuccess(new String[]{
                    appConfig.getGitCommitId(),
                    appConfig.getBuildTime(),
                    appConfig.getIp(),
                    appConfig.getVersion(),
                    appConfig.getStartTime()
            });
        });
    }

    /**
     * 应用静态资源
     */
    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @GetMapping("/open/app/static")
    @ApiOperation(value = "应用静态资源"
            , tags = {""}
            , notes = "h5、json静态资源"
            + "<br>应用每次打开都要通过这个接口检查静态资源版本号"
            + "<br>这个文件内容应该放到nginx下，现在放在这里的目的是可以通过接口文档查看"
    )
    @ApiOperationSupport(author = "谢长春")
    @ResponseBody
    public Result<AppStaticVO> appStatic() {
        return new Result<AppStaticVO>().execute(result -> {
            result.setSuccess(appConfig.getStaticList());
        });
//    public RedirectView appStatic() {
//        if (!fileService.exists(openAppStaticJsonFileDTO)) {
//            final String jsonText = new Result<AppStaticVO>()
//                    .execute(result -> result.setSuccess(appConfig.getStaticList()))
//                    .json();
//            fileService.write(jsonText, openAppStaticJsonFileDTO, true);
//        }
//        final RedirectView redirectView = new RedirectView(openAppStaticJsonFileDTO.getUrl());
//        //redirectView.setStatusCode(HttpStatus.MOVED_PERMANENTLY); // 301
//        redirectView.setStatusCode(HttpStatus.FOUND); // 302
//        return redirectView;
    }
}
