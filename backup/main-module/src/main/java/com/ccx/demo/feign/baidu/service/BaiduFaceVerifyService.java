package com.ccx.demo.feign.baidu.service;

import com.ccx.demo.feign.baidu.api.BaiduFaceIdCardApiService;
import com.ccx.demo.feign.baidu.api.BaiduFaceVerifyApiService;
import com.ccx.demo.feign.baidu.api.BaiduTokenApiService;
import com.ccx.demo.feign.baidu.cache.BaiduFaceTokenCache;
import com.ccx.demo.feign.baidu.dto.BaiduPersonIdMatchDTO;
import com.ccx.demo.feign.baidu.dto.BaiduPersonVerifyDTO;
import com.ccx.demo.feign.baidu.vo.BaiduBaseVO;
import com.ccx.demo.feign.baidu.vo.BaiduPersonVerifyVO;
import com.ccx.demo.feign.baidu.vo.BaiduTokenVO;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * 百度实名认证服务
 *
 * @author 谢长春 2022-04-09
 */
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.feign.baidu.face.enabled", havingValue = "true")
@ConfigurationProperties("spring.app.feign.baidu.face")
public class BaiduFaceVerifyService {
    private static final String GRANT_TYPE = "client_credentials";
    private final BaiduFaceTokenCache tokenCache;
    private final BaiduTokenApiService baiduTokenApiService;
    private final BaiduFaceVerifyApiService baiduFaceVerifyApiService;
    private final BaiduFaceIdCardApiService baiduFaceIdCardApiService;

    /**
     * 百度身份验证应用配置: 应用id
     */
    @Setter
    private String appId;
    /**
     * 百度身份验证应用配置: api调用钥匙
     */
    @Setter
    private String apiKey;
    /**
     * 百度身份验证应用配置: api调用密钥
     */
    @Setter
    private String secretKey;

    /**
     * 获取百度 token
     *
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO getToken() {
        return tokenCache.optional().orElseGet(this::refreshToken);
    }

    /**
     * 刷新百度 token 并写入缓存
     *
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO refreshToken() {
        final ResponseEntity<BaiduTokenVO> responseEntity = baiduTokenApiService.call(GRANT_TYPE, apiKey, secretKey);
        Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
        final BaiduTokenVO token = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为 null")
                .initCacheTime();
        token.assertErrorCode();
        tokenCache.set(token);
        if (log.isInfoEnabled()) {
            log.info("百度 token： {}", token);
        }
        return token;
    }

    /**
     * 人脸实名认证
     *
     * @param dto           {@link BaiduPersonVerifyDTO}
     * @param errorConsumer {@link Consumer}
     * @return {@link Boolean}
     */
    public boolean personVerify(BaiduPersonVerifyDTO dto, Consumer<String> errorConsumer) {
        final BaiduTokenVO token = getToken();
        final ResponseEntity<BaiduPersonVerifyVO> responseEntity = baiduFaceVerifyApiService.call(token.getAccess_token(), dto);
        if (log.isDebugEnabled()) {
            log.debug("{}", responseEntity.getBody());
        }
        Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
        final BaiduPersonVerifyVO vo = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为null");
        log.info("(0 == errorCode):{}, (score >= 80f):{}", vo.success(), vo.scoreSuccess());
        if (vo.success()) {
            if (vo.scoreSuccess()) {
                return true;
            }
            final String errMsg = String.format("公安验证失败，人脸匹配打分不足80分：%s", vo.getScore().orElse(null));
            if (Objects.nonNull(errorConsumer)) {
                errorConsumer.accept(errMsg);
            }
            log.error(errMsg);
        } else {
            final String errMsg = String.format("公安验证失败，%d：%s", vo.getErrorCode(), vo.getErrorMsg());
            if (Objects.nonNull(errorConsumer)) {
                errorConsumer.accept(errMsg);
            }
            log.error(errMsg);
        }
        return false;
    }

    /**
     * 身份证与名字比对
     * 根据error_code判断，为0时表示匹配为同一个人。否则按错误码表的定义，如222351表示身份证号码与名字不匹配。
     *
     * @param dto {@link BaiduPersonIdMatchDTO}
     * @return {@link Boolean}
     */
    public boolean personIdMatch(BaiduPersonIdMatchDTO dto) {
        final BaiduTokenVO token = getToken();
        ResponseEntity<BaiduBaseVO> responseEntity = baiduFaceIdCardApiService.call(token.getAccess_token(), dto);
        if (log.isDebugEnabled()) {
            log.debug("{}", responseEntity.getBody());
        }
        Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> String.format("百度接口请求失败:%d", responseEntity.getStatusCodeValue()));
        final BaiduBaseVO vo = Objects.requireNonNull(responseEntity.getBody(), "response.body 不能为null");
        if (vo.success()) {
            return true;
        } else {
            log.error("身份证与姓名不匹配，{}：{}", vo.getErrorCode(), vo.getErrorMsg());
        }
        return false;
    }
}
