package com.ccx.demo.aop;

import com.common.util.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Objects;

/**
 * 拦截所有 service 、 dao 层方法入参打印到日志
 *
 * @author 谢长春 2018-10-4
 */
@Component
@Aspect
@Slf4j
@Order(9999)
public class LogAspect {
    /**
     * 打印参数
     */
    @Before("execution(* com.ccx.demo..*.controller..*.*(..))" +
            "||execution(* com.ccx.demo..*.service..*.*(..))" +
            "||execution(* com.ccx.demo..*.cache..*.*(..))" +
            "||execution(* com.ccx.demo..*.feign..*.*(..))" +
            "||execution(* com.ccx.demo..*.api..*.*(..))" +
            "||execution(* com.ccx.demo..*.dao..*.*(..))")
    public void before(final JoinPoint joinPoint) {
        if (log.isInfoEnabled()) {
            log.info("{} => {}", joinPoint, Arrays.toString(joinPoint.getArgs()).replaceAll("\\w+=null, ", ""));
        }
    }

    /**
     * 打印参数
     */
    @Before("execution(* com.common.db.entity.*.*(..))")
    public void beforeCache(final JoinPoint joinPoint) {
        if (log.isInfoEnabled()) {
            log.info("{} => {}", joinPoint, Arrays.toString(joinPoint.getArgs()).replaceAll("\\w+=null, ", ""));
        }
    }

    @Pointcut("execution(* com.ccx.demo..*.controller..*.*(..))")
    public void point() {
    }
    private LocalTime time;
    @Before(value = "point()")
    public void before() {
        time = LocalTime.now();
    }

    /**
     * 打印执行时间和响应结果
     */
    @AfterReturning(value = "point()", returning = "result")
    public void afterReturn(JoinPoint joinPoint, Object result) {
        if (result instanceof SseEmitter) {
            log.info("SseEmitter");
            return;
        }
        if (log.isInfoEnabled()) {
            log.info("tps: {} ms({}-{})", Duration.between(time, LocalTime.now()).toMillis(), time, LocalTime.now());
        }
        if (log.isDebugEnabled()) {
            log.info("response: {}", Objects.isNull(result) ? "" : JSON.toJsonString(result));
        }
    }
}
