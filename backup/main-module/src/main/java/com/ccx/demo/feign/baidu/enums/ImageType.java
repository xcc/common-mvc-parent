package com.ccx.demo.feign.baidu.enums;

/**
 * 图片类型
 *
 * @author 谢长春 2022-04-09
 */
public enum ImageType {
    BASE64("图片的base64值，base64编码后的图片数据，编码后的图片大小不超过2M；图片尺寸不超过1920*1080"),
    URL("图片的 URL地址( 可能由于网络等原因导致下载图片时间过长)；FACE_TOKEN**: 人脸图片的唯一标识，调用人脸检测接口时，会为每个人脸图片赋予一个唯一的FACE_TOKEN，同一张图片多次检测得到的FACE_TOKEN是同一个。"),
    ;
    public final String comment;

    ImageType(final String comment) {
        this.comment = comment;
    }
}
