package com.ccx.demo.open.auth.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 图片验证码
 *
 * @author 谢长春 2022-06-24
 */
@Getter
@Setter
@ToString
@ApiModel(description = "图片验证码")
public class ImageCodeVO {
    /**
     * 唯一值，验证用户输入时需要比对 uuid 和 code 是否对应
     */
    @ApiModelProperty(value = "唯一值，验证用户输入时需要比对 uuid 和 code 是否对应", example = "000000000000000000000000")
    private String uuid;
    /**
     * 验证码，验证用户输入时需要比对 uuid 和 code 是否对应
     */
    @ApiModelProperty(value = "验证码，验证用户输入时需要比对 uuid 和 code 是否对应", example = "0000")
    private String code;
    /**
     * 验证码图片，base64格式
     */
    @ApiModelProperty(value = "验证码图片，base64格式", example = "data:image/png;base64,AAAAAAAAAAAAAAAA")
    private String base64Image;
}
