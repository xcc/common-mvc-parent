package com.ccx.demo.business.user.dao.jpa;

import com.ccx.demo.business.common.dto.UserAvatarImageDTO;
import com.ccx.demo.business.user.cache.LoginCache;
import com.ccx.demo.business.user.entity.QTabUser;
import com.ccx.demo.business.user.entity.TabUser;
import com.ccx.demo.enums.AppCode;
import com.common.db.ITableCache;
import com.common.db.IWhere;
import com.common.db.entity.Page;
import com.common.enums.Limit;
import com.common.util.Dates;
import com.google.common.collect.ObjectArrays;
import com.google.common.collect.Sets;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.util.CollectionUtils;

import javax.persistence.NonUniqueResultException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.ccx.demo.config.init.AppInit.getQueryFactory;
import static com.common.util.Dates.Pattern.yyyyMMddHHmmssSSS;

/**
 * 数据操作：用户
 *
 * @author 谢长春 on 2017/10/26
 */
public interface UserRepository extends
        ITableCache.CacheRepository<Long, TabUser>,
        LoginCache.CacheRepository,
        JpaRepository<TabUser, Long>,
        org.springframework.data.querydsl.QuerydslPredicateExecutor<TabUser> {
    // 每个 DAO 层顶部只能有一个查询实体,且必须以 q 命名,表示当前操作的数据库表. 当 q 作为主表的连接查询方法也必须写在这个类
    QTabUser table = QTabUser.tabUser;

    /**
     * 用户 新增数据
     * <pre>
     * 注意：
     *   原来通过 AOP 和数据库设置的默认值迁移到这里。
     *   因为 JPA 特性导致新增之后获取不到时间和数据库的默认值，
     *   所以这里把需要有默认值的数据库字段都预设值好，同时兼容不支持设置默认值的数据库
     * </pre>
     *
     * @param userId {@link Long} 操作用户ID
     * @param obj    {@link TabUser} 新增对象
     * @return {@link TabUser}
     */
    default TabUser insert(final Long userId, final TabUser obj) {
        obj.insert(Dates.now(), userId);
        return save(obj);
    }

    /**
     * 用户 新增数据
     * <pre>
     * 注意：
     *   原来通过 AOP 和数据库设置的默认值迁移到这里。
     *   因为 JPA 特性导致新增之后获取不到时间和数据库的默认值，
     *   所以这里把需要有默认值的数据库字段都预设值好，同时兼容不支持设置默认值的数据库
     * </pre>
     *
     * @param userId {@link Long} 操作用户ID
     * @param list   {@link List<TabUser>} 新增对象
     * @return {@link List<TabUser>}
     */
    default List<TabUser> insert(final Long userId, final List<TabUser> list) {
        final Dates now = Dates.now();
        list.forEach(obj -> obj.insert(now, userId));
        return saveAll(list);
    }

    /**
     * 用户 更新数据
     *
     * @param id     {@link Long} 数据ID
     * @param userId {@link Long} 操作用户ID
     * @param obj    {@link TabUser} 更新对象
     * @return {@link Long} 更新行数
     */
    default long update(final Long id, final Long userId, final TabUser obj) {
        final BooleanExpression whereBooleanExpression = table.id.eq(id)
                .and(table.updateTime.eq(obj.getUpdateTime()))
                .and(table.deleted.eq(false))
                .and(table.hidden.eq(false));
        obj.setUpdateUserId(userId);
        obj.setUpdateTime(yyyyMMddHHmmssSSS.now()); // 强制设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
        return obj.update(getQueryFactory().update(table))
                .get()
                .set(table.updateUserId, userId)
                .set(table.updateTime, obj.getUpdateTime())
                .where(whereBooleanExpression)
                .execute();
    }

    /**
     * 用户 按 id 逻辑删除
     *
     * @param id     {@link Long} 数据id
     * @param userId {@link Long} 操作用户ID
     * @return {@link Long} 逻辑删除行数
     */
    default long markDeleteById(final Long id, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.deleted, true)
                .set(table.updateUserId, userId)
                .set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 强制设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
                .where(table.id.eq(id)
                        .and(table.deleted.eq(false))
                        .and(table.hidden.eq(false))
                )
                .execute();
    }

    /**
     * 用户 按 id+updateTime 逻辑删除
     *
     * @param id     {@link Long} 数据id
     * @param userId {@link Long} 操作用户ID
     * @return {@link Long} 逻辑删除行数
     */
    default long markDeleteById(final Long id, final String updateTime, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.deleted, true)
                .set(table.updateUserId, userId)
                .set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 强制设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
                .where(table.id.eq(id)
                        .and(table.deleted.eq(false))
                        .and(table.updateTime.eq(updateTime))
                        .and(table.hidden.eq(false))
                )
                .execute();
    }

    /**
     * 用户 按 id 批量逻辑删除
     *
     * @param ids    {@link Set<Long>} 数据 id 集合
     * @param userId {@link Long} 操作用户ID
     * @return {@link Long} 逻辑删除行数
     */
    default long markDeleteByIds(final Set<Long> ids, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.deleted, true)
                .set(table.updateUserId, userId)
                .set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒'
                .where(table.id.in(ids)
                        .and(table.deleted.eq(false))
                        .and(table.hidden.eq(false))
                )
                .execute();
    }

    /**
     * 用户 按 id+updateTime 批量逻辑删除
     *
     * @param ids         {@link Set<Long>} 数据ID
     * @param updateTimes Set<String> 数据最后更新时间
     * @param userId      {@link Long} 操作用户ID
     * @return {@link Long} 逻辑删除行数
     */
    default long markDeleteByIds(final Set<Long> ids, final Set<String> updateTimes, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.deleted, true)
                .set(table.updateUserId, userId)
                .set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
                .where(table.id.in(ids)
                        .and(table.deleted.eq(false))
                        .and(table.hidden.eq(false))
                        .and(CollectionUtils.isEmpty(updateTimes) ? null : table.updateTime.in(updateTimes))
                )
                .execute();
    }

    /**
     * 用户 按 条件查询单条数据，如果查询结果超过 1 条数据则抛出异常
     *
     * @param condition {@link TabUser} 查询条件
     * @return {@link Optional<TabUser>} 实体对象
     */
    default Optional<TabUser> findOne(final TabUser condition) {
        return findOne(condition.where());
    }

    /**
     * 用户 按 条件查询单条数据，如果查询结果超过 1 条数据则抛出异常
     *
     * @param where {@link IWhere.QdslWhere} 查询条件
     * @return {@link Optional<TabUser>} 实体对象
     */
    default Optional<TabUser> findOne(final IWhere.QdslWhere where) {
        final List<TabUser> list = getQueryFactory()
                .selectFrom(table)
                .where(where.toPredicate())
                .limit(Limit.L2.value)
                .fetch();
        if (list.size() > 1) {
            throw new NonUniqueResultException("预期查询结果行数为1，实际查询结果行数大于1");
        }
        return list.stream().findFirst();
    }

    /**
     * 用户 按 ID 批量查询，以 id 为 key 返回map
     *
     * @param ids {@link Set<Long>} 数据ID
     * @return {@link Map <Long, TabUser>} Map 对象集合
     */
    default Map<Long, TabUser> mapByIds(final Collection<Long> ids) {
        return findAllById(Sets.newHashSet(ids))
                .stream()
                .collect(Collectors.toMap(TabUser::getId, TabUser::cloneObject));
    }

    /**
     * 用户 按 ID 批量查询 id + 指定字段，返回 map ， id 为 key ， 指定字段为 value
     *
     * @param ids {@link Set<Long>} 数据ID
     * @param exp {@link QTabUser} 指定单个字段
     * @return {@link Map<Long, R>} Map 对象集合
     */
    default <R> Map<Long, R> mapByIds(final Collection<Long> ids, final Expression<R> exp) {
        final Map<Long, R> map = new HashMap<>(ids.size());
        getQueryFactory()
                .select(table.id, exp)
                .from(table)
                .where(table.id.in(ids))
                .fetch()
                .forEach(tuple -> {
                    if (Objects.nonNull(tuple.get(exp))) {
                        map.put(tuple.get(table.id), tuple.get(exp));
                    }
                });
        return map;
    }

    /**
     * 用户 按 ID 批量查询 id + 指定字段，返回 map ， id 为 key ， 指定字段投影到 {@link TabUser}
     *
     * @param ids  {@link Set<Long>} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return {@link Map<Long, TabUser>} Map 对象集合
     */
    default Map<Long, TabUser> mapByIds(final Collection<Long> ids, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabUser.class, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(ids))
                .fetch()
                .stream()
                .collect(Collectors.toMap(TabUser::getId, row -> row));
    }

    /**
     * 用户 按 ID 批量查询 id + 指定字段，返回 map ， id 为 key ， 指定字段投影到 Class<R>
     *
     * @param ids  {@link Set<Long>} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return {@link Map<Long, R>} Map 对象集合
     */
    default <R> Map<Long, R> mapByIds(final Collection<Long> ids, final Class<R> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(table.id, Projections.bean(clazz, exps))
                .from(table)
                .where(table.id.in(ids))
                .fetch()
                .stream()
                .collect(Collectors.toMap(tuple -> tuple.get(table.id), tuple -> Objects.requireNonNull(tuple.get(1, clazz))));
    }

    /**
     * 用户 求总数
     *
     * @param where IWhere.QdslWhere 查询条件
     * @return long 总数
     */
    default long count(final IWhere.QdslWhere where) {
        return getQueryFactory()
                .select(table.id.count())
                .from(table)
                .where(where.toPredicate())
                .fetch()
                .stream()
                .findFirst()
                .orElse(0L);
    }

    /**
     * 用户 按条件分页查询， 仅返回 id 字段，用于分页查询优化
     *
     * @param condition {@link TabUser} 查询条件
     * @param page      {@link Page} 分页
     * @return {@link QueryResults<Long>} 分页结果集
     */
    default QueryResults<Long> pageIds(final TabUser condition, final Page page) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<Long> list = getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .offset(page.offset())
                .limit(page.limit())
                .fetch();
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * 用户 按条件分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @param page      {@link Page} 分页
     * @return {@link QueryResults<TabUser>} 分页结果集
     */
    default QueryResults<TabUser> page(final TabUser condition, final Page page) {
        { // 直接查询
            // return getQueryFactory()
            //        .selectFrom(table)
            //        .where(condition.where().toArray())
            //        .offset(page.offset())
            //        .limit(page.limit())
            //        .orderBy(condition.qdslOrderBy())
            //        .fetchResults();
        }
        { // 查询优化版
            final long count = count(condition.where());
            if (count == 0L) {
                return QueryResults.emptyResults();
            }
            final List<Long> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
            final List<TabUser> list = listByIds(ids); // 再按 id 批量查询
            return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
        }
    }

    /**
     * 用户 按条件分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @param page      {@link Page} 分页
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link QueryResults<TabUser>} 分页结果集
     */
    default QueryResults<TabUser> page(final TabUser condition, final Page page, final Expression<?>... exps) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<Long> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        final List<TabUser> list = listByIds(ids, exps); // 再按 id 批量查询
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * 用户 按条件分页查询，投影到 VO 类
     *
     * @param condition {@link TabUser} 查询条件
     * @param page      {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link QueryResults<TabUser>} 分页结果集
     */
    default <T extends TabUser> QueryResults<T> page(final TabUser condition, final Page page, final Class<T> clazz) {
        return page(condition, page, clazz, TabUser.allColumnAppends());
    }

    /**
     * 用户 按条件分页查询，查询指定字段，投影到 VO 类
     *
     * @param condition {@link TabUser} 查询条件
     * @param page      {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link QueryResults<TabUser>} 分页结果集
     */
    default <T extends TabUser> QueryResults<T> page(final TabUser condition, final Page page, final Class<T> clazz, final Expression<?>... exps) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<Long> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        final List<T> list = listByIds(ids, clazz, exps); // 再按 id 批量查询
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * 用户 按条件查询返回 id ，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<Long>} ID集合
     */
    default List<Long> listIds(final TabUser condition) {
        return listIds(Limit.L1000.value, condition);
    }

    /**
     * 用户 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<Long>} ID集合
     */
    default List<Long> listIds(final long limit, final TabUser condition) {
        return getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 用户 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param offset    {@link Long} 跳过行数
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<Long>} ID集合
     */
    default List<Long> listIds(final long offset, final long limit, final TabUser condition) {
        return getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .offset(offset)
                .limit(limit)
                .fetch();
    }

    /**
     * 用户 按 ID 批量查询
     *
     * @param ids {@link Long} 数据ID
     * @return {@link List<TabUser>} 实体对象集合
     */
    default List<TabUser> listByIds(final Collection<Long> ids) {
        final Map<Long, TabUser> map = findAllById(Sets.newHashSet(ids)).stream().collect(Collectors.toMap(TabUser::getId, TabUser::cloneObject));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 用户 按 ID 批量查询 id + 指定字段，返回 list , 返回结果保持同传入的 id 顺序一致
     *
     * @param ids  {@link Long} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return {@link List<TabUser>} 实体对象集合
     */
    default List<TabUser> listByIds(final Collection<Long> ids, final Expression<?>... exps) {
        final Map<Long, TabUser> map = getQueryFactory()
                .select(Projections.bean(TabUser.class, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(TabUser::getId, row -> row));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 用户 按 ID 批量查询 id + 指定字段，返回 list , 返回结果保持同传入的 id 顺序一致
     *
     * @param ids  {@link Long} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return {@link List<R>} 实体对象集合
     */
    default <R> List<R> listByIds(final Collection<Long> ids, final Class<R> clazz, final Expression<?>... exps) {
        final Map<Long, R> map = getQueryFactory()
                .select(table.id, Projections.bean(clazz, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(tuple -> tuple.get(table.id), tuple -> Objects.requireNonNull(tuple.get(1, clazz))));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 用户 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<TabUser>} 实体对象集合
     */
    default List<TabUser> list(final TabUser condition) {
        return list(Limit.L1000.value, condition);
    }

    /**
     * 用户 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<TabUser>} 实体对象集合
     */
    default List<TabUser> list(final long limit, final TabUser condition) {
        return getQueryFactory()
                .selectFrom(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 用户 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<TabUser>} 实体对象集合
     */
    default <R> List<R> list(final TabUser condition, final Expression<R> exp) {
        return list(Limit.L1000.value, condition, exp);
    }

    /**
     * 用户 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @return {@link List<TabUser>} 实体对象集合
     */
    default <R> List<R> list(final long limit, final TabUser condition, final Expression<R> exp) {
        return getQueryFactory()
                .select(exp)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 用户 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabUser>} 实体对象集合
     */
    default List<TabUser> list(final TabUser condition, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, exps);
    }

    /**
     * 用户 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabUser>} 实体对象集合
     */
    default List<TabUser> list(final long limit, final TabUser condition, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabUser.class, exps))
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 用户 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link List<TabUser>} 实体对象集合
     */
    default <T extends TabUser> List<T> list(final TabUser condition, final Class<T> clazz) {
        return list(Limit.L1000.value, condition, clazz, TabUser.allColumnAppends());
    }

    /**
     * 用户 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link List<TabUser>} 实体对象集合
     */
    default <T extends TabUser> List<T> list(final long limit, final TabUser condition, final Class<T> clazz) {
        return list(limit, condition, clazz, TabUser.allColumnAppends());
    }

    /**
     * 用户 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabUser} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabUser>} 实体对象集合
     */
    default <T extends TabUser> List<T> list(final TabUser condition, final Class<T> clazz, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, clazz, exps);
    }

    /**
     * 用户 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabUser} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabUser>} 实体对象集合
     */
    default <T extends TabUser> List<T> list(final long limit, final TabUser condition, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 用户 循环查询列表，使用 id 正序排列，用于批处理数据，类似分页查询批量导出。
     * <pre>
     * 注意：
     *   这里必须使用 id 排序(正序|倒序)，否则会导致数据重复查询，也可能漏掉一部分数据。
     * </pre>
     *
     * @param listConsumer List<TabUser> 查询结果
     * @param where        IWhere.QdslWhere 查询条件
     * @param exps         Expression 查询字段
     */
    default void forEach(final Consumer<List<TabUser>> listConsumer,
                         final IWhere.QdslWhere where,
                         final Expression<?>... exps
    ) {
        Long id = null;
        final int limit = Limit.L1000.value;
        List<TabUser> list;
        final Expression<?>[] columns = Optional.ofNullable(exps)
                .filter(arr -> arr.length > 0)
                .orElseGet(TabUser::allColumnAppends);
        do {
            list = getQueryFactory()
                    .select(Projections.bean(TabUser.class, columns))
                    .from(table)
                    .where(Optional.ofNullable(id).map(table.id::gt).orElse(null))
                    .where(where.toPredicate())
                    .orderBy(table.id.asc())
                    .limit(limit)
                    .fetch();
            if (!list.isEmpty()) {
                id = Objects.requireNonNull(list.get(list.size() - 1).getId());
                listConsumer.accept(list);
            }
        } while (Objects.equals(list.size(), limit));
    }

    //    /**
//     * 查询部分字段，这些字段会被缓存到 redis
//     *
//     * @return List<TabUser>
//     */
//    default List<TabUser> getSimpleList() {
//        return getQueryFactory()
//                .select(Projections.bean(TabUser.class, table.id, table.username, table.nickname, table.phone, table.email, table.roles))
//                .from(q)
//                .fetch();
////                .stream()
////                .map(row -> TabUser.builder()
////                        .id(row.get(table.id))
////                        .domain(row.get(table.domain))
////                        .username(row.get(table.username))
////                        .phone(row.get(table.phone))
////                        .email(row.get(table.email))
////                        .nickname(row.get(table.nickname))
////                        .role(row.get(table.role))
////                        .build()
////                ).collect(Collectors.toList());
//    }

    /**
     * 按【登录账户、手机号、邮箱】查找用户
     *
     * @param username {@link String} 登录账户名
     * @return Optional<TabUser>
     */
    default Optional<TabUser> findLoginUser(final String username) {
        Optional<TabUser> optional = findOne(table.username.eq(username));
        if (!optional.isPresent()) {
            optional = findOne(table.phone.eq(username));
        }
        if (!optional.isPresent()) {
            optional = findOne(table.email.eq(username));
        }
        return optional.map(TabUser::cloneObject);
    }

    /**
     * 检查账户是否存在，存在则抛出异常
     *
     * @param username {@link String} 登录用户名
     * @param phone    {@link String} 手机号
     * @param email    {@link String} 邮箱
     */
    default void assertUserExist(final String username, final String phone, final String email) {
        if (exists(table.username.eq(username))) {
            throw AppCode.B01008.toCodeException(username);
        }
        if (StringUtils.isNotBlank(phone)) {
            if (exists(table.phone.eq(phone))) {
                throw AppCode.B01007.toCodeException(phone);
            }
        }
        if (StringUtils.isNotBlank(email)) {
            if (exists(table.email.eq(email))) {
                throw AppCode.B01006.toCodeException(email);
            }
        }
    }

    /**
     * 修改密码
     *
     * @param id       {@link Long} 用户ID
     * @param password {@link String} 新密码
     * @param userId   {@link Long} 修改者ID
     * @return long 影响行数
     */
    @Modifying
    @Query
    default long updatePassword(final Long id, final String password, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.password, password)
                .set(table.updateUserId, userId)
                .where(table.id.eq(id))
                .execute();
    }

    /**
     * 修改禁用、启用状态
     *
     * @param id       {@link Long} 用户ID
     * @param disabled {@link String} 新密码
     * @param userId   {@link Long} 修改者ID
     * @return long 影响行数
     */
    @Modifying
    @Query
    default long updateDisabled(final Long id, final Boolean disabled, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.disabled, disabled)
                .set(table.updateUserId, userId)
                .where(table.id.eq(id))
                .execute();
    }

    /**
     * 修改昵称
     *
     * @param id       {@link Long} 用户ID
     * @param nickname {@link String} 昵称
     * @param userId   {@link Long} 修改者ID
     * @return long 影响行数
     */
    @Modifying
    @Query
    default long updateNickname(final Long id, final String nickname, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.nickname, nickname)
                .set(table.updateUserId, userId)
                .where(table.id.eq(id))
                .execute();
    }

    /**
     * 修改头像
     *
     * @param id     {@link Long} 用户ID
     * @param avatar {@link UserAvatarImageDTO} 用户头像
     * @param userId {@link Long} 修改者ID
     * @return long 影响行数
     */
    @Modifying
    @Query
    default long updateAvatar(final Long id, final UserAvatarImageDTO avatar, final Long userId) {
        return getQueryFactory()
                .update(table)
                .set(table.avatar, avatar)
                .set(table.updateUserId, userId)
                .where(table.id.eq(id))
                .execute();
    }
    /**
     * 销毁账号，重新注册
     *
     * @param id       {@link Long} 数据ID
     * @return {@link Long} 更新行数
     */
    default long clear(final Long id) {
        return getQueryFactory()
                .update(table)
                .set(table.oldPhone, table.phone)
                .set(table.username, RandomStringUtils.randomAlphanumeric(16).toLowerCase())
                .set(table.phone, "")
                .set(table.email, "")
                .set(table.disabled, true)
                .set(table.updateTime, yyyyMMddHHmmssSSS.now())
                .where(table.id.eq(id)
                        .and(table.hidden.eq(false))
                )
                .execute();
    }
//    /**
//     * 启用账户
//     *
//     * @param id     Long 用户ID
//     * @param userId Long 修改者ID
//     * @return long 影响行数
//     */
//    @Modifying
//    @Query
//    default long enable(final Long id, final Long userId) {
//        return getQueryFactory()
//                .update(q)
//                .set(table.deleted, false)
//                .set(table.updateUserId, userId)
//                .where(table.id.eq(id))
//                .execute();
//    }
//
//    /**
//     * 激活账户
//     *
//     * @param id     Long 用户ID
//     * @param userId Long 修改者ID
//     * @return long 影响行数
//     */
//    @Modifying
//    @Query
//    default long activate(final Long id, final Long userId) {
//        return getQueryFactory()
//                .update(q)
//                .set(table.expired, false)
//                .set(table.updateUserId, userId)
//                .where(table.id.eq(id))
//                .execute();
//    }

}
