package com.ccx.demo.feign.baidu.cache;

import com.ccx.demo.feign.baidu.vo.BaiduTokenVO;
import com.common.db.ICache;
import com.common.util.JSON;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 百度 token 缓存
 *
 * @author 谢长春 2022-04-09
 */
@Component
@RequiredArgsConstructor
public class BaiduAuditTokenCache implements ICache {
    private static final String CACHE_KEY = BaiduAuditTokenCache.class.getSimpleName();

    private final RedissonClient redissonClient;

    /**
     * 缓存百度 token 缓存
     *
     * @param vo {@link BaiduTokenVO}
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO set(final BaiduTokenVO vo) {
        redissonClient
                .getBucket(CACHE_KEY, StringCodec.INSTANCE)
                // 设置 token 过期时间
                .set(JSON.toJsonString(vo), vo.getExpires_in(), TimeUnit.SECONDS);
        return vo;
    }

    /**
     * 获取百度 token 缓存
     *
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO get() {
        final RBucket<String> bucket = redissonClient.getBucket(CACHE_KEY, StringCodec.INSTANCE);
        final String jsonText = bucket.get();
        if (StringUtils.isBlank(jsonText)) {
            return null;
        }
        return JSON.parseObject(jsonText, BaiduTokenVO.class);
    }

    /**
     * 获取百度 token 缓存
     *
     * @return {@link Optional<BaiduTokenVO>}
     */
    public Optional<BaiduTokenVO> optional() {
        final RBucket<String> bucket = redissonClient.getBucket(CACHE_KEY, StringCodec.INSTANCE);
        return Optional
                .ofNullable(bucket.get())
                .filter(StringUtils::isNotBlank)
                .map(jsonText -> JSON.parseObject(jsonText, BaiduTokenVO.class));

    }
}
