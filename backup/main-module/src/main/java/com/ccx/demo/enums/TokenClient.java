package com.ccx.demo.enums;

/**
 * 枚举：多端 token 登录
 *
 * @author 谢长春 2022-02-08
 */
public enum TokenClient {
    OM_PC("运营管理端：PC浏览器"),
    USER_ANDROID("普通用户：安卓设备"),
    USER_IOS("普通用户：苹果设备"),
    USER_WECHAT("普通用户：微信"),
    USER_WECHAT_APP("普通用户：微信小程序"),
    ;
    public final String comment;

    TokenClient(final String comment) {
        this.comment = comment;
    }

}
