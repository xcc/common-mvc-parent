package com.ccx.demo.config.encrypt.convert;

import com.ccx.demo.config.DOEncryptConfiguration;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *   @JsonSerialize(converter = AesIdLongJsonConvert.Serializer.class)
 *   @JsonDeserialize(converter = AesIdLongJsonConvert.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesIdLongJsonConvert {

    /**
     * id 序列化
     */
    public static class Serializer extends StdConverter<Long, String> {

        @Override
        public String convert(Long value) {
            if (Objects.isNull(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", value);
            }
            return DOEncryptConfiguration.encryptId(value);
        }
    }

    /**
     * id 反序列化
     */
    public static class Deserializer extends StdConverter<String, Long> {

        @Override
        public Long convert(String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", value);
            }
            return DOEncryptConfiguration.decryptLongId(value);
        }
    }

}
