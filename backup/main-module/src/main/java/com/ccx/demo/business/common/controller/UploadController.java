package com.ccx.demo.business.common.controller;

import com.ccx.demo.business.common.dto.FileDTO;
import com.ccx.demo.business.common.dto.ImageFileDTO;
import com.ccx.demo.business.common.dto.UserAvatarImageDTO;
import com.ccx.demo.business.common.service.FileService;
import com.common.annotations.AppSwaggerGroup;
import com.common.annotations.VueSwaggerGroup;
import com.common.annotations.WechatSwaggerGroup;
import com.common.db.IUser;
import com.common.db.entity.Result;
import com.common.entity.Item;
import com.common.enums.Code;
import com.common.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

@Api(tags = "文件上传")
@ApiSort(5)
@Controller
@RequestMapping("/upload")
@Slf4j
@RequiredArgsConstructor
public class UploadController {
    private final FileService fileService;

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiOperation(value = "", hidden = true)
    @PostMapping("/test")
    @ResponseBody
    public Result<Void> test(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<Void>().call(() -> {
            try {
                log.info("{}", JSON.toJsonString(fileService.write(
                        file,
                        new FileDTO(Objects.requireNonNull(file.getOriginalFilename())).buildMd5Uname(file.getInputStream(), "level-1", "level-2")
                )));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String content = "文本内容";
            log.info("{}", JSON.toJsonString(fileService.write(
                    content,
                    new FileDTO("text.txt").buildMd5Uname(content)
            )));
            byte[] bytes = "二进制内容".getBytes(UTF_8);
            log.info("{}", JSON.toJsonString(fileService.write(
                    bytes,
                    new FileDTO("bytes.txt").buildMd5Uname(bytes)
            )));
            File file1 = new File("/home/ccx/git-repository/code-projects/common-mvc-parent/doc/nginx.conf");
            log.info("{}", JSON.toJsonString(fileService.write(
                    file1,
                    new FileDTO("nginx.conf").buildMd5Uname(file1, "level-1", "config")
            )));
            final FileDTO resultFileInfo = fileService.writeJson(
                    new Result<Item>()
                            .setSuccess(new Item()
                                    .setKey("item key")
                                    .setValue("1")
                                    .setComment("说明")
                            ),
                    new FileDTO("result.json")
            );
            log.info("{}", JSON.toJsonString(resultFileInfo));
            log.info("{}", fileService
                    .readJson(
                            resultFileInfo,
                            new TypeReference<Result<Item>>() {
                            }
                    )
                    .map(JSON::toJsonString)
                    .orElse("文件不存在")
            );

            try {
                Files.list(Paths.get("/home/ccx/git-repository/code-projects/common-mvc-parent/.temp"))
                        .filter(path -> !Objects.equals("app-demo.log", path.toFile().getName()))
                        .forEach(path -> {
                            log.info("{}", JSON.toJsonString(fileService.write(
                                    path.toFile(),
                                    new FileDTO(path.toFile().getName()).buildMd5Uname(path.toFile(), ".temp")
                            )));
                        });
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传图片到临时目录，单个上传；上传后的图片会等比缩放和裁剪")
    @ApiOperationSupport(order = 1, author = "谢长春")
    @PostMapping("/temp/image")
    @ResponseBody
    public Result<FileDTO> uploadTempImage(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<FileDTO>().execute(result -> {
            if (Objects.isNull(file) || StringUtils.isBlank(file.getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【file】只能是单个文件");
            }
            Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
            Code.A00002.assertHasTrue(Objects.requireNonNull(file.getContentType()).startsWith("image/"), "上传文件必须是图片");
            try {
                final ImageFileDTO image = new ImageFileDTO(file.getOriginalFilename())
                        .<ImageFileDTO>buildMd5Uname(file.getInputStream())
                        .buildMini()
                        .buildCorp();
                final boolean exists = fileService.exists(image);
                if (!exists) {
                    fileService.write(file, image);
                    final int miniWidth = 500, miniHeight = 250, // 缩略图尺寸
                            cropWidth = 80, cropHeight = 80; // 裁剪图尺寸
                    @Cleanup final ByteArrayOutputStream miniOutputStream = new ByteArrayOutputStream();
                    {
                        // 基于原图生成迷你缩略图，等比缩放， 限制宽高 miniWidth×miniHeight px，按长边等比缩放，不会拉伸图片
                        Thumbnails.of(file.getInputStream())
                                .width(miniWidth)
                                .height(miniHeight)
                                .outputFormat(image.getSuffix())
                                .toOutputStream(miniOutputStream);
                        // 缩放后的图片写入文件
                        fileService.write(miniOutputStream.toByteArray(), new ImageFileDTO(image.getMini()));
                    }
                    {
                        // 读取缩略图
                        final BufferedImage miniBufferedImage = Thumbnails
                                .of(new ByteArrayInputStream(miniOutputStream.toByteArray()))
                                .size(miniWidth, miniHeight)
                                .asBufferedImage();
                        // 基于缩略图生成裁剪方图，限制宽高 cropWidth×cropHeight px，按短边等比缩放之后截取 cropWidth×cropHeight 的区域，长边超出部分会被裁掉
                        final int minSize = Math.min(miniBufferedImage.getWidth(), miniBufferedImage.getHeight());
                        @Cleanup final ByteArrayOutputStream cropOutputStream = new ByteArrayOutputStream();
                        // 裁剪图片
                        Thumbnails.of(miniBufferedImage)
                                .width(cropWidth)
                                .height(cropHeight)
                                .sourceRegion(Positions.CENTER, minSize, minSize)
                                .outputFormat(image.getSuffix())
                                .toOutputStream(cropOutputStream);
                        // 裁剪后的图片写入文件
                        fileService.write(cropOutputStream.toByteArray(), new ImageFileDTO(image.getCrop()));
                    }
                }
                result.setSuccess(image);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传到临时目录，单个上传")
    @ApiOperationSupport(order = 1, author = "谢长春")
    @PostMapping("/temp")
    @ResponseBody
    public Result<FileDTO> uploadTempFile(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<FileDTO>().execute(result -> {
            if (Objects.isNull(file) || StringUtils.isBlank(file.getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【file】只能是单个文件");
            }
            Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
            try {
                final FileDTO fileDTO = new FileDTO(file.getOriginalFilename()).buildMd5Uname(file.getInputStream());
                result.setSuccess(fileService.write(file, fileDTO));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传到临时目录，批量上传")
    @ApiOperationSupport(order = 2, author = "谢长春")
    @PostMapping("/temps")
    @ResponseBody
    public Result<FileDTO> uploadTempFiles(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile[] files) {
        return new Result<FileDTO>().execute(result -> {
            if (Objects.isNull(files) || StringUtils.isBlank(files[0].getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【files】为数组");
            }
            result.setSuccess(Stream.of(files)
                    .map(file -> {
                                Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
                                try {
                                    final FileDTO fileDTO = new FileDTO(Objects.requireNonNull(file.getOriginalFilename())).buildMd5Uname(file.getInputStream());
                                    return fileService.write(file, fileDTO);
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                    )
                    .collect(Collectors.toList())
            );
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传用户头像"
            , tags = {""}
            , notes = "上传用户头像"
    )
    @ApiOperationSupport(order = 3, author = "谢长春")
    @PostMapping("/user/avatar")
    @ResponseBody
    public Result<UserAvatarImageDTO> uploadUserAvatar(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<UserAvatarImageDTO>().execute(result -> {
            if (Objects.isNull(file) || StringUtils.isBlank(file.getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【file】只能是单个文件");
            }
            Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
            try {
                final UserAvatarImageDTO image = fileService.write(
                        file,
                        new UserAvatarImageDTO(file.getOriginalFilename())
                                .buildMd5Uname(file.getInputStream())
                );
                result.setSuccess(image);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
