package com.ccx.demo.open.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 应用静态资源
 *
 * @author 谢长春 2022-06-28
 */
@Getter
@Setter
@ToString
@ApiModel("应用静态资源")
public class AppStaticVO {
    /**
     * 唯一编码
     */
    @ApiModelProperty(value = "唯一编码", accessMode = READ_ONLY)
    private String id;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", accessMode = READ_ONLY)
    private String name;
    /**
     * 静态资源请求地址
     */
    @ApiModelProperty(value = "静态资源请求地址", accessMode = READ_ONLY)
    private String url;
    /**
     * 用途说明
     */
    @ApiModelProperty(value = "用途说明", accessMode = READ_ONLY)
    private String comment;
    /**
     * 内容版本号，版本号与应用本地不一致时，表示应用需要下载新资源
     */
    @ApiModelProperty(value = "内容版本号，版本号与应用本地不一致时，表示应用需要下载新资源", accessMode = READ_ONLY)
    private String version;
}
