package com.ccx.demo.config.encrypt.convert;

import com.ccx.demo.config.DOEncryptConfiguration;
import com.common.enums.Code;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *    @JsonSerialize(converter = AesIdLongArrayJsonConvert.Serializer.class)
 *    @JsonDeserialize(converter = AesIdLongArrayJsonConvert.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesIdLongArrayJsonConvert {

    /**
     * id 序列化
     */
    public static class Serializer extends StdConverter<Long[], String[]> {

        @Override
        public String[] convert(Long[] value) {
            if (Objects.isNull(value) || value.length == 0) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", Stream.of(value).map(Objects::toString).collect(Collectors.joining(",")));
            }
            try {
                return DOEncryptConfiguration
                        .encryptIds(Lists.newArrayList(value))
                        .toArray(String[]::new);
            } catch (Exception e) {
                throw Code.A00003.toCodeException(Stream.of(value).map(Objects::toString).collect(Collectors.joining(",")), e);
            }
        }
    }

    /**
     * id 反序列化
     */
    public static class Deserializer extends StdConverter<String[], Long[]> {

        @Override
        public Long[] convert(String[] value) {
            if (Objects.isNull(value) || value.length == 0) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", String.join(",", value));
            }
            try {
                return DOEncryptConfiguration
                        .decryptLongId(Lists.newArrayList(value))
                        .toArray(Long[]::new);
            } catch (Exception e) {
                throw Code.A00003.toCodeException(String.join(",", value), e);
            }
        }
    }

}
