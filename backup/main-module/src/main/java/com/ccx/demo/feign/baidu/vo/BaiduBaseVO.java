package com.ccx.demo.feign.baidu.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.Assert;

import java.util.Objects;

/**
 * 百度错误码
 * https://ai.baidu.com/ai-doc/FACE/5k37c1ujz
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class BaiduBaseVO {
    @JsonProperty("error_code")
    private Integer errorCode;
    @JsonProperty("error_msg")
    private String errorMsg;

    public boolean success() {
        return Objects.equals(0, errorCode);
    }

    public void assertErrorCode() {
        Assert.isTrue(Objects.equals(0, errorCode), () -> String.format("百度API请求失败[%d]:%s", errorCode, errorMsg));
    }
}
