package com.ccx.demo.business.common.service;

import com.ccx.demo.business.common.dto.FileDTO;
import com.common.enums.Code;
import com.common.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.obs.services.ObsClient;
import com.obs.services.model.CopyObjectResult;
import com.obs.services.model.ObjectMetadata;
import com.obs.services.model.ObsObject;
import com.obs.services.model.PutObjectResult;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.nio.channels.SeekableByteChannel;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 华为云 OBS 读写服务接口
 *
 * @author 谢长春 2020-11-25
 */
@Primary
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.files.type", havingValue = "obs")
@Getter
@Setter
@ConfigurationProperties(prefix = "spring.app.files.obs")
public class ObsHuaWeiFileService implements FileService {
    private static ObsClient obsClient;
    /**
     * 这里置空，文件存储在 bucketName
     */
    private String directory;
    /**
     * 文件服务访问地址
     */
    private String server;
    /**
     * 存储桶名称， 注： 华为云 OBS 的 bucket 必须是全局唯一，不能跟其他用户重叠，最好在华为云控制台提前建好
     */
    private String bucketName;
    /**
     * 区域
     */
    private String region;
    /**
     * 开发环境使用外网节点
     */
    private String endpoint;
    /**
     * Access key就像用户ID，可以唯一标识你的账户。
     */
    private String accessKey;
    /**
     * Secret key是你账户的密码。
     */
    private String secretKey;

    private static final int MAX = 90;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @SneakyThrows
    @PreDestroy
    public void preDestroy() {
        // 关闭obsClient，全局使用一个ObsClient客户端的情况下，不建议主动关闭ObsClient客户端
        obsClient.close();
    }

    @PostConstruct
    @Override
    public void postConstruct() {
        log.info(LOG_PATTERN
                , "阿里oss文件存储配置"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.directory: " + directory, MAX, ' '), MAX) + " # 这里置空，文件存储在 bucketName"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.server: " + server, MAX, ' '), MAX) + " # 文件服务访问地址"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.region: " + region, MAX, ' '), MAX) + " # 区域"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.bucketName: " + bucketName, MAX, ' '), MAX) + " # 存储桶名称"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.endpoint: " + endpoint, MAX, ' '), MAX) + " # 当前使用节点"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.accessKey: " + accessKey, MAX, ' '), MAX) + " # 授权账号"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.obs.secretKey: " + server, MAX, ' '), MAX) + " # 授权密钥"
                )
                , "阿里oss文件存储配置"
        );

        // 创建ObsClient实例
        obsClient = new ObsClient(accessKey, secretKey, endpoint);
        log.info("华为 obs 存储桶 【{}】 创建成功", bucketName);
    }


    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final MultipartFile uploadFile, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件路径【{}】：{}{}", fileDTO.getName(), server, fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = obsClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return fileDTO;
            }
        }
        @Cleanup final InputStream inputStream = uploadFile.getInputStream();
        final ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(uploadFile.getContentType());
        // metadata.setContentLength((long) inputStream.available());
        // 上传文件到存储桶
        final PutObjectResult result = obsClient.putObject(
                bucketName,
                fileDTO.getAbsolutePath(),
                inputStream
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getEtag()
                    , result.getRequestId()
            );
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final InputStream inputStream, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = obsClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return fileDTO;
            }
        }
        final ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setContentLength(inputStream.available());
        metadata.setContentType(fileDTO.getContentType());
        // 上传文件到存储桶
        final PutObjectResult result = obsClient.putObject(
                bucketName,
                fileDTO.getAbsolutePath(),
                inputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getEtag()
                    , result.getRequestId()
            );
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final File srcFile, final T outFileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件路径【{}】：{}{}", outFileDTO.getName(), server, outFileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(outFileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = obsClient.doesObjectExist(bucketName, outFileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return outFileDTO;
            }
        }
        @Cleanup final FileInputStream fileInputStream = new FileInputStream(srcFile);
        @Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        final ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setContentLength((long) bufferedInputStream.available());
        metadata.setContentType(outFileDTO.getContentType());
        // 上传文件到存储桶
        final PutObjectResult result = obsClient.putObject(
                bucketName,
                outFileDTO.getAbsolutePath(),
                bufferedInputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getEtag()
                    , result.getRequestId()
            );
        }
        return outFileDTO;
    }

    @SneakyThrows
    @Override
    public Optional<String> readString(final FileDTO fileDTO) {
        final ObsObject object = obsClient.getObject(bucketName, fileDTO.getAbsolutePath());
        @Cleanup final InputStream inputStream = object.getObjectContent();
        @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8);
        return Optional.of(CharStreams.toString(inputStreamReader));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJson(final T fileDTO, final TypeReference<R> typeReference) {
        return readString(fileDTO).map(jsonText -> JSON.parse(jsonText, typeReference));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJsonObject(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).map(jsonText -> JSON.parseObject(jsonText, clazz));
    }

    @Override
    public <T extends FileDTO, R> Optional<List<R>> readJsonArray(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).map(jsonText -> JSON.parseList(jsonText, clazz));
    }

    @SneakyThrows
    @Override
    public byte[] readBytes(final FileDTO fileDTO) {
        final ObsObject object = obsClient.getObject(bucketName, fileDTO.getAbsolutePath());
        @Cleanup final InputStream inputStream = object.getObjectContent();
        return ByteStreams.toByteArray(inputStream);
    }

    @SneakyThrows
    @Override
    public BufferedReader readStream(final FileDTO fileDTO) {
        final ObsObject object = obsClient.getObject(bucketName, fileDTO.getAbsolutePath());
        try (final InputStream inputStream = object.getObjectContent();
             final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8)) {
            return new BufferedReader(inputStreamReader);
        }
    }

    @SneakyThrows
    @Override
    public InputStream readInputStream(final FileDTO fileDTO) {
        final ObsObject object = obsClient.getObject(bucketName, fileDTO.getAbsolutePath());
        return object.getObjectContent();
    }

    @SneakyThrows
    @Override
    public SeekableByteChannel readChannel(final FileDTO fileDTO) {
        throw new Exception("obs 暂不支持 SeekableByteChannel");
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final FileDTO outFileDTO) {
        final String srcFilePath = srcFileDTO.getAbsolutePath();
        final String outFilePath = outFileDTO.getAbsolutePath();
        if (Objects.equals(srcFilePath, outFilePath)) {
            return;
        }
        final CopyObjectResult result = obsClient.copyObject(
                bucketName
                , srcFilePath
                , bucketName
                , outFilePath
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getEtag()
                    , result.getRequestId()
            );
        }
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final OutputStream outputStream) {
        ByteStreams.copy(readInputStream(srcFileDTO), outputStream);
    }

    @Override
    public <T extends FileDTO> boolean exists(final T fileDTO) {
        if (Objects.isNull(fileDTO)) {
            return false;
        }
        return obsClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFolder(final T folder, final BiConsumer<String, String> consumer) {
        log.error("obs 暂未实现该接口：forEachFolder");
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFile(final T folder, final BiConsumer<String, String> consumer) {
        log.error("obs 暂未实现该接口：forEachFile");
    }

    @Override
    public void deleteFolder(final List<String> folderPaths) {
        log.warn("obs 暂不支持该操作：deleteFolder");
        if (CollectionUtils.isEmpty(folderPaths)) {
            return;
        }
//        folderPaths.forEach(absolute -> {
//            try {
//                if (!aliOssClient.doesObjectExist(bucketName, absolute)) {
//                    aliOssClient.deleteDirectory(bucketName, absolute);
//                }
//                log.info("删除文件夹【{}】: 成功", absolute);
//            } catch (Exception e) {
//                log.error("删除文件夹【{}】: {}", absolute, e.getMessage(), e);
//            }
//        });
    }

    @Override
    public void deleteFile(final List<String> filePaths) {
        if (CollectionUtils.isEmpty(filePaths)) {
            return;
        }
        filePaths.forEach(absolute -> {
            try {
                if (!obsClient.doesObjectExist(bucketName, absolute)) {
                    obsClient.deleteObject(bucketName, absolute);
                }
                log.info("删除文件【{}】: 成功", absolute);
            } catch (Exception e) {
                log.error("删除文件【{}】: {}", absolute, e.getMessage(), e);
            }
        });
    }
}
