package com.ccx.demo.feign.baidu.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.List;
import java.util.Objects;

/**
 * 百度内容审核
 *
 * @author 谢长春 2022-04-09
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel("百度内容审核")
public class BaiduAuditAudioVO extends BaiduBaseVO {

    /**
     * 日志id
     */
    @JsonProperty("log_id")
    private String logId;
    /**
     * 审核结果，可取值描述：合规、不合规、疑似、审核失败
     */
    private String conclusion;
    /**
     * 审核结果类型，可取值1、2、3、4，分别代表1：合规，2：不合规，3：疑似，4：审核失败
     */
    private Integer conclusionType;
    /**
     * 音频: 音频数据唯一标识，系统内部产生
     */
    private String sn;
    /**
     * 音频: 用户信息，当入参传递account时，才会返回该字段
     */
    private String account;
    /**
     * 音频: 音频信息，当入参传递audioid时，才会返回该字段
     */
    private String audioId;
    /**
     * 音频: 语音识别文本结果，字符串数组，若不拆句，则为整段文本
     */
    private String[] rawText;
    /**
     * 不合规/疑似/命中白名单项详细信息。响应成功并且conclusion为疑似或不合规或命中白名单时才返回，响应失败或conclusion为合规且未命中白名单时不返回。
     */
    private List<DataBean> data;

    @Override
    public boolean success() {
        return Objects.isNull(getErrorCode());
    }

    public boolean auditSuccess() {
        return Objects.equals(1, conclusionType);
    }

    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class DataBean {
        /**
         * 分段文本结果
         */
        @JsonProperty("text")
        private String text;
        /**
         * 文本对应音频文件的起始时间
         */
        private Long startTime;
        /**
         * 文本对应音频文件的终止时间
         */
        private Long endTime;
        /**
         * 文本审核结果，可取值：合规、不合规、疑似、审核失败
         */
        private String conclusion;
        /**
         * 审核结果类型，可取值1、2、3、4，分别代表1：合规，2：不合规，3：疑似，4：审核失败
         */
        private Integer conclusionType;
        /**
         * 文本审核结果详情，参见auditData结构
         */
        private List<AuditDataBean> auditData;
    }

    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class AuditDataBean {
        /**
         * 审核主类型，11：百度官方违禁词库、12：文本反作弊、13:自定义文本黑名单、14:自定义文本白名单 20:存在娇喘内容
         */
        private Integer type;
        /**
         * 审核子类型，此字段需参照type主类型字段决定其含义：
         * 当type=11时subType取值含义：
         * 0:百度官方默认违禁词库
         * 当type=12时subType取值含义：
         * 0:低质灌水、1:违禁违规、2:文本色情、3:敏感信息、4:恶意推广、5:低俗辱骂、6:恶意推广-联系方式、7:恶意推广-软文推广、8:广告法审核
         * 当type=13时subType取值含义：
         * 0:自定义文本黑名单
         * 当type=14时subType取值含义：
         * 0:自定义文本白名单
         */
        private Integer subType;
        /**
         * 不合规项描述信息
         */
        private String msg;
        /**
         * 命中信息，参见hits结构
         */
        private List<BaiduAuditVO.DataBean.HitsBean> hits;
    }
}
