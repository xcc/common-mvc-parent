package com.ccx.demo.feign;

import com.ccx.demo.feign.baidu.dto.BaiduAuditImageDTO;

/**
 * 图片审核
 *
 * @author 谢长春 2022-04-10
 */
public interface AuditImageService {
    /**
     * 图片审核
     *
     * @param dto {@link BaiduAuditImageDTO}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    String auditImage(final BaiduAuditImageDTO dto);
}
