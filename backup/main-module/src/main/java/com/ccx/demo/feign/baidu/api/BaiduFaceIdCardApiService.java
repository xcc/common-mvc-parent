package com.ccx.demo.feign.baidu.api;

import com.ccx.demo.feign.baidu.dto.BaiduPersonIdMatchDTO;
import com.ccx.demo.feign.baidu.vo.BaiduBaseVO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 百度身份证与名字比对
 * https://ai.baidu.com/ai-doc/FACE/xk37c1jn6
 * 计费：https://ai.baidu.com/ai-doc/FACE/Ck37c1lmj
 *
 * @author 谢长春 2022-04-09
 */
@ConditionalOnProperty(value = "spring.app.feign.baidu.face.enabled", havingValue = "true")
@FeignClient(name = "baiduFaceIdCardApiService", url = "${spring.app.feign.baidu.face.face-api}")
public interface BaiduFaceIdCardApiService {

    /**
     * 身份证与名字比对
     * 根据error_code判断，为0时表示匹配为同一个人。否则按错误码表的定义，如222351表示身份证号码与名字不匹配。
     * https://ai.baidu.com/ai-doc/FACE/Tkqahnjtk
     *
     * @return {@link BaiduBaseVO}
     */
    @PostMapping(value = "/person/idmatch", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<BaiduBaseVO> call(
            @RequestParam("access_token") final String accessToken,
            @RequestBody BaiduPersonIdMatchDTO dto
    );
}
