package com.ccx.demo.config.init;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

/**
 * 初始化单例类、实体类、接口需要的bean，因为单例类无法直接注入bean
 *
 * @author 谢长春
 */
@Component
@Slf4j
@RequiredArgsConstructor
@Order(-1) // 这里优先级一定要最高，否则其他的初始化使用到路径会报错
public class AppInit extends ApplicationObjectSupport {
    private final AppConfig appConfig;

    private static ApplicationContext appContext;
    private static ExecutorService singleThreadExecutorService;

    private static ExecutorService multiThreadExecutorService;
    /**
     * redisson 缓存操作
     */
    private static RedissonClient redissonClient;
    /**
     * 数据库对象操作
     */
    private static JPAQueryFactory queryFactory;

    /**
     * 获取 Spring Context 对象
     *
     * @return ApplicationContext
     */
    public static ApplicationContext getAppContext() {
        return appContext;
    }

    /**
     * 单线程服务
     */
    public static ExecutorService getSingleThreadExecutorService() {
        return singleThreadExecutorService;
    }

    /**
     * 多线程服务
     */
    public static ExecutorService getMultiThreadExecutorService() {
        return multiThreadExecutorService;
    }

    public static RedissonClient getRedissonClient() {
        return redissonClient;
    }

    public static JPAQueryFactory getQueryFactory() {
        return queryFactory;
    }

    /**
     * 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
     */
    private static final Map<String, Object> BEAN_MAP = new ConcurrentHashMap<>();

    /**
     * 获取 bean， 并缓存到 map 集合，  避免每次通过反射获取
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getBean(Class<T> clazz) {
        if (!BEAN_MAP.containsKey(clazz.getName())) {
            // 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
            BEAN_MAP.put(clazz.getName(), appContext.getBean(clazz));
        }
        return (T) BEAN_MAP.get(clazz.getName());
    }

    /**
     * 获取 bean， 并缓存到 map 集合，  避免每次通过反射获取
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getBean(String beanName, Class<T> clazz) {
        if (!BEAN_MAP.containsKey(beanName)) {
            // 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
            BEAN_MAP.put(beanName, appContext.getBean(beanName, clazz));
        }
        return (T) BEAN_MAP.get(beanName);
    }

    @PostConstruct
    public void init() {
        appContext = Objects.requireNonNull(getApplicationContext());
        singleThreadExecutorService = Objects.requireNonNull(appContext.getBean("singleThreadExecutorService", ExecutorService.class));
        multiThreadExecutorService = Objects.requireNonNull(appContext.getBean("multiThreadExecutorService", ExecutorService.class));
        redissonClient = Objects.requireNonNull(appContext.getBean(RedissonClient.class));
        queryFactory = Objects.requireNonNull(appContext.getBean(JPAQueryFactory.class));
        final RMapCache<String, String> map = redissonClient.getMapCache(this.getClass().getSimpleName());
        // 测试 redisson 写入应用信息， hgetall AppInit 查看写入信息
        map.put(
                String.format("%s:%s:%s:%s:%s"
                        , appConfig.getName()
                        , appConfig.getEnv().name()
                        , appConfig.getVersion()
                        , appConfig.getIp()
                        , this.getClass().getName()
                )
                , String.format("%s - %s"
                        , appConfig.getBuildTime()
                        , appConfig.getStartTime()
                )
        );
    }

    @PreDestroy
    public void destroy() {
        redissonClient.getMapCache(this.getClass().getSimpleName())
                .remove(String.format("%s:%s:%s:%s:%s"
                                , appConfig.getName()
                                , appConfig.getEnv().name()
                                , appConfig.getVersion()
                                , appConfig.getIp()
                                , this.getClass().getName()
                        )
                );
    }

}
