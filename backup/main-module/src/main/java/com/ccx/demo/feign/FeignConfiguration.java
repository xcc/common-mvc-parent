package com.ccx.demo.feign;

import lombok.Setter;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * Feign 配置
 *
 * @author 谢长春 2022-08-17
 */
@Setter
@Configuration(proxyBeanMethods = false)
@EnableFeignClients(basePackages = {"com.ccx.demo.feign"})
public class FeignConfiguration {
//    @Value("${spring.app.feign.proxy.host}")
//    private String proxyHost;
//    @Value("${spring.app.feign.proxy.port}")
//    private Integer proxyPort;
//    @Value("${spring.app.feign.proxy.domains}")
//    private Set<String> proxyDomains;
//
//    @Bean
//    @ConditionalOnProperty(value = "spring.app.feign.proxy.enabled", havingValue = "true")
//    public OkHttpClientFactory okHttpClientFactory(OkHttpClient.Builder builder) {
//        return new ProxyOkHttpClientFactory(builder);
//    }
//
//    class ProxyOkHttpClientFactory extends DefaultOkHttpClientFactory {
//        public ProxyOkHttpClientFactory(OkHttpClient.Builder builder) {
//            super(builder);
//            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
//            List<Proxy> proxyList = new ArrayList<>(1);
//            proxyList.add(proxy);
//            builder.proxySelector(new ProxySelector() {
//                @Override
//                public List<Proxy> select(URI uri) {
//                    if (uri == null || !proxyDomains.contains(uri.getHost())) {
//                        return Collections.singletonList(Proxy.NO_PROXY);
//                    }
//
//                    return proxyList;
//                }
//
//                @Override
//                public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
//                }
//            });
//        }
//    }
}
