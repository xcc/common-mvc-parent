package com.ccx.demo.feign.baidu.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 内容审核平台-音频接口 参数
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("内容审核平台-音频接口")
public class BaiduAuditAudioDTO {
    /**
     * url
     * string
     * N （和base64二选一）
     * 音频文件的url地址	www.asd.com/asd.acc
     */
    @ApiModelProperty(value = "音频文件的url地址", position = 1)
    private String url;
    /**
     * base64
     * string
     * N（和url二选一）
     * 音频文件的base64编码，与url二选一，若都有按base64调用
     */
    @ApiModelProperty(value = "音频文件的base64编码", position = 2)
    private String base64;
    /**
     * fmt
     * string
     * Y
     * 音频文件的格式，pcm、wav、amr、m4a，推荐pcm格式
     */
    @ApiModelProperty(value = "音频文件的格式，pcm、wav、amr、m4a，推荐pcm格式", position = 3)
    private String fmt;
    /**
     * rate
     * Integer
     * Y
     * 音频采样率[16000]
     */
    @ApiModelProperty(value = "音频文件的格式，pcm、wav、amr、m4a，推荐pcm格式", position = 4)
    private int rate = 16000;
    /**
     * rawText
     * boolean
     * N
     * 是否返回音频识别结果 true:是;false:否 默认为true	true
     */
    @ApiModelProperty(value = "是否返回音频识别结果", position = 5)
    private boolean rawText;
    /**
     * split
     * boolean
     * N
     * rawText是否拆句，true:拆句;false:不拆句返回整段文本 默认为true	true
     */
    @ApiModelProperty(value = "rawText是否拆句", position = 5)
    private boolean split;
    /**
     * account
     * string
     * N
     * 用户信息标识，限长64位字符长度
     */
    @ApiModelProperty(value = "用户信息标识，限长64位字符长度", position = 6)
    private String account;
    /**
     * audioId
     * string
     * N
     * 音频信息标识，限长128位字符长度
     */
    @ApiModelProperty(value = "音频信息标识，限长128位字符长度", position = 7)
    private String audioId;
}
