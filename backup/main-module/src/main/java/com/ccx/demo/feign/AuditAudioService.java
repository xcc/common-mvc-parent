package com.ccx.demo.feign;

import com.ccx.demo.feign.baidu.dto.BaiduAuditAudioDTO;

/**
 * 音频审核
 *
 * @author 谢长春 2022-04-11
 */
public interface AuditAudioService {
    /**
     * 音频审核
     *
     * @param dto {@link BaiduAuditAudioDTO}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    String auditAudio(final BaiduAuditAudioDTO dto);
}
