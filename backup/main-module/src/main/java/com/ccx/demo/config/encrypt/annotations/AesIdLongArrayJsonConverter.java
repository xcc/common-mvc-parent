package com.ccx.demo.config.encrypt.annotations;

import com.ccx.demo.config.encrypt.convert.AesIdLongArrayJsonConvert;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解：数字 id json 序列化和反序列化加解密
 * <pre>
 * 使用方法：
 * {@code @AesIdLongArrayJsonConverter private Long[] ids;}
 * </pre>
 *
 * @author 谢长春 2022-09-08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@JacksonAnnotationsInside
@AesIdJsonConverter
@JsonSerialize(converter = AesIdLongArrayJsonConvert.Serializer.class)
@JsonDeserialize(converter = AesIdLongArrayJsonConvert.Deserializer.class)
public @interface AesIdLongArrayJsonConverter {
}
