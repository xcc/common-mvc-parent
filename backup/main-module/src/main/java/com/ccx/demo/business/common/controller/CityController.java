package com.ccx.demo.business.common.controller;

import com.ccx.demo.business.common.dto.TabCityInsertDTO;
import com.ccx.demo.business.common.dto.TabCityUpdateDTO;
import com.ccx.demo.business.common.entity.TabCity;
import com.ccx.demo.business.common.service.CityService;
import com.ccx.demo.config.DOEncryptConfiguration;
import com.ccx.demo.config.encrypt.DataVersion;
import com.common.db.IUser;
import com.common.db.entity.Result;
import com.common.enums.Code;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 对外接口：城市 V20220709
 *
 * @author 谢长春 on 2022-06-28
 */
@Api(tags = "城市")
//@ApiSort() // 控制接口排序
@RequestMapping("/city")
@Controller
@Slf4j
@RequiredArgsConstructor
public class CityController {

    private final CityService service;

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_load')")
    @ApiOperation(value = "城市根节点：省"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ResponseBody
    public Result<TabCity> listRoot(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<TabCity>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.listByPid(""));
        });
    }

    @GetMapping("/list/{pid}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_load')")
    @ApiOperation(value = "按编码查询子节点，根节点传空字符串"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 1,
            author = "谢长春"
    )
    @ResponseBody
    public Result<TabCity> listByPid(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "父节点城市编码", example = "1") @PathVariable final String pid) {
        return new Result<TabCity>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.listByPid(pid));
        });
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_load')")
//    @ApiOperation(value = "列表查询城市"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "pid", "name", "py", "pinyin"}
//    )
//    @ResponseBody
//    public Result<TabCity> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabCity condition) {
//        return new Result<TabCity>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list( condition ));
//        });
//    }
//

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_load')")
    @ApiOperation(value = "按 id 查询城市"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<TabCity> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id") @PathVariable final String id
    ) {
        return new Result<TabCity>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(DOEncryptConfiguration.decryptStringId(id)).orElse(null));
        });
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_insert')")
    @ApiOperation(value = "新增城市"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            includeParameters = {
                    "body.pid", "body.name", "body.py", "body.pinyin"
            })
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final TabCityInsertDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.insert(body, user.getId());
        });
    }

    @PutMapping("/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_update')")
    @ApiOperation(value = "修改城市"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            ignoreParameters = {
                    "body.pid", "body.name", "body.py", "body.pinyin"
            })
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv,
            @RequestBody final TabCityUpdateDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.update(
                    DOEncryptConfiguration
                            .decryptDV(dv)
                            .map(DataVersion::getStringId)
                            .orElseThrow(() -> Code.A00003.toCodeException("数据版本号无效：%s", dv))
                    , user.getId()
                    , body
            );
        });
    }

    //    @DeleteMapping("/{dv}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_delete')")
//    @ApiOperation(value = "物理删除城市"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(order = 6, author = "谢长春")
//    @ResponseBody
//    public Result<Void> deleteById(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv) {
//        return new Result<Void>().call(() -> {
//            final AesApiId.AesId decrypt = AesApiId.decrypt(dv);
//            service.deleteById(decrypt.getStringId(), user.getId());
//        });
//    }
//
//    @DeleteMapping
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_delete')")
//    @ApiOperation(value = "物理删除城市"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 7,
//            author = "谢长春",
//            params = @DynamicParameters(name = "DvArray", properties = {
//                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
//            })
//    )
//    @ResponseBody
//    public Result<Void> deleteByIds(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final Set<String> body
//    ) {
//        return new Result<Void>().call(() -> {
//            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
//            final List<AesApiId.AesId> decryptList = body.stream().map(AesApiId::decrypt).collect(Collectors.toList());
//            service.deleteByIds(
//                    decryptList.stream().map(AesApiId.AesId::getStringId).collect(Collectors.toSet())
//                    //, decryptList.stream().map(AesApiId.AesId::getUpdateTime).collect(Collectors.toSet())
//                    , user.getId()
//            );
//        });
//    }
//
    @PatchMapping("/refresh-json")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'setting_city_list_refresh_json')")
    @ApiOperation(value = "刷新json静态文件"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 8, author = "谢长春")
    @ResponseBody
    public Result<Void> refreshJson(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Void>().call(service::refreshJson);
    }
}
