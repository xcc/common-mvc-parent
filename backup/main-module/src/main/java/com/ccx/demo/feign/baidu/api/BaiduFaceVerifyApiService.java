package com.ccx.demo.feign.baidu.api;

import com.ccx.demo.feign.baidu.dto.BaiduPersonVerifyDTO;
import com.ccx.demo.feign.baidu.vo.BaiduPersonVerifyVO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 百度人脸实名认证
 * https://ai.baidu.com/ai-doc/FACE/xk37c1jn6
 * 计费：https://ai.baidu.com/ai-doc/FACE/Ck37c1lmj
 *
 * @author 谢长春 2022-04-09
 */
@ConditionalOnProperty(value = "spring.app.feign.baidu.face.enabled", havingValue = "true")
@FeignClient(name = "baiduFaceVerifyApiService", url = "${spring.app.feign.baidu.face.face-api}")
public interface BaiduFaceVerifyApiService {
    /**
     * 人脸实名认证
     * 身份验证：验证用户的身份证号码和姓名以及现场拍摄的图片与公安数据源是否匹配，用于判断用户信息是否真实
     * https://ai.baidu.com/ai-doc/FACE/7k37c1ucj
     *
     * @return {@link BaiduPersonVerifyVO}
     */
    @PostMapping(value = "/person/verify", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<BaiduPersonVerifyVO> call(
            @RequestParam("access_token") final String accessToken,
            @RequestBody BaiduPersonVerifyDTO dto
    );
}
