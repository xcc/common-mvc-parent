package com.ccx.demo.feign.baidu.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.Assert;

import static com.common.util.Dates.Pattern.yyyyMMddHHmmss;

/**
 * 百度 api token
 * https://ai.baidu.com/ai-doc/REFERENCE/Ck3dwjhhu
 * <p>
 * 成功响应
 * <pre>
 * {
 *   "refresh_token": "25.28cab96e34315b9a7239b1b2df636cab.315360000.1919296212.282335-22848400",
 *   "expires_in": 2592000,
 *   "session_key": "9mzdAqYqtncO7CHyEC27z2vJ1PbqH6yorNVgrD+LfaKaIqGkSDNePmlZ3Nhm6YWsmfbCkVR1Y/Ssjg6amZEJEiUVmkPeEw==",
 *   "access_token": "24.9def0476d20fe5b3fe1bd0456e6f513e.2592000.1606528212.282335-22848400",
 *   "scope": "public vis-ocr_ocr brain_all_scope brain_ocr_idcard vis-faceverify_faceverify_h5-face-liveness vis-faceverify_FACE_V3 vis-faceverify_FACE_Police vis-faceverify_idl_face_merge vis-faceverify_FACE_EFFECT wise_adapt lebo_resource_base lightservice_public hetu_basic lightcms_map_poi kaidian_kaidian ApsMisTest_Test权限 vis-classify_flower lpq_开放 cop_helloScope ApsMis_fangdi_permission smartapp_snsapi_base smartapp_mapp_dev_manage iop_autocar oauth_tp_app smartapp_smart_game_openapi oauth_sessionkey smartapp_swanid_verify smartapp_opensource_openapi smartapp_opensource_recapi fake_face_detect_开放Scope vis-ocr_虚拟人物助理 idl-video_虚拟人物助理 smartapp_component",
 *   "session_secret": "68aa54d0009dfba54647f07caf374170"
 * }
 * </pre>
 * 失败响应
 * <pre>
 * {
 *   "error": "invalid_client",
 *   "error_description": "unknown client id"
 * }
 * </pre>
 *
 * @author 谢长春 2022-04-09
 */
@Getter
@Setter
@ToString
@ApiModel("百度 api access token")
public class BaiduTokenVO {
    /**
     * access_token： 要获取的Access Token；
     */
    @ApiModelProperty(value = "Access Token")
    private String access_token;
    /**
     * expires_in： Access Token的有效期(秒为单位，一般为1个月)；
     */
    @ApiModelProperty(hidden = true)
    private Long expires_in;
    /**
     * 缓存写入时间
     */
    @ApiModelProperty(hidden = true)
    private String cacheTime;
    /**
     * 异常代码
     */
    @ApiModelProperty(hidden = true)
    private String error;
    /**
     * 异常消息
     */
    @ApiModelProperty(hidden = true)
    @JsonProperty("error_description")
    private String errorDescription;

    /**
     * 初始化缓存时间
     *
     * @return {@link BaiduTokenVO}
     */
    public BaiduTokenVO initCacheTime() {
        cacheTime = yyyyMMddHHmmss.now();
        return this;
    }

    /**
     * 断言响应状态
     */
    public void assertErrorCode() {
        Assert.isNull(error, () -> String.format("百度 token 获取失败[%s]:%s", error, errorDescription));
    }
}
