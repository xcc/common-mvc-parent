package com.ccx.demo.feign.baidu.enums;

/**
 * 图片质量控制
 * 默认 NONE
 *
 * @author 谢长春 2022-04-09
 */
public enum QualityControl {
    NONE("不进行控制"),
    LOW("较低的质量要求"),
    NORMAL("一般的质量要求"),
    HIGH("较高的质量要求"),
    ;
    public final String comment;


    QualityControl(final String comment) {
        this.comment = comment;
    }
}
