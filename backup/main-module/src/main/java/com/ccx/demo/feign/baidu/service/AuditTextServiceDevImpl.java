package com.ccx.demo.feign.baidu.service;

import com.ccx.demo.feign.AuditTextService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 文字审核
 *
 * @author 谢长春 2022-04-11
 */
@Slf4j
@Service
public class AuditTextServiceDevImpl implements AuditTextService {
    /**
     * 文本审核
     *
     * @param text {@link String}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditText(final String text) {
        log.warn("spring.app.feign.baidu.audit.enabled ： 内容审核开关未打开");
        return "";
    }
}
