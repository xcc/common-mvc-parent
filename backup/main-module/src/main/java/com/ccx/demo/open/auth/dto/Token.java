package com.ccx.demo.open.auth.dto;

import com.ccx.demo.business.user.cache.TabRoleCache;
import com.ccx.demo.business.user.cache.TabUserCache;
import com.ccx.demo.business.user.entity.TabRole;
import com.ccx.demo.business.user.vo.LoginUserVO;
import com.ccx.demo.enums.AppCode;
import com.ccx.demo.enums.TokenClient;
import com.ccx.demo.open.auth.cache.TokenCache;
import com.common.enums.Code;
import com.common.util.Dates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * token 登录模式缓存字段
 *
 * @author 谢长春 2019-12-16
 */
@Getter
@Setter
@Slf4j
@ToString
@Accessors(chain = true)
public class Token implements Serializable {
    private static final long serialVersionUID = 4742500780168126654L;
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    private static final SecretKeySpec SK;
    /**
     * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    private static final IvParameterSpec IV;

    static {
        try {
            final String md5 = DigestUtils.md5Hex(Token.class.getName() + Objects.toString(serialVersionUID));
            SK = new SecretKeySpec(md5.getBytes(), "AES");
            IV = new IvParameterSpec(StringUtils.right(md5, 16).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * aes 解码并转换为 {@link Token} 实体，执行 token 认证，认证失败会抛出异常，认证成功则更新 token 过期时间
     *
     * @param token {@link String} HTTP 请求头中获取到的 token
     * @return {@link Token}
     */
    public static Token auth(final String token) {
        if (log.isInfoEnabled()) {
            log.info("x-token:{}", token);
        }
        return Optional
                .ofNullable(token)
                .map(Token::parseToken)
                .orElseThrow(() -> new IllegalArgumentException("token 解析失败")).check();
    }

    @SneakyThrows
    private static Token parseToken(final String token) {
        // 初始化解密参数，设置为解密模式，指定密钥，设置IV
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, SK, IV);
//        final String payload = new String(cipher.doFinal(org.apache.commons.codec.binary.Hex.decodeHex(token)));
        final String payload = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(token)));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", token, payload);
        }
        final String[] payloads = payload.split("@");
        return new Token()
                .setClient(TokenClient.values()[Integer.parseInt(payloads[0])])
                .setTimestamp(Long.parseLong(payloads[1]))
                .setUserId(Long.parseLong(payloads[2]))
                ;
    }

    /**
     * 登录设备
     */
    private TokenClient client;
    /**
     * 用户 ID
     */
    private Long userId;
    /**
     * 授权时间
     */
    private Long timestamp;
    /**
     * 登录过期时间
     */
    private Long expired;

    /**
     * 从缓存中获取用户信息
     *
     * @return {@link LoginUserVO}
     */
    @JsonIgnore
    public LoginUserVO getUser() {
        return TabUserCache.instance().get(userId)
                .map(user -> {
                    AppCode.B01001.assertHasFalse(user.getDisabled()); // 账户已禁用
                    final LoginUserVO vo = new LoginUserVO();
                    BeanUtils.copyProperties(user, vo);
                    vo.setClient(client);
                    final List<TabRole> tabRoles = TabRoleCache.instance().listByIds(Sets.newHashSet(user.getRoles())).stream()
                            .filter(row -> Objects.equals(false, row.getDeleted()))
                            .collect(Collectors.toList());
                    if (!tabRoles.isEmpty()) {
                        vo.setAuthorityList(tabRoles.stream()
                                .map(TabRole::getAuthorities)
                                .filter(Objects::nonNull)
                                .flatMap(Arrays::stream)
                                .distinct()
                                .collect(Collectors.toList())
                        );
                        vo.setRoleNames(tabRoles.stream()
                                .map(TabRole::getName)
                                .filter(StringUtils::isNoneBlank)
                                .collect(Collectors.toList()));
                    }
                    return vo;
                })
                .orElseThrow(() -> new NullPointerException("用户不存在"));
    }

    /**
     * token中包含的内容
     */
    @JsonIgnore
    public String getPayload() {
        return String.format("%d@%d@%d", client.ordinal(), timestamp, userId);
    }

    /**
     * 生成 base64 编码
     *
     * @return {@link String}
     */
    @SneakyThrows
    public String token() {
        this.timestamp = Dates.now().toTimeMillis();
        TokenCache.instance().set(this);
        final String payload = getPayload();

        // 初始化加密参数，设置为加密模式，指定密钥，设置IV
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, SK, IV);
//        final String token = org.apache.commons.codec.binary.Hex.encodeHexString(cipher.doFinal(payload.getBytes()));
//        final String token = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(payload.getBytes()));
        final String token = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(cipher.doFinal(payload.getBytes()));
        if (log.isDebugEnabled()) {
            log.info("{} => {}", payload, token);
        }
        return token;
    }

    /**
     * 用 base64 解码前端的 token 同缓存中的对象比对 token 是否过期，过期抛出异常，未过期则更新过期时间，并返回新的缓存对象，需要将新的缓存对象存入缓存中
     *
     * @return {@link Token} 新的缓存对象
     */
    public Token check() {
        final Token token = TokenCache.instance().get(this).orElseThrow(() -> Code.A00002.toCodeException("token 过期， token 不存在"));
        Code.A00011.assertEquals(this.timestamp, token.getTimestamp(), "token 过期， token 随机值不匹配");
        Code.A00002.assertHasTrue(token.getExpired() > Instant.now().toEpochMilli(), "token 过期");
        // 更新过期时间
        //tokenCache.set(this);
        return this;
    }

    /**
     * 销毁 token , 清除缓存
     */
    public void destroy() {
        TokenCache.instance().delete(this);
    }

//    public static void main(String[] args) {
//        String token = new TokenCache()
//                .setUserId(1000L)
//                .setClient(TokenClient.OM_PC)
//                .token();
//        log.info("{}", parseToken(token));
//    }
}
