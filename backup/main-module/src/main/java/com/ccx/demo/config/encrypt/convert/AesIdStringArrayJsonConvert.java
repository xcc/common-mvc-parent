package com.ccx.demo.config.encrypt.convert;

import com.ccx.demo.config.DOEncryptConfiguration;
import com.common.enums.Code;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *   @JsonSerialize(converter = AesIdStringArrayJsonConvert.Serializer.class)
 *   @JsonDeserialize(converter = AesIdStringArrayJsonConvert.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesIdStringArrayJsonConvert {

    /**
     * id 序列化
     */
    public static class Serializer extends StdConverter<String[], String[]> {

        @Override
        public String[] convert(String[] value) {
            if (Objects.isNull(value) || value.length == 0) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", String.join(",", value));
            }
            try {
                return DOEncryptConfiguration
                        .encryptIds(Stream.of(value).collect(Collectors.toList()))
                        .toArray(String[]::new);
            } catch (Exception e) {
                throw Code.A00003.toCodeException(String.join(",", value), e);
            }
        }
    }

    /**
     * id 反序列化
     */
    public static class Deserializer extends StdConverter<String[], String[]> {

        @Override
        public String[] convert(String[] value) {
            if (Objects.isNull(value) || value.length == 0) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", String.join(",", value));
            }
            try {
                return DOEncryptConfiguration.decryptStringId(Lists.newArrayList(value)).toArray(String[]::new);
            } catch (Exception e) {
                throw Code.A00003.toCodeException(String.join(",", value), e);
            }
        }
    }

}
