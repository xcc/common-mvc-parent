package com.ccx.demo.business.common.service;

import com.ccx.demo.business.common.dto.FileDTO;
import com.common.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.io.Closer;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.net.URL;
import java.nio.channels.SeekableByteChannel;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 文件读写服务接口
 *
 * @author 谢长春 2020-11-13
 */
@Validated
public interface FileService {

    /**
     * 文件服务初始化
     */
    void postConstruct();

    /**
     * 是否本地磁盘存储
     *
     * @return boolean true：是，false
     */
    default boolean isLocalDisk() {
        return false;
    }

    /**
     * 单文件上传，文件已存在时不再重复时不上传
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * write(
     *   uploadFile,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getOriginalFilename(), "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * write(
     *   uploadFile,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getOriginalFilename(), "level-1", "level-2")
     *       .buildMd5Uname(uploadFile.getInputStream())
     *   )
     * );
     * </pre>
     *
     * @param uploadFile MultipartFile 上传文件
     * @param fileDTO    <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotNull(message = "参数【uploadFile】不能为null") final MultipartFile uploadFile,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(uploadFile, fileDTO, false);
    }

    /**
     * 单文件上传，文件重复时，可选择替换内容
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * write(
     *   uploadFile,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getOriginalFilename(), "level-1", "level-2")
     *       .buildUuidUname()
     *   ),
     *   true
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * write(
     *   uploadFile,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getOriginalFilename(), "level-1", "level-2")
     *       .buildMd5Uname(uploadFile.getInputStream())
     *   ),
     *   true
     * );
     * </pre>
     *
     * @param uploadFile MultipartFile 上传文件
     * @param fileDTO    <T extends FileDTO> 存储对象
     * @param replace    boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotNull(message = "参数【uploadFile】不能为null") final MultipartFile uploadFile,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            final boolean replace
    );

    /**
     * 写入内容，文件已存在时不再重复写入
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * String content = "text content";
     * write(
     *   content,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("文本内容.txt", "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * String content = "text content";
     * write(
     *   content,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("文本内容.txt", "level-1", "level-2")
     *       .buildMd5Uname(content)
     *   )
     * );
     * </pre>
     *
     * @param content String 写入内容
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotEmpty(message = "参数【content】不能为空") final String content,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(content.getBytes(UTF_8), fileDTO, false);
    }

    /**
     * 写入内容，文件已存在时可选择覆盖文件
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * String content = "text content";
     * write(
     *   content,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("文本内容.txt", "level-1", "level-2")
     *       .buildUuidUname()
     *   ),
     *   true
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * String content = "text content";
     * write(
     *   content,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("文本内容.txt", "level-1", "level-2")
     *       .buildMd5Uname(content)
     *   ),
     *   true
     * );
     * </pre>
     *
     * @param content String 写入内容
     * @param fileDTO <T extends FileDTO> 存储对象
     * @param replace boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotEmpty(message = "参数【content】不能为空") final String content,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            final boolean replace
    ) {
        return write(content.getBytes(UTF_8), fileDTO, replace);
    }

    /**
     * 写入内容，文件已存在时不会覆盖文件
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * byte[] bytes = "二进制内容".getBytes(UTF_8);
     * write(
     *   bytes,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("二进制内容.txt", "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * byte[] bytes = "二进制内容".getBytes(UTF_8);
     * write(
     *   bytes,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("二进制内容.txt", "level-1", "level-2")
     *       .buildMd5Uname(bytes)
     *   )
     * );
     * </pre>
     *
     * @param bytes   byte[] 写入内容
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotEmpty(message = "参数【bytes】不能为空") final byte[] bytes,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(bytes, fileDTO, false);
    }

    /**
     * 写入内容，文件已存在时可选择覆盖文件
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * byte[] bytes = "二进制内容".getBytes(UTF_8);
     * write(
     *   bytes,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("二进制内容.txt", "level-1", "level-2")
     *       .buildUuidUname()
     *   ),
     *   true
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * byte[] bytes = "二进制内容".getBytes(UTF_8);
     * write(
     *   bytes,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("二进制内容.txt", "level-1", "level-2")
     *       .buildMd5Uname(bytes)
     *   ),
     *   true
     * );
     * </pre>
     *
     * @param bytes   byte[] 写入内容
     * @param fileDTO <T extends FileDTO> 存储对象
     * @param replace boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    @SneakyThrows
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotEmpty(message = "参数【bytes】不能为空") final byte[] bytes,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            final boolean replace
    ) {
        @Cleanup final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        return write(byteArrayInputStream, fileDTO, replace);
    }

    /**
     * 写入内容，文件已存在时不会覆盖文件
     *
     * @param inputStream InputStream 写入内容
     * @param fileDTO     <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotEmpty(message = "参数【inputStream】不能为空") final InputStream inputStream,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(inputStream, fileDTO, false);
    }

    /**
     * 写入内容，文件已存在时可选择覆盖文件
     *
     * @param inputStream byte[] 写入内容
     * @param fileDTO     <T extends FileDTO> 存储对象
     * @param replace     boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotEmpty(message = "参数【inputStream】不能为空") final InputStream inputStream,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            final boolean replace
    );

    /**
     * 写入内容，文件已存在时不会覆盖文件
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * File file = new File("/tmp/nginx.conf");
     * write(
     *   file,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getName(), "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * File file = new File("/tmp/nginx.conf");
     * write(
     *   file,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getName(), "level-1", "level-2")
     *       .buildMd5Uname(file)
     *   )
     * );
     * </pre>
     *
     * @param file    File 原始文件
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotNull(message = "参数【file】不能为null") final File file,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(file, fileDTO, false);
    }

    /**
     * 写入内容，文件已存在时可选择覆盖文件
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * File file = new File("/tmp/nginx.conf");
     * write(
     *   file,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getName(), "level-1", "level-2")
     *       .buildUuidUname()
     *   ),
     *   true
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * File file = new File("/tmp/nginx.conf");
     * write(
     *   file,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getName(), "level-1", "level-2")
     *       .buildMd5Uname(file)
     *   ),
     *   true
     * );
     * </pre>
     *
     * @param srcFile    File 原始文件
     * @param outFileDTO <T extends FileDTO> 存储对象
     * @param replace    boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
            @NotNull(message = "参数【file】不能为null") final File srcFile,
            @NotNull(message = "参数【fileDTO】不能为null") final T outFileDTO,
            final boolean replace
    );

    /**
     * 单文件上传，文件已存在时会覆盖
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * write(
     *   uploadFile,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getOriginalFilename(), "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * write(
     *   uploadFile,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getOriginalFilename(), "level-1", "level-2")
     *       .buildMd5Uname(uploadFile.getInputStream())
     *   )
     * );
     * </pre>
     *
     * @param uploadFile MultipartFile 上传文件
     * @param fileDTO    <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeAndReplace(
            @NotNull(message = "参数【uploadFile】不能为null") final MultipartFile uploadFile,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(uploadFile, fileDTO, true);
    }

    /**
     * 写入内容，文件已存在时会覆盖
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * String content = "text content";
     * write(
     *   content,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("文本内容.txt", "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * String content = "text content";
     * write(
     *   content,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("文本内容.txt", "level-1", "level-2")
     *       .buildMd5Uname(content)
     *   )
     * );
     * </pre>
     *
     * @param content String 写入内容
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeAndReplace(
            @NotEmpty(message = "参数【content】不能为空") final String content,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(content.getBytes(UTF_8), fileDTO, true);
    }

    /**
     * 写入内容，文件已存在时会覆盖
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * byte[] bytes = "二进制内容".getBytes(UTF_8);
     * write(
     *   bytes,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("二进制内容.txt", "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * byte[] bytes = "二进制内容".getBytes(UTF_8);
     * write(
     *   bytes,
     *   FileDTO.valueOf(FPath.FileName
     *       .of("二进制内容.txt", "level-1", "level-2")
     *       .buildMd5Uname(bytes)
     *   )
     * );
     * </pre>
     *
     * @param bytes   byte[] 写入内容
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeAndReplace(
            @NotEmpty(message = "参数【bytes】不能为空") final byte[] bytes,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(bytes, fileDTO, true);
    }

    /**
     * 写入内容，文件已存在时会覆盖
     *
     * @param inputStream InputStream 写入内容
     * @param fileDTO     <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeAndReplace(
            @NotEmpty(message = "参数【inputStream】不能为空") final InputStream inputStream,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(inputStream, fileDTO, true);
    }

    /**
     * 写入内容，文件已存在时会覆盖
     * <pre>
     * // 以 uuid 生成文件名，每次生成的文件名不一样
     * File file = new File("/tmp/nginx.conf");
     * write(
     *   file,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getName(), "level-1", "level-2")
     *       .buildUuidUname()
     *   )
     * );
     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
     * File file = new File("/tmp/nginx.conf");
     * write(
     *   file,
     *   FileDTO.valueOf(FPath.FileName
     *       .of(file.getName(), "level-1", "level-2")
     *       .buildMd5Uname(file)
     *   )
     * );
     * </pre>
     *
     * @param file    File 原始文件
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeAndReplace(
            @NotNull(message = "参数【file】不能为null") final File file,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return write(file, fileDTO, true);
    }

    /**
     * 写入内容，文件已存在时不会覆盖文件
     *
     * @param url     File 原始文件
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    @SneakyThrows
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeFromUrl(
            @NotBlank(message = "参数【url】不能为空") final String url,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return writeFromUrl(url, fileDTO, false);
    }

    /**
     * 写入内容，文件已存在时会覆盖
     *
     * @param url     File 原始文件
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    @SneakyThrows
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeAndReplaceFromUrl(
            @NotBlank(message = "参数【url】不能为空") final String url,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return writeFromUrl(url, fileDTO, true);
    }

    /**
     * 写入内容，文件已存在时可选择覆盖文件
     *
     * @param url     File 原始文件
     * @param fileDTO <T extends FileDTO> 存储对象
     * @param replace boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    @SneakyThrows
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeFromUrl(
            @NotBlank(message = "参数【url】不能为空") final String url,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            final boolean replace
    ) {
        @Cleanup final Closer closer = Closer.create();
        @Cleanup final InputStream in = closer.register(new URL(url).openStream());
        return write(in, fileDTO, replace);
    }

//    /**
//     * 写入内容，文件已存在时不会覆盖文件
//     * <pre>
//     * // 以 uuid 生成文件名，每次生成的文件名不一样
//     * \@Cleanup final FileInputStream fileInputStream = new FileInputStream(new File("/path/file.txt"));
//     * \@Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
//     * write(
//     *   bufferedInputStream,
//     *   FileDTO.valueOf(FPath.FileName
//     *       .of("file.txt", "level-1", "level-2")
//     *       .buildUuidUname()
//     *   )
//     * );
//     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
//     * \@Cleanup final FileInputStream fileInputStream = new FileInputStream(new File("/path/file.txt"));
//     * \@Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
//     * write(
//     *   bufferedInputStream,
//     *   FileDTO.valueOf(FPath.FileName
//     *       .of("file.txt", "level-1", "level-2")
//     *       .buildMd5Uname(bufferedInputStream)
//     *   )
//     * );
//     * </pre>
//     *
//     * @param bufferedInputStream BufferedInputStream 原始数据流
//     * @param fileDTO            <T extends FileDTO> 存储对象
//     * @return FileDTO
//     */
//    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
//            @NotNull(message = "参数【stream】不能为null") final BufferedInputStream bufferedInputStream,
//            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
//    ) {
//        return write(bufferedInputStream, fileDTO, false);
//    }
//
//    /**
//     * 写入内容，文件已存在时可选择覆盖文件
//     * <pre>
//     * // 以 uuid 生成文件名，每次生成的文件名不一样
//     * \@Cleanup final FileInputStream fileInputStream = new FileInputStream(new File("/path/file.txt"));
//     * \@Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
//     * write(
//     *   bufferedInputStream,
//     *   FileDTO.valueOf(FPath.FileName
//     *       .of("file.txt", "level-1", "level-2")
//     *       .buildUuidUname()
//     *   ),
//     *   true
//     * );
//     * // 以文件内容 MD5 构造文件名，如果文件内容无变化，则每次 md5 值一样，表示重复文件
//     * \@Cleanup final FileInputStream fileInputStream = new FileInputStream(new File("/path/file.txt"));
//     * \@Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
//     * write(
//     *   bufferedInputStream,
//     *   FileDTO.valueOf(FPath.FileName
//     *       .of("file.txt", "level-1", "level-2")
//     *       .buildMd5Uname(bufferedInputStream)
//     *   ),
//     *   true
//     * );
//     * </pre>
//     *
//     * @param bufferedInputStream BufferedInputStream 原始数据流
//     * @param fileDTO            <T extends FileDTO> 存储对象
//     * @param replace             boolean 是否替换已存在的文件， true：是， false：否
//     * @return FileDTO
//     */
//    @NotNull(message = "返回值不能为null") <T extends FileDTO> T write(
//            @NotNull(message = "参数【stream】不能为null") final BufferedInputStream bufferedInputStream,
//            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
//            final boolean replace
//    );

    /**
     * 写入内容，将对象 JSON.toJsonString(object) 之后写入文件，文件已存在时不会覆盖文件
     * <pre>
     * write(
     *   "{\"content\":\"json 文件内容\"}",
     *   FileDTO.valueOf(FPath.FileName.of("result.json", "level-1", "level-2") )
     * );
     * </pre>
     *
     * @param object  Object 写入对象
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeJson(
            final Object object,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO
    ) {
        return writeJson(object, fileDTO, false);
    }

    /**
     * 写入内容，将对象 JSON.toJsonString(object) 之后写入文件，文件已存在时可选择覆盖文件
     * <pre>
     * // **********
     * write(
     *   "{\"content\":\"json 文件内容\"}",
     *   FileDTO.valueOf(FPath.FileName.of("result.json", "level-1", "level-2") )
     *   true
     * );
     * // **********
     * write(
     *   new Result(),
     *   FileDTO.valueOf(FPath.FileName.of("result.json", "level-1", "level-2") )
     *   true
     * );
     * </pre>
     *
     * @param object  Object 写入对象
     * @param fileDTO <T extends FileDTO> 存储对象
     * @param replace boolean 是否替换已存在的文件， true：是， false：否
     * @return FileDTO
     */
    default @NotNull(message = "返回值不能为null") <T extends FileDTO> T writeJson(
            final Object object,
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            final boolean replace
    ) {
        return write(JSON.toJsonString(object), fileDTO, replace);
    }

    /**
     * 读取内容为文本
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return Optional<String>
     */
    <T extends FileDTO> Optional<String> readString(@NotNull(message = "参数【fileDTO】不能为null") final T fileDTO);

    /**
     * 读取内容转换为 Json， 自定义转换规则
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return Optional<String>
     */
    <T extends FileDTO, R> Optional<R> readJson(
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            @NotNull(message = "参数【typeReference】不能为null") final TypeReference<R> typeReference
    );

    /**
     * 读取内容转换为 Json 对象
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return Optional<R>
     */
    <T extends FileDTO, R> Optional<R> readJsonObject(
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            @NotNull(message = "参数【clazz】不能为null") final Class<R> clazz
    );

    /**
     * 读取内容转换为 Json 数组
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return Optional<List < R>>
     */
    <T extends FileDTO, R> Optional<List<R>> readJsonArray(
            @NotNull(message = "参数【fileDTO】不能为null") final T fileDTO,
            @NotNull(message = "参数【clazz】不能为null") final Class<R> clazz
    );

    /**
     * 读取文件二进制数组
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return byte[]
     */
    <T extends FileDTO> byte[] readBytes(@NotNull(message = "参数【fileDTO】不能为null") final T fileDTO);

    /**
     * 读取文件流：字符流
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return BufferedReader
     */
    <T extends FileDTO> BufferedReader readStream(@NotNull(message = "参数【fileDTO】不能为null") final T fileDTO);

    /**
     * 读取文件流：字节流
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return InputStream
     */
    <T extends FileDTO> InputStream readInputStream(@NotNull(message = "参数【fileDTO】不能为null") final T fileDTO);

    /**
     * 读取文件流：Channel
     *
     * @param fileDTO <T extends FileDTO> 存储对象
     * @return SeekableByteChannel
     */
    <T extends FileDTO> SeekableByteChannel readChannel(@NotNull(message = "参数【fileDTO】不能为null") final T fileDTO);

    /**
     * 复制内容到目标文件
     *
     * @param srcFileDTO <T extends FileDTO> 存储对象
     * @param outFileDTO <T extends FileDTO> 目标文件对象
     */
    <T extends FileDTO> void copy(
            @NotNull(message = "参数【srcFileDTO】不能为null") final T srcFileDTO
            , @NotNull(message = "参数【outFileDTO】不能为null") final T outFileDTO
    );

    /**
     * 复制内容输出到 OutputStream
     *
     * @param srcFileDTO   <T extends FileDTO> 存储对象
     * @param outputStream OutputStream 输出目标
     */
    <T extends FileDTO> void copy(@NotNull(message = "参数【srcFileDTO】不能为null") final T srcFileDTO, @NotNull final OutputStream outputStream);

    /**
     * 检查文件是否存在
     *
     * @param fileDTO <T extends FileInfo> 存储对象
     * @return boolean true:存在，false:不存在
     */
    <T extends FileDTO> boolean exists(@NotNull(message = "参数【fileDTO】不能为null") final T fileDTO);

    /**
     * 遍历目录
     *
     * @param folder   <T extends FileInfo> 存储目录
     * @param consumer BiConsumer<String, String> 文件名，文件绝对路径
     */
    <T extends FileDTO> void forEachFolder(final T folder, final BiConsumer<String, String> consumer);

    /**
     * 遍历文件
     *
     * @param folder   <T extends FileInfo> 存储目录
     * @param consumer BiConsumer<String, String> 文件名，文件绝对路径
     */
    <T extends FileDTO> void forEachFile(final T folder, final BiConsumer<String, String> consumer);

    /**
     * 删除文件夹
     *
     * @param folderPath String 文件绝对路径
     */
    default void deleteFolder(final String folderPath) {
        deleteFolder(Lists.newArrayList(folderPath));
    }

    /**
     * 批量删除文件夹
     *
     * @param folderPaths List<String> 文件夹绝对路径
     */
    void deleteFolder(final List<String> folderPaths);

    /**
     * 删除文件
     *
     * @param filePath String 文件绝对路径
     */
    default void deleteFile(final String filePath) {
        deleteFile(Lists.newArrayList(filePath));
    }

    /**
     * 批量删除文件
     *
     * @param filePaths List<String> 文件绝对路径
     */
    void deleteFile(final List<String> filePaths);

}
