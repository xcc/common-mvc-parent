package com.ccx.demo.open.auth.cache;

import com.ccx.demo.enums.TokenClient;
import com.ccx.demo.open.auth.dto.Token;
import com.common.db.AbsRedissonStringCache;
import com.common.util.Dates;
import com.common.util.JSON;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBatch;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;

import static com.ccx.demo.config.init.AppInit.getBean;


/**
 * token 缓存
 *
 * @author 谢长春 2022-06-17
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.cache.token")
public class TokenCache extends AbsRedissonStringCache<String> {

    public static final String CACHE_KEY = TokenCache.class.getSimpleName() + ":%s";
    private static volatile TokenCache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration expired;
    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;

    /**
     * 获取 {@link TokenCache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link TokenCache}
     */
    public static TokenCache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (TokenCache.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = getBean(TokenCache.class);
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.token.expired: {}", expired);
        log.info("spring.app.cache.token.nullValueExpired: {}", nullValueExpired);
    }

    /**
     * 拼接 {@link Token#getClient()}:{@link Token#getUserId()} 作为主键
     *
     * @param token {@link Token}
     * @return String OM_PC:1000
     */
    public String getUnionKey(final Token token) {
        return String.format("%s:%d", token.getClient().name(), token.getUserId());
    }

    /**
     * 基于数据主键构造缓存key； 拼接前缀，一般用类名作为前缀:unionKey
     *
     * @param unionKey String tokenClient:userId
     * @return String this.getClass().getSimpleName() + ":" + unionKey
     */
    @Override
    protected Optional<String> formatCacheKey(final String unionKey) {
        return Optional.ofNullable(unionKey)
                .filter(StringUtils::isNoneBlank)
                .map(val -> String.format(CACHE_KEY, unionKey))
                ;
    }

    /**
     * 写缓存
     *
     * @param token {@link Token}
     */
    public void set(final Token token) {
        token.setExpired(Dates.now().addSecond((int) expired.getSeconds()).toTimeMillis());
        set(getUnionKey(token), token);
    }

    /**
     * 读缓存
     *
     * @return {@link Token}
     */
    public Optional<Token> get(final Token token) {
        return get(getUnionKey(token))
                .map(jsonText -> JSON.parseObject(jsonText, Token.class));
    }

    /**
     * token 延期
     *
     * @param client 登录客户端
     * @param userId 用户 ID
     */
    public void expireDelay(
            final TokenClient client,
            final Long userId
    ) {
        formatCacheKey(
                getUnionKey(new Token()
                        .setClient(client)
                        .setUserId(userId)
                )
        ).ifPresent(cacheKey -> {
            getRedissonClient().<String>getBucket(cacheKey, StringCodec.INSTANCE).expire(expired);
        });
    }

    /**
     * 删除缓存
     *
     * @param token {@link Token}
     */
    public void delete(final Token token) {
        delete(getUnionKey(token));
    }

    /**
     * 清理缓存
     *
     * @param userId {@link Long}
     */
    public void clear(final Long userId) {
        if (Objects.isNull(userId)) {
            return;
        }
        final RBatch batch = redissonClient.createBatch();
        for (TokenClient client : TokenClient.values()) {
            formatCacheKey(
                    getUnionKey(new Token()
                            .setClient(client)
                            .setUserId(userId)
                    )
            ).ifPresent(cacheKey -> {
                getRedissonClient().<String>getBucket(cacheKey, StringCodec.INSTANCE).deleteAsync();
            });
        }
        batch.execute();
    }
}
