package com.ccx.demo.business.common.controller;

import com.ccx.demo.business.common.entity.TabAppEvent;
import com.ccx.demo.business.common.service.AppEventService;
import com.ccx.demo.config.DOEncryptConfiguration;
import com.ccx.demo.enums.AppEvent;
import com.common.db.IUser;
import com.common.db.entity.Page;
import com.common.db.entity.Result;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * 对外接口：埋点表 V20220709
 *
 * @author 谢长春 on 2022-06-27
 */
@Api(tags = "埋点表")
//@ApiSort() // 控制接口排序
@RequestMapping("/app-event")
@Controller
@Slf4j
@RequiredArgsConstructor
public class AppEventController {

    private final AppEventService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询埋点表"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 1,
            author = "谢长春",
            includeParameters = {"number", "limit", "orderBy", "event", "eventTime", "userId", "refLongId", "refStringId"}
    )
    @ResponseBody
    public Result<TabAppEvent> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
            final TabAppEvent condition) {
        return new Result<TabAppEvent>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.page(condition, Page.of(number, limit)));
        });
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询埋点表"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "event", "eventTime", "userId", "refLongId", "refStringId"}
//    )
//    @ResponseBody
//    public Result<TabAppEvent> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabAppEvent condition) {
//        return new Result<TabAppEvent>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list( condition ));
//        });
//    }
//

    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询埋点表"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<TabAppEvent> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id") @PathVariable final String id
    ) {
        return new Result<TabAppEvent>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(DOEncryptConfiguration.decryptLongId(id)).orElse(null));
        });
    }

    @PostMapping("/{event}/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "新增埋点表"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            includeParameters = {
                    "body.event", "body.eventTime", "body.data", "body.userId", "body.refLongId", "body.refStringId"
            })
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "埋点事件，参考：AppEvent", example = "REAL_CLICK_3") @PathVariable final AppEvent event,
            @ApiParam(required = true, value = "关联数据id，没有关联数据的请传0", example = "0") @PathVariable final String id,
            @RequestBody(required = false) final Map<String, Object> body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            if (event.isRefLong()) {
                service.insert(event, user.getId(), DOEncryptConfiguration.decryptLongId(id), body);
            } else {
                service.insert(event, user.getId(), DOEncryptConfiguration.decryptStringId(id), body);
            }
        });
    }
//
//    @PutMapping("/{dv}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
//    @ApiOperation(value = "修改埋点表"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 5,
//            author = "谢长春",
//            ignoreParameters = {
//                    "body.event", "body.eventTime", "body.data", "body.userId", "body.refLongId", "body.refStringId"
//            })
//    @ResponseBody
//    public Result<Void> update(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv,
//            @RequestBody final TabAppEventUpdateDTO body
//    ) {
//        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
//            final AesApiId.AesId decrypt = AesApiId.decrypt(dv);
//
//            service.update(decrypt.getLongId(), user.getId(), body);
//        });
//    }

//    @DeleteMapping("/{dv}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除埋点表"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(order = 6, author = "谢长春")
//    @ResponseBody
//    public Result<Void> deleteById(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv) {
//        return new Result<Void>().call(() -> {
//            final AesApiId.AesId decrypt = AesApiId.decrypt(dv);
//            service.deleteById(decrypt.getLongId(), user.getId());
//        });
//    }
//
//    @DeleteMapping
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除埋点表"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 7,
//            author = "谢长春",
//            params = @DynamicParameters(name = "DvArray", properties = {
//                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
//            })
//    )
//    @ResponseBody
//    public Result<Void> deleteByIds(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final Set<String> body
//    ) {
//        return new Result<Void>().call(() -> {
//            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
//            final List<AesApiId.AesId> decryptList = body.stream().map(AesApiId::decrypt).collect(Collectors.toList());
//            service.deleteByIds(
//                    decryptList.stream().map(AesApiId.AesId::getLongId).collect(Collectors.toSet())
//                    //, decryptList.stream().map(AesApiId.AesId::getUpdateTime).collect(Collectors.toSet())
//                    , user.getId()
//            );
//        });
//    }
//
}
