package com.ccx.demo.feign.baidu.api;

import com.ccx.demo.feign.baidu.vo.BaiduAuditAudioVO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 百度内容审核平台：音频审核
 * https://ai.baidu.com/ai-doc/ANTIPORN/hk928u7bz
 * 计费：https://ai.baidu.com/ai-doc/ANTIPORN/lk3h6x7if
 * java剪辑音频： https://www.shuzhiduo.com/A/KE5QVDPkdL/
 * java获取音频文件播放时长： https://www.shuzhiduo.com/A/MAzAGWbM59/
 *
 * @author 谢长春 2022-04-11
 */
@ConditionalOnProperty(value = "spring.app.feign.baidu.audit.enabled", havingValue = "true")
@FeignClient(name = "baiduAuditAudioApiService", url = "${spring.app.feign.baidu.audit.api}")
public interface BaiduAuditAudioApiService {
    /**
     * 百度内容审核平台：音频审核
     * https://ai.baidu.com/ai-doc/ANTIPORN/hk928u7bz
     * 音频文件大小：小于10MB
     * 音频文件时长：小于1分钟
     * 推荐格式：pcm，采样率 ：16000 固定值，编码：16bit 位深的单声道
     * 其他格式：mp3、wav（不压缩，pcm编码）、aac、amr、m4a
     * 注意：若采用以上所列的其他格式，百度服务端会将其转为pcm格式，因此会存在额外的转换耗时，并且有可能会出现格式转换失败的情况，故强烈建议采用pcm格式
     *
     * @param accessToken {@link String}
     * @param dto         {@link MultiValueMap<String, Object>}
     * @return {@link ResponseEntity<BaiduAuditAudioVO>}
     */
    @PostMapping(value = "/voice_censor/v3/user_defined", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<BaiduAuditAudioVO> call(
            @RequestParam("access_token") final String accessToken,
            MultiValueMap<String, Object> dto
    );
}
