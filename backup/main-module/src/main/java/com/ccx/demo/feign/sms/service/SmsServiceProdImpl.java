package com.ccx.demo.feign.sms.service;

import com.ccx.demo.feign.SmsService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * 服务接口实现类：短信发送
 *
 * @author 谢长春 2022-04-10
 */
@Primary
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.feign.sms.enabled", havingValue = "true")
@ConfigurationProperties("spring.app.feign.sms")
public class SmsServiceProdImpl implements SmsService {

    /**
     * 账号
     */
    @Setter
    private String username;
    /**
     * MD5(密码)
     */
    @Setter
    private String password;

    @Override
    public void send(final String phone, final String content) {
        // fixme: 补充短信发送逻辑
//        final ResponseEntity<String> responseEntity = smsApiService.call(username, password, phone, content);
//        Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
//                () -> String.format("短信接口请求失败:%d", responseEntity.getStatusCodeValue()));
//        if (!Objects.equals("0", responseEntity.getBody())) {
//            log.error("code:{}； {}：{}", responseEntity.getBody(), phone, content);
//            throw AppCode.B02002.toCodeException();
//        }
    }
}
