package com.ccx.demo.business.user.vo;

import com.ccx.demo.business.user.entity.QTabUser;
import com.ccx.demo.business.user.entity.TabUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 用户实体
 *
 * @author 谢长春
 */
@ApiModel(description = "扩展用户实体VO")
@Getter
@Setter
@ToString(callSuper = true)
public class TabUserVO extends TabUser {

    private static final long serialVersionUID = 7185030320855492326L;

    @SneakyThrows
    public static TabUserVO ofTabUser(final TabUser entity) {
        return Optional.ofNullable(entity).map(obj -> {
            final TabUserVO vo = new TabUserVO();
            BeanUtils.copyProperties(entity, vo);
            return vo;
        }).orElseGet(TabUserVO::new);
    }

    /**
     * 模糊匹配：用户名/昵称/手机号/邮箱
     */
    @ApiModelProperty(value = "模糊匹配：用户名/昵称/手机号/邮箱", position = 22)
    private String containsAll;

    /**
     * 角色名称集合
     *
     * @return {@link List<String>}
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "角色名称集合", accessMode = READ_ONLY, position = 23)
    private List<String> roleNames;

    @JsonIgnore
    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public QdslWhere where() {
        final QTabUser table = QTabUser.tabUser;
        return super.where()
                .and(containsAll, () -> table.username.contains(containsAll)
                        .or(table.phone.contains(containsAll))
                        .or(table.nickname.contains(containsAll))
                        .or(table.email.contains(containsAll))
                );
    }

}
