package com.ccx.demo.feign.baidu.service;

import com.ccx.demo.feign.AuditAudioService;
import com.ccx.demo.feign.baidu.dto.BaiduAuditAudioDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 音频审核
 *
 * @author 谢长春 2022-04-11
 */
@Slf4j
@Service
public class AuditAudioServiceDevImpl implements AuditAudioService {
    /**
     * 音频审核
     *
     * @param dto {@link BaiduAuditAudioDTO}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditAudio(final BaiduAuditAudioDTO dto) {
        log.warn("spring.app.feign.baidu.audit.enabled ： 内容审核开关未打开");
        return "";
    }
}
