package com.ccx.demo.config.init;

import com.ccx.demo.business.common.dto.FileDTO;
import com.ccx.demo.business.common.service.FileService;
import com.ccx.demo.open.common.vo.AppStaticVO;
import com.common.enums.AppEnv;
import com.common.util.FPath;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.commons.util.InetUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.common.util.Dates.Pattern.yyyyMMddHHmmss;

/**
 * 初始化 application.properties 中的应用配置参数；
 *
 * @author 谢长春 on 2018-10-2
 */
@Component
@Getter
@Setter
@Slf4j
@ConfigurationProperties("spring.app")
public class AppConfig {
    private static volatile AppConfig INSTANCE = null;

    /**
     * 获取 {@link AppConfig} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link AppConfig}
     */
    public static AppConfig instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (AppConfig.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = AppInit.getAppContext().getBean(AppConfig.class);
                }
            }
        }
        return INSTANCE;
    }

    @SneakyThrows
    public AppConfig(InetUtils inetUtils) {
        this.ip = inetUtils.findFirstNonLoopbackHostInfo().getIpAddress();
        this.startTime = yyyyMMddHHmmss.now();
//        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
//        Set<String> ipAddressSet = Sets.newLinkedHashSet();
//        while (networkInterfaces.hasMoreElements()) {
//            Enumeration<InetAddress> inetAddress = networkInterfaces.nextElement().getInetAddresses();
//            while (inetAddress.hasMoreElements()) {
//                InetAddress address = inetAddress.nextElement();
//                if (address instanceof Inet4Address) {
//                    if (!"localhost".equals(address.getHostName())) {
//                        ipAddressSet.add(address.getHostAddress());
//                    }
//                }
//            }
//        }
//        this.ip = String.join(",", ipAddressSet);
    }

    private static final int MAX = 80;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PostConstruct
    private void postConstruct() {
        log.info(LOG_PATTERN
                , "环境配置"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.env: " + env.name(), MAX, ' '), MAX) + " # 当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]"
                        , StringUtils.left(StringUtils.rightPad("spring.app.name: " + name, MAX, ' '), MAX) + " # 应用名称"
                        , StringUtils.left(StringUtils.rightPad("spring.app.version: " + version, MAX, ' '), MAX) + " # 当前运行版本号"
                        , StringUtils.left(StringUtils.rightPad("spring.app.buildTime: " + buildTime, MAX, ' '), MAX) + " # 项目编译打包时间"
                        , StringUtils.left(StringUtils.rightPad("spring.app.gitCommitId: " + gitCommitId, MAX, ' '), MAX) + " # git 提交记录 id 前 8 位"
                        , StringUtils.left(StringUtils.rightPad("spring.app.ip: " + ip, MAX, ' '), MAX) + " # 当前主机 IP 地址"
                        , StringUtils.left(StringUtils.rightPad("server.port: " + port, MAX, ' '), MAX) + " # 当前服务端口"
                        , StringUtils.left(StringUtils.rightPad("spring.app.corsEnabled: " + corsEnabled, MAX, ' '), MAX) + " # 是否开启跨域"
                        , StringUtils.left(StringUtils.rightPad("spring.app.verifyCodeEnabled: " + verifyCodeEnabled, MAX, ' '), MAX) + " # 是否开启图片验证码校验，是否开启手机验证码校验"
                        , StringUtils.left(StringUtils.rightPad("spring.app.swaggerEnabled: " + swaggerEnabled, MAX, ' '), MAX) + " # 是否开启 swagger 文档"
                        //, StringUtils.left(StringUtils.rightPad("spring.app.role.omRoleId: " + omRoleId, MAX, ' '), MAX) + " # 运营管理端普通用户默认角色id"
                        //, StringUtils.left(StringUtils.rightPad("spring.app.role.userRoleId: " + userRoleId, MAX, ' '), MAX) + " # C端用户默认角色id"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.pwd.enabled: " + getEncrypt().getPwd().isEnabled(), MAX, ' '), MAX) + " # 登录密码：加密开关"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.pwd.type: " + getEncrypt().getPwd().getType(), MAX, ' '), MAX) + " # 登录密码：加密类型"
                        //, StringUtils.left(StringUtils.rightPad("spring.app.encrypt.pwd.privateKey: " + getEncrypt().getPwd().getPrivateKey(), MAX, ' '), MAX) + " # 登录密码：RSA 私钥"
                        //, StringUtils.left(StringUtils.rightPad("spring.app.encrypt.pwd.publicKey: " + getEncrypt().getPwd().getPublicKey(), MAX, ' '), MAX) + " # 登录密码：RSA 公钥"
                        //, StringUtils.left(StringUtils.rightPad(": " + , MAX, ' '), MAX) + " # "
                        //, StringUtils.left(StringUtils.rightPad(": " + , MAX, ' '), MAX) + " # "
                )
                , "环境配置"
        );
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Bean
    public CommandLineRunner initNfsCommandLineRunner(ApplicationContext context) {
        return args -> {
            final AppConfig.Type type = nfs.getType();
            log.info(LOG_PATTERN,
                    "文件存储路径配置",
                    Stream.of(AppConfig.Path.values())
                            .map(path -> {
                                if (Objects.equals(type, AppConfig.Type.disk)) {
                                    return StringUtils.left(StringUtils.rightPad(path.name() + ": " + FPath.of(path.get()).mkdirs().absolute(), MAX, ' '), MAX) + " # " + path.comment;
                                }
                                return StringUtils.left(StringUtils.rightPad(path.name() + ": " + path.get(), MAX, ' '), MAX) + " # " + path.comment;
                            })
                            .collect(Collectors.joining("\n")),
                    "文件存储路径配置"
            );
            log.info(LOG_PATTERN,
                    "文件访问路径配置",
                    Stream.of(AppConfig.URL.values())
                            .map(url -> StringUtils.left(StringUtils.rightPad(url.name() + ": " + url.value(), MAX, ' '), MAX) + " # " + url.comment)
                            .collect(Collectors.joining("\n")),
                    "文件访问路径配置"
            );
            final FileService fileService = context.getBean(FileService.class);
            final FileDTO dto = fileService.writeAndReplace(startTime, new FileDTO("test.txt"));
            log.info("测试文件上传存储路径: {}", dto.getAbsolutePath());
            log.info("测试文件上传访问路径: {}", dto.getUrl());
        };
    }

    /**
     * 当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]
     */
    private AppEnv env;
    /**
     * 应用域名
     */
    private String domain;
    /**
     * 当前主机 IP 地址. 使用 {@link InetUtils} 获取，不需要配置
     */
    private String ip;
    /**
     * 当前服务端口
     */
    @Value("${server.port:8080}")
    private int port;
    /**
     * 应用名
     */
    private String name;
    /**
     * 当前运行版本号
     */
    private String version;
    /**
     * 项目编译打包时间，发布完成之后检查打包时间是否是一致的，避免版本更新失败
     */
    private String buildTime;
    /**
     * git 提交记录 id 前 8 位
     */
    private String gitCommitId;
    /**
     * 项目启动时间
     */
    private String startTime;
    /**
     * 是否开启跨域 true：是，false：否.
     */
    private boolean corsEnabled;
    /**
     * 是否开启图片验证码校验，是否开启手机验证码校验
     * 开发环境关闭该开关可以用任意验证码登录，一般用于自动化测试和开发环境，生产环境请启用该配置 true：是，false：否.
     */
    private boolean verifyCodeEnabled;
    /**
     * 是否开启 swagger 文档 true：是，false：否.
     */
    private boolean swaggerEnabled;
    /**
     * 加密配置
     */
    private Encrypt encrypt;
    /**
     * 应用文件存储
     */
    private AppNfs nfs;
    /**
     * 应用静态资源
     */
    private List<AppStaticVO> staticList;

    /**
     * 加密配置
     */
    @Getter
    @Setter
    @ToString
    public static class Encrypt {
        private PwdEncrypt pwd;
    }

    /**
     * 密码加解密配置
     */
    @Getter
    @Setter
    @ToString
    public static class PwdEncrypt {
        /**
         * 加解密开关。 开启后登录密码需要使用 rsa 加密传输
         */
        private boolean enabled;
        /**
         * 加密类型，目前只支持 rsa
         */
        private String type;
        /**
         * 加密传输: 私钥
         */
        private String privateKey;
        /**
         * 加密传输: 公钥
         */
        private String publicKey;
    }

    /**
     * AES加密配置
     */
    @Getter
    @Setter
    @ToString
    public static class AesEncrypt {
        /**
         * 加解密开关。 true：是，false：否。
         */
        private boolean enabled;
        /**
         * 全局 aes 密钥， 32位
         */
        private String secretKey;
        /**
         * 固定 IV ， 16位。 设置固定 IV 之后同样内容每次加密结果一样。 默认为 null， 表示使用动态 IV
         */
        private String ivParameterSpec;
        /**
         * API 接口数据包签名类型
         */
        private String signType;
    }

//
//    /**
//     * 应用程序配置枚举
//     * 来源：application.properties
//     */
//    public enum App {
//        env("当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]",
//                "spring.app.env", () -> Objects.toString(AppConfig.instance()().getEnv())),
//        version("当前运行版本号",
//                "spring.app.version", () -> Objects.toString(AppConfig.instance()().getVersion())),
//        ip("当前主机 IP 地址",
//                "spring.app.ip", () -> AppConfig.instance()().getIp()),
//        domain("应用域名；",
//                "spring.app.domain", () -> AppConfig.instance()().getDomain()),
//        pathRoot("应用文件根目录",
//                "spring.app.path-root", () -> AppConfig.instance()().getPathRoot()),
//        tokenExpired("token 默认过期时间(天)",
//                "spring.app.token-expired", () -> Objects.toString(AppConfig.instance()().getTokenExpired())),
//        autoTaskEnabled("定时任务开关，true：开启，false：关闭",
//                "spring.app.auto-task.enabled", () -> Objects.toString(AppConfig.instance()().isAutoTaskEnabled())),
//        ;
//        public final String key;
//        public final String comment;
//        private final Supplier<String> supplier;
//
//        App(final String comment, final String key, final Supplier<String> supplier) {
//            this.key = key;
//            this.comment = comment;
//            this.supplier = supplier;
//        }
//
//        /**
//         * 获取应用配置
//         *
//         * @return {@link String}
//         */
//        public String value() {
//            return Optional.ofNullable(supplier.get())
//                    .orElseThrow(() -> new NullPointerException(String.format("应用配置【%s】不能空", this.name())));
//        }
//
//        /**
//         * 获取应用配置，并追加字符
//         *
//         * @return {@link String}
//         */
//        public String append(final String append) {
//            return value().concat(append);
//        }
//    }

    /**
     * 应用文件对象
     */
    @Getter
    @Setter
    @ToString
    public static class AppFolder {
        /**
         * 文件存储路径
         */
        private String directory;
        /**
         * 文件访问路径
         */
        private String url;
    }

    /**
     * 应用文件对象
     */
    @Getter
    @Setter
    @ToString
    public static class AppNfs {
        /**
         * 文件存储类型，可选参数： disk,minio,alioss,obs
         */
        private Type type;

        /**
         * 应用文件根目录
         */
        private String directory;
        /**
         * 文件服务访问地址
         */
        private String server;
        /**
         * 临时文件存储路径及访问路径配置
         */
        private AppFolder temp;
        /**
         * 用户头像文件存储路径及访问路径配置
         */
        private AppFolder user;
        /**
         * 静态json文件存储路径及访问路径配置
         */
        private AppFolder json;
    }

    public enum Type {
        // 磁盘存储
        disk,
        // minio 存储
        minio,
        // 阿里云 oss 存储
        alioss,
        // 华为云 obs 存储
        obs,
        ;
    }

    /**
     * 定义所有路径枚举
     *
     * @author 谢长春 on 2018-10-2
     */
    public enum Path {
        TEMP("文件上传及临时文件存储目录", () -> instance().getNfs().getTemp().getDirectory()),
        USER("用户头像存储目录", () -> instance().getNfs().getUser().getDirectory()),
        JSON("静态json文件存储目录", () -> instance().getNfs().getJson().getDirectory()),
        ;
        /**
         * 枚举属性说明
         */
        public final String comment;
        /**
         * 获取文件目录
         */
        public final Supplier<String> directorySupplier;

        Path(String comment, Supplier<String> directorySupplier) {
            this.comment = comment;
            this.directorySupplier = directorySupplier;
        }
//
//        /**
//         * 获取文件路径操作对象
//         *
//         * @param names String[] 追加目录名或文件名
//         * @return FPath
//         */
//        public FPath fpath(String... names) {
//            return FPath.of(directorySupplier.get(), names);
//        }
//
//        /**
//         * 获取绝对路径
//         *
//         * @param names String[] 追加目录名或文件名
//         * @return String 文件绝对路径：d:\java
//         */
//        public String absolute(String... names) {
//            return FPath.of(directorySupplier.get(), names).absolute();
//        }
//
//        /**
//         * 获取文件对象
//         *
//         * @param names String[] 追加目录名或文件名
//         * @return File 文件对象
//         */
//        public File file(String... names) {
//            return FPath.of(directorySupplier.get(), names).file();
//        }

        /**
         * 获取绝对路径
         *
         * @param names String[] 追加目录名或文件名
         * @return String 文件绝对路径：d:\java
         */
        public String get(String... names) {
            final String dir = directorySupplier.get();
            if (Objects.isNull(names)) {
                return dir;
            }
            final StringBuilder builder = new StringBuilder(dir);
            if (!dir.endsWith("/")) {
                builder.append("/");
            }
            builder.append(Arrays.stream(names)
                    .flatMap(v -> Stream.of(v.split("/")))
                    .filter(StringUtils::isNoneBlank)
                    .collect(Collectors.joining("/"))
            );
            return builder.toString();
        }
    }

    /**
     * 定义所有路径枚举
     * url定义规则：
     * > 所有以 {} 占位的 使用 format 方法格式化参数
     * > 所有以 / 结尾的url可以追加路径
     * > 所有非 / 结尾的url不能追加路径
     *
     * @author 谢长春 on 2018-10-2
     */
    public enum URL {
        TEMP("临时文件访问路径", () -> instance().getNfs().getTemp().getUrl()),
        USER("用户头像访问路径", () -> instance().getNfs().getUser().getUrl()),
        JSON("静态json文件访问路径", () -> instance().getNfs().getJson().getUrl()),
        ;
        /**
         * 枚举属性说明
         */
        public final String comment;
        /**
         * 获取访问地址
         */
        public final Supplier<String> urlSupplier;

        URL(String comment, Supplier<String> urlSupplier) {
            this.comment = comment;
            this.urlSupplier = urlSupplier;
        }

        /**
         * 获取路径
         *
         * @return String
         */
        public String value() {
            return urlSupplier.get();
        }

        /**
         * 追加路径
         *
         * @param args 参数集合
         * @return String
         */
        public String append(String... args) {
            return urlSupplier.get().concat(
                    String.join("/", args)
                            // 避免出现双斜杠
                            .replaceFirst("^/", "")
                            .replace("//", "/")
            );
        }

        /**
         * 格式化路径中 {} 指定的参数
         *
         * @param args 参数集合
         * @return String
         */
        public String format(String... args) {
            String result = urlSupplier.get();
            if (Objects.nonNull(args)) {
                for (Object value : args) { // 替换 url 参数占位符
                    result = result.replaceFirst("\\{(\\w+)?}", value.toString());
                }
            }
            return result;
        }

        /**
         * 格式化路径中 {} 指定的参数
         *
         * @param args 参数集合
         * @return String
         */
        public String format(final Map<String, String> args) {
            String result = urlSupplier.get();
            if (Objects.nonNull(args)) {
                for (Map.Entry<String, String> entry : args.entrySet()) {
                    // 替换 url 参数占位符
                    result = result.replace(String.format("{%s}", entry.getKey()), entry.getValue());
                }
            }
            return result;
        }
    }

}
