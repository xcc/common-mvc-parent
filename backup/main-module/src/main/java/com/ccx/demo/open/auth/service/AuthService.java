package com.ccx.demo.open.auth.service;

import com.ccx.demo.business.user.service.UserService;
import com.ccx.demo.business.user.vo.LoginUserVO;
import com.ccx.demo.enums.AppCode;
import com.common.enums.Code;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * 登录时要生成token，完成Spring Security认证，然后返回token给客户端
 * 注册时将用户密码用BCrypt加密，写入用户角色
 *
 * @author 谢长春
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class AuthService implements UserDetailsService {
    @Lazy
    @Autowired
    private AuthenticationConfiguration authenticationConfiguration;
    private final UserService userService;
    //    private MailService mailService;
//    private UserCache userCache;
//    private PhoneCode phoneCode;
//    private EmailActivate emailActivate;
//    private ResetPassword resetPasswordService;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
//            TabUser obj = JSON.parseObject(jsonText, TabUser.class);
//            Asserts.notEmpty(obj.getdomain(), "字段【domain】不能为空");
        Code.A00001.assertNonBlank(username, "参数【username】不能为空");
        return userService.findLoginUserVO(username)
                .orElseThrow(() -> new UsernameNotFoundException("用户不存在：".concat(username)));
    }

    /**
     * 用户名密码登录, 密码必须使用公钥加密
     *
     * @param username String 用户名
     * @param password String 公钥加密后的密码
     * @return LoginUserVO
     */
    @SneakyThrows
    @Validated
    public LoginUserVO login(@NotBlank final String username,
                             @NotBlank final String password
    ) {
        try {
            // 构造 username、password 登录认证token
            final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            // 授权认证
            final Authentication authentication = Objects.requireNonNull(authenticationConfiguration.getAuthenticationManager(), "AuthenticationManager is null")
                    .authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return (LoginUserVO) authentication.getPrincipal();
        } catch (AuthenticationException e) {
            log.error(e.getMessage(), e);
            if (e instanceof DisabledException) {
                throw AppCode.B01001.toCodeException("账户已禁用:%s", username);
            }
            if (e instanceof LockedException) {
                throw AppCode.B01002.toCodeException("账户已锁定:%s", username);
            }
            if (e instanceof AccountExpiredException) {
                throw AppCode.B01003.toCodeException("账户已过期:%s", username);
            }
            if (e instanceof CredentialsExpiredException) {
                throw AppCode.B01004.toCodeException("证书已过期:%s", username);
            }
            throw AppCode.B01000.toCodeException("用户名或密码错误:%s", username);
        }
    }

//    @Override
//    public TabUser regist(final UserRegist regist) throws Exception {
//        { // 手机号注册
////            if (!JUnit.get()) { // 校验手机验证码
////                if (!Cache.getInstance().checkPhoneCode(regist.getPhone(), regist.getCode()))
////                    throw AppCode.PHONE_CODE_ERROR.toCodeException("手机验证码不匹配");
////            }
//        }
//        final TabUser user = userService.save(
//                TabUser.builder()
////                        .username(regist.getPhone())
////                        .phone(regist.getPhone())
//                        .username(regist.getEmail())
//                        .email(regist.getEmail())
//                        .password(regist.getPassword())
//                        .nickname(Util.isNotEmpty(regist.getNickname()) ? regist.getNickname() : regist.getEmail().substring(0, regist.getEmail().lastIndexOf("@")))
//                        .registerSource(Objects.isNull(regist.getSource()) ? RegisterSource.System : regist.getSource())
//                        .role(Role.ROLE_USER)
//                        .build()
//                , App.AdminUserId.get() // 新用户注册，使用管理员账号ID作为创建者和修改者
//        );
//        return user;
//    }
//
//    @Override
//    public String activate(final String uuid) {
//        try {
//            Optional<String> userId = emailActivate.getUserId(uuid);
//            if (userId.isPresent()) {
//                userService.activate(userId.get());
//                return "redirect:/"; // 激活成功
//            } else { // 激活链接已过期
//                return "redirect:/activate-expired.html";
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            return "redirect:/activate-failure.html"; // 激活失败
//        }
//    }

//    @Override
//    public void forgetPassword(final String email) throws Exception {
//        Optional<TabUser> optional = userService.findByUsername(email);
//        Asserts.isTrue(optional.isPresent(), String.format("邮箱【%s】未注册", email));
//        { // 邮箱注册，发送账户激活邮件
//            try {
//                mailService.sendResetPassword(optional.get(),
//                        resetPasswordService.getUrl(optional.get()) // 获取重置链接
//                );
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw AppCode.CHECK_EMAIL.toCodeException("邮件发送失败");
//            }
//        }
//    }

}
