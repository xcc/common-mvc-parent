package com.ccx.demo.business.common.dto;

import com.ccx.demo.config.init.AppConfig.Path;
import com.ccx.demo.config.init.AppConfig.URL;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

/**
 * 用户头像文件对象
 *
 * @author 谢长春 on 2022-05-05
 */
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "用户头像文件对象")
public class UserAvatarImageDTO extends ImageFileDTO {

    private static final long serialVersionUID = -6779893372268172821L;

    public UserAvatarImageDTO(final String uname) {
        super(uname);
    }

    public UserAvatarImageDTO(final String name, final String uname) {
        super(name, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = UserAvatarImageDTO.class.getSimpleName();

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : Path.USER.get("avatar", uname);
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public String getMiniPath() {
        return StringUtils.isEmpty(mini) ? null : Path.USER.get("avatar", mini);
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public String getCropPath() {
        return StringUtils.isEmpty(crop) ? null : Path.USER.get("avatar", crop);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getUrl() {
        return StringUtils.isEmpty(uname) ? null : URL.USER.append("avatar", uname);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getMiniUrl() {
        return StringUtils.isEmpty(mini) ? getUrl() : URL.USER.append("avatar", mini);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getCropUrl() {
        return StringUtils.isEmpty(crop) ? getUrl() : URL.USER.append("avatar", crop);
    }

    @SneakyThrows
    public UserAvatarImageDTO cloneObject() {
        return (UserAvatarImageDTO) super.clone();
    }
}
