package com.ccx.demo.feign.baidu.service;

import com.ccx.demo.feign.AuditImageService;
import com.ccx.demo.feign.baidu.dto.BaiduAuditImageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 图片审核
 *
 * @author 谢长春 2022-04-11
 */
@Slf4j
@Service
public class AuditImageServiceDevImpl implements AuditImageService {
    /**
     * 图片内容审核
     *
     * @param dto {@link BaiduAuditImageDTO}
     * @return {@link String} 空字符串表示审核成功， 否则返回异常内容
     */
    public String auditImage(final BaiduAuditImageDTO dto) {
        log.warn("spring.app.feign.baidu.audit.enabled ： 内容审核开关未打开");
        return "";
    }
}
