package com.ccx.demo.business.common.service;

import com.ccx.demo.business.common.dto.FileDTO;
import com.common.enums.Code;
import com.common.util.FPath;
import com.common.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 磁盘读写服务接口
 *
 * @author 谢长春 2020-11-13
 */
@Slf4j
@Getter
@Setter
@Service
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.nfs.disk")
public class DiskFileService implements FileService {
    /**
     * 这里置空，文件存储在 bucketName
     */
    private String directory;
    /**
     * 文件服务访问地址
     */
    private String server;
    private static final int MAX = 70;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PostConstruct
    @Override
    public void postConstruct() {
        log.info(LOG_PATTERN
                , "磁盘文件存储"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.disk.directory: " + directory, MAX, ' '), MAX) + " # 文件存储根路径"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.disk.server: " + server, MAX, ' '), MAX) + " # 文件服务访问地址"
                )
                , "磁盘文件存储"
        );
    }


    @Override
    public boolean isLocalDisk() {
        return true;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final MultipartFile uploadFile, final T fileDTO, final boolean replace) {
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}", fileDTO.getName(), fileDTO.getAbsolutePath());
        }
        final FPath localFile = FPath.of(fileDTO.getAbsolutePath());
        if (replace || !localFile.exist()) { // 将文件写入到本地
            localFile.mkdirsParent();
            uploadFile.transferTo(localFile.file());
            localFile.chmod();
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final InputStream inputStream, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}", fileDTO.getName(), fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        final FPath to = FPath.of(fileDTO.getAbsolutePath());
        if (replace || !to.exist()) {
            to.mkdirsParent();
            @Cleanup final OutputStream outputStream = Files.newOutputStream(Paths.get(fileDTO.getAbsolutePath()));
            ByteStreams.copy(inputStream, outputStream);
//            FWrite.of(fileDTO.getPath()).write(bytes);
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final File srcFile, final T outFileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}", outFileDTO.getName(), outFileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(outFileDTO.getAbsolutePath(), "必须指定存储路径");
        final File outFile = new File(outFileDTO.getAbsolutePath());
        final FPath parentPath = FPath.of(outFile.getParent());
        if (!parentPath.exist()) {
            parentPath.mkdirs(); // 创建目录
        }
        if (!outFile.exists()) {
            Files.copy(srcFile.toPath(), outFile.toPath());
        } else if (replace) {
            Files.copy(srcFile.toPath(), outFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        return outFileDTO;
    }
//
//    @SneakyThrows
//    @Override
//    public <T extends FileDTO> T write(final BufferedInputStream bufferedInputStream, final T fileDTO, final boolean replace) {
////        @Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
////        bufferedInputStream.mark(0);
////        fileDTO.setName(fileName.getName());
////        fileDTO.setUname(fileName.toMd5Uname(bufferedInputStream));
//        if (log.isInfoEnabled()) {
//            log.info("上传文件【{}】：{}", fileDTO.getName(), fileDTO.getPath());
//        }
//        Code.A00001.assertNonBlank(fileDTO.getPath(), "必须指定存储路径");
//        final FPath to = FPath.of(fileDTO.getPath());
//        if (replace || !to.exist()) {
////            bufferedInputStream.reset();
//            to.mkdirsParent(); // 创建目录
//            Files.copy(bufferedInputStream, to.get(), StandardCopyOption.REPLACE_EXISTING);
//            to.chmodFile(); // 文件授权
//        }
//        return fileDTO;
//    }

    @SneakyThrows
    @Override
    public Optional<String> readString(final FileDTO fileDTO) {
        return Optional.of(new String(Files.readAllBytes(Paths.get(fileDTO.getAbsolutePath())), UTF_8));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJson(final T fileDTO, final TypeReference<R> typeReference) {
        return readString(fileDTO).map(jsonText -> JSON.parse(jsonText, typeReference));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJsonObject(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).map(jsonText -> JSON.parseObject(jsonText, clazz));
    }

    @Override
    public <T extends FileDTO, R> Optional<List<R>> readJsonArray(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).map(jsonText -> JSON.parseList(jsonText, clazz));
    }

    @SneakyThrows
    @Override
    public byte[] readBytes(final FileDTO fileDTO) {
        return Files.readAllBytes(Paths.get(fileDTO.getAbsolutePath()));
    }

    @SneakyThrows
    @Override
    public BufferedReader readStream(final FileDTO fileDTO) {
        return Files.newBufferedReader(Paths.get(fileDTO.getAbsolutePath()), UTF_8);
    }

    @SneakyThrows
    @Override
    public InputStream readInputStream(final FileDTO fileDTO) {
        return Files.newInputStream(Paths.get(fileDTO.getAbsolutePath()), StandardOpenOption.READ);
    }

    @SneakyThrows
    @Override
    public SeekableByteChannel readChannel(final FileDTO fileDTO) {
        return Files.newByteChannel(Paths.get(fileDTO.getAbsolutePath()), StandardOpenOption.READ);
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final FileDTO outFileDTO) {
        final String srcFilePath = srcFileDTO.getAbsolutePath();
        final String outFilePath = outFileDTO.getAbsolutePath();
        if (Objects.equals(srcFilePath, outFilePath)) {
            return;
        }
        Files.copy(Paths.get(srcFilePath), Paths.get(outFilePath));
        FPath.of(outFilePath).chmod(644);
    }

//    @SneakyThrows
//    @Override
//    public void copy(final FileDTO file,final String absolutePath) {
//        @Cleanup final FileChannel fromFileChannel = FileChannel.open(Paths.get(file.getPath()), EnumSet.of(StandardOpenOption.READ));
//        @Cleanup final FileChannel toFileChannel = FileChannel.open(Paths.get(absolutePath), EnumSet.of(StandardOpenOption.CREATE, StandardOpenOption.WRITE));
//        fromFileChannel.transferTo(0L, fromFileChannel.size(), toFileChannel);
//        FPath.of(absolutePath).chmod(644);
//    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final OutputStream outputStream) {
        Files.copy(Paths.get(srcFileDTO.getAbsolutePath()), outputStream);
    }

    @Override
    public <T extends FileDTO> boolean exists(final T fileDTO) {
        if (Objects.isNull(fileDTO)) {
            return false;
        }
        return Files.exists(Paths.get(fileDTO.getAbsolutePath()));
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFolder(final T folder, final BiConsumer<String, String> consumer) {
        Files.list(Paths.get(folder.getAbsolutePath()))
                .map(Path::toFile)
                .filter(File::isDirectory)
                .forEach(file -> consumer.accept(file.getName(), file.getAbsolutePath()));
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFile(final T folder, final BiConsumer<String, String> consumer) {
        Files.list(Paths.get(folder.getAbsolutePath()))
                .map(Path::toFile)
                .filter(File::isFile)
                .forEach(file -> consumer.accept(file.getName(), file.getAbsolutePath()));
    }

    @Override
    public void deleteFolder(final List<String> folderPaths) {
        if (CollectionUtils.isEmpty(folderPaths)) {
            return;
        }
        folderPaths.forEach(absolute -> {
            try {
                FileUtils.forceDeleteOnExit(Paths.get(absolute).toFile());
                log.info("删除文件夹【{}】: 成功", absolute);
            } catch (Exception e) {
                log.error("删除文件夹【{}】: {}", absolute, e.getMessage(), e);
            }
        });
    }

    @Override
    public void deleteFile(final List<String> filePaths) {
        if (CollectionUtils.isEmpty(filePaths)) {
            return;
        }
        filePaths.forEach(absolute -> {
            try {
                final boolean success = Files.deleteIfExists(Paths.get(absolute));
                log.info("删除文件【{}】: {}", absolute, success);
            } catch (Exception e) {
                log.error("删除文件【{}】: {}", absolute, e.getMessage(), e);
            }
        });
    }

}
