package com.ccx.demo.config;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * JPA + MongoDB 部分自定义配置
 * <pre>
 * Spring Data JPA 配置
 *   参考配置：https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
 *   Hibernate 命名策略可自定义
 *     参考配置：https://www.baeldung.com/hibernate-naming-strategy
 *   QueryDSL
 *     参考配置：http://www.querydsl.com/static/querydsl/latest/reference/html_single/#jpa_integration
 *   BlazeQueryDSL
 *     参考配置：https://persistence.blazebit.com/documentation/1.5/core/manual/en_US/index.html#querydsl-integration
 *   QueryDSL SQL Spring:
 *     参考配置：http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html
 * Spring Data MongoDB 配置, AbstractMongoConfiguration 已经默认实现了 mongoTemplate和mongoFactoryBean
 *   参考配置：https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference
 *   QueryDSL
 *     参考配置： http://www.querydsl.com/static/querydsl/latest/reference/html_single/#mongodb_integration
 *
 * @author 谢长春 2019/1/23
 */
@Configuration(proxyBeanMethods = false)
@EnableJpaRepositories(basePackages = {"com.ccx.demo.**.dao.jpa"})
public class QueryDSLConfiguration {
    @PersistenceContext
    private EntityManager entityManager;

    @Bean
    public JPAQueryFactory jpaQueryFactory() {
        return new JPAQueryFactory(entityManager);
    }

//    @Bean
//    public com.querydsl.sql.Configuration sqlQueryDslConfiguration() {
//        final com.querydsl.sql.SQLTemplates templates = com.querydsl.sql.MySQLTemplates.builder().build();
//        final com.querydsl.sql.Configuration configuration = new com.querydsl.sql.Configuration(templates);
//        configuration.setExceptionTranslator(new com.querydsl.sql.spring.SpringExceptionTranslator());
//        return configuration;
//    }
//
//    @Bean
//    public com.querydsl.sql.SQLQueryFactory sqlQueryFactory(final DataSource dataSource) {
//        final javax.inject.Provider<java.sql.Connection> provider = new com.querydsl.sql.spring.SpringConnectionProvider(dataSource);
//        return new com.querydsl.sql.SQLQueryFactory(sqlQueryDslConfiguration(), provider);
//    }
}
