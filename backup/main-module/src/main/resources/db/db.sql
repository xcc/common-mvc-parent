-- 建库命令
DROP DATABASE IF EXISTS app_demo_dev_db;
CREATE DATABASE app_demo_dev_db CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci';

-- 建表参考
DROP TABLE IF EXISTS tab_name;
CREATE TABLE tab_name
(
  id             BIGINT UNSIGNED     NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '数据ID',
  username       VARCHAR(15)         NOT NULL COMMENT '登录名',
  password       VARCHAR(150)        NOT NULL COMMENT '登录密码',
  nickname       VARCHAR(30)         NOT NULL DEFAULT '' COMMENT '昵称',
  phone          VARCHAR(11)         NOT NULL DEFAULT '' COMMENT '手机号',
  email          VARCHAR(30)         NOT NULL DEFAULT '' COMMENT '邮箱',
  avatar         JSON                NULL COMMENT '用户头像',
  roles          JSON                NOT NULL COMMENT '角色 ID 集合，tab_role.id，{@link Long}[]',
  registerSource TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账户注册渠道',
  createTime     CHAR(14)            NOT NULL COMMENT '创建时间，yyyyMMddHHmmss',    -- date_format(now(), '%Y%m%d%H%i%S');
  createUserId   BIGINT UNSIGNED     NOT NULL COMMENT '创建用户ID',
  updateTime     CHAR(17)            NOT NULL COMMENT '修改时间，yyyyMMddHHmmssSSS', -- concat(date_format(now(), '%Y%m%d%H%i%S'), '000');
  updateUserId   BIGINT UNSIGNED     NOT NULL COMMENT '修改用户ID',
  hidden         TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为隐藏用户，禁止编辑且前端列表不展示。 [0:否,1:是]',
  disabled       TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否禁用账号，禁用账号无法登录。 [0:否,1:是]',
  deleted        TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否逻辑删除。 [0:否,1:是]',
  -- createTime, updateTime； 使用前 8 位建立索引，每一天的数据在同一个树节点上，用于按时间区间查询优化
  KEY (createTime(8)),
  KEY (updateTime(8))
) ENGINE InnoDB
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_general_ci COMMENT '表说明'
  AUTO_INCREMENT 10000;


/*
# 创建用户 app_demo_dev_db_admin, 任意IP即可登录; 设置密码为 app_demo_dev_db@admin
CREATE USER 'app_demo_dev_db_admin' @'%' IDENTIFIED BY 'app_demo_dev_db@admin';

# 删除用户
DROP USER 'app_demo_dev_db_admin' @'%';

# 修改密码
SET PASSWORD FOR 'app_demo_dev_db_admin' @'%' = PASSWORD ('456');

# 为 app_demo_dev_db_admin 提供 app_demo_dev_db 数据库的全部增删改查权限
GRANT ALL PRIVILEGES ON app_demo_dev_db.* TO 'app_demo_dev_db_admin' @'%';

# 收回 app_demo_dev_db_admin 全部权限
REVOKE ALL ON app_demo_dev_db.* FROM 'app_demo_dev_db_admin' @'%';

# 问题排查：查询用户
select user,host from mysql.user;
# 问题排查：查看非 root 用户连接初始化参数
show variables like 'init_connect';
# 问题排查：重新设置 init_connect ， 原来的 init_connect 值包含字符串，新用户连接数据库会导致异常 => (init_connect command failed)
# set global init_connect='SET NAMES utf8mb4';
*/
/*
-- 建议将 utf8 改为 utf8mb4 **********************************************************************************************
utf8mb4_unicode_ci和utf8mb4_general_ci的对比：
准确性：
  utf8mb4_unicode_ci是基于标准的Unicode来排序和比较，能够在各种语言之间精确排序
  utf8mb4_general_ci没有实现Unicode排序规则，在遇到某些特殊语言或者字符集，排序结果可能不一致。
  但是，在绝大多数情况下，这些特殊字符的顺序并不需要那么精确。
性能
  utf8mb4_general_ci在比较和排序的时候更快
  utf8mb4_unicode_ci在特殊情况下，Unicode排序规则为了能够处理特殊字符的情况，实现了略微复杂的排序算法。
  但是在绝大多数情况下发，不会发生此类复杂比较。相比选择哪一种collation，使用者更应该关心字符集与排序规则在db里需要统一。
*/
/*
-- 查看字符集 ************************************************************************************************************
SHOW VARIABLES WHERE Variable_name LIKE 'character_set_%' OR Variable_name LIKE 'collation%';

character_set_client	    (客户端来源数据使用的字符集)
character_set_connection	(连接层字符集)
character_set_database	(当前选中数据库的默认字符集)
character_set_results	(查询结果字符集)
character_set_server	    (默认的内部操作字符集)
*/
/*
-- 远程数据库同步到本地 ****************************************************************************************************
mysqldump -h ${remote_host} -uroot -p111111 ${remote_db} | mysql -uroot -p111111 ${local_db}

mysqldump -h ${remote_host} -uroot -p111111 ${remote_db} --tables ${table_name1} ${table_name2} ${table_name3} | mysql -uroot -p111111 ${local_db}
*/
/*
-- 备份 *****************************************************************************************************************
mysqldump -h ${host} -uroot -p111111 db_name > db_name.sql
*/
/*
-- 恢复 *****************************************************************************************************************
mysql -h ${host} -uroot -p111111 db_name < db_name.sql
mysql -h ${host} -uroot -p111111 db_name   \n   source db_name.sql
*/
/*
-- 另一种备份恢复 *********************************************************************************************************
# 查看 csv 文件存储目录
SHOW GLOBAL VARIABLES WHERE Variable_name ='secure_file_priv';
# 表数据导出到 csv 文件
SELECT * INTO OUTFILE '/var/lib/mysql-files/{table_name}.csv' FROM {table_name};
# 从 csv 文件还原数据
LOAD DATA INFILE '/var/lib/mysql-files/{table_name}.csv' INTO TABLE {table_name};
*/
/*
-- 查看所有表定义参数，where name = '指定表名' ******************************************************************************
SHOW TABLE STATUS FROM app_demo_dev_db;
*/
/*
-- 查看 DDL *************************************************************************************************************
SHOW CREATE TABLE tab_code_example;
*/
/*
-- 表全量信息 ************************************************************************************************************
SHOW FULL COLUMNS FROM tab_code_example;
*/
/*
-- 表部分信息 ************************************************************************************************************
DESCRIBE tab_code_example;
*/
/*
-- 查看表数据行数(只是近似值,实际的数据行数以 count 为准) **********************************************************************
SELECT TABLE_NAME, TABLE_ROWS FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_SCHEMA = 'app_demo_dev_db'
*/
/*
-- 查看当前链接数量 *******************************************************************************************************
SELECT * FROM information_schema.PROCESSLIST WHERE db = 'app_demo_dev_db' ORDER BY TIME DESC;
*/
/*
-- 查看当前执行 SQL 线程 id ***********************************************************************************************
SELECT t.trx_mysql_thread_id,t.* FROM information_schema.INNODB_TRX t;
SELECT t.THREAD_ID,t.* FROM performance_schema.threads t WHERE PROCESSLIST_ID = ${trx_mysql_thread_id};
SELECT * FROM performance_schema.events_statements_current WHERE THREAD_ID = ${THREAD_ID};
*/
/*
-- 新建字段时判断字段是否存在 ***********************************************************************************************
DROP PROCEDURE IF EXISTS add_column;
DELIMITER $$ -- 声明存储过程的结束符
CREATE PROCEDURE add_column()
BEGIN
  IF EXISTS(SELECT * FROM information_schema.columns WHERE table_schema = DATABASE() AND table_name = '表名' AND column_name = '字段名') THEN
    ALTER TABLE DROP COLUMN 字段名;
    -- ALTER TABLE 表名 MODIFY 字段名 int(1) NOT NULL DEFAULT 0 COMMENT '字段说明';
    -- ALTER TABLE 表名 ADD 字段名 int(1) NOT NULL DEFAULT 0 COMMENT '字段说明';
  END IF;
END $$
DELIMITER ; -- 将语句的结束符号恢复为分号
CALL add_column();
DROP PROCEDURE IF EXISTS add_column;
*/
/*
-- 新建索引时判断索引是否存在 ***********************************************************************************************
DROP PROCEDURE IF EXISTS add_index;
DELIMITER $$ -- 声明存储过程的结束符
CREATE PROCEDURE add_index()
BEGIN
  IF EXISTS(SELECT * FROM information_schema.statistics WHERE table_schema = DATABASE() AND table_name = '表名' AND index_name = '表名_字段名') THEN
    ALTER TABLE 表名 DROP INDEX 表名_字段名 (字段名);
    -- ALTER TABLE 表名 ADD INDEX 表名_字段名 (字段名);
    -- ALTER TABLE 表名 ADD UNIQUE INDEX 表名_字段名 (字段名); -- 添加唯一索引
  END IF;
END $$
DELIMITER ; -- 将语句的结束符号恢复为分号
CALL add_index();
DROP PROCEDURE IF EXISTS add_index;
*/

-- 查询所有视图
-- SHOW TABLE STATUS WHERE Comment = 'view';

/*
CREATE OR REPLACE VIEW view_vip AS
  SELECT
    u.id, -- 用户ID
    u.email, -- 用户邮箱
    u.nickname, -- 用户昵称
    u.role, -- ROLE_ADMIN【0】:超级管理员,ROLE_SYS_ADMIN【1】:系统管理员,ROLE_USER【2】:普通用户,ROLE_VIP【3】:VIP用户
    u.phone -- 手机号
  FROM tab_user u
  WHERE u.role = 3
;
SELECT * FROM view_vip;
*/

/*
-- 测试 createTime 数据类型从 timestamp 改成 char 之后按时间查询场景 **********************************************************
-- 查询整年数据
EXPLAIN SELECT * FROM tab_user WHERE createTime LIKE '2021%'; -- 索引有效
EXPLAIN SELECT * FROM tab_user WHERE createTime >= '20210101000000' AND createTime <=  '20211231235959'; -- 索引有效
-- 查询 6 月数据
EXPLAIN SELECT * FROM tab_user WHERE createTime LIKE '202106%'; -- 索引有效
EXPLAIN SELECT * FROM tab_user WHERE createTime >= '20210601' AND createTime <  '20210701'; -- 索引可能有效， 不能用 '<=' 符号，
EXPLAIN SELECT * FROM tab_user WHERE createTime BETWEEN '20210601' AND '20210701' ; -- 索引可能有效
EXPLAIN SELECT * FROM tab_user WHERE createTime >= '20210601000000' AND createTime <=  '20210631235959'; -- 索引有效
EXPLAIN SELECT * FROM tab_user WHERE createTime BETWEEN '20210601000000' AND '20210631235959'; -- 索引有效
-- 查询 6-7 月数据
EXPLAIN SELECT * FROM tab_user WHERE createTime REGEXP '20210[67]'; -- 索引无效
EXPLAIN SELECT * FROM tab_user WHERE createTime LIKE '202106%' OR createTime LIKE '202107%'; -- or 前面索引有效， or 后面索引无效
EXPLAIN SELECT * FROM tab_user WHERE createTime >= '202106' AND createTime <  '202108'; -- 索引有效
EXPLAIN SELECT * FROM tab_user WHERE createTime BETWEEN '202106' AND '202108'; -- 索引有效
EXPLAIN SELECT * FROM tab_user WHERE createTime >= '20210601' AND createTime <  '20210801'; -- 索引可能有效
EXPLAIN SELECT * FROM tab_user WHERE createTime BETWEEN '20210601' AND '20210801'; -- 索引可能有效
EXPLAIN SELECT * FROM tab_user WHERE createTime >= '20210601000000' AND createTime <=  '20210731235959'; -- 索引有效
EXPLAIN SELECT * FROM tab_user WHERE createTime BETWEEN '20210601000000' AND '20210731235959'; -- 索引有效
*/
/*

-- 存储过程
DROP PROCEDURE IF EXISTS proc_tab_name;
DELIMITER $$ -- 声明存储过程的结束符
CREATE PROCEDURE proc_tab_name()
BEGIN
  SET @idx = 1;
  WHILE @idx <= 1000 DO
    INSERT INTO tab_name(id, fromId, toId) VALUES (left(REPLACE(UUID(), '-', ''), 18), FLOOR(1 + RAND()*(10-1)), FLOOR(1 + RAND()*(10-1)));
    SET @idx = @idx + 1;
  END while;
END $$
DELIMITER ; -- 将语句的结束符号恢复为分号
CALL proc_tab_name();
DROP PROCEDURE IF EXISTS proc_tab_name;
*/
