INSERT INTO tab_user(id, username, phone, password, nickname, roles, hidden, createTime, updateTime)
VALUES
     -- 初始化超级管理员账户，密码：superadmin 。 18888888888
       (1000, 'admin', '7uXXgCW32af/0TP8OlXVrA==', '$2a$10$2Z/gKxCNhoRQBO.ATJUco.nSBotBRXktNfalKkCx2tmOnMqYkvAvC', '超级管理员', '["5j3m2ypc1y6qXBSQ4h"]', 1, DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
     -- user:111111 。 18800000001
     , (1001, 'user', 'SC91Hn7QiFHL5ArfJyrxgA==', '$2a$10$6unbpf74Dc7NEBywaCHl..FzzprMb69gA.Qi09U7ud7vlKHP9PXfu', '普通用户', '["5j3m2ypc1y6aFFUS0E"]', 1, DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
     -- 初始化超级管理员测试账户，密码：superadmin 。 18800000002
     , (1002, 'admin-test', '+KvUnGEa0sVitgX2+LLp0A==', '$2a$10$2Z/gKxCNhoRQBO.ATJUco.nSBotBRXktNfalKkCx2tmOnMqYkvAvC', '超级管理员', '["5j3m2ypc1y6zA0p37N"]', 1, DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
     -- user:111111 。 18800000003
     , (1003, 'user-test', '7XPwId8ZxG7U5oK423BJPw==', '$2a$10$6unbpf74Dc7NEBywaCHl..FzzprMb69gA.Qi09U7ud7vlKHP9PXfu', '普通用户', '["5j3m2ypc1y61GZGSOO"]', 1, DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
;
