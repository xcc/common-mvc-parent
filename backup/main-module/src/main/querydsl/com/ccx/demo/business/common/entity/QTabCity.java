package com.ccx.demo.business.common.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCity is a Querydsl query type for TabCity
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCity extends EntityPathBase<TabCity> {

    private static final long serialVersionUID = 1101287762L;

    public static final QTabCity tabCity = new QTabCity("tabCity");

    public final StringPath id = createString("id");

    public final StringPath name = createString("name");

    public final StringPath pid = createString("pid");

    public final StringPath pinyin = createString("pinyin");

    public final StringPath py = createString("py");

    public QTabCity(String variable) {
        super(TabCity.class, forVariable(variable));
    }

    public QTabCity(Path<? extends TabCity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCity(PathMetadata metadata) {
        super(TabCity.class, metadata);
    }

}

