package com.ccx.demo.business.user.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabRole is a Querydsl query type for TabRole
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabRole extends EntityPathBase<TabRole> {

    private static final long serialVersionUID = -2085482243L;

    public static final QTabRole tabRole = new QTabRole("tabRole");

    public final ArrayPath<String[], String> authorities = createArray("authorities", String[].class);

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Long> createUserId = createNumber("createUserId", Long.class);

    public final BooleanPath deleted = createBoolean("deleted");

    public final BooleanPath hidden = createBoolean("hidden");

    public final StringPath id = createString("id");

    public final StringPath name = createString("name");

    public final StringPath remark = createString("remark");

    public final StringPath updateTime = createString("updateTime");

    public final NumberPath<Long> updateUserId = createNumber("updateUserId", Long.class);

    public QTabRole(String variable) {
        super(TabRole.class, forVariable(variable));
    }

    public QTabRole(Path<? extends TabRole> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabRole(PathMetadata metadata) {
        super(TabRole.class, metadata);
    }

}

