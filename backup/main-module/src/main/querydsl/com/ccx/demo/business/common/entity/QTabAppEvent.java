package com.ccx.demo.business.common.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabAppEvent is a Querydsl query type for TabAppEvent
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabAppEvent extends EntityPathBase<TabAppEvent> {

    private static final long serialVersionUID = -492330656L;

    public static final QTabAppEvent tabAppEvent = new QTabAppEvent("tabAppEvent");

    public final StringPath data = createString("data");

    public final EnumPath<com.ccx.demo.enums.AppEvent> event = createEnum("event", com.ccx.demo.enums.AppEvent.class);

    public final StringPath eventTime = createString("eventTime");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> refLongId = createNumber("refLongId", Long.class);

    public final StringPath refStringId = createString("refStringId");

    public final NumberPath<Long> userId = createNumber("userId", Long.class);

    public QTabAppEvent(String variable) {
        super(TabAppEvent.class, forVariable(variable));
    }

    public QTabAppEvent(Path<? extends TabAppEvent> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabAppEvent(PathMetadata metadata) {
        super(TabAppEvent.class, metadata);
    }

}

