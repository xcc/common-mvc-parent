package com.querydsl.entity;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QTabAuthId is a Querydsl query type for QTabAuthId
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTabAuthId extends com.querydsl.sql.RelationalPathBase<QTabAuthId> {

    private static final long serialVersionUID = -290697914;

    public static final QTabAuthId tabAuthId = new QTabAuthId("tab_auth_id");

    public final DateTimePath<java.sql.Timestamp> createTime = createDateTime("createTime", java.sql.Timestamp.class);

    public final StringPath createUserId = createString("createUserId");

    public final StringPath updateUserId = createString("updateUserId");

    public final NumberPath<Byte> deleted = createNumber("deleted", Byte.class);

    public final StringPath id = createString("id");

    public final StringPath name = createString("name");

    public final DateTimePath<java.sql.Timestamp> updateTime = createDateTime("updateTime", java.sql.Timestamp.class);

    public final com.querydsl.sql.PrimaryKey<QTabAuthId> primary = createPrimaryKey(id);

    public QTabAuthId(String variable) {
        super(QTabAuthId.class, forVariable(variable), "null", "tab_auth_id");
        addMetadata();
    }

    public QTabAuthId(String variable, String schema, String table) {
        super(QTabAuthId.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTabAuthId(String variable, String schema) {
        super(QTabAuthId.class, forVariable(variable), schema, "tab_auth_id");
        addMetadata();
    }

    public QTabAuthId(Path<? extends QTabAuthId> path) {
        super(path.getType(), path.getMetadata(), "null", "tab_auth_id");
        addMetadata();
    }

    public QTabAuthId(PathMetadata metadata) {
        super(QTabAuthId.class, metadata, "null", "tab_auth_id");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(createTime, ColumnMetadata.named("createTime").withIndex(3).ofType(Types.TIMESTAMP).withSize(19).notNull());
        addMetadata(createUserId, ColumnMetadata.named("createUserId").withIndex(4).ofType(Types.VARCHAR).withSize(18).notNull());
        addMetadata(deleted, ColumnMetadata.named("deleted").withIndex(7).ofType(Types.TINYINT).withSize(3).notNull());
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.CHAR).withSize(32).notNull());
        addMetadata(name, ColumnMetadata.named("name").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(updateTime, ColumnMetadata.named("updateTime").withIndex(5).ofType(Types.TIMESTAMP).withSize(23).notNull());
        addMetadata(updateUserId, ColumnMetadata.named("updateUserId").withIndex(6).ofType(Types.VARCHAR).withSize(18).notNull());
    }

}

