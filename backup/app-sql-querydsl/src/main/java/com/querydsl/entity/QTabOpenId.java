package com.querydsl.entity;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QTabOpenId is a Querydsl query type for QTabOpenId
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTabOpenId extends com.querydsl.sql.RelationalPathBase<QTabOpenId> {

    private static final long serialVersionUID = 105051496;

    public static final QTabOpenId tabOpenId = new QTabOpenId("tab_open_id");

    public final NumberPath<Long> fromId = createNumber("fromId", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> toId = createNumber("toId", Long.class);

    public final com.querydsl.sql.PrimaryKey<QTabOpenId> primary = createPrimaryKey(id);

    public QTabOpenId(String variable) {
        super(QTabOpenId.class, forVariable(variable), "null", "tab_open_id");
        addMetadata();
    }

    public QTabOpenId(String variable, String schema, String table) {
        super(QTabOpenId.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTabOpenId(String variable, String schema) {
        super(QTabOpenId.class, forVariable(variable), schema, "tab_open_id");
        addMetadata();
    }

    public QTabOpenId(Path<? extends QTabOpenId> path) {
        super(path.getType(), path.getMetadata(), "null", "tab_open_id");
        addMetadata();
    }

    public QTabOpenId(PathMetadata metadata) {
        super(QTabOpenId.class, metadata, "null", "tab_open_id");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fromId, ColumnMetadata.named("fromId").withIndex(2).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(toId, ColumnMetadata.named("toId").withIndex(3).ofType(Types.BIGINT).withSize(20).notNull());
    }

}

