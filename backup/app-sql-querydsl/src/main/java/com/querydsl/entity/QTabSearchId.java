package com.querydsl.entity;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QTabSearchId is a Querydsl query type for QTabSearchId
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTabSearchId extends com.querydsl.sql.RelationalPathBase<QTabSearchId> {

    private static final long serialVersionUID = -731101050;

    public static final QTabSearchId tabSearchId = new QTabSearchId("tab_search_id");

    public final NumberPath<Long> fromId = createNumber("fromId", Long.class);

    public final StringPath id = createString("id");

    public final NumberPath<Long> toId = createNumber("toId", Long.class);

    public final com.querydsl.sql.PrimaryKey<QTabSearchId> primary = createPrimaryKey(id);

    public QTabSearchId(String variable) {
        super(QTabSearchId.class, forVariable(variable), "null", "tab_search_id");
        addMetadata();
    }

    public QTabSearchId(String variable, String schema, String table) {
        super(QTabSearchId.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTabSearchId(String variable, String schema) {
        super(QTabSearchId.class, forVariable(variable), schema, "tab_search_id");
        addMetadata();
    }

    public QTabSearchId(Path<? extends QTabSearchId> path) {
        super(path.getType(), path.getMetadata(), "null", "tab_search_id");
        addMetadata();
    }

    public QTabSearchId(PathMetadata metadata) {
        super(QTabSearchId.class, metadata, "null", "tab_search_id");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fromId, ColumnMetadata.named("fromId").withIndex(2).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.CHAR).withSize(32).notNull());
        addMetadata(toId, ColumnMetadata.named("toId").withIndex(3).ofType(Types.BIGINT).withSize(20).notNull());
    }

}

