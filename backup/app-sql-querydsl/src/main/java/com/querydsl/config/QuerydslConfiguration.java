package com.querydsl.config;

import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.spring.SpringConnectionProvider;
import com.querydsl.sql.spring.SpringExceptionTranslator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.inject.Provider;
import javax.sql.DataSource;
import java.sql.Connection;

/**
 * querydsl 配置
 *
 * @author 谢长春 2021-03-17
 */
@Configuration(proxyBeanMethods = false)
public class QuerydslConfiguration {
    @Bean
    public com.querydsl.sql.Configuration querydslConfiguration() {
        final SQLTemplates templates = MySQLTemplates.builder().build();
        final com.querydsl.sql.Configuration configuration = new com.querydsl.sql.Configuration(templates);
        configuration.setExceptionTranslator(new SpringExceptionTranslator());
        // declares a mapping for the gender column in the person table
        // configuration.register("person", "gender",  new EnumByNameType<Gender>(Gender.class));
        return configuration;
    }

    @Bean
    public SQLQueryFactory sqlQueryFactory(
            final com.querydsl.sql.Configuration querydslConfiguration,
            final DataSource dataSource
    ) {
        Provider<Connection> provider = new SpringConnectionProvider(dataSource);
        return new SQLQueryFactory(querydslConfiguration, provider);
    }
}
