package com.ccx.demo.business.code.dao.sql;

import com.alibaba.fastjson.annotation.JSONField;
import com.ccx.demo.business.code.entity.TabSearchId;
import com.google.common.collect.Sets;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.entity.QTabSearchId;
import com.support.mvc.entity.base.Page;
import com.utils.enums.Limit;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static com.ccx.demo.config.init.AppInit.getQueryFactory;

/**
 * 数据操作：测试 SearchId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Repository
public class SearchIdRepository {
    // 每个 DAO 层顶部只能有一个查询实体,且必须以 q 命名,表示当前操作的数据库表. 当 q 作为主表的连接查询方法也必须写在这个类
    QTabSearchId q = QTabSearchId.tabSearchId;

    /**
     * 测试 SearchId 模板表 按 ID 查询
     *
     * @param id {@link String} 数据ID
     * @return {@link Optional<TabSearchId>} 实体对象
     */
    public Optional<TabSearchId> findById(final String id) {
        return Optional.ofNullable(getQueryFactory()
                .select(Projections.bean(TabSearchId.class, q.all()))
                .from(q)
                .where(q.id.eq(id))
                .fetchOne()
        );
    }

    /**
     * 测试 SearchId 模板表 按 id 批量查询
     *
     * @param ids Set<String> id 集合
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public List<TabSearchId> findAllById(final Set<String> ids) {
        return getQueryFactory()
                .select(Projections.bean(TabSearchId.class, q.all()))
                .from(q)
                .where(q.id.in(ids))
                .fetch();
    }


    /**
     * 测试 SearchId 模板表 按 ID 批量查询，以 id 为 key 返回map
     *
     * @param ids {@link String} 数据ID
     * @return {@link Map<String, TabSearchId>} Map 对象集合
     */
    public Map<String, TabSearchId> mapByIds(final Iterable<String> ids) {
        return findAllById(Sets.newHashSet(ids))
                .stream()
                .collect(Collectors.toMap(TabSearchId::getId, TabSearchId::cloneObject));
    }

    /**
     * 测试 SearchId 模板表 按 ID 批量查询，以 id 为 key 返回map
     *
     * @param ids {@link String} 数据ID
     * @return {@link Map<String, TabSearchId>} Map 对象集合
     */
    public Map<String, TabSearchId> mapByIds(final Iterable<String> ids, Expression<?>... exps) {
        final Map<String, TabSearchId> map = new HashMap<>();
        getQueryFactory()
                .select(q.id, Projections.bean(TabSearchId.class, exps))
                .from(q)
                .fetch()
                .forEach(tuple -> map.put(tuple.get(q.id), tuple.get(1, TabSearchId.class)));
        return map;
    }

    /**
     * 测试 SearchId 模板表 按条件分页查询， 仅返回 id 字段，用于分页查询优化
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param page     {@link Page} 分页
     * @return {@link QueryResults<String>} 分页结果集
     */
    public QueryResults<String> pageIds(final TabSearchId condition, final Page page) {
        return getQueryFactory()
                .select(q.id)
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 SearchId 模板表 按条件分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param page     {@link Page} 分页
     * @return {@link QueryResults<TabSearchId>} 分页结果集
     */
    public QueryResults<TabSearchId> page(final TabSearchId condition, final Page page) {
        return getQueryFactory()
                .select(Projections.bean(TabSearchId.class, q.all()))
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 SearchId 模板表 按条件分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param page     {@link Page} 分页
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link QueryResults<TabSearchId>} 分页结果集
     */
    public QueryResults<TabSearchId> page(final TabSearchId condition, final Page page, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabSearchId.class, exps))
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 SearchId 模板表 按条件分页查询，投影到 VO 类
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param page     {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link QueryResults<TabSearchId>} 分页结果集
     */
    public <T extends TabSearchId> QueryResults<T> pageProjection(final TabSearchId condition, final Page page, final Class<T> clazz) {
        return pageProjection(condition, page, clazz, TabSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板表 按条件分页查询，查询指定字段，投影到 VO 类
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param page     {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link QueryResults<TabSearchId>} 分页结果集
     */
    public <T extends TabSearchId> QueryResults<T> pageProjection(final TabSearchId condition, final Page page, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回 id ，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @return {@link List<String>} ID集合
     */
    public List<String> listIds(final TabSearchId condition) {
        return listIds(Limit.L1000.value, condition);
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabSearchId} 查询条件
     * @return {@link List<String>} ID集合
     */
    public List<String> listIds(final long limit, final TabSearchId condition) {
        return getQueryFactory()
                .select(q.id)
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public List<TabSearchId> list(final TabSearchId condition) {
        return list(Limit.L1000.value, condition);
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabSearchId} 查询条件
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public List<TabSearchId> list(final long limit, final TabSearchId condition) {
        return getQueryFactory()
                .select(Projections.bean(TabSearchId.class, q.all()))
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public List<TabSearchId> list(final TabSearchId condition, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, exps);
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabSearchId} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public List<TabSearchId> list(final long limit, final TabSearchId condition, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabSearchId.class, q.all()))
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public <T extends TabSearchId> List<T> listProjection(final TabSearchId condition, final Class<T> clazz) {
        return listProjection(Limit.L1000.value, condition, clazz, TabSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public <T extends TabSearchId> List<T> listProjection(final long limit, final TabSearchId condition, final Class<T> clazz) {
        return listProjection(limit, condition, clazz, TabSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public <T extends TabSearchId> List<T> listProjection(final TabSearchId condition, final Class<T> clazz, final Expression<?>... exps) {
        return listProjection(Limit.L1000.value, condition, clazz, exps);
    }

    /**
     * 测试 SearchId 模板表 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabSearchId>} 实体对象集合
     */
    public <T extends TabSearchId> List<T> listProjection(final long limit, final TabSearchId condition, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

}
