package com.ccx.demo.business.code.dao.sql;

import com.alibaba.fastjson.annotation.JSONField;
import com.ccx.demo.business.code.entity.TabOpenId;
import com.google.common.collect.Sets;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.entity.QTabOpenId;
import com.querydsl.sql.dml.SQLInsertClause;
import com.support.mvc.entity.base.Page;
import com.utils.enums.Limit;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ccx.demo.config.init.AppInit.getQueryFactory;

/**
 * 数据操作：测试 OpenId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Repository
public class OpenIdRepository {
    // 每个 DAO 层顶部只能有一个查询实体,且必须以 q 命名,表示当前操作的数据库表. 当 q 作为主表的连接查询方法也必须写在这个类
    QTabOpenId q = QTabOpenId.tabOpenId;


    /**
     * 测试 OpenId 模板表 新增数据
     *
     * @param obj {@link TabOpenId} 新增对象
     * @return {@link TabOpenId} 新增对象
     */
    public TabOpenId save(final TabOpenId obj) {
        final Long id = obj.insert(getQueryFactory().insert(q))
                .get()
                .executeWithKey(q.id);
        obj.setId(id);
        return obj;
    }

    /**
     * 测试 OpenId 模板表 批量新增数据
     *
     * @param list {@link List<TabOpenId>} 新增对象集合
     * @return {@link List<TabOpenId>} 新增对象集合
     */
    public List<TabOpenId> saveAll(final List<TabOpenId> list) {
        final SQLInsertClause batchInsert = getQueryFactory().insert(q);
        list.forEach(obj -> obj.insert(batchInsert).get().addBatch());
        final List<Long> ids = batchInsert.executeWithKeys(q.id);
        for (int i = 0; i < ids.size(); i++) {
            list.get(i).setId(ids.get(i));
        }
        return list;
    }

    /**
     * 测试 OpenId 模板表 更新数据
     *
     * @param id  {@link Long} 数据ID
     * @param obj {@link TabOpenId} 更新对象
     * @return {@link Long} 更新行数
     */
    public long update(
            final Long id

            , final TabOpenId obj) {
        return obj.update(getQueryFactory().update(q))
                .get()
                .where(q.id.eq(id))
                .execute();
    }

//    /**
//     * 测试 OpenId 模板表 按 ID 物理删除
//     *
//     * @param id   {@link Long} 数据ID
//     * @return {@link TabOpenId} 实体对象
//     */
//    public TabOpenId deleteById(
//             final Long id
//             ) {
//        return findById(id)
//                .map(obj -> {
//                    getQueryFactory().delete(q).where(q.id.eq(id)).execute();
//                    return obj;
//                })
//                .orElseThrow(() -> new NullPointerException("数据物理删除失败：".concat(Objects.toString(id))));
//    }

    /**
     * 测试 OpenId 模板表 按 ID 查询
     *
     * @param id {@link Long} 数据ID
     * @return {@link Optional<TabOpenId>} 实体对象
     */
    public Optional<TabOpenId> findById(final Long id) {
        return Optional.ofNullable(getQueryFactory()
                .select(Projections.bean(TabOpenId.class, q.all()))
                .from(q)
                .where(q.id.eq(id))
                .fetchOne()
        );
    }

    /**
     * 测试 OpenId 模板表 按 id 批量查询
     *
     * @param ids Set<String> id 集合
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public List<TabOpenId> findAllById(final Set<Long> ids) {
        return getQueryFactory()
                .select(Projections.bean(TabOpenId.class, q.all()))
                .from(q)
                .where(q.id.in(ids))
                .fetch();
    }


    /**
     * 测试 OpenId 模板表 按 ID 批量查询，以 id 为 key 返回map
     *
     * @param ids {@link Long} 数据ID
     * @return {@link Map<Long, TabOpenId>} Map 对象集合
     */
    public Map<Long, TabOpenId> mapByIds(final Iterable<Long> ids) {
        return findAllById(Sets.newHashSet(ids))
                .stream()
                .collect(Collectors.toMap(TabOpenId::getId, TabOpenId::cloneObject));
    }

    /**
     * 测试 OpenId 模板表 按条件分页查询， 仅返回 id 字段，用于分页查询优化
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param page     {@link Page} 分页
     * @return {@link QueryResults<Long>} 分页结果集
     */
    public QueryResults<Long> pageIds(final TabOpenId condition, final Page page) {
        return getQueryFactory()
                .select(q.id)
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 OpenId 模板表 按条件分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param page     {@link Page} 分页
     * @return {@link QueryResults<TabOpenId>} 分页结果集
     */
    public QueryResults<TabOpenId> page(final TabOpenId condition, final Page page) {
        return getQueryFactory()
                .select(Projections.bean(TabOpenId.class, q.all()))
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 OpenId 模板表 按条件分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param page     {@link Page} 分页
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link QueryResults<TabOpenId>} 分页结果集
     */
    public QueryResults<TabOpenId> page(final TabOpenId condition, final Page page, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabOpenId.class, exps))
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 OpenId 模板表 按条件分页查询，投影到 VO 类
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param page     {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link QueryResults<TabOpenId>} 分页结果集
     */
    public <T extends TabOpenId> QueryResults<T> pageProjection(final TabOpenId condition, final Page page, final Class<T> clazz) {
        return pageProjection(condition, page, clazz, TabOpenId.allColumnAppends());
    }

    /**
     * 测试 OpenId 模板表 按条件分页查询，查询指定字段，投影到 VO 类
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param page     {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link QueryResults<TabOpenId>} 分页结果集
     */
    public <T extends TabOpenId> QueryResults<T> pageProjection(final TabOpenId condition, final Page page, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(q)
                .where(condition.where().toArray())
                .offset(page.offset())
                .limit(page.limit())
                .orderBy(condition.qdslOrderBy())
                .fetchResults();
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回 id ，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @return {@link List<Long>} ID集合
     */
    public List<Long> listIds(final TabOpenId condition) {
        return listIds(Limit.L1000.value, condition);
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabOpenId} 查询条件
     * @return {@link List<Long>} ID集合
     */
    public List<Long> listIds(final long limit, final TabOpenId condition) {
        return getQueryFactory()
                .select(q.id)
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public List<TabOpenId> list(final TabOpenId condition) {
        return list(Limit.L1000.value, condition);
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabOpenId} 查询条件
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public List<TabOpenId> list(final long limit, final TabOpenId condition) {
        return getQueryFactory()
                .select(Projections.bean(TabOpenId.class, q.all()))
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public List<TabOpenId> list(final TabOpenId condition, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, exps);
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabOpenId} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public List<TabOpenId> list(final long limit, final TabOpenId condition, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabOpenId.class, q.all()))
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public <T extends TabOpenId> List<T> listProjection(final TabOpenId condition, final Class<T> clazz) {
        return listProjection(Limit.L1000.value, condition, clazz, TabOpenId.allColumnAppends());
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabOpenId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public <T extends TabOpenId> List<T> listProjection(final long limit, final TabOpenId condition, final Class<T> clazz) {
        return listProjection(limit, condition, clazz, TabOpenId.allColumnAppends());
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabOpenId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public <T extends TabOpenId> List<T> listProjection(final TabOpenId condition, final Class<T> clazz, final Expression<?>... exps) {
        return listProjection(Limit.L1000.value, condition, clazz, exps);
    }

    /**
     * 测试 OpenId 模板表 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabOpenId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return {@link List<TabOpenId>} 实体对象集合
     */
    public <T extends TabOpenId> List<T> listProjection(final long limit, final TabOpenId condition, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(q)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

}
