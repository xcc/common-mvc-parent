package com.ccx.demo.business.code.controller;

import com.alibaba.fastjson.JSON;
import com.ccx.demo.business.code.entity.TabAuthId;
import com.ccx.demo.business.code.service.AuthIdService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.support.mvc.entity.IUser;
import com.support.mvc.entity.base.Page;
import com.support.mvc.entity.base.Result;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.annotations.ApiIgnore;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Optional;
import java.util.Set;

/**
 * 对外接口：测试 AuthId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Api(tags = "测试 AuthId 模板表")
//@ApiSort() // 控制接口排序
@RequestMapping("/auth-id")
@Controller
@Slf4j
@RequiredArgsConstructor
public class AuthIdController  {

    private final AuthIdService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询测试 AuthId 模板表", tags = {""})
    @ApiOperationSupport(
            order = 1,
            author = "谢长春",
            includeParameters = {"number", "limit", "orderBy", "name", "createUserId", "updateUserId", "deleted"},
            ignoreParameters = {"createTime", "updateTime"}
    )
    @ResponseBody
        public Result<TabAuthId> page(@ApiIgnore @AuthenticationPrincipal final IUser user,
@ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
 final TabAuthId condition) {
        return new Result<TabAuthId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.page(
                    Optional.ofNullable(condition).orElseGet(TabAuthId::new),
                    Page.builder().number(number).limit(limit).build()
            ));
        });
    }

// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询测试 AuthId 模板表", tags = {""})
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "name", "createUserId", "updateUserId", "deleted"},
//            ignoreParameters = {"createTime", "updateTime"}
//    )
//    @ResponseBody
//    //    public Result<TabAuthId> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabAuthId condition) {
//        return new Result<TabAuthId>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list(
//                Optional.ofNullable(condition).orElseGet(TabAuthId::new),
//            ));
//        });
//    }

    // 优先使用 find 方法，可以阻止平行越权。 只有在实体没有 uuid 的情况才能将该方法开放给前端
    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询测试 AuthId 模板表", tags = {""})
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
        public Result<TabAuthId> findById(@ApiIgnore @AuthenticationPrincipal final IUser user,
@ApiParam(required = true, value = "数据id", example = "1") @PathVariable final String id) {
        return new Result<TabAuthId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_save')")
    @ApiOperation(value = "新增测试 AuthId 模板表", tags = {""})
    @ApiImplicitParam(name = "body", dataType = "TabAuthId", dataTypeClass = TabAuthId.class, required = true)
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            ignoreParameters = {
                    "body.id", "body.uuid", "body.deleted", "body.createTime", "body.createUserId", "body.createUserName", "body.updateTime", "body.updateUserId", "body.updateUserName", "body.timestamp", "body.orderBy"
            })
    @ResponseBody
        public Result<TabAuthId> save(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestBody(required = false) final String body) {
        return new Result<TabAuthId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.save(JSON.parseObject(body, TabAuthId.class), user.getId()));
        });
    }

    @PutMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
    @ApiOperation(value = "修改测试 AuthId 模板表", tags = {""})
    @ApiImplicitParam(name = "body", dataType = "TabAuthId", dataTypeClass = TabAuthId.class, required = true)
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            ignoreParameters = {
                    "body.id", "body.uuid", "body.deleted", "body.createTime", "body.createUserId", "body.createUserName", "body.updateTime", "body.updateUserId", "body.updateUserName", "body.timestamp", "body.orderBy"
            })
    @ResponseBody
        public Result<Void> update(@ApiIgnore @AuthenticationPrincipal final IUser user,
@ApiParam(required = true, value = "数据id", example = "1") @PathVariable final String id, @RequestBody(required = false) final String body) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.update(id, user.getId(), JSON.parseObject(body, TabAuthId.class));
        });
    }

// 优先使用 delete 方法，可以阻止平行越权。 只有在实体没有 uuid 的情况才能将该方法开放给前端
//    @DeleteMapping("/{id}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除测试 AuthId 模板表", tags = {""})
//    @ApiOperationSupport(order = 6, author = "谢长春")
//    @ResponseBody
//    //    public Result<Void> deleteById(@ApiIgnore @AuthenticationPrincipal final IUser user, @ApiParam(required = true, value = "数据id", example = "1") @PathVariable final String id) {
//        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
//            return service.deleteById(id, user.getId());
//        });
//    }

    // 优先使用 markDelete 方法，可以阻止平行越权。 只有在实体没有 uuid 的情况才能将该方法开放给前端
    @PatchMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除测试 AuthId 模板表", tags = {""})
    @ApiOperationSupport(order = 7, author = "谢长春")
    @ResponseBody
        public Result<Void> markDeleteById(@ApiIgnore @AuthenticationPrincipal final IUser user,
@ApiParam(required = true, value = "数据id", example = "1") @PathVariable final String id) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.markDeleteById(id, user.getId());
        });
    }

    @PatchMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "批量逻辑删除测试 AuthId 模板表", tags = {""})
    @ApiOperationSupport(order = 8, author = "谢长春")
    @ResponseBody
        public Result<Void> markDeleteByIds(@ApiIgnore @AuthenticationPrincipal final IUser user, final Set<String> body) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.markDeleteByIds(body, user.getId());
        });
    }

//    @PatchMapping
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "批量逻辑删除测试 AuthId 模板表", tags = {""})
//    @ApiOperationSupport(order = 9, author = "谢长春")
//    @ResponseBody
//    //    public Result<Void> markDelete(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestBody(required = false) final List<MarkDelete> body) {
//        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
//            service.markDelete(body, user.getId());
//        });
//    }

}
