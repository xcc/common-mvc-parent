package com.ccx.demo.business.code.controller;

import com.ccx.demo.business.code.entity.TabSearchId;
import com.ccx.demo.business.code.service.SearchIdService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.support.mvc.entity.IUser;
import com.support.mvc.entity.base.Page;
import com.support.mvc.entity.base.Result;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.annotations.ApiIgnore;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

/**
 * 对外接口：测试 SearchId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Api(tags = "测试 SearchId 模板表")
//@ApiSort() // 控制接口排序
@RequestMapping("/search-id")
@Controller
@Slf4j
@RequiredArgsConstructor
public class SearchIdController  {

    private final SearchIdService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询测试 SearchId 模板表", tags = {""})
    @ApiOperationSupport(
            order = 1,
            author = "谢长春",
            includeParameters = {"number", "limit", "orderBy", "fromId", "toId"},
            ignoreParameters = {"createTime", "updateTime"}
    )
    @ResponseBody
        public Result<TabSearchId> page(@ApiIgnore @AuthenticationPrincipal final IUser user,
@ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
 final TabSearchId condition) {
        return new Result<TabSearchId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.page(
                    Optional.ofNullable(condition).orElseGet(TabSearchId::new),
                    Page.builder().number(number).limit(limit).build()
            ));
        });
    }

// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询测试 SearchId 模板表", tags = {""})
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "fromId", "toId"},
//            ignoreParameters = {"createTime", "updateTime"}
//    )
//    @ResponseBody
//    //    public Result<TabSearchId> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabSearchId condition) {
//        return new Result<TabSearchId>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list(
//                Optional.ofNullable(condition).orElseGet(TabSearchId::new),
//            ));
//        });
//    }

    // 优先使用 find 方法，可以阻止平行越权。 只有在实体没有 uuid 的情况才能将该方法开放给前端
    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询测试 SearchId 模板表", tags = {""})
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
        public Result<TabSearchId> findById(@ApiIgnore @AuthenticationPrincipal final IUser user,
@ApiParam(required = true, value = "数据id", example = "1") @PathVariable final String id) {
        return new Result<TabSearchId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

}
