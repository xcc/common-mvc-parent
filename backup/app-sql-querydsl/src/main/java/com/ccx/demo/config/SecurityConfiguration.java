package com.ccx.demo.config;

import com.support.mvc.entity.IOpenUser;
import com.support.mvc.entity.IUser;
import com.utils.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

/**
 * Spring Security 权限控制
 * <pre>
 *     中文文档：https://springcloud.cc/spring-security-zhcn.html
 *     官方文档：https://docs.spring.io/spring-security/site/docs/current/reference/html5/
 *             https://docs.spring.io/spring-security/site/docs/current/guides/helloworld-boot.html
 *
 *
 * @author 谢长春 2017年7月7日 下午1:28:31
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
// 使用 @EnableGlobalMethodSecurity 注解来启用基于注解的安全性
// @EnableGlobalMethodSecurity(securedEnabled = true) // 启用注解：@Secured；[@Secured("ROLE_USER"), @Secured("IS_AUTHENTICATED_ANONYMOUSLY")]
// @EnableGlobalMethodSecurity(prePostEnabled = true) // 启用注解：@PreAuthorize；[@PreAuthorize("hasAuthority('ROLE_USER')"), @PreAuthorize("isAnonymous()")]
@Slf4j
public class SecurityConfiguration {
    //    /**
//     * 自定义校验规则
//     */
//    @Configuration
//    public static class GlobalMethodSecurity extends GlobalMethodSecurityConfiguration {
//        @Override
//        protected MethodSecurityExpressionHandler createExpressionHandler() {
//            final DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
//            expressionHandler.setPermissionEvaluator(null);
//            return expressionHandler;
//        }
//    }
//
//    /**
//     * 开放接口，无需登录授权
//     *
//     * @author 谢长春 2017年7月7日 上午9:58:58
//     */
//    @Order(1)
//    @RequiredArgsConstructor
//    @Configuration(proxyBeanMethods = false)
//    public static class OpenWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
//
//        @Override
//        public void configure(WebSecurity web) {
//            // 解决静态资源被拦截的问题
//            web.ignoring().antMatchers( // resources 下的静态资源
//                    "/favicon.ico"
//                    , "/static/**"
//                    , "/druid/**"
//                    , "/doc.html"
//                    , "/webjars/**"
//                    , "/swagger-resources/**"
//                    , "/v2/api-docs/**"
//                    , "/v3/api-docs/**"
//            );
//        }
//
//        @Override
//        protected void configure(HttpSecurity http) throws Exception {
//            http
//                    .requestMatchers(configurer -> {
//                        configurer
//                                .antMatchers("/")
//                                // open 前缀的 url 不需要登录
//                                .antMatchers("/open/**")
//                                // druid 监控
//                                .antMatchers("/druid/**")
//                                // resources 下的静态资源
//                                .antMatchers(
//                                        "/favicon.ico",
//                                        "/static/**"
//                                )
//                                // knife4j 增强 swagger 页面
//                                .antMatchers(
//                                        "/doc.html"
//                                        , "/webjars/**"
//                                        , "/swagger-resources/**"
//                                        , "/v2/api-docs/**"
//                                        , "/v3/api-docs/**"
//                                );
//                    })
//                    .cors(cors -> { // 跨域配置
//                        cors.disable();
////                        if (appConfig.isCorsEnabled()) {
////                            cors.configurationSource(appConfig.getCorsConfiguration());
////                        }
//                    })
//                    // csrf 令牌 <<<<<<
//                    .csrf(csrf -> {
//                        csrf.disable();
////                        if (appConfig.isDev()) {
////                            csrf.disable();
////                        } else {
////                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
////                        }
//                    })
//                    .headers(headers -> headers
//                            .addHeaderWriter((req, res) -> res.addHeader("traceId", MDC.get("traceId")))
//                            .xssProtection()
//                    )
//                    .anonymous().and()
//                    .servletApi().and()
//                    .headers().and()
//                    .authorizeRequests()
//                    .anyRequest().permitAll()
//            ;
//            final String anonymousKey = UUID.randomUUID().toString().replace("-", "");
//            http.addFilterBefore(
//                    // 自定义游客认证逻辑，如果用户已登录，访问开放接口时，使用会话中的用户。
//                    // 当使用 token 模式登录是，需要在 controller 主动调用 OpenUser.fillUserByToken() 方法换用户信息
//                    new AnonymousAuthenticationFilter(anonymousKey) {
//                        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//                            buildOpenUser((HttpServletRequest) req);
//                            chain.doFilter(req, res);
//                        }
//
//                        /**
//                         * 构造游客权限
//                         *
//                         * @param request {@link HttpServletRequest}
//                         */
//                        private void buildOpenUser(final HttpServletRequest request) {
//                            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//                            if (Objects.nonNull(authentication)) {
//                                if ((authentication.getPrincipal() instanceof IUser)) {
//                                    // 已登录用户，不需要处理
//                                    return;
//                                } else if ((authentication.getPrincipal() instanceof IOpenUser.OpenUser)) {
//                                    // 已生成游客对象
//                                    return;
//                                }
//                            }
//                            final String token = request.getHeader(Session.token.name());
//                            // 生成默认的游客对象， controller 有需求时使用 token 换用户信息，如果 token 为空则表示用户未登录
//                            // 如果 token 过期，填充用户时捕获异常，避免接口报错，因为开放接口允许任何用户访问
//                            final IOpenUser.OpenUser openUser = new IOpenUser.OpenUser(token, base64Token -> TokenCache.auth(base64Token).getUser());
//                            final AnonymousAuthenticationToken authenticationToken = new AnonymousAuthenticationToken(
//                                    anonymousKey,
//                                    openUser,
//                                    // 开放接口强制使用游客权限，避免开放接口注入权限，导致越权访问
//                                    AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS")
//                            );
////                            authenticationToken.setDetails(authenticationDetailsSource.buildDetails(request));
//                            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//                        }
//                    },
//                    AnonymousAuthenticationFilter.class
//            );
//        }
//    }
    @Order(1)
    @Configuration(proxyBeanMethods = false)
    public static class OpenConfiguration extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .requestMatchers(configurer -> {
                        configurer
                                .antMatchers("/")
                                // open 前缀的 url 不需要登录
                                .antMatchers("/open/**")
                                // druid 监控
                                .antMatchers("/druid/**")
                                // resources 下的静态资源
                                .antMatchers(
                                        "/favicon.ico",
                                        "/static/**"
                                )
                                .antMatchers(
                                        "/doc.html"
                                        , "/webjars/**"
                                        , "/swagger-resources/**"
                                        , "/v2/api-docs/**"
                                        , "/v3/api-docs/**"
                                );
                    })
                    .csrf().disable()
                    .anonymous().and()
                    .servletApi().and()
                    .headers().and()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                    .anyRequest().permitAll();
            final String anonymousKey = UUID.randomUUID().toString().replace("-", "");
            http.addFilterBefore(
                    // 自定义游客认证逻辑，如果用户已登录，访问开放接口时，使用会话中的用户。
                    // 当使用 token 模式登录是，需要在 controller 主动调用 OpenUser.fillUserByToken() 方法换用户信息
                    new AnonymousAuthenticationFilter(anonymousKey) {
                        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
                            buildOpenUser((HttpServletRequest) req);
                            chain.doFilter(req, res);
                        }

                        /**
                         * 构造游客权限
                         *
                         * @param request {@link HttpServletRequest}
                         */
                        private void buildOpenUser(final HttpServletRequest request) {
                            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                            if (Objects.nonNull(authentication)) {
                                if ((authentication.getPrincipal() instanceof IUser)) {
                                    // 已登录用户，不需要处理
                                    return;
                                } else if ((authentication.getPrincipal() instanceof IOpenUser.OpenUser)) {
                                    // 已生成游客对象
                                    return;
                                }
                            }
                            final IUser openUser = IUser.EMPTY;
                            final AnonymousAuthenticationToken authenticationToken = new AnonymousAuthenticationToken(
                                    anonymousKey,
                                    openUser,
                                    // 开放接口强制使用游客权限，避免开放接口注入权限，导致越权访问
                                    AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS")
                            );
//                            authenticationToken.setDetails(authenticationDetailsSource.buildDetails(request));
                            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                        }
                    },
                    AnonymousAuthenticationFilter.class
            );
        }
    }

    @Order(2)
    @Configuration(proxyBeanMethods = false)
    public class BasicConfiguration extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .authorizeRequests()
                    .anyRequest()
                    .authenticated()
                    .and()
                    .httpBasic();
        }

        @Bean
        @Override
        public UserDetailsService userDetailsService() {
            return s -> new User();
        }
    }

    public static class User implements IUser, UserDetails {

        @Override
        public String getId() {
            return "1";
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }

        @Override
        public String getPassword() {
            return PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("superadmin");
        }

        @Override
        public String getUsername() {
            return "admin";
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public String getNickname() {
            return "admin";
        }

        @Override
        public String getPhone() {
            return null;
        }
    }

}
