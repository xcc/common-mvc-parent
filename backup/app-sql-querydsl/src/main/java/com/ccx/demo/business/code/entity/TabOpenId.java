package com.ccx.demo.business.code.entity;
//

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.QueryTransient;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.entity.QTabOpenId;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import com.support.mvc.entity.ITable;
import com.support.mvc.entity.IWhere;
import com.support.mvc.entity.IWhere.QdslWhere;
import com.support.mvc.entity.base.Sorts;
import com.utils.enums.Code;
import com.utils.util.Then;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.querydsl.entity.QTabOpenId.tabOpenId;

/**
 * 实体类：测试 OpenId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Getter
@Setter
@ToString
@ApiModel(description = "测试 OpenId 模板表")

public class TabOpenId implements
        ITable, // 所有与数据库表 - 实体类映射的表都实现该接口；方便后续一键查看所有表的实体
        //
        // SQLUpdateClause => 需要的动态更新字段；采用 方案2 时需要实现该接口
        // QdslWhere       => 动态查询条件
        IWhere<SQLUpdateClause, QdslWhere> {
    private static final long serialVersionUID = 1L;

    /**
     * 数据ID
     */
    @Positive
    @ApiModelProperty(value = "数据ID", position = 1)
    private Long id;
    /**
     * fromId
     */
    @Min(0)
    @DecimalMax("9223372036854776000")
    @ApiModelProperty(value = "fromId", position = 2)
    private Long fromId;
    /**
     * toId
     */
    @Min(0)
    @DecimalMax("9223372036854776000")
    @ApiModelProperty(value = "toId", position = 3)
    private Long toId;

    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    @QueryTransient
    @ApiModelProperty(hidden = true)
    @JSONField(serialize = false, deserialize = false)
    private Set<Long> ids;
    /**
     * 排序字段
     */
    @QueryTransient
    @ApiModelProperty(value = "查询排序字段")
    private List<OrderBy> orderBy;
//{tabUserCacheInsert(table)}
//{tabUserCacheUpdate(table)}

    @SneakyThrows
    public TabOpenId cloneObject() {
        return (TabOpenId) super.clone();
    }

// Enum Start **********************************************************************************************************

    /**
     * 枚举：定义排序字段
     */
    public enum OrderBy {
        // 按 id 排序可替代按创建时间排序
        id(tabOpenId.id),
//        fromId(tabOpenId.fromId),
//        toId(tabOpenId.toId),
        ;
        private final Sorts asc;
        private final Sorts desc;

        public Sorts get(final Sorts.Direction direction) {
            return Objects.equals(direction, Sorts.Direction.ASC) ? asc : desc;
        }

        public Sorts.Order asc() {
            return Sorts.Order.asc(this.name());
        }

        public Sorts.Order desc() {
            return Sorts.Order.desc(this.name());
        }

        /**
         * 获取所有排序字段名
         *
         * @return String[]
         */
        public static String[] names() {
            return Stream.of(OrderBy.values()).map(Enum::name).toArray(String[]::new);
        }

        OrderBy(final ComparableExpressionBase<?> qdsl) {
            asc = Sorts.asc(qdsl, this);
            desc = Sorts.desc(qdsl, this);
        }
    }

// Enum End : DB Start *************************************************************************************************

    public Then<SQLInsertClause> insert(final SQLInsertClause sqlInsertClause) {
        final QTabOpenId q = tabOpenId;
        this.id = null;


        // 动态拼接 insert 语句
        return Then.of(sqlInsertClause)
                .then(id, insert -> insert.set(q.id, id))
                .then(fromId, insert -> insert.set(q.fromId, fromId))
                .then(toId, insert -> insert.set(q.toId, toId))
                ;
    }

    @Override
    public Then<SQLUpdateClause> update(final SQLUpdateClause SQLUpdateClause) {
        final QTabOpenId q = tabOpenId;
        // 动态拼接 update 语句
        // 以下案例中 只有 name 属性 为 null 时才不会加入 update 语句；
        return Then.of(SQLUpdateClause)
//                .then(fromId, update -> update.set(q.fromId, fromId))
//                .then(toId, update -> update.set(q.toId, toId))
////                // 当 name != null 时更新 name 属性
////                .then(name, update -> update.set(q.name, name))
////                .then(update -> update.set(q.updateUserId, updateUserId))
////                // 假设数据库中 content is not null；可以在属性为null时替换为 ""
////                .then(update -> update.set(q.content, Optional.ofNullable(content).orElse("")))
////                // 数据库中 amount 可以为 null
////                .then(update -> update.set(q.amount, amount))
                ;
    }

    @Override
    public QdslWhere where() {
        final QTabOpenId q = tabOpenId;
        // 构建查询顺序规则请参考：com.support.mvc.entity.IWhere#where
        return QdslWhere.of()
                .andIfNonEmpty(ids, () -> q.id.in(ids))
                .and(id, () -> q.id.eq(id))
//                .and(fromId, () -> q.fromId.eq(fromId))
//                .and(toId, () -> q.toId.eq(toId))
////                .and(phone, () -> q.phone.eq(phone))
////                .and(createUserId, () -> q.createUserId.eq(createUserId))
////                .and(updateUserId, () -> q.updateUserId.eq(updateUserId))
////                // 强制带默认值的查询字段
////                .and(q.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
////                // 数字区间查询
////                .and(amountRange, () -> q.amount.between(amountRange.getMin(), amountRange.getMax()))
////                // 日期区间查询；Range.rebuild() : 先将时间区间重置到 00:00:00.000 - 23:59:59.999 ; 大多数情况都需要重置时间
////                .and(createTimeRange, () -> q.createTime.between(createTimeRange.rebuild().getBegin(), createTimeRange.getEnd()))
////                // 模糊匹配查询：后面带 % ；建议优先使用
////                .and(name, () -> q.name.startsWith(name)) // 模糊匹配查询：后面带 %
////                .and(name, () -> q.name.endsWith(name)) // 模糊匹配查询：前面带 %
////                .and(name, () -> q.name.contains(name)) // 模糊匹配查询：前后带 %,同 MessageFormat.format("%{0}%", name)
////                .and(name, () -> q.name.like(MessageFormat.format("%{0}%", name))) 模糊匹配查询：前后带 %
                ;
    }



    @Override
    public List<Sorts> parseSorts() {
        try {
            return Objects.isNull(orderBy) ? null : orderBy.stream().map(by -> OrderBy.valueOf(by.getName()).get(by.getDirection())).collect(Collectors.toList());
        } catch (Exception e) {
            throw Code.A00012.toCodeException("排序字段可选范围：".concat(JSON.toJSONString(OrderBy.names())));
        }
    }

    /**
     * 获取查询实体与数据库表映射的所有字段,用于投影到 VO 类
     * 支持追加扩展字段,追加扩展字段一般用于连表查询
     *
     * @param appends {@link Expression}[] 追加扩展连表查询字段
     * @return {@link Expression}[]
     */
    public static Expression<?>[] allColumnAppends(final Expression<?>... appends) {
        final List<Expression<?>> columns = Lists.newArrayList(appends);
        columns.addAll(Arrays.asList(tabOpenId.all()));
        return columns.toArray(new Expression<?>[0]);
    }

// DB End **************************************************************************************************************

}
