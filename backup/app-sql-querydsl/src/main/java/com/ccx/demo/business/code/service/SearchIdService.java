package com.ccx.demo.business.code.service;

import com.ccx.demo.business.code.dao.sql.SearchIdRepository;
import com.ccx.demo.business.code.entity.TabSearchId;
import com.querydsl.core.QueryResults;
import com.support.mvc.entity.base.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;


/**
 * 服务接口实现类：测试 SearchId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
public class SearchIdService

{
    private final SearchIdRepository repository;


    /**
     * 测试 SearchId 模板表 按ID查询对象
     *
     * @param id {@link String} 数据ID
     * @return {@link Optional<TabSearchId>} 实体对象
     */
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public Optional<TabSearchId> findById(@NotBlank(message = "【id】不能为空") final String id) {
        return repository.findById(id)
                .map(TabSearchId::cloneObject); // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作

    }

    /**
     * 测试 SearchId 模板表 按条件分页查询列表
     *
     * @param condition {@link TabSearchId} 查询条件
     * @param page     {@link Page} 分页排序集合
     * @return {@link QueryResults<TabSearchId>} 分页对象
     */
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public @NotNull(message = "返回值不能为null") QueryResults<TabSearchId> page(
            @NotNull(message = "【condition】不能为null") final TabSearchId condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        return repository.page(condition, page);
    }

// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试 SearchId 模板表 按条件查询列表
//     *
//     * @param condition {@link TabSearchId} 查询条件
//     * @return {@link List<TabSearchId>} 结果集合
//     */
//    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
//    public @NotNull(message = "返回值不能为null") List<TabSearchId> list(
//            @NotNull(message = "【condition】不能为null") final TabSearchId condition) {
//        return repository.list(condition);
//    }

}
