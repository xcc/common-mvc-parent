package com.ccx.demo.business.code.controller;

import com.alibaba.fastjson.JSON;
import com.ccx.demo.business.code.entity.TabOpenId;
import com.ccx.demo.business.code.service.OpenIdService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.support.mvc.entity.IUser;
import com.support.mvc.entity.base.Page;
import com.support.mvc.entity.base.Result;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.web.bind.annotation.PathVariable;

import com.utils.util.Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;

/**
 * 对外接口：测试 OpenId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Api(tags = "测试 OpenId 模板表")
//@ApiSort() // 控制接口排序
@RequestMapping("/open/open-id")
@Controller
@Slf4j
@RequiredArgsConstructor
public class OpenOpenIdController {

    private final OpenIdService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询测试 OpenId 模板表", tags = {""})
    @ApiOperationSupport(
            order = 1,
            author = "谢长春",
            includeParameters = {"number", "limit", "orderBy", "fromId", "toId"},
            ignoreParameters = {"createTime", "updateTime"}
    )
    @ResponseBody
        public Result<TabOpenId> page(@ApiIgnore @AuthenticationPrincipal final IUser user,
                                  @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
                                  @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
                                  final TabOpenId condition) {
        return new Result<TabOpenId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.page(
                    Optional.ofNullable(condition).orElseGet(TabOpenId::new),
                    Page.builder().number(number).limit(limit).build()
            ));
        });
    }

// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询测试 OpenId 模板表", tags = {""})
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "fromId", "toId"},
//            ignoreParameters = {"createTime", "updateTime"}
//    )
//    @ResponseBody
//    //    public Result<TabOpenId> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabOpenId condition) {
//        return new Result<TabOpenId>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list(
//                Optional.ofNullable(condition).orElseGet(TabOpenId::new),
//            ));
//        });
//    }

    // 优先使用 find 方法，可以阻止平行越权。 只有在实体没有 uuid 的情况才能将该方法开放给前端
    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询测试 OpenId 模板表", tags = {""})
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
        public Result<TabOpenId> findById(@ApiIgnore @AuthenticationPrincipal final IUser user,
                                      @ApiParam(required = true, value = "数据id", example = "1") @PathVariable final Long id) {
        return new Result<TabOpenId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_save')")
    @ApiOperation(value = "新增测试 OpenId 模板表", tags = {""})
    @ApiImplicitParam(name = "body", dataType = "TabOpenId", dataTypeClass = TabOpenId.class, required = true)
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            ignoreParameters = {
                    "body.id", "body.uuid", "body.deleted", "body.createTime", "body.createUserId", "body.createUserName", "body.updateTime", "body.updateUserId", "body.updateUserName", "body.timestamp", "body.orderBy"
            })
    @ResponseBody
        public Result<TabOpenId> save(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestBody(required = false) final String body) {
        return new Result<TabOpenId>().execute(result -> { // Controller 方法逻辑写在这里
            List<TabOpenId> list = Lists.newArrayList();
            list.add(service.save(JSON.parseObject(body, TabOpenId.class)));

            List<TabOpenId> saveList = Lists.newArrayList();

            final TabOpenId save = new TabOpenId();
            save.setFromId((long) Util.randomMax(100000));
            save.setToId((long) Util.randomMax(100000));
            saveList.add(save.cloneObject());
            save.setFromId((long) Util.randomMax(100000));
            save.setToId((long) Util.randomMax(100000));
            saveList.add(save.cloneObject());
            save.setFromId((long) Util.randomMax(100000));
            save.setToId((long) Util.randomMax(100000));
            saveList.add(save.cloneObject());
            save.setFromId((long) Util.randomMax(100000));
            save.setToId((long) Util.randomMax(100000));
            saveList.add(save.cloneObject());

            list.addAll(service.saveAll(saveList));
            result.setSuccess(list);
        });
    }

    @PutMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
    @ApiOperation(value = "修改测试 OpenId 模板表", tags = {""})
    @ApiImplicitParam(name = "body", dataType = "TabOpenId", dataTypeClass = TabOpenId.class, required = true)
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            ignoreParameters = {
                    "body.id", "body.uuid", "body.deleted", "body.createTime", "body.createUserId", "body.createUserName", "body.updateTime", "body.updateUserId", "body.updateUserName", "body.timestamp", "body.orderBy"
            })
    @ResponseBody
        public Result<Void> update(@ApiIgnore @AuthenticationPrincipal final IUser user,
                               @ApiParam(required = true, value = "数据id", example = "1") @PathVariable final Long id, @RequestBody(required = false) final String body) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.update(id, JSON.parseObject(body, TabOpenId.class));
        });
    }

// 优先使用 delete 方法，可以阻止平行越权。 只有在实体没有 uuid 的情况才能将该方法开放给前端
//    @DeleteMapping("/{id}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除测试 OpenId 模板表", tags = {""})
//    @ApiOperationSupport(order = 6, author = "谢长春")
//    @ResponseBody
//    //    public Result<Void> deleteById(@ApiIgnore @AuthenticationPrincipal final IUser user, @ApiParam(required = true, value = "数据id", example = "1") @PathVariable final Long id) {
//        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
//            return service.deleteById(id);
//        });
//    }

}
