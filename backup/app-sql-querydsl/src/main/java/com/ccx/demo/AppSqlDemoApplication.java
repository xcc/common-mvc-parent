package com.ccx.demo;

import com.querydsl.config.QuerydslConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 应用入口
 *
 * @author 谢长春 2019/1/25
 */
@Slf4j
@SpringBootApplication
@EnableConfigurationProperties
@ImportAutoConfiguration({QuerydslConfiguration.class})
public class AppSqlDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSqlDemoApplication.class, args);
    }


    /**
     * @author 谢长春
     */
    @Controller
    public static class AppController {
//    public HomeController(ErrorAttributes errorAttributes) {
//        super(errorAttributes);
//    }

//    /**
//     * 处理服务端 500 异常
//     */
//    @RequestMapping(value = "/error", method = {GET, POST, PUT, PATCH, DELETE})
//    @ResponseBody
//    public Result<?> error() {
//        return Code.A00001.toResult("500：请求失败，不明确的异常");
//    }

        /**
         * swagger 文档
         *
         * @return {@link String}
         */
        @GetMapping(value = "/")
        public String doc() {
            return "redirect:/doc.html";
        }

        /**
         * 用于获取 csrf token
         *
         * @return {@link String}
         */
        @RequestMapping(value = "/", method = {HEAD, OPTIONS})
        @ResponseBody
        public String csrf() {
            return "ok";
        }

    }
}
