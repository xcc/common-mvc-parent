package com.ccx.demo.business.code.service;

import com.ccx.demo.business.code.dao.sql.AuthIdRepository;
import com.ccx.demo.business.code.entity.TabAuthId;
import com.querydsl.core.QueryResults;

import com.support.mvc.entity.base.Page;
import com.support.mvc.entity.validated.ISave;
import com.support.mvc.entity.validated.IUpdate;
import com.support.mvc.exception.DeleteRowsException;
import com.support.mvc.exception.UpdateRowsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.groups.Default;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * 服务接口实现类：测试 AuthId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Slf4j
@Service

@Validated
@RequiredArgsConstructor
public class AuthIdService

{
    private final AuthIdRepository repository;


    /**
     * 新增 测试 AuthId 模板表 ； 
     *
     * @param obj    {@link TabAuthId} 实体对象
     * @param userId {@link Long} 操作用户ID
     * @return {@link TabAuthId} 实体对象
     */
    @Validated({Default.class, ISave.class})
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") TabAuthId save(
            @Valid @NotNull(message = "【obj】不能为null") final TabAuthId obj
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        return repository.save(obj);
    }

    /**
     * 批量新增 测试 AuthId 模板表 ； 
     *
     * @param list   {@link List<TabAuthId>}  实体对象集合
     * @param userId {@link Long} 操作用户ID
     * @return List<TabAuthId> 实体对象集合
     */
    @Validated({Default.class, ISave.class})
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") List<TabAuthId> saveAll(
            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull TabAuthId> list
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        return repository.saveAll(list);
    }

    /**
     * 更新 测试 AuthId 模板表 ； 
     *
     * @param id     {@link String} 数据ID
     * @param userId {@link Long} 操作用户ID
     * @param obj    {@link TabAuthId} 实体对象
     */
    @Validated({Default.class, IUpdate.class})
    @Transactional(rollbackFor = Exception.class)
    public void update(
            @NotBlank(message = "【id】不能为空") final String id
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId
            , @Valid @NotNull(message = "【obj】不能为null") final TabAuthId obj) {
        UpdateRowsException.asserts(repository.update(id, userId, obj));
    }

// 注释掉的方法只有在需要的时候解开
//    /**
//     * 测试 AuthId 模板表 按ID删除，物理删除；执行物理删除前先查询到数据，删除成功之后返回该数据对象，通过 AOP 拦截记录到删除日志中
//     *
//     * @param id     {@link String} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     * @return {@link TabAuthId} 删除对象数据实体
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public TabAuthId deleteById(
//            @NotBlank(message = "【id】不能为空") final String id
//            ,@NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
//        return repository.deleteById(id, userId);
//    }

// 注释掉的方法只有在需要的时候解开

    /**
     * 测试 AuthId 模板表 按ID删除，逻辑删除
     *
     * @param id     {@link String} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotBlank(message = "【id】不能为空") final String id
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        DeleteRowsException.asserts(repository.markDeleteById(id, userId));
    }

// 注释掉的方法只有在需要的时候解开

    /**
     * 测试 AuthId 模板表 批量操作按ID删除，逻辑删除
     *
     * @param ids    {@link List<String>} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@NotNull String> ids
            , @NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, userId), ids.size());
        
    }

//    /**
//     * 测试 AuthId 模板表 批量操作按 id+uuid 删除，逻辑删除
//     *
//     * @param list   {@link List<MarkDelete>} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Validated({Default.class, IMarkDelete.class})
//    @Transactional(rollbackFor = Exception.class)
//    public void markDelete(
//            @NotEmpty(message = "【list】不能为null") final List<@Valid @NotNull MarkDelete> list
//            ,@NotNull(message = "【userId】不能为null") @Positive(message = "【userId】必须大于0") final Long userId) {
//        DeleteRowsException.asserts(repository.markDelete(list, userId), list.size());

//    }

    /**
     * 测试 AuthId 模板表 按ID查询对象
     *
     * @param id {@link String} 数据ID
     * @return {@link Optional<TabAuthId>} 实体对象
     */
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public Optional<TabAuthId> findById(@NotBlank(message = "【id】不能为空") final String id) {
        return repository.findById(id)
                .map(TabAuthId::cloneObject); // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作

    }

    /**
     * 测试 AuthId 模板表 按条件分页查询列表
     *
     * @param condition {@link TabAuthId} 查询条件
     * @param page     {@link Page} 分页排序集合
     * @return {@link QueryResults<TabAuthId>} 分页对象
     */
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public @NotNull(message = "返回值不能为null") QueryResults<TabAuthId> page(
            @NotNull(message = "【condition】不能为null") final TabAuthId condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        return repository.page(condition, page);
    }

// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试 AuthId 模板表 按条件查询列表
//     *
//     * @param condition {@link TabAuthId} 查询条件
//     * @return {@link List<TabAuthId>} 结果集合
//     */
//    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
//    public @NotNull(message = "返回值不能为null") List<TabAuthId> list(
//            @NotNull(message = "【condition】不能为null") final TabAuthId condition) {
//        return repository.list(condition);
//    }

}
