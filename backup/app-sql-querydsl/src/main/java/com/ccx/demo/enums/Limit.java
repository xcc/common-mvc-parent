package com.ccx.demo.enums;

/**
 * 枚举：列表查询返回行数
 *
 * @author 谢长春 2021-01-07
 */
public enum Limit {
    L1(1),
    L2(2),
    L3(3),
    L4(4),
    L5(5),
    L6(6),
    L7(7),
    L8(8),
    L9(9),
    L10(10),
    L100(100),
    L500(500),
    L1000(1000),
    L10000(10000),
    ;
    /**
     * 枚举属性说明
     */
    public final int value;

    Limit(final int value) {
        this.value = value;
    }
}
