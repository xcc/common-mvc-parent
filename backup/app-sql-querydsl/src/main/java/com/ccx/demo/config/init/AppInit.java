package com.ccx.demo.config.init;

import com.querydsl.sql.SQLQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

/**
 * 初始化单例类、实体类、接口需要的bean，因为单例类无法直接注入bean
 *
 * @author 谢长春
 */
@Component
@Slf4j
@RequiredArgsConstructor
@Order(-1) // 这里优先级一定要最高，否则其他的初始化使用到路径会报错
public class AppInit {
    private static ApplicationContext APP_CONTEXT;
//    /**
//     * 单线程服务
//     */
//    private static ExecutorService SINGLE_THREAD;
//    /**
//     * 多线程服务
//     */
//    private static ExecutorService MULTI_THREAD;
//    /**
//     * 缓存管理器
//     */
//    private static CacheManager CACHE_MANAGER;
//    /**
//     * QueryDSL 数据操作，通过此枚举获取到 jpa 查询对象，可以在接口中声明 default 方法后做更新删除查询操作
//     */
//    private static JPAQueryFactory JPA_QUERY_FACTORY;
//        sqlQueryFactory("QueryDSL 数据操作，通过此枚举获取到 sql 查询对象，可以在接口中声明 default 方法后做更新删除查询操作", SQLQueryFactory.class),
//        mongoTemplate("Mongodb 数据操作，通过此枚举获取到 mongo 查询对象，可以在接口中声明 default 方法后做更新删除查询操作", MongoTemplate.class),
    /**
     * 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
     */
    private static final Map<String, Object> BEAN_MAP = new ConcurrentHashMap<>();

    @Bean
    public CommandLineRunner beanInitContextCommandLineRunner(ApplicationContext context) {
        APP_CONTEXT = context;
        return args -> {
        };
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Bean
    public CommandLineRunner printBeanCommandLineRunner(ApplicationContext context) {
        return args -> {
            System.out.println("打印所有bean:");
            Stream.of(context.getBeanDefinitionNames()).sorted().forEach(System.out::println);
        };
    }

    /**
     * 获取 Spring Context 对象
     *
     * @return ApplicationContext
     */
    public static ApplicationContext getAppContext() {
        return APP_CONTEXT;
    }

    public static ExecutorService getSingleThread() {
        return getBean("singleThreadExecutorService", ExecutorService.class);
    }

    public static ExecutorService getMultiThread() {
        return getBean("multiThreadExecutorService", ExecutorService.class);
    }

    public static SQLQueryFactory getQueryFactory() {
        return getBean(SQLQueryFactory.class);
    }

    /**
     * 获取 bean， 并缓存到 map 集合，  避免每次通过反射获取
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getBean(Class<T> clazz) {
        if (!BEAN_MAP.containsKey(clazz.getName())) {
            // 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
            BEAN_MAP.put(clazz.getName(), APP_CONTEXT.getBean(clazz));
        }
        return (T) BEAN_MAP.get(clazz.getName());
    }

    /**
     * 获取 bean， 并缓存到 map 集合，  避免每次通过反射获取
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getBean(String beanName, Class<T> clazz) {
        if (!BEAN_MAP.containsKey(beanName)) {
            // 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
            BEAN_MAP.put(beanName, APP_CONTEXT.getBean(beanName, clazz));
        }
        return (T) BEAN_MAP.get(beanName);
    }
}
