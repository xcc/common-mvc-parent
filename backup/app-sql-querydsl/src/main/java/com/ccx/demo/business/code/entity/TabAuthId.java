package com.ccx.demo.business.code.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.QueryTransient;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.entity.QTabAuthId;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import com.support.mvc.entity.ITable;
import com.support.mvc.entity.IWhere;
import com.support.mvc.entity.IWhere.QdslWhere;
import com.support.mvc.entity.base.Sorts;
import com.support.mvc.entity.validated.ISave;
import com.support.mvc.entity.validated.IUpdate;

import com.utils.enums.Code;
import com.utils.util.Then;
import com.utils.util.Util;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.querydsl.entity.QTabAuthId.tabAuthId;

/**
 * 实体类：测试 AuthId 模板
 *
 * @author 谢长春 on 2021-03-20 V20210530
 */
@Getter
@Setter
@ToString
@ApiModel(description = "测试 AuthId 模板表")

public class TabAuthId implements
        ITable, // 所有与数据库表 - 实体类映射的表都实现该接口；方便后续一键查看所有表的实体
        // SQLUpdateClause => 需要的动态更新字段；采用 方案2 时需要实现该接口
        // QdslWhere       => 动态查询条件
        IWhere<SQLUpdateClause, QdslWhere> {
    private static final long serialVersionUID = 1L;

    /**
     * 数据ID，主键
     */
    @Size(max = 18)
    @ApiModelProperty(value = "数据id", position = 1)
    private String id;
    /**
     * 名称
     */
    @Size(max = 50)
    @ApiModelProperty(value = "名称", position = 2)
    private String name;
    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "数据新增时间", example = "2020-02-02 02:02:02", position = 3)
    private Timestamp createTime;
    /**
     * 创建用户ID
     */
    @PositiveOrZero
    @ApiModelProperty(value = "新增操作人id", position = 4)
    private Long createUserId;
    /**
     * 修改时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss.SSS")
    @ApiModelProperty(value = "数据最后一次更新时间", example = "2020-02-02 02:02:02.002", position = 5)
    private Timestamp updateTime;
    /**
     * 修改用户ID
     */
    @PositiveOrZero
    @ApiModelProperty(value = "更新操作人id", position = 6)
    private Long updateUserId;
    /**
     * 是否逻辑删除。 [0:否,1:是]
     */
    @ApiModelProperty(value = "是否逻辑删除", position = 7)
    private Byte deleted;

    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    @QueryTransient
    @ApiModelProperty(hidden = true)
    @JSONField(serialize = false, deserialize = false)
    private Set<String> ids;
    /**
     * 排序字段
     */
    @QueryTransient
    @ApiModelProperty(value = "查询排序字段")
    private List<OrderBy> orderBy;
//{tabUserCacheInsert(table)}
//{tabUserCacheUpdate(table)}

    @SneakyThrows
    public TabAuthId cloneObject() {
        return (TabAuthId) super.clone();
    }

// Enum Start **********************************************************************************************************

    /**
     * 枚举：定义排序字段
     */
    public enum OrderBy {
        // 按 id 排序可替代按创建时间排序
        id(tabAuthId.id),
//        name(tabAuthId.name),
//        createTime(tabAuthId.createTime),
//        createUserId(tabAuthId.createUserId),
//        updateTime(tabAuthId.updateTime),
//        updateUserId(tabAuthId.updateUserId),
//        deleted(tabAuthId.deleted),
        ;
        private final Sorts asc;
        private final Sorts desc;

        public Sorts get(final Sorts.Direction direction) {
            return Objects.equals(direction, Sorts.Direction.ASC) ? asc : desc;
        }

        public Sorts.Order asc() {
            return Sorts.Order.asc(this.name());
        }

        public Sorts.Order desc() {
            return Sorts.Order.desc(this.name());
        }

        /**
         * 获取所有排序字段名
         *
         * @return String[]
         */
        public static String[] names() {
            return Stream.of(OrderBy.values()).map(Enum::name).toArray(String[]::new);
        }

        OrderBy(final ComparableExpressionBase<?> qdsl) {
            asc = Sorts.asc(qdsl, this);
            desc = Sorts.desc(qdsl, this);
        }
    }

// Enum End : DB Start *************************************************************************************************

    public Then<SQLInsertClause> insert(final SQLInsertClause sqlInsertClause) {
        final QTabAuthId q = tabAuthId;
        this.id = Util.uuid32();
        this.createTime = null;
        this.updateUserId = createUserId;
        this.updateTime = null;
        this.deleted = 0;
        // 动态拼接 insert 语句
        return Then.of(sqlInsertClause)
                .then(id, insert -> insert.set(q.id, id))
                .then(name, insert -> insert.set(q.name, name))
                .then(createTime, insert -> insert.set(q.createTime, createTime))
                .then(createUserId, insert -> insert.set(q.createUserId, createUserId))
                .then(updateTime, insert -> insert.set(q.updateTime, updateTime))
                .then(updateUserId, insert -> insert.set(q.updateUserId, updateUserId))
                .then(deleted, insert -> insert.set(q.deleted, deleted))
                ;
    }

    @Override
    public Then<SQLUpdateClause> update(final SQLUpdateClause SQLUpdateClause) {
        final QTabAuthId q = tabAuthId;
        // 动态拼接 update 语句
        // 以下案例中 只有 name 属性 为 null 时才不会加入 update 语句；
        return Then.of(SQLUpdateClause)
                .then(name, update -> update.set(q.name, name))
                .then(updateUserId, update -> update.set(q.updateUserId, updateUserId))
////                // 当 name != null 时更新 name 属性
////                .then(name, update -> update.set(q.name, name))
////                .then(update -> update.set(q.updateUserId, updateUserId))
////                // 假设数据库中 content is not null；可以在属性为null时替换为 ""
////                .then(update -> update.set(q.content, Optional.ofNullable(content).orElse("")))
////                // 数据库中 amount 可以为 null
////                .then(update -> update.set(q.amount, amount))
                ;
    }

    @Override
    public QdslWhere where() {
        final QTabAuthId q = tabAuthId;
        // 构建查询顺序规则请参考：com.support.mvc.entity.IWhere#where
        return QdslWhere.of()
                .andIfNonEmpty(ids, () -> q.id.in(ids))
                .and(id, () -> q.id.eq(id))
//                .and(name, () -> name.endsWith("%") || name.startsWith("%") ? q.name.like(name) : q.name.eq(name))
//                .and(createTimeRange, () -> q.createTime.between(createTimeRange.rebuild().getBegin(), createTimeRange.getEnd()))
//                .and(createUserId, () -> q.createUserId.eq(createUserId))
//                .and(updateTimeRange, () -> q.updateTime.between(updateTimeRange.rebuild().getBegin(), updateTimeRange.getEnd()))
//                .and(updateUserId, () -> q.updateUserId.eq(updateUserId))
                .and(q.deleted.eq(Objects.isNull(deleted) ? 0 : deleted))
////                .and(phone, () -> q.phone.eq(phone))
////                .and(createUserId, () -> q.createUserId.eq(createUserId))
////                .and(updateUserId, () -> q.updateUserId.eq(updateUserId))
////                // 强制带默认值的查询字段
////                .and(q.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
////                // 数字区间查询
////                .and(amountRange, () -> q.amount.between(amountRange.getMin(), amountRange.getMax()))
////                // 日期区间查询；Range.rebuild() : 先将时间区间重置到 00:00:00.000 - 23:59:59.999 ; 大多数情况都需要重置时间
////                .and(createTimeRange, () -> q.createTime.between(createTimeRange.rebuild().getBegin(), createTimeRange.getEnd()))
////                // 模糊匹配查询：后面带 % ；建议优先使用
////                .and(name, () -> q.name.startsWith(name)) // 模糊匹配查询：后面带 %
////                .and(name, () -> q.name.endsWith(name)) // 模糊匹配查询：前面带 %
////                .and(name, () -> q.name.contains(name)) // 模糊匹配查询：前后带 %,同 MessageFormat.format("%{0}%", name)
////                .and(name, () -> q.name.like(MessageFormat.format("%{0}%", name))) 模糊匹配查询：前后带 %
                ;
    }



    @Override
    public List<Sorts> parseSorts() {
        try {
            return Objects.isNull(orderBy) ? null : orderBy.stream().map(by -> OrderBy.valueOf(by.getName()).get(by.getDirection())).collect(Collectors.toList());
        } catch (Exception e) {
            throw Code.A00012.toCodeException("排序字段可选范围：".concat(JSON.toJSONString(OrderBy.names())));
        }
    }

    /**
     * 获取查询实体与数据库表映射的所有字段,用于投影到 VO 类
     * 支持追加扩展字段,追加扩展字段一般用于连表查询
     *
     * @param appends {@link Expression}[] 追加扩展连表查询字段
     * @return {@link Expression}[]
     */
    public static Expression<?>[] allColumnAppends(final Expression<?>... appends) {
        final List<Expression<?>> columns = Lists.newArrayList(appends);
        columns.addAll(Arrays.asList(tabAuthId.all()));
        return columns.toArray(new Expression<?>[0]);
    }

// DB End **************************************************************************************************************

}
