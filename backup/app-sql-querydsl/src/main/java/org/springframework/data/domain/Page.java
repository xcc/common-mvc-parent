package org.springframework.data.domain;

import java.util.function.Function;

/**
 * A page is a sublist of a list of objects. It allows gain information about the position of it in the containing
 * entire list.
 *
 * @param <T>
 * @author Oliver Gierke
 */
public interface Page<T> {

    /**
     * Creates a new empty {@link Page}.
     *
     * @return
     * @since 2.0
     */
    static <T> Page<T> empty() {
        return empty(Pageable.unpaged());
    }

    /**
     * Creates a new empty {@link Page} for the given {@link Pageable}.
     *
     * @param pageable must not be {@literal null}.
     * @return
     * @since 2.0
     */
    static <T> Page<T> empty(Pageable pageable) {
        return null;
    }

    /**
     * Returns the number of total pages.
     *
     * @return the number of total pages
     */
    int getTotalPages();

    /**
     * Returns the total amount of elements.
     *
     * @return the total amount of elements
     */
    long getTotalElements();

    /**
     * Returns a new {@link Page} with the content of the current one mapped by the given {@link Function}.
     *
     * @param converter must not be {@literal null}.
     * @return a new {@link Page} with the content of the current one mapped by the given {@link Function}.
     * @since 1.10
     */
    <U> Page<U> map(Function<? super T, ? extends U> converter);
}
