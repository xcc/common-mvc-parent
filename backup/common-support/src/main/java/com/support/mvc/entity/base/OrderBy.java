package com.support.mvc.entity.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

/**
 * 通用排序参数接收对象
 *
 * @author 谢长春 2022-02-07
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel(description = "通用排序参数接收对象")
public class OrderBy implements Serializable {
    private static final long serialVersionUID = 8760879633278119365L;
    /**
     * 排序字段名
     */
    @ApiModelProperty(required = true, value = "排序字段名，可以通过实体类的 OrderByColumn 枚举查看所有支持排序的字段", example = "id")
    private String name;
    /**
     * 排序方向
     */
    @ApiModelProperty(position = 1, value = "排序方向，org.springframework.data.domain.Sort.Direction", example = "ASC")
    private Sort.Direction direction = Sort.Direction.ASC;

}
