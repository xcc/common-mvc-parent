package com.support.mvc.encrypt.convert;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.support.mvc.encrypt.AesApiId;
import com.utils.enums.Code;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

/**
 * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesApiIdLongArrayJsonConvert.Serializer.class, deserializeUsing = AesApiIdLongArrayJsonConvert.Deserializer.class)
 *
 * @author 谢长春 2022-02-06
 */
@Slf4j
public class AesApiIdLongArrayJsonConvert {
//    public static class Serializer implements ObjectSerializer {
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            if (log.isDebugEnabled()) {
//                log.debug("加密:{}:{}", fieldName, value);
//            }
//            jsonSerializer.write(AesApi.encrypt(Stream.of(value).map(Objects::toString).collect(Collectors.toList())));
//        }
//    }
//
//    public static class Deserializer implements ObjectDeserializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            if (log.isDebugEnabled()) {
//                log.debug("解密:{}:{}", fieldName, list);
//            }
//            try {
//                return (T) AesApi.decrypt(list).stream().map(Long::parseLong).toArray(Long[]::new);
//            } catch (Exception e) {
//                throw Code.A00003.toCodeException(String.join(",", list), e);
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return JSONToken.LBRACKET;
//        }
//    }

    /**
     * id 反序列化， 去掉携带的 updateTime
     */
    public static class AesIdDeserializer implements ObjectDeserializer {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
            final List<String> list = defaultJSONParser.parseArray(String.class);
            if (Objects.isNull(list)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}:{}", fieldName, list);
            }
            try {
                return (T) AesApiId.decrypt(list)
                        .map(AesApiId.AesId::getLongId)
                        .toArray(Long[]::new);
            } catch (Exception e) {
                throw Code.A00003.toCodeException(String.join(",", list), e);
            }
        }

        @Override
        public int getFastMatchToken() {
            return JSONToken.LBRACKET;
        }
    }

}
