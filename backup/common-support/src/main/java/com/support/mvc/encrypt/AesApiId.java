package com.support.mvc.encrypt;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.utils.aes.Aes;
import com.utils.aes.IvRandomRule;
import com.utils.enums.Code;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.collections4.CollectionUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * api 数据包加解密
 *
 * @author 谢长春 2022-01-30
 */
@Slf4j
public class AesApiId {
    /**
     * https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#trans
     */
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 参考文档: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
     * <pre>
     * Cipher (Encryption) Algorithms
     * Cipher Algorithm Names
     * The following names can be specified as the algorithm component in a transformation when requesting an instance of Cipher.
     *
     * <table border="5" cellpadding="5" frame="border" width="90%" summary="Cipher Algorithm Names">
     * <thead> <tr> <th>Algorithm Name</th> <th>Description</th> </tr> </thead>
     * <tbody>
     * <tr> <td>AES</td> <td>Advanced Encryption Standard as specified by NIST in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS 197</a>. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits.</td> </tr>
     * <tr> <td>AESWrap</td> <td>The AES key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3394.txt">RFC 3394</a>.</td> </tr>
     * <tr> <td>ARCFOUR</td> <td>A stream cipher believed to be fully interoperable with the RC4 cipher developed by Ron Rivest. For more information, see K. Kaukonen and R. Thayer, "A Stream Cipher Encryption Algorithm 'Arcfour'", Internet Draft (expired), <a href="http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt"> draft-kaukonen-cipher-arcfour-03.txt</a>.</td> </tr>
     * <tr> <td>Blowfish</td> <td>The <a href="http://www.schneier.com/blowfish.html">Blowfish block cipher</a> designed by Bruce Schneier.</td> </tr>
     * <tr> <td>CCM</td> <td>Counter/CBC Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38C/SP800-38C_updated-July20_2007.pdf">NIST Special Publication SP 800-38C</a>.</td> </tr>
     * <tr> <td>DES</td> <td>The Digital Encryption Standard as described in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS PUB 46-3</a>.</td> </tr>
     * <tr> <td>DESede</td> <td>Triple DES Encryption (also known as DES-EDE, 3DES, or Triple-DES). Data is encrypted using the DES algorithm three separate times. It is first encrypted using the first subkey, then decrypted with the second subkey, and encrypted with the third subkey.</td> </tr>
     * <tr> <td>DESedeWrap</td> <td>The DESede key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3217.txt">RFC 3217</a> .</td> </tr>
     * <tr> <td>ECIES</td> <td>Elliptic Curve Integrated Encryption Scheme</td> </tr>
     * <tr> <td>GCM</td> <td>Galois/Counter Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38D/SP-800-38D.pdf">NIST Special Publication SP 800-38D</a>.</td> </tr>
     * <tr> <td>PBEWith&lt;digest&gt;And&lt;encryption&gt; PBEWith&lt;prf&gt;And&lt;encryption&gt;</td> <td>The password-based encryption algorithm found in (PKCS5), using the specified message digest (&lt;digest&gt;) or pseudo-random function (&lt;prf&gt;) and encryption algorithm (&lt;encryption&gt;). Examples: <ul> <li><b>PBEWithMD5AndDES</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, Nov 1993</a>. Note that this algorithm implies <a href="#cbcMode"><i>CBC</i></a> as the cipher mode and <a href="#pkcs5Pad"><i>PKCS5Padding</i></a> as the padding scheme and cannot be used with any other cipher modes or padding schemes.</li> <li><b>PBEWithHmacSHA256AndAES_128</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Cryptography Standard," version 2.0, March 1999</a>.</li> </ul> </td> </tr>
     * <tr> <td>RC2</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RC4</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc. (See note prior for ARCFOUR.)</td> </tr>
     * <tr> <td>RC5</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RSA</td> <td>The RSA encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2125">PKCS #1</a></td> </tr>
     * </tbody>
     * </table>
     */
    private static final String CIPHER_ALGORITHM = "AES";
    /**
     * 随机生成 IV 长度， 只能固定 16 位
     */
    private static final int IV_LENGTH = 16;
    private static final int ZERO = 0;
    /**
     * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    public static IvParameterSpec IV_PARAMETER;
    /**
     * 16 位 IV 规则， 默认全部随机，无规律
     */
    public static IvRandomRule IV_RANDOM_RULE = IvRandomRule.alphanumeric;
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    public static SecretKeySpec SECRET_KEY;
    /**
     * 加解密开关。 true：是，false：否。
     */
    public static boolean ENABLED;

    private AesApiId() {

    }

    private static final Function<byte[], String> hexEncode = (byte[] bytes) -> Hex.encodeHexString(bytes);
    private static final Function<String, byte[]> hexDecode = (String hexString) -> {
        try {
            return Hex.decodeHex(hexString);
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    };
    private static final Function<byte[], String> base64Encode = (byte[] bytes) -> Base64.encodeBase64String(bytes);
    private static final Function<String, byte[]> base64Decode = (String base64String) -> Base64.decodeBase64(base64String);

    /**
     * 设置是否开启加解密开关。 true：是，false：否。
     *
     * @param enabled boolean true：是，false：否。
     */
    public static void setEnabled(final boolean enabled) {
        ENABLED = enabled;
    }

    /**
     * 设置密钥
     *
     * @param secretKey {@link String} RandomStringUtils.randomAlphanumeric(32)
     */
    public static void setSecretKey(final String secretKey) {
        if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
            throw new IllegalArgumentException("secretKey 长度必须是 32 位");
        }
        SECRET_KEY = new SecretKeySpec(secretKey.getBytes(UTF_8), CIPHER_ALGORITHM);
    }

    /**
     * <pre>
     * 设置动态 IV 生成规则
     *   IvRandomRule.alphanumeric     : RandomStringUtils.randomAlphanumeric 工具类生成 16 位随机 【A-Za-z0-9】 的字符串，解密时不校验 IV 生成规则
     *   IvRandomRule.dateAlphanumeric : 10 位日期(yyyyMMddHH) + 6 位随机 【A-Za-z0-9】 的字符串，解密时提取前 10 位日期，检查有效期为 24 小时
     *   IvRandomRule.timestamp        : 13位时间戳，前面补0，解密检查时间戳，有效期为 24 小时
     * </pre>
     *
     * @param rule {@link IvRandomRule}
     */
    public static void setIvRandomRule(final IvRandomRule rule) {
        IV_RANDOM_RULE = rule;
    }

    /**
     * 设置 IV ， 长度必须是 16 位自负
     *
     * @param iv {@link String} RandomStringUtils.randomAlphanumeric(16)
     */
    @SneakyThrows
    public static void setIV(final String iv) {
        if (Strings.isNullOrEmpty(iv) || iv.length() != 16) {
            throw new IllegalArgumentException("secretKey 长度必须是 16 位");
        }
        IV_PARAMETER = new IvParameterSpec(iv.getBytes(UTF_8));
    }

    /**
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param data {@link String} 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encrypt(final String data) {
        try {
            if (Strings.isNullOrEmpty(data)) {
                return null;
            }
            if (!ENABLED) {
                return data;
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 IV 使用动态 IV 加密
                // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
                final byte[] ivBytes = IV_RANDOM_RULE.get();
                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                // 初始化加密参数，设置为加密模式，指定密钥，设置IV
                cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
                final byte[] dataBytes = cipher.doFinal(data.getBytes(UTF_8));
                final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
                System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
                System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
                final String result = hexEncode.apply(bytes);
//            final String result = base64Encode(bytes);
                if (log.isDebugEnabled()) {
                    log.debug("{} => {}", data, result);
                }
                return result;
            }
            // 使用固定 IV 加密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            final String result = hexEncode.apply(cipher.doFinal(data.getBytes(UTF_8)));
//        final String result = base64Encode(cipher.doFinal(data.getBytes(UTF_8)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", data, result);
            }
            return result;
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%s", data);
        }
    }

    /**
     * 拼接 id 和 updateTime 一起加密，用于更新和删除时附加 updateTime 作为查询条件
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param id         {@link Long} id
     * @param updateTime {@link String}  更新时间
     * @return {@link String} 密文
     */
    public static String encrypt(final Long id, final String updateTime) {
        try {
            return Optional.ofNullable(id)
                    // 拼接 id 和 updateTime 一起加密，用于更新和删除时附加 updateTime 作为查询条件
                    .map(val -> String.format("%d@%s", val, Optional.ofNullable(updateTime).orElse("")))
                    .map(AesApiId::encrypt)
                    .orElse(null);
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%d@%s", id, updateTime);
        }
    }

    /**
     * 拼接 id 和 updateTime 一起加密，用于更新和删除时附加 updateTime 作为查询条件
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param id         {@link Long} id
     * @param updateTime {@link String}  更新时间
     * @return {@link String} 密文
     */
    public static String encrypt(final String id, final String updateTime) {
        try {
            return Optional.ofNullable(id)
                    // 拼接 id 和 updateTime 一起加密，用于更新和删除时附加 updateTime 作为查询条件
                    .map(val -> String.format("%s@%s", val, Optional.ofNullable(updateTime).orElse("")))
                    .map(AesApiId::encrypt)
                    .orElse(null);
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%d@%s", id, updateTime);
        }
    }

    /**
     * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
     *
     * @param encryptId {@link String} 密文
     * @return {@link AesId}
     */
    @SneakyThrows
    public static AesId decrypt(final String encryptId) {
        try {
            Code.A00002.assertNonBlank(encryptId, "【encryptId】不能为空");
            if (!ENABLED) {
                return AesId.parse(encryptId);
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
                // 将 data 分割成 IV 和密文
                final byte[] bytes = hexDecode.apply(Objects.requireNonNull(encryptId));
//            final byte[] bytes = base64Decode.apply(data);
                final byte[] ivBytes = new byte[IV_LENGTH];
                final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);

                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
                final String result = new String(cipher.doFinal(dataBytes));
                if (log.isDebugEnabled()) {
                    log.debug("{}({}) => {}", encryptId, new String(ivBytes), result);
                }
                return AesId.parse(result);
            }
            // 使用固定的 IV 解密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            final String result = new String(cipher.doFinal(hexDecode.apply(encryptId)));
//        final String result = new String(cipher.doFinal(base64Decode.apply(data)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", encryptId, result);
            }
            return AesId.parse(result);
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", encryptId);
        }
    }

    /**
     * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
     *
     * @param list {@link Collection<String>} 密文
     * @return 明文
     */
    @SneakyThrows
    public static Stream<AesId> decrypt(final Collection<String> list) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return Stream.empty();
            }
            if (!ENABLED) {
                return list.stream().map(AesId::parse);
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                final byte[] ivBytes = new byte[IV_LENGTH];
                return list.stream()
                        .map(data -> {
                            try {
                                // 将 data 分割成 IV 和密文
                                final byte[] bytes = hexDecode.apply(data);
                                // final byte[] bytes = base64Decode.apply(data);
                                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
                                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                                cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));

                                final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
                                System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);
                                final String result = new String(cipher.doFinal(dataBytes));
                                if (log.isDebugEnabled()) {
                                    log.debug("{} => {}", data, result);
                                }
                                return AesId.parse(result);
                            } catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
                                throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                            }
                        })
                        ;

            }
            // 使用固定的 IV 解密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            return list.stream()
                    .map(data -> {
                        try {
                            final String result = new String(cipher.doFinal(hexDecode.apply(data)));
                            // final String result = new String(cipher.doFinal(base64Decode.apply(data)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return AesId.parse(result);
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                        }
                    })
                    ;
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", String.join(",", list));
        }
    }

    /**
     * encryptId 携带 updateTime 加密，用于更新和删除时附加 updateTime 作为查询条件
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    public static class AesId {
        /**
         * 数值型 id
         */
        private String id;
        /**
         * 数据最近一次更新时间
         */
        private String updateTime;

        public Long getLongId() {
            return Long.parseLong(id);
        }

        public String getStringId() {
            return id;
        }

        public static AesId parse(final String decryptText) {
            Code.A00003.assertNonBlank(decryptText, "解密后的内容不能为空");
            Code.A00003.assertHasTrue(decryptText.contains("@"), "解密后的数据格式错误:%s", decryptText);
            // split[0]:id，split[1]:updateTime
            final String[] split = decryptText.split("@");
            return new AesId(split[0], split.length > 1 ? split[1] : null);
        }

    }

    public static void main(String[] args) {
        AesApiId.setSecretKey("VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF");
        AesApiId.setIvRandomRule(IvRandomRule.dateAlphanumeric);
        AesApiId.setEnabled(true);
        System.out.println(AesApiId.decrypt("323032323032303432326237434c366efafa9c8891389a17b857ab5ba812a5d8f2a6c2218993ace1c00e7897d8e1a5a550483b047aab4eed812d578ced84dee2"));
        System.out.println(AesApiId.decrypt("323032323032303432326237434c366efafa9c8891389a17b857ab5ba812a5d8f2a6c2218993ace1c00e7897d8e1a5a550483b047aab4eed812d578ced84dee2"));
    }

}
