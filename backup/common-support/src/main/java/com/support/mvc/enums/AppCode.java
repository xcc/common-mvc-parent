package com.support.mvc.enums;

import com.support.mvc.entity.base.Item;
import com.support.mvc.entity.base.Result;
import com.utils.ICode;
import com.utils.util.Dates;
import com.utils.util.FWrite;
import lombok.val;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 定义{@link Result#setCode(ICode)}返回编码
 *
 * @author 谢长春 2019-1-9
 */
public enum AppCode implements ICode {
    A01000("用户名密码错误"),
    A01001("账户已禁用"),
    A01002("账户已锁定"),
    A01003("账户已过期"),
    A01004("证书已过期"),
    A01005("密码错误"),
    A01006("邮箱已存在"),
    A01007("手机号已被占用"),
    A01008("登录名已存在"),
    A01009("图片验证码输入错误"),
    A01010("短信验证码输入错误"),
    A01011("文件格式不支持"),
    A01012("无下载权限"),
    A01013("链接已失效"),
    A01014("邮件发送失败，请检查邮箱是否正确"),
    A01015("原密码输入错误"),

    ;
    /**
     * 枚举属性说明
     */
    public final String comment;

    AppCode(String comment) {
        this.comment = comment;
    }

    /**
     * 转换为 {@link Item} 对象
     *
     * @return {@link Item}
     */
    public Item getObject() {
        return Item.builder()
                .key(this.name())
                .value(this.ordinal())
                .comment(this.comment)
                .build();
    }

    @Override
    public String getComment() {
        return this.comment;
    }

//    /**
//     * 通过 AppCode 构建 Result 对象；注：只能构建 Result<Object>，若要指定泛型，请使用 new Result<?> 指定泛型
//     *
//     * @return Result<Object>
//     */
//    public <E> Result<E> toResult() {
//        return new Result<>(this);
//    }
//    /**
//     * 通过 AppCode 构建 Result 对象；注：只能构建 Result<Object>，若要指定泛型，请使用 new Result<?> 指定泛型
//     *
//     * @param exception String 异常消息，可选参数，
//     * @return Result<Object>
//     */
//    public <E> Result<E> toResult(final String exception) {
//        return new Result<E>(this).setException(exception);
//    }

    public static void main(String[] args) {
        { // 构建 js 枚举文件
            val name = "枚举：响应状态码";
            StringBuilder sb = new StringBuilder();
            sb.append("/**\n")
                    .append(" * ").append(name).append("\n")
                    .append(String.format(" * Created by 谢长春 on %s.%n", Dates.now().formatDate()))
                    .append(" */\n");
            sb.append("// 枚举值定义").append("\n");
            sb.append("const status = Object.freeze({").append("\n");
            Stream.of(AppCode.values()).forEach(item -> sb.append(
                            "\t{name}: {value: '{name}', comment: '{comment}'},"
                                    .replace("{name}", item.name())
                                    .replace("{comment}", item.comment)
                    ).append("\n")
            );
            sb.append("\tgetComment:function(key){return (this[key]||{}).comment}\n});").append("\n");
            sb.append("// 枚举值转换为选项集合").append("\n");
            sb.append("const options = [").append("\n");
            Stream.of(AppCode.values()).forEach(item -> sb.append(
                            "\t{value: status.{name}.value, label: status.{name}.comment},"
                                    .replace("{name}", item.name())
                    ).append("\n")
            );
            sb.append("];").append("\n");
            sb.append("export default status;");
            System.out.println("JS文件输出路径：\n" +
                    FWrite.of("logs", AppCode.class.getSimpleName().concat(".js"))
                            .write(sb.toString())
                            .getAbsolute().orElse(null)
            );
        }

        System.out.println(
                Stream.of(AppCode.values())
                        .map(item -> String.format("%s【%s】", item.name(), item.comment))
                        .collect(Collectors.joining("|"))
        );
    }

}
