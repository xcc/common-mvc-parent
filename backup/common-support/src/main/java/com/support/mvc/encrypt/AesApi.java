package com.support.mvc.encrypt;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.utils.aes.Aes;
import com.utils.aes.IvRandomRule;
import com.utils.enums.Code;
import com.utils.util.Dates;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.utils.util.Dates.Pattern.time_millis;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * api 数据包加解密
 *
 * @author 谢长春 2022-01-30
 */
@Slf4j
public class AesApi {

    /**
     * https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#trans
     */
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 参考文档: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
     * <pre>
     * Cipher (Encryption) Algorithms
     * Cipher Algorithm Names
     * The following names can be specified as the algorithm component in a transformation when requesting an instance of Cipher.
     *
     * <table border="5" cellpadding="5" frame="border" width="90%" summary="Cipher Algorithm Names">
     * <thead> <tr> <th>Algorithm Name</th> <th>Description</th> </tr> </thead>
     * <tbody>
     * <tr> <td>AES</td> <td>Advanced Encryption Standard as specified by NIST in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS 197</a>. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits.</td> </tr>
     * <tr> <td>AESWrap</td> <td>The AES key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3394.txt">RFC 3394</a>.</td> </tr>
     * <tr> <td>ARCFOUR</td> <td>A stream cipher believed to be fully interoperable with the RC4 cipher developed by Ron Rivest. For more information, see K. Kaukonen and R. Thayer, "A Stream Cipher Encryption Algorithm 'Arcfour'", Internet Draft (expired), <a href="http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt"> draft-kaukonen-cipher-arcfour-03.txt</a>.</td> </tr>
     * <tr> <td>Blowfish</td> <td>The <a href="http://www.schneier.com/blowfish.html">Blowfish block cipher</a> designed by Bruce Schneier.</td> </tr>
     * <tr> <td>CCM</td> <td>Counter/CBC Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38C/SP800-38C_updated-July20_2007.pdf">NIST Special Publication SP 800-38C</a>.</td> </tr>
     * <tr> <td>DES</td> <td>The Digital Encryption Standard as described in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS PUB 46-3</a>.</td> </tr>
     * <tr> <td>DESede</td> <td>Triple DES Encryption (also known as DES-EDE, 3DES, or Triple-DES). Data is encrypted using the DES algorithm three separate times. It is first encrypted using the first subkey, then decrypted with the second subkey, and encrypted with the third subkey.</td> </tr>
     * <tr> <td>DESedeWrap</td> <td>The DESede key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3217.txt">RFC 3217</a> .</td> </tr>
     * <tr> <td>ECIES</td> <td>Elliptic Curve Integrated Encryption Scheme</td> </tr>
     * <tr> <td>GCM</td> <td>Galois/Counter Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38D/SP-800-38D.pdf">NIST Special Publication SP 800-38D</a>.</td> </tr>
     * <tr> <td>PBEWith&lt;digest&gt;And&lt;encryption&gt; PBEWith&lt;prf&gt;And&lt;encryption&gt;</td> <td>The password-based encryption algorithm found in (PKCS5), using the specified message digest (&lt;digest&gt;) or pseudo-random function (&lt;prf&gt;) and encryption algorithm (&lt;encryption&gt;). Examples: <ul> <li><b>PBEWithMD5AndDES</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, Nov 1993</a>. Note that this algorithm implies <a href="#cbcMode"><i>CBC</i></a> as the cipher mode and <a href="#pkcs5Pad"><i>PKCS5Padding</i></a> as the padding scheme and cannot be used with any other cipher modes or padding schemes.</li> <li><b>PBEWithHmacSHA256AndAES_128</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Cryptography Standard," version 2.0, March 1999</a>.</li> </ul> </td> </tr>
     * <tr> <td>RC2</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RC4</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc. (See note prior for ARCFOUR.)</td> </tr>
     * <tr> <td>RC5</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RSA</td> <td>The RSA encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2125">PKCS #1</a></td> </tr>
     * </tbody>
     * </table>
     */
    private static final String CIPHER_ALGORITHM = "AES";
    private static final int ZERO = 0;
    /**
     * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    public static IvParameterSpec IV_PARAMETER;
    /**
     * 16 位 IV 规则， 默认全部随机，无规律
     */
    public static IvRandomRule IV_RANDOM_RULE = IvRandomRule.alphanumeric;
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    private static String SK;
    public static SecretKeySpec SECRET_KEY;
    /**
     * 加解密开关。 true：是，false：否。
     */
    public static boolean ENABLED;

    private AesApi() {

    }

    private static final Function<byte[], String> hexEncode = (byte[] bytes) -> Hex.encodeHexString(bytes);
    private static final Function<String, byte[]> hexDecode = (String hexString) -> {
        try {
            return Hex.decodeHex(hexString);
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    };
    private static final Function<byte[], String> base64Encode = (byte[] bytes) -> Base64.encodeBase64String(bytes);
    private static final Function<String, byte[]> base64Decode = (String base64String) -> Base64.decodeBase64(base64String);


    /**
     * 设置是否开启加解密开关。 true：是，false：否。
     *
     * @param enabled boolean true：是，false：否。
     */
    public static void setEnabled(final boolean enabled) {
        ENABLED = enabled;
    }

    /**
     * 设置密钥
     *
     * @param secretKey {@link String} RandomStringUtils.randomAlphanumeric(32)
     */
    public static void setSecretKey(final String secretKey) {
        if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
            throw new IllegalArgumentException("secretKey 长度必须是 32 位");
        }
        SK = secretKey;
        SECRET_KEY = new SecretKeySpec(secretKey.getBytes(UTF_8), CIPHER_ALGORITHM);
    }

    /**
     * <pre>
     * 设置动态 IV 生成规则
     *   IvRandomRule.alphanumeric     : RandomStringUtils.randomAlphanumeric 工具类生成 16 位随机 【A-Za-z0-9】 的字符串，解密时不校验 IV 生成规则
     *   IvRandomRule.dateAlphanumeric : 10 位日期(yyyyMMddHH) + 6 位随机 【A-Za-z0-9】 的字符串，解密时提取前 10 位日期，检查有效期为 24 小时
     *   IvRandomRule.timestamp        : 13位时间戳，前面补0，解密检查时间戳，有效期为 24 小时
     * </pre>
     *
     * @param rule {@link IvRandomRule}
     */
    public static void setIvRandomRule(final IvRandomRule rule) {
        IV_RANDOM_RULE = rule;
    }

    /**
     * 设置 IV ， 长度必须是 16 位自负
     *
     * @param iv {@link String} RandomStringUtils.randomAlphanumeric(16)
     */
    @SneakyThrows
    public static void setIV(final String iv) {
        if (Strings.isNullOrEmpty(iv) || iv.length() != 16) {
            throw new IllegalArgumentException("secretKey 长度必须是 16 位");
        }
        IV_PARAMETER = new IvParameterSpec(iv.getBytes(UTF_8));
    }

    /**
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param data {@link String} 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encrypt(final String data) {
        try {
            if (Strings.isNullOrEmpty(data)) {
                return null;
            }
            if (!ENABLED) {
                return data;
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 IV 使用动态 IV 加密
                // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
                final byte[] ivBytes = IV_RANDOM_RULE.get();
                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                // 初始化加密参数，设置为加密模式，指定密钥，设置IV
                cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
                final byte[] dataBytes = cipher.doFinal(data.getBytes(UTF_8));
                final byte[] bytes = new byte[IvRandomRule.IV_LENGTH + dataBytes.length];
                System.arraycopy(ivBytes, ZERO, bytes, ZERO, IvRandomRule.IV_LENGTH);
                System.arraycopy(dataBytes, ZERO, bytes, IvRandomRule.IV_LENGTH, dataBytes.length);
                // final String result = hexEncode.apply(bytes);
                final String result = base64Encode.apply(bytes);
                if (log.isDebugEnabled()) {
                    log.debug("{} => {}", data, result);
                }
                return result;
            }
            // 使用固定 IV 加密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            // final String result = hexEncode.apply(cipher.doFinal(data.getBytes(UTF_8)));
            final String result = base64Encode.apply(cipher.doFinal(data.getBytes(UTF_8)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", data, result);
            }
            return result;
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%s", data);
        }
    }

    /**
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param list {@link Collection<String>} 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static List<String> encrypt(final Collection<String> list) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return Collections.emptyList();
            }
            if (!ENABLED) {
                return new ArrayList<>(list);
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 加密
                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                return list.stream()
                        .map(data -> {
                            try {
                                // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
                                final byte[] ivBytes = IV_RANDOM_RULE.get();
                                cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));

                                final byte[] dataBytes = cipher.doFinal(data.getBytes(UTF_8));
                                final byte[] bytes = new byte[IvRandomRule.IV_LENGTH + dataBytes.length];
                                System.arraycopy(ivBytes, ZERO, bytes, ZERO, IvRandomRule.IV_LENGTH);
                                System.arraycopy(dataBytes, ZERO, bytes, IvRandomRule.IV_LENGTH, dataBytes.length);
                                // final String result = hexEncode.apply(bytes);
                                final String result = base64Encode.apply(bytes);
                                if (log.isDebugEnabled()) {
                                    log.debug("{} => {}", data, result);
                                }
                                return result;
                            } catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
                                throw new IllegalArgumentException(String.format("加密失败:%s", data), e);
                            }
                        })
                        .collect(Collectors.toList());
            }
            // 使用固定的 IV 加密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            return list.stream()
                    .map(data -> {
                        try {
                            // final String result = hexEncode.apply(cipher.doFinal(data.getBytes(UTF_8)));
                            final String result = base64Encode.apply(cipher.doFinal(data.getBytes(UTF_8)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return result;
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("加密失败:%s", data), e);
                        }
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%s", String.join(",", list));
        }
    }

    /**
     * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
     *
     * @param data {@link String} 密文
     * @return 明文
     */
    @SneakyThrows
    public static String decrypt(final String data) {
        try {
            if (Strings.isNullOrEmpty(data)) {
                return null;
            }
            if (!ENABLED) {
                return data;
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
                // 将 data 分割成 IV 和密文
                // final byte[] bytes = hexDecode.apply(data);
                final byte[] bytes = base64Decode.apply(data);
                final byte[] ivBytes = new byte[IvRandomRule.IV_LENGTH];
                final byte[] dataBytes = new byte[bytes.length - IvRandomRule.IV_LENGTH];
                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IvRandomRule.IV_LENGTH);
//                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                System.arraycopy(bytes, IvRandomRule.IV_LENGTH, dataBytes, ZERO, dataBytes.length);

                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
                final String result = new String(cipher.doFinal(dataBytes));
                if (log.isDebugEnabled()) {
                    log.debug("{}({}) => {}", data, new String(ivBytes), result);
                }
                return result;
            }
            // 使用固定的 IV 解密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            // final String result = new String(cipher.doFinal(hexDecode.apply(data)));
            final String result = new String(cipher.doFinal(base64Decode.apply(data)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", data, result);
            }
            return result;
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", data);
        }
    }

    /**
     * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
     *
     * @param list {@link Collection<String>} 密文
     * @return 明文
     */
    @SneakyThrows
    public static List<String> decrypt(final Collection<String> list) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return Collections.emptyList();
            }
            if (!ENABLED) {
                return new ArrayList<>(list);
            }
            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
                final byte[] ivBytes = new byte[IvRandomRule.IV_LENGTH];
                return list.stream()
                        .map(data -> {
                            try {
                                // 将 data 分割成 IV 和密文
                                // final byte[] bytes = hexDecode.apply(data);
                                final byte[] bytes = base64Decode.apply(data);
                                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IvRandomRule.IV_LENGTH);
//                                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                                cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));

                                final byte[] dataBytes = new byte[bytes.length - IvRandomRule.IV_LENGTH];
                                System.arraycopy(bytes, IvRandomRule.IV_LENGTH, dataBytes, ZERO, dataBytes.length);
                                final String result = new String(cipher.doFinal(dataBytes));
                                if (log.isDebugEnabled()) {
                                    log.debug("{} => {}", data, result);
                                }
                                return result;
                            } catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
                                throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                            }
                        })
                        .collect(Collectors.toList());

            }
            // 使用固定的 IV 解密
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
            return list.stream()
                    .map(data -> {
                        try {
                            // final String result = new String(cipher.doFinal(hexDecode.apply(data)));
                            final String result = new String(cipher.doFinal(base64Decode.apply(data)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return result;
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                        }
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", String.join(",", list));
        }
    }

    /**
     * api请求数据验签
     * post|put|patch|delete 数据接口验证请求 body
     * post 文件上传接口验证 request uri ， 需要排除 sign 参数
     * get 接口验证 request uri ， 需要排除 sign 参数
     *
     * @param timeMillis 当前时间戳精确到毫秒
     * @param random     随机字符串
     * @param aesBody    aes加密后的请求体
     * @param sign       客户端签名
     */
    public static void assertSign(
            final String timeMillis
            , final String random
            , final String aesBody
            , final String sign
    ) {
        if (!ENABLED) {
            return;
        }
        String signature = "";
        try {
            Code.A00002.assertNonBlank(timeMillis, "签名时间戳无效");
            Code.A00002.assertNonBlank(random, "签名随机字符串无效");
            Code.A00002.assertNonBlank(sign, "签名无效");
            Code.A00002.assertHasTrue(
                    Objects.requireNonNull(time_millis.parse(timeMillis)).before(Dates.now().addMinute(5))
                    , "签名时间戳有效期为5分钟"
            );
            signature = DigestUtils.md5Hex(
                    String.join("",
                            Stream.of(
                                            timeMillis, // 当前时间戳精确到毫秒
                                            random, // 随机字符串长度32-128位
                                            SK, // AES固定加密密钥32位
                                            Optional.ofNullable(aesBody).orElse("") // AES+Base64加密的数据包
                                    )
                                    .collect(Collectors.toCollection(TreeSet::new))
                    )
            );
            Code.A00002.assertHasTrue(Objects.equals(sign, signature), "验签失败");
        } catch (Exception e) {
            log.error("sign:{}, signature:{}, timeMillis:{}, random:{}, aesBody:{}", sign, signature, timeMillis, random, aesBody);
            throw e;
        }
    }

    public static void main(String[] args) {
        final String secretKey = "VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF";
        AesApi.setSecretKey(secretKey);
        AesApi.setIvRandomRule(IvRandomRule.timestamp);
        AesApi.setEnabled(true);
        System.out.println(AesApi.decrypt("MDAwMTY0NTM3MDc5NDg3Noi6yx5tK5fmHi8+hph2+LbJVLjZ9qFUy6reDA9D9hvk89xOhz0S4c8KerEwyidZNRuhqhIsoMftT65sPy4zFyOcnT3sLNtoEhbrajDc9YTwTG7KcIe1y976JaFqa7JKWoSNsOtYnIf6K5VwyGKOHsg0l+SCAQrKOb0W4aTQBEJTKDZisqZd6zlTtkhxz0a9h3uuvES+d0AaX11Bc+SsxS72URyuuQrvxfiNsw7N+Yl8+PkJG4vi2srJ8IGNKlLAgPNzhjttb1v5Au+wcJw8/ClF3OL3M4ouMMZ5zR83wh963e2D4Z/m/tUAjNtaSCaBnIMYBYomDBa01CYqXK9bBEEjqj+yU20bxxtFR6yDqpt7/pr3nWTgKDKXzLoO7Cx4bsKvpVuc2zSUJvkGmsLpLQqphlWZl149+18QbxgvQd3cYU8q5JKoly+99mMEvlqi6jKOxhCyd3TofVCfHlo3DbqFFPv1cKl/pmyS+A0ftIlg3m/BhMYqeWgTYpSKek+3Xyy9XzVCgeUlMWjF3fk98Z/t61M5/EaDkA2ngz3sPwCy1IgnlN859OmzDQIkWzFkB0IGkDkD99aNgL7v39HzQHizOqUtfl2s5dE2h6IJtq3npEJ3NV9yDvkJg2l30RMCKU3KZ8TaUgY8wkBhFWaUnuuN4tHhESvsb2uOKcAahn9ZIySPldovX57d9aMTnIRmvSd6SA5ZAPU5YoqQsLQZWe4cFGD15whWdD1maUg38vyL1Lae9V6ozoepgeDwWJ3yZHcvZ8dzDySddNpGESiq/8VngJFlkmOGqA+d+agS4x+je/g8qyZ0mzM0ycJ4t++j1n7478UIHbQqUyCDWtonAo48IESBhVHp1Dm3qOFa8U086BcE25+cO+XGLW8N2x0m36dLu1NEsu3ZI58ck7j1iKY7+kPGqmdMTBPzSmwsIrqamKflfURXi3fhhXuNf4PEje30W3J5lZobG2bOZ4KvdXiqvowmK7pPvMR2bblCFsrVR/WcalJlmF5/+XIMqvqbQ1Zm/FdqvytREVcqHNNjklEOdrsadUcggKXOKA0X7tMR3f5mrs8xY3Nc+37s/FzFpAW4o17lHB0tXIbpolsCadLIoDfYtj5DqFB785FnPw3Im+fxGL7Ge9CvO6ofcasrhrqscF6skuGEsHNWywJV9SJuLeLTvMFipFE8HFZud3xz78/F7ZVvUwbABIraQChjSY9KZiCerQM1eMsS7Yu401k4eMzCwoz+b9UzUx+zIJyhZJrVouTFZFDoG47wDap2/yBxp1Fsokf4ci9GjKHFj0jTUVUtkFJbFY/MKCcTYh2vFoZtMwf0lF1hBqcRxKUANe+sZsyNNPDXbjCpJhxVjiF4UP7+OCO+WJCExP7c2Yak7SsDR53eUaJjV7Ob9dO/X2T3IVUoQQBsqq6qtMAAAtIQDQAOBg9W5/Y9/7Wl/BYXLXXNUNBFvguCWTKVW7DH6EFhvbpcn1ZijIRzcOqgFQBnAYdMqbf6w6k52mGwMudzPZNcs25ZKkYD8BA0q63Z+vSgs8qt7rERugjq7bkc4JzePT2/532TA6b4YukIwf6XfRMSQ7KuBKofoFOPsKQlmDB3pkGnAYodOfPTApv4i229ZUXUSuQ9k/XD5sQT2cWBJAWVA1rDutvNNDwfLdvr6Z8YpEwFHvZRuiSr/gq5f5RCJpXQBDxR9FIIYgn7SLx6pjE1ey2QECCtEP7j4/XAbPvKx778E+hZXvPCCiDb6EcWUzMxmw+SIOgpPHczVC004AY3WwcmWrgJvppkX3geBuYbv3SXK8ZiKmHzrt42C7xiBztlxz3ANBZkgGgnF/6+QrjFKxgaWlE2RHaqxVY3EgPNsxSg3EwKMjI2Jt5jEqLnXRjJRlrobu9m39CMpNsRz4h4HaiMHnUZT72cNolEy+nQtVHB0xMZvRPSNC6WgKTKAfQTzGpUR+p5ASEdNE/AUTA17Ddq/StiiWQqj9hbuROVXxFyqE7tVDIedHiikrPIsC0++Xk+5jkHkIYj1rzRx4k2japuURyTxPqma1Pd+wRPfa/C6RT+sgJHxuC5d2omGgr1Kp8cRheoRNEH0gLIM7WywmltiIDvHa8rxl04QBOYZ04F1FPPyHphpKpaMtRKagpV8fVIsTVXAcDrxI9UaBELEWt2jspw6Qwx3SWl+/AQmfBo1MElMphlBkT5Xv1OajWgo+1iRMgeWMw1PAAq8MQYfaGi59rv3HthfWKFX9LiMToIl73HgBd4mF+WnBKUgSy36rADqdTIihx4oQEJHr2Ywbf9zx/uw4KCX2ySwRMG2IKLqs5X9lILw5Y9121UdPFZpcRX25JsOZLZjOdMuNwzQ9beW65qYwphs9kgNzLJQpxpQnsbHr63+XeyWfFpOsojGdvCFVIUSMfnBmR5xRBbMHnUPIvkjIxVRbcF1yU3XycmgMWD182bF97PCnBpKUyiBCNLYoasaBEcRi4RlswX1EGFdfNX5ZyNBibRHMEcl1taBj+m/Z2bCnF8JOkfuNKNT7me0n1cxt+wp8g5zO1L/IrGy4iSjf9hlOICmD5Wy+yvRo6rET5GAxXFhSVKwWeDTJLNd/dy5MLgmmP3+SPgz/JRU93I7y/q4lDOQO/OjJlua7Ms3JUU0sAysrxSpgk8GxhKSYksxu3agZZu+/2W2Ai3hIg6RAdb0Z9lXMFGS6+rOeFDKpUsj6u93S80+W3rl4xLDPh5jORKsvG8ZCYYOijhTt6JlZIEB6b+ZuqDH/bkEyyGnvcLnCwmO+Q3mNTlYFicJXLULDlbChe8xWDYY4WYgW5/pNHDq5hjmq8btbqg9BSDgX4RC4RhNiEzIPNZkp3QGlA1jk90KKXWti5oZJ48oe1sdImfLB+0N62Rc1Vp1P3leK4M79VhEORZbn5Wzukid7wruqE+B4SD9/3jKD77X8kld+S6SyasRUTgwcfwbEFAD5s6xoCepiiBGbpX1K7V9tQjp3+oGr2o06890jkZKztqfz/L9GC2uJ8CGD1tz1liJw8EzX5Rt9wm1uqViSlxJeEblUFSbSj/GTzgxvzwMRPMOQ9QSFvOyKCc+LiIX0Yp1kOOUSAH+yfQcQgOUP+li18V0xG3njx6jGZH2eGuKuVTEW2oOdTQsITz03Fi8q5jiXw92amtL+jkpjrfqNWk9c4E1A7DxJ/fTFr1gh7aJmI2EQNPaj9cAHM9bFnsHIQ7YQJY6B9p+PRj5gyI+2VXekRew+1J5BG/WWtD0byeqOH9GCsoBSKqlbFZZyXHb5tpGO5v32WNLDvTut4jpQ2r63bZFSrjS9NDubrjaAUuNSPyUq2oE4Y7zHlq8Rk/pityhycWLdqkrkF4Sx5zs0P2BJYasx0m1Za2o+jpnw9IjPS2aaQM5CBLRQHp32cIH/qo3IkLqyo7XjIkVkgk3D2U7UKwOeLGhXPhyq5mAsWuix0JPR+wqE94VlaQHTWjK8J1GK/4UxgKCV3ET+7+MTYz1EkSIltmftiDmOLlL0H0HyfjnVzu/LwpVfftSMxHFMIz55iOpR7sXxn1hLE/gTKAyOLq4QDQWxwfhJdAOYzI+7do6aZcnJWAERn0dFSSj3hZxnyPRNiMvl3zmcai+LZpS37AqOr3scotB1zkw0VNblwGyLKnKCcaHGE2f63kN34oOi5ttSdE7Mp2xXe9pIHV4EsYOY4IGM30Mx4tzbArC5MJYqe6AtSkOoj41vXnfmIh6W6mQ4QkGQ1ZLLxX2i6bz4ar0mPPkNo5U9nxuc0L/kI8rpzweTKXF2SfwOrgeT+XJrkAcKlqZpgGgPd0S/42AWJDPFI49+JuhUNHmM++OHF0yRKzwMzTEFuird2pLgUW0LXJJMQhk4y6HLGb1w1M/hwGcZCPp1a3137MK38x+kRwMs+fO0rhfPktiRFY8YBMWNBizNqea2vbH2F9jh/sEclVyw4MCkFHNIlSpScxh4yuU420u80PtWNv0IQbn81QF+wJKOKBU7jvwBBMho2aJL6CTPF7JTRrpyzgDgqTwE+6Lbv3Al90Rkg5jNA1LOz+MxwUWsN8UaIxy+QHiS+42oA/d/c3HcE78WNEjUnKO26jMPTOmYWfeOr2lXPdn2rDthimcpmgQcu6wK8rFnjroP9dKSywPgKVgNtzjaAZYTA57blREaBGSate5EXrOnPuLWYnnsc9eiLhVadg/ExJ/aklyMXbjP8YLYapahsJR5EuFzlkbbMs4ZmML0j9Uhqgt/UHgbG4XbMBVgjJa2UWit8MBrSGrURGcGFGSnJQ0ua0/g0Lnd/56eAN9XqVvta7+WMI85oqGGhVIUDIga4y1Ar8S7kaCizDZj6B8Y7aW3/lYY9G6I9/f9UOxfCkR29mLQtE62a2GYP/p9/KjBQyuDiFiCQ+8coNHSqqoxEWYBWQdacwsB84tAgLoE5F0QDAbsnKbd06mWN8wU6flKciP3nwajpNs7l4lES/KRnuEUJ86DhoBSND70OLeeckcHiAbJAo8Hs9ojg2Hz+CSfwE5VbogzCEIc8BrMD5fQR297hsxl6vvkJ+jZhzRfRWZFgATHs0FZK/no59NqEkFz+87ABE49swsl10zjWFW9pOW94ndloXMmz7SUuyFJD2cP9cH5M0rP9Kfa49q1hACBK2ILUmTy01vcOb6FXUQrQHTwQlkr8zrgH6ISQ5ky4UdW1bo69kvjS/CIsj9NIgc4gilRvLzdk1VbT1GIDiktzhHrom5Y316j7Gj2pb/GTWAaLeWWsFWT1xUfiUVoM/qyCucT14eqzqqgy6L2sujeSC7pxXfyWs/+/if91322Il4xFsbdWEMPmyEBxEsGnom8hM4MzlRDNQhVDsIf6W312bE3uMn3gGH4OrmDAPAu6HbONYeZ0OzMAZxcNSvZksv/BAzQx0CJ5gxWC0zin5vPyhq2KRNgwQ8bmb7kCNMQZV9RO8OWyz/6GbuERCrnZ1RcgNGMl16fUWFWYggBx4em7P1NR03Hmx8dX4tkmpban6a27psWjoQZ0Eh9//mlY3brmkrxjTGSCNA5gdfVtXK+FimRhZO+zfulkkf1c/y7egWh4EFt2Uv8fuAWTe3eDrEhTl4NO13JkOKutTooGhYB8++8kCfpGWsofaJGJoNiwK0ASSfpA4l+zjfLJQvEnBohM3IGwB9pbMJZhbD1qbWmTdc/cKJsL+oXuJxe9TJ+IeJ87MoF6aV57vylA4gzOX1WjMhZQa7tvfV/CLx5zsX+vggF6pCTLZkjR2kf1kSwEtwBkxBUvz5ntTJQPzkg1FuC4/i1BIC9bG6hiB"));

        // api请求数据签名测试
        {
//            final String aesBody = encrypt("明文，使用aes加密");
//
//            final long timeMillis = System.currentTimeMillis();
//            final String random = RandomStringUtils.randomAlphanumeric(32);
//            final String sign = DigestUtils.md5Hex(
//                    String.join("",
//                            Sets.newTreeSet(Arrays.asList(
//                                    Objects.toString(timeMillis), // 当前时间戳精确到毫秒
//                                    random, // 随机字符串长度32-128位
//                                    secretKey, // AES固定加密密钥32位
//                                    aesBody // AES+Base64加密的数据包
//                            ))
//                    )
//            );
//            log.info("\n- api请求数据签名测试 ------------------------" +
//                            "\n\t前后端约定的密钥：{}" +
//                            "\n\t当前时间戳精确到毫秒：{}" +
//                            "\n\t随机字符串：{}" +
//                            "\n\taes加密后的请求体：{}" +
//                            "\n\t签名：{}" +
//                            "\napi请求数据验签测试 : {}"
//                    , secretKey
//                    , timeMillis
//                    , random
//                    , aesBody
//                    , sign
//                    , Objects.equals(sign, DigestUtils.md5Hex(
//                            String.join("", Stream
//                                    .of(
//                                            Objects.toString(timeMillis), // 当前时间戳精确到毫秒
//                                            random, // 随机字符串长度32-128位
//                                            secretKey, // AES固定加密密钥32位
//                                            aesBody // AES+Base64加密的数据包
//                                    )
//                                    .filter(Objects::nonNull)
//                                    .collect(Collectors.toCollection(TreeSet::new))
//                            )
//                    ))
//            );
//            log.info("测试签名数据排序规则：{}", JSON.toJSONString(
//                    Stream.of(
//                                    System.currentTimeMillis() + ":当前时间戳精确到毫秒",
//                                    RandomStringUtils.randomAlphanumeric(32) + "：随机字符串长度32-128位",
//                                    secretKey + "：AES固定加密密钥32位",
//                                    aesBody // AES+Base64加密的数据包
//                            )
//                            .collect(Collectors.toCollection(TreeSet::new))
//            ));
//            log.info("测试签名数据排序规则：{}", JSON.toJSONString(Stream.of("B", "9", "0", "a", "1", "b", "A", "Z", "z").collect(Collectors.toCollection(TreeSet::new))));
        }
    }

}
