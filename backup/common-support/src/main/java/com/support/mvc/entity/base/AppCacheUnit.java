package com.support.mvc.entity.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.unit.DataSize;

import java.time.Duration;

/**
 * 缓存单元配置
 */
@Getter
@Setter
@ToString
public class AppCacheUnit {
    /**
     * 过期时间
     */
    private Duration expired;
    /**
     * 堆内保持数据行数
     */
    private long heap;
    /**
     * 堆外占用内存
     */
    private DataSize offheap;
    /**
     * 磁盘占用空间
     */
    private DataSize disk;
}
