package com.support.mvc.entity;

import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * 用户信息抽象接口， 用于 Controller 层泛型获取用户信息
 *
 * @author 谢长春
 */
public interface IUser extends IOpenUser {
    /**
     * 客户端指纹
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getFingerprint() {
        return null;
    }

    /**
     * 用户id
     *
     * @return {@link Long}
     */
    Long getId();

    /**
     * 用户登录名
     *
     * @return {@link String}
     */
    String getUsername();

    /**
     * 用户昵称
     *
     * @return {@link String}
     */
    String getNickname();

    /**
     * 用户手机号
     *
     * @return {@link String}
     */
    String getPhone();

    @Override
    default boolean isUserSessionExist() {
        return Objects.nonNull(getId()) && getId() > 0L;
    }

    IUser EMPTY = new IUser() {
        @Override
        public String getFingerprint() {
            return null;
        }

        @Override
        public Long getId() {
            return null;
        }

        @Override
        public String getUsername() {
            return null;
        }

        @Override
        public String getNickname() {
            return null;
        }

        @Override
        public String getPhone() {
            return null;
        }

        @Override
        public String toString() {
            return null;
        }
    };
}
