package com.support.mvc.entity.convert;

import com.utils.util.Base64;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * mysql 字段 Base64 编码后存储
 * 需要在实体类属性上添加注解：@Convert(converter = {@link Base64Convert}.class)
 *
 * @author 谢长春 2019/2/12
 */
@Converter
public class Base64Convert implements AttributeConverter<String, String> {
    @Override
    public String convertToDatabaseColumn(final String attribute) {
        return Base64.encode(attribute);
    }

    @Override
    public String convertToEntityAttribute(final String dbData) {
        return Base64.decode(dbData);
    }
}
