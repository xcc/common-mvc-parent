package com.support.mvc.entity.base;

import com.alibaba.fastjson.annotation.JSONType;
import com.utils.IJson;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 事件消息对象
 *
 * @author 谢长春 on 2017/11/16.
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel(description = "通用事件消息")
@JSONType(orders = {"event", "message", "extras"})
public class Message implements IJson {

    /**
     * 可选的事件属性
     */
    @ApiModelProperty(value = "事件类型", example = "SUCCESS")
    private Object event;
    /**
     * 异常消息内容
     */
    @ApiModelProperty(value = "事件消息", example = "成功")
    private String message;
    /**
     * 扩展属性
     */
    @ApiModelProperty(value = "事件响应结果")
    private Map<String, Object> extras;

    /**
     * 添加附加属性
     *
     * @param key   {@link String}
     * @param value {@link Object}
     * @return {@link Message}
     */
    public Message addExtras(String key, Object value) {
        if (org.apache.commons.collections4.MapUtils.isEmpty(this.extras)) {
            this.extras = new LinkedHashMap<>(3);
        }
        extras.put(key, value);
        return this;
    }


    public static Message.MessageBuilder builder() {
        return new Message.MessageBuilder();
    }

    public static class MessageBuilder {
        private Object event;
        private String message;
        private Map<String, Object> extras;

        MessageBuilder() {
        }

        public Message.MessageBuilder event(Object event) {
            this.event = event;
            return this;
        }

        public Message.MessageBuilder message(String message) {
            this.message = message;
            return this;
        }

        public Message.MessageBuilder extras(Map<String, Object> extras) {
            this.extras = extras;
            return this;
        }

        public Message build() {
            final Message message = new Message();
            message.setEvent(this.event);
            message.setMessage(this.message);
            message.setExtras(this.extras);
            return message;
        }
    }

}
