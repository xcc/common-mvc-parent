package com.support.mvc.entity.convert;

import com.google.common.base.Strings;
import com.utils.aes.Aes;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 数据库字段加密存储；
 * 项目启动时需要设置密钥：AesConvert.setSecretKey(32位密钥)
 * 项目启动时可选择设置固定IV，IV固定之后相同的数据加密得到相同的结果：AesConvert.setIV(16位固定IV)
 * 需要在实体类属性上添加注解：@Convert(converter = {@link AesConvert}.class)
 *
 * @author 谢长春 2022-01-30
 */
@Slf4j
@Converter
public class AesConvert implements AttributeConverter<String, String> {

    /**
     * https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#trans
     */
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 参考文档: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
     * <pre>
     * Cipher (Encryption) Algorithms
     * Cipher Algorithm Names
     * The following names can be specified as the algorithm component in a transformation when requesting an instance of Cipher.
     *
     * <table border="5" cellpadding="5" frame="border" width="90%" summary="Cipher Algorithm Names">
     * <thead> <tr> <th>Algorithm Name</th> <th>Description</th> </tr> </thead>
     * <tbody>
     * <tr> <td>AES</td> <td>Advanced Encryption Standard as specified by NIST in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS 197</a>. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits.</td> </tr>
     * <tr> <td>AESWrap</td> <td>The AES key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3394.txt">RFC 3394</a>.</td> </tr>
     * <tr> <td>ARCFOUR</td> <td>A stream cipher believed to be fully interoperable with the RC4 cipher developed by Ron Rivest. For more information, see K. Kaukonen and R. Thayer, "A Stream Cipher Encryption Algorithm 'Arcfour'", Internet Draft (expired), <a href="http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt"> draft-kaukonen-cipher-arcfour-03.txt</a>.</td> </tr>
     * <tr> <td>Blowfish</td> <td>The <a href="http://www.schneier.com/blowfish.html">Blowfish block cipher</a> designed by Bruce Schneier.</td> </tr>
     * <tr> <td>CCM</td> <td>Counter/CBC Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38C/SP800-38C_updated-July20_2007.pdf">NIST Special Publication SP 800-38C</a>.</td> </tr>
     * <tr> <td>DES</td> <td>The Digital Encryption Standard as described in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS PUB 46-3</a>.</td> </tr>
     * <tr> <td>DESede</td> <td>Triple DES Encryption (also known as DES-EDE, 3DES, or Triple-DES). Data is encrypted using the DES algorithm three separate times. It is first encrypted using the first subkey, then decrypted with the second subkey, and encrypted with the third subkey.</td> </tr>
     * <tr> <td>DESedeWrap</td> <td>The DESede key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3217.txt">RFC 3217</a> .</td> </tr>
     * <tr> <td>ECIES</td> <td>Elliptic Curve Integrated Encryption Scheme</td> </tr>
     * <tr> <td>GCM</td> <td>Galois/Counter Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38D/SP-800-38D.pdf">NIST Special Publication SP 800-38D</a>.</td> </tr>
     * <tr> <td>PBEWith&lt;digest&gt;And&lt;encryption&gt; PBEWith&lt;prf&gt;And&lt;encryption&gt;</td> <td>The password-based encryption algorithm found in (PKCS5), using the specified message digest (&lt;digest&gt;) or pseudo-random function (&lt;prf&gt;) and encryption algorithm (&lt;encryption&gt;). Examples: <ul> <li><b>PBEWithMD5AndDES</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, Nov 1993</a>. Note that this algorithm implies <a href="#cbcMode"><i>CBC</i></a> as the cipher mode and <a href="#pkcs5Pad"><i>PKCS5Padding</i></a> as the padding scheme and cannot be used with any other cipher modes or padding schemes.</li> <li><b>PBEWithHmacSHA256AndAES_128</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Cryptography Standard," version 2.0, March 1999</a>.</li> </ul> </td> </tr>
     * <tr> <td>RC2</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RC4</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc. (See note prior for ARCFOUR.)</td> </tr>
     * <tr> <td>RC5</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RSA</td> <td>The RSA encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2125">PKCS #1</a></td> </tr>
     * </tbody>
     * </table>
     */
    private static final String CIPHER_ALGORITHM = "AES";
    /**
     * 随机生成 IV 长度， 只能固定 16 位
     */
    private static final int IV_LENGTH = 16;
    private static final int ZERO = 0;
    /**
     * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    public static IvParameterSpec IV_PARAMETER;
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    public static SecretKeySpec SECRET_KEY;
    /**
     * 加解密开关。 true：是，false：否。
     */
    public static boolean ENABLED;

    @Override
    public String convertToDatabaseColumn(final String attribute) {
        if (!ENABLED) {
            return attribute;
        }
        return encrypt(attribute);
    }

    @Override
    public String convertToEntityAttribute(final String dbData) {
        if (!ENABLED) {
            return dbData;
        }
        return decrypt(dbData);
    }

    /**
     * 设置是否开启加解密开关。 true：是，false：否。
     *
     * @param enabled boolean true：是，false：否。
     */
    public static void setEnabled(final boolean enabled) {
        ENABLED = enabled;
    }

    /**
     * 设置密钥
     *
     * @param secretKey {@link String} RandomStringUtils.randomAlphanumeric(32)
     */
    public static void setSecretKey(final String secretKey) {
        if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
            throw new IllegalArgumentException("secretKey 长度必须是 32 位");
        }
        SECRET_KEY = new SecretKeySpec(secretKey.getBytes(UTF_8), CIPHER_ALGORITHM);
    }

    /**
     * 设置 IV ， 长度必须是 16 位自负
     *
     * @param iv {@link String} RandomStringUtils.randomAlphanumeric(16)
     */
    @SneakyThrows
    public static void setIV(final String iv) {
        if (Strings.isNullOrEmpty(iv) || iv.length() != 16) {
            throw new IllegalArgumentException("secretKey 长度必须是 16 位");
        }
        IV_PARAMETER = new IvParameterSpec(iv.getBytes(UTF_8));
    }

    /**
     * <pre>
     * aes 加密， 有两种模式：
     * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
     * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
     * </pre>
     *
     * @param data {@link String} 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encrypt(final String data) {
        if (Strings.isNullOrEmpty(data)) {
            return "";
        }
        if (Objects.isNull(IV_PARAMETER)) { // 未指定 IV 使用动态 IV 加密
            // 数据库敏感字段不能用动态IV加密，否则不支持搜索
            throw new IllegalArgumentException("请指定16位固定IV");
//            // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
//            final byte[] ivBytes = RandomStringUtils.randomAlphanumeric(IV_LENGTH).getBytes(UTF_8);
//            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            // 初始化加密参数，设置为加密模式，指定密钥，设置IV
//            cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//            final byte[] dataBytes = cipher.doFinal(data.getBytes(UTF_8));
//            final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
//            System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
//            System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
//            final String result = Hex.encodeHexString(bytes);
////            final String result = org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
//            if (log.isDebugEnabled()) {
//                log.debug("{} => {}", data, result);
//            }
//            return result;
        }
        // 使用固定 IV 加密
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV_PARAMETER);
        final String result = Hex.encodeHexString(cipher.doFinal(data.getBytes(UTF_8)));
//        final String result = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(data.getBytes(UTF_8)));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", data, result);
        }
        return result;
    }

    /**
     * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
     *
     * @param data {@link String} 密文
     * @return 明文
     */
    @SneakyThrows
    public static String decrypt(final String data) {
        if (Strings.isNullOrEmpty(data)) {
            return "";
        }
        if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
            // 数据库敏感字段不能用动态IV加密，否则不支持搜索
            throw new IllegalArgumentException("请指定16位固定IV");
//            // 将 data 分割成 IV 和密文
//            final byte[] bytes = Hex.decodeHex(data);
////            final byte[] bytes = org.apache.commons.codec.binary.Base64.decodeBase64(data);
//            final byte[] ivBytes = new byte[IV_LENGTH];
//            final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
//            System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
//            System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);
//
//            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//            final String result = new String(cipher.doFinal(dataBytes));
//            if (log.isDebugEnabled()) {
//                log.debug("{}({}) => {}", data, new String(ivBytes), result);
//            }
//            return result;
        }
        // 使用固定的 IV 解密
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
        final String result = new String(cipher.doFinal(Hex.decodeHex(data)));
//        final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data)));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", data, result);
        }
        return result;
    }

    public static void main(String[] args) {
        AesConvert.setEnabled(true);
        AesConvert.setSecretKey("Hc7yoFQb00R5Uto69YCRDW4pr9cjSvje");
        AesConvert.setIV("t4WI85mPLC4pMUd3");
        System.out.println(AesConvert.decrypt("d62e71ef5f3e16fe3971fb787c7bb3b5"));
        System.out.println(AesConvert.decrypt("05dd30b6260072ff5cf731fa24d0a1c2"));
    }
}
