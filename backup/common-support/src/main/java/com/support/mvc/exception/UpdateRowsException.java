package com.support.mvc.exception;

import com.utils.enums.Code;

/**
 * 自定义异常: 数据更新异常
 *
 * @author 谢长春 2016-11-23
 */
public class UpdateRowsException extends RuntimeException {

    /**
     * 断言是否抛出数据更新异常，数据变更行数必须为1
     *
     * @param rows long 影响行数
     */
    public static void asserts(long rows) {
        if (1 != rows) {
            throw Code.A00004.toCodeException("数据变更失败，影响行数:%d。 操作数据不存在或数据已过期，调用列表查询接口刷新数据之后再操作。", rows);
        }
    }

    public static void asserts(int rows) {
        asserts((long) rows);
    }

    /**
     * 断言是否抛出数据更新异常，数据变更行数必须 = count
     *
     * @param rows  long 影响行数
     * @param count long 预计更新行数
     */
    public static void asserts(long rows, long count) {
        if (count != rows) {
            throw Code.A00004.toCodeException("数据变更失败，影响行数:%d，预期更新行数%d。 操作数据不存在或数据已过期，调用列表查询接口刷新数据之后再操作。", rows, count);
        }
    }

    /**
     * 断言是否抛出数据更新异常，数据变更行数必须 = count
     *
     * @param rows  long 影响行数
     * @param count int 预计更新行数
     */
    public static void asserts(long rows, int count) {
        asserts(rows, (long) count);
    }

    /**
     * 断言是否为批量更新
     * 断言是否抛出批量更新异常，数据变更行数必须大于0
     *
     * @param rows long 影响行数
     */
    public static void batch(long rows) {
        if (rows > 0) {
            throw Code.A00004.toCodeException("批量数据变更失败，影响行数:%d。 操作数据不存在或数据已过期，调用列表查询接口刷新数据之后再操作。", rows);
        }
    }

    public static void batch(int rows) {
        batch((long) rows);
    }

    public UpdateRowsException() {
        super();
    }

    public UpdateRowsException(String msg) {
        super(msg);
    }

//    public UpdateRowsException(String msg, Throwable cause) {
//        super(msg, cause);
//    }
//
//    public UpdateRowsException(Throwable cause) {
//        super(cause);
//    }
}
