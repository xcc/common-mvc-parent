package com.support.annotations;

import java.lang.annotation.*;

/**
 * 实体类 JSON 序列化或反序列化时自动加解密
 *
 * @author 谢长春 2022-01-31
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AesEncrypt {

    /**
     * @return boolean
     */
    boolean value() default true;

}
