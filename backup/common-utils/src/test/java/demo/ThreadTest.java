package demo;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

@Slf4j
public class ThreadTest {
    private static final ExecutorService executorService = new ThreadPoolExecutor(
            8,
            8 * 4,
            0L,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(8192),
            new ThreadFactoryBuilder().setNameFormat("thread-%d").build(),
            new ThreadPoolExecutor.AbortPolicy()
    );

    @SneakyThrows
    public static void main(String[] args) {
        Callable<String> callable500 = () -> {
            for (int i = 1; i < 10; i++) {
                System.out.println(Thread.currentThread().getId() + ": callable500 : " + i);
                TimeUnit.MILLISECONDS.sleep(500);
            }
            return "end callable500";
        };
        Callable<String> callable1000 = () -> {
            for (int i = 1; i < 10; i++) {
                System.out.println(Thread.currentThread().getId() + ": callable1000 : " + i);
                TimeUnit.MILLISECONDS.sleep(1000);
            }
            return "end callable1000";
        };
        Callable<String> callable1500 = () -> {
            for (int i = 1; i < 10; i++) {
                System.out.println(Thread.currentThread().getId() + ": callable1500 : " + i);
                TimeUnit.MILLISECONDS.sleep(1500);
            }
            return "end callable1500";
        };

        List<Future<String>> futures = executorService.invokeAll(Arrays.asList(callable500, callable1000, callable1500));
        for (Future<String> future : futures) {
            System.out.println(future.get());
        }
        executorService.shutdown();
    }
}
