package demo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Sets;
import com.utils.aes.Aes;
import com.utils.aes.IvRandomRule;
import com.utils.aes.convert.*;
import com.utils.util.Util;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author 谢长春 2018/12/5 .
 */
public class AesJsonConvertTest {
    public static void main(String[] args) {
        Aes.setSecretKey(Util.uuid32());
        Aes.setIvRandomRule(IvRandomRule.dateAlphanumeric);
        Row row = Row.builder()
                .id(RandomUtils.nextLong(0, 10000))
                .idArray(new Long[]{RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000)})
                .idList(Arrays.asList(RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000)))
                .idSet(Sets.newHashSet(RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000)))
                .name("谢CC")
                .nameArray(new String[]{RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10)})
                .nameList(Arrays.asList(RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10)))
                .nameSet(Sets.newHashSet(RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10)))
                .build();
        final String jsonText = JSON.toJSONString(row);
        System.out.println(jsonText);
        System.out.println(JSON.parseObject(jsonText, Row.class));
//        System.out.println(JSON.toJSONString(row, new SerializeConfig()
//                .clearSerializers();
//        ));
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @Builder
    public static class Row {
        @JSONField(serializeUsing = AesLongJsonConvert.Serializer.class, deserializeUsing = AesLongJsonConvert.Deserializer.class)
        private Long id;
        @JSONField(serializeUsing = AesLongArrayJsonConvert.Serializer.class, deserializeUsing = AesLongArrayJsonConvert.Deserializer.class)
        private Long[] idArray;
        @JSONField(serializeUsing = AesLongListJsonConvert.Serializer.class, deserializeUsing = AesLongListJsonConvert.Deserializer.class)
        private List<Long> idList;
        @JSONField(serializeUsing = AesLongSetJsonConvert.Serializer.class, deserializeUsing = AesLongSetJsonConvert.Deserializer.class)
        private Set<Long> idSet;

        @JSONField(serializeUsing = AesStringJsonConvert.Serializer.class, deserializeUsing = AesStringJsonConvert.Deserializer.class)
        private String name;
        @JSONField(serializeUsing = AesStringArrayJsonConvert.Serializer.class, deserializeUsing = AesStringArrayJsonConvert.Deserializer.class)
        private String[] nameArray;
        @JSONField(serializeUsing = AesStringListJsonConvert.Serializer.class, deserializeUsing = AesStringListJsonConvert.Deserializer.class)
        private List<String> nameList;
        @JSONField(serializeUsing = AesStringSetJsonConvert.Serializer.class, deserializeUsing = AesStringSetJsonConvert.Deserializer.class)
        private Set<String> nameSet;
    }

}
