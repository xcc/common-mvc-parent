package com.utils.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.utils.enums.Image;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Objects;

/**
 * 二维码生成；依赖 google.zxing 库
 *
 * @author 谢长春 on 2018/1/29 .
 */
@Slf4j
@RequiredArgsConstructor(staticName = "of")
@Getter
@Setter
@ToString
@Accessors(chain = true)
public final class QRCode {

    /**
     * 需要生成二维码的内容
     */
    private final String text;
    /**
     * 二维码宽度
     */
    private int width = 200;
    /**
     * 二维码高度
     */
    private int height = 200;
    /**
     * 图片类型
     */
    private Image type = Image.JPEG;

    @SneakyThrows
    public BufferedImage generate() {
        final BitMatrix matrix = new MultiFormatWriter()
                .encode(text,
                        BarcodeFormat.QR_CODE,
                        width,
                        height,
                        new Hashtable<EncodeHintType, Object>() {{
                            put(EncodeHintType.CHARACTER_SET, "utf-8");
                            put(EncodeHintType.MARGIN, 0);
                        }}
                );
        final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
            }
        }
        return image;
    }

    @SneakyThrows
    public File write(final File file) {
        Objects.requireNonNull(file, "参数【file】是必须的");
        if (!ImageIO.write(generate(), type.name(), file)) {
            throw new IOException(String.format("Could not write an image of format %s to %s", type.name(), file.getAbsolutePath()));
        }
        return file;
    }

    @SneakyThrows
    public void write(final OutputStream stream) {
        Objects.requireNonNull(stream, "参数【stream】是必须的");
        if (!ImageIO.write(generate(), type.name(), stream)) {
            throw new IOException("Could not write an image of format ".concat(type.name()));
        }
    }

    @SneakyThrows
    public String base64() {
        @Cleanup ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(generate(), type.name(), byteArrayOutputStream);
        return type.base64(Base64.encoder(byteArrayOutputStream.toByteArray()));
    }

    public static void main(String[] args) {
        try {
            System.out.println(
                    QRCode.of("http://github.com")
                            .setHeight(100)
                            .setWidth(100)
                            .setType(Image.JPEG)
                            .write(
                                    Paths.get("logs", "二维码.jpeg").toAbsolutePath().toFile()
                            )
                            .getAbsolutePath()
            );
            System.out.println(
                    QRCode.of("http://github.com")
                            .setHeight(100)
                            .setWidth(100)
                            .setType(Image.PNG)
                            .write(
                                    Paths.get("logs", "二维码.png").toAbsolutePath().toFile()
                            )
                            .getAbsolutePath()
            );
            System.out.println(
                    QRCode.of("http://github.com")
                            .setHeight(100)
                            .setWidth(100)
                            .setType(Image.JPEG)
                            .base64()
            );
            System.out.println(
                    QRCode.of("http://github.com")
                            .setHeight(100)
                            .setWidth(100)
                            .setType(Image.PNG)
                            .base64()
            );
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
