package com.utils.enums;

import com.utils.ICode;

/**
 * 全局响应代码，异常响应代码: 限制区间在 A00000 - A00999
 * 定义 com.support.mvc.entity.base.Result#setCode(Code) 返回编码
 *
 * @author 谢长春 2019-1-9
 */
public enum Code implements ICode {
    A00000("成功"),
    A00001("失败"),
    /**
     * 自定义消息提示，代码执行时指定 message 内容，一般用于抛出带动态参数消息，可直接在前端弹窗
     */
    A00002("-"),
    A00003("非用户操作错误，请联系系统管理员并提供错误代码"),
    A00004("页面数据已过期，请刷新之后再操作"),
    A00005("文件大小超过限制"),
    A00006("无操作权限"),
    A00008("分页查询，每页显示数据量超过最大值"),
    A00009("上传文件列表为空"),
    A00010("无权限使用该客户端"),
    ;
    /**
     * 枚举属性说明
     */
    public final String comment;

    Code(String comment) {
        this.comment = comment;
    }

    @Override
    public String getComment() {
        return this.comment;
    }

    //    /**
    //     * 通过 Code 构建 Result 对象；注：只能构建 Result<Object>，若要指定泛型，请使用 new Result<?> 指定泛型
    //     *
    //     * @return Result<Object>
    //     */
    //    public <E> Result<E> toResult() {
    //        return new Result<>(this);
    //    }
    //
    //    /**
    //     * 通过 Code 构建 Result 对象；注：只能构建 Result<Object>，若要指定泛型，请使用 new Result<?> 指定泛型
    //     *
    //     * @param exception String 异常消息，可选参数，
    //     * @return Result<Object>
    //     */
    //    public <E> Result<E> toResult(final String exception) {
    //        return new Result<E>(this).setException(exception);
    //    }
    //
    //    /**
    //     * 构造并返回 CodeException
    //     *
    //     * @param exception String 异常消息内容
    //     * @return CodeException
    //     */
    //    public CodeException exception(final String exception) {
    //        return new CodeException(this, exception);
    //    }
    //
    //    /**
    //     * 构造并返回 CodeException
    //     *
    //     * @param exception String 异常消息内容
    //     * @param throwable Throwable 异常堆栈
    //     * @return CodeException
    //     */
    //    public CodeException exception(final String exception, final Throwable throwable) {
    //        return new CodeException(this, exception, throwable);
    //    }
    //
    //    public static void main(String[] args) {
    //        { // 构建 js 枚举文件
    //            val name = "枚举：响应状态码";
    //            StringBuilder sb = new StringBuilder();
    //            sb.append("/**\n")
    //                    .append(" * ").append(name).append("\n")
    //                    .append(String.format(" * Created by 谢长春 on %s.%n", Dates.now().formatDate()))
    //                    .append(" */\n");
    //            sb.append("// 枚举值定义").append("\n");
    //            sb.append("const status = Object.freeze({").append("\n");
    //            Stream.of(Code.values()).forEach(item -> sb.append(
    //                    "\t{name}: {value: '{name}', comment: '{comment}'},"
    //                            .replace("{name}", item.name())
    //                            .replace("{comment}", item.comment)
    //                    ).append("\n")
    //            );
    //            sb.append("\tgetComment:function(key){return (this[key]||{}).comment}\n});").append("\n");
    //            sb.append("// 枚举值转换为选项集合").append("\n");
    //            sb.append("const options = [").append("\n");
    //            Stream.of(Code.values()).forEach(item -> sb.append(
    //                    "\t{value: status.{name}.value, label: status.{name}.comment},"
    //                            .replace("{name}", item.name())
    //                    ).append("\n")
    //            );
    //            sb.append("];").append("\n");
    //            sb.append("export default status;");
    //            System.out.println("JS文件输出路径：\n" +
    //                    FWrite.of("logs", Code.class.getSimpleName().concat(".js"))
    //                            .write(sb.toString())
    //                            .getAbsolute().orElse(null)
    //            );
    //        }
    //
    //        System.out.println(
    //                Stream.of(Code.values())
    //                        .map(item -> String.format("%s【%s】", item.name(), item.comment))
    //                        .collect(Collectors.joining("|"))
    //        );
    //    }
}
