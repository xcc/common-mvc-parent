package com.utils.aes.convert;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.utils.aes.Aes;
import com.utils.enums.Code;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Objects;

/**
 * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesStringJsonConvert.Serializer.class, deserializeUsing = AesStringJsonConvert.Deserializer.class)
 *
 * @author 谢长春 2022-02-06
 */
@Slf4j
public class AesStringJsonConvert {
    public static class Serializer implements ObjectSerializer {

        @Override
        public void write(JSONSerializer serializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
            if (Objects.isNull(value)) {
                return;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}:{}", fieldName, value);
            }
            serializer.write(Aes.encrypt(Objects.toString(value)));
        }
    }

    public static class Deserializer implements ObjectDeserializer {
        @SuppressWarnings({"unchecked"})
        @Override
        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
            final String value = defaultJSONParser.parseObject(String.class);
            if (Objects.isNull(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}:{}", fieldName, value);
            }
            try {
                final Object val = Aes.decrypt(value);
                return (T) val;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw Code.A00003.toCodeException(value);
            }
        }

        @Override
        public int getFastMatchToken() {
            return JSONToken.LITERAL_STRING;
        }
    }

}
