package com.ccx.demo.feign.base.vo;

/**
 * 基础服务：埋点实体规范
 */
public interface ITabErrorLog {

    /**
     * 数据ID，主键自增
     */
    void setId(Long id);

    /**
     * 链路追踪id
     */
    void setTraceId(String traceId);

    /**
     * 日志内容
     */
    void setMessage(String message);

    /**
     * 错误发生时间，格式：yyyyMMddHHmmss
     */
    void setCreateTime(String createTime);

    /**
     * 创建用户ID
     */
    void setCreateUserId(Long createUserId);

    /**
     * 处理时间，yyyyMMddHHmmssSSS
     */
    void setUpdateTime(String updateTime);

    /**
     * 修改用户ID
     */
    void setUpdateUserId(Long updateUserId);

    /**
     * 是否逻辑删除。 [0:否,1:是]
     */
    void setDeleted(Boolean deleted);
}
