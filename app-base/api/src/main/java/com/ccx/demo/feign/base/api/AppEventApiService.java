package com.ccx.demo.feign.base.api;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.Result;
import com.ccx.demo.feign.base.dto.ApiAppEventDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 埋点服务调用
 *
 * @author 谢长春 2022-11-06
 */
@AppEventRequest(skip = true)
@ConditionalOnProperty(value = "spring.app.feign.base.enabled", havingValue = "true")
@FeignClient(name = "appEventApiService", url = "${spring.app.feign.base.api}/feign/app-event")
public interface AppEventApiService {

    /**
     * 埋点保存
     *
     * @param dto {@link ApiAppEventDTO}
     * @return {@link Result}
     */
    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<Void> insert(@RequestBody final ApiAppEventDTO dto);

}
