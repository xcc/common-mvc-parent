package com.ccx.demo.feign.base.service;

import com.ccx.demo.feign.base.dto.ApiAppEventDTO;
import com.ccx.demo.feign.base.dto.ApiErrorLogDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;

/**
 * 挡板：基础服务调用包装
 *
 * @author 谢长春 2022-11-06
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConditionalOnMissingBean(BaseServiceEnabledImpl.class) // 且 user 服务开关未打开时才加载挡板实现
public class BaseServiceDisabledImpl implements IBaseService {

    @PostConstruct
    private void postConstruct() {
        log.warn("spring.app.feign.base.enabled 配置项开关未开启，加载挡板逻辑实现");
    }

    /**
     * 埋点保存
     *
     * @param dto {@link ApiAppEventDTO}
     */
    @Override
    public void insertAppEvent(ApiAppEventDTO dto) {
        log.warn("spring.app.feign.base.enabled 配置项开关未开启");
    }

    /**
     * 异常日志记录保存
     *
     * @param traceId String {@link ApiErrorLogDTO#getTraceId()} 链路追踪id
     * @param message String {@link ApiErrorLogDTO#getMessage()} 日志内容
     */
    @Override
    public void insertErrorLog(final String traceId, final String message) {
        log.warn("spring.app.feign.base.enabled 配置项开关未开启");
    }

}
