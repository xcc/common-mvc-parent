package com.ccx.demo.feign.base.service;

import com.ccx.demo.feign.base.dto.ApiAppEventDTO;
import com.ccx.demo.feign.base.dto.ApiErrorLogDTO;

/**
 * 接口：用户服务调用包装
 *
 * @author 谢长春 2022-11-06
 */
public interface IBaseService {
    /**
     * 埋点保存
     *
     * @param dto {@link ApiAppEventDTO}
     */
    void insertAppEvent(final ApiAppEventDTO dto);

    /**
     * 异常日志记录保存
     *
     * @param traceId String {@link ApiErrorLogDTO#getTraceId()} 链路追踪id
     * @param message String {@link ApiErrorLogDTO#getMessage()} 日志内容
     */
    void insertErrorLog(final String traceId, final String message);
}
