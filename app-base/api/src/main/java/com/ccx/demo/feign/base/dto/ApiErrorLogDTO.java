package com.ccx.demo.feign.base.dto;

import com.ccx.demo.feign.base.vo.ITabErrorLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;

import javax.validation.constraints.Size;

/**
 * DTO：异常日志记录
 *
 * @author 谢长春 on 2022-02-16
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "异常日志记录")
public class ApiErrorLogDTO implements ITabErrorLog, Cloneable {
    private static final long serialVersionUID = 1L;
    /**
     * 链路追踪id
     */
    @Size(max = 64)
    @ApiModelProperty(value = "链路追踪id", position = 2)
    private String traceId;
    /**
     * 日志内容
     */
    @Size(max = 262140)
    @ApiModelProperty(value = "日志内容", position = 3)
    private String message;

    @SneakyThrows
    public ApiErrorLogDTO cloneObject() {
        return (ApiErrorLogDTO) super.clone();
    }

    @Deprecated
    @Override
    public void setId(Long id) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setCreateTime(String createTime) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setCreateUserId(Long createUserId) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setUpdateTime(String updateTime) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setUpdateUserId(Long updateUserId) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setDeleted(Boolean deleted) {
        // 不需要的属性
    }
}
