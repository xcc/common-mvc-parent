package com.ccx.demo.feign.base.api;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.Result;
import com.ccx.demo.feign.base.dto.ApiErrorLogDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 异常日志记录服务调用
 *
 * @author 谢长春 2022-11-06
 */
@AppEventRequest(skip = true)
@ConditionalOnProperty(value = "spring.app.feign.base.enabled", havingValue = "true")
@FeignClient(name = "errorLogApiService", url = "${spring.app.feign.base.api}/open/error-log")
public interface ErrorLogApiService {

    /**
     * 异常日志记录保存
     *
     * @param dto {@link ApiErrorLogDTO}
     * @return {@link Result}
     */
    @PostMapping(value = "/{traceId}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<Void> insert(@PathVariable("traceId") final String traceId, @RequestBody final String message);

}
