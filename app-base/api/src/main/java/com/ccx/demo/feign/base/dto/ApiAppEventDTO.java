package com.ccx.demo.feign.base.dto;

import app.encrypt.domain.starter.annotations.AesIdLongJsonConverter;
import app.enums.starter.AppEventKey;
import com.ccx.demo.feign.base.vo.ITabAppEvent;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 暴露给外部操作的埋点属性
 *
 * @author 谢长春 2022-11-06
 */
@ApiModel(description = "埋点")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ApiAppEventDTO implements ITabAppEvent {

    /**
     * 数据ID，主键自增
     */
    @AesIdLongJsonConverter
    @ApiModelProperty(value = "数据ID", position = 1)
    private Long id;
    /**
     * 埋点事件，参考：AppEventKey
     */
    @ApiModelProperty(value = "埋点事件，参考：AppEventKey", position = 2)
    private AppEventKey event;
    /**
     * 埋点时间，yyyyMMddHHmmss
     */
    @ApiModelProperty(value = "埋点时间，yyyyMMddHHmmss", position = 3)
    private String eventTime;
    /**
     * 埋点附加数据
     */
    @ApiModelProperty(value = "埋点附加数据", position = 4)
    private String data;
    /**
     * 用户ID。 tab_user.id
     */
    @AesIdLongJsonConverter // id 使用 aes 加密
    @ApiModelProperty(value = "用户ID。 tab_user.id", position = 5)
    private Long userId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    @ApiModelProperty(value = "埋点关联数据id，用于简单业务埋点", position = 6)
    private Long refLongId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    @ApiModelProperty(value = "埋点关联数据id，用于简单业务埋点", position = 7)
    private String refStringId;
}
