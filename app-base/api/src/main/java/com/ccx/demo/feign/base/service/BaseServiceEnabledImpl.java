package com.ccx.demo.feign.base.service;

import app.common.starter.event.AppEvent;
import app.common.starter.event.ErrorLogEvent;
import com.ccx.demo.feign.base.api.AppEventApiService;
import com.ccx.demo.feign.base.api.ErrorLogApiService;
import com.ccx.demo.feign.base.dto.ApiAppEventDTO;
import com.ccx.demo.feign.base.dto.ApiErrorLogDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;

/**
 * 实现：基础服务调用包装
 *
 * @author 谢长春 2022-11-06
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConfigurationProperties("spring.app.feign.base")
@ConditionalOnProperty(value = "spring.app.feign.base.enabled", havingValue = "true") // user 服务开关打开时才加载具体实现
public class BaseServiceEnabledImpl implements IBaseService {

    /**
     * 基础服务调用开关；true：开，false：关，关闭之后会启用挡板
     */
    @Setter
    @Getter
    private boolean enabled = true;
    /**
     * 基础服务API地址，默认值：http://app-base:8080
     */
    @Setter
    @Getter
    private String api = "http://app-base:8080";

    private final AppEventApiService appEventApiService;
    private final ErrorLogApiService errorLogApiService;

    @PostConstruct
    private void postConstruct() {
        log.info("spring.app.feign.base.enabled: {}", enabled);
        log.info("spring.app.feign.base.api: {}", api);
    }

    @Async
    @EventListener
    @SneakyThrows
    public void listenerAppEvent(AppEvent event) {
//        if (event.getEventKey().isClick()) { // 点击数只存在 redis , 不写 mysql
//            clickEventCache.increment(
//                    event.getEventKey()
//                    , Optional.ofNullable(event.getRefLongId())
//                            .map(Objects::toString)
//                            .orElseGet(event::getRefStringId)
//                    , event.getUserId()
//            );
//            return;
//        }
//        final TabAppEvent obj = new TabAppEvent();
////        obj.setAppName(event.getAppName());
//        obj.setEvent(event.getEventKey());
//        obj.setRefStringId(event.getRefStringId());
//        obj.setRefLongId(event.getRefLongId());
//        obj.setUserId(event.getUserId());
//        obj.setData(event.getData());
//        repository.insert(event.getUserId(), obj);
        try {
            final ApiAppEventDTO dto = new ApiAppEventDTO();
            dto.setEvent(event.getEventKey());
            dto.setUserId(event.getUserId());
            dto.setRefLongId(event.getRefLongId());
            dto.setRefStringId(event.getRefStringId());
            dto.setData(event.getData());
            insertAppEvent(dto);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 注册 ErrorLogEvent 事件监听实现方法
     *
     * @param event ErrorLogEvent
     */
    @Async
    @EventListener
    @SneakyThrows
    public void listenerErrorLogEvent(final ErrorLogEvent event) {
        try {
            insertErrorLog(event.getTraceId(), event.getAppName() + ":" + event.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 埋点保存
     *
     * @param dto {@link ApiAppEventDTO}
     */
    @Override
    public void insertAppEvent(ApiAppEventDTO dto) {
        try {
            appEventApiService.insert(dto).assertSuccess();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 异常日志记录保存
     *
     * @param traceId String {@link ApiErrorLogDTO#getTraceId()} 链路追踪id
     * @param message String {@link ApiErrorLogDTO#getMessage()} 日志内容
     */
    @Override
    public void insertErrorLog(final String traceId, final String message) {
        try {
            errorLogApiService.insert(traceId, message).assertSuccess();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
