package com.ccx.demo.feign.base.vo;

import app.enums.starter.AppEventKey;

/**
 * 基础服务：埋点实体规范
 *
 * @author 谢长春 2022-11-06
 */
public interface ITabAppEvent {

    /**
     * 数据ID，主键自增
     */
    void setId(Long id);

    /**
     * 埋点事件，参考：AppEventKey
     */
    void setEvent(AppEventKey event);

    /**
     * 埋点时间，yyyyMMddHHmmss
     */
    void setEventTime(String eventTime);

    /**
     * 埋点附加数据
     */
    void setData(String data);

    /**
     * 用户ID。 tab_user.id
     */
    void setUserId(Long userId);

    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    void setRefLongId(Long refLongId);

    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    void setRefStringId(String refStringId);
}
