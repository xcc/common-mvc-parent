package com.ccx.demo.business.base.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabErrorLog is a Querydsl query type for TabErrorLog
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabErrorLog extends EntityPathBase<TabErrorLog> {

    private static final long serialVersionUID = -1553351587L;

    public static final QTabErrorLog tabErrorLog = new QTabErrorLog("tabErrorLog");

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Long> createUserId = createNumber("createUserId", Long.class);

    public final BooleanPath deleted = createBoolean("deleted");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath message = createString("message");

    public final StringPath traceId = createString("traceId");

    public final StringPath updateTime = createString("updateTime");

    public final NumberPath<Long> updateUserId = createNumber("updateUserId", Long.class);

    public QTabErrorLog(String variable) {
        super(TabErrorLog.class, forVariable(variable));
    }

    public QTabErrorLog(Path<? extends TabErrorLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabErrorLog(PathMetadata metadata) {
        super(TabErrorLog.class, metadata);
    }

}

