package com.ccx.demo.business.base.controller;

import app.common.starter.entity.Result;
import app.security.starter.vo.IUser;
import com.ccx.demo.business.base.service.AppEventService;
import com.ccx.demo.feign.base.dto.ApiAppEventDTO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 对外接口：埋点表 V20221105
 *
 * @author 谢长春 on 2022-06-27
 */
@Api(tags = "埋点表")
//@ApiSort() // 控制接口排序
@RequestMapping("/feign/app-event")
@Controller
@Slf4j
@RequiredArgsConstructor
public class FeignAppEventController {

    private final AppEventService service;

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "新增埋点表"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 4, author = "谢长春")
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final ApiAppEventDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.insert(body, user.optionalUserId().orElse(0L));
        });
    }
}
