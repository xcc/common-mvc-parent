package com.ccx.demo.business.base.controller;

import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.common.starter.util.JSON;
import app.enums.starter.TokenClient;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.ccx.demo.business.base.enums.AppCode;
import com.ccx.demo.feign.user.api.UserApiService;
import com.ccx.demo.feign.user.dto.ApiUserDTO;
import com.ccx.demo.feign.user.vo.ApiUserVO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmss;

/**
 * 开发环境支撑
 *
 * @author 谢长春 on 2017/11/13.
 */
@Api(tags = "开发环境支撑")
@ApiSort(0)
@RequestMapping("/open/dev")
@Controller
@Slf4j
@RequiredArgsConstructor
@ConditionalOnExpression("!'prod'.equals('${spring.app.env}')") // 生产环境不初始化这个 controller
public class OpenDevController {
    @VueSwaggerGroup
    @AppSwaggerGroup
    @WechatSwaggerGroup
    @GetMapping("/enums")
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ApiOperation(value = "查看应用所有枚举值说明"
            , tags = {""}
            , notes = ""
    )
    @ResponseBody
    public Result<Map<String, Map<String, String>>> enums(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Map<String, Map<String, String>>>().execute(result -> {
//            final HashBasedTable<String, String, String> table = HashBasedTable.create();
//            for (TokenClient ele : TokenClient.values()) {
//                table.put(TokenClient.class.getSimpleName(), ele.name(), ele.comment);
//            }
            final Map<String, Map<String, String>> table = Maps.newHashMap();
            {
                table.put(TokenClient.class.getSimpleName(), Maps.newLinkedHashMap());
                for (TokenClient ele : TokenClient.values()) {
                    table.get(TokenClient.class.getSimpleName()).put(ele.name(), ele.comment);
                }
            }
            {
                table.put(Code.class.getSimpleName(), Maps.newLinkedHashMap());
                for (Code ele : Code.values()) {
                    table.get(Code.class.getSimpleName()).put(ele.name(), ele.comment);
                }
                for (AppCode ele : AppCode.values()) {
                    table.get(Code.class.getSimpleName()).put(ele.name(), ele.comment);
                }
            }
            result.setSuccess(table);
        });
    }

    @GetMapping("/headers")
    @ResponseBody
    public Result<LinkedHashMap<String, String>> headers(
            @ApiIgnore @AuthenticationPrincipal final IUser user
            , final HttpServletRequest request
    ) {
        return new Result<LinkedHashMap<String, String>>().execute(result -> {
            final Enumeration<String> headerNames = request.getHeaderNames();
            final LinkedHashMap<String, String> map = new LinkedHashMap<>();
            while (headerNames.hasMoreElements()) {
                String key = headerNames.nextElement();
                map.put(key, request.getHeader(key));
            }
            HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            map.put("req.getRequestURI", req.getRequestURI());
            map.put("req.getRemoteHost", req.getRemoteHost());
            map.put("req.getRemoteAddr", req.getRemoteAddr());
            result.setSuccess(map);
        });
    }

    private final UserApiService userApiService;

    @GetMapping("/feign-user-test")
    @ResponseBody
    public Result<Void> feignUserTest(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Void>().execute(result -> {
            {
                final ApiUserDTO condition = new ApiUserDTO();
//                condition.setCreateUserId(1001L);
                final Result<ApiUserVO> page = userApiService.page(1, 10, condition);
                log.info("分页查询（禁用 AES 加解密注解）: {}", JSON.toJsonString(page));
                log.info("分页查询（启用 AES 加解密注解）: {}", JSON.toJsonStringAesConvert(page));
//                result.setSuccess(page);
            }
            {
                final ApiUserDTO insertDTO = new ApiUserDTO();
                final String username = RandomStringUtils.randomNumeric(8);
                insertDTO.setUsername("187" + username);
                insertDTO.setPhone("187" + username);
                insertDTO.setNickname("187****" + StringUtils.right(username, 4));
                insertDTO.setPassword("lzzrMysb72zOiC5iFtDlTflpclMbj4KCYEa73+8I6J5qG5LI/kXn4DozaexAUgzxZWUpoZIvbgNYtLwwKnsl5axut/Iyj9WA3+YN+424LOD6TP6WePBZ2pTVsuJmgVb6XAmoKJfq7TayleM7jNo686IPJVOsOxMm8T+ZmFTlO4x1TpV6E7sDYvQzBfKZABpnExo00aC7lheTL670n+KZ/RdO9+ItBPr+j3Ko1PXfYGBZ1KW/XpTAs0EOQ45HC2VFg364i+5WiZzQfMs6FIDCspVPgeoYknK2t+aciMRUF5AnKt8HcAPWRQP3NL4EXgi1j6qzcGjPDIc1YkeIndk2Lw==");
                insertDTO.setRoles(new String[]{"5j3m2ypc1y61GZGSOO"});
                final Result<Void> insertResult = userApiService.insert(insertDTO);
                log.info("新增: {}", JSON.toJsonString(insertResult));
//                result.setSuccess(insertResult);
            }
            {
                final ApiUserDTO updateDTO = JSON.parseObject("{\"id\": 1004, \"username\": \"18700000001\", \"nickname\": \"187****0001\", \"phone\": \"18700000001\", \"oldPhone\": \"\", \"email\": \"\", \"roles\": [\"5j3m2ypc1y61GZGSOO\"], \"createTime\": \"20221111092637\", \"createUserId\": 1001, \"updateTime\": \"20221111092637911\", \"updateUserId\": 1001, \"hidden\": false, \"disabled\": false, \"deleted\": false, \"createUserNickname\": \"普通用户\", \"updateUserNickname\": \"普通用户\", \"createTimeFormat\": \"2022-11-11 09:26:37\", \"updateTimeFormat\": \"2022-11-11 09:26:37.911\", \"roleNames\": [\"APP端、微信端用户\"], \"dv\": \"WndkZmI4cGJkUGdrZ0s0TVlRPOX5RUibLcQM0DcP-WPHml0vZSk3WDdQCl1R9RdC\"}", ApiUserDTO.class);
                updateDTO.setNickname("编辑昵称：" + yyyyMMddHHmmss.now());
                final Result<Void> updateResult = userApiService.update(updateDTO);
                log.info("修改: {}", JSON.toJsonString(updateResult));
//                result.setSuccess(updateResult);
            }
            {
                final Result<Void> markDeleteResult = userApiService.markDeleteByIds(new Long[]{1011L, 1012L});
                log.info("按 ID 批量逻辑删除: {}", JSON.toJsonString(markDeleteResult));
//                result.setSuccess(markDeleteResult);
            }
            {
                final Result<Void> markDeleteResult = userApiService.markDeleteByDvs(new String[]{
                        "bWRvWGFlVFU1SmJSeFN3Ze-Ij_TBQMg6A86NQT8vvdCU7aQ-gUoOcavQzCQQ5-JN",
                        "c2RxMjVYek9ud0wzSmFyQ8HVHmgP8CnxpXqg6sH-oetAcEjIIhajufqdQMSm4VOR",
                });
                log.info("按 DV 批量逻辑删除: {}", JSON.toJsonString(markDeleteResult));
                result.setSuccess(markDeleteResult);
            }
        });
    }
}
