package com.ccx.demo.business.base.entity;

import app.common.starter.entity.OrderBy;
import app.common.starter.enums.Code;
import app.common.starter.interfaces.ITable;
import app.common.starter.util.Dates;
import app.common.starter.util.JSON;
import app.dao.jpa.starter.where.QdslWhere;
import app.encrypt.domain.starter.annotations.AesIdLongJsonConverter;
import app.enums.starter.AppEventKey;
import com.ccx.demo.feign.base.vo.ITabAppEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.QueryEntity;
import com.querydsl.core.annotations.QueryTransient;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmss;
import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 实体类：埋点表 V20221105
 *
 * @author 谢长春 on 2022-06-27
 */
@Table(name = "tab_app_event")
@Entity
@QueryEntity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@ToString
@ApiModel(description = "埋点表")

public class TabAppEvent implements
        ITable<Void, QdslWhere> // 所有与数据库表 - 实体类映射的表都实现该接口；方便后续一键查看所有表的实体
        , ITabAppEvent
        // JPAUpdateClause => 需要的动态更新字段；采用 方案2 时需要实现该接口
        // QdslWhere       => 动态查询条件
{
    private static final long serialVersionUID = 1L;

    /**
     * 数据ID，主键自增
     */
    @AesIdLongJsonConverter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Positive
    @ApiModelProperty(value = "数据ID", position = 1)
    private Long id;
    /**
     * 埋点事件，参考：AppEventKey
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "event")
    @ApiModelProperty(value = "埋点事件，参考：AppEventKey", position = 2)
    private AppEventKey event;
    /**
     * 埋点时间，yyyyMMddHHmmss
     */
    @Column(name = "eventTime")
    @Size(min = 14, max = 14)
    @ApiModelProperty(value = "埋点时间，yyyyMMddHHmmss", position = 3)
    private String eventTime;
    /**
     * 埋点附加数据
     */
    @Column(name = "data")
    @ApiModelProperty(value = "埋点附加数据", position = 4)
    private String data;
    /**
     * 用户ID。 tab_user.id
     */
    @AesIdLongJsonConverter // id 使用 aes 加密
    @Column(name = "userId")
    @DecimalMin("-9223372036854776000")
    @DecimalMax("9223372036854776000")
    @ApiModelProperty(value = "用户ID。 tab_user.id", position = 5)
    private Long userId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    @Column(name = "refLongId")
    @Min(0)
    @DecimalMax("9223372036854776000")
    @ApiModelProperty(value = "埋点关联数据id，用于简单业务埋点", position = 6)
    private Long refLongId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    @Column(name = "refStringId")
    @Size(max = 36)
    @ApiModelProperty(value = "埋点关联数据id，用于简单业务埋点", position = 7)
    private String refStringId;
    ///**
    // * 新增操作人昵称
    // */
    //@Transient
    //@QueryTransient
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY) // 实体类只读方法一定要加禁止反序列化注解
    //@ApiModelProperty(value = "新增操作人昵称", accessMode = READ_ONLY)
    //private String createUserNickname;
    ///**
    // * 更新操作人昵称
    // */
    //@Transient
    //@QueryTransient
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY) // 实体类只读方法一定要加禁止反序列化注解
    //@ApiModelProperty(value = "更新操作人昵称", accessMode = READ_ONLY)
    //private String updateUserNickname;
    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Set<Long> ids;
    /**
     * 排序字段
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(value = "查询排序字段")
    private List<OrderBy> orderBy;

    /**
     * 防止踩坑。
     * 当 JPA 查询对象没有 clone 时，执行 setValue 操作时，会触发更新动作，这里拦截之后抛出异常
     */
    @PreUpdate
    public void onPreUpdate() {
        throw Code.A00003.toCodeException("防止踩坑，谨慎使用JPA默认的 update 方法。 如果业务逻辑确定需要使用 setValue 方式更新对象时，去掉这个异常");
    }

    /**
     * 数据版本号，用于更新和删除参数，规避脏数据更新
     *
     * @return String
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY) // 实体类只读方法一定要加禁止反序列化注解
    @JsonSerialize(converter = app.encrypt.domain.starter.convert.AesDataVersionJsonConverter.Serializer.class)
    @ApiModelProperty(value = "数据版本号，用于更新和删除参数，规避脏数据更新", position = 0, accessMode = READ_ONLY)
    public String getDv() {
        return String.format("%s@%s", id, Optional.ofNullable(getEventTime()).orElse(""));
    }
    ///**
    // * 创建时间格式化，仅用于前端展示
    // *
    // * @return String
    // */
    //@Transient
    //@QueryTransient
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY) // 实体类只读方法一定要加禁止反序列化注解
    //@ApiModelProperty(value = "创建时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
    //public String getCreateTimeFormat() {
    //    return yyyyMMddHHmmss.parseOfNullable(createTime).map(yyyy_MM_dd_HH_mm_ss::format).orElse(null);
    //}

    ///**
    // * 修改时间格式化，仅用于前端展示
    // *
    // * @return String
    // */
    //@Transient
    //@QueryTransient
    //@JsonProperty(access = JsonProperty.Access.READ_ONLY) // 实体类只读方法一定要加禁止反序列化注解
    //@ApiModelProperty(value = "修改时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
    //public String getUpdateTimeFormat() {
    //    return yyyyMMddHHmmssSSS.parseOfNullable(updateTime).map(yyyy_MM_dd_HH_mm_ss_SSS::format).orElse(null);
    //}

    @SneakyThrows
    public TabAppEvent cloneObject() {
        return (TabAppEvent) super.clone();
    }

// Enum Start **********************************************************************************************************

    /**
     * 枚举：定义排序字段
     */
    public enum OrderByColumn {
        // 按 id 排序可替代按创建时间排序
        id(QTabAppEvent.tabAppEvent.id),
//        event(tabAppEvent.event),
//        eventTime(tabAppEvent.eventTime),
//        data(tabAppEvent.data),
//        userId(tabAppEvent.userId),
//        refLongId(tabAppEvent.refLongId),
//        refStringId(tabAppEvent.refStringId),
        ;
        private final ComparableExpressionBase<?> column;

        public OrderBy asc() {
            return new OrderBy().setName(this.name());
        }

        public OrderBy desc() {
            return new OrderBy().setName(this.name()).setDirection(Sort.Direction.DESC);
        }

        /**
         * 获取所有排序字段名
         *
         * @return String[]
         */
        public static String[] names() {
            return Stream.of(OrderByColumn.values()).map(Enum::name).toArray(String[]::new);
        }

        OrderByColumn(final ComparableExpressionBase<?> column) {
            this.column = column;
        }
    }

// Enum End : DB Start *************************************************************************************************

    /**
     * DAO层新增数据时，设置字段默认值
     *
     * @param now 当前时间
     */
    public void insert(final Dates now, final Long userId) {
        this.id = null;
        //this.deleted = Optional.ofNullable(this.deleted).orElse(false);
        this.userId = userId;
        //this.createUserId = userId;
        //this.createTime = now.format(yyyyMMddHHmmss);
        //this.updateUserId = userId;
        //this.updateTime = now.format(yyyyMMddHHmmssSSS);
        //this.event=Optional.ofNullable(this.event).orElse(0); // 埋点事件，参考：AppEvent
        this.eventTime = now.format(yyyyMMddHHmmss); // 埋点时间，yyyyMMddHHmmss
        this.data = Optional.ofNullable(this.data).orElse(null); // 埋点附加数据
        this.userId = Optional.ofNullable(this.userId).orElse(0L); // 用户ID。 tab_user.id
        this.refLongId = Optional.ofNullable(this.refLongId).orElse(0L); // 埋点关联数据id，用于简单业务埋点
        this.refStringId = Optional.ofNullable(this.refStringId).orElse(""); // 埋点关联数据id，用于简单业务埋点
    }

    @Override
    public QdslWhere where() {
        final QTabAppEvent table = QTabAppEvent.tabAppEvent;
        // 构建查询顺序规则请参考：com.support.mvc.entity.IWhere#where
        return QdslWhere.of()
                .andIfNonEmpty(ids, () -> table.id.in(ids))
                .and(id, () -> table.id.eq(id))
                .and(event, () -> table.event.eq(event))
                .and(eventTime, () -> eventTime.endsWith("%") || eventTime.startsWith("%") ? table.eventTime.like(eventTime) : table.eventTime.eq(eventTime))
//                .and(data, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.data, JSON.toJsonString(data)))
                .and(userId, () -> table.userId.eq(userId))
//                .and(refLongId, () -> table.refLongId.eq(refLongId))
//                .and(refStringId, () -> refStringId.endsWith("%") || refStringId.startsWith("%") ? table.refStringId.like(refStringId) : table.refStringId.eq(refStringId))
////                .and(phone, () -> table.phone.eq(phone))
////                .and(createUserId, () -> table.createUserId.eq(createUserId))
////                .and(updateUserId, () -> table.updateUserId.eq(updateUserId))
////                // 强制带默认值的查询字段
////                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
////                // 数字区间查询
////                .and(amountRange, () -> table.amount.between(amountRange.getMin(), amountRange.getMax()))
////                // 日期区间查询；Range.rebuild() : 先将时间区间重置到 00:00:00.000 - 23:59:59.999 ; 大多数情况都需要重置时间
////                .and(createTimeRange, () -> ExpressionUtils.and(createTimeRange.rebuild().ifPresentCreateTimeBegin(table.createTime::goe), createTimeRange.ifPresentCreateTimeEnd(table.createTime::loe)))
////                .and(updateTimeRange, () -> ExpressionUtils.and(updateTimeRange.rebuild().ifPresentUpdateTimeBegin(table.createTime::goe), updateTimeRange.ifPresentUpdateTimeEnd(table.createTime::loe)))
////                // 模糊匹配查询：后面带 % ；建议优先使用
////                .and(name, () -> table.name.startsWith(name)) // 模糊匹配查询：后面带 %
////                .and(name, () -> table.name.endsWith(name)) // 模糊匹配查询：前面带 %
////                .and(name, () -> table.name.contains(name)) // 模糊匹配查询：前后带 %,同 MessageFormat.format("%{0}%", name)
////                .and(name, () -> table.name.like(MessageFormat.format("%{0}%", name))) 模糊匹配查询：前后带 %
                ;
    }

    /**
     * 排序参数构建：QueryDSL 查询模式；QueryDSL 模式构建排序对象返回 null 则会抛出异常
     *
     * @return OrderSpecifier[]
     */
    public OrderSpecifier<?>[] qdslOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return new OrderSpecifier<?>[]{OrderByColumn.id.column.asc()}; // 指定默认排序字段
                return new OrderSpecifier<?>[]{};
            }
            return orderBy.stream().map((OrderBy by) -> {
                final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                // 自定义排序
                // new OrderSpecifier<>(Order.valueOf(by.getDirection().name()) , Expressions.stringTemplate("convert_gbk({0})", column.column));

                // return Objects.equals(orderBy.getDirection(), Sort.Direction.DESC) ? column.column.desc(): column.column.asc();
                return new OrderSpecifier<>(Order.valueOf(by.getDirection().name()), column.column);
            }).toArray(OrderSpecifier<?>[]::new);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 排序参数构建：Spring JPA 查询模式；JPA 模式构建排序对象返回 null 表示不排序
     *
     * @return {@link Sort}
     */
    public Sort jpaOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return Sort.by(Sort.Direction.ASC, OrderByColumn.id.name()); // 指定默认排序字段
                return Sort.unsorted();
            }
            return orderBy.stream()
                    .map((OrderBy by) -> {
                        final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                        // 自定义排序
                        // JpaSort.unsafe(by.getDirection(), String.format("convert_gbk(%s)", column.name()));
                        return Sort.by(by.getDirection(), column.name());
                    })
                    .reduce(Sort::and)
                    .orElseGet(Sort::unsorted);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 获取查询实体与数据库表映射的所有字段,用于投影到 VO 类
     * 支持追加扩展字段,追加扩展字段一般用于连表查询
     *
     * @param appends {@link Expression}[] 追加扩展连表查询字段
     * @return {@link Expression}[]
     */
    public static Expression<?>[] allColumnAppends(final Expression<?>... appends) {
        final List<Expression<?>> columns = Lists.newArrayList(appends);
        final Class<?> clazz = QTabAppEvent.tabAppEvent.getClass();
        try {
            for (Field field : clazz.getDeclaredFields()) {
                if (field.getType().isPrimitive()) continue;
                final Object o = field.get(QTabAppEvent.tabAppEvent);
                if (o instanceof EntityPath || o instanceof BeanPath) continue;
                if (o instanceof Path) {
                    columns.add((Path<?>) o);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("获取查询实体属性与数据库映射的字段异常", e);
        }
        return columns.toArray(new Expression<?>[0]);
    }

// DB End **************************************************************************************************************

}
/* yml 缓存配置
spring:
  app:
    cache:
      # AppEventCache
      tab-app-event: { expired: 30d, nullValueExpired: 1m }
*/
/*

import com.ccx.demo.business.app.entity.AppEvent;
import com.support.mvc.entity.ITableCache;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static app.common.starter.AppContext.getBean;

\/**
 * 缓存：埋点表
 *
 * @author 谢长春 on 2022-06-27
 *\/
@Slf4j
@Component
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.cache.tab-app-event")
public class AppEventCache implements ITableCache<Long, AppEvent> {

    private static final String CACHE_KEY = AppEventCache.class.getSimpleName() + ":%d";
    private static volatile AppEventCache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    private final CacheRepository<Long, AppEvent> cacheRepository;

    @Override
    public CacheRepository<Long, AppEvent> getCacheRepository() {
        return cacheRepository;
    }

    @Override
    public Class<AppEvent> getEntityClazz() {
        return AppEvent.class;
    }

    \/**
     * 缓存过期时间
     *\/
    @Setter
    @Getter
    private Duration expired;

    \/**
     * 缓存过期时间
     *\/
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;

    \/**
     * 获取 {@link AppEventCache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link AppEventCache}
     *\/
    public static AppEventCache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (AppEventCache.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = getBean(AppEventCache.class);
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.tab-app-event.expired: {}", expired);
        log.info("spring.app.cache.tab-app-event.nullValueExpired: {}", nullValueExpired);
    }

    @Override
    public String cacheKey(final Long id) {
        return String.format(CACHE_KEY, id);
    }

    private RBucket<String> getBucket(final Long id) {
        return redissonClient.getBucket(cacheKey(id));
    }

}

*/
