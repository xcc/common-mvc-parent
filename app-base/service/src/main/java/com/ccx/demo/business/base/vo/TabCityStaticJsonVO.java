package com.ccx.demo.business.base.vo;

import com.ccx.demo.business.base.entity.TabCity;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 城市静态资源实体
 *
 * @author 谢长春 2022-10-31
 */
@ApiModel(description = "扩展城市实体VO")
@Getter
@Setter
@ToString(callSuper = true)
public class TabCityStaticJsonVO extends TabCity {
    @Override
    public String getDv() {
        return null;
    }
}
