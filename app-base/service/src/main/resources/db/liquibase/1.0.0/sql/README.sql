INSERT INTO tab_role(id, name, authorities, hidden, remark, createTime, updateTime)
VALUES ('5j3m2ypc1y6qXBSQ4h', '超级管理员', '["ROLE_ADMIN"]', 1, '', DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
     , ('5j3m2ypc1y6aFFUS0E', '普通用户', '["ROLE_USER"]', 1, '', DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
     , ('5j3m2ypc1y6zA0p37N', '运营管理端普通用户', '["OM_PC"]', 0, '不要删除这个角色，允许登录使用运营管理端', DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
     , ('5j3m2ypc1y61GZGSOO', 'APP端、微信端用户', '["USER_ANDROID", "USER_IOS", "USER_WECHAT", "USER_WECHAT_APP"]', 0, '不要删除这个角色，允许登录使用安卓APP、苹果APP、微信小程序', DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), CONCAT(DATE_FORMAT(NOW(), '%Y%m%d%H%i%S'), '000'))
;
