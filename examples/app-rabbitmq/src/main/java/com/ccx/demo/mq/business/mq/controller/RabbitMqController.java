package com.ccx.demo.mq.business.mq.controller;

import app.common.starter.entity.Result;
import app.common.starter.util.JSON;
import com.ccx.demo.mq.business.mq.dto.MessageDTO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 对外接口： RabbitMq 消息测试
 *
 * @author 谢长春 on 2022-03-15
 */
@Api(tags = " RabbitMq 消息测试")
//@ApiSort() // 控制接口排序
@RequestMapping("/rabbit-mq")
@Controller
@Slf4j
@RequiredArgsConstructor
public class RabbitMqController {
    private final RabbitTemplate rabbitTemplate;
    @Value("${spring.app.rabbitmq.topicExchangeName}")
    private String topicExchangeName;
    @Value("${spring.app.rabbitmq.defaultQueueRoutingKey}")
    private String defaultQueueRoutingKey;
    @Value("${spring.app.rabbitmq.testQueueRoutingKey}")
    private String testQueueRoutingKey;

    @PostMapping("/push/default")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "发送MQ消息"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 1,
            author = "谢长春",
            includeParameters = {
                    "body.key", "body.value", "body.comment"
            })
    @ResponseBody
    public Result<Void> pushDefault(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final MessageDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            String traceId = MDC.get("traceId") + "-%d";
            for (int i = 0; i < 100; i++) {
                body.setKey(String.format(traceId, i));
                rabbitTemplate.convertAndSend(topicExchangeName, defaultQueueRoutingKey, JSON.toJsonString(body));
            }
        });
    }

    @PostMapping("/push/test")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "发送MQ消息"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 2,
            author = "谢长春",
            includeParameters = {
                    "body.key", "body.value", "body.comment"
            })
    @ResponseBody
    public Result<Void> pushTest(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final MessageDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            String traceId = MDC.get("traceId") + "-%d";
            for (int i = 0; i < 100; i++) {
                body.setKey(String.format(traceId, i));
                rabbitTemplate.convertAndSend(topicExchangeName, testQueueRoutingKey, JSON.toJsonString(body));
            }
        });
    }
}
