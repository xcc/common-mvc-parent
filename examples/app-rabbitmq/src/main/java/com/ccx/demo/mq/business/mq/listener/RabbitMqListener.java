package com.ccx.demo.mq.business.mq.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.rabbitmq.enabled", havingValue = "true")
public class RabbitMqListener {
    /**
     * mq消息监听
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    @RabbitListener(queues = "${spring.app.rabbitmq.defaultQueueName}")
    public void defaultMqMessageListener(final String message) {
        log.info("defaultMqMessageListener=>{}", message);
    }

    /**
     * mq消息监听
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    @RabbitListener(queues = "${spring.app.rabbitmq.testQueueName}")
    public void testMqMessageListener(final String message) {
        log.info("testMqMessageListener=>{}", message);
    }
}
