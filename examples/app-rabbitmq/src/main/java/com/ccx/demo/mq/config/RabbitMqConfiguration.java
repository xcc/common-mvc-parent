package com.ccx.demo.mq.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * RabbitMq 配置
 *
 * @author 谢长春 2022-03-15
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(value = "spring.app.rabbitmq.enabled", havingValue = "true")
@ConfigurationProperties(prefix = "spring.app.rabbitmq")
public class RabbitMqConfiguration {
    @Setter
    @Getter
    private String topicExchangeName;
    @Setter
    @Getter
    private String defaultQueueName;
    @Setter
    @Getter
    private String defaultQueueRoutingKey;
    @Setter
    @Getter
    private int defaultQueuePriority;
    @Setter
    @Getter
    private String testQueueName;
    @Setter
    @Getter
    private String testQueueRoutingKey;
    @Setter
    @Getter
    private int testQueuePriority;

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Queue defaultQueue() {
        final Map<String, Object> args = new HashMap<>();
        args.put("x-max-priority", defaultQueuePriority);
        // # durable
        // 持久化，保证RabbitMQ在退出或者crash等异常情况下数据没有丢失，需要将queue，exchange和Message都持久化。
        // 若是将queue的持久化标识durable设置为true，则代表是一个持久的队列，那么在服务重启之后，会重新读取之前被持久化的queue。
        // 虽然队列可以被持久化，但是里面的消息是否为持久化，还要看消息的持久化设置。即重启queue，但是queue里面还没有发出去的消息，那队列里面还存在该消息么？这个取决于该消息的设置。
        // # autoDeleted
        // 自动删除，如果该队列没有任何订阅的消费者的话，该队列会被自动删除。这种队列适用于临时队列。
        // 当一个Queue被设置为自动删除时，当消费者断掉之后，queue会被删除，这个主要针对的是一些不是特别重要的数据，不希望出现消息积累的情况。
        // ********************************************************
        // 重要的数据，一般设置 durable=true, autoDeleted=false
        // ********************************************************
        return new Queue(defaultQueueName, true, false, false, args);
    }

    @Bean
    Binding defaultQueueBinding(final Queue defaultQueue, final TopicExchange exchange) {
        log.info("topicExchangeName:{}, defaultQueueName:{}, defaultQueueRoutingKey:{}, defaultQueuePriority:{}, "
                , topicExchangeName
                , defaultQueueName
                , defaultQueueRoutingKey
                , defaultQueuePriority
        );
        // defaultQueueRoutingKey + ".#"
        // # 表示零个或多个词
        // * 表示一个词
        return BindingBuilder.bind(defaultQueue).to(exchange).with(defaultQueueRoutingKey);
    }

    @Bean
    Queue testQueue() {
        final Map<String, Object> args = new HashMap<>();
        args.put("x-max-priority", testQueuePriority);
        return new Queue(testQueueName, true, false, false, args);
    }

    @Bean
    Binding testQueueBinding(final Queue testQueue, final TopicExchange exchange) {
        log.info("topicExchangeName:{}, testQueueName:{}, testQueueRoutingKey:{}, testQueuePriority:{}, "
                , topicExchangeName
                , testQueueName
                , testQueueRoutingKey
                , testQueuePriority
        );
        // defaultQueueRoutingKey + ".#"
        // # 表示零个或多个词
        // * 表示一个词
        return BindingBuilder.bind(testQueue).to(exchange).with(testQueueRoutingKey);
    }

}
