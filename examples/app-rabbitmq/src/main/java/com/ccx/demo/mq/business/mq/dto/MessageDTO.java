package com.ccx.demo.mq.business.mq.dto;

import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * RabbitMq 消息内容
 *
 * @author 谢长春 2022-03-15
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "MQ消息内容")
public class MessageDTO {
    public String key;
    public String value;
    public String comment;
}
