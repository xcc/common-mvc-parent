package com.ccx.demo.quick.open.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @author 谢长春 2022-06-28
 */
@RequiredArgsConstructor
@Controller
public class OpenAppController {

    @RequestMapping(value = "/", method = {GET, HEAD, OPTIONS})
    @ResponseBody
    public String csrf() {
        return "ok";
    }

}
