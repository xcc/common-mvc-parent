package app.cache.starter;

import app.common.starter.CommonConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Objects;

/**
 * <pre>
 * Spring 缓存配置:
 *   开发环境使用 ehcache.xml
 *     参考：
 *       https://www.cnblogs.com/Sicwen/p/10588047.html
 *       https://blog.csdn.net/weixin_30722589/article/details/98150575
 * </pre>
 *
 * @author 谢长春
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@ComponentScan("app.cache.starter")
public class RedissonCacheConfiguration {
    private static final String CACHE_KEY_SERVICE_ID = RedissonCacheConfiguration.class.getName();
    private static final String CACHE_KEY_APP_INFO = RedissonCacheConfiguration.class.getSimpleName();
    private static ApplicationContext APP_CONTEXT;
    private static RedissonClient REDISSON_CLIENT_INSTANCE;
    private final ApplicationContext applicationContext;
    private final CommonConfiguration commonConfiguration;

    @PostConstruct
    public void postConstruct() {
        APP_CONTEXT = applicationContext;
        REDISSON_CLIENT_INSTANCE = Objects.requireNonNull(applicationContext.getBean(RedissonClient.class), "RedissonClient 初始化失败");

        // 生成服务ID
        final RAtomicLong atomicLong = REDISSON_CLIENT_INSTANCE.getAtomicLong(CACHE_KEY_SERVICE_ID);
        commonConfiguration.setId(atomicLong.incrementAndGet());

        // 测试 redisson 写入应用信息， hgetall RedissonCacheConfiguration 查看写入信息
        final RMapCache<String, String> map = REDISSON_CLIENT_INSTANCE.getMapCache(CACHE_KEY_APP_INFO);
        map.put(
                String.join(
                        ":"
                        , Objects.toString(commonConfiguration.getId())
                        , commonConfiguration.getName()
                )
                , String.join(
                        ":"
                        , commonConfiguration.getHostIp()
                        , commonConfiguration.getEnv().name()
                        , commonConfiguration.getVersion()
                        , commonConfiguration.getBuildTime()
                        , commonConfiguration.getStartTime()
                )
        );
    }

    /**
     * 获取数据库对象操作实例
     *
     * @return {@link RedissonClient}
     */
    public static RedissonClient getRedissonClient() {
        return REDISSON_CLIENT_INSTANCE;
    }

    @PreDestroy
    public void destroy() {
        REDISSON_CLIENT_INSTANCE.getMapCache(CACHE_KEY_APP_INFO)
                .remove(String.join(
                        ":"
                        , Objects.toString(commonConfiguration.getId())
                        , commonConfiguration.getName()
                ));
    }
}
