package app.cache.starter.util;

import app.common.starter.util.ChainMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 封装 jackson 操作静态方法
 *
 * @author 谢长春 2022-07-05
 */
@Slf4j
public final class JsonCache {
    private JsonCache() {
    }

    private static ObjectMapper objectMapper;
    private static final SimpleFilterProvider simpleFilterProvider;

    static {
        simpleFilterProvider = new SimpleFilterProvider();
        objectMapper = new ObjectMapper()
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) // 反序列化忽略 java 类中不存在的字段
                .enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES) // 反序列化 null 值忽略
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS) // bean序列化为空时不会异常失败
                .disable(SerializationFeature.FAIL_ON_SELF_REFERENCES)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL) // 序列化不返回 null 值
        ;
    }


    /**
     * 添加自定义拦截器 fixme: 添加忽略加解密注解
     * <pre>
     * {@code
     *   JSON.addAnnotationIntrospector(
     *     new JacksonAnnotationIntrospector() {
     *       @Override
     *       public boolean isAnnotationBundle(Annotation ann) {
     *           final Class<? extends Annotation> type = ann.annotationType();
     *           if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
     *               // https://www.daimajiaoliu.com/daima/4ed17441d100404
     *               return false;
     *           }
     *           return super.isAnnotationBundle(ann);
     *       }
     *     }
     *   );
     * }
     * </pre>
     *
     * @param introspector {@link JacksonAnnotationIntrospector}
     */
    public static void addAnnotationIntrospector(final JacksonAnnotationIntrospector introspector) {
        objectMapper = objectMapper.setAnnotationIntrospector(
                introspector
//                new JacksonAnnotationIntrospector() {
//                    @Override
//                    public boolean isAnnotationBundle(Annotation ann) {
//                        final Class<? extends Annotation> type = ann.annotationType();
//                        if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
//                            // https://www.daimajiaoliu.com/daima/4ed17441d100404
//                            return false;
//                        }
//                        return super.isAnnotationBundle(ann);
//                    }
//                }
        );
    }

    public static ObjectMapper get() {
        return objectMapper;
    }

    /**
     * 注册属性过滤器
     * SimpleBeanPropertyFilter.serializeAllExcept() // 序列化排除字段
     * SimpleBeanPropertyFilter.filterOutAllExcept() // 序列化包含字段
     * SimpleBeanPropertyFilter.serializeAll() // 默认规则
     *
     * @param id     String 过滤器名称，全局唯一
     * @param filter SimpleBeanPropertyFilter
     */
    public static void addFilter(final String id, final SimpleBeanPropertyFilter filter) {
        simpleFilterProvider.addFilter(id, filter);
        objectMapper.setFilterProvider(simpleFilterProvider);
    }

    @SneakyThrows
    public static String toJsonString(final Object value) {
        return objectMapper.writeValueAsString(value);
    }

    @SneakyThrows
    public static <V> ChainMap<String, V> parseObject(final String jsonText) {
        if (StringUtils.isBlank(jsonText)) {
            return null;
        }
        final LinkedHashMap<String, V> map = objectMapper.readValue(jsonText, new TypeReference<LinkedHashMap<String, V>>() {
        });
        return ChainMap.create(map);
    }

    @SneakyThrows
    public static <T> T parseObject(final String jsonText, Class<T> clazz) {
        if (StringUtils.isBlank(jsonText)) {
            return null;
        }
        return objectMapper.readValue(jsonText, clazz);
    }

    @SneakyThrows
    public static <T> T parseObject(final String jsonText, TypeReference<T> typeReference) {
        if (StringUtils.isBlank(jsonText)) {
            return null;
        }
        return objectMapper.readValue(jsonText, typeReference);
    }

    @SneakyThrows
    public static <T> List<T> parseArray(final String jsonText) {
        if (StringUtils.isBlank(jsonText)) {
            return null;
        }
        return objectMapper.readValue(jsonText, new TypeReference<List<T>>() {
        });
    }

}
