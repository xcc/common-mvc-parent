package com;

import app.common.starter.enums.Code;
import app.common.starter.interfaces.ICache;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;

/**
 * 手机验证码缓存
 *
 * @author 谢长春 2022-05-04
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PhoneCodeCacheTest implements ICache {
    public static final String CACHE_KEY = PhoneCodeCacheTest.class.getSimpleName() + ":%s";
    private static final String CODE = "code";
    private static final String COUNT = "count";

    private final RedissonClient redissonClient;
    private final boolean verifyCodeEnabled;

    /**
     * 生成 4 位验码， 手机号作为key缓存
     *
     * @param phone String 手机号
     * @return String
     */
    public String get(final String phone) {
        final String code = RandomStringUtils.randomNumeric(4); // 4 位数字验证码
        final RMap<String, Object> loginPhoneCodeBucket = redissonClient.getMap(String.format(CACHE_KEY, phone));
        loginPhoneCodeBucket.put(CODE, code); // 缓存验证码
        loginPhoneCodeBucket.put(COUNT, 0); // 计数，避免暴力破解，验证码输入错误次数达到 3 次时，删除验证码
        loginPhoneCodeBucket.expire(Duration.ofMinutes(5)); // 验证码有效期 5 分钟
        return code;
    }


    /**
     * 断言验证码是否正确， 避免暴力破解，验证码错误次数达到 3 次时，删除验证码
     *
     * @param phone String 手机号
     * @param code  String 验证码
     */
    public void asserts(final String phone, final String code) {
        if (!verifyCodeEnabled) {
            log.warn("spring.app.verifyCodeEnabled => spring.app.feign.sms.enabled ： 短信验证码开关未打开");
            return;
        }
        final RMap<String, Object> loginPhoneCodeBucket = redissonClient.getMap(String.format(CACHE_KEY, phone));
        Code.A00002.assertHasTrue(loginPhoneCodeBucket.isExists(), "验证码已过期");
        if (Objects.equals(code, loginPhoneCodeBucket.get(CODE))) {
            loginPhoneCodeBucket.delete(); // 验证码校验成功
        } else {
            final int count = Optional
                    .ofNullable(loginPhoneCodeBucket.get(COUNT))
                    .map(Objects::toString)
                    .map(Integer::parseInt)
                    .orElse(0)
                    + 1;
            loginPhoneCodeBucket.put(COUNT, count);
            if (count >= 3) { // 计数，避免暴力破解，验证码输入错误次数达到 3 次时，删除验证码
                loginPhoneCodeBucket.delete();
            }
            throw Code.A00002.toCodeException("验证码错误");
        }
    }

    public static void main(String[] args) {
        Config config = new Config();
        config
                .useSingleServer()
                .setTimeout(1000000)
                .setAddress("redis://127.0.0.1:6379")
                .setPassword("111111");
        @Cleanup("shutdown") RedissonClient redissonClient = Redisson.create(config);
        final PhoneCodeCacheTest phoneCodeCacheTest = new PhoneCodeCacheTest(redissonClient, false);
        phoneCodeCacheTest.get("18717942600");
        phoneCodeCacheTest.asserts("18717942600", "1111");
    }

}
