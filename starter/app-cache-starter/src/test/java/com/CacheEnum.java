package com;

import java.util.Arrays;

/**
 * 枚举：应用操作埋点事件
 *
 * @author 谢长春 2021-05-31
 */
public enum CacheEnum {
    REQUEST_0("未知事件"),
    LOGIN_1("登录"),
    CHANGE_PASSWORD_2("修改密码"),
    AE3("xx点击事件", true) {
        @Override
        public boolean isClick() {
            return true;
        }
    },
    ;
    /**
     * 枚举属性说明
     */
    public final String comment;
    /**
     * 是否在客户端埋点
     */
    public final boolean client;

    CacheEnum(final String comment) {
        this(comment, false);
    }

    CacheEnum(final String comment, final boolean client) {
        this.comment = comment;
        this.client = client;
    }

    /**
     * 是否点击事件触发埋点
     *
     * @return boolean true:是
     */
    public boolean isClick() {
        return false;
    }

    /**
     * 埋点关联数据表主键数据类型是否为字符串
     *
     * @return boolean true:是
     */
    public boolean isRefString() {
        return false;
    }

    /**
     * 埋点关联数据表主键数据类型是否为数字
     *
     * @return boolean true:是
     */
    public boolean isRefLong() {
        return !isRefString();
    }

    public static void main(String[] args) {
        Arrays.stream(CacheEnum.values())
                .filter(CacheEnum::isClick) // 获取点击事件埋点
                .forEach(ele -> {
                    System.out.printf("点击事件埋点：%s：%s\n", ele.name(), ele.comment);
                });
        Arrays.stream(CacheEnum.values())
                .filter(CacheEnum::isRefString)
                .forEach(ele -> {
                    System.out.printf("埋点关联表主键为字符串：%s：%s\n", ele.name(), ele.comment);
                });
    }
}
