package com;

import app.common.starter.util.Dates;
import app.common.starter.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Sets;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.redisson.Redisson;
import org.redisson.api.RBatch;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
public class RedissonTest {
    public static void main(String[] args) {
        Config config = new Config();
        config
                .useSingleServer()
                .setTimeout(1000000)
                .setAddress("redis://127.0.0.1:6379")
                .setPassword("111111");
        @Cleanup("shutdown") RedissonClient redissonClient = Redisson.create(config);
        {
            RBucket<String> bucket = redissonClient.getBucket("TabRoleCache::1", StringCodec.INSTANCE);
            System.out.println(bucket.get());
        }
        {
            RBucket<String> bucket = redissonClient.getBucket("TabUserCache.login::admin-test", StringCodec.INSTANCE);
            System.out.println(bucket.get());
        }
        {
            RBucket<String> bucket = redissonClient.getBucket("TabUserCache::1", StringCodec.INSTANCE);
            System.out.println(bucket.get());
        }
        {
            RMap<String, String> map = redissonClient.getMap("map", StringCodec.INSTANCE);
            map.put("a", "1");
            JSON.parseOptional(
                    JSON.toJsonString(new HashMap<String, Object>() {{
                        put("b", 2);
                        put("c", Dates.now().date());
                        put("f", Dates.now().timestamp());
                        put("d", "中文");
                    }}),
                    new TypeReference<Map<String, String>>() {
                    }
            ).ifPresent(map::putAll);
            System.out.println(map.get("b"));
            System.out.println(JSON.toJsonString(map.getAll(Sets.newHashSet("b", "a", "c", "d", "f"))));
        }
        {
            Set<String> ids = Sets.newHashSet("1111", "2222", "3333");
            { // 批量保存
                final RBatch batch = redissonClient.createBatch();
                ids.forEach(id -> batch.getBucket("key:" + id).setAsync(RandomStringUtils.randomAlphanumeric(32)));
                List<?> responses = batch.execute().getResponses();
                responses.forEach(System.out::println);
            }
            { // 批量取值
                final RBatch batch = redissonClient.createBatch();
                ids.forEach(id -> batch.getBucket("key:" + id).getAsync());
                List<?> responses = batch.execute().getResponses();
                responses.forEach(System.out::println);
            }
        }
    }
}
