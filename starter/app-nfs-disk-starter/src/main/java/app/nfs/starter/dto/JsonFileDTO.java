package app.nfs.starter.dto;

import app.nfs.starter.NfsConfiguration;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 静态json文件对象
 *
 * @author 谢长春 on 2022-06-24
 */
@Component
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "静态json文件对象")
public class JsonFileDTO extends FileDTO {

    private static final long serialVersionUID = -6779893372268172821L;
    public static final String DIR_NAME = JsonFileDTO.class.getSimpleName();

    public JsonFileDTO(final String uname) {
        super(uname);
    }

    public JsonFileDTO(final String name, final String uname) {
        super(name, uname);
    }
//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = JsonFileDTO.class.getSimpleName();

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, uname);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getUrl() {
        return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, uname);
    }

    @SneakyThrows
    public JsonFileDTO cloneObject() {
        return (JsonFileDTO) super.clone();
    }
}
