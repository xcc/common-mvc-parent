package app.nfs.starter.dto;

import app.common.starter.enums.Charsets;
import app.common.starter.enums.ContentType;
import app.common.starter.interfaces.IJson;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.URLEncoder;
import java.util.Objects;
import java.util.UUID;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 上传文件对象
 *
 * @author 谢长春 on 2017/10/17.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(description = "上传文件对象")
public abstract class FileDTO implements Serializable, Cloneable, IJson {

    private static final long serialVersionUID = -5061171931624765053L;

    public FileDTO(final String uname) {
        this(uname.replaceFirst("^/+", ""), uname.replaceFirst("^/+", ""));
    }

    @SneakyThrows
    public FileDTO(final String name, final String uname) {
        final String[] arrs = name.split("/");
        this.name = URLEncoder.encode(StringUtils.right(arrs[arrs.length - 1], 50), Charsets.UTF_8.displayName());
        this.uname = uname.replaceFirst("^/+", "");
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = FileDTO.class.getSimpleName();

    /**
     * 文件名，用户上传的文件名
     */
    @ApiModelProperty(position = 1, value = "文件名，用户上传的文件名", example = "测试.png", required = true)
    protected String name;
    /**
     * 唯一文件名，磁盘上存储的uuid文件名
     */
    @ApiModelProperty(position = 2, value = "唯一文件名，磁盘上存储的uuid文件名", example = "uuid.png", required = true)
    protected String uname;

    /**
     * 是否为私有文件；OSS获取私有文件访问地址时需要拼接访问令牌，磁盘存储不支持该功能；默认值：false
     *
     * @return boolean true:私有文件，false:公有文件，
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public boolean isPrivateFile() {
        return false;
    }

    /**
     * 文件默认存在临时目录
     *
     * @return String 文件绝对路径
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public abstract String getAbsolutePath();
//    {return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, uname);}

    /**
     * 文件访问路径， 示例：https://oss.app.com/test.xlsx
     *
     * @return String
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(position = 3, value = "文件访问路径", example = "https://oss.app.com/uuid.png", accessMode = READ_ONLY)
    public abstract String getUrl();
    // {return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendUrl(DIR_NAME, uname);}

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public String getContentType() {
        final String suffix = getSuffix();
        try {
            return ContentType.valueOf(suffix).type;
        } catch (IllegalArgumentException e) {
            log.warn("文件后缀【{}】未定义在枚举类 app.common.enums.ContentType ， 请将文件后缀对应的 content-type，定义到枚举类， 否则将使用默认的content-type: application/octet-stream", suffix);
            return ContentType.stream.type;
        }
    }

    /**
     * 获取文件后缀。  uname.png => png
     *
     * @return String 文件后缀
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public String getSuffix() {
        return StringUtils.isEmpty(uname) ? "" : uname.replaceFirst("^.*\\.(\\w+)$", "$1");
    }

    /**
     * 构造 MD5 文件名带后缀，拼接目录前缀 <br>
     * 参考案例： <br>
     * {@code
     * buildMd5Uname("文件内容", "文件夹A", "文件夹B"); => this.uname = "文件夹A/文件夹B/{文件流md5}.后缀"
     * }
     *
     * @param text {@link String} 文件内容
     * @param dirs {@link String} 目录名
     * @return T
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    public <T extends FileDTO> T buildMd5Uname(final String text, final String... dirs) {
        if (Objects.isNull(dirs) || dirs.length == 0) {
            this.uname = String.format("%s.%s", DigestUtils.md5Hex(text), getSuffix());

        } else {
            this.uname = String.format(
                    "%s/%s.%s"
                    , String.join("/", dirs)
                            .replaceAll("[/]{2,}", "/") // 去除目录中间多余的 "/"
                            .replaceFirst("^/", "") // 去除目录前面的 "/"
                            .replaceFirst("/$", "") // 去除目录结尾的 "/"
                    , DigestUtils.md5Hex(text)
                    , getSuffix());
        }
        return (T) this;
    }

    /**
     * 构造 MD5 文件名带后缀，拼接目录前缀 <br>
     * 参考案例： <br>
     * {@code
     * buildMd5Uname("文件内容".getBytes(UTF_8), "文件夹A", "文件夹B"); => this.uname = "文件夹A/文件夹B/{文件流md5}.后缀"
     * }
     *
     * @param bytes byte[] 文件内容
     * @param dirs  {@link String} 目录名
     * @return T
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    public <T extends FileDTO> T buildMd5Uname(final byte[] bytes, final String... dirs) {
        if (Objects.isNull(dirs) || dirs.length == 0) {
            this.uname = String.format("%s.%s", DigestUtils.md5Hex(bytes), getSuffix());

        } else {
            this.uname = String.format(
                    "%s/%s.%s"
                    , String.join("/", dirs)
                            .replaceAll("[/]{2,}", "/") // 去除目录中间多余的 "/"
                            .replaceFirst("^/", "") // 去除目录前面的 "/"
                            .replaceFirst("/$", "") // 去除目录结尾的 "/"
                    , DigestUtils.md5Hex(bytes)
                    , getSuffix()
            );
        }
        return (T) this;
    }

    /**
     * 构造 MD5 文件名带后缀，拼接目录前缀 <br>
     * 参考案例： <br>
     * {@code
     * buildMd5Uname("文件内容".getBytes(UTF_8), "文件夹A", "文件夹B"); => this.uname = "文件夹A/文件夹B/{文件流md5}.后缀"
     * }
     *
     * @param file {@link File} 文件内容
     * @param dirs {@link String} 目录名
     * @return T
     */
    @SneakyThrows
    public <T extends FileDTO> T buildMd5Uname(final File file, final String... dirs) {
        @Cleanup final FileInputStream fileInputStream = new FileInputStream(file);
        @Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        return buildMd5Uname(bufferedInputStream, dirs);
    }

    /**
     * 构造 MD5 文件名带后缀，拼接目录前缀 <br>
     * 参考案例： <br>
     * {@code
     * buildMd5Uname(MultipartFile.getInputStream(), "文件夹A", "文件夹B"); => this.uname = "文件夹A/文件夹B/{文件流md5}.后缀"
     * }
     *
     * @param inputStream {@link InputStream}
     * @param dirs        {@link String} 目录名
     * @return T
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    public <T extends FileDTO> T buildMd5Uname(final InputStream inputStream, final String... dirs) {
        if (Objects.isNull(dirs) || dirs.length == 0) {
            this.uname = String.format("%s.%s", DigestUtils.md5Hex(inputStream), getSuffix());
        } else {
            this.uname = String.format(
                    "%s/%s.%s"
                    , String.join("/", dirs)
                            .replaceAll("[/]{2,}", "/") // 去除目录中间多余的 "/"
                            .replaceFirst("^/", "") // 去除目录前面的 "/"
                            .replaceFirst("/$", "") // 去除目录结尾的 "/"
                    , DigestUtils.md5Hex(inputStream)
                    , getSuffix()
            );
        }
        return (T) this;
    }

    /**
     * 构造 UUID 文件名带后缀，拼接目录前缀 <br>
     * 参考案例： <br>
     * {@code
     * buildUuidUname("文件夹A", "文件夹B"); => this.uname = "文件夹A/文件夹B/{uuid}.后缀"
     * }
     *
     * @param dirs {@link String} 目录名
     * @return T
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    public <T extends FileDTO> T buildUuidUname(final String... dirs) {
        final String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        if (Objects.isNull(dirs) || dirs.length == 0) {
            this.uname = String.format("%s.%s", uuid, getSuffix());

        } else {
            this.uname = String.format(
                    "%s/%s.%s"
                    , String.join("/", dirs)
                            .replaceAll("[/]{2,}", "/") // 去除目录中间多余的 "/"
                            .replaceFirst("^/", "") // 去除目录前面的 "/"
                            .replaceFirst("/$", "") // 去除目录结尾的 "/"
                    , uuid
                    , getSuffix()
            );
        }
        return (T) this;
    }

//    public <T> T parse(Class<T> clazz) {
//        return JSON.parseObject(json(), clazz);
//    }

    @SneakyThrows
    public FileDTO cloneObject() {
        return (FileDTO) super.clone();
    }
}
