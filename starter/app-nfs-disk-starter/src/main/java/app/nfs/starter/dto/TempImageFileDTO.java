package app.nfs.starter.dto;

import app.nfs.starter.NfsConfiguration;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 图片文件对象
 *
 * @author 谢长春 on 2022-06-24
 */
@Component
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "临时图片文件对象")
public class TempImageFileDTO extends ImageFileDTO {

    private static final long serialVersionUID = -6779893372268172821L;
    public static final String DIR_NAME = TempImageFileDTO.class.getSimpleName();

    public TempImageFileDTO(final String uname) {
        super(uname);
    }

    public TempImageFileDTO(final String name, final String uname) {
        super(name, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = ImageFileDTO.class.getSimpleName();

    @ApiModelProperty(value = "缩略图最大宽度，取长边等比压缩，单位：px", example = "500", accessMode = READ_ONLY)
    public Integer getMiniMaxWidth() {
        return 500;
    }

    @ApiModelProperty(value = "缩略图最大高度，取长边等比压缩，单位：px", example = "300", accessMode = READ_ONLY)
    public Integer getMiniMaxHeight() {
        return 300;
    }

    @ApiModelProperty(value = "裁减方图宽度，单位：px", example = "100", accessMode = READ_ONLY)
    public Integer getCropWidth() {
        return 100;
    }

    @ApiModelProperty(value = "裁减方图高度，单位：px", example = "100", accessMode = READ_ONLY)
    public Integer getCropHeight() {
        return 100;
    }

    @ApiModelProperty(hidden = true)
    @Override
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, uname);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMiniAbsolutePath() {
        return StringUtils.isEmpty(mini) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, mini);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getCropAbsolutePath() {
        return StringUtils.isEmpty(crop) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, crop);
    }

    @Override
    public String getUrl() {
        return StringUtils.isEmpty(uname) ? getUrl() : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, uname);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getMiniUrl() {
        return StringUtils.isEmpty(mini) ? getUrl() : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, mini);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getCropUrl() {
        return StringUtils.isEmpty(crop) ? getUrl() : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, crop);
    }

    @SneakyThrows
    public TempImageFileDTO cloneObject() {
        return (TempImageFileDTO) super.clone();
    }
}
