package app.nfs.starter.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * jvm 临时文件存储目录
 *
 * @author 谢长春 2022-08-16
 */
@NoArgsConstructor
@ToString(callSuper = true)
@Getter
@Setter
public class JavaIoTmpDirFileDTO extends FileDTO {

    private final static String tmpdir;

    static {
        tmpdir = System.getProperty("java.io.tmpdir");
    }

    public JavaIoTmpDirFileDTO(final String uname) {
        super(uname);
    }

    public JavaIoTmpDirFileDTO(final String name, final String uname) {
        super(name, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = JavaIoTmpDirFileDTO.class.getSimpleName();

    @Override
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : Paths.get(tmpdir, uname).toAbsolutePath().toString();
    }

    public Path toPath() {
        return StringUtils.isEmpty(uname) ? null : Paths.get(tmpdir, uname);
    }

    @Override
    public String getUrl() {
        throw new RuntimeException("不支持访问");
    }

    @Override
    @SneakyThrows
    public JavaIoTmpDirFileDTO cloneObject() {
        return (JavaIoTmpDirFileDTO) super.clone();
    }

}
