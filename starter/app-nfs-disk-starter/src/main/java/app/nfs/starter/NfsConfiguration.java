package app.nfs.starter;

import app.nfs.starter.dto.FileDTO;
import app.nfs.starter.dto.TempFileDTO;
import app.nfs.starter.service.FileService;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.annotation.PostConstruct;
import java.util.Objects;

import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmssSSS;

/**
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableConfigurationProperties
//@ConditionalOnProperty(value = "spring.app.nfs", matchIfMissing = true)
@ComponentScan("app.nfs.starter")
public class NfsConfiguration {

    private static ApplicationContext APP_CONTEXT;
    private static volatile FileService FILE_SERVICE_INSTANCE;
    private final ApplicationContext applicationContext;

    @PostConstruct
    public void postConstruct() {
        APP_CONTEXT = applicationContext;
    }

    /**
     * 文件存储服务
     *
     * @return {@link FileService}
     */
    public static FileService getFileService() {
        if (Objects.isNull(FILE_SERVICE_INSTANCE)) {
            synchronized (NfsConfiguration.class) {
                if (Objects.isNull(FILE_SERVICE_INSTANCE)) {
                    FILE_SERVICE_INSTANCE = APP_CONTEXT.getBean(FileService.class);
                }
            }
        }
        return FILE_SERVICE_INSTANCE;
    }

    private static final int MAX = 80;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Bean
    public CommandLineRunner initNfsCommandLineRunner(ApplicationContext context) {
        return args -> {
//            final AppConfig.Type type = nfs.getType();
//            log.info(LOG_PATTERN,
//                    "文件存储路径配置",
//                    Stream.of(AppConfig.Path.values())
//                            .map(path -> {
//                                if (Objects.equals(type, AppConfig.Type.disk)) {
//                                    return StringUtils.left(StringUtils.rightPad(path.name() + ": " + FPath.of(path.get()).mkdirs().absolute(), MAX, ' '), MAX) + " # " + path.comment;
//                                }
//                                return StringUtils.left(StringUtils.rightPad(path.name() + ": " + path.get(), MAX, ' '), MAX) + " # " + path.comment;
//                            })
//                            .collect(Collectors.joining("\n")),
//                    "文件存储路径配置"
//            );
//            log.info(LOG_PATTERN,
//                    "文件访问路径配置",
//                    Stream.of(AppConfig.URL.values())
//                            .map(url -> StringUtils.left(StringUtils.rightPad(url.name() + ": " + url.value(), MAX, ' '), MAX) + " # " + url.comment)
//                            .collect(Collectors.joining("\n")),
//                    "文件访问路径配置"
//            );
            final FileService fileService = context.getBean(FileService.class);
            final TempFileDTO dto = fileService.writeAndReplace(yyyyMMddHHmmssSSS.now(), new TempFileDTO("test.txt"));
            log.info("测试文件上传存储路径: {}", dto.getAbsolutePath());
            log.info("测试文件上传访问路径: {}", dto.getUrl());

            context.getBeansOfType(FileDTO.class).forEach((beanName, file) -> {
                file.setUname("/");
                log.info("{} => 文件存储(访问路径)： {} ( {} )"
                        , file.getClass().getName().replaceAll("\\$.*", "")
                        , file.getAbsolutePath()
                        , file.getUrl()
                );
            });
        };
    }

}
