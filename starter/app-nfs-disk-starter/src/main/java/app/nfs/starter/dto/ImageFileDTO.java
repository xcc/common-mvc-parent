package app.nfs.starter.dto;

import app.nfs.starter.service.FileService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Objects;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 图片文件对象
 *
 * @author 谢长春 on 2022-06-24
 */
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "图片文件对象")
public abstract class ImageFileDTO extends FileDTO {

    private static final long serialVersionUID = -6779893372268172821L;

    public ImageFileDTO(final String uname) {
        super(uname);
    }

    public ImageFileDTO(final String name, final String uname) {
        super(name, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = ImageFileDTO.class.getSimpleName();

    /**
     * 等比缩放的缩略图，用于列表页面优化图片加载速度，点击该图片才显示原图
     */
    @ApiModelProperty(value = "等比缩放的缩略图，用于列表页面优化图片加载速度，点击该图片才显示原图", example = "https://oss.app.com/image_mini.png", accessMode = READ_ONLY)
    protected String mini;

    /**
     * 裁剪过的图片，比如裁减方图用于9宫格，点击该图片才显示原图
     */
    @ApiModelProperty(value = "裁剪过的图片，比如裁减方图用于9宫格，点击该图片才显示原图", example = "https://oss.app.com/image_crop.png", accessMode = READ_ONLY)
    protected String crop;

    @ApiModelProperty(value = "缩略图最大宽度，取长边等比压缩，单位：px", example = "500", accessMode = READ_ONLY)
    public abstract Integer getMiniMaxWidth();

    @ApiModelProperty(value = "缩略图最大高度，取长边等比压缩，单位：px", example = "300", accessMode = READ_ONLY)
    public abstract Integer getMiniMaxHeight();

    @ApiModelProperty(value = "裁减方图宽度，单位：px", example = "100", accessMode = READ_ONLY)
    public abstract Integer getCropWidth();

    @ApiModelProperty(value = "裁减方图高度，单位：px", example = "100", accessMode = READ_ONLY)
    public abstract Integer getCropHeight();

    /**
     * 构造缩略图文件名: uname.png => uname_mini.png
     */
    private void buildMini() {
        this.mini = this.uname.replaceAll("(\\.[a-zA-Z]+)$", "_mini$1");
    }

    /**
     * 构造裁剪图文件名: uname.png => uname_crop.png
     */
    private void buildCorp() {
        this.crop = this.uname.replaceAll("(\\.[a-zA-Z]+)$", "_crop$1");
    }

    /**
     * 缩略图存储路径
     *
     * @return
     */
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public abstract String getMiniAbsolutePath();

    /**
     * @return
     */
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public abstract String getCropAbsolutePath();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(position = 3, value = "缩略图访问路径", example = "https://oss.app.com/uuid_mini.png", accessMode = READ_ONLY)
    public abstract String getMiniUrl();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(position = 3, value = "裁减方图访问路径", example = "https://oss.app.com/uuid_crop.png", accessMode = READ_ONLY)
    public abstract String getCropUrl();

    @SneakyThrows
    public ImageFileDTO cloneObject() {
        return (ImageFileDTO) super.clone();
    }

    /**
     * 生成缩略图
     *
     * @param fileService     {@link FileService}
     * @param fileInputStream {@link InputStream} {@code MultipartFile.getInputStream()}
     */
    @SneakyThrows
    public void writeMiniImage(final FileService fileService, final InputStream fileInputStream) {
        this.buildMini();
        final int miniWidth = Objects.requireNonNull(getMiniMaxWidth(), "未定义缩略图宽度： public Integer getMiniWidth() {return null;}"), // 缩略图尺寸: 宽px
                miniHeight = Objects.requireNonNull(getMiniMaxHeight(), "未定义缩略图高度： public Integer getMiniWidth() {return null;}") // 缩略图尺寸: 高px
                        ;
        @Cleanup final ByteArrayOutputStream miniOutputStream = new ByteArrayOutputStream();
        // 基于原图生成迷你缩略图，等比缩放， 限制宽高 miniWidth×miniHeight px，按长边等比缩放，不会拉伸图片
        Thumbnails.of(fileInputStream)
                .width(miniWidth)
                .height(miniHeight)
                .outputFormat(getSuffix())
                .toOutputStream(miniOutputStream);
        final ImageFileDTO miniImageFileDTO = this.cloneObject();
        miniImageFileDTO.setUname(getMini());
        // 缩放后的图片写入文件
        fileService.write(miniOutputStream.toByteArray(), miniImageFileDTO);
    }

    /**
     * 生成缩略图和裁减方图
     *
     * @param fileService     {@link FileService}
     * @param fileInputStream {@link InputStream} {@code MultipartFile.getInputStream()}
     */
    @SneakyThrows
    public void writeMiniAndCropImage(final FileService fileService, final InputStream fileInputStream) {
        this.buildMini();
        this.buildCorp();
        final int miniWidth = Objects.requireNonNull(getMiniMaxWidth(), "未定义缩略图宽度： public Integer getMiniWidth() {return null;}"), // 缩略图尺寸: 宽px
                miniHeight = Objects.requireNonNull(getMiniMaxHeight(), "未定义缩略图高度： public Integer getMiniWidth() {return null;}"), // 缩略图尺寸: 高px
                cropWidth = Objects.requireNonNull(getCropWidth(), "未定义裁减方图宽度： public Integer getMiniWidth() {return null;}"), // 裁剪图尺寸: 宽px
                cropHeight = Objects.requireNonNull(getCropHeight(), "未定义裁减方图高度： public Integer getMiniWidth() {return null;}")  // 裁剪图尺寸: 高px
                        ;
        @Cleanup final ByteArrayOutputStream miniOutputStream = new ByteArrayOutputStream();
        {
            // 基于原图生成迷你缩略图，等比缩放， 限制宽高 miniWidth×miniHeight px，按长边等比缩放，不会拉伸图片
            Thumbnails.of(fileInputStream)
                    .width(miniWidth)
                    .height(miniHeight)
                    .outputFormat(getSuffix())
                    .toOutputStream(miniOutputStream);
            final ImageFileDTO miniImageFileDTO = this.cloneObject();
            miniImageFileDTO.setUname(getMini());
            // 缩放后的图片写入文件
            fileService.write(miniOutputStream.toByteArray(), miniImageFileDTO);
        }
        {
            // 读取缩略图
            final BufferedImage miniBufferedImage = Thumbnails
                    .of(new ByteArrayInputStream(miniOutputStream.toByteArray()))
                    .size(miniWidth, miniHeight)
                    .asBufferedImage();
            // 基于缩略图生成裁剪方图，限制宽高 cropWidth × cropHeight px，按短边等比缩放之后截取 cropWidth × cropHeight 的区域，长边超出部分会被裁掉
            final int minSize = Math.min(miniBufferedImage.getWidth(), miniBufferedImage.getHeight());
            @Cleanup final ByteArrayOutputStream cropOutputStream = new ByteArrayOutputStream();
            // 裁剪图片
            Thumbnails.of(miniBufferedImage)
                    .width(cropWidth)
                    .height(cropHeight)
                    .sourceRegion(Positions.CENTER, minSize, minSize)
                    .outputFormat(getSuffix())
                    .toOutputStream(cropOutputStream);
            final ImageFileDTO miniImageFileDTO = this.cloneObject();
            miniImageFileDTO.setUname(getCrop());
            // 裁剪后的图片写入文件
            fileService.write(cropOutputStream.toByteArray(), miniImageFileDTO);
        }
    }
}
