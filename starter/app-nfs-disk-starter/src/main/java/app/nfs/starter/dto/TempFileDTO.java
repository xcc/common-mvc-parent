package app.nfs.starter.dto;

import app.nfs.starter.NfsConfiguration;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 临时文件对象
 *
 * @author 谢长春 on 2022-06-24
 */
@Component
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "临时文件对象")
public class TempFileDTO extends FileDTO {

    private static final long serialVersionUID = -6779893372268172821L;
    public static final String DIR_NAME = TempFileDTO.class.getSimpleName();

    public TempFileDTO(final String uname) {
        super(uname);
    }

    public TempFileDTO(final String name, final String uname) {
        super(name, uname);
    }

    @ApiModelProperty(hidden = true)
    @Override
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, uname);
    }

    @Override
    public String getUrl() {
        return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = ImageFileDTO.class.getSimpleName();

    @SneakyThrows
    public TempFileDTO cloneObject() {
        return (TempFileDTO) super.clone();
    }
}
