package com;


import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ScheduledExecutorServiceTest {
    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(() -> {
            try {
                log.info("ScheduledExecutorService begin");
                Thread.currentThread().setName("Thread_ScheduledExecutorService");
                log.info("ScheduledExecutorService end");
            } catch (Exception e) {
                log.info("ScheduledExecutorService error", e);
            }
        }, 0, 1440, TimeUnit.MINUTES);
    }
}
