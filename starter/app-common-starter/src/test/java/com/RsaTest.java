package com;

import app.common.starter.util.Rsa;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Created by 谢长春 on 2017/10/14.
 */
@Slf4j
public class RsaTest {
    @SneakyThrows
    public static void main(String[] args) {
        generateKeyFiles(); // 生成新的 公钥、私钥 对，并写入 .temp 目录
        String env = "local";
//        env = "dev";
//        env = "sit";
//        env = "uat";
//        env = "prod";
        // 测试加解密
        if (Objects.equals(env, "local") || Objects.equals(env, "dev")) {
            String privateKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/dev/private.pkcs8")));
            String publicKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/dev/public.pkcs8")));
            log.info(env + ": 私钥加密: 111111\n{}", Rsa.encryptByPrivateKey("111111", privateKey));

            String adminPwd = Rsa.encryptByPublicKey("superadmin", publicKey);
            String userPwd = Rsa.encryptByPublicKey("111111", publicKey);
            log.info(env + ": 公钥加密|私钥解密: \nadminPwd: {}\nuserPwd: {}\nadminPwd: {}\nuserPwd: {}",
                    adminPwd, userPwd,
                    Rsa.decryptByPrivateKey(adminPwd, privateKey),
                    Rsa.decryptByPrivateKey(userPwd, privateKey)
            );
            // adminPwd: wnQd7TXKhjEaxMEwa9Rbxcui1GrU9GJqrMUrkLonRgjjMJgRQiZm9Kc1N+sx6TjlaRdAtCTczfR7d8e1SSzhFTFUepsbTlrArd4kkml+tnJGKjKQ7fl35XV9sV+6vimjy+YPgnu0FRGBlxgpPdh634EQqmtJ+TcInFvavZoXXN7b6IQR0Mp97f9Pv74oiwGz8kZuOMJN2f0INNQIEsoyL0YN01sW1pGHQ+OXCA6OhNkIH9Opg1r8Cgzz+6ORcPdo9der7Ez59bHErQpU7QoeEQi0dDjskCZblApjL7TCdX6wzX/lVm5VkSlfrYM3R46FeYANCJAhebGQyg/He4DR8Q==
            // userPwd: lzzrMysb72zOiC5iFtDlTflpclMbj4KCYEa73+8I6J5qG5LI/kXn4DozaexAUgzxZWUpoZIvbgNYtLwwKnsl5axut/Iyj9WA3+YN+424LOD6TP6WePBZ2pTVsuJmgVb6XAmoKJfq7TayleM7jNo686IPJVOsOxMm8T+ZmFTlO4x1TpV6E7sDYvQzBfKZABpnExo00aC7lheTL670n+KZ/RdO9+ItBPr+j3Ko1PXfYGBZ1KW/XpTAs0EOQ45HC2VFg364i+5WiZzQfMs6FIDCspVPgeoYknK2t+aciMRUF5AnKt8HcAPWRQP3NL4EXgi1j6qzcGjPDIc1YkeIndk2Lw==
            log.info(env + ": 私钥解密: " + Rsa.decryptByPrivateKey(
                    "wnQd7TXKhjEaxMEwa9Rbxcui1GrU9GJqrMUrkLonRgjjMJgRQiZm9Kc1N+sx6TjlaRdAtCTczfR7d8e1SSzhFTFUepsbTlrArd4kkml+tnJGKjKQ7fl35XV9sV+6vimjy+YPgnu0FRGBlxgpPdh634EQqmtJ+TcInFvavZoXXN7b6IQR0Mp97f9Pv74oiwGz8kZuOMJN2f0INNQIEsoyL0YN01sW1pGHQ+OXCA6OhNkIH9Opg1r8Cgzz+6ORcPdo9der7Ez59bHErQpU7QoeEQi0dDjskCZblApjL7TCdX6wzX/lVm5VkSlfrYM3R46FeYANCJAhebGQyg/He4DR8Q==",
                    privateKey
            ));
        }
        if (Objects.equals(env, "sit")) {
            String privateKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/private.pkcs8")));
            String publicKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/public.pkcs8")));

        }
        if (Objects.equals(env, "uat")) {
            String privateKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/private.pkcs8")));
            String publicKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/public.pkcs8")));
        }
        if (Objects.equals(env, "prod")) {
            String privateKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/private.pkcs8")));
            String publicKey = new String(Files.readAllBytes(Paths.get("./main-module/src/main/resources/env/" + env + "/public.pkcs8")));
        }
    }

    /**
     * 生成新的 公钥、私钥 对，并写入 .temp 目录
     */
    @SneakyThrows
    public static void generateKeyFiles() {
        final Rsa.KeyStore keys = Rsa.createKeys();
        final String privateKey = keys.getPrivateKey();
        final String publicKey = keys.getPublicKey();
        System.out.println("私钥：" + privateKey);
        System.out.println("公钥：" + publicKey);
        Files.write(Paths.get(".temp/private.pkcs8"), privateKey.getBytes());
        Files.write(Paths.get(".temp/public.pkcs8"), publicKey.getBytes());
        Files.write(
                Paths.get(".temp/private.pem"),
                ("-----BEGIN RSA PRIVATE KEY-----\n"
                        + privateKey.replaceAll("(.{64})", "$1\n")
                        + "\n-----END RSA PRIVATE KEY-----"
                ).getBytes()
        );
        Files.write(
                Paths.get(".temp/public.pem"),
                ("-----BEGIN RSA PUBLIC KEY-----\n"
                        + publicKey.replaceAll("(.{64})", "$1\n")
                        + "\n-----END RSA PUBLIC KEY-----"
                ).getBytes()
        );
        String privateContent = Rsa.encryptByPrivateKey("zh_CN 中文", privateKey);
        System.out.println("私钥加密：" + privateContent);
        try {
            System.out.println("私钥加密 => 私钥解密：" + Rsa.decryptByPrivateKey(privateContent, privateKey));
        } catch (Exception e) {
            System.out.println("私钥加密 => 私钥解密：失败");
        }
        try {
            System.out.println("私钥加密 => 公钥解密：" + Rsa.decryptByPublicKey(privateContent, publicKey));
        } catch (Exception e) {
            System.out.println("私钥加密 => 公钥解密：失败");
        }

        String publicContent = Rsa.encryptByPublicKey("zh_CN 中文", publicKey);
        System.out.println("公钥加密：" + publicContent);
        try {
            System.out.println("公钥加密 => 公钥解密：" + Rsa.decryptByPublicKey(publicContent, publicKey));
        } catch (Exception e) {
            System.out.println("公钥加密 => 公钥解密：失败");
        }
        try {
            System.out.println("公钥加密 => 私钥解密：" + Rsa.decryptByPrivateKey(publicContent, privateKey));
        } catch (Exception e) {
            System.out.println("公钥加密 => 私钥解密：失败");
        }

        final String signature = Rsa.signatureByPrivateKey(privateContent, privateKey);
        System.out.println("私钥签名：" + signature);
        System.out.println("公钥验签：" + Rsa.verifyByPublicKey(privateContent, publicKey, signature));
    }

}
