package com;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

@Slf4j
public class SystemPropertiesText {
    @SneakyThrows
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
        final Properties properties = System.getProperties();
        final SortedMap<String, Object> map = new TreeMap<>();
        properties.forEach((key, value) -> map.put(Objects.toString(key), value));
        map.forEach((key, value) -> System.out.printf("%s: %s\n", key, value));
    }
}
