package com;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GuavaTableTest {
    public static void main(String[] args) {
        {
            Table<Integer, String, String> table = HashBasedTable.create();
            table.put(10, "C", "10C");
            table.put(10, "B", "10B");
            table.put(10, "A", "10A");
            table.put(5, "C", "5C");
            table.put(5, "A", "5A");
            table.put(5, "B", "5B");
            table.put(1, "B", "5B");
            table.put(1, "C", "5C");
            table.put(1, "A", "5A");
            table.rowMap().forEach((k1, valueMap) -> {
                valueMap.forEach((k2, value) -> {
                    log.info("{} : {} : {}", k1, k2, value);
                });
            });
        }
        log.info("*****************************************************************************************************");
        {
            Table<Integer, Integer, String> table = HashBasedTable.create();
            for (int week = 1; week <= 4; week++) {
                for (int day = 1; day <= 7; day++) {
                    table.put(week, day, String.format("%d-%d", week, day));
                }
            }
            log.info("row 0: {}", table.row(0));
            log.info("col 0: {}", table.column(0));
            log.info("row 1: {}", table.row(1));
            log.info("col 1: {}", table.column(1));
            table.rowMap().forEach((week, valueMap) -> {
                log.info("week: {} , valueMap.size: {}", week, valueMap.size());
                valueMap.forEach((day, value) -> {
                    log.info("week: {} , day: {} , value: {}", week, day, value);
                });
            });
        }

    }
}
