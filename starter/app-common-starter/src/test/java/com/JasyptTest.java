package com;

import lombok.extern.slf4j.Slf4j;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jasypt.util.text.StrongTextEncryptor;

/**
 * 配置文件加解密测试。
 * <pre>
 * # 3.x 命令行加密
 *   java -cp ~/.m2/repository/org/jasypt/jasypt/1.9.3/jasypt-1.9.3.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI algorithm=PBEWithHMACSHA512AndAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator password=6782bc11b94c4c788e4b731471e6c103 input=root
 * # 3.x 命令行解密
 *   java -cp ~/.m2/repository/org/jasypt/jasypt/1.9.3/jasypt-1.9.3.jar org.jasypt.intf.cli.JasyptPBEStringDecryptionCLI algorithm=PBEWithHMACSHA512AndAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator password=6782bc11b94c4c788e4b731471e6c103 input='LCrdIg9p3GpiUQwRRVl/rh9vZNpJyUiWwC95rUvPbTnwrS+gev/1myE9JboEf9ZU'
 *
 * # 2.x 命令行加密
 *   java -cp ~/.m2/repository/org/jasypt/jasypt/1.9.3/jasypt-1.9.3.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI algorithm=PBEWithMD5AndDES password=6782bc11b94c4c788e4b731471e6c103 input=root
 * # 2.x 命令行解密
 *   java -cp ~/.m2/repository/org/jasypt/jasypt/1.9.3/jasypt-1.9.3.jar org.jasypt.intf.cli.JasyptPBEStringDecryptionCLI algorithm=PBEWithMD5AndDES password=6782bc11b94c4c788e4b731471e6c103 input='W3f7iaUctQpHvr+nnJgaYg=='
 * </pre>
 *
 * <pre>
 * application.yml 配置说明
 *   # 加解密密钥
 *   jasypt.encryptor.password=6782bc11b94c4c788e4b731471e6c103
 *   # 默认加密方式为： PBEWithMD5AndDES
 *   # jasypt.encryptor.algorithm=PBEWithMD5AndDES
 *   spring.datasource.username=ENC(6eaMh/RX5oXUVca9ignvtg==)
 *   spring.datasource.password=ENC(6eaMh/RX5oXUVca9ignvtg==)
 * </pre>
 *
 * @author 谢长春 2022-02-03
 */
@Slf4j
public class JasyptTest {
    public static void main(String[] args) {
        // 加密方式： PBEWithMD5AndDES
        {
            final BasicTextEncryptor encryptor = new BasicTextEncryptor();
            encryptor.setPassword("6782bc11b94c4c788e4b731471e6c103"); // 加解密密钥
            log.info("BasicTextEncryptor：加密：111111：{}", encryptor.encrypt("111111"));
            log.info("BasicTextEncryptor：加密：root：{}", encryptor.encrypt("root"));
            log.info("BasicTextEncryptor：加密：passwd：{}", encryptor.encrypt("passwd"));
            log.info("BasicTextEncryptor：解密：root：{}", encryptor.decrypt("W3f7iaUctQpHvr+nnJgaYg=="));
            log.info("BasicTextEncryptor：解密：passwd：{}", encryptor.decrypt("aoKBfUiV3cavhXn+t1DmDg=="));
            log.info("BasicTextEncryptor：解密：{}", encryptor.decrypt("1/+SjBau/yFkLoHu0x1oRw=="));
        }
        // 加密方式： PBEWithMD5AndTripleDES
        {
            StrongTextEncryptor encryptor = new StrongTextEncryptor();
            encryptor.setPassword("6782bc11b94c4c788e4b731471e6c103"); // 加解密密钥
            log.info("StrongTextEncryptor：加密：111111：{}", encryptor.encrypt("111111"));
            log.info("StrongTextEncryptor：加密：root：{}", encryptor.encrypt("root"));
            log.info("StrongTextEncryptor：加密：passwd：{}", encryptor.encrypt("passwd"));
            log.info("StrongTextEncryptor：解密：root：{}", encryptor.decrypt("C3yd579oOTdWN51Ul0h82Q=="));
            log.info("StrongTextEncryptor：解密：passwd：{}", encryptor.decrypt("CwnXjd6OiQt+vZ5Nyjms4g=="));
        }
        // 3.0.0 开始默认的加密方式变更为： PBEWITHHMACSHA512ANDAES_256
        // https://github.com/ulisesbocchio/jasypt-spring-boot#update-11242019-version-300-release-includes
        {
            AES256TextEncryptor encryptor = new AES256TextEncryptor();
            encryptor.setPassword("6782bc11b94c4c788e4b731471e6c103"); // 加解密密钥
            log.info("AES256TextEncryptor：加密：111111：{}", encryptor.encrypt("111111"));
            log.info("AES256TextEncryptor：加密：root：{}", encryptor.encrypt("root"));
            log.info("AES256TextEncryptor：加密：passwd：{}", encryptor.encrypt("passwd"));
            log.info("AES256TextEncryptor：解密：root：{}", encryptor.decrypt("1asZuZbdM03fMPvX8CzESAh3p4UITybjUVjgX5blfo3o4ZX9WoywV7oE4TMbVbcA"));
            log.info("AES256TextEncryptor：解密：passwd：{}", encryptor.decrypt("SDe0YzBev29mzhLgNazQVO9NPT7KbvqzRqy8PgFNFL2Nb5GwkGb/7PSjycIClLzD"));
        }
    }
}
