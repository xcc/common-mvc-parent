package com;

import app.common.starter.util.JSON;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.collect.Sets;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by 谢长春 on 2017/10/14.
 */
@Slf4j
public class JacksonConvertTest {
    public static void main(String[] args) {
        String jsonString = JSON.toJsonString(Item.builder()
                .key("key")
                .stringValue(null)
                .stringValue("")
                .stringValue("stringValue")
                .stringArray(null)
                .stringArray(new String[]{})
                .stringArray(new String[]{""})
                .stringArray(new String[]{"stringArray value1", "stringArray value2"})
                .stringList(null)
                .stringList(Collections.emptyList())
                .stringList(Collections.singletonList(""))
                .stringList(Arrays.asList("stringList value1", "stringList value2"))
                .stringSet(null)
                .stringSet(Collections.emptySet())
                .stringSet(Sets.newHashSet(""))
                .stringSet(Sets.newHashSet("stringSet value1", "stringSet value2"))
                .longValue(null)
                .longValue(0L)
                .longValue(1000L)
                .longArray(null)
                .longArray(new Long[]{})
                .longArray(new Long[]{0L})
                .longArray(new Long[]{1001L, 1002L})
                .longList(Arrays.asList(1003L, 1004L))
                .longSet(Sets.newHashSet(1005L, 1006L))
                .build()
        );
        System.out.println(jsonString);
        System.out.println(JSON.parseObject(jsonString, Item.class));
    }

    @Getter
    @Setter
    @ToString
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Item {
        @JsonSerialize(converter = AesStringSerializer.class)
        @JsonDeserialize(converter = AesStringDeserializer.class)
        private String key;
        @JsonSerialize(converter = AesStringSerializer.class)
        @JsonDeserialize(converter = AesStringDeserializer.class)
        private String stringValue;
        @JsonSerialize(converter = AesStringArraySerializer.class)
        @JsonDeserialize(converter = AesStringArrayDeserializer.class)
        private String[] stringArray;
        @JsonSerialize(converter = AesStringListSerializer.class)
        @JsonDeserialize(converter = AesStringListDeserializer.class)
        private List<String> stringList;
        @JsonSerialize(converter = AesStringSetSerializer.class)
        @JsonDeserialize(converter = AesStringSetDeserializer.class)
        private Set<String> stringSet;
        @JsonSerialize(converter = AesLongSerializer.class)
        @JsonDeserialize(converter = AesLongDeserializer.class)
        private Long longValue;
        @JsonSerialize(converter = AesLongArraySerializer.class)
        @JsonDeserialize(converter = AesLongArrayDeserializer.class)
        private Long[] longArray;
        @JsonSerialize(converter = AesLongListSerializer.class)
        @JsonDeserialize(converter = AesLongListDeserializer.class)
        private List<Long> longList;
        @JsonSerialize(converter = AesLongSetSerializer.class)
        @JsonDeserialize(converter = AesLongSetDeserializer.class)
        private Set<Long> longSet;
    }

    public static class AesStringSerializer extends StdConverter<String, String> {

        @Override
        public String convert(String value) {
            if (Objects.isNull(value)) {
                return null;
            }
            if (StringUtils.isBlank(value)) {
                return "";
            }
            return String.format("加密(%s)", value);
        }
    }

    public static class AesStringDeserializer extends StdConverter<String, String> {

        @Override
        public String convert(String value) {
            if (Objects.isNull(value)) {
                return null;
            }
            if (StringUtils.isBlank(value)) {
                return "";
            }
            return value.replaceAll("加", "解");
        }
    }

    public static class AesStringArraySerializer extends StdConverter<String[], String[]> {

        @Override
        public String[] convert(String[] value) {
            for (int i = 0; i < value.length; i++) {
                if (StringUtils.isNoneBlank(value)) {
                    value[i] = String.format("加密(%s)", value[i]);
                }
            }
            return value;
        }
    }

    public static class AesStringArrayDeserializer extends StdConverter<String[], String[]> {

        @Override
        public String[] convert(String[] value) {
            for (int i = 0; i < value.length; i++) {
                if (StringUtils.isNoneBlank(value)) {
                    value[i] = value[i].replaceAll("加", "解");
                }
            }
            return value;
        }
    }

    public static class AesStringListSerializer extends StdConverter<List<String>, List<String>> {

        @Override
        public List<String> convert(List<String> value) {
            return value.stream().map(val -> {
                if (StringUtils.isNoneBlank(val)) {
                    return String.format("加密(%s)", val);
                }
                return val;
            }).collect(Collectors.toList());
        }
    }

    public static class AesStringListDeserializer extends StdConverter<List<String>, List<String>> {

        @Override
        public List<String> convert(List<String> value) {
            return value.stream().map(val -> {
                if (StringUtils.isNoneBlank(val)) {
                    return val.replaceAll("加", "解");
                }
                return val;
            }).collect(Collectors.toList());
        }
    }

    public static class AesStringSetSerializer extends StdConverter<Set<String>, Set<String>> {

        @Override
        public Set<String> convert(Set<String> value) {
            return value.stream().map(val -> {
                if (StringUtils.isNoneBlank(val)) {
                    return String.format("加密(%s)", val);
                }
                return val;
            }).collect(Collectors.toSet());
        }
    }

    public static class AesStringSetDeserializer extends StdConverter<Set<String>, Set<String>> {

        @Override
        public Set<String> convert(Set<String> value) {
            return value.stream().map(val -> {
                if (StringUtils.isNoneBlank(val)) {
                    return val.replaceAll("加", "解");
                }
                return val;
            }).collect(Collectors.toSet());
        }
    }

    public static class AesLongSerializer extends StdConverter<Long, String> {

        @Override
        public String convert(Long value) {
            if (Objects.isNull(value)) {
                return null;
            }
            return String.format("加密(%d)", value);
        }
    }

    public static class AesLongDeserializer extends StdConverter<String, Long> {

        @Override
        public Long convert(String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }
            return Long.parseLong(value.replaceAll("加密\\((\\d+)\\)", "$1"));
        }
    }

    public static class AesLongArraySerializer extends StdConverter<Long[], String[]> {

        @Override
        public String[] convert(Long[] value) {
            return Stream.of(value)
                    .filter(Objects::nonNull)
                    .map(val -> String.format("加密(%s)", val))
                    .toArray(String[]::new);
        }
    }

    public static class AesLongArrayDeserializer extends StdConverter<String[], Long[]> {

        @Override
        public Long[] convert(String[] value) {
            return Stream.of(value)
                    .filter(StringUtils::isNoneBlank)
                    .map(val -> Long.parseLong(val.replaceAll("加密\\((\\d+)\\)", "$1")))
                    .toArray(Long[]::new);
        }
    }

    public static class AesLongListSerializer extends StdConverter<List<Long>, List<String>> {

        @Override
        public List<String> convert(List<Long> value) {
            return value.stream().filter(Objects::nonNull).map(val -> String.format("加密(%d)", val)).collect(Collectors.toList());
        }
    }

    public static class AesLongListDeserializer extends StdConverter<List<String>, List<Long>> {

        @Override
        public List<Long> convert(List<String> value) {
            return value.stream()
                    .filter(StringUtils::isNoneBlank)
                    .map(val -> Long.parseLong(val.replaceAll("加密\\((\\d+)\\)", "$1")))
                    .collect(Collectors.toList());
        }
    }

    public static class AesLongSetSerializer extends StdConverter<Set<Long>, Set<String>> {

        @Override
        public Set<String> convert(Set<Long> value) {
            return value.stream().filter(Objects::nonNull).map(val -> String.format("加密(%d)", val)).collect(Collectors.toSet());
        }
    }

    public static class AesLongSetDeserializer extends StdConverter<Set<String>, Set<Long>> {

        @Override
        public Set<Long> convert(Set<String> value) {
            return value.stream()
                    .filter(StringUtils::isNoneBlank)
                    .map(val -> Long.parseLong(val.replaceAll("加密\\((\\d+)\\)", "$1")))
                    .collect(Collectors.toSet());
        }
    }
}
