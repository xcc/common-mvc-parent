package com;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.annotation.JSONField;
//import com.alibaba.fastjson.parser.DefaultJSONParser;
//import com.alibaba.fastjson.parser.JSONToken;
//import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
//import com.alibaba.fastjson.serializer.JSONSerializer;
//import com.alibaba.fastjson.serializer.ObjectSerializer;
//import com.google.common.base.Strings;
//import com.google.common.collect.Lists;
//import com.google.common.collect.Sets;
//import com.utils.enums.Code;
//import app.common.util.Util;
//import lombok.*;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.collections4.CollectionUtils;
//import org.apache.commons.lang3.RandomStringUtils;
//import org.apache.commons.lang3.RandomUtils;
//
//import javax.crypto.BadPaddingException;
//import javax.crypto.Cipher;
//import javax.crypto.IllegalBlockSizeException;
//import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.SecretKeySpec;
//import java.io.IOException;
//import java.lang.reflect.Type;
//import java.security.InvalidAlgorithmParameterException;
//import java.security.InvalidKeyException;
//import java.util.*;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import static java.nio.charset.StandardCharsets.UTF_8;
//
///**
// * @author 谢长春 2018/12/5 .
// */
//public class AesFastjsonConvertTest {
//    public static void main(String[] args) {
//        Aes.setSecretKey(Util.uuid32());
//        Row row = Row.builder()
//                .id(RandomUtils.nextLong(0, 10000))
//                .idArray(new Long[]{RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000)})
//                .idList(Arrays.asList(RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000)))
//                .idSet(Sets.newHashSet(RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000), RandomUtils.nextLong(0, 10000)))
//                .name("谢CC")
//                .nameArray(new String[]{RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10)})
//                .nameList(Arrays.asList(RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10)))
//                .nameSet(Sets.newHashSet(RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10), RandomStringUtils.randomAlphanumeric(10)))
//                .build();
//        final String jsonText = JSON.toJSONString(row);
//        System.out.println(jsonText);
//        System.out.println(JSON.parseObject(jsonText, Row.class));
////        System.out.println(JSON.toJSONString(row, new SerializeConfig()
////                .clearSerializers();
////        ));
//    }
//
//    @NoArgsConstructor
//    @AllArgsConstructor
//    @Data
//    @Builder
//    public static class Row {
//        @JSONField(serializeUsing = AesLongJsonConvert.Serializer.class, deserializeUsing = AesLongJsonConvert.Deserializer.class)
//        private Long id;
//        @JSONField(serializeUsing = AesLongArrayJsonConvert.Serializer.class, deserializeUsing = AesLongArrayJsonConvert.Deserializer.class)
//        private Long[] idArray;
//        @JSONField(serializeUsing = AesLongListJsonConvert.Serializer.class, deserializeUsing = AesLongListJsonConvert.Deserializer.class)
//        private List<Long> idList;
//        @JSONField(serializeUsing = AesLongSetJsonConvert.Serializer.class, deserializeUsing = AesLongSetJsonConvert.Deserializer.class)
//        private Set<Long> idSet;
//
//        @JSONField(serializeUsing = AesStringJsonConvert.Serializer.class, deserializeUsing = AesStringJsonConvert.Deserializer.class)
//        private String name;
//        @JSONField(serializeUsing = AesStringArrayJsonConvert.Serializer.class, deserializeUsing = AesStringArrayJsonConvert.Deserializer.class)
//        private String[] nameArray;
//        @JSONField(serializeUsing = AesStringListJsonConvert.Serializer.class, deserializeUsing = AesStringListJsonConvert.Deserializer.class)
//        private List<String> nameList;
//        @JSONField(serializeUsing = AesStringSetJsonConvert.Serializer.class, deserializeUsing = AesStringSetJsonConvert.Deserializer.class)
//        private Set<String> nameSet;
//    }
//
//
//    /**
//     * 用于 Json 序列化和反序列化， @JSONField(serializeUsing = AesLongArrayJsonConvert.Serializer.class, deserializeUsing = AesLongArrayJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesLongArrayJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//            @Override
//            public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                jsonSerializer.write(Aes.encrypt(Stream.of(value).map(Objects::toString).collect(Collectors.toList())));
//            }
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//            @SuppressWarnings("unchecked")
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final List<String> list = defaultJSONParser.parseArray(String.class);
//                if (Objects.isNull(list)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, list);
//                }
//                try {
//                    return (T) Aes.decrypt(list).stream().map(Long::parseLong).toArray(Long[]::new);
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(String.join(",", list));
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LBRACKET;
//            }
//        }
//
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesLongJsonConvert.Serializer.class, deserializeUsing = AesLongJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesLongJsonConvert {
//
//        public static class Serializer implements ObjectSerializer {
//
//            @Override
//            public void write(JSONSerializer serializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                serializer.write(Aes.encrypt(Objects.toString(value)));
//            }
//        }
//
//        @Slf4j
//        public static class Deserializer implements ObjectDeserializer {
//            @SuppressWarnings({"unchecked"})
//            @Override
//            public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
//                final String value = parser.getLexer().stringVal();
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, value);
//                }
//                return (T) Optional.ofNullable(Aes.decrypt(value)).map(Long::parseLong).orElse(null);
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return 0;
//            }
//        }
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesLongListJsonConvert.Serializer.class, deserializeUsing = AesLongListJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesLongListJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//            @Override
//            public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                jsonSerializer.write(Aes.encrypt(Stream.of(value).map(Objects::toString).collect(Collectors.toList())));
//            }
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//
//            @SuppressWarnings("unchecked")
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final List<String> list = defaultJSONParser.parseArray(String.class);
//                if (Objects.isNull(list)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, list);
//                }
//                try {
//                    return (T) Aes.decrypt(list).stream().map(Long::parseLong).collect(Collectors.toList());
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(String.join(",", list));
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LBRACKET;
//            }
//        }
//
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesLongSetJsonConvert.Serializer.class, deserializeUsing = AesLongSetJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesLongSetJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//            @Override
//            public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                jsonSerializer.write(Aes.encrypt(Stream.of(value).map(Objects::toString).collect(Collectors.toList())));
//            }
//
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//
//            @SuppressWarnings("unchecked")
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final List<String> list = defaultJSONParser.parseArray(String.class);
//                if (Objects.isNull(list)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, list);
//                }
//                try {
//                    return (T) Aes.decrypt(list).stream().map(Long::parseLong).collect(Collectors.toSet());
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(String.join(",", list));
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LBRACKET;
//            }
//        }
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesStringArrayJsonConvert.Serializer.class, deserializeUsing = AesStringArrayJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesStringArrayJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//            @Override
//            public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                final List<String> values = Aes.encrypt(Lists.newArrayList(((String[]) value)));
//                jsonSerializer.write(values);
//            }
//
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//
//            @SuppressWarnings("unchecked")
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final List<String> list = defaultJSONParser.parseArray(String.class);
//                if (Objects.isNull(list)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, list);
//                }
//                try {
//                    return (T) Aes.decrypt(list).toArray(new String[0]);
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(String.join(",", list));
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LBRACKET;
//            }
//        }
//
//
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesStringJsonConvert.Serializer.class, deserializeUsing = AesStringJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesStringJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//
//            @Override
//            public void write(JSONSerializer serializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                serializer.write(Aes.encrypt(Objects.toString(value)));
//            }
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//            @SuppressWarnings({"unchecked"})
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final String value = defaultJSONParser.parseObject(String.class);
//                if (Objects.isNull(value)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, value);
//                }
//                try {
//                    final Object val = Aes.decrypt(value);
//                    return (T) val;
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(value);
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LITERAL_STRING;
//            }
//        }
//
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesStringListJsonConvert.Serializer.class, deserializeUsing = AesStringListJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesStringListJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//            @SuppressWarnings("unchecked")
//            @Override
//            public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                final List<String> values = Aes.encrypt((List<String>) value);
//                jsonSerializer.write(values);
//            }
//
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//            @SuppressWarnings("unchecked")
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final List<String> list = defaultJSONParser.parseArray(String.class);
//                if (Objects.isNull(list)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, list);
//                }
//                try {
//                    return (T) Aes.decrypt(list);
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(String.join(",", list));
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LBRACKET;
//            }
//        }
//
//    }
//
//    /**
//     * 用于 JSON 序列化和反序列化， @JSONField(serializeUsing = AesStringSetJsonConvert.Serializer.class, deserializeUsing = AesStringSetJsonConvert.Deserializer.class)
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static class AesStringSetJsonConvert {
//        public static class Serializer implements ObjectSerializer {
//            @SuppressWarnings("unchecked")
//            @Override
//            public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//                if (Objects.isNull(value)) {
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("加密:{}:{}", fieldName, value);
//                }
//                final List<String> values = Aes.encrypt(Lists.newArrayList((Set<String>) value));
//                jsonSerializer.write(values);
//            }
//
//        }
//
//        public static class Deserializer implements ObjectDeserializer {
//
//            @SuppressWarnings("unchecked")
//            @Override
//            public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//                final List<String> list = defaultJSONParser.parseArray(String.class);
//                if (Objects.isNull(list)) {
//                    return null;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("解密:{}:{}", fieldName, list);
//                }
//                try {
//                    return (T) new HashSet<>(Aes.decrypt(list));
//                } catch (Exception e) {
//                    log.error(e.getMessage(), e);
//                    throw Code.A00003.toCodeException(String.join(",", list));
//                }
//            }
//
//            @Override
//            public int getFastMatchToken() {
//                return JSONToken.LBRACKET;
//            }
//        }
//
//    }
//
//    /**
//     * AES 工具类
//     * https://www.liaoxuefeng.com/wiki/1252599548343744/1304227762667553
//     *
//     * @author 谢长春 2022-02-06
//     */
//    @Slf4j
//    public static final class Aes {
//
//        /**
//         * https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#trans
//         */
//        private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
//        /**
//         * 参考文档: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
//         * <pre>
//         * Cipher (Encryption) Algorithms
//         * Cipher Algorithm Names
//         * The following names can be specified as the algorithm component in a transformation when requesting an instance of Cipher.
//         *
//         * <table border="5" cellpadding="5" frame="border" width="90%" summary="Cipher Algorithm Names">
//         * <thead> <tr> <th>Algorithm Name</th> <th>Description</th> </tr> </thead>
//         * <tbody>
//         * <tr> <td>AES</td> <td>Advanced Encryption Standard as specified by NIST in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS 197</a>. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits.</td> </tr>
//         * <tr> <td>AESWrap</td> <td>The AES key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3394.txt">RFC 3394</a>.</td> </tr>
//         * <tr> <td>ARCFOUR</td> <td>A stream cipher believed to be fully interoperable with the RC4 cipher developed by Ron Rivest. For more information, see K. Kaukonen and R. Thayer, "A Stream Cipher Encryption Algorithm 'Arcfour'", Internet Draft (expired), <a href="http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt"> draft-kaukonen-cipher-arcfour-03.txt</a>.</td> </tr>
//         * <tr> <td>Blowfish</td> <td>The <a href="http://www.schneier.com/blowfish.html">Blowfish block cipher</a> designed by Bruce Schneier.</td> </tr>
//         * <tr> <td>CCM</td> <td>Counter/CBC Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38C/SP800-38C_updated-July20_2007.pdf">NIST Special Publication SP 800-38C</a>.</td> </tr>
//         * <tr> <td>DES</td> <td>The Digital Encryption Standard as described in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS PUB 46-3</a>.</td> </tr>
//         * <tr> <td>DESede</td> <td>Triple DES Encryption (also known as DES-EDE, 3DES, or Triple-DES). Data is encrypted using the DES algorithm three separate times. It is first encrypted using the first subkey, then decrypted with the second subkey, and encrypted with the third subkey.</td> </tr>
//         * <tr> <td>DESedeWrap</td> <td>The DESede key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3217.txt">RFC 3217</a> .</td> </tr>
//         * <tr> <td>ECIES</td> <td>Elliptic Curve Integrated Encryption Scheme</td> </tr>
//         * <tr> <td>GCM</td> <td>Galois/Counter Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38D/SP-800-38D.pdf">NIST Special Publication SP 800-38D</a>.</td> </tr>
//         * <tr> <td>PBEWith&lt;digest&gt;And&lt;encryption&gt; PBEWith&lt;prf&gt;And&lt;encryption&gt;</td> <td>The password-based encryption algorithm found in (PKCS5), using the specified message digest (&lt;digest&gt;) or pseudo-random function (&lt;prf&gt;) and encryption algorithm (&lt;encryption&gt;). Examples: <ul> <li><b>PBEWithMD5AndDES</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, Nov 1993</a>. Note that this algorithm implies <a href="#cbcMode"><i>CBC</i></a> as the cipher mode and <a href="#pkcs5Pad"><i>PKCS5Padding</i></a> as the padding scheme and cannot be used with any other cipher modes or padding schemes.</li> <li><b>PBEWithHmacSHA256AndAES_128</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Cryptography Standard," version 2.0, March 1999</a>.</li> </ul> </td> </tr>
//         * <tr> <td>RC2</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
//         * <tr> <td>RC4</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc. (See note prior for ARCFOUR.)</td> </tr>
//         * <tr> <td>RC5</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
//         * <tr> <td>RSA</td> <td>The RSA encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2125">PKCS #1</a></td> </tr>
//         * </tbody>
//         * </table>
//         */
//        private static final String CIPHER_ALGORITHM = "AES";
//        /**
//         *
//         */
//        private static final int IV_LENGTH = 16;
//        private static final int ZERO = 0;
//        /**
//         * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
//         */
//        public static IvParameterSpec IV_PARAMETER;
//
//        /**
//         * 密钥， 长度:32字节(256位)
//         *
//         * @alias RandomStringUtils.randomAlphanumeric(32)
//         * @alias RandomStringUtils.randomAscii(32)
//         */
//        public static SecretKeySpec SECRET_KEY;
//
//        private Aes() {
//
//        }
//
//        /**
//         * 设置密钥
//         *
//         * @param secretKey {@link String} RandomStringUtils.randomAlphanumeric(32)
//         */
//        public static void setSecretKey(final String secretKey) {
//            if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
//                throw new IllegalArgumentException("secretKey 长度必须是 32 位");
//            }
//            SECRET_KEY = new SecretKeySpec(secretKey.getBytes(UTF_8), CIPHER_ALGORITHM);
//        }
//
//        /**
//         * 设置 IV ， 长度必须是 16 位自负
//         *
//         * @param iv {@link String} RandomStringUtils.randomAlphanumeric(16)
//         */
//        @SneakyThrows
//        public static void setIV(final String iv) {
//            if (Objects.isNull(iv)) {
//                IV_PARAMETER = null;
//                return;
//            }
//            if (Strings.isNullOrEmpty(iv) || iv.length() != 16) {
//                throw new IllegalArgumentException("secretKey 长度必须是 16 位");
//            }
//            IV_PARAMETER = new IvParameterSpec(iv.getBytes(UTF_8));
//        }
//
//        /**
//         * <pre>
//         * aes 加密， 有两种模式：
//         * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
//         * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
//         * </pre>
//         *
//         * @param data {@link String} 明文
//         * @return {@link String} 密文
//         */
//        @SneakyThrows
//        public static String encrypt(final String data) {
//            if (Strings.isNullOrEmpty(data)) {
//                return null;
//            }
//            if (Objects.isNull(IV_PARAMETER)) { // 未指定 IV 使用动态 IV 加密
//                // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
//                // CBC模式需要生成一个16 bytes的initialization vector:
//                // final SecureRandom sr = SecureRandom.getInstanceStrong();
//                // final byte[] ivBytes = sr.generateSeed(16);
//
//
//                final byte[] ivBytes = RandomStringUtils.randomAlphanumeric(IV_LENGTH).getBytes();
//                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//                // 初始化加密参数，设置为加密模式，指定密钥，设置IV
//                cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//                final byte[] dataBytes = cipher.doFinal(data.getBytes(UTF_8));
//                final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
//                System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
//                System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
////            final String result = org.apache.commons.codec.binary.Hex.encodeHexString(bytes);
//                final String result = org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
//                if (log.isDebugEnabled()) {
//                    log.debug("{} => {}", data, result);
//                }
//                return result;
//            }
//            // 使用固定 IV 加密
//            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV_PARAMETER);
////        final String result = org.apache.commons.codec.binary.Hex.encodeHexString(cipher.doFinal(data.getBytes(UTF_8)));
//            final String result = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(data.getBytes(UTF_8)));
//            if (log.isDebugEnabled()) {
//                log.debug("{} => {}", data, result);
//            }
//            return result;
//        }
//
//        /**
//         * <pre>
//         * aes 加密， 有两种模式：
//         * 动态IV： （默认），相同内容每次获得的密文不一样， 可自定义 IV 生成规则
//         * 固定IV： 通过设置 {@link Aes#setIV(String)} ，固定 IV 之后相同内容每次获得的密文都一样
//         * </pre>
//         *
//         * @param list {@link Collection<String>} 明文
//         * @return {@link String} 密文
//         */
//        @SneakyThrows
//        public static List<String> encrypt(final Collection<String> list) {
//            if (CollectionUtils.isEmpty(list)) {
//                return Collections.emptyList();
//            }
//            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 加密
//                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//                return list.stream()
//                        .map(data -> {
//                            try {
//                                // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
//                                final byte[] ivBytes = RandomStringUtils.randomAlphanumeric(IV_LENGTH).getBytes();
//                                cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//
//                                final byte[] dataBytes = cipher.doFinal(data.getBytes(UTF_8));
//                                final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
//                                System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
//                                System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
//                                // final String result = org.apache.commons.codec.binary.Hex.encodeHexString(bytes);
//                                final String result = org.apache.commons.codec.binary.Base64.encodeBase64String(bytes);
//                                if (log.isDebugEnabled()) {
//                                    log.debug("{} => {}", data, result);
//                                }
//                                return result;
//                            } catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException |
//                                     BadPaddingException e) {
//                                throw new IllegalArgumentException(String.format("加密失败:%s", data), e);
//                            }
//                        })
//                        .collect(Collectors.toList());
//            }
//            // 使用固定的 IV 加密
//            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            cipher.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV_PARAMETER);
//            return list.stream()
//                    .map(data -> {
//                        try {
//                            // final String result = org.apache.commons.codec.binary.Hex.encodeHexString(cipher.doFinal(data.getBytes(UTF_8)));
//                            final String result = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(data.getBytes(UTF_8)));
//                            if (log.isDebugEnabled()) {
//                                log.debug("{} => {}", data, result);
//                            }
//                            return result;
//                        } catch (IllegalBlockSizeException | BadPaddingException e) {
//                            throw new IllegalArgumentException(String.format("加密失败:%s", data), e);
//                        }
//                    })
//                    .collect(Collectors.toList());
//        }
//
//        /**
//         * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
//         *
//         * @param data {@link String} 密文
//         * @return 明文
//         */
//        @SneakyThrows
//        public static String decrypt(final String data) {
//            if (Strings.isNullOrEmpty(data)) {
//                return null;
//            }
//            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
//                // 将 data 分割成 IV 和密文
//                // final byte[] bytes = org.apache.commons.codec.binary.Hex.decodeHex(data);
//                final byte[] bytes = org.apache.commons.codec.binary.Base64.decodeBase64(data);
//                final byte[] ivBytes = new byte[IV_LENGTH];
//                final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
//                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
//                System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);
//
//                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//                cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//                final String result = new String(cipher.doFinal(dataBytes));
//                if (log.isDebugEnabled()) {
//                    log.debug("{}({}) => {}", data, new String(ivBytes, UTF_8), result);
//                }
//                return result;
//            }
//            // 使用固定的 IV 解密
//            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
//            // final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Hex.decodeHex(data)));
//            final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data)));
//            if (log.isDebugEnabled()) {
//                log.debug("{} => {}", data, result);
//            }
//            return result;
//        }
//
//        /**
//         * 解密，如果是动态 IV，则会校验 IV 规则是否正确，不正确会抛出异常
//         *
//         * @param list {@link Collection<String>} 密文
//         * @return 明文
//         */
//        @SneakyThrows
//        public static List<String> decrypt(final Collection<String> list) {
//            if (CollectionUtils.isEmpty(list)) {
//                return Collections.emptyList();
//            }
//            if (Objects.isNull(IV_PARAMETER)) { // 未指定 iv 使用动态 iv 解密
//                final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//                final byte[] ivBytes = new byte[IV_LENGTH];
//                return list.stream()
//                        .map(data -> {
//                            try {
//                                // 将 data 分割成 IV 和密文
//                                // final byte[] bytes = org.apache.commons.codec.binary.Hex.decodeHex(data);
//                                final byte[] bytes = org.apache.commons.codec.binary.Base64.decodeBase64(data);
//                                System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
//                                cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//
//                                final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
//                                System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);
//                                final String result = new String(cipher.doFinal(dataBytes));
//                                if (log.isDebugEnabled()) {
//                                    log.debug("{} => {}", data, result);
//                                }
//                                return result;
//                            } catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException |
//                                     BadPaddingException e) {
//                                throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
//                            }
//                        })
//                        .collect(Collectors.toList());
//
//            }
//            // 使用固定的 IV 解密
//            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
//            cipher.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV_PARAMETER);
//            return list.stream()
//                    .map(data -> {
//                        try {
//                            // final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Hex.decodeHex(data)));
//                            final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data)));
//                            if (log.isDebugEnabled()) {
//                                log.debug("{} => {}", data, result);
//                            }
//                            return result;
//                        } catch (IllegalBlockSizeException | BadPaddingException e) {
//                            throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
//                        }
//                    })
//                    .collect(Collectors.toList());
//        }
//
//        @SneakyThrows
//        public static void main(String[] args) {
//            final String secretKey = "VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF";
//            Aes.setSecretKey(secretKey);
//
//            // 动态 IV
//            {
//                final String planText = "java 加密";
//                log.info("动态 IV: 明文：{} => 加密：{}", planText, Aes.encrypt(planText));
//                log.info("动态 IV: 明文：{} => 加密：{}", planText, Aes.encrypt(planText));
//
//                String encryptText = Aes.encrypt("java 加密");
//                log.info("动态 IV: 明文：{} => 加密：{} => 解密：{}", planText, encryptText, Aes.decrypt(encryptText));
//                encryptText = Aes.encrypt("java 加密");
//                log.info("动态 IV: 明文：{} => 加密：{} => 解密：{}", planText, encryptText, Aes.decrypt(encryptText));
//            }
//
////        for (int i = 0; i < 100; i++) {
////            System.out.println(Aes.encrypt(Util.uuid32()));
////        }
//
//            final String iv = RandomStringUtils.randomAlphanumeric(16);
//            // 固定 IV
//            Aes.setIV("2022010100112299");
//            log.info("固定iv：{}", iv);
//            {
//                final String planText = "java 加密";
//                log.info("固定 IV: 明文：{} => 加密：{}", planText, Aes.encrypt(planText));
//                log.info("固定 IV: 明文：{} => 加密：{}", planText, Aes.encrypt(planText));
//
//                String encryptText = Aes.encrypt("java 加密");
//                log.info("固定 IV: 明文：{} => 加密：{} => 解密：{}", planText, encryptText, Aes.decrypt(encryptText));
//                encryptText = Aes.encrypt("java 加密");
//                log.info("固定 IV: 明文：{} => 加密：{} => 解密：{}", planText, encryptText, Aes.decrypt(encryptText));
//            }
//
//            {
//                setIV("2022010100112299");
//                log.info("js 静态IV密文解密 => {}", Aes.decrypt("/IcOIjPiIvm3oKbNISzwdonBPfcHE4vxUiP22KTXdlU="));
//            }
//            {
//                setIV(null);
//                log.info("js 动态IV密文解密 => {}", Aes.decrypt("MDAwMTY0NTQ0NDYyODY0OQOK1jx9D1vA+fy4VW7f2e8od4X3klGeQx67JB/uiIMC"));
//            }
//        }
//    }
//
//}
