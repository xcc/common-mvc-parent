package com;


import com.google.common.io.ByteStreams;
import com.google.common.io.Closer;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static app.common.starter.util.Dates.Pattern.HHmmssSSS;

@Slf4j
public class GuavaFileDownloadTest {
    @SneakyThrows
    public static void main(String[] args) {
        final String url = "https://image.baidu.com/search/down?tn=download&word=download&ie=utf8&fr=detail&url=https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.jj20.com%2Fup%2Fallimg%2F1112%2F012019115430%2F1Z120115430-7.jpg&refer=http%3A%2F%2Fpic.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1655021528&t=8d6023d31ca02592f6bd3f103c456fa5&thumburl=https://img2.baidu.com/it/u=4084621093,2971972319&fm=253&fmt=auto&app=120&f=JPEG?w=889&h=500";

        System.out.println(HHmmssSSS.now());
        @Cleanup Closer closer = Closer.create();
//        String fileName = StringUtils.substringBefore(url, "?");
//        fileName = StringUtils.substringAfterLast(fileName, "/");
        @Cleanup final InputStream in = closer.register(new URL(url).openStream());
        @Cleanup final OutputStream outputStream = Files.newOutputStream(
                Paths.get(".temp", "down.jpeg")
                , StandardOpenOption.CREATE
        );
        ByteStreams.copy(in, outputStream);
        System.out.println(HHmmssSSS.now());
    }
}
