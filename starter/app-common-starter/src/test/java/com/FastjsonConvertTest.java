package com;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.annotation.JSONField;
//import com.alibaba.fastjson.parser.DefaultJSONParser;
//import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
//import com.alibaba.fastjson.serializer.JSONSerializer;
//import com.alibaba.fastjson.serializer.ObjectSerializer;
//import com.google.common.collect.Sets;
//import com.utils.enums.Code;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.IOException;
//import java.lang.reflect.Type;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Objects;
//import java.util.Set;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
///**
// * Created by 谢长春 on 2017/10/14.
// */
//@Slf4j
//public class FastjsonConvertTest {
//    public static void main(String[] args) {
//        String jsonString = JSON.toJSONString(Item.builder()
//                .key("key")
//                .stringValue("stringValue")
//                .stringSet(Sets.newHashSet("stringSet value1", "stringSet value2"))
//                .stringList(Arrays.asList("stringList value1", "stringList value2"))
//                .stringArray(new String[]{"stringArray value1", "stringArray value2"})
//                .longValue(1000L)
//                .longSet(Sets.newHashSet(1001L, 1002L))
//                .longList(Arrays.asList(1003L, 1004L))
//                .longArray(new Long[]{1005L, 1006L})
//                .build()
//        );
//        System.out.println(jsonString);
//        System.out.println(JSON.parseObject(jsonString, Item.class));
//    }
//
//    @Data
//    @Builder
//    @NoArgsConstructor
//    @AllArgsConstructor
//    public static class Item {
//        @JSONField(serializeUsing = AesStringSerializer.class, deserializeUsing = AesStringDeserializer.class)
//        private String key;
//        @JSONField(serializeUsing = AesStringSerializer.class, deserializeUsing = AesStringDeserializer.class)
//        private String stringValue;
//        @JSONField(serializeUsing = AesStringSetSerializer.class, deserializeUsing = AesStringSetDeserializer.class)
//        private Set<String> stringSet;
//        @JSONField(serializeUsing = AesStringListSerializer.class, deserializeUsing = AesStringListDeserializer.class)
//        private List<String> stringList;
//        @JSONField(serializeUsing = AesStringArraySerializer.class, deserializeUsing = AesStringArrayDeserializer.class)
//        private String[] stringArray;
//        @JSONField(serializeUsing = AesLongSerializer.class, deserializeUsing = AesLongDeserializer.class)
//        private Long longValue;
//        @JSONField(serializeUsing = AesLongSetSerializer.class, deserializeUsing = AesLongSetDeserializer.class)
//        private Set<Long> longSet;
//        @JSONField(serializeUsing = AesLongListSerializer.class, deserializeUsing = AesLongListDeserializer.class)
//        private List<Long> longList;
//        @JSONField(serializeUsing = AesLongArraySerializer.class, deserializeUsing = AesLongArrayDeserializer.class)
//        private Long[] longArray;
//    }
//
//    public static class AesStringSerializer implements ObjectSerializer {
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            String text = "加密：" + Objects.toString(value);
//            System.out.println(text);
//            jsonSerializer.write(text);
//        }
//    }
//
//    public static class AesStringDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final String value = defaultJSONParser.parseObject(String.class);
//            if (Objects.isNull(value)) {
//                return null;
//            }
//            try {
//                final Object val = value.replace("加密：", "");
//                return (T) val;
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(value);
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//    public static class AesStringArraySerializer implements ObjectSerializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            String[] values = Stream.of((String[]) value).map(val -> "加密：" + val).toArray(String[]::new);
//            System.out.println(String.join(",", values));
//            jsonSerializer.write(values);
//        }
//    }
//
//    public static class AesStringArrayDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            try {
//                return (T) list.stream()
//                        .map(val -> val.replace("加密：", ""))
//                        .toArray(String[]::new);
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(String.join(",", list));
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//    public static class AesStringListSerializer implements ObjectSerializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            List<String> values = ((List<String>) value).stream().map(val -> "加密：" + val).collect(Collectors.toList());
//            System.out.println(values);
//            jsonSerializer.write(values);
//        }
//    }
//
//    public static class AesStringListDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            try {
//                return (T) list.stream()
//                        .map(val -> val.replace("加密：", ""))
//                        .collect(Collectors.toList());
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(String.join(",", list));
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//    public static class AesStringSetSerializer implements ObjectSerializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            Set<String> values = ((Set<String>) value).stream().map(val -> "加密：" + val).collect(Collectors.toSet());
//            System.out.println(values);
//            jsonSerializer.write(values);
//        }
//    }
//
//    public static class AesStringSetDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            try {
//                return (T) list.stream()
//                        .map(val -> val.replace("加密：", ""))
//                        .collect(Collectors.toSet());
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(String.join(",", list));
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//
//    public static class AesLongSerializer implements ObjectSerializer {
//
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            String text = "加密：" + Objects.toString(value);
//            System.out.println(text);
//            jsonSerializer.write(text);
//        }
//    }
//
//    public static class AesLongDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final String value = defaultJSONParser.parseObject(String.class);
//            if (Objects.isNull(value)) {
//                return null;
//            }
//            try {
//                System.out.println(value);
//                System.out.println(value.replace("加密：", ""));
//                final Object val = Long.parseLong(value.replace("加密：", ""));
//                return (T) val;
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(value);
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//    public static class AesLongArraySerializer implements ObjectSerializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            String[] values = Stream.of((Long[]) value).map(val -> "加密：" + val).toArray(String[]::new);
//            System.out.println(String.join(",", values));
//            jsonSerializer.write(values);
//        }
//    }
//
//    public static class AesLongArrayDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            try {
//                return (T) list.stream()
//                        .map(val -> val.replace("加密：", ""))
//                        .map(Long::parseLong)
//                        .toArray(Long[]::new);
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(String.join(",", list));
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//    public static class AesLongListSerializer implements ObjectSerializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            List<String> values = ((List<Long>) value).stream().map(val -> "加密：" + val).collect(Collectors.toList());
//            System.out.println(values);
//            jsonSerializer.write(values);
//        }
//    }
//
//    public static class AesLongListDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            try {
//                return (T) list.stream()
//                        .map(val -> val.replace("加密：", ""))
//                        .map(Long::parseLong)
//                        .collect(Collectors.toList());
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(String.join(",", list));
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//
//    public static class AesLongSetSerializer implements ObjectSerializer {
//        @SuppressWarnings("unchecked")
//        @Override
//        public void write(JSONSerializer jsonSerializer, Object value, Object fieldName, Type fieldType, int features) throws IOException {
//            if (Objects.isNull(value)) {
//                return;
//            }
//            Set<String> values = ((Set<Long>) value).stream().map(val -> "加密：" + val).collect(Collectors.toSet());
//            System.out.println(values);
//            jsonSerializer.write(values);
//        }
//    }
//
//    public static class AesLongSetDeserializer implements ObjectDeserializer {
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public <T> T deserialze(DefaultJSONParser defaultJSONParser, Type fieldType, Object fieldName) {
//            final List<String> list = defaultJSONParser.parseArray(String.class);
//            if (Objects.isNull(list)) {
//                return null;
//            }
//            try {
//                return (T) list.stream()
//                        .map(val -> val.replace("加密：", ""))
//                        .map(Long::parseLong)
//                        .collect(Collectors.toSet());
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                throw Code.A00003.toCodeException(String.join(",", list));
//            }
//        }
//
//        @Override
//        public int getFastMatchToken() {
//            return 0;
//        }
//    }
//}
