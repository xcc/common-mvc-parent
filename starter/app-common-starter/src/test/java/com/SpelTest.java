package com;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * SPEL 表达式测试
 * 参考文档：http://itmyhome.com/spring/expressions.html
 *
 * @author 谢长春 2022-09-09
 */
public class SpelTest {
    private static final SpelExpressionParser SPEL_EXPRESSION_PARSER;

    static {
        SPEL_EXPRESSION_PARSER = new SpelExpressionParser();
    }

    public static void main(String[] args) {
        final EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("key", "A0");
        context.setVariable("value", 1);
        context.setVariable("hidden", true);
        context.setVariable("deleted", false);
        String spel = "#key == 'A0' and #value > 0";
        final Boolean isMatch = SPEL_EXPRESSION_PARSER.parseExpression(spel).getValue(context, Boolean.class);
        System.out.println("isMatch: " + isMatch);
        System.out.println("hidden: " + SPEL_EXPRESSION_PARSER.parseExpression("#hidden").getValue(context, Boolean.class));
        System.out.println("hidden and deleted: " + SPEL_EXPRESSION_PARSER.parseExpression("#hidden && #deleted").getValue(context, Boolean.class));
    }
}
