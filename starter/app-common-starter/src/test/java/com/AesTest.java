package com;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * AES 加解密
 *
 * @author 谢长春 2022-05-25
 */
@Slf4j
public class AesTest {

    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final SecretKeySpec SK;
    private static final IvParameterSpec IV;
//    private static final Cipher cipher;
//    private static final Cipher ENCRYPT_MODE;
//    private static final Cipher DECRYPT_MODE;

    static {
        try {
            final String sk = DigestUtils.md5Hex(AesTest.class.getName());
            SK = new SecretKeySpec(sk.getBytes(), "AES");
            IV = new IvParameterSpec(StringUtils.right(sk, 16).getBytes());

//            cipher =  Cipher.getInstance(TRANSFORMATION);
//            ENCRYPT_MODE =  Cipher.getInstance(TRANSFORMATION);
//            ENCRYPT_MODE.init(Cipher.ENCRYPT_MODE, SK, IV);
//            DECRYPT_MODE =  Cipher.getInstance(TRANSFORMATION);
//            DECRYPT_MODE.init(Cipher.DECRYPT_MODE, SK, IV);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        System.out.println(encrypt("999999999999999"));
        System.out.println(encrypt("AAAAAAAAAAAAAAA"));
        System.out.println(decrypt("gKwVVTi0P-MOpArlwRao0A"));
        System.out.println(decrypt("Zy6jGAl0gpqbWURz8w1ScQ"));
        final ExecutorService executorService = Executors.newFixedThreadPool(16);
        for (int i = 0; i < 10000; i++) {
            // Cipher 线程安全测试
            // 结论： Cipher 只能作为局部变量，如果作为全局常量，多线程情况会报错
            executorService.execute(() -> System.out.println(decrypt("Zy6jGAl0gpqbWURz8w1ScQ")));
        }
    }

    /**
     * aes 加密
     *
     * @param data {@link String} 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encrypt(final String data) {
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, SK, IV);
//        final String result = org.apache.commons.codec.binary.Hex.encodeHexString(cipher.doFinal(data.getBytes()));
        final String result = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(cipher.doFinal(data.getBytes()));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", data, result);
        }
        return result;
    }

    /**
     * 解密
     *
     * @param data {@link String} 密文
     * @return 明文
     */
    @SneakyThrows
    public static String decrypt(final String data) {
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, SK, IV);
//        final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Hex.decodeHex(data)));
        final String result = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data)));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", data, result);
        }
        return result;
    }

}
