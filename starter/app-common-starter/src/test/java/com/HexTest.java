package com;

import app.common.starter.util.Dates;
import com.google.common.base.Strings;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.math.BigInteger;

import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmssSSS;

/**
 * @author 谢长春 2022-06-22
 */
@Slf4j
public class HexTest {

    @SneakyThrows
    public static void main(String[] args) {
        System.out.println(Long.toHexString(Long.parseLong(yyyyMMddHHmmssSSS.now())) + RandomStringUtils.randomAlphanumeric(6));
        System.out.println(Hex.encodeHexString("20991231235959".getBytes()));
        System.out.println(new String(Hex.decodeHex("3230393931323331323335393539")));
        System.out.println(Hex.encodeHexString("20991231235959999".getBytes()));
        System.out.println(new String(Hex.decodeHex("3230393931323331323335393539393939")));
        System.out.println("1 ***************************************************************************************");
        System.out.println(Long.toHexString(20991231235959L));
        System.out.println(Dates.now().toTimeSecond() + ": " + Long.toHexString(Dates.now().toTimeSecond()));
        System.out.println(Dates.now().addYear(99).toTimeSecond() + ": " + Long.toHexString(Dates.now().addYear(99).toTimeSecond()));
        System.out.println(Dates.now().addYear(999).toTimeSecond() + ": " + Long.toHexString(Dates.now().addYear(999).toTimeSecond()));
        System.out.println(Long.toHexString(20991231235959999L));
        System.out.println(Dates.now().toTimeMillis() + ": " + Long.toHexString(Dates.now().toTimeMillis()));
        System.out.println(Dates.now().addYear(99).toTimeMillis() + ": " + Long.toHexString(Dates.now().addYear(99).toTimeMillis()));
        System.out.println(Dates.now().addYear(999).toTimeMillis() + ": " + Long.toHexString(Dates.now().addYear(999).toTimeMillis()));
        System.out.println(RandomUtils.nextInt(1, 999999999));
        System.out.println(Base64.encodeBase64URLSafeString(RandomUtils.nextBytes(10)));
        for (int i = 1; i < 16; i++) {
            System.out.println(String.format("%02d: ", i) + new String(Base64.encodeInteger(new BigInteger(Strings.repeat("9", i)))));
        }
        System.out.println("2 ***************************************************************************************");
        for (int i = 0; i < 10000; i++) {
            System.out.println(new String(Base64.encodeInteger(new BigInteger(RandomStringUtils.randomNumeric(9)))).replace("=", ""));
        }
        System.out.println("3 ***************************************************************************************");
        for (int i = 0; i < 10000; i++) {
            System.out.println(RandomStringUtils.randomNumeric(9));
        }
        System.out.println("4 ***************************************************************************************");
        for (int i = 0; i < 10000; i++) {
            System.out.println(RandomStringUtils.randomAlphanumeric(6));
        }
    }

}
