package com;

import app.common.starter.enums.Code;
import app.common.starter.exception.CodeException;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 自定义异常使用例子
 *
 * @author 谢长春 2022-03-02
 */
@Slf4j
public class CodeExceptionTest {
    public static void main(String[] args) {
        // *************************************************************************************************************
        // 警告：禁止使用 e.printStackTrace(); 输出异常日志
        // A00002: 用户操作不对，引起的异常。 比如页面数据过期，提醒用户刷新页面之后就能解决问题，应该用 A00002 抛出异常
        // A00003: 非用户操作问题，bug引起的异常。 比如少传参数，导致接口参数校验失败的，用户没办法解决，需要弹出错误代码，提醒用户联系运营人员
        // *************************************************************************************************************

        // 全局通用异常 **************************************************************************************************
        try {
            // 抛出自定义异常：全局通用
            throw Code.A00001.toCodeException();
        } catch (CodeException e) {
            // 最后一个参数一定要传 Exception 实例，且不需要占位符， 用于输出堆栈信息
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            // 抛出自定义异常：针对特定场景的异常提示
            throw Code.A00004.toCodeException();
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            throw Code.A00001.toCodeException("自定义异常提示");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            throw Code.A00001.toCodeException("自定义异常提示，带参数占位符。 行号【%d】字符串【%s】无效", 1, "字符串");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            throw Code.A00002.toCodeException("自定义异常消息，抛给前端用户的消息提示");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            throw Code.A00002.toCodeException("自定义异常消息，抛给前端用户的消息提示，带参数占位符。 行号【%d】字符串【%s】无效", 1, "字符串");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }

        // 特定场景抛出明确异常代码 ****************************************************************************************
        try {
            throw Code.A00011.toCodeException();
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            throw Code.A00011.toCodeException("账号【%s】已经在其他设备登录", "admin");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }

        // 异常断言 ****************************************************************************************
        try {
            Long id = null;
            Code.A00002.assertNonNull(id, "主键不能为空");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            String phone = "";
            Code.A00002.assertNonBlank(phone, "手机号不能为空");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            Long id = 0L;
            Code.A00002.assertHasTrue(id > 0, "主键必须大于0", id);
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            String phone = "18812345678";
            Code.A00002.assertHasFalse(Objects.equals("18812345678", phone), "手机号【%s】已存在", phone);
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            final List<Object> list = Collections.emptyList();
            Code.A00002.assertNonEmpty(list, "list集合不能为空");
        } catch (CodeException e) {
            log.error("{}:{}", e.getCode(), e.getMessage(), e);
        }
        try {
            Optional.empty().orElseThrow(NullPointerException::new);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        try {
            Optional.empty().orElseThrow(Code.A10001::toCodeException);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        try {
            Optional.empty().orElseThrow(() -> Code.A00002.toCodeException("订单【%s】已过期", "88888888"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
