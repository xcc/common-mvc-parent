package com;

import app.common.starter.entity.Item;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class GroupingByTest {
    public static void main(String[] args) {
        {
            final List<Item> list = Lists.newArrayList(
                    new Item().setKey("A").setComment("AA1")
                    , new Item().setKey("A").setComment("AA2")
                    , new Item().setKey("B").setComment("BB2")
                    , new Item().setKey("B").setComment("BB1")

            );
            final Map<String, String> groupByKey = list.stream().collect(Collectors.groupingBy(
                    Item::getKey
                    , Collectors.collectingAndThen( // 取满足条件的第 一 个值
                            Collectors.reducing((a, b) -> a)
                            , row -> row.map(Item::getComment).orElse("")
                    )
            ));
            groupByKey.forEach((key, value) -> log.info("{}: {}", key, value));
        }
    }
}
