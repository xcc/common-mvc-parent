package app.common.starter;
// spring.factories
// =com.app.common.starter.CommonConfiguration
//

import app.common.starter.entity.ResponseResult;
import app.common.starter.enums.AppEnv;
import app.common.starter.util.Util;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.sleuth.instrument.async.TraceableExecutorService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableConfigurationProperties
@EnableAsync
@ConfigurationProperties("spring.app")
@ComponentScan("app.common.starter")
public class CommonConfiguration {
    private final ApplicationContext applicationContext;
    /**
     * 服务id，启动时由 redisson 生成
     */
    @Getter
    @Setter
    private long id;
    /**
     * 当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]
     */
    @Getter
    @Setter
    private AppEnv env;
    /**
     * 应用域名
     */
    @Getter
    @Setter
    private String domain;
    /**
     * 主机ip
     */
    @Getter
    @Setter
    private String hostIp;
    /**
     * 主机名
     */
    @Getter
    @Setter
    private String hostName;
    /**
     * 应用名
     */
    @Getter
    @Setter
    private String name;
    /**
     * 当前运行版本号: '@version@'
     */
    @Getter
    @Setter
    private String version;
    /**
     * 项目编译打包时间，发布完成之后检查打包时间是否是一致的，避免版本更新失败: '@build.time@'
     */
    @Getter
    @Setter
    private String buildTime;
    /**
     * git 提交记录 id 前 8 位: '@git.commit.id.abbrev@'
     */
    @Getter
    @Setter
    private String gitCommitId;
    /**
     * 项目启动时间
     */
    @Getter
    @Setter
    private String startTime;

    @PostConstruct
    public void postConstruct() {
        final InetAddress inetAddress = Util.getInetAddress();
        hostIp = inetAddress.getHostAddress();
        hostName = inetAddress.getHostName();
    }

    // Result.java 序列化时排除 exception 属性
    private static final String EXCEPTION_EXCLUDE_FILTER = "ResultExceptionExcludeFilter";

    // Result.java 序列化时排除 exception 属性
    @JsonFilter(EXCEPTION_EXCLUDE_FILTER)
    interface ResultExceptionExcludeFilter {
    }

    /**
     * api 请求响应 JSON 数据序列化和反序列化规则定义
     *
     * @return {@link ObjectMapper}
     */
    @Bean
    public ObjectMapper objectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper(
                JsonFactory.builder()
                        .enable(JsonReadFeature.ALLOW_JAVA_COMMENTS)  // 允许 '//' 注释
                        .enable(JsonReadFeature.ALLOW_YAML_COMMENTS) // 允许 '#' 注释
                        .build()
        )
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) // 反序列化忽略 java 类中不存在的字段
                .enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES) // 反序列化 null 值忽略
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS) // bean序列化为空时不会异常失败
                .setSerializationInclusion(JsonInclude.Include.NON_NULL) // 序列化不返回 null 值
                ;
        if (Objects.equals(AppEnv.prod.name(), env)) { // 生产环境不返回 exception 内容
            objectMapper.setFilterProvider(
                    new SimpleFilterProvider().addFilter(EXCEPTION_EXCLUDE_FILTER, SimpleBeanPropertyFilter.serializeAllExcept("exception")) // 序列化时排除 exception 字段
                    //new SimpleFilterProvider().addFilter(EXCEPTION_EXCLUDE_FILTER, SimpleBeanPropertyFilter.filterOutAllExcept("code", "message")) // 序列化时只返回 code、message 字段
//                new SimpleFilterProvider().addFilter(EXCEPTION_EXCLUDE_FILTER, SimpleBeanPropertyFilter.serializeAll())
            );
            objectMapper.addMixIn(ResponseResult.class, ResultExceptionExcludeFilter.class);
        }
        return objectMapper;
    }

    /**
     * 多线程管理
     *
     * @return ExecutorService
     */
    @Bean(name = "multiThreadExecutorService", destroyMethod = "shutdownNow")
    public ExecutorService multiThreadExecutorService() {
        // http://www.saily.top/2018/12/29/springcloud/sleuth-lost-traceId/
        // https://cloud.spring.io/spring-cloud-static/spring-cloud-sleuth/1.3.4.RELEASE/single/spring-cloud-sleuth.html#_asynchronous_communication
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                16,
                16 * 4,
                1L,
                TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(55555), // 线程池队列，超过 55555 个任务将会抛出异常
                new ThreadFactoryBuilder().setNameFormat("multi-thread-%d").build(),
                new ThreadPoolExecutor.AbortPolicy()
        );
        // 自动填充 traceId
        return new TraceableExecutorService(applicationContext, threadPoolExecutor);
    }

    /**
     * 单线程管理
     *
     * @return ExecutorService
     */
    @Bean(name = "singleThreadExecutorService", destroyMethod = "shutdownNow")
    public ExecutorService singleThreadExecutorService() {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                1,
                1,
                1L,
                TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(4096), // 线程池队列，超过 4096 个任务将会抛出异常
                new ThreadFactoryBuilder().setNameFormat("single-thread-%d").build(),
                new ThreadPoolExecutor.AbortPolicy()
        );
        // 自动填充 traceId
        return new TraceableExecutorService(applicationContext, threadPoolExecutor);
    }

    /**
     * Spring validator 方法级别的校验
     * https://www.baeldung.com/javax-validation-method-constraints
     * https://juejin.im/entry/5b5a94d2f265da0f7c4fd2b2
     */
    @Primary
    @Bean
    public MethodValidationPostProcessor appMethodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }
    /*
     * 快速失败返回模式，只要有一个异常就返回
     * https://www.cnblogs.com/mr-yang-localhost/p/7812038.html
     *//*
    @Bean
    public Validator validator() {
        return Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .addProperty("hibernate.validator.fail_fast", "true")
                .buildValidatorFactory()
                .getValidator();
    }*/


    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ConditionalOnExpression("'local'.equals('${spring.app.env}')")
    @Bean
    public CommandLineRunner printBeanCommandLineRunner(ApplicationContext context) {
        return args -> {
            log.info("开发环境项目启动成功之后打印所有 bean 到日志文件");
            log.debug("\n{}\n* {}\n{}"
                    , Strings.repeat("*", 100)
                    , Stream.of(context.getBeanDefinitionNames()).sorted().collect(Collectors.joining("\n* "))
                    , Strings.repeat("*", 100)
            );
        };
    }
//    @Bean
//    public GenericConversionService appGenericConversionService(){
//        GenericConversionService genericConversionService = new GenericConversionService();
//        genericConversionService.addConverter(new StringToDateConverter());
//        genericConversionService.addConverter(new StringToDateRangeConverter());
//        genericConversionService.addConverter(new StringToListOrderConverter());
//        genericConversionService.addConverter(new StringToRangeBigDecimalConverter());
//        genericConversionService.addConverter(new StringToRangeDoubleConverter());
//        genericConversionService.addConverter(new StringToRangeIntConverter());
//        genericConversionService.addConverter(new StringToRangeLongConverter());
//        genericConversionService.addConverter(new StringToTimestampConverter());
//        genericConversionService.addConverter(new StringToArrayOrderConverter());
//        return genericConversionService;
//    }

}
