package app.common.starter.aop;

import java.lang.annotation.*;

/**
 * 埋点注解
 * \@AfterReturning Aop 拦截所有 AppEventAfterReturning 记录埋点
 * SPEL： ```http://itmyhome.com/spring/expressions.html```
 *
 * @author 谢长春 2022-04-22
 * 用法：
 * <pre>
 * {@code
 *   @AppEventAfterReturning(comment = "埋点说明文字" // 说明一下这个埋点是干啥的
 *   , condition = "1 == 1 and 'a' == 'a'" // 满足条件才会埋点
 *   , name = "AppEvent.name"              // 枚举参考： app.enums.starter.AppEventKey
 *   , userId = "#user.id"                 // 用户 id , spEL取值表达式
 *   , longId = "#result.id"               // 业务longId , spEL取值表达式
 *   , stringId = "#result.id"             // 业务stringId , spEL取值表达式
 *   , data = "{\"userId\":\"#user.id\",\"dataId\":\"#body.id\",\"dataName\":\"#body.name\"}" // data : spEL取值表达式，构造新对象
 *   )
 * }
 * <pre>
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AppEventAfterReturning {

    /**
     * 参考： app.enums.starter.AppEventKey#name 枚举名称
     *
     * @return String
     */
    String name();

    /**
     * 埋点说明: 只是备注说明，不会入库
     *
     * @return String
     */
    String comment() default "";

    /**
     * 满足条件的才会埋点
     * 参考：org.springframework.boot.autoconfigure.condition.OnExpressionCondition
     *
     * @return String true|false
     */
    String condition() default "true";

    /**
     * 用户 id , spEL取值表达式
     *
     * @return the Spring-EL expression to be evaluated before invoking the protected method
     */
    String userId() default "";

    /**
     * 业务id , spEL取值表达式
     *
     * @return the Spring-EL expression to be evaluated before invoking the protected method
     */
    String longId() default "";

    /**
     * 业务id , spEL取值表达式
     *
     * @return the Spring-EL expression to be evaluated before invoking the protected method
     */
    String stringId() default "";

    /**
     * spEL取值表达式构造数据对象
     * data = "{\"userId\":\"#user.id\",\"dataId\":\"#body.id\",\"dataName\":\"#body.name\"}" // data : spEL取值表达式，构造新对象
     *
     * @return the Spring-EL expression to be evaluated before invoking the protected method
     */
    String data() default "";

}
