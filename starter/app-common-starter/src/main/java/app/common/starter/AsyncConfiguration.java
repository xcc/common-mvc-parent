package app.common.starter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

/**
 * 异步执行封装 @Async org.springframework.core.task.TaskExecutor Sleuth初始化时会默认查找TaskExecutor作为Async的线程池
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableAsync
@AutoConfigureAfter(CommonConfiguration.class)
public class AsyncConfiguration implements AsyncConfigurer {
    private final ExecutorService multiThreadExecutorService;

    /**
     * 自定义 @Async 异步执行注解线程池
     *
     * @return Executor
     */
    @Override
    public Executor getAsyncExecutor() {
        return multiThreadExecutorService;
    }
}
