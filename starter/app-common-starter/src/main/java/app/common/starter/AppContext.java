package app.common.starter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

/**
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Component
@Slf4j
@RequiredArgsConstructor
@Order(-1) // 这里优先级一定要最高，否则其他的初始化使用到路径会报错
public class AppContext extends ApplicationObjectSupport {
    private static ApplicationContext appContext;
    private static ExecutorService singleThreadExecutorService;
    private static ExecutorService multiThreadExecutorService;

    /**
     * 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
     */
    private static final Map<String, Object> BEAN_MAP = new ConcurrentHashMap<>();

    /**
     * 获取 bean， 并缓存到 map 集合，  避免每次通过反射获取
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getBean(Class<T> clazz) {
        if (!BEAN_MAP.containsKey(clazz.getName())) {
            // 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
            BEAN_MAP.put(clazz.getName(), appContext.getBean(clazz));
        }
        return (T) BEAN_MAP.get(clazz.getName());
    }

    /**
     * 获取 bean， 并缓存到 map 集合，  避免每次通过反射获取
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getBean(String beanName, Class<T> clazz) {
        if (!BEAN_MAP.containsKey(beanName)) {
            // 缓存从 spring application context 获取到的 bean， 避免每次通过反射获取
            BEAN_MAP.put(beanName, appContext.getBean(beanName, clazz));
        }
        return (T) BEAN_MAP.get(beanName);
    }

    @PostConstruct
    public void init() {
        appContext = Objects.requireNonNull(getApplicationContext());
        singleThreadExecutorService = Objects.requireNonNull(appContext.getBean("singleThreadExecutorService", ExecutorService.class));
        multiThreadExecutorService = Objects.requireNonNull(appContext.getBean("multiThreadExecutorService", ExecutorService.class));
    }

    /**
     * 获取 Spring Context 对象
     *
     * @return ApplicationContext
     */
    public static ApplicationContext getAppContext() {
        return appContext;
    }

    /**
     * 单线程服务
     */
    public static ExecutorService getSingleThreadExecutorService() {
        return singleThreadExecutorService;
    }

    /**
     * 多线程服务
     */
    public static ExecutorService getMultiThreadExecutorService() {
        return multiThreadExecutorService;
    }

}
