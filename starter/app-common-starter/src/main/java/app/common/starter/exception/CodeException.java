package app.common.starter.exception;

import app.common.starter.enums.Code;
import app.common.starter.interfaces.ICode;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * 自定义异常:指定返回编码异常，禁止指定Code.A00000， Code.A00000 表示成功
 *
 * @author 谢长春 2017年7月21日 下午1:02:04
 */
@Slf4j
public class CodeException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private ICode code;

    public CodeException(final CodeException e) {
        super(e.getMessage(), e);
        this.code = e.getCode();
        if (Objects.equals(Code.A00000.name(), this.code.name())) {
            this.code = Code.A00001;
        }
//        log.error("{}:{}", this.code, this.getMessage());
    }

    public CodeException(final ICode code, final String message) {
        super(message);
        this.code = code;
        if (Objects.equals(Code.A00000.name(), this.code.name())) {
            this.code = Code.A00001;
        }
//        log.error("{}:{}", this.code, this.getMessage());
    }

    public CodeException(final ICode code, final String message, final Throwable cause) {
        super(message, cause);
        this.code = code;
        if (Objects.equals(Code.A00000.name(), this.code.name())) {
            this.code = Code.A00001;
        }
//        log.error("{}:{}", this.code, this.getMessage());
    }

    public CodeException(final ICode code, final String message, final String exception) {
        super(message);
        this.code = code;
        if (Objects.equals(Code.A00000.name(), this.code.name())) {
            this.code = Code.A00001;
        }
        log.error("{}:{}:{}", code.name(), message, exception);
    }

    public CodeException(final ICode code, final String message, final String exception, final Throwable cause) {
        super(message, cause);
        this.code = code;
        if (Objects.equals(Code.A00000.name(), this.code.name())) {
            this.code = Code.A00001;
        }
        log.error("{}:{}:{}", code.name(), message, exception);
    }

    public ICode getCode() {
        return code;
    }
}
