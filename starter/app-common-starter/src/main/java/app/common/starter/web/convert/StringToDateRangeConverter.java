package app.common.starter.web.convert;

import app.common.starter.util.Dates;
import app.common.starter.util.JSON;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToDateRangeConverter implements Converter<String, Dates.Range> {
    @Override
    public Dates.Range convert(@Nullable String value) {
        return JSON.parseObjectOptional(value, Dates.Range.class).orElse(null);
    }
}
