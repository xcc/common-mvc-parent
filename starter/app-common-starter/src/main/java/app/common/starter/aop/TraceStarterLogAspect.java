package app.common.starter.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 拦截所有 基础接口实现类 方法入参打印到日志
 *
 * @author 谢长春 2022-09-25
 */
@Component
@Aspect
@Slf4j
@Order(9999)
public class TraceStarterLogAspect {

    /**
     * 打印参数
     */
    @Before("(execution(* app..*.starter.cache..*.*(..))" +
            "||execution(* app..*.starter.service..*.*(..))" +
            "||execution(* app..*.starter.controller..*.*(..))" +
            "||execution(* app..*.starter.api..*.*(..)))" +
            "&&!@annotation(app.common.starter.aop.IgnoreTraceLog)")
    public void before(final JoinPoint joinPoint) {
        if (log.isInfoEnabled()) {
            log.info("{} => {}", joinPoint, Arrays.toString(joinPoint.getArgs()).replaceAll("\\w+=null, ", ""));
        }
    }
}
