package app.common.starter.dto;

import app.common.starter.util.JSON;
import app.enums.starter.AppEventKey;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * DTO：埋点表新增 V20221105
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义埋点表新增时的扩展属性
 *
 * @author 谢长春 on 2022-06-27
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString(callSuper = true)
@ApiModel(description = "埋点")
public class AppEventDTO implements Cloneable {
    private static final long serialVersionUID = 1L;
    /**
     * 应用名
     */
    private String appName;
    /**
     * 埋点事件，参考：AppEventKey
     */
    private AppEventKey event;
    /**
     * 埋点时间，yyyyMMddHHmmss
     */
    private String eventTime;
    /**
     * 埋点附加数据
     */
    private String data;
    /**
     * 用户ID。 tab_user.id
     */
    private Long userId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    private Long refLongId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    private String refStringId;

    public AppEventDTO setData(String data) {
        this.data = data;
        return this;
    }

    public AppEventDTO setData(Map<String, Object> map) {
        this.data = JSON.toJsonString(map);
        return this;
    }

    @SneakyThrows
    public AppEventDTO cloneObject() {
        return (AppEventDTO) super.clone();
    }
}
