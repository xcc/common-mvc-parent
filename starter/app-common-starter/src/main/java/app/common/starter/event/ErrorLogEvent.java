package app.common.starter.event;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;


/**
 * 异常日志事件
 *
 * @author 谢长春 2022-11-15
 */
@Getter
@ToString
public class ErrorLogEvent extends ApplicationEvent {

    public ErrorLogEvent(Object source, String appName, String traceId, long userId, String message) {
        super(source);
        this.appName = appName;
        this.traceId = traceId;
        this.userId = userId;
        this.message = message;
    }


    /**
     * 应用名称
     */
    private final String appName;
    /**
     * 链路追踪id
     */
    private final String traceId;
    /**
     * 操作用户id
     */
    private final long userId;
    /**
     * 日志内容
     */
    private final String message;

}
