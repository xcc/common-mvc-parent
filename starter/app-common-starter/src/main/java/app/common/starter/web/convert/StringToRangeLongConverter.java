package app.common.starter.web.convert;

import app.common.starter.entity.RangeLong;
import app.common.starter.util.JSON;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToRangeLongConverter implements Converter<String, RangeLong> {
    @Override
    public RangeLong convert(@Nullable String value) {
        return JSON.parseObjectOptional(value, RangeLong.class).orElse(null);
    }
}
