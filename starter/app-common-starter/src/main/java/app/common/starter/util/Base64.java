package app.common.starter.util;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Base64 编码解码工具类
 *
 * @author 谢长春  2016-11-23 .
 */
public final class Base64 {

    /**
     * 将指定编码的字符串转换为 Base64 编码字符串
     *
     * @param content String
     * @return String 返回Base64编码字符串
     */
    public static String encode(final String content) {
        return encode(content, null);
    }

    /**
     * 将指定编码的字符串转换为 Base64 编码字符串
     *
     * @param content String
     * @param charset Charset
     * @return String 返回Base64编码字符串
     */
    public static String encode(final String content, Charset charset) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        if (Objects.isNull(charset)) {
            return org.apache.commons.codec.binary.Base64.encodeBase64String(content.getBytes());
        }
        return org.apache.commons.codec.binary.Base64.encodeBase64String(content.getBytes(charset));
    }

    /**
     * 将 Base64 编码字符串转换为指定编码的字符串
     *
     * @param content String
     * @return String 返回指定编码字符串
     */
    public static String decode(final String content) {
        return decode(content, null);
    }

    /**
     * 将 Base64 编码字符串转换为指定编码的字符串
     *
     * @param content String
     * @param charset Charset
     * @return String 返回指定编码字符串
     */
    public static String decode(final String content, Charset charset) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        if (Objects.isNull(charset)) {
            return new String(org.apache.commons.codec.binary.Base64.decodeBase64(content));
        }
        return new String(org.apache.commons.codec.binary.Base64.decodeBase64(content), charset);
    }

    /**
     * 将指定编码的字符串转换为 Base64 编码字符串
     *
     * @param content String
     * @return String 返回Base64编码字符串
     */
    public static String encoder(final byte[] content) {
        if (Objects.isNull(content)) {
            return null;
        }
        return org.apache.commons.codec.binary.Base64.encodeBase64String(content);
    }

    /**
     * 将 Base64 编码字符串转换为 byte[]
     *
     * @param content String
     * @return String 返回指定编码字符串
     */
    public static byte[] decoder(String content) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        return org.apache.commons.codec.binary.Base64.decodeBase64(content);
    }

    /**
     * 将指定编码的字符串转换为 Base64 编码字符串；支持编码结果作为 url 参数；
     *
     * @param content String
     * @return String 返回Base64编码字符串
     */
    public static String encodeUrl(final String content) {
        return encodeUrl(content, null);
    }

    /**
     * 将指定编码的字符串转换为 Base64 编码字符串；支持编码结果作为 url 参数；
     *
     * @param content String
     * @param charset Charset
     * @return String 返回Base64编码字符串
     */
    public static String encodeUrl(final String content, Charset charset) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        if (Objects.isNull(charset)) {
            return org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(content.getBytes());
        }
        return org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(content.getBytes(charset));
    }

    /**
     * 将 Base64 编码字符串转换为指定编码的字符串；支持编码结果作为 url 参数；
     *
     * @param content String
     * @return String 返回指定编码字符串
     */
    public static String decodeUrl(final String content) {
        return decodeUrl(content, null);
    }

    /**
     * 将 Base64 编码字符串转换为指定编码的字符串；支持编码结果作为 url 参数；
     *
     * @param content String
     * @param charset Charset
     * @return String 返回指定编码字符串
     */
    public static String decodeUrl(final String content, Charset charset) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        if (Objects.isNull(charset)) {
            return new String(org.apache.commons.codec.binary.Base64.decodeBase64(content));
        }
        return new String(
                org.apache.commons.codec.binary.Base64.decodeBase64(content)
                , charset
        );
    }

    /**
     * 将指定编码的字符串转换为 Base64 编码字符串；支持编码结果作为 url 参数；
     *
     * @param content String
     * @return String 返回Base64编码字符串
     */
    public static String encoderUrl(final byte[] content) {
        if (Objects.isNull(content)) {
            return null;
        }
        return org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(content);
    }

    /**
     * 将 Base64 编码字符串转换为 byte[]；支持编码结果作为 url 参数；
     *
     * @param content String
     * @return String 返回指定编码字符串
     */
    public static byte[] decoderUrl(String content) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        return org.apache.commons.codec.binary.Base64.decodeBase64(content);
    }

    public static void main(String[] args) {
        System.out.println(Base64.encode(JSON.toJsonString(new HashMap<String, String>(1) {{
            put("", "");
        }})));
        System.out.println(Base64.encode("{\"name\":\"JX\"} "));
        System.out.println(Base64.encode("测试"));
        System.out.println(Base64.encode("5rWL6K+V"));
        System.out.println(Base64.encode(",./<>?<[]-=)_())("));
        System.out.println(Base64.encode("LC4vPD4-PFtdLT0pXygpKSg_"));
        System.out.println(Base64.encode("{1:\"1\"}"));
        System.out.println(Base64.encode("ezE6IjEifQ__"));
        System.out.println(Base64.encode("{role:管理员,description:测试描述}"));
        System.out.println(Base64.decode("eyJpbml0IjpmYWxzZSwicGFzc3dvcmQiOiIxMTExMTEiLCJ1c2VybmFtZSI6IjEwMDAwMDAyMTAx\nIn0="));

        Dates.Range.month().forEach((a, b) -> System.out.println(Base64.encode(Dates.Pattern.yyyyMMdd.format(a))));

        System.out.println(Base64.encodeUrl(JSON.toJsonString(new HashMap<String, String>(1) {{
            put("", "");
        }})));
        System.out.println(Base64.encodeUrl("{\"name\":\"JX\"} "));
        System.out.println(Base64.encodeUrl("测试"));
        System.out.println(Base64.encodeUrl("5rWL6K+V"));
        System.out.println(Base64.encodeUrl(",./<>?<[]-=)_())("));
        System.out.println(Base64.encodeUrl("LC4vPD4-PFtdLT0pXygpKSg_"));
        System.out.println(Base64.encodeUrl("{1:\"1\"}"));
        System.out.println(Base64.encodeUrl("ezE6IjEifQ__"));
        System.out.println(Base64.encodeUrl("{role:管理员,description:测试描述}"));
        System.out.println(Base64.decodeUrl("eyJpbml0IjpmYWxzZSwicGFzc3dvcmQiOiIxMTExMTEiLCJ1c2VybmFtZSI6IjEwMDAwMDAyMTAx\nIn0="));

        Dates.Range.month().forEach((a, b) -> System.out.println(Base64.encodeUrl(Dates.Pattern.yyyyMMdd.format(a))));
        Stream.of("eyIiOiIifQ",
                "eyJuYW1lIjoiSlgifSA",
                "5rWL6K-V",
                "NXJXTDZLK1Y",
                "LC4vPD4_PFtdLT0pXygpKSg",
                "TEM0dlBENC1QRnRkTFQwcFh5Z3BLU2df",
                "ezE6IjEifQ",
                "ZXpFNklqRWlmUV9f",
                "e3JvbGU6566h55CG5ZGYLGRlc2NyaXB0aW9uOua1i-ivleaPj-i_sH0"
        ).forEach(base64 -> System.out.println(Base64.decodeUrl(base64)));
        System.out.println("++++++++");
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("1871794260018717942600".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("{\"name\":\"JX\"} ".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("测试".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("5rWL6K+V".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(",./<>?<[]-=)_())(".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("LC4vPD4-PFtdLT0pXygpKSg_".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("{1:\"1\"}".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("ezE6IjEifQ__".getBytes()));
        System.out.println(org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString("{role:管理员,description:测试描述}".getBytes()));
        System.out.println(new String(org.apache.commons.codec.binary.Base64.decodeBase64("e3JvbGU6566h55CG5ZGYLGRlc2NyaXB0aW9uOua1i-ivleaPj-i_sH0")));
        System.out.println(new String(org.apache.commons.codec.binary.Base64.decodeBase64("5rWL6K-V")));
        System.out.println(new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger("20221231235959"))));
        System.out.println(new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger("20991231235959"))));
        System.out.println(new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger("20221231235959999"))));
        System.out.println(new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger("20991231235959999"))));
        for (int i = 1; i <= 20; i++) {
            System.out.println(String.format("%02d: ", i) + new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger(Strings.repeat("9", i)))));
        }
        Dates now = Dates.now();
        for (int i = 0; i < 512; i++) {
            now.addSecond(i);
            System.out.println(now.format(Dates.Pattern.yyyyMMddHHmmss) + " " + new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger(now.format(Dates.Pattern.yyyyMMddHHmmssSSS)))));
        }
        for (int i = 0; i < 512; i++) {
            System.out.println(String.format("%03d: ", i) + new String(org.apache.commons.codec.binary.Base64.encodeInteger(new BigInteger(i + ""))));
        }
    }
}
