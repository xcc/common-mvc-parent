package app.common.starter.web.convert;

import app.common.starter.entity.Range;
import app.common.starter.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.math.BigDecimal;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToRangeBigDecimalConverter implements Converter<String, Range<BigDecimal>> {
    @Override
    public Range<BigDecimal> convert(@Nullable String value) {
        return JSON.parseOptional(value, new TypeReference<Range<BigDecimal>>() {
        }).orElse(null);
    }
}
