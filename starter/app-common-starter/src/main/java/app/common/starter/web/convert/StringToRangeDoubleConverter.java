package app.common.starter.web.convert;

import app.common.starter.entity.Range;
import app.common.starter.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToRangeDoubleConverter implements Converter<String, Range<Double>> {
    @Override
    public Range<Double> convert(@Nullable String value) {
        return JSON.parseOptional(value, new TypeReference<Range<Double>>() {
        }).orElse(null);
    }
}
