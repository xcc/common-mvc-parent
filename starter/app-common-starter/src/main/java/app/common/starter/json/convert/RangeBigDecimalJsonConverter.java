package app.common.starter.json.convert;

import app.common.starter.entity.Range;
import app.common.starter.util.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Slf4j
public class RangeBigDecimalJsonConverter extends StdConverter<String, Range<BigDecimal>> {

    @Override
    public Range<BigDecimal> convert(String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        return JSON.parseOptional(value, new TypeReference<Range<BigDecimal>>() {
        }).orElse(null);
    }
}
