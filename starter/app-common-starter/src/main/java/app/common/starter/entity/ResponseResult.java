package app.common.starter.entity;

import app.common.starter.enums.Code;
import app.common.starter.exception.CodeException;
import app.common.starter.interfaces.ICode;
import app.common.starter.interfaces.IJson;
import app.common.starter.util.ChainMap;
import app.common.starter.util.JSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.*;

/**
 * 返回结果集对象
 *
 * @author 谢长春 2017-9-20
 */
@ToString
@Slf4j
@Accessors(chain = true)

public class ResponseResult<E> implements IJson, Cloneable {

    /**
     * 默认构造函数
     */
    public ResponseResult() {
        this(Code.A00001);
    }

    /**
     * 带参构造函数
     *
     * @param code {@link ICode} 操作响应码
     */
    public ResponseResult(final ICode code) {
        super();
        setCode(code);
    }

    /**
     * 将json格式转换为ResponseResult对象
     *
     * @param jsonText {@link String} Json文本
     * @return ResponseResult<Object>
     */
    public static <T> ResponseResult<T> valueOfJson(final String jsonText) {
        Objects.requireNonNull(jsonText, "参数【jsonText】是必须的");
        return JSON.parse(jsonText, new TypeReference<ResponseResult<T>>() {
        });
    }

//    /**
//     * 是否显示异常信息
//     * true: 是，默认值
//     * false: 否，生产环境设置为 false
//     */
//    @JsonIgnore
//    private static boolean showException = true;
//
//    /**
//     * 是否显示异常信息
//     * true: 是，默认值
//     * false: 否，生产环境设置为 false
//     */
//    @JsonIgnore
//    protected static boolean isShowException() {
//        return showException;
//    }
//
//    /**
//     * 是否显示异常信息
//     * true: 是，默认值
//     * false: 否，生产环境设置为 false
//     */
//    @JsonIgnore
//    public static void setShowException(boolean showException) {
//        ResponseResult.showException = showException;
//    }

    /**
     * 编码，成功、失败、异常编码
     */
    private ICode icode;
    /**
     * 本次响应数据总行数
     */
    @Getter
    @Setter
    private long rowCount;
    /**
     * 总页数
     */
    @Getter
    @Setter
    private int pageCount;
    /**
     * 总行数
     */
    @Getter
    @Setter
    private long totalCount;
    /**
     * 异常消息
     */
    @Getter
    private String exception;
    /**
     * 返回数据集合
     */
    @Getter
    private List<E> data = Collections.emptyList();
    /**
     * 附加信息
     */
    @Getter
    @Setter
    private Map<String, Object> extras = null;

    public String getCode() {
        return icode.name();
    }

    /**
     * 将编码转换成具体消息
     *
     * @return String
     */
    public String getMessage() {
        if (Objects.equals(Code.A00002.name(), icode.name())) {
            // 处理自定义动态异常消息
            return Objects.isNull(this.exception)
                    ? null
                    : this.exception.replace(Code.A00002.name().concat(":"), "");
        } else if (Objects.equals(Code.A00003.name(), icode.name())) {
            return String.format("请联系程序员小哥哥提供错误代码 \uD83D\uDC49 %s \uD83D\uDC48", MDC.get("traceId"));
//            return Objects.isNull(this.exception)
//                    ? null
//                    : this.exception.replace(Code.A00002.name().concat(":"), "");
        }
        return this.icode.getComment();
    }

    public ResponseResult<E> setCode(final ICode code) {
        this.icode = (Objects.isNull(code) ? Code.A00001 : code);
        return this; // 保证链式请求，返回:this
    }

    /**
     * Json 字符串反序列化时需要此方法，其他情况禁止直接调用 setData 方法，请使用 {@link ResponseResult#setSuccess(List)} 方法
     *
     * @param data List<E>
     * @deprecated 禁止直接调用 setData 方法，请使用 {@link ResponseResult#setSuccess(List)} 方法
     */
    @Deprecated
    public void setData(final List<E> data) {
        this.data = data;
    }

    public ResponseResult<E> setException(final String exception) {
        this.exception = exception;
//        log.error("{}:{}:{}", this.getCode(), this.getMessage(), this.getException());
        return this; // 保证链式请求，返回:this
    }

    /**
     * 设置异常编码及异常信息
     *
     * @param code      {@link ICode} 异常响应码
     * @param exception String 异常消息内容
     */
    public ResponseResult<E> setException(final ICode code, final String exception) {
        setCode(Objects.equals(code.name(), Code.A00000.name()) ? Code.A00001 : code);
        this.exception = exception;
//        log.error("{}:{}:{}", this.getCode(), this.getMessage(), this.getException());
        return this; // 保证链式请求，返回:this
    }

    /**
     * 将业务逻辑中捕获到的异常转换为对应的code
     *
     * @param e {@link Exception} 捕获到的异常
     * @return ResponseResult<E>
     */
    public ResponseResult<E> setException(final Exception e) {
        if (e instanceof CodeException) {
            setException(((CodeException) e).getCode(), e.getMessage());
        } else {
            setException(Code.A00001, e.getMessage());
        }
//        log.error("{}:{}:{}", this.getCode(), this.getMessage(), this.getException());
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param data List<E>
     * @return ResponseResult<E>
     */
    public ResponseResult<E> setSuccess(final List<E> data) {
        this.icode = Code.A00000;
        this.data = Objects.nonNull(data) ? data : Collections.emptyList(); // 设置有效的结果集
        this.rowCount = this.data.size(); // 设置结果集大小
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param result ResponseResult<E>
     * @return ResponseResult<E>
     */
    public ResponseResult<E> setSuccess(final ResponseResult<E> result) {
        result.assertSuccess();
        this.icode = Code.A00000;
        this.data = Objects.nonNull(result.getData()) ? result.getData() : Collections.emptyList(); // 设置有效的结果集
        this.rowCount = this.data.size(); // 设置结果集大小
        this.totalCount = result.getTotalCount();
        this.pageCount = result.getPageCount();
        this.exception = result.getException();
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param data E[]
     * @return ResponseResult<E>
     */
    public ResponseResult<E> setSuccess(final E[] data) {
        this.icode = Code.A00000;
        this.data = (data.length > 0 && Objects.nonNull(data[0])) ? Arrays.asList(data) : Collections.emptyList(); // 设置有效的结果集
        this.rowCount = this.data.size(); // 设置结果集大小
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param data E
     * @return ResponseResult<E>
     */
    public ResponseResult<E> setSuccess(final E data) {
        this.icode = Code.A00000;
        if (Objects.nonNull(data)) {
            this.data = Collections.singletonList(data);
        } else {
            this.data = Collections.emptyList();
        }
        this.rowCount = this.data.size(); // 设置结果集大小
        return this; // 保证链式请求，返回:this
    }

    /**
     * 添加扩展属性，返回ResponseResult对象本身，支持链式请求
     *
     * @param key   String
     * @param value Object
     * @return ResponseResult<E>
     */
    public ResponseResult<E> addExtras(final String key, final Object value) {
        Objects.requireNonNull(key, "参数【key】是必须的");
        if (Objects.isNull(this.extras)) {
            this.extras = new HashMap<>(10);
        }
        this.extras.put(key, value);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 添加扩展属性，返回ResponseResult对象本身，支持链式请求
     *
     * @return ResponseResult<E>
     */
    public ResponseResult<E> addExtras(final ChainMap<String, Object> obj) {
        Objects.requireNonNull(obj, "参数【obj】是必须的");
        if (Objects.isNull(this.extras)) {
            this.extras = new HashMap<>(10);
        }
        this.extras.putAll(obj.getMapObject());
        return this; // 保证链式请求，返回:this
    }

    /**
     * 添加扩展属性，返回ResponseResult对象本身，支持链式请求
     *
     * @return ResponseResult<E>
     */
    public ResponseResult<E> addExtras(final Map<String, String> extras) {
        if (Objects.isNull(this.extras)) {
            this.extras = new HashMap<>(10);
        }
        this.extras.putAll(extras);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 判断 code 是否为 SUCCESS
     *
     * @return ResponseResult<E> code == SUCCESS 返回结果集对象
     * @throws CodeException code != SUCCESS 则抛出异常
     */
    @JsonIgnore
    public ResponseResult<E> isSuccess() throws CodeException {
        if (Objects.equals(Code.A00000.name(), this.icode.name())) {
            return this; // return this; 保证链式请求
        }
        throw new CodeException(this.icode, this.getMessage());
    }

    /**
     * 判断 code 是否为 SUCCESS
     *
     * @return ResponseResult<E> code == SUCCESS 返回结果集对象
     * @throws CodeException code != SUCCESS 则抛出异常
     */
    public ResponseResult<E> assertSuccess() throws CodeException {
        if (Objects.equals(Code.A00000.name(), this.icode.name())) {
            return this; // return this; 保证链式请求
        }
        throw new CodeException(this.icode, this.getMessage(), this.getException());
    }

    /**
     * 判断 rowCount 是否等于0
     *
     * @return true等于0，false不等于0
     */
    @JsonIgnore
    public boolean isEmpty() {
        return this.rowCount == 0;
    }

    /**
     * 判断 rowCount 是否大于0
     *
     * @return true大于0
     */
    @JsonIgnore
    public boolean isNonEmpty() {
        return !isEmpty();
    }

    /**
     * 获取data集合中的第一项;获取前先校验集合长度是否大于0
     *
     * @return E
     */
    public Optional<E> dataFirst() {
        return (this.rowCount == 0)
                ? Optional.empty()
                : Optional.of(this.data.get(0));
    }

    /**
     * 用于打印日志时，清除 data 集合
     */
    public void clearData() {
        this.data = Collections.emptyList();
    }

    @SuppressWarnings({"unchecked"})
    @SneakyThrows
    public ResponseResult<E> cloneObject() {
        return (ResponseResult<E>) super.clone();
    }

    //    public static void main(String[] args) {
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 打印所有状态码 <<<<<<<<<<<<<<<<<<");
//            for (Code code : Code.values()) {
//                log.info(code + ":" + code.comment);
//            }
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 打印对象 toJson 之后的全部字段 <<<<<<<<<<<<<<<<<<");
//            log.info(JSON.toJsonString(new ResponseResult<>()));
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 声明data集合中只能是String <<<<<<<<<<<<<<<<<<");
//            ResponseResult<String> result = new ResponseResult<>();
//            // 设置单一对象，必须是泛型声明的类型
//            result.setSuccess("111");
//            log.info(result.json());
//            // 设置多个对象，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList("222", "333"));
//            log.info(result.json());
//            // 设置对象对象数组，必须是泛型声明的类型
//            result.setSuccess(new String[]{"444", "555"});
//            log.info(result.json());
//            // 设置对象集合，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList("666", "777"));
//            // 带有附加属性(扩展属性),可以链式调用
//            result.addExtras("name", "JX").addExtras("amount", 100).addExtras("roles", new String[]{"ROLE_USER"});
//            log.info(result.json());
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 声明data集合中只能是Map<String, Object> <<<<<<<<<<<<<<<<<<");
//            ResponseResult<Map<String, Object>> result = new ResponseResult<>();
//            // 设置单一对象，必须是泛型声明的类型
//            result.setSuccess(Maps.bySO("key", "111"));
//            log.info(result.json());
//            // 设置多个对象，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Maps.bySO("key", "222"), Maps.bySO("key", "333")));
//            log.info(result.json());
//            // 设置对象集合，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Maps.bySO("key", "444"), Maps.bySO("key", "555")));
//            // 带有附加属性(扩展属性),可以链式调用
//            result.addExtras("name", "JX").addExtras("amount", 100).addExtras("roles", new String[]{"ROLE_USER"});
//            log.info(result.json());
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 声明data集合中只能是 Item <<<<<<<<<<<<<<<<<<");
//            ResponseResult<Item> result = new ResponseResult<>();
//            // 设置单一对象，必须是泛型声明的类型
//            result.setSuccess(Item.builder().key("key").value(111).build());
//            log.info(result.json());
//            // 设置多个对象，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Item.builder().key("key").value(222).build(), Item.builder().key("key").value(333).build()));
//            log.info(result.json());
//            // 设置对象对象数组，必须是泛型声明的类型
//            result.setSuccess(new Item[]{Item.builder().key("key").value(444).build(), Item.builder().key("key").value(555).build()});
//            log.info(result.json());
//            // 设置对象集合，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Item.builder().key("key").value(666).build(), Item.builder().key("key").value(777).build()));
//            // 带有附加属性(扩展属性),可以链式调用
//            result.addExtras("name", "JX").addExtras("amount", 100).addExtras("roles", new String[]{"ROLE_USER"});
//            log.info(result.json());
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 将 Json 字符串反序列化为ResponseResult对象 <<<<<<<<<<<<<<<<<<");
//            log.info(ResponseResult.valueOfJson("{\"code\":\"SUCCESS\",\"message\":\"成功\",\"rowCount\":90,\"pageCount\":100,\"totalCount\":200,\"data\":[\"A\",\"B\"]}").json());
//            log.info(ResponseResult.valueOfJson("{\"code\":\"SUCCESS\",\"message\":\"成功\",\"rowCount\":90,\"pageCount\":100,\"totalCount\":200,\"data\":[\"A\",\"B\"]}").json());
//            log.info(ResponseResult.valueOfJson("{\"code\":\"FAILURE\",\"message\":\"失败\",\"data\":[{\"name\":\"A\"},{\"name\":\"B\"}]}").json());
//        }
////        {
////            try {
////                ResponseResult<Table> result = JSON.parseObject(FileUtil.read("D:\\project\\files\\upload-com-data\\c9d6ad96-3eed-4d70-879b-bead504f0730\\2018\\BudgetMainIncome\\66f871de-5265-462d-8f55-1e34baa0e286.json"), new TypeReference<ResponseResult<Table>>(){});
////                log.info("{}",result.dataFirst());
////                log.info(result.json());
////            } catch (IOException e) {
////                log.error(e.getMessage(), e);
////            }
////        }
//
//    }
    public static void main(String[] args) {
        System.out.println(new ResponseResult<>().json());
        System.out.println(new ResponseResult<>(Code.A00003).json());
//        System.out.println(new ResponseResult<>(AppCode.APP_CODE_ERROR).json());
    }
}
