package app.common.starter.event;

import app.common.starter.dto.AppEventDTO;
import app.enums.starter.AppEventKey;
import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

import java.util.Optional;


/**
 * 埋点事件
 *
 * @author 谢长春 2022-11-15
 */
@Getter
@ToString
public class AppEvent extends ApplicationEvent {

    public AppEvent(Object source, AppEventDTO dto) {
        super(source);
        this.appName = dto.getAppName();
        this.eventKey = dto.getEvent();
        this.userId = Optional.ofNullable(dto.getUserId()).orElse(0L);
        this.refLongId = dto.getRefLongId();
        this.refStringId = dto.getRefStringId();
        this.data = dto.getData();
    }

    /**
     * 应用名称
     */
    private final String appName;
    /**
     * 应用操作埋点事件key
     */
    private final AppEventKey eventKey;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    private final String refStringId;
    /**
     * 埋点关联数据id，用于简单业务埋点
     */
    private final Long refLongId;
    /**
     * 操作用户id
     */
    private final long userId;
    /**
     * 埋点附加数据
     */
    private final String data;

}
