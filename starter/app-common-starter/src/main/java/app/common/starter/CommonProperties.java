package app.common.starter;
//
//import com.app.common.starter.enums.AppEnv;
//import com.google.common.base.Strings;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.core.Ordered;
//import org.springframework.core.annotation.Order;
//
//import javax.annotation.PostConstruct;
//import java.util.Objects;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import static com.app.common.starter.util.Dates.Pattern.yyyyMMddHHmmss;
//
///**
// * 应用运行环境配置属性
// *
// * @author 谢长春 2022-02-08
// */
//@Slf4j
//@Getter
//@Setter
//@ConfigurationProperties("spring.app")
//public class CommonProperties {
//    private static volatile CommonProperties INSTANCE = null;
//
//    /**
//     * 获取 {@link CommonProperties} 实例，用于静态类或实体类获取配置参数
//     *
//     * @return {@link CommonProperties}
//     */
//    public static CommonProperties instance() {
//        if (Objects.isNull(INSTANCE)) {
//            synchronized (CommonProperties.class) {
//                if (Objects.isNull(INSTANCE)) {
//                    INSTANCE = AppContext.getAppContext().getBean(CommonProperties.class);
//                }
//            }
//        }
//        return INSTANCE;
//    }
//
//    public CommonProperties() {
////    public CommonProperties(org.springframework.cloud.commons.util.InetUtils  inetUtils) {
////        this.ip = inetUtils.findFirstNonLoopbackHostInfo().getIpAddress();
//        this.startTime = yyyyMMddHHmmss.now();
////        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
////        Set<String> ipAddressSet = Sets.newLinkedHashSet();
////        while (networkInterfaces.hasMoreElements()) {
////            Enumeration<InetAddress> inetAddress = networkInterfaces.nextElement().getInetAddresses();
////            while (inetAddress.hasMoreElements()) {
////                InetAddress address = inetAddress.nextElement();
////                if (address instanceof Inet4Address) {
////                    if (!"localhost".equals(address.getHostName())) {
////                        ipAddressSet.add(address.getHostAddress());
////                    }
////                }
////            }
////        }
////        this.ip = String.join(",", ipAddressSet);
//    }
//
//    private static final int MAX = 80;
//    private static final String LOG_PATTERN = String.format(
//            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
//            , Strings.repeat(" ┳", 30)
//            , Strings.repeat(" ┳", 30)
//            , Strings.repeat(" ┻", 30)
//            , Strings.repeat(" ┻", 30)
//    );
//
//    @PostConstruct
//    private void postConstruct() {
//        log.info(LOG_PATTERN
//                , "环境配置"
//                , String.join("\n"
//                        , StringUtils.left(StringUtils.rightPad("spring.app.env: " + env.name(), MAX, ' '), MAX) + " # 当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]"
//                        , StringUtils.left(StringUtils.rightPad("spring.app.name: " + name, MAX, ' '), MAX) + " # 应用名称"
//                        , StringUtils.left(StringUtils.rightPad("spring.app.version: " + version, MAX, ' '), MAX) + " # 当前运行版本号"
//                        , StringUtils.left(StringUtils.rightPad("spring.app.buildTime: " + buildTime, MAX, ' '), MAX) + " # 项目编译打包时间"
//                        , StringUtils.left(StringUtils.rightPad("spring.app.gitCommitId: " + gitCommitId, MAX, ' '), MAX) + " # git 提交记录 id 前 8 位"
//                        , StringUtils.left(StringUtils.rightPad("server.port: " + port, MAX, ' '), MAX) + " # 当前服务端口"
//                )
//                , "环境配置"
//        );
//    }
//    /**
//     * 当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]
//     */
//    private AppEnv env;
//    /**
//     * 应用域名
//     */
//    private String domain;
//    /**
//     * 当前服务端口
//     */
//    @Value("${server.port:8080}")
//    private int port;
//    /**
//     * 应用名
//     */
//    private String name;
//    /**
//     * 当前运行版本号
//     */
//    private String version;
//    /**
//     * 项目编译打包时间，发布完成之后检查打包时间是否是一致的，避免版本更新失败
//     */
//    private String buildTime;
//    /**
//     * git 提交记录 id 前 8 位
//     */
//    private String gitCommitId;
//    /**
//     * 项目启动时间
//     */
//    private String startTime;
//}
