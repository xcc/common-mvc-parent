package app.common.starter.util;

import app.common.starter.entity.OrderBy;
import app.common.starter.enums.Code;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.util.*;

import static app.common.starter.util.Dates.Pattern.yyyy_MM_dd_HH_mm_ss_SSS;

/**
 * 封装 jackson 操作静态方法
 *
 * @author 谢长春 2022-06-21
 */
@Slf4j
public final class JSON {
    private JSON() {
    }

    /**
     * 包含加解密逻辑的 json 序列化反序列化操作
     */
    private static ObjectMapper objectMapper;
    /**
     * 不包含加解密逻辑的 json 序列化反序列化操作
     */
    private static ObjectMapper objectMapperIntrospector;

    static {
        objectMapper = new ObjectMapper(
                JsonFactory.builder()
                        .enable(JsonReadFeature.ALLOW_JAVA_COMMENTS)  // 允许 '//' 注释
                        .enable(JsonReadFeature.ALLOW_YAML_COMMENTS) // 允许 '#' 注释
                        .build()
        )
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) // 反序列化忽略 java 类中不存在的字段
                .enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES) // 反序列化 null 值忽略
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS) // bean序列化为空时不会异常失败
                .setSerializationInclusion(JsonInclude.Include.NON_NULL) // 序列化不返回 null 值
        ;
        objectMapperIntrospector = new ObjectMapper(
                JsonFactory.builder()
                        .enable(JsonReadFeature.ALLOW_JAVA_COMMENTS)  // 允许 '//' 注释
                        .enable(JsonReadFeature.ALLOW_YAML_COMMENTS) // 允许 '#' 注释
                        .build()
        )
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) // 反序列化忽略 java 类中不存在的字段
                .enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES) // 反序列化 null 值忽略
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS) // bean序列化为空时不会异常失败
                .setSerializationInclusion(JsonInclude.Include.NON_NULL) // 序列化不返回 null 值
        ;
    }

    /**
     * 添加自定义拦截器 fixme: 添加忽略加解密注解
     * <pre>
     * {@code
     *   JSON.addAnnotationIntrospector(
     *     new JacksonAnnotationIntrospector() {
     *       @Override
     *       public boolean isAnnotationBundle(Annotation ann) {
     *           final Class<? extends Annotation> type = ann.annotationType();
     *           if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
     *               // https://www.daimajiaoliu.com/daima/4ed17441d100404
     *               return false;
     *           }
     *           return super.isAnnotationBundle(ann);
     *       }
     *     }
     *   );
     * }
     * </pre>
     *
     * @param introspector {@link JacksonAnnotationIntrospector}
     */
    public static void addAnnotationIntrospector(final JacksonAnnotationIntrospector introspector) {
        objectMapperIntrospector = objectMapperIntrospector.setAnnotationIntrospector(
                introspector
//                new JacksonAnnotationIntrospector() {
//                    @Override
//                    public boolean isAnnotationBundle(Annotation ann) {
//                        final Class<? extends Annotation> type = ann.annotationType();
//                        if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
//                            // https://www.daimajiaoliu.com/daima/4ed17441d100404
//                            return false;
//                        }
//                        return super.isAnnotationBundle(ann);
//                    }
//                }
        );
    }

    public static ObjectMapper get() {
        return objectMapperIntrospector;
    }

    /**
     * json 序列化时，禁用 @AesIdJsonConverter 加解密相关的逻辑
     *
     * @param value Object
     * @return String
     */
    @SneakyThrows
    public static String toJsonString(final Object value) {
        return objectMapperIntrospector.writeValueAsString(value);
    }

    /**
     * json 序列化时，启用 @AesIdJsonConverter 加解密相关的逻辑
     *
     * @param value Object
     * @return String
     */
    @SneakyThrows
    public static String toJsonStringAesConvert(final Object value) {
        return objectMapper.writeValueAsString(value);
    }

    @SneakyThrows
    public static <T> T parse(final String jsonText, TypeReference<T> typeReference) {
        Code.A00001.assertHasTrue(StringUtils.isNoneBlank(jsonText), "JSON反序列化失败");
        return objectMapperIntrospector.readValue(jsonText, typeReference);
    }

    @SneakyThrows
    public static <T> T parseAesConvert(final String jsonText, TypeReference<T> typeReference) {
        Code.A00001.assertHasTrue(StringUtils.isNoneBlank(jsonText), "JSON反序列化失败");
        return objectMapper.readValue(jsonText, typeReference);
    }

    @SneakyThrows
    public static <T> Optional<T> parseOptional(final String jsonText, TypeReference<T> typeReference) {
        if (StringUtils.isBlank(jsonText)) {
            return Optional.empty();
        }
        return Optional.ofNullable(objectMapperIntrospector.readValue(jsonText, typeReference));
    }

    @SneakyThrows
    public static <T> Optional<T> parseOptionalAesConvert(final String jsonText, TypeReference<T> typeReference) {
        if (StringUtils.isBlank(jsonText)) {
            return Optional.empty();
        }
        return Optional.ofNullable(objectMapper.readValue(jsonText, typeReference));
    }

    @SneakyThrows
    public static <T> T parseObject(final String jsonText, Class<T> clazz) {
        Code.A00001.assertHasTrue(StringUtils.isNoneBlank(jsonText), "JSON反序列化失败");
        return objectMapperIntrospector.readValue(jsonText, clazz);
    }

    @SneakyThrows
    public static <T> Optional<T> parseObjectOptional(final String jsonText, Class<T> clazz) {
        if (StringUtils.isBlank(jsonText)) {
            return Optional.empty();
        }
        return Optional.ofNullable(objectMapperIntrospector.readValue(jsonText, clazz));
    }

    @SneakyThrows
    public static <V> Map<String, V> parseMap(final String jsonText) {
        if (StringUtils.isBlank(jsonText)) {
            return Collections.emptyMap();
        }
        return objectMapperIntrospector.readValue(jsonText, new TypeReference<LinkedHashMap<String, V>>() {
        });
    }

    @SneakyThrows
    public static <T> List<T> parseList(final String jsonText, Class<T> clazz) {
        if (StringUtils.isBlank(jsonText)) {
            return Collections.emptyList();
        }
        final CollectionType javaType = objectMapperIntrospector.getTypeFactory()
                .constructCollectionType(List.class, clazz);
        return objectMapperIntrospector.readValue(jsonText, javaType);
    }

    /**
     * 用于 Json 序列化和反序列化
     *
     * <pre>
     * 使用方法：
     * {@code
     *   @JsonDeserialize(converter = JSON.TimestampJsonConvert.Deserializer.class)
     * }
     * </pre>
     *
     * @author 谢长春 2022-07-04
     */
    @Slf4j
    public static class TimestampJsonConvert extends StdConverter<String, Timestamp> {
        @Override
        public Timestamp convert(final String value) {
            return yyyy_MM_dd_HH_mm_ss_SSS.parseOfNullable(value).map(Dates::timestamp).orElse(null);
        }
    }

    public static void main(String[] args) {
        System.out.println(JSON.parseList("[{\"name\":\"id\",\"direction\":\"DESC\"}]", OrderBy.class));
    }
}
