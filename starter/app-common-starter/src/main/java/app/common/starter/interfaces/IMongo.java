package app.common.starter.interfaces;

import app.common.starter.util.Then;

import java.io.Serializable;

/**
 * 所有mongodb数据库的实体类必须实现此接口
 *
 * @author 谢长春 2017-9-26
 */
public interface IMongo<U, W extends IWhere> extends Serializable, IJson {
    default String getUpdateTime() {
        return null;
    }

    /**
     * 动态 update 语句规范
     *
     * @param update {@link Then}
     * @return {@link Then}
     */
    default Then<U> update(final U update) {
        throw new NullPointerException(this.getClass().getName().concat("：方法【update(final U update)】未实现动态更新方法"));
    }

    /**
     * <pre>
     * 构建 QueryDSL 查询条件，用于执行
     * 构建查询顺序规则：
     * 带索引的字段
     * 一次能排除最多数据的字段
     * 绝对相等的字段
     * 区间查询字段
     * like查询字段，无必要情况尽量避免字段前后都带 % 查询 %name%
     *
     * @return extends IWhere
     */
    W where();
}
