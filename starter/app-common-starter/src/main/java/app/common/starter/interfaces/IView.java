package app.common.starter.interfaces;

import java.io.Serializable;

/**
 * 所有数据库的视图必须实现此接口
 *
 * @author 谢长春 2017-9-26
 */
public interface IView<W extends IWhere> extends Serializable, IJson {


    /**
     * <pre>
     * 构建 QueryDSL 查询条件，用于执行
     * 构建查询顺序规则：
     * 带索引的字段
     * 一次能排除最多数据的字段
     * 绝对相等的字段
     * 区间查询字段
     * like查询字段，无必要情况尽量避免字段前后都带 % 查询 %name%
     *
     * @return extends IWhere
     */
    W where();
}
