package app.common.starter.web.convert;

import app.common.starter.entity.OrderBy;
import app.common.starter.util.JSON;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.List;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToListOrderConverter implements Converter<String, List<OrderBy>> {
    @Override
    public List<OrderBy> convert(@Nullable final String value) {
        return JSON.parseList(value, OrderBy.class);
    }
}
