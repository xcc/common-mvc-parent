package app.common.starter.interfaces;

import java.time.Duration;

/**
 * 所有缓存相关必须实现此接口
 *
 * @author 谢长春 2019-12-18
 */
public interface ICache {

    /**
     * 空值缓存过期时间
     */
    Duration NULL_VALUE_EXPIRED = Duration.ofMinutes(1);

    /**
     * 本地缓存 Map | List | Set
     */
    interface ILocalCache extends ICache {
        /**
         * 刷新本地缓存
         */
        void refreshCache();
//        // 主题名：发布/订阅 本地缓存清理消息队列
//        private static final String TOPIC = String.format("topic:%s", CityService.class.getSimpleName());
//        // 本地缓存清理消息
//        private static final String TOPIC_PUBLISH_MESSAGE = "clear:CACHE_MAP:%s";
//        @PostConstruct
//        public void postConstruct() {
//            // 订阅缓存清理通知
//            redissonClient.getTopic(TOPIC).addListener(String.class, (final CharSequence channel, final String msg) -> {
//                log.info(msg);
//                CACHE_MAP.clear();
//            });
//        }
//
//        /**
//         * 发送内存缓存清理通知
//         */
//        private void pushTopicClearCacheMap() {
//            redissonClient.getTopic(TOPIC).publishAsync(String.format(TOPIC_PUBLISH_MESSAGE, MDC.get(MDC_TRACE_ID)));
//        }
//        /**
//         * 获取城市缓存集合
//         *
//         * @return Map<String, TabCity>
//         */
//        public Map<String, TabCity> getCacheMap() {
//            if (CACHE_MAP.isEmpty()) {
//                synchronized (this) {
//                    if (CACHE_MAP.isEmpty()) {
//                        repository.forEach(
//                                list -> list.forEach(row -> CACHE_MAP.put(row.getId(), row))
//                                , new TabCity().where()
//                        );
//                    }
//                }
//            }
//            return CACHE_MAP;
//        }
    }
}
