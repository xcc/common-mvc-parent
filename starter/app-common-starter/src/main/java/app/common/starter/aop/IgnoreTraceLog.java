package app.common.starter.aop;

import java.lang.annotation.*;

/**
 * 忽略链路日志
 *
 * @author 谢长春 2022-02-02
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreTraceLog {

}
