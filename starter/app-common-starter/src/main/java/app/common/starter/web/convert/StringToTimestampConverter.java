package app.common.starter.web.convert;

import app.common.starter.util.Dates;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.sql.Timestamp;

import static app.common.starter.util.Dates.Pattern.yyyy_MM_dd_HH_mm_ss_SSS;

/**
 * 自定义入参转换
 *
 * @author 谢长春 2020-10-27
 */
@Component
public class StringToTimestampConverter implements Converter<String, Timestamp> {
    @Override
    public Timestamp convert(@Nullable String value) {
        return yyyy_MM_dd_HH_mm_ss_SSS.parseOfNullable(value).map(Dates::timestamp).orElse(null);
    }
}
