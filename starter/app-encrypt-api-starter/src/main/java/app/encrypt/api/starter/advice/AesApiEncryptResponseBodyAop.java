package app.encrypt.api.starter.advice;


import app.common.starter.entity.ResponseResult;
import app.common.starter.util.JSON;
import app.encrypt.api.starter.service.AesBodyService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 响应结果加密
 */
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.encrypt.api.enabled", havingValue = "true")
@ControllerAdvice(basePackages = "com.ccx.demo")
public class AesApiEncryptResponseBodyAop implements ResponseBodyAdvice<Object> {
    private final AesBodyService aesBodyService;

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        // 是否执行 beforeBodyWrite
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body,
                                  MethodParameter returnType,
                                  MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request,
                                  ServerHttpResponse response) {
        if (body instanceof ResponseResult) {
            return aesBodyService.encryptBody(JSON.toJsonString(body));
        }
        return body;
    }
}
