package app.encrypt.api.starter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * api 数据包加解密
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
// @ConditionalOnProperty(value = "spring.app.encrypt.api", havingValue = "true")
@EnableConfigurationProperties
@ComponentScan("app.encrypt.api.starter")
public class ApiEncryptConfiguration {


}
