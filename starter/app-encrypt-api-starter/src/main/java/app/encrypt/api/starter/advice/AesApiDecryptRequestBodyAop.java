package app.encrypt.api.starter.advice;

import app.encrypt.api.starter.service.AesBodyService;
import com.google.common.io.CharStreams;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 请求参数解密， 只有 POST PUT DELETE PATCH 请求，且 body 不为空的情况才需要解密。 上传文件也不会走解密逻辑
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.encrypt.api.enabled", havingValue = "true")
@ControllerAdvice(basePackages = "com.ccx.demo")
public class AesApiDecryptRequestBodyAop implements RequestBodyAdvice {
    private final AesBodyService aesBodyService;

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType,
                            Class<? extends HttpMessageConverter<?>> converterType) {
        // 是否执行 beforeBodyRead 、 afterBodyRead 、 handleEmptyBody
        return true;
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage request,
                                           MethodParameter parameter,
                                           Type targetType,
                                           Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        // 请求数据
        @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(request.getBody());
        final String body = CharStreams.toString(inputStreamReader);
        if (log.isDebugEnabled()) {
            log.debug("密文:{}", body);
        }
        return new HttpInputMessage() {
            @SneakyThrows
            @Override
            public InputStream getBody() {
                @Cleanup final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                        Optional.of(body)
                                .map(val -> {
                                    final String decrypt = aesBodyService.decryptBody(val);
                                    if (log.isInfoEnabled()) {
                                        log.info("body: {}", decrypt);
                                    }
                                    return decrypt;
                                })
                                .map(val -> val.getBytes(UTF_8))
                                .orElse(new byte[0])
                );
                return byteArrayInputStream;
            }

            @Override
            public HttpHeaders getHeaders() {
                return request.getHeaders();
            }
        };
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                Class<? extends HttpMessageConverter<?>> converterType) {
        if (log.isTraceEnabled()) log.trace("# afterBodyRead");
        return body;
    }

    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter,
                                  Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        if (log.isTraceEnabled()) log.trace("# handleEmptyBody");
        return body;
    }

}
