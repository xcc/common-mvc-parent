//package app.auto.task.starter.service;
//
//import app.auto.task.starter.interfaces.IAutoTask;
//import app.common.starter.interfaces.ICache;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.ApplicationContext;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.annotation.Validated;
//
///**
// * 定时任务: 刷新缓存: 城市名称
// *
// * @author 谢长春 2022-11-06
// */
//@Slf4j
//@Service
//@Validated
//@RequiredArgsConstructor
//@ConditionalOnProperty(value = "spring.app.autoTask.services.cacheRefreshAutoTaskService.enabled", havingValue = "true")
//public class CacheRefreshAutoTaskService implements IAutoTask {
//
//    private final ApplicationContext applicationContext;
//
//    @Override
//    @Transactional(propagation = Propagation.NEVER)
//    public void call(final String args) {
//        applicationContext.getBeansOfType(ICache.ILocalCache.class).forEach((name, service) -> {
//            try {
//                log.info("开始：{}", name);
//                service.refreshCache();
//                log.info("结束：{}", name);
//            } catch (Exception e) {
//                log.error("异常：{}", name);
//            }
//        });
//    }
//}
//
