package app.auto.task.starter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * 定时任务配置
 *
 * @author 谢长春 on 2018/1/5.
 */
@Slf4j
@Configuration
@EnableScheduling
@RequiredArgsConstructor
@ComponentScan("app.auto.task.starter")
public class ScheduleConfiguration {
    private static final int CORE_POOL_SIZE = 20;

    /**
     * 方案一：支持 CronTrigger 表达式
     * 多线程定时任务调度器
     *
     * @return ThreadPoolTaskScheduler
     */
    @Bean(destroyMethod = "destroy")
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(CORE_POOL_SIZE);
//        scheduler.setThreadNamePrefix("taskExecutor-");
//        scheduler.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
//        //调度器shutdown被调用时等待当前被调度的任务完成
//        scheduler.setWaitForTasksToCompleteOnShutdown(true);
//        //等待时长
//        scheduler.setAwaitTerminationSeconds(60);
        return scheduler;
    }

    /**
     * 方案二：仅支持时间戳
     * 多线程定时任务调度器
     *
     * @return ScheduledExecutorService
     */
    @Bean(destroyMethod = "shutdownNow")
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(CORE_POOL_SIZE);
    }

}
