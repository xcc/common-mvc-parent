package app.auto.task.starter.service;

import app.auto.task.starter.interfaces.IAutoTask;
import app.common.starter.enums.Code;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.MDC;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Pattern;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务
 *
 * @author 谢长春 2017年7月21日 上午10:19:28
 */
@Service
@Slf4j
@RequiredArgsConstructor
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring.app.auto-task")
//@ConditionalOnProperty(value = "spring.app.auto-task.enabled", havingValue = "true")
public class SchedulingConfigurerService implements SchedulingConfigurer {
    @Setter
    @Getter
    private boolean enabled;
    @Setter
    @Getter
    private LinkedHashMap<String, IAutoTask.AutoTaskItem> services;
    private final ApplicationContext applicationContext;
    private final RedissonClient redissonClient;
    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;
    private final ScheduledExecutorService scheduledExecutorService;


    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        if (!enabled) {
            log.warn("定时任务总开关处于关闭状态");
            return;
        }
        services.forEach((key, task) -> {
            final String beanName = Optional.ofNullable(task.getName()).orElse(key);
            if (!task.isEnabled()) {
                log.info("定时任务服务开关已关闭: {} : {} : {}", beanName, task.getCron(), task.getComment());
                return;
            }
            log.info("注册定时任务服务: {} : {} : {}", beanName, task.getCron(), task.getComment());
            scheduledTaskRegistrar.addCronTask(() -> {
                // 判断分布式锁开关是否打开
                if (task.isLockEnabled()) {
                    final RLock lock = redissonClient.getLock(beanName);
                    try {
                        // 锁争夺成功之后，不立即释放锁，等待 redis 锁过期，避免服务器时间差，造成重复执行
                        final boolean locked = lock.tryLock(
                                1L, // 加锁等待时间
                                task.getLockExpired().getSeconds(), // 锁过期时间， 到期自动解锁，不需要使用代码解锁
                                TimeUnit.SECONDS
                        );
                        if (!locked) {
                            throw new IllegalStateException("locked = false");
                        }
                    } catch (Exception e) {
                        // 锁争夺失败， 不执行业务逻辑，直接退出
                        log.info("锁争夺失败: {} : {} : {}", beanName, task.getComment(), e.getMessage());
                        return;
                    }
                }
                try {
                    MDC.put("traceId", RandomStringUtils.randomAlphanumeric(16));
                    if (log.isInfoEnabled()) {
                        log.info("开始:{}", task.getComment());
                    }
                    applicationContext.getBean(beanName, IAutoTask.class).call(null);
                } catch (Exception e) {
                    log.error("异常：{}", task.getComment(), e);
                } finally {
                    log.info("结束:{}", task.getComment());
                    MDC.remove("traceId");
                }
            }, task.getCron());

            if (task.isStarted()) {
                // 判断分布式锁开关是否打开
                if (task.isLockEnabled()) {
                    final RLock lock = redissonClient.getLock(beanName);
                    try {
                        // 锁争夺成功之后，不立即释放锁，等待 redis 锁过期，避免服务器时间差，造成重复执行
                        final boolean locked = lock.tryLock(
                                1L, // 加锁等待时间
                                task.getLockExpired().getSeconds(), // 锁过期时间， 到期自动解锁，不需要使用代码解锁
                                TimeUnit.SECONDS
                        );
                        if (!locked) {
                            throw new IllegalStateException("locked = false");
                        }
                    } catch (Exception e) {
                        // 锁争夺失败， 不执行业务逻辑，直接退出
                        log.info("锁争夺失败: {} : {} : {}", beanName, task.getComment(), e.getMessage());
                        return;
                    }
                }
                // 启动时先执行一次， ScheduledExecutorService 添加延时功能，保证服务启动成功之后再执行定时任务，否则在 bean 初始化时执行，会出现报错
                scheduledExecutorService.schedule(
                        () -> {
                            try {
                                MDC.put("traceId", RandomStringUtils.randomAlphanumeric(16));
                                applicationContext.getBean(beanName, IAutoTask.class).started();
                            } catch (Exception e) {
                                log.error("异常：{}", task.getComment(), e);
                            } finally {
                                log.info("结束:{}", task.getComment());
                                MDC.remove("traceId");
                            }
                        },
                        1,
                        TimeUnit.SECONDS
                );
            }
        });
        scheduledTaskRegistrar.setScheduler(threadPoolTaskScheduler);
    }

    /**
     * 触发定时任务集合中指定下标的定时任务， 前提是定时任务开关处于开启状态
     *
     * @param beanName int {@link SchedulingConfigurerService#getServices()} 集合下标
     */
    public void call(final String beanName) {
        Code.A00002.assertHasTrue(enabled, "定时任务总开关处于关闭状态");
        final IAutoTask.AutoTaskItem task = services.get(beanName);
        Code.A00002.assertHasTrue(task.isEnabled(), "[%s:%s] 定时任务开关处于关闭状态", beanName, task.getComment());
        applicationContext.getBean(Optional.ofNullable(task.getName()).orElse(beanName), IAutoTask.class).call(null);
    }

    /**
     * 触发定时任务集合中指定下标的定时任务， 前提是定时任务开关处于开启状态
     *
     * @param beanName int {@link SchedulingConfigurerService#getServices()} 集合下标
     * @param args     String {@link String} 日期格式： yyyyMMdd 或 yyyyMMddHHmmss
     */
    public void call(final String beanName, @Pattern(regexp = "\\d{8,}") final String args) {
        Code.A00002.assertHasTrue(enabled, "定时任务总开关处于关闭状态");
        final IAutoTask.AutoTaskItem task = services.get(beanName);
        Code.A00002.assertHasTrue(task.isEnabled(), "[%s:%s] 定时任务开关处于关闭状态", beanName, task.getComment());
        applicationContext.getBean(Optional.ofNullable(task.getName()).orElse(beanName), IAutoTask.class).call(args);
    }
}
