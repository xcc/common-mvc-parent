package app.auto.task.starter.service;
//
//import app.auto.task.starter.interfaces.IAutoTask;
//import app.common.starter.enums.Code;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.ApplicationContext;
//import org.springframework.stereotype.Service;
//
//import javax.validation.constraints.Pattern;
//import java.util.Optional;
//
///**
// * 定时任务
// *
// * @author 谢长春 2017年7月21日 上午10:19:28
// */
//@Service
//@Slf4j
//@RequiredArgsConstructor
//public class AutoTaskCallService {
//
//    private final ApplicationContext applicationContext;
//    private final SchedulingConfigurerService service;
//
//    /**
//     * 触发定时任务集合中指定下标的定时任务， 前提是定时任务开关处于开启状态
//     *
//     * @param beanName int {@link SchedulingConfigurerService#getServices()} 集合下标
//     */
//    public void call(final String beanName) {
//        Code.A00002.assertHasTrue(service.isEnabled(), "定时任务总开关处于关闭状态");
//        final IAutoTask.AutoTaskItem task = service.getServices().get(beanName);
//        Code.A00002.assertHasTrue(task.isEnabled(), "[%s:%s] 定时任务开关处于关闭状态", beanName, task.getComment());
//        applicationContext.getBean(Optional.ofNullable(task.getName()).orElse(beanName), IAutoTask.class).call(null);
//    }
//
//    /**
//     * 触发定时任务集合中指定下标的定时任务， 前提是定时任务开关处于开启状态
//     *
//     * @param beanName int {@link SchedulingConfigurerService#getServices()} 集合下标
//     * @param args     String {@link String} 日期格式： yyyyMMdd 或 yyyyMMddHHmmss
//     */
//    public void call(final String beanName, @Pattern(regexp = "\\d{8,}") final String args) {
//        Code.A00002.assertHasTrue(service.isEnabled(), "定时任务总开关处于关闭状态");
//        final IAutoTask.AutoTaskItem task = service.getServices().get(beanName);
//        Code.A00002.assertHasTrue(task.isEnabled(), "[%s:%s] 定时任务开关处于关闭状态", beanName, task.getComment());
//        applicationContext.getBean(Optional.ofNullable(task.getName()).orElse(beanName), IAutoTask.class).call(args);
//    }
//}
