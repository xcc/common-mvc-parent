package app.auto.task.starter.controller;

import app.auto.task.starter.service.SchedulingConfigurerService;
import app.common.starter.entity.Result;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.Pattern;

/**
 * 定时任务
 *
 * @author 谢长春 2017年7月21日 上午10:19:28
 */
@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/auto-task")
@Slf4j
@RequiredArgsConstructor
public class AutoTaskController {
    private final SchedulingConfigurerService service;

    /**
     * 触发定时任务集合中指定名称的定时任务， 前提是定时任务开关处于开启状态
     *
     * @param beanName String {@link app.auto.task.starter.service.SchedulingConfigurerService#getServices()} 集合下标
     * @return {@link Result}
     */
    @PostMapping("/call/{beanName}")
    @ResponseBody
    public Result<Void> call(@PathVariable final String beanName) {
        return new Result<Void>().call(() -> service.call(beanName));
    }

    /**
     * 触发定时任务集合中指定名称的定时任务， 前提是定时任务开关处于开启状态
     *
     * @param beanName String {@link app.auto.task.starter.service.SchedulingConfigurerService#getServices()} 集合下标
     * @param args     String {@link String} 日期格式： yyyyMMdd 或 yyyyMMddHHmmss
     * @return {@link Result}
     */
    @PostMapping("/call/{beanName}/{args}")
    @ResponseBody
    public Result<Void> call(@PathVariable final String beanName,
                             @PathVariable @Pattern(regexp = "\\d{8,}") final String args) {
        return new Result<Void>().call(() -> service.call(beanName, args));
    }
}
