package com;

import app.common.starter.entity.ResponseResult;
import app.common.starter.enums.Code;
import app.common.starter.exception.CodeException;
import app.common.starter.interfaces.ICall;
import app.common.starter.interfaces.ICode;
import app.common.starter.util.ChainMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * 返回结果集对象
 *
 * @author 谢长春 2017-9-20
 */
@ToString(callSuper = true)
@Slf4j
@Accessors(chain = true)
@ApiModel(description = "全局通用响应对象")
public class AutoTaskResult<E> extends ResponseResult<E> {

    /**
     * 默认构造函数
     */
    public AutoTaskResult() {
        super();
    }

    /**
     * 带参构造函数
     *
     * @param code {@link ICode} 操作响应码
     */
    public AutoTaskResult(final ICode code) {
        super(code);
        this.code = code.name();
    }

    @ApiModelProperty(position = 1, value = "状态码")
    private String code;

    @Setter
    @ApiModelProperty(position = 2, value = "响应消息，用于页面弹窗内容")
    private String message;

    @ApiModelProperty(position = 3, value = "本次响应数据行数，data 集合大小")
    @Override
    public long getRowCount() {
        return super.getRowCount();
    }

    @ApiModelProperty(position = 4, value = "总页数，该参数分页查询时才会起作用，其他情况一直返回 0")
    @Override
    public int getPageCount() {
        return super.getPageCount();
    }

    @ApiModelProperty(position = 5, value = "总行数，该参数分页查询时才会起作用，其他情况一直返回 0")
    @Override
    public long getTotalCount() {
        return super.getTotalCount();
    }

    @ApiModelProperty(position = 7, value = "异常消息，用于开发调试")
    @Override
    public String getException() {
        return super.getException();
    }

    @ApiModelProperty(position = 8, value = "数据集合，查询单条数据或多条数据，都放在泛型集合中")
    @Override
    public List<E> getData() {
        return super.getData();
    }

    @ApiModelProperty(position = 9, value = "扩展属性， 补充 data 集合")
    @Override
    public Map<String, Object> getExtras() {
        return super.getExtras();
    }

    /**
     * 本次链路追踪
     *
     * @return {@link String}
     */
    @ApiModelProperty(position = 6, value = "链路追踪 id")
    public String getXid() {
        return MDC.get("traceId");
    }

    /**
     * 将编码转换成具体消息
     *
     * @return String
     */
    @Override
    public String getMessage() {
        final String message = super.getMessage();
        return StringUtils.isBlank(message) ? this.message : message;
    }

    public void setCode(final String code) {
        this.code = code;
        final Supplier<String> getMessage = () -> this.message;
        super.setCode(new ICode() {
            @Override
            public String name() {
                return code;
            }

            @Override
            public String getComment() {
                return getMessage.get();
            }
        });
    }

    @Override
    public AutoTaskResult<E> setCode(final ICode code) {
        this.code = code.name();
        this.message = code.getComment();
        super.setCode(code);
        return this; // 保证链式请求，返回:this
    }

    @Override
    public AutoTaskResult<E> setException(final String exception) {
        super.setException(exception);
        return this; // 保证链式请求，返回:this
    }
//
//    /**
//     * 设置异常编码及异常信息
//     *
//     * @param code      {@link ICode} 异常响应码
//     * @param exception String 异常消息内容
//     */
//    @Override
//    public AutoTaskResult<E> setException(final ICode code, final String exception) {
//        super.setException(code, exception);
//        return this; // 保证链式请求，返回:this
//    }

    /**
     * 将业务逻辑中捕获到的异常转换为对应的code
     *
     * @param e {@link Exception} 捕获到的异常
     * @return AutoTaskResult<E>
     */
    @Override
    public AutoTaskResult<E> setException(final Exception e) {
        super.setException(e);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param data List<E>
     * @return AutoTaskResult<E>
     */
    @JsonIgnore
    @Override
    public AutoTaskResult<E> setSuccess(final List<E> data) {
        super.setSuccess(data);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param data E[]
     * @return AutoTaskResult<E>
     */
    @JsonIgnore
    @Override
    public AutoTaskResult<E> setSuccess(final E[] data) {
        super.setSuccess(data);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 重载方法，设置成功后的数据集合；返回当前对象，便于链式调用
     *
     * @param data E
     * @return AutoTaskResult<E>
     */
    @JsonIgnore
    @Override
    public AutoTaskResult<E> setSuccess(final E data) {
        super.setSuccess(data);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 添加扩展属性，返回Result对象本身，支持链式请求
     *
     * @param key   String
     * @param value Object
     * @return AutoTaskResult<E>
     */
    @Override
    public AutoTaskResult<E> addExtras(final String key, final Object value) {
        super.addExtras(key, value);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 添加扩展属性，返回Result对象本身，支持链式请求
     *
     * @return AutoTaskResult<E>
     */
    @Override
    public AutoTaskResult<E> addExtras(final ChainMap<String, Object> obj) {
        super.addExtras(obj);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 添加扩展属性，返回Result对象本身，支持链式请求
     *
     * @return AutoTaskResult<E>
     */
    @Override
    public AutoTaskResult<E> addExtras(final Map<String, String> extras) {
        super.addExtras(extras);
        return this; // 保证链式请求，返回:this
    }

    /**
     * 判断 code 是否为 SUCCESS
     *
     * @return AutoTaskResult<E> code == SUCCESS 返回结果集对象
     * @throws CodeException code != SUCCESS 则抛出异常
     */
    @Override
    @JsonIgnore
    public AutoTaskResult<E> isSuccess() throws CodeException {
        super.isSuccess();
        return this;
    }

    /**
     * 判断 code 是否为 SUCCESS
     *
     * @return AutoTaskResult<E> code == SUCCESS 返回结果集对象
     * @throws CodeException code != SUCCESS 则抛出异常
     */
    @Override
    @JsonIgnore
    public AutoTaskResult<E> assertSuccess() throws CodeException {
        super.assertSuccess();
        return this;
    }

    /**
     * 判断 rowCount 是否等于0
     *
     * @return true等于0，false不等于0
     */
    @Override
    @JsonIgnore
    public boolean isEmpty() {
        return super.isEmpty();
    }

    /**
     * 判断 rowCount 是否大于0
     *
     * @return true大于0
     */
    @Override
    @JsonIgnore
    public boolean isNonEmpty() {
        return super.isNonEmpty();
    }

    /**
     * 获取data集合中的第一项;获取前先校验集合长度是否大于0
     *
     * @return E
     */
    @Override
    public Optional<E> dataFirst() {
        return super.dataFirst();
    }

    /**
     * <pre>
     * 执行 consumer 代码逻辑；
     * 处理 consumer 异常，并设置到响应对象中，但不会调用 {@link AutoTaskResult#setCode}({@link Code#A00000})，需要在 consumer 中设置成功状态
     * {@link AutoTaskResult#execute} 可执行多次，上一个 {@link AutoTaskResult#execute} 执行失败，并不会影响下一次 {@link AutoTaskResult#execute} 执行，响应状态以最后一次 {@link AutoTaskResult#execute} 指定的状态
     *
     * @param consumer {@link Consumer<AutoTaskResult>}
     * @return {@link AutoTaskResult}{@link AutoTaskResult<E>}
     */
    public AutoTaskResult<E> execute(final Consumer<AutoTaskResult<E>> consumer) {
        try {
            consumer.accept(this);
        } catch (Exception e) {
//            log.error(e.getMessage(), e);
            throw e;
        }
        return this;
    }

    /**
     * 执行 call 代码逻辑；如果 call 不抛异常，则调用 {@link AutoTaskResult#setCode}({@link Code#A00000})
     *
     * @param call {@link ICall}
     * @return {@link AutoTaskResult}{@link AutoTaskResult<E>}
     */
    public AutoTaskResult<E> call(final ICall call) {
        try {
            call.call();
            setCode(Code.A00000);
        } catch (Exception e) {
//            log.error(e.getMessage(), e);
            throw e;
        }
        return this;
    }

    /**
     * <pre>
     * 同 {@link AutoTaskResult#execute} 类似，但是会先判断 code == Code.A00000，才会执行 consumer
     * 一般 then 应该在 {@link AutoTaskResult#execute} 之后，then 表示上一步执行状态为 SUCCESS , 才会继续执行
     *
     * @param consumer {@link Consumer<AutoTaskResult>}
     * @return {@link AutoTaskResult}{@link AutoTaskResult<E>}
     */
    public AutoTaskResult<E> then(final Consumer<AutoTaskResult<E>> consumer) {
        if (Objects.equals(Code.A00000.name(), this.getCode())) {
            try {
                consumer.accept(this);
            } catch (Exception e) {
//                log.error(e.getMessage(), e);
                throw e;
            }
        }
        return this;
    }
    // 扩展方法：end *************************************************************************************************************************************************

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public AutoTaskResult<E> cloneObject() {
        return (AutoTaskResult<E>) super.clone();
    }

    //    public static void main(String[] args) {
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 打印所有状态码 <<<<<<<<<<<<<<<<<<");
//            for (AppCode code : AppCode.values()) {
//                log.info(code + ":" + code.comment);
//            }
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 打印对象 toJson 之后的全部字段 <<<<<<<<<<<<<<<<<<");
//            log.info(JSON.toJsonString(new AutoTaskResult<>(),
//                    SerializerFeature.WriteMapNullValue,
//                    SerializerFeature.WriteNullBooleanAsFalse,
//                    SerializerFeature.WriteNullListAsEmpty,
//                    SerializerFeature.WriteNullNumberAsZero,
//                    SerializerFeature.WriteNullStringAsEmpty
//            ));
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 声明data集合中只能是String <<<<<<<<<<<<<<<<<<");
//            AutoTaskResult<String> result = new AutoTaskResult<>();
//            // 设置单一对象，必须是泛型声明的类型
//            result.setSuccess("111");
//            log.info(result.json());
//            // 设置多个对象，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList("222", "333"));
//            log.info(result.json());
//            // 设置对象对象数组，必须是泛型声明的类型
//            result.setSuccess(new String[]{"444", "555"});
//            log.info(result.json());
//            // 设置对象集合，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList("666", "777"));
//            // 带有附加属性(扩展属性),可以链式调用
//            result.addExtras("name", "JX").addExtras("amount", 100).addExtras("roles", new String[]{"ROLE_USER"});
//            log.info(result.json());
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 声明data集合中只能是Map<String, Object> <<<<<<<<<<<<<<<<<<");
//            AutoTaskResult<Map<String, Object>> result = new AutoTaskResult<>();
//            // 设置单一对象，必须是泛型声明的类型
//            result.setSuccess(Maps.bySO("key", "111"));
//            log.info(result.json());
//            // 设置多个对象，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Maps.bySO("key", "222"), Maps.bySO("key", "333")));
//            log.info(result.json());
//            // 设置对象集合，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Maps.bySO("key", "444"), Maps.bySO("key", "555")));
//            // 带有附加属性(扩展属性),可以链式调用
//            result.addExtras("name", "JX").addExtras("amount", 100).addExtras("roles", new String[]{"ROLE_USER"});
//                log.info(result.json());
//        }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 声明data集合中只能是 Item <<<<<<<<<<<<<<<<<<");
//            AutoTaskResult<Item> result = new AutoTaskResult<>();
//            // 设置单一对象，必须是泛型声明的类型
//            result.setSuccess(Item.builder().key("key").value(111).build());
//            log.info(result.json());
//            // 设置多个对象，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Item.builder().key("key").value(222).build(), Item.builder().key("key").value(333).build()));
//            log.info(result.json());
//            // 设置对象对象数组，必须是泛型声明的类型
//            result.setSuccess(new Item[]{Item.builder().key("key").value(444).build(), Item.builder().key("key").value(555).build()});
//            log.info(result.json());
//            // 设置对象集合，必须是泛型声明的类型
//            result.setSuccess(Arrays.asList(Item.builder().key("key").value(666).build(), Item.builder().key("key").value(777).build()));
//            // 带有附加属性(扩展属性),可以链式调用
//            result.addExtras("name", "JX").addExtras("amount", 100).addExtras("roles", new String[]{"ROLE_USER"});
//            log.info(result.json());
//            }
//        {
//            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 将 Json 字符串反序列化为Result对象 <<<<<<<<<<<<<<<<<<");
//            log.info(Result.valueOfJson("{\"code\":\"SUCCESS\",\"message\":\"成功\",\"rowCount\":90,\"pageCount\":100,\"totalCount\":200,\"data\":[\"A\",\"B\"]}").json());
//            log.info(Result.valueOfJson("{\"code\":\"SUCCESS\",\"message\":\"成功\",\"rowCount\":90,\"pageCount\":100,\"totalCount\":200,\"data\":[\"A\",\"B\"]}").json());
//            log.info(Result.valueOfJson("{\"code\":\"FAILURE\",\"message\":\"失败\",\"data\":[{\"name\":\"A\"},{\"name\":\"B\"}]}").json());
//        }
////        {
////            try {
////                AutoTaskResult<Table> result = JSON.parseObject(FileUtil.read("D:\\project\\files\\upload-com-data\\c9d6ad96-3eed-4d70-879b-bead504f0730\\2018\\BudgetMainIncome\\66f871de-5265-462d-8f55-1e34baa0e286.json"), new TypeReference<AutoTaskResult<Table>>(){});
////                log.info("{}",result.dataFirst());
////                log.info(result.json());
////            } catch (IOException e) {
////                log.error(e.getMessage(), e);
////            }
////        }
//
//    }

}
