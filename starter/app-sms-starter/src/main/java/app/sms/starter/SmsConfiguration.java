package app.sms.starter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
//@Import({FeignConfiguration.class})
@ComponentScan("app.sms.starter")
@EnableConfigurationProperties
public class SmsConfiguration {

    private static ApplicationContext APP_CONTEXT;
    private final ApplicationContext applicationContext;

    @PostConstruct
    public void postConstruct() {
        APP_CONTEXT = applicationContext;
    }

}
