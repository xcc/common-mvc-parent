package app.sms.starter.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 服务接口实现类：短信发送
 *
 * @author 谢长春 2022-04-10
 */
@Primary
@Slf4j
@Service
@RequiredArgsConstructor
@ConfigurationProperties("spring.app.sms")
@ConditionalOnProperty(value = "spring.app.sms.enabled", havingValue = "true")
public class SmsServiceProdImpl implements SmsService {
    /**
     * 短信开关。 true：是，false：否。
     * 是否开启手机验证码校验；开发环境关闭该开关可以用任意验证码登录，一般用于自动化测试和开发环境，生产环境请启用该配置
     */
    @Setter
    @Getter
    private boolean enabled;
    /**
     * 账号
     */
    @Setter
    @Getter
    private String username;
    /**
     * 密码
     */
    @Setter
    @Getter
    private String password;

    @Async
    @Override
    public void send(final String phone, final String content) {
// fixme: 补充短信发送逻辑
//        final ResponseEntity<String> responseEntity = smsApiService.call(username, password, phone, content);
//        Assert.isTrue(Objects.equals(HttpStatus.OK, responseEntity.getStatusCode()),
//                () -> String.format("短信接口请求失败:%d", responseEntity.getStatusCodeValue()));
//        if (!Objects.equals("0", responseEntity.getBody())) {
//            log.error("code:{}； {}：{}", responseEntity.getBody(), phone, content);
//            throw AppCode.B02002.toCodeException();
//        }
    }
}
