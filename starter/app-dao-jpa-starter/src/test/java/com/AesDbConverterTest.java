package com;

import app.dao.jpa.starter.convert.AesDBConverter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * 数据敏感字段加解密
 *
 * @author 谢长春 2022-09-17
 */
@Slf4j
public final class AesDbConverterTest {

    @SneakyThrows
    public static void main(String[] args) {
        AesDBConverter.setEnabled(true);

        String env = "local";
//        env = "dev";
//        env = "sit";
//        env = "uat";
//        env = "prod";
        if (Objects.equals(env, "local") || Objects.equals(env, "dev")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/dev/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/dev/spring.app.encrypt.db.ivParameterSpec")));
            AesDBConverter.setSecretKey(secretKey);
            AesDBConverter.setIV(ivParameterSpec);

            final AesDBConverter convert = new AesDBConverter();
            System.out.println(env + ": \"" + convert.convertToDatabaseColumn("18800000000") + "\"");
            System.out.println(env + ": \"" + convert.convertToEntityAttribute("mazJ1tRuRp6QXV5gTSKtGw==") + "\"");

        }
        if (Objects.equals(env, "sit")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.ivParameterSpec")));
            AesDBConverter.setSecretKey(secretKey);
            AesDBConverter.setIV(ivParameterSpec);

            final AesDBConverter convert = new AesDBConverter();
            System.out.println(env + ": \"" + convert.convertToDatabaseColumn("18800000000") + "\"");
            System.out.println(env + ": \"" + convert.convertToEntityAttribute("mazJ1tRuRp6QXV5gTSKtGw==") + "\"");
        }
        if (Objects.equals(env, "uat")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.ivParameterSpec")));
            AesDBConverter.setSecretKey(secretKey);
            AesDBConverter.setIV(ivParameterSpec);

            final AesDBConverter convert = new AesDBConverter();
            System.out.println(env + ": \"" + convert.convertToDatabaseColumn("18800000000") + "\"");
            System.out.println(env + ": \"" + convert.convertToEntityAttribute("mazJ1tRuRp6QXV5gTSKtGw==") + "\"");
        }
        if (Objects.equals(env, "prod")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.ivParameterSpec")));
            AesDBConverter.setSecretKey(secretKey);
            AesDBConverter.setIV(ivParameterSpec);

            final AesDBConverter convert = new AesDBConverter();
            System.out.println(env + ": \"" + convert.convertToDatabaseColumn("18800000000") + "\"");
            System.out.println(env + ": \"" + convert.convertToEntityAttribute("mazJ1tRuRp6QXV5gTSKtGw==") + "\"");
        }
    }
}
