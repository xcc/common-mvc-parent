package app.dao.jpa.starter.convert;

import com.google.common.base.Strings;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 支撑 mysql 数组类型与实体类属性的映射；可以使用 find_in_set 查询；
 * 需要在实体类属性上添加注解：@Convert(converter = {@link ArrayIntegerDBConverter}.class)
 *
 * @author 谢长春 2022-10-14
 */
@Converter
public class ArrayIntegerDBConverter implements AttributeConverter<Integer[], String> {
    @Override
    public String convertToDatabaseColumn(final Integer[] attribute) {
        if (Objects.isNull(attribute)) {
            return "";
        }
        if (attribute.length == 0) {
            return "";
        }
        return Stream.of(attribute)
                .filter(Objects::nonNull)
                .map(Objects::toString)
                .collect(Collectors.joining(","));
    }

    @Override
    public Integer[] convertToEntityAttribute(final String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new Integer[0];
        }
        return Arrays.stream(dbData.split(","))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);
    }
}
