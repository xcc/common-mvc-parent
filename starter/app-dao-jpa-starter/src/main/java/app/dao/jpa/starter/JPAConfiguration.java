package app.dao.jpa.starter;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;

/**
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableJpaAuditing
@Import({DbEncryptConfiguration.class})
@ComponentScan("app.dao.jpa.starter")
public class JPAConfiguration {
    private static volatile JPAQueryFactory JPA_QUERY_FACTORY_INSTANCE = null;

    private static ApplicationContext APP_CONTEXT;
    private final ApplicationContext applicationContext;

    @PostConstruct
    public void postConstruct() {
        APP_CONTEXT = applicationContext;
    }

    /**
     * 获取数据库对象操作实例
     *
     * @return {@link JPAQueryFactory}
     */
    public static JPAQueryFactory getQueryFactory() {
        if (Objects.isNull(JPA_QUERY_FACTORY_INSTANCE)) {
            synchronized (JPAConfiguration.class) {
                if (Objects.isNull(JPA_QUERY_FACTORY_INSTANCE)) {
                    JPA_QUERY_FACTORY_INSTANCE = APP_CONTEXT.getBean(JPAQueryFactory.class);
                }
            }
        }
        return JPA_QUERY_FACTORY_INSTANCE;

    }

    @PersistenceContext
    private EntityManager entityManager;

    @Bean
    public JPAQueryFactory jpaQueryFactory() {
        return new JPAQueryFactory(entityManager);
    }

//    @Bean
//    public com.querydsl.sql.Configuration sqlQueryDslConfiguration() {
//        final com.querydsl.sql.SQLTemplates templates = com.querydsl.sql.MySQLTemplates.builder().build();
//        final com.querydsl.sql.Configuration configuration = new com.querydsl.sql.Configuration(templates);
//        configuration.setExceptionTranslator(new com.querydsl.sql.spring.SpringExceptionTranslator());
//        return configuration;
//    }
//
//    @Bean
//    public com.querydsl.sql.SQLQueryFactory sqlQueryFactory(final DataSource dataSource) {
//        final javax.inject.Provider<java.sql.Connection> provider = new com.querydsl.sql.spring.SpringConnectionProvider(dataSource);
//        return new com.querydsl.sql.SQLQueryFactory(sqlQueryDslConfiguration(), provider);
//    }

}
