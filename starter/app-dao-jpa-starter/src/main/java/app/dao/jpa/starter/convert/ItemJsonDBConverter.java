package app.dao.jpa.starter.convert;

import app.common.starter.entity.Item;
import app.common.starter.util.JSON;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

/**
 * 支撑 mysql 原生 JSON 数据类型与实体类属性的映射；
 * 需要在实体类属性上添加注解：@Convert(converter = {@link ItemJsonDBConverter}.class)
 *
 * @author 谢长春 2019/2/12
 */
@Converter
public class ItemJsonDBConverter implements AttributeConverter<Item, String> {
    @Override
    public String convertToDatabaseColumn(final Item attribute) {
        if (Objects.isNull(attribute)) {
            return null;
        }
        return JSON.toJsonString(attribute);
    }

    @Override
    public Item convertToEntityAttribute(final String dbData) {
        return JSON.parseObjectOptional(dbData, Item.class).orElse(null);
    }
}
