package app.dao.jpa.starter.generator;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;
import java.util.Properties;

import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmssSSS;

/**
 * <pre>
 * 生成 18 位 有序 id，同一毫秒生成的 id 不是绝对有序的
 * 规则： 11位日期=Base36(Long(yyyyMMddHHmmssSSS)) + 7位随机=RandomStringUtils.randomAlphanumeric(7)
 *
 * 使用参考：在实体类主键 ID 字段使用注解
 * {@code
 *   @Id
 *   @GeneratedValue(generator = "base36")
 *   @GenericGenerator(name = "base36", strategy = "app.dao.jpa.generator.Base36Generator")
 *   private String id;
 * }
 * @author 谢长春 2022-06-25
 */
@Slf4j
public class Base36Generator implements IdentifierGenerator {

    private static final String BASE_CHAR = "0123456789abcdefghijklmnopqrstuvwxyz";
    private static final int BASE_CHAR_LENGTH = BASE_CHAR.length();
    private static final int RANDOM_LENGTH = 7;
//    /**
//     * 随机数长度
//     */
//    private static final int RANDOM_LENGTH = 10;
//    /**
//     * 所有 mac 地址的二进制之和，不足5位前面补0，超过5位从右侧开始截取5位
//     */
//    private static String MAC_SUM = "00000";
//    static {
//        try {
//            int macSum = 0;
//            for (Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces(); networkInterfaces.hasMoreElements(); ) {
//                final NetworkInterface networkInterface = networkInterfaces.nextElement();
//                if (networkInterface.isLoopback()
//                        || networkInterface.isVirtual()
//                        || !networkInterface.isUp()
//                        || !isIpV4(networkInterface.getInetAddresses())
//                ) {
//                    continue;
//                }
//                final byte[] mac = networkInterface.getHardwareAddress();
//                if (mac != null) {
//                    final StringBuilder sb = new StringBuilder();
//                    for (byte b : mac) {
//                        macSum += Byte.toUnsignedLong(b);
//                        sb.append(String.format("%02X", b));
//                    }
//                    if (log.isDebugEnabled()) {
//                        log.debug("mac:{} => {} - {}", sb, networkInterface.getName(), networkInterface.getDisplayName());
//                    }
//                }
//            }
//            MAC_SUM = StringUtils.right(String.format("%05d", macSum), MAC_SUM.length());
//            if (log.isDebugEnabled()) {
//                log.debug("MAC_SUM:{}", MAC_SUM);
//            }
//        } catch (Exception e) {
//            log.error("MAC地址获取失败", e);
//        }
//    }
//
//    /**
//     * 判断是否为 ipv4 地址
//     *
//     * @param address Enumeration<InetAddress>
//     * @return true：是，false：否
//     */
//    public static boolean isIpV4(final Enumeration<InetAddress> address) {
//        while (address.hasMoreElements()) {
//            InetAddress inetAddress = address.nextElement();
//            if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
//                return true;
//            }
//        }
//        return false;
//    }

    /**
     * 从 10 进制 转 36 进制
     *
     * @param value {@link Long} 十进制数字
     * @return {@link String} 36进制字符串
     */
    private static String encodeBase36(BigInteger value) {
        //if (BigInteger.valueOf(BASE_CHAR_LENGTH).compareTo(value) > 0) {
        //    return BASE_CHAR.charAt(value.intValue()) + "";
        //}
        final BigInteger baseLength = BigInteger.valueOf(BASE_CHAR_LENGTH);
        final StringBuilder sb = new StringBuilder();
        int rem;
        while (!Objects.equals(BigInteger.ZERO, value)) {
            rem = value.remainder(baseLength).intValue();
            sb.append(BASE_CHAR.charAt(rem));
            value = value.divide(baseLength);
        }
        return sb.reverse().toString();
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {

    }

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return encodeBase36(new BigInteger(yyyyMMddHHmmssSSS.now())).concat(RandomStringUtils.randomAlphanumeric(RANDOM_LENGTH));
    }

//    @SneakyThrows
//    public static void main(String[] args) {
//        final Base36Generator generator = new Base36Generator();
//        long start = System.currentTimeMillis();
//        for (int i = 0; i < 10000000; i++) {
//            generator.generate(null, null);
//        }
//        System.out.printf("耗时 %d ms%n", System.currentTimeMillis() - start);
//        {
//            String encode = (String) generator.generate(null, null);
//            System.out.printf("encode:%s => %d%n", encode, encode.length());
//        }
//        {
//            ExecutorService executorService = Executors.newFixedThreadPool(8);
//            for (int i = 0; i < 8; i++) {
//                final int index = i;
//                executorService.submit(() -> {
//                    try {
//                        final Path path = Paths.get(".temp/", Base36Generator.class.getSimpleName() + index + ".csv");
//                        if (!Files.exists(path)) {
//                            Files.createFile(path);
//                        }
//                        @Cleanup final OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.APPEND);
//                        @Cleanup final OutputStreamWriter writer = new OutputStreamWriter(outputStream);
//                        for (int j = 0; j < 10000000; j++) {
//                            try {
//                                writer.write((generator.generate(null, null)) + "\n");
//                            } catch (IOException e) {
//                                log.error(e.getMessage());
//                            }
//                        }
//                        writer.flush();
//                    } catch (IOException e) {
//                        log.error(">> {}", e.getMessage());
//                    }
//                });
//            }
//            executorService.shutdown();
//        }
//    }
}
