package app.dao.jpa.starter.convert;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 支撑 mysql 数组类型与实体类属性的映射；可以使用 find_in_set 查询；
 * 需要在实体类属性上添加注解：@Convert(converter = {@link ArrayStringDBConverter}.class)
 *
 * @author 谢长春 2022-10-14
 */
@Converter
public class ArrayStringDBConverter implements AttributeConverter<String[], String> {
    @Override
    public String convertToDatabaseColumn(final String[] attribute) {
        if (Objects.isNull(attribute)) {
            return "";
        }
        if (attribute.length == 0) {
            return "";
        }
        return Stream.of(attribute)
                .filter(StringUtils::isNoneBlank)
                .collect(Collectors.joining(","));
    }

    @Override
    public String[] convertToEntityAttribute(final String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new String[0];
        }
        return Arrays.stream(dbData.split(","))
                .toArray(String[]::new);
    }
}
