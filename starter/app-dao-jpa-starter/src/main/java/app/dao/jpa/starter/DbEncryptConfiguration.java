package app.dao.jpa.starter;

import app.dao.jpa.starter.convert.AesDBConverter;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * 数据敏感字段加解密
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableConfigurationProperties
@ConfigurationProperties("spring.app.encrypt.db")
public class DbEncryptConfiguration {
    /**
     * 加解密开关。 true：是，false：否。
     */
    @Setter
    @Getter
    private boolean enabled;
    /**
     * 全局 aes 密钥， 32位
     */
    @Setter
    @Getter
    private String secretKey;
    /**
     * 固定 IV ， 16位。 设置固定 IV 之后同样内容每次加密结果一样。
     */
    @Setter
    @Getter
    private String ivParameterSpec;

    private static final int MAX = 70;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PostConstruct
    public void postConstruct() {
//        AesDbConvert.setEnabled(enabled);
//        AesDbConvert.setSecretKey(secretKey); // 设置全局 aes 密钥
//        if (enabled) {
//            if (Strings.isNullOrEmpty(ivParameterSpec)) {
//                throw new IllegalArgumentException("spring.app.encrypt.db.ivParameterSpec='请指定16位固定IV'");
//            } else {
//                AesDbConvert.setIV(ivParameterSpec);
//            }
//        }
        log.info(LOG_PATTERN
                , "数据敏感字段加解密"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.db.enabled: " + enabled, MAX, ' '), MAX) + " # 是否开启数据库敏感字段加密"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.db.secretKey: " + secretKey, MAX, ' '), MAX) + " # 密钥"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.db.ivParameterSpec: " + ivParameterSpec, MAX, ' '), MAX) + " # 固定 IV ， 16位"
                )
                , "数据敏感字段加解密"
        );
        if (!enabled) {
            return;
        }
        if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
            // 设置密钥 32 位: RandomStringUtils.randomAlphanumeric(32)
            throw new IllegalArgumentException("spring.app.encrypt.db.secretKey='请指定32位密钥'");
        }
        if (Strings.isNullOrEmpty(ivParameterSpec) || ivParameterSpec.length() != 16) {
            // 设置 IV ， 长度必须是 16 位 RandomStringUtils.randomAlphanumeric(16)
            throw new IllegalArgumentException("spring.app.encrypt.db.ivParameterSpec='请指定16位固定IV'");
        }
        AesDBConverter.setEnabled(enabled);
        AesDBConverter.setSecretKey(secretKey); // 设置全局 aes 密钥
        AesDBConverter.setIV(ivParameterSpec);
    }

}
