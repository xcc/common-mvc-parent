package app.security.starter;

import app.security.starter.cache.TokenCache;
import app.security.starter.vo.TokenUserVO;
import app.security.starter.vo.TokenUserVOAuthenticationToken;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import java.util.Optional;

import static app.security.starter.SecurityConfiguration.*;
import static org.springframework.security.config.Customizer.withDefaults;

/**
 * 只验证 token 有效性，不加载完整用户信息
 *
 * @author 谢长春 2022-06-19
 */
@Configuration(proxyBeanMethods = false)
@EnableMethodSecurity(prePostEnabled = false) // 关闭方法头上的注解较验
@Slf4j
//@AutoConfigureAfter(SecurityUserValidConfiguration.class)
@ConditionalOnMissingBean({SecurityUserValidConfiguration.class})
public class SecurityTokenValidConfiguration {

    /**
     * 自定义 token 认证逻辑。
     */
    static OncePerRequestFilter TOKEN_ONCE_PER_REQUEST_FILTER = new OncePerRequestFilter() {
        @SneakyThrows
        @Override
        protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) {
            final String ipAddress = getIpAddress(request)
                    .map(ip -> {
                        MDC.put(HEADER_X_REAL_IP, ip);
                        return ip;
                    })
                    .orElse(null);
            final String token = Optional.ofNullable(request.getHeader(HEADER_X_TOKEN))
                    .orElseGet(() -> request.getParameter(HEADER_X_TOKEN));
            if (StringUtils.isBlank(token)) {
                chain.doFilter(request, response);
                return;
            }
            if (!tokenRegexp(token)) {
                throw new IllegalArgumentException(String.format("token格式错误: %s", token));
            }
            // token 验证通过就解析 token 中携带的用户信息，填充 TokenUserVO 对象；token 解析失败则抛出异常
            final TokenUserVO tokenUser = TokenUserVO.parse(token);
            tokenUser.setToken(token);
            tokenUser.setIpAddress(ipAddress);
            MDC.put(HEADER_X_TOKEN, token); // token 写入线程上下文对象
            MDC.put(HEADER_X_USER_ID, Objects.toString(tokenUser.getUserId()));
            SecurityContextHolder.getContext().setAuthentication(new TokenUserVOAuthenticationToken(tokenUser));
            chain.doFilter(request, response);
        }
    };

    @ConditionalOnMissingBean({TokenCache.class})
    @Bean
    public UserDetailsService userDetailsService() {
        return username -> {
            throw new UsernameNotFoundException("服务不支持鉴权");
        };
    }

    /**
     * 只验证 token 有效性，不加载完整用户信息
     */
    @ConditionalOnMissingBean({TokenCache.class})
    @Bean
    public SecurityFilterChain tokenValidSecurityFilterChain(HttpSecurity http) throws Exception {
        log.info("权限认证模式：只验证 token 有效性，不加载完整用户信息");
        http
                .cors(withDefaults()) // 跨域配置
                .requestCache().disable()
                //.sessionManagement().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                // csrf 令牌 <<<<<<
                .csrf(csrf -> {
                    csrf.disable();
//                        if (appConfig.isDev()) {
//                            csrf.disable();
//                        } else {
//                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
//                        }
                })
                .headers(headers -> headers
                        .addHeaderWriter((req, res) -> res.addHeader(HEADER_X_TRACE_ID, MDC.get(MDC_TRACE_ID)))
                        .xssProtection()
                )
                .authorizeHttpRequests(authorize -> authorize
                        .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                        .anyRequest().authenticated()
                )
                .addFilterBefore(TOKEN_ONCE_PER_REQUEST_FILTER, BasicAuthenticationFilter.class) // 自定义 token 认证逻辑。
                .addFilterBefore(logUriFilter, ChannelProcessingFilter.class)
        ;
        return http.build();
    }

}
