package app.security.starter.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serializable;

/**
 * 实体：登录参数
 *
 * @author 谢长春
 */
@Getter
@Setter
@ConfigurationProperties("spring.app")
public class ConfigProperties implements Serializable {
    @Value("${spring.app.swagger.enabled:-false}")
    private boolean swaggerEnabled;
}
