package app.security.starter.vo;

import app.enums.starter.TokenClient;
import app.security.starter.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * 自定义 token 认证对象：只验证 token ， 从 token 解析用户信息，不查用户完整信息，用于微服务认证
 *
 * @author 谢长春 2019-12-16
 */
@NoArgsConstructor
@Getter
@Setter
@Slf4j
@ToString
@Accessors(chain = true)
public class TokenUserVO implements IUser, UserDetails, Serializable {
    private static final long serialVersionUID = 4742500780168126655L;
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    private static final SecretKeySpec SK;
    /**
     * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    private static final IvParameterSpec IV;

    static {
        try {
            final String md5 = DigestUtils.md5Hex(TokenUserVO.class.getSimpleName() + Objects.toString(serialVersionUID));
            SK = new SecretKeySpec(md5.getBytes(), "AES");
            IV = new IvParameterSpec(StringUtils.right(md5, 16).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * aes 解码并转换为 {@link TokenUserVO} 实体，执行 token 认证，认证失败会抛出异常，认证成功则更新 token 过期时间
     *
     * @param token {@link String} HTTP 请求头中获取到的 token
     * @return {@link TokenUserVO}
     */
    @SneakyThrows
    public static TokenUserVO parse(final String token) {
        if (log.isInfoEnabled()) {
            log.info("x-token: {}", token);
        }
        if (StringUtils.isBlank(token)) {
            throw new NullPointerException("token 不能为空");
        }
        try {
            // 初始化解密参数，设置为解密模式，指定密钥，设置IV
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, SK, IV);
//        final String payload = new String(cipher.doFinal(org.apache.commons.codec.binary.Hex.decodeHex(token)));
            final String payload = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(token)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", token, payload);
            }
            // 解析 payload 内容
            final String[] payloads = payload.split("@");
            return new TokenUserVO()
                    .setClient(TokenClient.values()[Integer.parseInt(payloads[0])])
                    .setTimestamp(Long.parseLong(payloads[1]))
                    .setUserId(Long.parseLong(payloads[2]))
                    .setUsername(payloads[3])
                    .setPhone(payloads[4])
                    ;
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("token 解析失败: %s", token), e);
        }
    }

    /**
     * token中包含的内容
     */
    @JsonIgnore
    public String formatPayload() {
        return String.format(
                "%d@%d@%d@%s@%s"
                , client.ordinal()
                , timestamp
                , userId
                , Optional.ofNullable(username).orElse("")
                , Optional.ofNullable(phone).orElse("")
        );
    }

    /**
     * 加密生成 token
     *
     * @return {@link String}
     */
    @SneakyThrows
    public String encrypt() {
        final String payload = formatPayload();
        // 初始化加密参数，设置为加密模式，指定密钥，设置IV
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, SK, IV);
//        final String token = org.apache.commons.codec.binary.Hex.encodeHexString(cipher.doFinal(payload.getBytes()));
//        final String token = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(payload.getBytes()));
        final String token = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(cipher.doFinal(payload.getBytes()));
        if (log.isDebugEnabled()) {
            log.debug("{} => {}", payload, token);
        }
        return token;
    }

    /**
     * 请求头携带的 x-token
     */
    private String token;
    /**
     * 用户真实IP地址
     */
    private String ipAddress;
    /**
     * 登录设备
     */
    private TokenClient client;
    /**
     * 用户 ID
     */
    private long userId;
    /**
     * 用户登录名
     */
    private String username;
    /**
     * 用户手机号
     */
    private String phone;
    /**
     * 授权时间
     */
    private Long timestamp;
    /**
     * 登录过期时间
     */
    private Long expired;

    @Override
    public TokenClient getTokenClient() {
        return client;
    }

    @Override
    public Long getId() {
        return userId;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // token 解析到 userId 表示用户已经登录；赋予 ROLE_USER(普通用户) 权限，否则赋予 ROLE_ANONYMOUS(游客) 权限
        if (Objects.nonNull(userId)) {
            // token 用户只做 token 较验，然后授权普通用户；微服务在网关鉴权，服务之间相互调用走用户服务鉴权影响性能
            return AuthorityUtils.createAuthorityList(Role.ROLE_USER.name());
        } else {
            // 开放接口强制使用游客权限，避免开放接口注入权限，导致越权访问
            return AuthorityUtils.createAuthorityList(Role.ROLE_ANONYMOUS.name());
        }
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public String getPassword() {
        throw new IllegalArgumentException("Token 认证用户没有密码");
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isEnabled() {
        return true;
    }

}
