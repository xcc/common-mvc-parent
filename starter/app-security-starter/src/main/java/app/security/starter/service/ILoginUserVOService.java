package app.security.starter.service;

import app.security.starter.vo.LoginUserVO;

import java.util.Optional;

/**
 * 用 userId 换用户信息
 *
 * @author 谢长春 2022-06-17
 */
public interface ILoginUserVOService {
//        /**
//         * 登录用户名、手机号、邮箱查找登录用户
//         *
//         * @param username String
//         * @return Optional<LoginUserVO>
//         */
//        Optional<LoginUserVO> findLoginUserVO(final String username);

    /**
     * 用 userId 换用户信息
     *
     * @param userId Long
     * @return Optional<LoginUserVO>
     */
    Optional<LoginUserVO> getLoginUserVO(final Long userId);
}
