package app.security.starter;

import app.common.starter.enums.Code;
import app.common.starter.exception.CodeException;
import app.security.starter.cache.TokenCache;
import app.security.starter.vo.LoginUserVO;
import app.security.starter.vo.LoginUserVOAuthenticationToken;
import app.security.starter.vo.TokenUserVO;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static app.security.starter.SecurityConfiguration.*;
import static org.springframework.security.config.Customizer.withDefaults;

/**
 * 验证 token 有效性，且加载完整用户信息并验证接口权限
 *
 * @author 谢长春 2022-06-19
 */
@Configuration(proxyBeanMethods = false)
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true)
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
// 使用 @EnableGlobalMethodSecurity 注解来启用基于注解的安全性
// @EnableGlobalMethodSecurity(securedEnabled = true) // 启用注解：@Secured；[@Secured("ROLE_USER"), @Secured("IS_AUTHENTICATED_ANONYMOUSLY")]
// @EnableGlobalMethodSecurity(prePostEnabled = true) // 启用注解：@PreAuthorize；[@PreAuthorize("hasAuthority('ROLE_USER')"), @PreAuthorize("isAnonymous()")]
@Slf4j
@RequiredArgsConstructor
@ConditionalOnExpression("'${spring.app.cache.token.expired:0}' != '0'")
public class SecurityUserValidConfiguration {
    /**
     * 验证 token 有效性，且加载完整用户信息并验证接口权限
     */
    @ConditionalOnBean({TokenCache.class}) // 检查必须的依赖
    @Bean
    public SecurityFilterChain userValidSecurityFilterChain(HttpSecurity http) throws Exception {
        log.info("权限认证模式：验证 token 有效性，且加载完整用户信息并验证接口权限");
        http
//                .requestMatchers(configurer -> configurer.antMatchers("/**"))
                .cors(withDefaults()) // 跨域配置
                .requestCache().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                // csrf 令牌 <<<<<<
                .csrf(csrf -> { // https://docs.spring.io/spring-security/reference/servlet/exploits/csrf.html
                    csrf.disable();
//                        if (appConfig.isDev()) {
//                            csrf.disable();
//                        } else {
//                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
//                        }
                })
                // 响应头
                .headers(headers -> headers // https://docs.spring.io/spring-security/reference/servlet/exploits/headers.html
                        // http 响应头链路追踪 id
                        .addHeaderWriter((req, res) -> res.addHeader(HEADER_X_TRACE_ID, MDC.get(MDC_TRACE_ID)))
                        .xssProtection()
                        // .and().cacheControl() // 启用缓存
                        .and().httpStrictTransportSecurity(hsts -> hsts
                                .includeSubDomains(true)
                                .preload(true)
                                .maxAgeInSeconds(TimeUnit.DAYS.toSeconds(30))
                        )
                )
                // 异常
                //.exceptionHandling(configurer -> configurer
                //        .authenticationEntryPoint((request, response, e) -> {
                //            // 未登录，返回错误码 401
                //            if (log.isErrorEnabled()) {
                //                log.error("authenticationEntryPoint： {}", e.getMessage(), e);
                //            }
                //            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "未登录");
                //        })
                //        .accessDeniedHandler((request, response, e) -> {
                //            // 已登录，无权限，返回错误码 403
                //            if (log.isErrorEnabled()) {
                //                log.error("accessDeniedHandler： {}", e.getMessage(), e);
                //            }
                //            throw Code.A00006.toCodeException("无权限");
                //        })
                //)
                // 请求拦截规则
                .authorizeHttpRequests(authorize -> authorize
                                .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                                .anyRequest().authenticated() // 所有方法都需要登录认证
                        // *************************************************************************************
                        // 建议在类头部或方法头上加权限注解，这里配置太多的权限容易混淆
                        // *************************************************************************************
                        // .antMatchers("/").permitAll()
                        // .antMatchers("/error").permitAll() // HomeController 中需要异常处理方法
                        // *************************************************************************************
                )
                .httpBasic(withDefaults()) // https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/basic.html
                // 登录认证回调
                //.formLogin(configurer -> configurer // https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/form.html
                //        .successHandler((request, response, authentication) -> {
                //            if (log.isDebugEnabled()) {
                //                log.debug("successHandler: {}", JSON.toJsonString(authentication));
                //            }
                //            response.setStatus(HttpServletResponse.SC_OK);
                //        })
                //        .failureHandler((request, response, e) -> {
                //            if (log.isErrorEnabled()) {
                //                log.error("failureHandler: {}", e.getMessage(), e);
                //            }
                //            throw Code.A00002.toCodeException("登录失败");
                //        })
                //)
                // 登出动作
                .logout(configurer -> configurer // https://docs.spring.io/spring-security/reference/servlet/authentication/logout.html
                        .invalidateHttpSession(true) // 指定是否在注销时让HttpSession无效, 默认设置为 true
                        .deleteCookies("JSESSIONID") // 删除 JSESSIONID
                        .addLogoutHandler(new SecurityContextLogoutHandler()) // 添加一个LogoutHandler, 清除 session
                        // 向客户端发送 清除 “cookie、storage、缓存” 消息
                        //.addLogoutHandler(new HeaderWriterLogoutHandler(new ClearSiteDataHeaderWriter(ClearSiteDataHeaderWriter.Directive.ALL)))
                        .addLogoutHandler((request, response, authentication) -> { // 添加一个LogoutHandler, 退出操作需要删除 redis token 缓存
                            final String token = Optional.ofNullable((request).getHeader(HEADER_X_TOKEN))
                                    .orElseGet(() -> request.getParameter(HEADER_X_TOKEN));
                            if (StringUtils.isNotBlank(token) && tokenRegexp(token)) {
                                try {
                                    TokenCache.instance().destroy(TokenUserVO.parse(token));
                                } catch (Exception e) {
                                    log.error(e.getMessage(), e);
                                    // 有可能 token 已经过期了， 所以需要捕获异常，不向前端抛出
                                }
                            }
                        })
                        .logoutSuccessHandler((request, response, authentication) -> response.setStatus(HttpServletResponse.SC_OK))
                )
                // token 模式登录认证
                .addFilterBefore(
                        new OncePerRequestFilter() {
                            @SneakyThrows
                            @Override
                            protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) {
                                final String ipAddress = getIpAddress(request)
                                        .map(ip -> {
                                            MDC.put(HEADER_X_REAL_IP, ip);
                                            return ip;
                                        })
                                        .orElse(null);
                                final String token = Optional.ofNullable(request.getHeader(HEADER_X_TOKEN))
                                        .orElseGet(() -> request.getParameter(HEADER_X_TOKEN));
                                if (StringUtils.isNotBlank(token)) {
                                    try {
                                        if (!tokenRegexp(token)) {
                                            throw new IllegalArgumentException("token格式错误");
                                        }
                                        final TokenCache tokenCacheInstance = TokenCache.instance();
                                        final TokenUserVO tokenUser = tokenCacheInstance.asserts(TokenUserVO.parse(token));
                                        final LoginUserVO user = tokenCacheInstance.getLoginUserVOByUserId(tokenUser.getUserId());
                                        user.setClient(tokenUser.getClient());
                                        user.setIpAddress(ipAddress);
                                        Code.A00001.assertHasTrue(user.isAllow()
                                                , "用户状态异常；isEnabled： %s ，isCredentialsNonExpired： %s ，isAccountNonLocked： %s ，isAccountNonExpired： %s"
                                                , user.isEnabled()
                                                , user.isCredentialsNonExpired()
                                                , user.isAccountNonLocked()
                                                , user.isAccountNonExpired()
                                        );
                                        MDC.put(HEADER_X_TOKEN, token); // token 写入线程上下文对象
                                        MDC.put(HEADER_X_USER_ID, Objects.toString(tokenUser.getUserId()));
                                        SecurityContextHolder.getContext().setAuthentication(new LoginUserVOAuthenticationToken(user));
                                    } catch (Exception e) {
                                        if (e instanceof CodeException) {
                                            log.warn("{}:{}", token, e.getMessage());
                                        } else {
                                            log.error("{}", token, e);
                                        }
                                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "会话超时");
                                        return;
                                    }
                                }
                                filterChain.doFilter(request, response);
                            }
                        }
                        , BasicAuthenticationFilter.class
                )
                .addFilterBefore(logUriFilter, ChannelProcessingFilter.class)
        ;
        // http.authenticationProvider(new AuthenticationProvider()); // 自定义登录验证适配器验证
        return http.build();
    }


}
