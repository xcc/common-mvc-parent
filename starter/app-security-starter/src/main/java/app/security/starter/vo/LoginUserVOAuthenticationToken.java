package app.security.starter.vo;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * 自定义 token 认证对象
 *
 * @author 谢长春 2022-06-18
 */
public class LoginUserVOAuthenticationToken extends AbstractAuthenticationToken {

    private final LoginUserVO user;

    public LoginUserVOAuthenticationToken(final LoginUserVO user) {
        super(user.getAuthorities());
        super.setAuthenticated(true);
        this.user = user;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public String getName() {
        return user.getUsername();
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public Object getDetails() {
        return user;
    }
}
