package app.security.starter.interfaces;

import app.common.starter.interfaces.ICall;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import java.util.Optional;

/**
 * 用户信息抽象接口， 用于 Controller 层泛型获取用户信息
 *
 * @author 谢长春
 * @Deprecated 废弃：2022-11-06
 */
@Deprecated
public interface IUser {
    /**
     * 客户端指纹
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getFingerprint() {
        return null;
    }

    /**
     * 请求头 x-token
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getToken() {
        return null;
    }

    /**
     * 用户id
     *
     * @return {@link Long}
     */
    Long getId();

    void setId(final Long id);

    /**
     * 用户登录名
     *
     * @return {@link String}
     */
    String getUsername();

    void setUsername(final String username);

    /**
     * 用户昵称
     *
     * @return {@link String}
     */
    String getNickname();

    void setNickname(final String nickname);

    /**
     * 用户手机号
     *
     * @return {@link String}
     */
    String getPhone();

    void setPhone(final String phone);

    /**
     * 判断用户id是否有效，有效则执行 call 方法
     *
     * @param call {@link ICall}
     * @return boolean true: call 方法已执行
     */
    default boolean ifUserId(final ICall call) {
        if (Objects.nonNull(getId()) && getId() > 0) {
            call.call();
            return true;
        }
        return false;
    }

    /**
     * 判断用户id是否有效
     *
     * @return Optional<Long>
     */
    default Optional<Long> optionalUserId() {
        return Optional.ofNullable(getId()).filter(id -> id > 0);
    }
}
