package app.security.starter;

import app.common.starter.util.Rsa;
import app.security.starter.cache.TokenCache;
import app.security.starter.dto.ConfigProperties;
import app.security.starter.service.ILoginUserVOService;
import app.security.starter.vo.TokenUserVO;
import app.security.starter.vo.TokenUserVOAuthenticationToken;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * Spring Security 权限控制
 * <pre>
 *     中文文档：https://springcloud.cc/spring-security-zhcn.html
 *     官方文档：https://docs.spring.io/spring-security/reference/servlet/configuration/java.html
 *             https://docs.spring.ioio/spring-security/reference/getting-spring-security.html
 *             https://docs.spring.io/spring-security/reference/servlet/authentication/events.html
 *
 * @author 谢长春 2022-06-19
 */
@Configuration(proxyBeanMethods = false)
@EnableWebSecurity
@Slf4j
@EnableConfigurationProperties(ConfigProperties.class)
@ConfigurationProperties("spring.app.encrypt.pwd")
@ComponentScan("app.security.starter")
public class SecurityConfiguration {
//    /**
//     * 自定义校验规则
//     * https://docs.spring.io/spring-security/reference/servlet/authorization/method-security.html
//     */
//    @Configuration
//    public static class GlobalMethodSecurity extends GlobalMethodSecurityConfiguration {
//        @Override
//        protected MethodSecurityExpressionHandler createExpressionHandler() {
//            final DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
//            expressionHandler.setPermissionEvaluator(null);
//            return expressionHandler;
//        }
//    }

    public static final String HEADER_X_TOKEN = "x-token";
    public static final String HEADER_X_USER_ID = "x-user-id";
    public static final String HEADER_X_TRACE_ID = "X-Trace-Id";
    public static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
    public static final String HEADER_X_REAL_IP = "X-Real-Ip";
    public static final String MDC_TRACE_ID = "traceId";
    static final Pattern REG_TOKEN = Pattern.compile("^[\\w-]+$");
    static final Pattern REG_IP = Pattern.compile("[0-9.]+");

    static boolean tokenRegexp(final String token) {
        return REG_TOKEN.matcher(token).matches();
    }

    /**
     * 请求url打印
     */
    static final OncePerRequestFilter logUriFilter = new OncePerRequestFilter() {

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
            if (log.isInfoEnabled()) {
                log.info("{} {}", request.getMethod(), request.getRequestURI());
            }
            filterChain.doFilter(request, response);
        }
    };

    /**
     * 获取用户ip
     *
     * @param request HttpServletRequest
     * @return Optional<String>
     */
    public static Optional<String> getIpAddress(final HttpServletRequest request) {
        return Stream
                .of(request.getHeader(HEADER_X_FORWARDED_FOR), request.getHeader(HEADER_X_REAL_IP))
                .filter(StringUtils::isNoneBlank)
                .map(val -> val.split(",")[0])
                .filter(val -> REG_IP.matcher(val).matches())
                .findFirst()
                ;
    }

    /**
     * 跨域配置。
     * 注意 allowedHeaders 会影响跨域条件
     * org.springframework.web.cors.DefaultCorsProcessor#handleInternal(ServerHttpRequest, ServerHttpResponse, CorsConfiguration, boolean)
     *
     * @return CorsConfigurationSource
     */
    @ConditionalOnProperty(value = "spring.app.cors.enabled", havingValue = "true")
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        log.info("装载跨域配置");
        final List<String> allowedHeaders = Arrays.asList(
                "content-type"
                , HEADER_X_TOKEN
                , HEADER_X_TRACE_ID
                , "x-version"
        );
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOriginPatterns(Collections.singletonList("*"));
//        configuration.setAllowedOrigins(Arrays.asList("http://localhost:9528"));
        configuration.setAllowedMethods(Collections.singletonList("*"));
//        configuration.setAllowedMethods(Arrays.asList("GET","POST"));
        configuration.setAllowedHeaders(allowedHeaders);
        configuration.setExposedHeaders(allowedHeaders);
        configuration.setAllowCredentials(true);
        configuration.setMaxAge(TimeUnit.HOURS.toSeconds(1));
//        configuration.setMaxAge(10L);

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /**
     * 静态文件映射配置
     */
    @Configuration(proxyBeanMethods = false)
    public static class StaticAssetConfiguration
//            extends WebMvcConfigurationSupport
            implements WebMvcConfigurer {
        @Setter
        @Value("${spring.app.nfs.disk.directory:-''}")
        private String nfsDiskDirectory;

        @Override
        public void addResourceHandlers(final ResourceHandlerRegistry registry) {
            log.info("装载静态资源目录映射配置");
            // 添加静态资源过滤
            registry.addResourceHandler("favicon.ico")
                    .addResourceLocations("classpath:/static/")
                    .setCacheControl(CacheControl.maxAge(30, TimeUnit.DAYS).cachePublic());
            // 需要在 Spring Security 中配置忽略静态资源 WebSecurity.ignoring().antMatchers("/static/**");
            registry.addResourceHandler("/static/**")
                    // Locations 这里应该是编译后的静态文件目录
                    .addResourceLocations("classpath:/static/")
                    .setCacheControl(CacheControl.maxAge(1, TimeUnit.MINUTES).cachePublic());

            // knife4j 增强 swagger 配置 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            registry.addResourceHandler("doc.html")
                    .addResourceLocations("classpath:/META-INF/resources/")
                    .setCacheControl(CacheControl.maxAge(1, TimeUnit.DAYS).cachePublic());
            registry.addResourceHandler("/webjars/**")
                    .addResourceLocations("classpath:/META-INF/resources/webjars/")
                    .setCacheControl(CacheControl.maxAge(30, TimeUnit.DAYS).cachePublic());
            // knife4j 增强 swagger 配置 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            // 添加静态资源过滤
            // 需要在 com.ccx.demo.config.SecurityConfiguration#staticSecurityFilterChain 中配置静态资源路径不走权限校验
            if (StringUtils.isNoneBlank(nfsDiskDirectory)) {
                registry.addResourceHandler("/nfs/**")
                        // TODO 采坑记录结尾带 / 和不带 / 的区别
                        //   假设请求url为：{{domain}}/files/temp/a.txt
                        //   addResourceLocations 指定绝对路径
                        //   d:/files => d:/temp/a.txt
                        //   d:/files/ => d:/files/temp/a.txt
                        .addResourceLocations(String.format("file:%s/", nfsDiskDirectory))
                ;
            }
        }

        @Override
        public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {

        }
    }
//
//    @Bean
//    public WebSecurityCustomizer webSecurityCustomizer() {
//        return web -> {
//            web.ignoring().antMatchers( // resources 下的静态资源
//                    "/favicon.ico"
//                    , "/nfs/**"
//                    , "/static/**"
//                    , "/druid/**"
//            );
//            if (swaggerEnabled) {
//                // knife4j 增强 swagger 页面
//                web.ignoring().antMatchers(
//                        "/doc.html"
//                        , "/webjars/**"
//                        , "/swagger-resources/**"
//                        , "/v2/api-docs/**"
//                        , "/v3/api-docs/**"
//                );
//            }
//        };
//    }

    @Setter
    @Value("${spring.app.swagger.enabled:-false}")
    private boolean swaggerEnabled;

    /**
     * 静态资源，无需登录授权
     */
    @Order(1)
    @Bean
    public SecurityFilterChain staticSecurityFilterChain(HttpSecurity http) throws Exception {
        log.info("装载静态资源过滤器配置");
        http
                .cors(withDefaults()) // 跨域配置
                // csrf 令牌 <<<<<<
                .csrf(AbstractHttpConfigurer::disable)
                .requestCache().disable()
                .securityContext().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                )
                .requestMatchers(configurer -> {
                    configurer
                            .antMatchers(
                                    "/"
                                    , "/version"
                                    // 健康检查接口
                                    , "/actuator/health/**"
                                    // druid 监控
                                    , "/druid/**"
                                    // resources 下的静态资源
                                    , "/favicon.ico"
                                    , "/nfs/**"
                                    , "/static/**"
                            );
                    if (swaggerEnabled) {
                        // knife4j 增强 swagger 页面
                        configurer.antMatchers(
                                "/doc.html"
                                , "/webjars/**"
                                , "/swagger-resources/**"
                                , "/v2/api-docs/**"
                                , "/v3/api-docs/**"
                        );
                    }
                })
                .headers(headers -> headers
                        .addHeaderWriter((req, res) -> res.addHeader(HEADER_X_TRACE_ID, MDC.get(MDC_TRACE_ID)))
                        .xssProtection()
                )
                .authorizeHttpRequests(authorize -> authorize
                        .anyRequest().permitAll()
                )
//                .anonymous()
//                .and().servletApi()
//                .and().authorizeRequests()
//                .anyRequest().permitAll()
        ;
        return http.build();
    }

    /**
     * 开放接口，无需登录授权
     */
    @Order(2)
    @Bean
    public SecurityFilterChain tokenOpenSecurityFilterChain(HttpSecurity http) throws Exception {
        log.info("装载开放接口过滤器配置：/open/**, /feign/**");
        http
                .requestMatchers(configurer -> {
                    configurer
                            // open 前缀的 url 不需要登录
                            .antMatchers("/open/**", "/feign/**")
                    ;
                })
                .cors(withDefaults()) // 跨域配置
                .requestCache().disable()
                //.sessionManagement().disable()
                .sessionManagement(configurer -> configurer
                        // 指定会话策略；
                        // ALWAYS:总是创建HttpSession,
                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
                        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                )
                // csrf 令牌 <<<<<<
                .csrf(csrf -> {
                    csrf.disable();
//                        if (appConfig.isDev()) {
//                            csrf.disable();
//                        } else {
//                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
//                        }
                })
                .headers(headers -> headers
                        .addHeaderWriter((req, res) -> res.addHeader(HEADER_X_TRACE_ID, MDC.get(MDC_TRACE_ID)))
                        .xssProtection()
                )
                .authorizeHttpRequests(authorize -> authorize
                        .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                        .anyRequest().permitAll()
                )
                .addFilterBefore(
                        new AnonymousAuthenticationFilter(TokenUserVOAuthenticationToken.ANONYMOUS_KEY) {
                            @SneakyThrows
                            public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) {
                                final String ipAddress = getIpAddress((HttpServletRequest) request)
                                        .map(ip -> {
                                            MDC.put(HEADER_X_REAL_IP, ip);
                                            return ip;
                                        })
                                        .orElse(null);
                                String tokenHeader = Optional.ofNullable(((HttpServletRequest) request).getHeader(HEADER_X_TOKEN))
                                        .orElseGet(() -> request.getParameter(HEADER_X_TOKEN));
                                if (StringUtils.isNotBlank(tokenHeader)) {
                                    if (!tokenRegexp(tokenHeader)) {
                                        log.error("token格式错误:{}", tokenHeader);
                                        tokenHeader = null;
                                    }
                                }
                                final TokenUserVO anonymous = new TokenUserVO();
                                anonymous.setIpAddress(ipAddress);
                                SecurityContextHolder.getContext().setAuthentication(
                                        Optional.ofNullable(tokenHeader)
                                                // token 验证通过就解析 token 中携带的用户信息，填充 TokenUserVO 对象；token 解析失败则抛出异常
                                                .map(token -> {
                                                    try {
                                                        final TokenUserVO tokenUser = TokenUserVO.parse(token);
                                                        tokenUser.setToken(token);
                                                        tokenUser.setIpAddress(ipAddress);
                                                        MDC.put(HEADER_X_TOKEN, token); // token 写入线程上下文对象
                                                        MDC.put(HEADER_X_USER_ID, Objects.toString(tokenUser.getUserId()));
                                                        return new TokenUserVOAuthenticationToken(tokenUser);
                                                    } catch (Exception e) {
                                                        log.warn("无效 token 访问开放接口 ：{}", token);
                                                        // token 无效依然允许以匿名用户访问开放接口
                                                        return new TokenUserVOAuthenticationToken(anonymous);
                                                    }
                                                })
                                                // 没带 token 表示匿名用户
                                                .orElseGet(() -> new TokenUserVOAuthenticationToken(anonymous))
                                );
                                chain.doFilter(request, response);
                            }
                        }
                        , AnonymousAuthenticationFilter.class
                )
                .addFilterBefore(logUriFilter, ChannelProcessingFilter.class)
        ;
        return http.build();
    }

//    /**
//     * 只验证 token 有效性，不加载完整用户信息
//     */
//    @Order(3)
//    @Bean
//    public SecurityFilterChain tokenValidFeignSecurityFilterChain(HttpSecurity http) throws Exception {
//        log.info("装载内部服务暴露接口过滤器配置：/feign/**");
//        http
//                .requestMatchers(configurer -> {
//                    configurer
//                            // feign 前缀的 url 不需要登录
//                            .antMatchers("/feign/**")
//                    ;
//                })
//                .cors(withDefaults()) // 跨域配置
//                .requestCache().disable()
//                //.sessionManagement().disable()
//                .sessionManagement(configurer -> configurer
//                        // 指定会话策略；
//                        // ALWAYS:总是创建HttpSession,
//                        // IF_REQUIRED:只会在需要时创建一个HttpSession,
//                        // NEVER:不会创建HttpSession，但如果它已经存在，将可以使用HttpSession,
//                        // STATELESS:永远不会创建HttpSession，它不会使用HttpSession来获取SecurityContext
//                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                )
//                // csrf 令牌 <<<<<<
//                .csrf(csrf -> {
//                    csrf.disable();
////                        if (appConfig.isDev()) {
////                            csrf.disable();
////                        } else {
////                            csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and();
////                        }
//                })
//                .headers(headers -> headers
//                        .addHeaderWriter((req, res) -> res.addHeader(HEADER_X_TRACE_ID, MDC.get(MDC_TRACE_ID)))
//                        .xssProtection()
//                )
//                .authorizeHttpRequests(authorize -> authorize
//                        .antMatchers(HttpMethod.TRACE, "/**").denyAll()
//                        .anyRequest().authenticated()
//                )
//                .addFilterBefore(TOKEN_ONCE_PER_REQUEST_FILTER, BasicAuthenticationFilter.class) // 自定义 token 认证逻辑。
//                .addFilterBefore(logUriFilter, ChannelProcessingFilter.class)
//        ;
//        return http.build();
//    }


    /**
     * 加解密开关。 开启后登录密码需要使用 rsa 加密传输
     */
    @Setter
    @Getter
    private boolean enabled;
    /**
     * 加密类型，目前只支持 rsa
     */
    @Setter
    @Getter
    private String type;
    /**
     * 加密传输: 私钥解密
     */
    @Setter
    @Getter
    private String privateKey;
    /**
     * 加密传输: 公钥加密
     */
    @Setter
    @Getter
    private String publicKey;

    /**
     * 开启密码加密传输
     *
     * @return {@link PasswordEncoder}
     */
    // @ConditionalOnExpression("'true'.equals('${spring.app.encrypt.pwd.enabled}') && 'rsa'.equals('${spring.app.encrypt.pwd.type}')")
    @ConditionalOnBean({TokenCache.class, ILoginUserVOService.class}) // 检查必须的依赖
    @Bean
    public PasswordEncoder rsaBCryptPasswordEncoder() {
        log.info("PasswordEncoder：rsaBCryptPasswordEncoder");
        return new BCryptPasswordEncoder() {
            @Override
            public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
                if (enabled && (Objects.isNull(type) || Objects.equals("rsa", type))) {
                    // rsa 解密之后再验证密码
                    final String decryptPassword = Rsa.decryptByPrivateKey(rawPassword.toString(), privateKey);
                    return super.matches(decryptPassword, encodedPassword);
                }
                return super.matches(rawPassword, encodedPassword);
            }
        };
    }

//    @SneakyThrows
//    @Bean
//    public UserDetailsService userDetailsService(final PasswordEncoder passwordEncoder) {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User
//                .withUsername("user")
//                .roles("USER")
//                .password(passwordEncoder.encode("password"))
//                .build()
//        );
//        manager.createUser(User
//                .withUsername("admin")
//                .roles("ADMIN", "USER")
//                .password(passwordEncoder.encode("password"))
//                .build()
//        );
//        return manager;
//    }
//    @Bean
//    public AuthenticationManager myAuthenticationManager(final AuthenticationManagerBuilder builder) throws Exception {
//        builder.inMemoryAuthentication();
//        final DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
//        provider.setHideUserNotFoundExceptions(false);
//        provider.setUserDetailsService(this.userDetailsService);
//        provider.setPasswordEncoder(passwordEncoder());
//        builder.authenticationProvider(provider);
//        builder.userDetailsService(this.userDetailsService).passwordEncoder(passwordEncoder());
//        return builder.build();
//    }
}
//
//import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
//import org.springframework.security.web.csrf.CsrfToken;
//import org.springframework.security.web.csrf.CsrfTokenRepository;
//import org.springframework.security.web.csrf.DefaultCsrfToken;
//import org.springframework.util.Assert;
//import org.springframework.util.StringUtils;
//import org.springframework.web.util.WebUtils;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.UUID;
//
//public final class HeaderCsrfTokenRepository implements CsrfTokenRepository {
//    static final String DEFAULT_CSRF_COOKIE_NAME = "XSRF-TOKEN";
//    static final String DEFAULT_CSRF_PARAMETER_NAME = "_csrf";
//    static final String DEFAULT_CSRF_HEADER_NAME = "X-XSRF-TOKEN";
//    private String parameterName = DEFAULT_CSRF_PARAMETER_NAME;
//    private String headerName = DEFAULT_CSRF_HEADER_NAME;
//    private String cookieName = DEFAULT_CSRF_COOKIE_NAME;
//    private boolean cookieHttpOnly = true;
//    private String cookiePath;
//    private String cookieDomain;
//
//    public HeaderCsrfTokenRepository() {
//    }
//
//    public CsrfToken generateToken(HttpServletRequest request) {
//        return new DefaultCsrfToken(this.headerName, this.parameterName, this.createNewToken());
//    }
//
//    public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
//        String tokenValue = token == null ? "" : token.getToken();
//        Cookie cookie = new Cookie(this.cookieName, tokenValue);
//        cookie.setSecure(request.isSecure());
//        if (this.cookiePath != null && !this.cookiePath.isEmpty()) {
//            cookie.setPath(this.cookiePath);
//        } else {
//            cookie.setPath(this.getRequestContext(request));
//        }
//
//        if (token == null) {
//            cookie.setMaxAge(0);
//        } else {
//            cookie.setMaxAge(-1);
//        }
//
//        cookie.setHttpOnly(this.cookieHttpOnly);
//        if (this.cookieDomain != null && !this.cookieDomain.isEmpty()) {
//            cookie.setDomain(this.cookieDomain);
//        }
//
//        response.addCookie(cookie);
//    }
//
//    public CsrfToken loadToken(HttpServletRequest request) {
//        Cookie cookie = WebUtils.getCookie(request, this.cookieName);
//        if (cookie == null) {
//            return null;
//        } else {
//            String token = cookie.getValue();
//            return !StringUtils.hasLength(token) ? null : new DefaultCsrfToken(this.headerName, this.parameterName, token);
//        }
//    }
//
//    public void setParameterName(String parameterName) {
//        Assert.notNull(parameterName, "parameterName is not null");
//        this.parameterName = parameterName;
//    }
//
//    public void setHeaderName(String headerName) {
//        Assert.notNull(headerName, "headerName is not null");
//        this.headerName = headerName;
//    }
//
//    public void setCookieName(String cookieName) {
//        Assert.notNull(cookieName, "cookieName is not null");
//        this.cookieName = cookieName;
//    }
//
//    public void setCookieHttpOnly(boolean cookieHttpOnly) {
//        this.cookieHttpOnly = cookieHttpOnly;
//    }
//
//    private String getRequestContext(HttpServletRequest request) {
//        String contextPath = request.getContextPath();
//        return contextPath.length() > 0 ? contextPath : "/";
//    }
//
//    public static CookieCsrfTokenRepository withHttpOnlyFalse() {
//        CookieCsrfTokenRepository result = new CookieCsrfTokenRepository();
//        result.setCookieHttpOnly(false);
//        return result;
//    }
//
//    private String createNewToken() {
//        return UUID.randomUUID().toString();
//    }
//
//    public void setCookiePath(String path) {
//        this.cookiePath = path;
//    }
//
//    public String getCookiePath() {
//        return this.cookiePath;
//    }
//
//    public void setCookieDomain(String cookieDomain) {
//        this.cookieDomain = cookieDomain;
//    }
//}
//
