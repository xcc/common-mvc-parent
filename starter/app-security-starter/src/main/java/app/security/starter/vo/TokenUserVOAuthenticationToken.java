package app.security.starter.vo;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Objects;

/**
 * 自定义 token 认证对象：只验证 token ， 从 token 解析用户信息，不查用户完整信息，用于微服务认证
 *
 * @author 谢长春 2022-06-18
 */
public class TokenUserVOAuthenticationToken extends AbstractAuthenticationToken {
    public static final String ANONYMOUS_KEY;

    static {
        ANONYMOUS_KEY = DigestUtils.md5Hex(TokenUserVO.class.getName());
    }

    private final TokenUserVO user;

    public TokenUserVOAuthenticationToken(final TokenUserVO user) {
        super(user.getAuthorities());
        super.setAuthenticated(Objects.nonNull(user.getId()));
        this.user = user;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public String getName() {
        return user.getUsername();
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public Object getDetails() {
        return user;
    }
}
