package app.security.starter.cache;

import app.cache.starter.abs.AbsRedissonStringCache;
import app.common.starter.AppContext;
import app.common.starter.enums.Code;
import app.common.starter.util.Dates;
import app.common.starter.util.JSON;
import app.enums.starter.TokenClient;
import app.security.starter.service.ILoginUserVOService;
import app.security.starter.vo.LoginUserVO;
import app.security.starter.vo.TokenUserVO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBatch;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

/**
 * token 缓存
 *
 * @author 谢长春 2022-06-17
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnBean(ILoginUserVOService.class)
@ConditionalOnExpression("'${spring.app.cache.token.expired:0}' != '0'")
@ConfigurationProperties(prefix = "spring.app.cache.token")
public class TokenCache extends AbsRedissonStringCache<String> {

    public static final String CACHE_KEY = TokenCache.class.getSimpleName() + ":%s";
    private static volatile TokenCache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration expired;
    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;
    @Lazy
    @Autowired
    private ILoginUserVOService loginUserVOService;

    /**
     * 获取 {@link TokenCache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link TokenCache}
     */
    public static TokenCache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (TokenCache.class) {
                if (Objects.isNull(INSTANCE)) {
                    try {
                        INSTANCE = Objects.requireNonNull(AppContext.getBean(TokenCache.class));
                    } catch (Exception e) {
                        throw new RuntimeException("需要打开开关 spring.app.cache.token.enabled: true 并实现 ILoginUserVOService 接口，用于 token 认证获取全量用户信息", e);
                    }
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.token.expired: {}", expired);
        log.info("spring.app.cache.token.nullValueExpired: {}", nullValueExpired);
    }

    /**
     * 拼接 {@link TokenUserVO#getClient()}:{@link TokenUserVO#getUserId()} 作为主键
     *
     * @param token {@link TokenUserVO}
     * @return String OM_PC:1000
     */
    public String getUnionKey(final TokenUserVO token) {
        return String.format("%s:%d", token.getClient().name(), token.getUserId());
    }

    /**
     * 基于数据主键构造缓存key； 拼接前缀，一般用类名作为前缀:unionKey
     *
     * @param unionKey String tokenClient:userId
     * @return String this.getClass().getSimpleName() + ":" + unionKey
     */
    @Override
    protected Optional<String> formatCacheKey(final String unionKey) {
        return Optional.ofNullable(unionKey)
                .filter(StringUtils::isNoneBlank)
                .map(val -> String.format(CACHE_KEY, unionKey))
                ;
    }

    /**
     * 写缓存
     *
     * @param token {@link TokenUserVO}
     */
    public void set(final TokenUserVO token) {
        token.setTimestamp(Dates.now().toTimeMillis());
        token.setExpired(Dates.now().addSecond((int) expired.getSeconds()).toTimeMillis());
        set(getUnionKey(token), token);
    }

    /**
     * 读缓存
     *
     * @return {@link TokenUserVO}
     */
    public Optional<TokenUserVO> get(final TokenUserVO token) {
        return get(getUnionKey(token))
                .flatMap(jsonText -> JSON.parseObjectOptional(jsonText, TokenUserVO.class));
    }

    /**
     * token 延期
     *
     * @param client 登录客户端
     * @param userId 用户 ID
     */
    public void expireDelay(
            final TokenClient client,
            final Long userId
    ) {
        formatCacheKey(
                getUnionKey(new TokenUserVO()
                        .setClient(client)
                        .setUserId(userId)
                )
        ).ifPresent(cacheKey -> {
            getRedissonClient().<String>getBucket(cacheKey, StringCodec.INSTANCE).expire(expired);
        });
    }

    /**
     * 删除缓存
     *
     * @param token {@link TokenUserVO}
     */
    public void delete(final TokenUserVO token) {
        delete(getUnionKey(token));
    }

    /**
     * 清理缓存
     *
     * @param userId {@link Long}
     */
    public void clear(final Long userId) {
        if (Objects.isNull(userId)) {
            return;
        }
        final RBatch batch = redissonClient.createBatch();
        for (TokenClient client : TokenClient.values()) {
            formatCacheKey(
                    getUnionKey(new TokenUserVO()
                            .setClient(client)
                            .setUserId(userId)
                    )
            ).ifPresent(cacheKey -> {
                getRedissonClient().<String>getBucket(cacheKey, StringCodec.INSTANCE).deleteAsync();
            });
        }
        batch.execute();
    }

    /**
     * token 解析成功之后，用 userId 换用户信息
     *
     * @param userId Long
     * @return LoginUserVO
     */
    public LoginUserVO getLoginUserVOByUserId(final Long userId) {
        return loginUserVOService.getLoginUserVO(userId)
                .orElseThrow(() -> new NullPointerException("用户不存在"));
    }

    /**
     * token 解析之后同缓存中的对象比对 token 是否过期，过期抛出异常，未过期则更新过期时间，并返回新的缓存对象，需要将新的缓存对象存入缓存中
     *
     * @return {@link TokenUserVO} 新的缓存对象
     */
    public TokenUserVO asserts(final TokenUserVO token) {
        final TokenUserVO cacheToken = this.get(token).orElseThrow(() -> Code.A00002.toCodeException("token 过期， token 不存在"));
        Code.A00011.assertEquals(token.getTimestamp(), cacheToken.getTimestamp(), "token 过期， token 随机值不匹配");
        Code.A00002.assertHasTrue(cacheToken.getExpired() > Instant.now().toEpochMilli(), "token 过期");
        // 更新过期时间
        //tokenCache.set(this);
        return token;
    }

    /**
     * 销毁 token , 清除缓存
     */
    public void destroy(final TokenUserVO token) {
        this.delete(token);
    }
}
