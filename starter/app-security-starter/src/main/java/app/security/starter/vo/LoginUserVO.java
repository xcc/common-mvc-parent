package app.security.starter.vo;

import app.encrypt.domain.starter.annotations.AesIdLongJsonConverter;
import app.encrypt.domain.starter.annotations.AesIdStringArrayJsonConverter;
import app.enums.starter.TokenClient;
import app.security.starter.cache.TokenCache;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 自定义 token 认证用户对象：验证 token 并查询用户完整信息，检查用户状态，用于单体应用或需要用户完整信息的微服务
 *
 * @author 谢长春 2022-06-18
 */
@Getter
@Setter
@ToString(callSuper = true, exclude = {"password"})
@ApiModel(description = "登录用户对象")
public class LoginUserVO implements IUser, UserDetails {
    /**
     * 数据ID
     */
    @AesIdLongJsonConverter
    @Positive
    @ApiModelProperty(value = "数据ID", position = 1, dataType = "string")
    private Long id;
    /**
     * 登录名
     */
    @Pattern(regexp = "^[0-9a-z]{5,15}$", message = "登录用户名必须是5-15位小写字母或数字")
    @ApiModelProperty(value = "登录名", position = 3)
    private String username;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", position = 6)
    private String phone;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱", position = 7)
    private String email;
    /**
     * 登录密码
     */
    @JsonIgnore
    @ApiModelProperty(value = "登录密码", position = 4)
    private String password;
    /**
     * 用户昵称
     */
    @Size(max = 32)
    @ApiModelProperty(value = "昵称", position = 5)
    private String nickname;
    /**
     * 是否禁用账号，禁用账号无法登录。 [0:否,1:是]
     */
    @ApiModelProperty(value = "是否禁用账号，禁用账号无法登录。 [0:否,1:是]", position = 16)
    private Boolean disabled;
    /**
     * 是否逻辑删除。 [0:否,1:是]
     */
    @ApiModelProperty(value = "是否逻辑删除。 [0:否,1:是]", position = 16)
    private Boolean deleted;

    /**
     * 登录客户端
     */
    @ApiModelProperty(value = "登录客户端", example = "OM_PC")
    private TokenClient client;
    /**
     * 角色 ID 集合，tab_role.id，{@link String}[]
     */
    @AesIdStringArrayJsonConverter // id 使用 aes 加密
    @ApiModelProperty(value = "角色 ID 集合，tab_role.id，{@link String}[]", position = 9)
    private String[] roles;
    /**
     * 权限代码
     */
    @ApiModelProperty(value = "权限代码", example = "user_list_load")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<String> authorityList;

    /**
     * 角色名称集合  TabRole#getName()
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "角色名称集合", accessMode = READ_ONLY, position = 23)
    private List<String> roleNames;

    /**
     * 用户真实IP地址
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(hidden = true)
    private String ipAddress;

    @Override
    public TokenClient getTokenClient() {
        return client;
    }

    /**
     * 权限
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (CollectionUtils.isEmpty(authorityList)) {
            return Collections.emptyList();
        }
        return authorityList.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isAccountNonExpired() {
        return true;
//        return Objects.equals(false, user.getExpired());
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return !(getDisabled() || getDeleted());
    }

    /**
     * 检查是否允许登录
     *
     * @return boolean true：是， false：否
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public boolean isAllow() {
        return isEnabled() && isCredentialsNonExpired() && isAccountNonLocked() && isAccountNonExpired();
    }

    /**
     * 基于用户生成 token，同一端每个用户只支持一个 token ，每次调用该方法，前一次生成的 token 将失效
     *
     * @return {@link String} 生成的 token
     */
    public String token(final TokenClient client) {
        final TokenUserVO token = new TokenUserVO()
                .setClient(client)
                .setUserId(this.id)
                .setUsername(username)
                .setPhone(phone);
        TokenCache.instance().set(token);
        return token.encrypt();
    }
}
