package app.security.starter.vo;

import app.common.starter.interfaces.ICall;
import app.enums.starter.TokenClient;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import java.util.Optional;

/**
 * 用户信息抽象接口， 用于 Controller 层注解获取用户信息
 *
 * @author 谢长春 2022-11-06
 */
public interface IUser {
    /**
     * 客户端指纹
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getFingerprint() {
        return null;
    }

    /**
     * 用户真实IP地址
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getIpAddress() {
        return null;
    }

    /**
     * 请求头 x-token
     *
     * @return {@link String}
     */
    @ApiModelProperty(hidden = true)
    default String getToken() {
        return null;
    }

    /**
     * 用户登陆客户端
     *
     * @return {@link TokenClient}
     */
    TokenClient getTokenClient();

    /**
     * 用户id
     *
     * @return {@link Long}
     */
    Long getId();

    /**
     * 用户登录名
     *
     * @return {@link String}
     */
    String getUsername();

    /**
     * 用户手机号
     *
     * @return {@link String}
     */
    String getPhone();

    /**
     * 判断用户id是否有效，有效则执行 call 方法
     *
     * @param call {@link ICall}
     * @return boolean true: call 方法已执行
     */
    default boolean ifUserId(final ICall call) {
        if (Objects.nonNull(getId()) && getId() > 0) {
            call.call();
            return true;
        }
        return false;
    }

    /**
     * 判断用户id是否有效
     *
     * @return Optional<Long>
     */
    default Optional<Long> optionalUserId() {
        return Optional.ofNullable(getId()).filter(id -> id > 0);
    }


}
