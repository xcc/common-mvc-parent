package com;

import app.encrypt.domain.starter.DomainEncrypt;
import app.encrypt.domain.starter.DomainEncryptConfiguration;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 数据实体对象 id 和 数据版本加解密
 *
 * @author 谢长春 2022-09-17
 */
@Slf4j
public final class DomainEncryptTest {

    @SneakyThrows
    public static void main(String[] args) {
        final DomainEncryptConfiguration configuration = new DomainEncryptConfiguration();
        configuration.setEnabled(true);

        String env = "local";
//        env = "dev";
//        env = "sit";
//        env = "uat";
//        env = "prod";
        if (Objects.equals(env, "local") || Objects.equals(env, "dev")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/dev/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/dev/spring.app.encrypt.db.ivParameterSpec")));
            configuration.setSecretKey(secretKey);
            configuration.setIvParameterSpec(ivParameterSpec);
            configuration.postConstruct();

            System.out.println(env + ": \"" + DomainEncrypt.encryptId(100000000L) + "\"");
            System.out.println(env + ": \"" + DomainEncrypt.encryptId("abCD1") + "\"");
            System.out.println(env + ": \"" + DomainEncrypt.encryptDV(100000000L, "20220101223344555") + "\"");
            System.out.println(env + ": \"" + DomainEncrypt.encryptDV("abCD1", "20220101223344555") + "\"");
            System.out.println(env + ": \"" + DomainEncrypt.encryptIds(Arrays.asList(100000000L, 100000001L)).collect(Collectors.joining("\", \"")) + "\"");
            System.out.println(env + ": \"" + DomainEncrypt.encryptIds(Arrays.asList("abCD1", "abCD2")).collect(Collectors.joining("\", \"")) + "\"");
            System.out.println(env + ": \"" + String.join("\", \"", DomainEncrypt.encryptDV(100000000, "20220101223344555"), DomainEncrypt.encryptDV(100000001, "20220101223344555")) + "\"");

            System.out.println(env + ": " + DomainEncrypt.decryptLongId("U-_FbRJBfOssB3qUHUrn9g"));
            System.out.println(env + ": " + DomainEncrypt.decryptStringId("7vmo6dNcO6D5l3KgA8xnLQ"));
            System.out.println(env + ": " + DomainEncrypt.decryptDV("7LKN9p4H93lj0MzqvENo7a6tXXaXl4dm_Wl_omIOPmut-EuC7tkgObRK1qDrXe1z").orElse(null));
            System.out.println(env + ": " + DomainEncrypt.decryptDV("QU1uQlVpNFQzYUN6VVVuWvFhAqQOx68iQgeWbfzt-CNiH9M8ztYtJnJDnUJZZZgX").orElse(null));
            System.out.println(env + ": " + DomainEncrypt.decryptLongId(Arrays.asList("U-_FbRJBfOssB3qUHUrn9g", "-_u0e-oq1tRBc8XyHTMTeA")).map(Objects::toString).collect(Collectors.joining(", ")));
            System.out.println(env + ": " + DomainEncrypt.decryptStringId(Arrays.asList("7vmo6dNcO6D5l3KgA8xnLQ", "bkGbQVKNVaSFVsb7EWNWqA")).collect(Collectors.joining(", ")));
            System.out.println(env + ": " + DomainEncrypt.decryptDV(Arrays.asList(
                    "WkRnNFZ6Tm50NUZXVnU1MoeRu63rwFilgPNISVxxO5MS8Sb96PGTaeO2K8OLn3q2"
                    , "Z3hLbGUzRDc1RFBzUnhQSPsEXoX3wPWz8WZlpIZQtorg2nJb0AEVzEV9ty3FSff2"
            )).map(Objects::toString).collect(Collectors.joining(", ")));

        }
        if (Objects.equals(env, "sit")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.ivParameterSpec")));
            configuration.setSecretKey(secretKey);
            configuration.setIvParameterSpec(ivParameterSpec);
            configuration.postConstruct();
        }
        if (Objects.equals(env, "uat")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.ivParameterSpec")));
            configuration.setSecretKey(secretKey);
            configuration.setIvParameterSpec(ivParameterSpec);
            configuration.postConstruct();
        }
        if (Objects.equals(env, "prod")) {
            final String secretKey = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.secretKey")));
            final String ivParameterSpec = new String(Files.readAllBytes(Paths.get("main-module/src/main/resources/env/" + env + "/spring.app.encrypt.db.ivParameterSpec")));
            configuration.setSecretKey(secretKey);
            configuration.setIvParameterSpec(ivParameterSpec);
            configuration.postConstruct();
        }
    }
}
