package app.encrypt.domain.starter.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解：字符串 id json 序列化和反序列化加解密
 * <pre>
 * 使用方法：
 * {@code @AesIdStringJsonConverter private String id;}
 * </pre>
 *
 * @author 谢长春 2022-09-08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@JacksonAnnotationsInside
@AesIdJsonConverter
@JsonSerialize(converter = app.encrypt.domain.starter.convert.AesIdStringJsonConverter.Serializer.class)
@JsonDeserialize(converter = app.encrypt.domain.starter.convert.AesIdStringJsonConverter.Deserializer.class)
public @interface AesIdStringJsonConverter {
}
