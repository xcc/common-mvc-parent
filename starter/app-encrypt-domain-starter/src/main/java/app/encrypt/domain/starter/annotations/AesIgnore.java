package app.encrypt.domain.starter.annotations;

import java.lang.annotation.*;

/**
 * 接口方法跳过加解密
 *
 * @author 谢长春 2022-02-02
 * @deprecated 暂未实现
 */
@Deprecated
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AesIgnore {

    /**
     * @return boolean
     */
    boolean value() default true;

}
