package app.encrypt.domain.starter.convert;

import app.encrypt.domain.starter.DomainEncrypt;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *    @JsonSerialize(converter = AesIdStringJsonConvert.Serializer.class)
 *    @JsonDeserialize(converter = AesIdStringJsonConvert.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesIdStringJsonConverter {
    /**
     * id 序列化
     */
    public static class Serializer extends StdConverter<String, String> {

        @Override
        public String convert(String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", value);
            }
            return DomainEncrypt.encryptId(value);
        }
    }

    /**
     * id 反序列化
     */
    public static class Deserializer extends StdConverter<String, String> {

        @Override
        public String convert(String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", value);
            }
            return DomainEncrypt.decryptStringId(value);
        }
    }
}
