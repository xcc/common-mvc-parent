package app.encrypt.domain.starter.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;

import java.lang.annotation.*;

/**
 * 注解：用于所有加解密注解标志，jackson 序列化和反序列化可以统一配置忽略加解密
 * 参考文档： https://www.daimajiaoliu.com/daima/4ed17441d100404
 * <pre>
 * 配置方法：
 *   {@code
 *     new ObjectMapper()
 *         .setAnnotationIntrospector(new JacksonAnnotationIntrospector() {
 *             @Override
 *             public boolean isAnnotationBundle(Annotation ann) {
 *                 final Class<? extends Annotation> type = ann.annotationType();
 *                 if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
 *                     return false;
 *                 }
 *                 return super.isAnnotationBundle(ann);
 *             }
 *         });
 *   }
 * </pre>
 *
 * @author 谢长春 2022-09-08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE})
@JacksonAnnotationsInside
@Inherited
public @interface AesIdJsonConverter {
}
