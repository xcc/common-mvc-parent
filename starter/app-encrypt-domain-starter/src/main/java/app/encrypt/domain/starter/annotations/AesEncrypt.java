package app.encrypt.domain.starter.annotations;

import java.lang.annotation.*;

/**
 * 实体类 Json 序列化或反序列化时自动加解密
 *
 * @author 谢长春 2022-01-31
 * @deprecated 暂未实现
 */
@Deprecated
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AesEncrypt {

    /**
     * @return boolean
     */
    boolean value() default true;

}
