package app.encrypt.domain.starter.convert;

import app.encrypt.domain.starter.DomainEncrypt;
import app.encrypt.domain.starter.model.DataVersion;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *   @JsonSerialize(converter = AesDataVersionJsonConverter.Serializer.class)
 *   @JsonDeserialize(converter = AesDataVersionJsonConverter.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesDataVersionJsonConverter {
    /**
     * dv 序列化
     */
    public static class Serializer extends StdConverter<String, String> {

        @Override
        public String convert(String value) {
            if (Strings.isNullOrEmpty(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", value);
            }
            return DomainEncrypt
                    .encryptDV(value)
                    ;
        }
    }

    /**
     * dv 反序列化
     */
    public static class Deserializer extends StdConverter<String, DataVersion> {

        @Override
        public DataVersion convert(String value) {
            if (Strings.isNullOrEmpty(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", value);
            }
            return DomainEncrypt
                    .decryptDV(value)
                    .orElse(null)
                    ;
        }
    }
}
