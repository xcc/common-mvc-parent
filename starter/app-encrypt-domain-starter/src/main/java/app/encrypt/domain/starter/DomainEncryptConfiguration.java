package app.encrypt.domain.starter;

import app.cache.starter.util.JsonCache;
import app.common.starter.util.JSON;
import app.encrypt.domain.starter.annotations.AesIdJsonConverter;
import app.encrypt.domain.starter.resolver.*;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.PostConstruct;
import java.lang.annotation.Annotation;
import java.util.List;

/**
 * 数据实体对象 id 和 数据版本加解密
 * // com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure
 * // com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration
 * //@ConditionalOnClass(DruidDataSource.class)
 * //@AutoConfigureBefore(DataSourceAutoConfiguration.class)
 * //@EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
 * //@Import({DruidSpringAopConfiguration.class,
 * //        DruidStatViewServletConfiguration.class,
 * //        DruidWebStatFilterConfiguration.class,
 * //        DruidFilterConfiguration.class
 * //})
 *
 * @author 谢长春 2022-02-08
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@ComponentScan("app.encrypt.domain.starter")
@EnableConfigurationProperties
@ConfigurationProperties("spring.app.encrypt.db")
public class DomainEncryptConfiguration implements WebMvcConfigurer {
    @Autowired
    @Setter
    @Getter
    private ConfigurableBeanFactory beanFactory;
    /**
     * 加解密开关。 true：是，false：否。
     */
    @Setter
    @Getter
    private boolean enabled;
    /**
     * 全局 aes 密钥， 32位
     */
    @Setter
    @Getter
    private String secretKey;
    /**
     * 固定 IV ， 16位。 设置固定 IV 之后同样内容每次加密结果一样。
     */
    @Setter
    @Getter
    private String ivParameterSpec;


    private static final int MAX = 70;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PostConstruct
    public void postConstruct() {
        final JacksonAnnotationIntrospector introspector = new JacksonAnnotationIntrospector() {
            @Override
            public boolean isAnnotationBundle(Annotation ann) {
                final Class<? extends Annotation> type = ann.annotationType();
                if (type.isAnnotationPresent(AesIdJsonConverter.class)) { // 忽略加解密注解
                    return false;
                }
                return super.isAnnotationBundle(ann);
            }
        };
        JSON.addAnnotationIntrospector(introspector); // 忽略加解密注解
        JsonCache.addAnnotationIntrospector(introspector); // 忽略加解密注解
        log.info(LOG_PATTERN
                , "数据实体对象 id 和 数据版本加解密"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.db.enabled: " + enabled, MAX, ' '), MAX) + " # 是否开启数据库敏感字段加密"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.db.secretKey: " + secretKey, MAX, ' '), MAX) + " # 密钥"
                        , StringUtils.left(StringUtils.rightPad("spring.app.encrypt.db.ivParameterSpec: " + ivParameterSpec, MAX, ' '), MAX) + " # 固定 IV ， 16位"
                )
                , "数据实体对象 id 和 数据版本加解密"
        );
        DomainEncrypt.setProperties(enabled, secretKey, ivParameterSpec); // 设置 aes 密钥
    }

    @Override
    public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> resolvers) {
        log.info("注册 Controller 参数解密转换器：@AesPathVariable 、 @AesRequestParam");
        resolvers.add(new AesLongIdPathVariableMethodArgumentResolver());
        resolvers.add(new AesStringIdPathVariableMethodArgumentResolver());
        resolvers.add(new AesDataVersionPathVariableMethodArgumentResolver());
        resolvers.add(new AesLongIdRequestParamMethodArgumentResolver());
        resolvers.add(new AesStringIdRequestParamMethodArgumentResolver());
//        resolvers.add(new AesLongIdRequestParamMethodArgumentResolver(beanFactory, false));
//        resolvers.add(new AesLongIdRequestParamMethodArgumentResolver(beanFactory, true));
    }
}
