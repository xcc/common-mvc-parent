package app.encrypt.domain.starter.model;

import app.common.starter.enums.Code;
import app.encrypt.domain.starter.convert.AesDataVersionJsonConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Objects;

/**
 * dv 携带 id 和 updateTime 加密，用于更新和删除时附加 updateTime 作为查询条件
 *
 * @author 谢长春 2022-09-08
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonDeserialize(converter = AesDataVersionJsonConverter.Deserializer.class)
public class DataVersion {
    /**
     * 数值型 id
     */
    @ApiModelProperty(hidden = true)
    private String id;
    /**
     * 数据最近一次更新时间
     */
    @ApiModelProperty(hidden = true)
    private String updateTime;

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public Long getLongId() {
        if (Objects.isNull(id) || Objects.equals("", id)) {
            return null;
        }
        return Long.parseLong(id);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getStringId() {
        if (Objects.isNull(id) || Objects.equals("", id)) {
            return null;
        }
        return id;
    }

    public static DataVersion parse(final String decryptText) {
        Code.A00003.assertNonBlank(decryptText, "解密后的内容不能为空");
        Code.A00003.assertHasTrue(decryptText.contains("@"), "解密后的数据格式错误:%s", decryptText);
        // split[0]:id，split[1]:updateTime
//            String id = decryptText.replaceAll("^(.*)@.*", "$1");
//            Optional.ofNullable(decryptText.replaceAll("^.*@(\\d+)$", "$1"))
//                    .filter("")
        return new DataVersion(
                decryptText.replaceAll("^(.*)@.*", "$1")
                , decryptText.replaceAll("^.*@(\\d+)?", "$1")
        );
    }

}
