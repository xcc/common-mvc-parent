package app.encrypt.domain.starter;

import app.common.starter.enums.Code;
import app.encrypt.domain.starter.model.DataVersion;
import com.google.common.base.Strings;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 数据实体对象 id 和 数据版本加解密
 *
 * @author 谢长春 2022-09-17
 */
@Slf4j
public final class DomainEncrypt {

    private static final Cipher CIPHER;
    /**
     * https://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#trans
     */
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 参考文档: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
     * <pre>
     * Cipher (Encryption) Algorithms
     * Cipher Algorithm Names
     * The following names can be specified as the algorithm component in a transformation when requesting an instance of Cipher.
     *
     * <table border="5" cellpadding="5" frame="border" width="90%" summary="Cipher Algorithm Names">
     * <thead> <tr> <th>Algorithm Name</th> <th>Description</th> </tr> </thead>
     * <tbody>
     * <tr> <td>AES</td> <td>Advanced Encryption Standard as specified by NIST in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS 197</a>. Also known as the Rijndael algorithm by Joan Daemen and Vincent Rijmen, AES is a 128-bit block cipher supporting keys of 128, 192, and 256 bits.</td> </tr>
     * <tr> <td>AESWrap</td> <td>The AES key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3394.txt">RFC 3394</a>.</td> </tr>
     * <tr> <td>ARCFOUR</td> <td>A stream cipher believed to be fully interoperable with the RC4 cipher developed by Ron Rivest. For more information, see K. Kaukonen and R. Thayer, "A Stream Cipher Encryption Algorithm 'Arcfour'", Internet Draft (expired), <a href="http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt"> draft-kaukonen-cipher-arcfour-03.txt</a>.</td> </tr>
     * <tr> <td>Blowfish</td> <td>The <a href="http://www.schneier.com/blowfish.html">Blowfish block cipher</a> designed by Bruce Schneier.</td> </tr>
     * <tr> <td>CCM</td> <td>Counter/CBC Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38C/SP800-38C_updated-July20_2007.pdf">NIST Special Publication SP 800-38C</a>.</td> </tr>
     * <tr> <td>DES</td> <td>The Digital Encryption Standard as described in <a href="http://csrc.nist.gov/publications/fips/index.html">FIPS PUB 46-3</a>.</td> </tr>
     * <tr> <td>DESede</td> <td>Triple DES Encryption (also known as DES-EDE, 3DES, or Triple-DES). Data is encrypted using the DES algorithm three separate times. It is first encrypted using the first subkey, then decrypted with the second subkey, and encrypted with the third subkey.</td> </tr>
     * <tr> <td>DESedeWrap</td> <td>The DESede key wrapping algorithm as described in <a href="http://www.ietf.org/rfc/rfc3217.txt">RFC 3217</a> .</td> </tr>
     * <tr> <td>ECIES</td> <td>Elliptic Curve Integrated Encryption Scheme</td> </tr>
     * <tr> <td>GCM</td> <td>Galois/Counter Mode, as defined in <a href="http://csrc.nist.gov/publications/nistpubs/800-38D/SP-800-38D.pdf">NIST Special Publication SP 800-38D</a>.</td> </tr>
     * <tr> <td>PBEWith&lt;digest&gt;And&lt;encryption&gt; PBEWith&lt;prf&gt;And&lt;encryption&gt;</td> <td>The password-based encryption algorithm found in (PKCS5), using the specified message digest (&lt;digest&gt;) or pseudo-random function (&lt;prf&gt;) and encryption algorithm (&lt;encryption&gt;). Examples: <ul> <li><b>PBEWithMD5AndDES</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Encryption Standard," version 1.5, Nov 1993</a>. Note that this algorithm implies <a href="#cbcMode"><i>CBC</i></a> as the cipher mode and <a href="#pkcs5Pad"><i>PKCS5Padding</i></a> as the padding scheme and cannot be used with any other cipher modes or padding schemes.</li> <li><b>PBEWithHmacSHA256AndAES_128</b>: The password-based encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2127">RSA Laboratories, "PKCS #5: Password-Based Cryptography Standard," version 2.0, March 1999</a>.</li> </ul> </td> </tr>
     * <tr> <td>RC2</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RC4</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc. (See note prior for ARCFOUR.)</td> </tr>
     * <tr> <td>RC5</td> <td>Variable-key-size encryption algorithms developed by Ron Rivest for RSA Data Security, Inc.</td> </tr>
     * <tr> <td>RSA</td> <td>The RSA encryption algorithm as defined in <a href="http://www.rsa.com/rsalabs/node.asp?id=2125">PKCS #1</a></td> </tr>
     * </tbody>
     * </table>
     */
    private static final String CIPHER_ALGORITHM = "AES";
    private static final int ZERO = 0;
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    private static SecretKeySpec SECRET_KEY;
    /**
     * IV 长度
     */
    private static final int IV_LENGTH = 16;
    /**
     * 请求响应报文加解密：固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    private static IvParameterSpec IV;
    /**
     * 加解密开关。 true：是，false：否。
     */
    private static boolean ENABLED;

    static {
        try {
            CIPHER = Cipher.getInstance(TRANSFORMATION);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final int MAX = 70;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    private DomainEncrypt() {
        throw new NullPointerException("DomainEncryptProperties NPE");
    }

    public static void setProperties(
            final boolean enabled,
            final String secretKey,
            final String ivParameterSpec
    ) {
        ENABLED = enabled;
        if (!enabled) {
            return;
        }
        if (Strings.isNullOrEmpty(secretKey) || secretKey.length() != 32) {
            // 设置密钥 32 位: RandomStringUtils.randomAlphanumeric(32)
            throw new IllegalArgumentException("spring.app.encrypt.db.secretKey='请指定32位密钥'");
        }
        if (Strings.isNullOrEmpty(ivParameterSpec) || ivParameterSpec.length() != 16) {
            // 设置 IV ， 长度必须是 16 位 RandomStringUtils.randomAlphanumeric(16)
            throw new IllegalArgumentException("spring.app.encrypt.db.ivParameterSpec='请指定16位固定IV'");
        }
        SECRET_KEY = new SecretKeySpec(secretKey.getBytes(UTF_8), CIPHER_ALGORITHM);
        IV = new IvParameterSpec(ivParameterSpec.getBytes(UTF_8));
    }

    private static final Function<byte[], String> hexEncode = Hex::encodeHexString;
    private static final Function<String, byte[]> hexDecode = (String hexString) -> {
        try {
            return Hex.decodeHex(hexString);
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    };
    private static final Function<byte[], String> base64Encode = Base64::encodeBase64URLSafeString;
    private static final Function<String, byte[]> base64Decode = Base64::decodeBase64;

    /**
     * aes 加密
     *
     * @param id 明文
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encryptId(final Object id) {
        try {
            return Optional.ofNullable(id)
                    .map(Objects::toString)
                    .filter(StringUtils::isNoneBlank)
                    .map(val -> {
                        if (!ENABLED) {
                            return val;
                        }
                        try {
                            // 使用固定的 IV 加密
                            CIPHER.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV);
                            // final String result = hexEncode.apply(cipher.doFinal(data.getBytes(UTF_8)));
                            final String result = base64Encode.apply(CIPHER.doFinal(val.getBytes(UTF_8)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", val, result);
                            }
                            return result;
                        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException |
                                 InvalidAlgorithmParameterException e) {
                            throw new IllegalArgumentException(String.format("加密失败:%s", val), e);
                        }
                    })
                    .orElse(null);
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%s", Objects.toString(id));
        }
    }

    /**
     * aes 加密
     *
     * @param ids {@link Collection <String>} 明文
     * @return Stream<String> 密文
     */
    @SneakyThrows
    public static Stream<String> encryptIds(final Collection<?> ids) {
        try {
            if (CollectionUtils.isEmpty(ids)) {
                return Stream.empty();
            }
            if (!ENABLED) {
                return ids.stream().map(Objects::toString);
            }
            // 使用固定的 IV 加密
            CIPHER.init(Cipher.ENCRYPT_MODE, SECRET_KEY, IV);
            return ids.stream()
                    .map(Objects::toString)
                    .map(data -> {
                        try {
                            // final String result = hexEncode.apply(cipher.doFinal(data.getBytes(UTF_8)));
                            final String result = base64Encode.apply(CIPHER.doFinal(data.getBytes(UTF_8)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return result;
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("加密失败:%s", data), e);
                        }
                    });
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "加密失败:%s", ids.stream().map(Objects::toString).collect(Collectors.joining(",")));
        }
    }

    /**
     * 解密
     *
     * @param ids {@link Collection<String>} 密文
     * @return Stream<String> 明文
     */
    @SneakyThrows
    public static Stream<String> decryptStringId(final Collection<String> ids) {
        try {
            if (CollectionUtils.isEmpty(ids)) {
                return Stream.empty();
            }
            if (!ENABLED) {
                return ids.stream();
            }
            // 使用固定的 IV 解密
            CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV);
            return ids.stream()
                    .map(data -> {
                        try {
                            // final String result = new String(cipher.doFinal(hexDecode.apply(data)));
                            final String result = new String(CIPHER.doFinal(base64Decode.apply(data)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return result;
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                        }
                    });
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", String.join(",", ids));
        }
    }

    /**
     * 解密
     *
     * @param ids {@link Collection<String>} 密文
     * @return Stream<String> 明文
     */
    @SneakyThrows
    public static Stream<Long> decryptLongId(final Collection<String> ids) {
        try {
            if (CollectionUtils.isEmpty(ids)) {
                return Stream.empty();
            }
            if (!ENABLED) {
                return ids.stream().map(Long::parseLong);
            }
            // 使用固定的 IV 解密
            CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV);
            return ids.stream()
                    .map(data -> {
                        try {
                            // final String result = new String(cipher.doFinal(hexDecode.apply(data)));
                            final String result = new String(CIPHER.doFinal(base64Decode.apply(data)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return Long.parseLong(result);
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                        }
                    });
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", String.join(",", ids));
        }
    }

    /**
     * 解密
     *
     * @param id {@link String} 密文
     * @return String 明文
     */
    @SneakyThrows
    public static String decryptStringId(final String id) {
        try {
            // 使用固定的 IV 解密
            CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, IV);
            return Optional.ofNullable(id)
                    .filter(StringUtils::isNoneBlank)
                    .map(val -> {
                        if (!ENABLED) {
                            return val;
                        }
                        try {
                            // final String result = new String(cipher.doFinal(hexDecode.apply(data)));
                            final String result = new String(CIPHER.doFinal(base64Decode.apply(val)));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", val, result);
                            }
                            return result;
                        } catch (IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("解密失败:%s", val), e);
                        }
                    })
                    .orElse(null);
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", id);
        }
    }

    /**
     * 解密
     *
     * @param id {@link String} 密文
     * @return Long 明文
     */
    @SneakyThrows
    public static Long decryptLongId(final String id) {
        return Optional.ofNullable(decryptStringId(id)).map(Long::parseLong).orElse(null);
    }

//    /**
//     * 加密数据版本号
//     *
//     * @param dvList {@link Collection <String>} 明文
//     * @return {@link String} 密文
//     */
//    @SneakyThrows
//    public static Stream<String> encryptDV(final Collection<String> dvList) {
//        try {
//            if (CollectionUtils.isEmpty(dvList)) {
//                return Stream.empty();
//            }
//            if (!ENABLED) {
//                return dvList.stream();
//            }
//            return dvList.stream()
//                    .map(data -> {
//                        try {
//                            // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
//                            final byte[] ivBytes = RandomStringUtils.randomAlphanumeric(IV_LENGTH).getBytes();
//                            CIPHER.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));
//
//                            final byte[] dataBytes = CIPHER.doFinal(data.getBytes(UTF_8));
//                            final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
//                            System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
//                            System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
//                            // final String result = hexEncode.apply(bytes);
//                            final String result = base64Encode.apply(bytes);
//                            if (log.isDebugEnabled()) {
//                                log.debug("{} => {}", data, result);
//                            }
//                            return result;
//                        } catch (InvalidKeyException | InvalidAlgorithmParameterException |
//                                 IllegalBlockSizeException | BadPaddingException e) {
//                            throw new IllegalArgumentException(String.format("加密失败:%s", data), e);
//                        }
//                    });
//        } catch (Exception e) {
//            throw Code.A00003.toCodeException(e, "加密失败:%s", String.join(",", dvList));
//        }
//    }

    /**
     * 加密数据版本号
     *
     * @param dv {@link String} String.format("%s@%s", id, Optional.ofNullable(updateTime).orElse(""))
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encryptDV(final String dv) {
        return Optional.ofNullable(dv)
                .map(val -> {
                    if (!ENABLED) {
                        return dv;
                    }
                    try {
                        // CBC模式需要生成一个 16 bytes 的 IV（initialization vector）。  IV不需要保密，把IV和密文一起返回，返回随机IV的好处是每次加密都能获得不同的密文
                        final byte[] ivBytes = RandomStringUtils.randomAlphanumeric(IV_LENGTH).getBytes();
                        CIPHER.init(Cipher.ENCRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));

                        final byte[] dataBytes = CIPHER.doFinal(dv.getBytes(UTF_8));
                        final byte[] bytes = new byte[IV_LENGTH + dataBytes.length];
                        System.arraycopy(ivBytes, ZERO, bytes, ZERO, IV_LENGTH);
                        System.arraycopy(dataBytes, ZERO, bytes, IV_LENGTH, dataBytes.length);
                        // final String result = hexEncode.apply(bytes);
                        final String result = base64Encode.apply(bytes);
                        if (log.isDebugEnabled()) {
                            log.debug("{} => {}", dv, result);
                        }
                        return result;
                    } catch (InvalidKeyException | InvalidAlgorithmParameterException |
                             IllegalBlockSizeException | BadPaddingException e) {
                        throw new IllegalArgumentException(String.format("加密失败:%s", dv), e);
                    }
                })
                .orElse(null);
    }

    /**
     * 加密数据版本号
     *
     * @param id         {@link Objects} 数据id
     * @param updateTime {@link String} 数据更新时间
     * @return {@link String} 密文
     */
    @SneakyThrows
    public static String encryptDV(final Object id, final String updateTime) {
        return Optional.ofNullable(id)
                .map(val -> {
                    final String dv = String.format("%s@%s", id, Optional.ofNullable(updateTime).orElse(""));
                    if (!ENABLED) {
                        return dv;
                    }
                    return encryptDV(dv);
                })
                .orElse(null);
    }

    /**
     * 解密数据版本号
     *
     * @param dv {@link Collection<String>} 密文
     * @return 明文
     */
    @SneakyThrows
    public static Optional<DataVersion> decryptDV(final String dv) {
        return Optional.ofNullable(dv)
                .filter(StringUtils::isNoneBlank)
                .map(val -> {
                    try {
                        if (!ENABLED) {
                            return DataVersion.parse(val);
                        }
                        final byte[] ivBytes = new byte[IV_LENGTH];
                        // 将 data 分割成 IV 和密文
                        // final byte[] bytes = hexDecode.apply(data);
                        final byte[] bytes = base64Decode.apply(val);
                        System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
//                                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                        CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));

                        final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
                        System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);
                        final String result = new String(CIPHER.doFinal(dataBytes));
                        if (log.isDebugEnabled()) {
                            log.debug("{} => {}", val, result);
                        }
                        return DataVersion.parse(result);
                    } catch (InvalidKeyException | InvalidAlgorithmParameterException |
                             IllegalBlockSizeException | BadPaddingException e) {
                        throw Code.A00003.toCodeException(e, "解密失败:%s", String.join(",", dv));
                    }
                });
    }

    /**
     * 解密数据版本号
     *
     * @param dvList {@link Collection<String>} 密文
     * @return 明文
     */
    @SneakyThrows
    public static Stream<DataVersion> decryptDV(final Collection<String> dvList) {
        try {
            if (CollectionUtils.isEmpty(dvList)) {
                return Stream.empty();
            }
            if (!ENABLED) {
                return dvList.stream().map(DataVersion::parse);
            }
            final byte[] ivBytes = new byte[IV_LENGTH];
            return dvList.stream()
                    .map(data -> {
                        try {
                            // 将 data 分割成 IV 和密文
                            // final byte[] bytes = hexDecode.apply(data);
                            final byte[] bytes = base64Decode.apply(data);
                            System.arraycopy(bytes, ZERO, ivBytes, ZERO, IV_LENGTH);
//                                IV_RANDOM_RULE.assertIV(ivBytes); // 断言 IV 规则是否匹配
                            CIPHER.init(Cipher.DECRYPT_MODE, SECRET_KEY, new IvParameterSpec(ivBytes));

                            final byte[] dataBytes = new byte[bytes.length - IV_LENGTH];
                            System.arraycopy(bytes, IV_LENGTH, dataBytes, ZERO, dataBytes.length);
                            final String result = new String(CIPHER.doFinal(dataBytes));
                            if (log.isDebugEnabled()) {
                                log.debug("{} => {}", data, result);
                            }
                            return DataVersion.parse(result);
                        } catch (InvalidKeyException | InvalidAlgorithmParameterException |
                                 IllegalBlockSizeException | BadPaddingException e) {
                            throw new IllegalArgumentException(String.format("解密失败:%s", data), e);
                        }
                    });
        } catch (Exception e) {
            throw Code.A00003.toCodeException(e, "解密失败:%s", String.join(",", dvList));
        }
    }
}
