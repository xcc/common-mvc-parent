package app.encrypt.domain.starter.convert;

import app.encrypt.domain.starter.DomainEncrypt;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * 用于 Json 序列化和反序列化加解密
 *
 * <pre>
 * 使用方法：
 * {@code
 *    @JsonSerialize(converter = AesIdLongSetJsonConvert.Serializer.class)
 *    @JsonDeserialize(converter = AesIdLongSetJsonConvert.Deserializer.class)
 * }
 * </pre>
 *
 * @author 谢长春 2022-07-04
 */
@Slf4j
public class AesIdLongSetJsonConverter {
    /**
     * id 序列化
     */
    public static class Serializer extends StdConverter<Set<Long>, Set<String>> {

        @Override
        public Set<String> convert(Set<Long> value) {
            if (CollectionUtils.isEmpty(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("加密:{}", value);
            }
            return DomainEncrypt
                    .encryptIds(value)
                    .collect(Collectors.toSet())
                    ;
        }
    }

    /**
     * id 反序列化
     */
    public static class Deserializer extends StdConverter<Set<String>, Set<Long>> {

        @Override
        public Set<Long> convert(Set<String> value) {
            if (CollectionUtils.isEmpty(value)) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("解密:{}", value);
            }
            return DomainEncrypt
                    .decryptLongId(value)
                    .collect(Collectors.toSet())
                    ;
        }
    }

}
