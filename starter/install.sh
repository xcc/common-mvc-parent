#!/usr/bin/env bash
# **********************************************************************************************************************
# 首次启动项目安装所有 starter ，之后如果有改动 starter 代码才需要再次执行安装命令
# **********************************************************************************************************************
mvn clean install -f ../pom.xml -pl \
  starter/app-auto-task-starter\
,starter/app-dao-jpa-starter\
,starter/app-encrypt-api-starter\
,starter/app-encrypt-domain-starter\
,starter/app-enums-starter\
,starter/app-excel-starter\
,starter/app-image-code-starter\
,starter/app-nfs-alioss-starter\
,starter/app-nfs-minio-starter\
,starter/app-nfs-obs-starter\
,starter/app-qrcode-starter\
,starter/app-security-starter\
,starter/app-sms-starter\
,starter/app-swagger-starter\
  -am -P starter
