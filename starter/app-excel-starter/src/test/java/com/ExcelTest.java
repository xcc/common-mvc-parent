package com;

import app.common.starter.util.FPath;
import app.excel.ExcelRewriter;
import app.excel.ISheetWriter;
import app.excel.Rownum;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Paths;
import java.util.Arrays;

import static app.excel.enums.Column.*;

@Slf4j
public class ExcelTest {
    @SneakyThrows
    public static void main(String[] args) {
//        Workbook wb = WorkbookFactory.create(DIR.FILES.file("excel/联系人-111111.xlsx"), "111111", true);//设置密码打开
//        wb.forEach(sheet -> {
//            sheet.forEach(row->{
//                row.forEach(cell -> {
//                    System.out.println(cell.getStringCellValue());
//                });
//            });
//        });
//        System.out.println("=================================");
//        System.out.println("Number of Sheets:" + wb.getNumberOfSheets());
//        System.out.println("Sheet3's name:" + wb.getSheetName(0));
//        System.out.println();
        @Cleanup final ExcelRewriter rewriter = ExcelRewriter
                .of(
                        Paths.get("starter/app-excel-starter/src/test/files/excel/data-row-template.xlsx").toFile(),
                        ISheetWriter.Options.DEFAULT
                )
                .sheet(0);
        final int fromRow = 0;
        final Rownum rownum = Rownum.of(11);
        Arrays.asList(
                DataRow.builder().id(1).name("Jack").phone("1234567980").amount(123456D).num(1.5).count(21).build(),
                DataRow.builder().id(2).name("Joe").amount(123456D).num(1.5).count(21).build(),
                DataRow.builder().id(3).name("Jhon").amount(123456D).num(1.5).count(21).build(),
                DataRow.builder().id(4).name("Joise").amount(123456D).num(1.5).count(21).build(),
                DataRow.builder().id(5).name("vcx").count(21).build(),
                DataRow.builder().id(6).name("fdsak").count(21).build(),
                DataRow.builder().id(7).name("Jackfdsa").phone("1234567980").amount(123456D).num(1.5).count(21).build(),
                DataRow.builder().id(8).name("Jack1").phone("1234567980").amount(123456D).build(),
                DataRow.builder().id(9).name("Jackf").phone("1234567980").amount(123456D).count(21).build(),
                DataRow.builder().id(10).name("Jack4").phone("1234567980").num(1.5).count(21).build()
        ).forEach(row -> {
            rewriter.copy(fromRow, rownum.index())
                    .row(rownum)
//                    .cell(B).writeNumber(row.getId())
                    .cell(C).writeString(row.getName())
                    .cell(D).writeString(row.getPhone())
                    .cell(E).writeNumber(row.getAmount())
                    .cell(F).writeNumber(row.getNum())
                    .cell(G).writeNumber(row.getCount())
            ;
            rownum.next();
        });
        rewriter.evaluateAllFormulaCells();
        log.info(String.format("写入路径：%s", rewriter.saveWorkBook(FPath.of(".temp", "data-row-template-重写.xlsx")).absolute()));
    }

    @Getter
    @Setter
    @ToString
    @Builder
    static class DataRow {
        private int id;
        private String name;
        private String phone;
        private Double amount;
        private double num;
        private int count;
    }
}
