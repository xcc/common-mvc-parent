package app.excel.starter.entity;

import app.common.starter.interfaces.IJson;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Excel sheet 表格实体对象
 *
 * @author 谢长春 on 2017/10/15 .
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)

public class Table implements IJson {
    /**
     * excel sheet 标签页索引
     */
    private int index;
    /**
     * excel sheet 标签页名称
     */
    private String name;
    /**
     * <pre>
     * 表头集合 =>
     * [
     * {index: 0, label: '房租', type: 'NUMBER', group: "固定成本",tag:"标签"},
     * {index: 1, label: '工资', type: 'NUMBER', group: "固定成本",tag:"标签"}
     * ]
     */
    private List<Cell> header;
    /**
     * <pre>
     * 表格行集合, （0.1.....）表示表头中的index，取值时，可以通过遍历表头的index字段获取值
     * [
     * {
     * 0: {type: 'NUMBER', text: "$1000", value: 1000},
     * 1: {type: 'NUMBER', text: "$1000", value: 1000}
     * }
     * ]
     */
    private List<Row> body;
    /**
     * <pre>
     * 表格底部合计行,（0.1.....）表示表头中的index，取值时，可以通过遍历表头的index字段获取值
     * {
     * 0: {type: 'NUMBER', text: "$2000", value: 2000},
     * 1: {type: 'NUMBER', text: "$2000", value: 2000}
     * }
     */
    private Row footer;
    /**
     * 表格附加字段（扩展属性）
     */
    private Map<String, Object> extras;

    /**
     * 将 header + body 转换为 map 对象；优先使用别名
     *
     * @return {@link List}{@link List<Map<String, String>>}
     */
    public List<Map<String, String>> toObjects() {
        return body.stream().map(row -> row.toMapString(header)).collect(Collectors.toList());
    }

    public static Table.TableBuilder builder() {
        return new Table.TableBuilder();
    }

    public static class TableBuilder {
        private int index;
        private String name;
        private List<Cell> header;
        private List<Row> body;
        private Row footer;
        private Map<String, Object> extras;

        TableBuilder() {
        }

        public Table.TableBuilder index(int index) {
            this.index = index;
            return this;
        }

        public Table.TableBuilder name(String name) {
            this.name = name;
            return this;
        }

        public Table.TableBuilder header(List<Cell> header) {
            this.header = header;
            return this;
        }

        public Table.TableBuilder body(List<Row> body) {
            this.body = body;
            return this;
        }

        public Table.TableBuilder footer(Row footer) {
            this.footer = footer;
            return this;
        }

        public Table.TableBuilder extras(Map<String, Object> extras) {
            this.extras = extras;
            return this;
        }

        public Table build() {
            return new Table()
                    .setIndex(this.index)
                    .setName(this.name)
                    .setHeader(this.header)
                    .setBody(this.body)
                    .setFooter(this.footer)
                    .setExtras(this.extras)
                    ;
        }
    }
}
