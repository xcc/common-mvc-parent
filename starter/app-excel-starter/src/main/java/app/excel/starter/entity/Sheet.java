package app.excel.starter.entity;

import app.common.starter.interfaces.IJson;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Excel sheet 实体对象
 *
 * @author 谢长春 on 2017/10/15 .
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)

public class Sheet implements IJson {
    /**
     * excel sheet 标签页索引
     */
    private int index;
    /**
     * excel sheet 标签页名称
     */
    private String name;
    /**
     * excel sheet 标签页内容
     */
    private Table table;

}
