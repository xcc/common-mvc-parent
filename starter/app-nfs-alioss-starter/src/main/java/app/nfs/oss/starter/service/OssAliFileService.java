package app.nfs.oss.starter.service;

import app.common.starter.enums.Code;
import app.common.starter.util.JSON;
import app.nfs.starter.dto.FileDTO;
import app.nfs.starter.service.FileService;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.nio.channels.SeekableByteChannel;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 阿里云 oss 读写服务接口
 * <pre>
 * https://help.aliyun.com/document_detail/32011.html
 * </pre>
 *
 * @author 谢长春 2020-11-25
 */
@Primary
@Slf4j
@Getter
@Setter
@Service
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.nfs.oss")
public class OssAliFileService implements FileService {
    private OSS aliOssClient;
    /**
     * 是否开启 alioss 存储；true: 是，false: 否
     */
    @Setter
    @Getter
    private boolean enabled;
    /**
     * 存储桶名称， 注： 阿里云 OSS 的 bucket 必须是全局唯一，不能跟其他用户重叠，最好在阿里云控制台提前建好
     */
    @Setter
    @Getter
    private String bucketName;
    /**
     * 这里置空，文件存储在 bucketName
     */
    @Setter
    @Getter
    private String directory;
    /**
     * 文件服务访问地址
     */
    @Setter
    @Getter
    private String server;
    /**
     * 区域
     */
    @Setter
    @Getter
    private String region;
    /**
     * 外网文件上传节点: https://help.aliyun.com/document_detail/31837.html
     */
    @Setter
    @Getter
    private String external;
    /**
     * ecs 内网文件上传节点: https://help.aliyun.com/document_detail/31837.html
     */
    @Setter
    @Getter
    private String internal;
    /**
     * 当前文件上传使用节点
     * 开发环境使用外网节点
     * 如果部署在阿里云服务器，上传文件优先选内网地址
     */
    @Setter
    @Getter
    private String endpoint;
    /**
     *
     */
    @Setter
    @Getter
    private String sts;
    /**
     * Access key就像用户ID，可以唯一标识你的账户。
     */
    @Setter
    @Getter
    private String accessKey;
    /**
     * Secret key是你账户的密码。
     */
    @Setter
    @Getter
    private String secretKey;

    private static final int MAX = 90;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PreDestroy
    public void preDestroy() {
        aliOssClient.shutdown();
    }

    @PostConstruct
    @Override
    public void postConstruct() {
        log.info(LOG_PATTERN
                , "阿里oss文件存储配置"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.directory: " + directory, MAX, ' '), MAX) + " # 这里置空，文件存储在 bucketName"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.server: " + server, MAX, ' '), MAX) + " # 文件服务访问地址"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.region: " + region, MAX, ' '), MAX) + " # 区域"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.bucketName: " + bucketName, MAX, ' '), MAX) + " # 存储桶名称"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.external: " + external, MAX, ' '), MAX) + " # 外网文件上传节点"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.internal: " + internal, MAX, ' '), MAX) + " # 内网文件上传节点"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.endpoint: " + endpoint, MAX, ' '), MAX) + " # 当前文件上传使用节点，如果部署在阿里云服务器，上传文件优先选内网地址"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.sts: " + sts, MAX, ' '), MAX) + " # 用于生成前端上传token"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.accessKey: " + accessKey, MAX, ' '), MAX) + " # 授权账号"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.oss.secretKey: " + secretKey, MAX, ' '), MAX) + " # 授权密钥"
                )
                , "阿里oss文件存储配置"
        );

        directory = "";
        if (!server.endsWith("/")) {
            throw new IllegalArgumentException("【spring.app.nfs.oss.server】 配置必须以 / 结尾");
        }

        // 创建OSSClient实例。
        aliOssClient = new OSSClientBuilder().build(endpoint, accessKey, secretKey);
        // 检查存储桶是否已经存在
        final boolean isBucketExist = aliOssClient.doesBucketExist(bucketName);
        if (!isBucketExist) {
            // 创建存储桶
            aliOssClient.createBucket(bucketName);
            // 设置权限: 公有读
            aliOssClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
            {// 设置跨域访问
//                final SetBucketCORSRequest request = new SetBucketCORSRequest(bucketName);
//                SetBucketCORSRequest.CORSRule rule = new SetBucketCORSRequest.CORSRule();
//                rule.addAllowdOrigin("*");
//                rule.addAllowedMethod("*");
//                rule.addAllowedHeader("GET");
//                rule.setMaxAgeSeconds(3600);
//                aliOssClient.setBucketCORS(request);
            }
            {
                // 设置桶内静态资源可以直接访问
//                final SetBucketWebsiteRequest request = new SetBucketWebsiteRequest(bucketName);
//                request.setIndexDocument("index.html");
//                request.setErrorDocument("error.html");
//                request.AddRoutingRule();
//                aliOssClient.setBucketWebsite(request);
            }
            {
                // BucketPolicy 授权策略： https://help.aliyun.com/document_detail/85111.html
                // {"Statement": [{"Action": ["s3:GetBucketLocation", "s3:ListBucket"], "Effect": "Allow", "Principal": "*", "Resource": "arn:aws:s3:::%s"}, {"Action": "s3:GetObject", "Effect": "Allow", "Principal": "*", "Resource": "arn:aws:s3:::%s/myobject*"}], "Version": "2012-10-17"}
                // aliOssClient.setBucketPolicy(bucketName, String.format("{\"Version\": \"2012-10-17\", \"Statement\": [{\"Action\": \"s3:GetObject\", \"Effect\": \"Allow\", \"Principal\": \"*\", \"Resource\": \"arn:aws:s3:::%s/*\"}]}"
                //         , bucketName
                // ));
            }
        }
        log.info("ali oss 存储桶 【{}】 创建成功", bucketName);
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final MultipartFile uploadFile, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = aliOssClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return fileDTO;
            }
        }
        @Cleanup final InputStream inputStream = uploadFile.getInputStream();
        final ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(uploadFile.getContentType());
        // metadata.setContentLength(inputStream.available());
        // 上传文件到存储桶
        final PutObjectResult result = aliOssClient.putObject(
                bucketName,
                fileDTO.getAbsolutePath(),
                inputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getRequestId()
            );
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final InputStream inputStream, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = aliOssClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return fileDTO;
            }
        }
        final ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setContentLength(inputStream.available());
        metadata.setContentType(fileDTO.getContentType());
        // 上传文件到存储桶
        final PutObjectResult result = aliOssClient.putObject(
                bucketName,
                fileDTO.getAbsolutePath(),
                inputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getRequestId()
            );
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final File srcFile, final T outFileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", outFileDTO.getName(), server, outFileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(outFileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = aliOssClient.doesObjectExist(bucketName, outFileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return outFileDTO;
            }
        }
        @Cleanup final FileInputStream fileInputStream = new FileInputStream(srcFile);
        @Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        final ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setContentLength(bufferedInputStream.available());
        metadata.setContentType(outFileDTO.getContentType());
        // 上传文件到存储桶
        final PutObjectResult result = aliOssClient.putObject(
                bucketName,
                outFileDTO.getAbsolutePath(),
                bufferedInputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getRequestId()
            );
        }
        return outFileDTO;
    }

    @SneakyThrows
    @Override
    public Optional<String> readString(final FileDTO fileDTO) {
        @Cleanup final OSSObject object = aliOssClient.getObject(bucketName, fileDTO.getAbsolutePath());
        @Cleanup final InputStream inputStream = object.getObjectContent();
        @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8);
        return Optional.of(CharStreams.toString(inputStreamReader));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJson(final T fileDTO, final TypeReference<R> typeReference) {
        return readString(fileDTO).flatMap(jsonText -> JSON.parseOptional(jsonText, typeReference));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJsonObject(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).flatMap(jsonText -> JSON.parseObjectOptional(jsonText, clazz));
    }

    @Override
    public <T extends FileDTO, R> Optional<List<R>> readJsonArray(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).map(jsonText -> JSON.parseList(jsonText, clazz));
    }

    @SneakyThrows
    @Override
    public byte[] readBytes(final FileDTO fileDTO) {
        @Cleanup final OSSObject object = aliOssClient.getObject(bucketName, fileDTO.getAbsolutePath());
        @Cleanup final InputStream inputStream = object.getObjectContent();
        return ByteStreams.toByteArray(inputStream);
    }

    @SneakyThrows
    @Override
    public BufferedReader readStream(final FileDTO fileDTO) {
        try (
                final OSSObject object = aliOssClient.getObject(bucketName, fileDTO.getAbsolutePath());
                final InputStream inputStream = object.getObjectContent();
                final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8)
        ) {
            return new BufferedReader(inputStreamReader);
        }
    }

    @SneakyThrows
    @Override
    public InputStream readInputStream(final FileDTO fileDTO) {
        try (final OSSObject object = aliOssClient.getObject(bucketName, fileDTO.getAbsolutePath());
             final InputStream inputStream = object.getObjectContent()) {
            return inputStream;
        }
    }

    @SneakyThrows
    @Override
    public SeekableByteChannel readChannel(final FileDTO fileDTO) {
        throw new Exception("alioss 暂不支持 SeekableByteChannel");
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final FileDTO outFileDTO) {
        final String srcFilePath = srcFileDTO.getAbsolutePath();
        final String outFilePath = outFileDTO.getAbsolutePath();
        if (Objects.equals(srcFilePath, outFilePath)) {
            return;
        }
        final CopyObjectResult result = aliOssClient.copyObject(
                bucketName
                , srcFilePath
                , bucketName
                , outFilePath
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, requestId={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getRequestId()
            );
        }
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final OutputStream outputStream) {
        ByteStreams.copy(readInputStream(srcFileDTO), outputStream);
    }

    @Override
    public <T extends FileDTO> boolean exists(final T fileDTO) {
        if (Objects.isNull(fileDTO)) {
            return false;
        }
        return aliOssClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFolder(final T folder, final BiConsumer<String, String> consumer) {
        log.error("ali oss 暂未实现该接口：forEachFolder");
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFile(final T folder, final BiConsumer<String, String> consumer) {
        log.error("ali oss 暂未实现该接口：forEachFile");
    }

    @Override
    public void deleteFolder(final List<String> folderPaths) {
        log.warn("ali oss 暂不支持该操作：deleteFolder");
        if (CollectionUtils.isEmpty(folderPaths)) {
            return;
        }
//        folderPaths.forEach(absolute -> {
//            try {
//                if (!aliOssClient.doesObjectExist(bucketName, absolute)) {
//                    aliOssClient.deleteDirectory(bucketName, absolute);
//                }
//                log.info("删除文件夹【{}】: 成功", absolute);
//            } catch (Exception e) {
//                log.error("删除文件夹【{}】: {}", absolute, e.getMessage(), e);
//            }
//        });
    }

    @Override
    public void deleteFile(final List<String> filePaths) {
        if (CollectionUtils.isEmpty(filePaths)) {
            return;
        }
        filePaths.forEach(absolute -> {
            try {
                if (!aliOssClient.doesObjectExist(bucketName, absolute)) {
                    aliOssClient.deleteObject(bucketName, absolute);
                }
                log.info("删除文件【{}】: 成功", absolute);
            } catch (Exception e) {
                log.error("删除文件【{}】: {}", absolute, e.getMessage(), e);
            }
        });
    }
}
