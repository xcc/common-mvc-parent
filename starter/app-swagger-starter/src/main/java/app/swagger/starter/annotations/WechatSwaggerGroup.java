package app.swagger.starter.annotations;

import java.lang.annotation.*;

/**
 * 微信小程序、微信公众号接口分组
 *
 * @author 谢长春 2022-02-02
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface WechatSwaggerGroup {

}
