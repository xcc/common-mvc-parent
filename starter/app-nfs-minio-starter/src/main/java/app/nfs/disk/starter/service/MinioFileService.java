package app.nfs.disk.starter.service;

import app.common.starter.enums.Code;
import app.common.starter.util.JSON;
import app.nfs.starter.dto.FileDTO;
import app.nfs.starter.service.FileService;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.nio.channels.SeekableByteChannel;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Minio 文件读写服务接口
 * <pre>
 * https://docs.minio.io/
 * http://docs.minio.org.cn/
 * </pre>
 *
 * @author 谢长春 2020-11-19
 */
@Primary
@Slf4j
@Getter
@Setter
@Service
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.nfs.minio")
public class MinioFileService implements FileService {
    private AmazonS3 minioClient;
    /**
     * 是否开启 minio 存储；true: 是，false: 否
     */
    @Setter
    @Getter
    private boolean enabled;
    /**
     * 存储桶名称
     */
    @Setter
    @Getter
    private String bucketName;
    /**
     * 这里置空，文件存储在 bucketName
     */
    @Setter
    @Getter
    private String directory;
    /**
     * 文件服务访问地址
     */
    @Setter
    @Getter
    private String server;
    /**
     * 对象存储服务的URL
     */
    @Setter
    @Getter
    private String endpoint;
    /**
     * 区域
     */
    @Setter
    @Getter
    private Regions region = Regions.CN_NORTH_1;
    /**
     * Access key就像用户ID，可以唯一标识你的账户。
     */
    @Setter
    @Getter
    private String accessKey;
    /**
     * Secret key是你账户的密码。
     */
    @Setter
    @Getter
    private String secretKey;

    private static final int MAX = 90;
    private static final String LOG_PATTERN = String.format(
            "\n┏ ┳%s {} %s ┳ ┓\n\n{}\n\n┗ ┻%s {} %s ┻ ┛"
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┳", 30)
            , Strings.repeat(" ┻", 30)
            , Strings.repeat(" ┻", 30)
    );

    @PreDestroy
    public void preDestroy() {
        minioClient.shutdown();
    }

    @PostConstruct
    @SneakyThrows
    @Override
    public void postConstruct() {
        log.info(LOG_PATTERN
                , "minio文件存储配置"
                , String.join("\n"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.directory: " + directory, MAX, ' '), MAX) + " # 这里置空，文件存储在 bucketName"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.server: " + server, MAX, ' '), MAX) + " # 文件服务访问地址"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.bucketName: " + bucketName, MAX, ' '), MAX) + " # 存储桶名称"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.endpoint: " + endpoint, MAX, ' '), MAX) + " # 当前使用节点"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.region: " + region, MAX, ' '), MAX) + " # 当前区域"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.accessKey: " + accessKey, MAX, ' '), MAX) + " # 授权账号"
                        , StringUtils.left(StringUtils.rightPad("spring.app.nfs.minio.secretKey: " + server, MAX, ' '), MAX) + " # 授权密钥"
                )
                , "minio文件存储配置"
        );

        directory = "";
        if (!server.endsWith("/")) {
            throw new IllegalArgumentException("【spring.app.nfs.minio.server】 配置必须以 / 结尾");
        }

        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setSignerOverride("AWSS3V4SignerType");

        minioClient = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region.getName()))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                .build();
        // 检查存储桶是否已经存在
        final boolean isBucketExist = minioClient.doesBucketExistV2(bucketName);
        if (!isBucketExist) {
            // 创建存储桶
            minioClient.createBucket(bucketName);
            // 设置权限: 公有读
//            minioClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
            // BucketPolicy 授权策略
            // {"Statement": [{"Action": ["s3:GetBucketLocation", "s3:ListBucket"], "Effect": "Allow", "Principal": "*", "Resource": "arn:aws:s3:::%s"}, {"Action": "s3:GetObject", "Effect": "Allow", "Principal": "*", "Resource": "arn:aws:s3:::%s/myobject*"}], "Version": "2012-10-17"}
            // 设置权限: 公有读
            minioClient.setBucketPolicy(bucketName, String.format("{\"Version\": \"2012-10-17\", \"Statement\": [{\"Action\": \"s3:GetObject\", \"Effect\": \"Allow\", \"Principal\": \"*\", \"Resource\": \"arn:aws:s3:::%s/*\"}]}"
                    , bucketName
            ));
        }
        log.info("minio 存储桶 【{}】 创建成功", bucketName);
    }
//    @SneakyThrows
//    private void putObject(final String path, final InputStream inputStream, final boolean replace) {
//        if (log.isInfoEnabled()) {
//            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getPath());
//        }
//        Code.A00001.assertNonBlank(path, "必须指定存储路径");
//        if (!replace) {
//            final boolean fileExists = minioClient.doesObjectExist(bucketName, path);
//            if (fileExists) { // 文件已存在，不重复上传文件
//                return;
//            }
//        }
//        minioClient.putObject(bucketName, path, inputStream)
//        // 上传文件到存储桶
//        final ObjectWriteResponse response = minioClient.putObject(PutObjectArgs.builder()
//                .bucket(bucketName)
//                .object(path)
//                .stream(inputStream, inputStream.available(), -1)
//                .build()
//        );
//        if (log.isDebugEnabled()) {
//            log.debug("bucket={}, region={}, object={}, etag={}, versionId={}"
//                    , response.bucket()
//                    , response.region()
//                    , response.object()
//                    , response.etag()
//                    , response.versionId()
//            );
//        }
//    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final MultipartFile uploadFile, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = minioClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return fileDTO;
            }
        }
        @Cleanup final InputStream inputStream = uploadFile.getInputStream();
        final ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(uploadFile.getContentType());
        // metadata.setContentLength(inputStream.available());
        // 上传文件到存储桶
        final PutObjectResult result = minioClient.putObject(
                bucketName,
                fileDTO.getAbsolutePath(),
                inputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, contentMd5={}, metadata={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getContentMd5()
                    , JSON.toJsonString(result.getMetadata())
            );
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final InputStream inputStream, final T fileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(fileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = minioClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return fileDTO;
            }
        }
        final ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setContentLength(inputStream.available());
        metadata.setContentType(fileDTO.getContentType());
        // 上传文件到存储桶
        final PutObjectResult result = minioClient.putObject(
                bucketName,
                fileDTO.getAbsolutePath(),
                inputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, contentMd5={}, metadata={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getContentMd5()
                    , JSON.toJsonString(result.getMetadata())
            );
        }
        return fileDTO;
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> T write(final File srcFile, final T outFileDTO, final boolean replace) {
        if (log.isInfoEnabled()) {
            log.info("上传文件【{}】：{}{}", outFileDTO.getName(), server, outFileDTO.getAbsolutePath());
        }
        Code.A00001.assertNonBlank(outFileDTO.getAbsolutePath(), "必须指定存储路径");
        if (!replace) {
            final boolean fileExists = minioClient.doesObjectExist(bucketName, outFileDTO.getAbsolutePath());
            if (fileExists) { // 文件已存在，不重复上传文件
                return outFileDTO;
            }
        }
        @Cleanup final FileInputStream fileInputStream = new FileInputStream(srcFile);
        @Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        final ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setContentLength(bufferedInputStream.available());
        metadata.setContentType(outFileDTO.getContentType());
        // 上传文件到存储桶
        final PutObjectResult result = minioClient.putObject(
                bucketName,
                outFileDTO.getAbsolutePath(),
                bufferedInputStream,
                metadata
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}, contentMd5={}, metadata={}"
                    , result.getVersionId()
                    , result.getETag()
                    , result.getContentMd5()
                    , JSON.toJsonString(result.getMetadata())
            );
        }
        return outFileDTO;
    }
//
//    @SneakyThrows
//    @Override
//    public <T extends FileDTO> T write(final BufferedInputStream bufferedInputStream, final T fileDTO, final boolean replace) {
//        if (log.isInfoEnabled()) {
//            log.info("上传文件【{}】：{}{}", fileDTO.getName(), server, fileDTO.getPath());
//        }
//        Code.A00001.assertNonBlank(fileDTO.getPath(), "必须指定存储路径");
//        if (!replace) {
//            final boolean fileExists = minioClient.doesObjectExist(bucketName, fileDTO.getPath());
//            if (fileExists) { // 文件已存在，不重复上传文件
//                return fileDTO;
//            }
//        }
//        final ObjectMetadata metadata = new ObjectMetadata();
//        metadata.setContentLength(bufferedInputStream.available());
//        // 上传文件到存储桶
//        final PutObjectResult result = minioClient.putObject(
//                bucketName,
//                fileDTO.getPath(),
//                bufferedInputStream,
//                metadata
//        );
//        if (log.isDebugEnabled()) {
//            log.debug("versionId={}, etag={}, contentMd5={}, metadata={}"
//                    , result.getVersionId()
//                    , result.getETag()
//                    , result.getContentMd5()
//                    , JSON.toJsonString(result.getMetadata())
//            );
//        }
//        return fileDTO;
//    }

    @SneakyThrows
    @Override
    public Optional<String> readString(final FileDTO fileDTO) {
        @Cleanup final S3Object object = minioClient.getObject(bucketName, fileDTO.getAbsolutePath());
        @Cleanup final S3ObjectInputStream inputStream = object.getObjectContent();
        @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8);
        return Optional.of(CharStreams.toString(inputStreamReader));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJson(final T fileDTO, final TypeReference<R> typeReference) {
        return readString(fileDTO).flatMap(jsonText -> JSON.parseOptional(jsonText, typeReference));
    }

    @Override
    public <T extends FileDTO, R> Optional<R> readJsonObject(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).flatMap(jsonText -> JSON.parseObjectOptional(jsonText, clazz));
    }

    @Override
    public <T extends FileDTO, R> Optional<List<R>> readJsonArray(final T fileDTO, final Class<R> clazz) {
        return readString(fileDTO).map(jsonText -> JSON.parseList(jsonText, clazz));
    }

    @SneakyThrows
    @Override
    public byte[] readBytes(final FileDTO fileDTO) {
        @Cleanup final S3Object object = minioClient.getObject(bucketName, fileDTO.getAbsolutePath());
        @Cleanup final S3ObjectInputStream inputStream = object.getObjectContent();
        return ByteStreams.toByteArray(inputStream);
    }

    @SneakyThrows
    @Override
    public BufferedReader readStream(final FileDTO fileDTO) {
        try (final S3Object object = minioClient.getObject(bucketName, fileDTO.getAbsolutePath());
             final S3ObjectInputStream inputStream = object.getObjectContent();
             final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8)) {
            return new BufferedReader(inputStreamReader);
        }
    }

    @SneakyThrows
    @Override
    public InputStream readInputStream(final FileDTO fileDTO) {
        try (final S3Object object = minioClient.getObject(bucketName, fileDTO.getAbsolutePath());
             final S3ObjectInputStream inputStream = object.getObjectContent()) {
            return inputStream;
        }
    }

    @SneakyThrows
    @Override
    public SeekableByteChannel readChannel(final FileDTO fileDTO) {
        throw new Exception("minio 暂不支持 SeekableByteChannel");
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final FileDTO outFileDTO) {
        final String srcFilePath = srcFileDTO.getAbsolutePath();
        final String outFilePath = outFileDTO.getAbsolutePath();
        if (Objects.equals(srcFilePath, outFilePath)) {
            return;
        }
        final CopyObjectResult result = minioClient.copyObject(
                bucketName
                , srcFilePath
                , bucketName
                , outFilePath
        );
        if (log.isDebugEnabled()) {
            log.debug("versionId={}, etag={}"
                    , result.getVersionId()
                    , result.getETag()
            );
        }
    }

    @SneakyThrows
    @Override
    public void copy(final FileDTO srcFileDTO, final OutputStream outputStream) {
        ByteStreams.copy(readInputStream(srcFileDTO), outputStream);
    }

    @Override
    public <T extends FileDTO> boolean exists(final T fileDTO) {
        if (Objects.isNull(fileDTO)) {
            return false;
        }
        return minioClient.doesObjectExist(bucketName, fileDTO.getAbsolutePath());
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFolder(final T folder, final BiConsumer<String, String> consumer) {
        log.error("minio 暂未实现该接口：forEachFolder");
    }

    @SneakyThrows
    @Override
    public <T extends FileDTO> void forEachFile(final T folder, final BiConsumer<String, String> consumer) {
        log.error("minio 暂未实现该接口：forEachFile");
    }

    @Override
    public void deleteFolder(final List<String> folderPaths) {
        log.warn("minio 暂不支持该操作：deleteFolder");
        if (CollectionUtils.isEmpty(folderPaths)) {
            return;
        }
    }

    @Override
    public void deleteFile(final List<String> filePaths) {
        if (CollectionUtils.isEmpty(filePaths)) {
            return;
        }
        filePaths.forEach(absolute -> {
            try {
                minioClient.deleteObject(bucketName, absolute);
                log.info("删除文件【{}】: 成功", absolute);
            } catch (Exception e) {
                log.error("删除文件【{}】: {}", absolute, e.getMessage(), e);
            }
        });
    }
}

//
///**
// * Minio 读写服务接口
// *
// * @author 谢长春 2020-11-19
// */
//@Primary
//@Slf4j
//@Service
//@RequiredArgsConstructor
//@ConditionalOnProperty(value = "spring.app.nfs.type", havingValue = "minio")
//public class MinioFileService implements FileService {
//    private final MinioConfiguration minioConfiguration;
//    private final MinioClient minioClient;
//
//    @SneakyThrows
//    private void putObject(final String path, final InputStream inputStream, final boolean replace) {
//        if (log.isInfoEnabled()) {
//            log.info("上传文件路径：{}{}", server, path);
//        }
//        Code.A00001.assertNonBlank(path, "必须指定存储路径");
//        if (!replace) {
//            final boolean fileExists = minioClient
//                    .listObjects(ListObjectsArgs.builder()
//                            .bucket(bucketName)
//                            .prefix(path)
//                            .build()
//                    )
//                    .iterator()
//                    .hasNext();
//            if (fileExists) { // 文件已存在，不重复上传文件
//                return;
//            }
//        }
//        // 上传文件到存储桶
//        final ObjectWriteResponse response = minioClient.putObject(PutObjectArgs.builder()
//                .bucket(bucketName)
//                .object(path)
//                .stream(inputStream, inputStream.available(), -1)
//                .build()
//        );
//        if (log.isDebugEnabled()) {
//            log.debug("bucket={}, region={}, object={}, etag={}, versionId={}"
//                    , response.bucket()
//                    , response.region()
//                    , response.object()
//                    , response.etag()
//                    , response.versionId()
//            );
//        }
//    }
//
//    @SneakyThrows
//    @Override
//    public <T extends FileDTO> T write(final MultipartFile uploadFile, final T fileDTO, final boolean replace) {
//        putObject(fileDTO.getPath(), uploadFile.getInputStream(), replace);
//        return fileDTO;
//    }
//
//    @Override
//    public <T extends FileDTO> T write(final String content, final T fileDTO, final boolean replace) {
//        return write(content.getBytes(UTF_8), fileDTO, replace);
//    }
//
//    @SneakyThrows
//    @Override
//    public <T extends FileDTO> T write(final byte[] bytes, final T fileDTO, final boolean replace) {
//        @Cleanup final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
////        fileDTO.setName(fileName.getName());
////        byteArrayInputStream.mark(0);
////        fileDTO.setUname(fileName.toMd5Uname(byteArrayInputStream));
////        byteArrayInputStream.reset();
////        putObject(fileDTO.getPath(), byteArrayInputStream, replace);
//        putObject(fileDTO.getPath(), byteArrayInputStream, replace);
//        return fileDTO;
//    }
//
//    @SneakyThrows
//    @Override
//    public <T extends FileDTO> T write(final File file, final T fileDTO, final boolean replace) {
//        @Cleanup final FileInputStream fileInputStream = new FileInputStream(file);
//        @Cleanup final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
////        bufferedInputStream.mark(0);
////        fileDTO.setUname(fileName.toMd5Uname(bufferedInputStream));
////        bufferedInputStream.reset();
//        putObject(fileDTO.getPath(), bufferedInputStream, replace);
//        return fileDTO;
//    }
//
//    @SneakyThrows
//    @Override
//    public <T extends FileDTO> T write(final BufferedInputStream bufferedInputStream, final T fileDTO, final boolean replace) {
////        bufferedInputStream.mark(0);
////        fileDTO.setUname(fileName.toMd5Uname(bufferedInputStream));
////        bufferedInputStream.reset();
//        putObject(fileDTO.getPath(), bufferedInputStream, replace);
//        return fileDTO;
//    }
//
//    @SneakyThrows
//    @Override
//    public Optional<String> readString(final FileDTO file) {
//        @Cleanup final InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
//                .bucket(bucketName)
//                .object(file.getPath())
//                .build()
//        );
//        @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8);
//        return Optional.of(CharStreams.toString(inputStreamReader));
//    }
//
//    @Override
//    public <T extends FileDTO, R> Optional<R> readJson(final T file, final TypeReference<R> typeReference) {
//        return readString(file).map(jsonText -> JSON.parseObject(jsonText, typeReference));
//    }
//
//    @Override
//    public <T extends FileDTO, R> Optional<R> readJsonObject(final T file, final Class<R> clazz) {
//        return readString(file).map(jsonText -> JSON.parseObject(jsonText, clazz));
//    }
//
//    @Override
//    public <T extends FileDTO, R> Optional<List<R>> readJsonArray(final T file, final Class<R> clazz) {
//        return readString(file).map(jsonText -> JSON.parseArray(jsonText, clazz));
//    }
//
//    @SneakyThrows
//    @Override
//    public byte[] readBytes(final FileDTO file) {
//        @Cleanup final InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
//                .bucket(bucketName)
//                .object(file.getUname())
//                .build()
//        );
//        return ByteStreams.toByteArray(inputStream);
//    }
//
//    @SneakyThrows
//    @Override
//    public BufferedReader readStream(final FileDTO file) {
//        @Cleanup final InputStream inputStream = minioClient.getObject(GetObjectArgs.builder()
//                .bucket(bucketName)
//                .object(file.getUname())
//                .build()
//        );
//        @Cleanup final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8);
//        return new BufferedReader(inputStreamReader);
//    }
//}
