package app.enums.starter.interfaces;

/**
 * 应用操作埋点事件
 *
 * @author 谢长春 2022-11-15
 */
public interface IAppEventKey {
    /**
     * 埋点事件名称
     */
    String name();

    /**
     * 埋点事件说明
     */
    String comment();

    /**
     * 是否在客户端埋点
     */
    default boolean isClient() {
        return false;
    }

    /**
     * 是否点击事件触发埋点
     *
     * @return boolean true:是
     */
    default boolean isClick() {
        return false;
    }

    /**
     * 埋点关联数据表主键数据类型是否为字符串
     *
     * @return boolean true:是
     */
    default boolean isRefString() {
        return false;
    }

    /**
     * 埋点关联数据表主键数据类型是否为数字
     *
     * @return boolean true:是
     */
    default boolean isRefLong() {
        return !isRefString();
    }
}
