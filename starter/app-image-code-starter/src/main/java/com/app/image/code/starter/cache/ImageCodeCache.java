package com.app.image.code.starter.cache;

import app.common.starter.enums.Code;
import app.common.starter.interfaces.ICache;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;

/**
 * 图片验证码缓存
 *
 * @author 谢长春 2022-05-04
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ImageCodeCache implements ICache {
    public static final String CACHE_KEY = ImageCodeCache.class.getSimpleName() + ":%s";


    private final RedissonClient redissonClient;

    /**
     * uuid作为key缓存验证码
     *
     * @param uuid String UUID
     * @param code String 验证码
     * @return String
     */
    public String set(final String uuid, final String code) {
        final RMap<String, Object> imageCodeBucket = redissonClient.getMap(String.format(CACHE_KEY, uuid));
        imageCodeBucket.put("code", code); // 缓存验证码
        imageCodeBucket.put("count", 0); // 计数，避免暴力破解，验证码输入错误次数达到 3 次时，删除验证码
        imageCodeBucket.expire(Duration.ofMinutes(10)); // 验证码有效期 10 分钟
        return code;
    }

    private static final String CODE = "code";
    private static final String COUNT = "count";

    /**
     * 断言验证码是否正确， 避免暴力破解，验证码错误次数达到 3 次时，删除验证码
     *
     * @param uuid String
     * @param code String 验证码
     */
    public void asserts(final String uuid, final String code) {
        final RMap<String, Object> imageCodeBucket = redissonClient.getMap(String.format(CACHE_KEY, uuid));
        Code.A00002.assertHasTrue(imageCodeBucket.isExists(), "验证码已过期");
        if (Objects.equals(code, imageCodeBucket.get(CODE))) {
            imageCodeBucket.delete(); // 验证码校验成功
        } else {
            final int count = Optional
                    .ofNullable(imageCodeBucket.get(COUNT))
                    .map(Objects::toString)
                    .map(Integer::parseInt)
                    .orElse(0)
                    + 1;
            imageCodeBucket.put(COUNT, count);
            if (count >= 3) { // 计数，避免暴力破解，验证码输入错误次数达到 3 次时，删除验证码
                imageCodeBucket.delete();
            }
            throw Code.A00002.toCodeException("验证码错误");
        }
    }


}
