import lodash from 'lodash'
import gulp from 'gulp'
import watch from 'gulp-watch'
import batch from 'gulp-batch'
import env from 'gulp-env'
import randomstring from 'randomstring'
import fs from 'fs'
import path from 'path'
import readline from 'readline'
import './src/utils/string-prototype'
import './src/utils/date-prototype'
import xlsx from 'xlsx'
// import crypto from 'crypto-js'
// import AES from 'crypto-js/aes'
// import crypto.enc.Utf8 from 'crypto-js/enc-utf8'
// import crypto.enc.Base64 from 'crypto-js/enc-base64'
// import Mode from 'crypto-js/mode-cfb'
// import PaddingPkcs7 from 'crypto-js/pad-pkcs7'
import browser from 'browser-sync'
import Table from './src/code/core/Table'
import Paths from './src/utils/entity/Paths'
import Aes from './src/utils/Aes'
import axios from 'axios'
import {pinyin} from 'pinyin-pro'
import Dates from './src/utils/entity/Dates'

const web = browser.create()

const args = require('minimist')(process.argv.slice(2), {
  default: {
    // 默认值
    env: 'dev'
  }
})

env({
  file: `.env.${args.env}.js`,
  vars: {
    // 默认值
    // value: defaultValue
  }
})

gulp.task('default', async() => {
})

gulp.task('test', async() => {
  await import('./src/api/http')
  // await (await import('./test/api/CommonService.test')).default.csrfToken()

  await (await import('./test/api/CodeAuthIdService.test.js')).default.testAll() // 测试代码模板接口
  await (await import('./test/api/CodeSearchIdService.test.js')).default.testAll() // 测试代码模板接口
  await (await import('./test/api/OpenCodeOpenSearchIdService.test')).default.testAll() // 测试代码模板接口
  await (await import('./test/api/OpenCodeOpenIdService.test.js')).default.testAll() // 测试代码模板接口

  //await (await import('./test/api/CodeExampleService.test')).default.testAll()
  await (await import('./test/api/ErrorLogService.test')).default.testAll()
  await (await import('./test/api/UploadService.test')).default.testAll() // 测试通用模块接口
  await (await import('./test/api/RoleService.test')).default.testAll() // 测试角色接口
  await (await import('./test/api/UserService.test')).default.testAll() // 测试用户相关的接口
})
gulp.task('test:one', async() => {
  await import('./src/api/http')
  // await (await import('./test/api/UploadService.test')).default.csrfToken()
  await (await import('./test/api/UserService.test')).default.loginAdmin()

})

gulp.task('aes', async() => {
  // process.env.VUE_APP_AES_ENABLED='YES'`
  // process.env.VUE_APP_AES_SECRET_KEY='VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF'
  // process.env.VUE_APP_AES_IV='2022010100112299'

  Aes.setEnabled('YES')
  Aes.setSecretKey('VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF')

  console.log(Aes.decrypt('MDAwMTY0NTQ0NDU1NjAwNvvAt+nSiC/vRWuvBMFPcS6XeET6qKXOcBjfBIxamR+2Mbby087PzqTg8VwhRDCEd8FEWeRYVDy54E3PPSwHKrHEYR4uPVM9dDHoymqkFyxzl++FiSzEh7ygkJdtvOpm6N4MOa0XzwJ2LzBNKsseYoWTu2oi49klTrPv58hHQ3a9jZzgpCOF9/JzZWPnaY9EYHZnqW4L214vwn5bPly3m8ynzajiDamGeGyXr3ucQaL2rVad5rGQYro94rJDS6+vXk2E8VdPEQ3rtuBm+aMepGVKN8ak+QS5L8QZ5DVB+xZQShVHQfRQc8oRVrfAmohGl3EDN8zRP0WYi4vBIb8/SOYVQjmmiyEplxvuu8hlACJlkYP9asHMFVM91j/v1jFENynw2ojUcsuF49D6S6o9c+xn3NUy7MnTI0dNwb+ZPgPxGenAR40McOqsWLpWLKJUwb4na3dvgX/6Jj9/91T9QXv34VjUmFWOkIpDDO3QshMe2667ww3Oj4LIgiEc5YzgaU4AHnwqoAciOcShHp5/g2ucyd+YbLvght3auywjSBIitFFJl3OncWgLNZXFH0dLGVcJQdTg3U9pNO5MVq+jHnS3ccJOkKWW59enXTraSiCaUIkU31PpbFbqBq6G6L4BYNUZh55mjVx+eZpU4TMRz4PXc+21CmaTyedpo7IHfLp/vTN+lNC6Wqya/Wq4HBwJ07vWZZkObXUTEa6furpnq86t43x3MpzqOEOLHjMMc/M15WkZNUCu7MaMXpO0MBAjGrXE5/WnF+9NFgACKbkRPks939GUMH5zjD7JVLO/bqK2+OXMIAASnBRjo0NevwBlz+VJhGoFsKAJzhnN+xn8OAk='))
  console.log(Aes.decrypt('MDAwMTY0NTQ0NTEyMTcwN7BEKBitso1RUr8NkziaDKLxurXEgIwSiDnIFD3/UjuVC/kxnRwX+D+txsOQbiRXbMK8GcPRzwYxpO5qN0dK2ywzwJI3xA4LBBAGar/w6TehvMvTReMUrMmE1lgyJY/LGip4f2EFrLV6JFY/VGS475kFc0N+OW8Y8BIOdVONuX84vxiS7OCWKJo2eAz/aLp+7u/SIyai7YcQ4FW5889dEOr5qhkqqmk3iWA89Ef+4dYfutf8jUsUq7RQ0oktyfAet6NsDfWVxFM4YV82Y4loXHTowlkYB0jIEpgRBoORFQ5m8YCYBlXU6dg9X7VH2p9B21vAp8MUzv5K0WkD6RE8W7gg+ViEz5DynJDqbkjReMpJ'))

  console.log(JSON.stringify(['动态 IV 加密 Aes.encrypt', Aes.encrypt('测试 js 加密')]))
  console.log(JSON.stringify(['动态 IV 解密 Aes.decrypt', Aes.decrypt('MDAwMTY0NTQ0NDYyODY0OQOK1jx9D1vA+fy4VW7f2e8od4X3klGeQx67JB/uiIMC')]))
  console.log(JSON.stringify(['动态 IV 解密 java 加密数据', Aes.decrypt('MjAyMjAyMjEwMmxsdmNVV/Dmre0Gf0fTXDb9DCxOhWM=')]))

  Aes.setIV('2022010100112299')
  console.log(JSON.stringify(['静态 IV 加密 Aes.encrypt', Aes.encrypt('测试 js 加密')]))
  console.log(JSON.stringify(['静态 IV 解密 Aes.decrypt', Aes.decrypt('/IcOIjPiIvm3oKbNISzwdonBPfcHE4vxUiP22KTXdlU=')]))
  console.log(JSON.stringify(['静态 IV 解密 java 加密数据', Aes.decrypt('QR+MoBavrFsTrzFngtx6Vg==')]))
})

gulp.task('axios', async() => {
  const {data} = await axios.get('http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/index.html')
  console.log(data)
})

/**
 * mysql sql 执行
 * @param connection
 * @param sql
 * @return {Promise<*>}
 */
const query = (connection, sql) => new Promise((resolve, reject) => {
  console.info(sql)
  connection.query(sql, (err, result) => {
    if (err) {
      console.error('[ERROR] - ', err.message)
      reject(err)
    } else {
      resolve(result)
    }
  })
})
gulp.task('db:java:code', async() => {
  const templates = [ // 模板参考
    ['tab_code_auth_id', 'AuthId.js'],
    ['tab_code_search_id', 'AuthSearchId.js'],
    ['tab_code_open_id', 'OpenId.js'],
    ['tab_code_open_search_id', 'OpenSearchId.js']
  ]
  templates.forEach(([tableName, templateName]) => {
    console.log({table: [tableName], template: templateName})
  })

  const author = '谢长春'
  const host = 'localhost'
  const port = '3306'
  const user = 'root'
  const password = '111111'
  const database = 'app_demo_dev_db' // 数据库名
  const table = [ // 表名
    // tableName
    'tab_code_auth_id'
  ] // 表名
  const module = '../app-code/service' // 模块目录
  const pkg = 'com.ccx.demo' // 包名(也会作为文件输出目录)
  const model = 'JPA' // 模板引擎模式，可选值：[JPA, SQL]
  const template = 'AuthId.js' // 代码模板，表中包含以下字段：【createUserId,updateUserId,deleted】
  // const template = 'AuthSearchId.js' // 代码模板，表中包含以下字段：【createUserId,updateUserId,deleted】
  // const template = 'OpenId.js' // 代码模板，表中包含以下字段：【deleted】
  // const template = 'OpenSearchId.js' // 代码模板，表中包含以下字段：【deleted】
  // const template = templateName // 代码模板

  const mysql = require('mysql')
  console.log([{database, host, user, password, port, table, template, pkg}])
  const connection = mysql.createConnection({database, host, user, password, port})
  connection.connect()
  // *******************************************************************************************************************
  // for (const [tableName, template] of templates) {
  //   console.log({table: [tableName], template})
  //   const table = [tableName]

  const tables = await query(connection, `SHOW TABLE STATUS FROM ${database} where Name in ('${table.join('\',\'')}')`)
  console.table(tables)
  for (let i = 0, len = tables.length; i < len; i++) {
    const table = new Table(tables[i], model)
    const columns = await query(connection, `SHOW FULL COLUMNS FROM ${table.name}`)
    console.table(columns)
    table
      .setAuthor(author)
      .setPkg(pkg)
      .setColumns(columns)
      .setOutput(module)
    await table.writeHttp(template.replace(/\.js/, ''))
    await table.writeController(template.replace(/\.js/, ''))
    await table.writeEntity(template.replace(/\.js/, ''))
    await table.writeService(template.replace(/\.js/, ''))
    await table.writeRepository(template.replace(/\.js/, ''))
    await table.writeJsEntity(template.replace(/\.js/, ''))
    await table.writeJsService(template.replace(/\.js/, ''))
    await table.writeJsServiceTest(template.replace(/\.js/, ''))
  }
  // }
  // *******************************************************************************************************************
  connection.end()
})

gulp.task('mysql:read:write', async() => {
  // # 查看所有表定义参数，where name = '指定表名'
  // SHOW TABLE STATUS FROM app_demo_dev_db;
  // # 查看 DDL
  // SHOW CREATE TABLE tab_code_example;
  // # 表全量信息
  // SHOW FULL COLUMNS FROM tab_code_example;
  // # 表部分信息
  // DESCRIBE tab_code_example;
  // # 查看表数据行数
  // SELECT TABLE_NAME, TABLE_ROWS FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_SCHEMA = 'app_demo_dev_db'
  // SELECT * FROM information_schema.PROCESSLIST WHERE db = 'app_demo_dev_db' ORDER BY TIME DESC;
  // SELECT t.trx_mysql_thread_id,t.* FROM information_schema.INNODB_TRX t;
  // SELECT t.THREAD_ID,t.* FROM performance_schema.threads t WHERE PROCESSLIST_ID = ${trx_mysql_thread_id};
  // SELECT * FROM performance_schema.events_statements_current WHERE THREAD_ID = ${THREAD_ID};
  const mysql = require('mysql')
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '111111',
    database: 'app_demo_dev_db'
  })
  connection.connect()
  connection.query('SELECT * FROM tab_user;',
    (err, result) => {
      if (err) {
        console.error('[SELECT ERROR] - ', err.message)
        return
      }
      const wb = xlsx.utils.book_new()
      xlsx.utils.book_append_sheet(wb, xlsx.utils.json_to_sheet(result))
      xlsx.writeFile(wb, './.temp/tab_name.xlsx')
      console.table(result)
    })
  connection.end()
})
gulp.task('mysql:markdown', async() => {
  const mysql = require('mysql')
  const db = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '111111',
    database: 'app_demo_dev_db'
  }
  const paths = Paths.resolve('.temp/md')
  paths.rm(true)
  paths.mkdirs()
  const connection = mysql.createConnection(db)
  connection.connect()
  const tables = await query(connection, `SHOW TABLE STATUS FROM ${db.database} WHERE engine IS NOT NULL;`)
    .then(async result => {
      // console.table(result);
      return result.map(({Name, Engine, Collation, Comment}) =>
        Object.assign({name: Name, engine: Engine, collation: Collation, comment: Comment}))
    })
  console.table(tables)

  const excludeTableNames = ['databasechangelog', 'databasechangeloglock', 'tab_code_convert', 'tab_code_example'
    , 'tab_code_auth_id', 'tab_code_open_id', 'tab_code_open_search_id', 'tab_code_search_id', '']
  const writePath = path.resolve(`.temp/md/${db.database}.md`)
  const writer = fs.createWriteStream(writePath)
  writer.write(`# ${db.database} 数据库说明文档

### 目录
|表名|说明|
|----|----|
`)
  for (let i = 0, len = tables.length; i < len; i++) {
    const {name, engine, collation, comment} = tables[i]
    if (excludeTableNames.includes(name.toLowerCase())) {
      continue
    }
    writer.write(`|[${name}](#${name})|${comment || '-'}|\n`)
  }
  writer.write('\n')
  for (let i = 0, len = tables.length; i < len; i++) {
    const {name, engine, collation, comment} = tables[i]
    if (excludeTableNames.includes(name.toLowerCase())) {
      continue
    }
    writer.write(`<a id="${name}" name="${name}"></a>\n`)
    writer.write(`#### ${comment || '-'}\n`)
    writer.write(`${'select * from'} ${name};\n\n`)
    { // markdown 字段表格部分
      writer.write('|序号|字段|类型|允许空|默认值|描述|\n')
      writer.write('|:----:|----|----|----|----|----|\n')
      const columns = await query(connection, `SHOW FULL COLUMNS FROM ${name}`)
      columns.forEach(({Field, Type, Collation, Null, Default, Comment}, index) =>
        writer.write(`|${index + 1}|${Field}|${Type}|${Null}|${Default || '-'}|${Comment || '-'}|\n`))
      // console.log(columns);
    }
    writer.write('\n\n\n')
    // { // DDL 部分
    //     writer.write('```mysql\n');
    //     const [{'Create Table': ddl}] = await query(connection, `SHOW CREATE TABLE ${name}`);
    //     // console.log([tableName, ddl]);
    //     writer.write(ddl);
    //     writer.write('\n```');
    // }
  }
  writer.end()
  connection.end()
  console.log(writePath)
})
gulp.task('mysql:ddl', async() => {
  const mysql = require('mysql')
  const db = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '111111',
    database: 'app_demo_dev_db'
  }
  const paths = Paths.resolve('.temp/db')
  paths.rm(true)
  paths.mkdirs()
  const connection = mysql.createConnection(db)
  connection.connect()
  const tables = await query(connection, `SHOW TABLE STATUS FROM ${db.database} WHERE engine IS NOT NULL;`)
    .then(async result => {
      // console.table(result);
      return result.map(({Name, Engine, Collation, Comment}) =>
        Object.assign({name: Name, engine: Engine, collation: Collation, comment: Comment}))
    })
  console.table(tables)
  const excludeTableNames = ['databasechangelog', 'databasechangeloglock', 'tab_code_convert', 'tab_code_example'
    , 'tab_code_auth_id', 'tab_code_open_id', 'tab_code_open_search_id', 'tab_code_search_id', '']
  const yyyyMMdd = Dates.now().format_yyyyMMdd()
  for (let i = 0, len = tables.length; i < len; i++) {
    const {name, engine, collation, comment} = tables[i]
    if (excludeTableNames.includes(name.toLowerCase())) {
      continue
    }
    const writePath = path.resolve(`.temp/db/${name}.xml`)
    const writer = fs.createWriteStream(writePath)
    const [{'Create Table': ddl}] = await query(connection, `SHOW CREATE TABLE ${name}`)
    writer.write(`<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd">
    <changeSet id="create" author="谢长春">
        <preConditions onFail="MARK_RAN">
            <not>
                <tableExists tableName="${name}"/> <!-- 检查表是否已经存在，存在则不执行 -->
            </not>
        </preConditions>
        <comment>${yyyyMMdd}：建表：${comment}</comment>
        <sql stripComments="true">
            ${ddl.replace(/`/g, '').replace(/default null/gi, 'NULL')}
        </sql>
    </changeSet>
</databaseChangeLog>
`)
    // console.log([tableName, ddl]);
    // writer.write(ddl.replace(/`/g, '').replace(/default null/gi, 'NULL'));
    writer.end()
    console.log(writePath)
  }
  connection.end()
})
gulp.task('mysql:column:csv', async() => {
  const mysql = require('mysql')
  const db =
    {
      host: 'localhost',
      port: '3306',
      user: 'root',
      password: '111111',
      database: 'app_demo_dev_db'
    }
  // db = {host: 'localhost', port: '3306', user: 'app_demo_dev_db_admin', password: 'app_demo_dev_db@admin', database: 'app_fd_rs_dev_db'}
  // db = {host: 'localhost', port: '3306', user: 'app_demo_dev_db_admin', password: 'app_demo_dev_db@admin', database: 'app_fd_rs_sit_db'}
  // db = {host: 'localhost', port: '3306', user: 'app_demo_dev_db_admin', password: 'app_demo_dev_db@admin', database: 'app_fd_rs_uat_db'}
  const connection = mysql.createConnection(db)
  connection.connect()
  const writer = fs.createWriteStream(path.resolve(`.temp/${db.database}.csv`))
  writer.write(`# SHOW TABLE STATUS FROM ${db.database} WHERE engine IS NOT NULL;\n`)
  const tables = await query(connection, `SHOW TABLE STATUS FROM ${db.database} WHERE engine IS NOT NULL;`)
    .then(async result => {
      // console.table(result)
      result.forEach(row => {
        writer.write(`${row.Name},${row.Engine},${row.Version},${row.Row_format},${row.Collation},${row.Create_options},${row.Comment}\n`)
      })
      return result.map(({Name, Engine, Collation, Comment}) =>
        Object.assign({name: Name, engine: Engine, collation: Collation, comment: Comment}))
    })
  console.table(tables)
  writer.write('\n')
  for (let i = 0, len = tables.length; i < len; i++) {
    const {name, engine, collation, comment} = tables[i]
    writer.write(`\n# ${name}\n`)
    const columns = lodash.sortBy(await query(connection, `SHOW FULL COLUMNS FROM  ${name}`), 'Field')
    writer.write('# ' + columns.map(col => col.Field).join(',') + '\n')
    columns.forEach(row =>
      writer.write(lodash.values(row).map(val => `${val}`.trim().replace(' unsigned', '')).join(',') + '\n')
    )
  }
  writer.end()
  connection.end()
})

gulp.task('listener', function() {
  watch('src/**/*.html', batch(function() {
    web.reload()
    gulp.start('build')
  }))
  // gulp.watch([
  //     'src/**/*.js'
  // ], function() {
  //     console.log('js');
  //     return gulp.src('src/**/*.js')
  //         .pipe(plugins.changed('dest'))
  //         .pipe(plugins.debug({title: 'scripts监听修改文件:'}))
  //         .pipe(gulp.dest('dest'))
  //         .pipe(reload({stream: true}));
  // });
  // gulp.watch('src/**/*.css', function() {
  //     console.log('css');
  //     return gulp.src('src/**/*.css')
  //         .pipe(plugins.changed('dest'))
  //         .pipe(plugins.debug({title: 'css监听修改文件:'}))
  //         .pipe(gulp.dest('dest'))
  //         .pipe(web.stream());
  // });
  return Promise.resolve()
})
gulp.task('server', gulp.series('listener', () => {
  web.init({
    // proxy: 'http://localhost/index.html'
    server: {
      baseDir: 'src/',
      index: 'index.html'
    }
  })
}))

gulp.task('in:out', async() => {
  const filename = path.resolve('.temp/out.txt')
  const writer = fs.createWriteStream(filename)
  writer.on('close', () => console.log(filename))
  readline.createInterface(
    fs.createReadStream(path.resolve('.temp/in.txt')),
    writer
  )
    .on('line', line => {
      writer.write(line.concat(',\n'))
    })
    .on('close', () => writer.end())
})
gulp.task('write:line', async() => {
  const filename = path.resolve('.temp/test.log')
  const writer = fs.createWriteStream(filename)
  writer.on('close', () => console.log(filename))
  writer.write('测试\n')
  writer.write('测试\n')
  writer.write('测试\n')
  writer.end()
})
gulp.task('read:line', async() => {
  readline.createInterface(fs.createReadStream(path.resolve('.temp/test.log')))
    .on('line', text => {
      console.log(text)
    })
    .on('close', () => console.log('end'))
})

gulp.task('xlsx:read', async() => {
  const wb = xlsx.readFile(
    './temp/数据文件.xls'
  )
  const filename = '.temp/数据文件.sql'
  const writer = fs.createWriteStream(filename)
  writer.on('close', () => console.log(path.resolve(filename)))
  const sheet = wb.Sheets[wb.SheetNames.shift()]
  // const sheet = wb.Sheets['SQL Results']
  sheet['!ref'] = sheet['!ref'].replace(/\w+:(\w+)/, 'B1:$1')
  const arrs = xlsx.utils.sheet_to_json(sheet)
    .map(row => {
      lodash.forEach(row, (v, k) => {
        if (v === '') delete row[k]
      })
      writer.write(
        `${Object.values(row).join(',')}\n`
      )
      return row
    })

  writer.end()
})
gulp.task('xlsx:write', async() => {
  const {data: {errorInfos: array}} = JSON.parse('')
  array.sort((a, b) => a.rowNumber - b.rowNumber)
  const wb = xlsx.utils.book_new()
  xlsx.utils.book_append_sheet(wb, xlsx.utils.json_to_sheet(array), 'Sheet1')
  xlsx.writeFile(wb, './temp/write.xlsx')
})

gulp.task('exec', async() => {
  const proc = require('child_process')
  proc.exec('ls', function(error, stdout, stderr) {
    console.log(stdout)
    if (error !== null) {
      console.log('exec error: ' + error)
    }
  })
})

gulp.task('stdout', async() => {
  process.stdin.setEncoding('utf8')
  process.stdin.on('readable', () => {
    const chunk = process.stdin.read()
    if (chunk) {
      process.stdout.write(`data: ${chunk}`)
      process.exitCode = 1
    }
  })
  process.stdin.on('end', () => {
    process.stdout.write('end')
  })
})

gulp.task('randomstring:test', async() => {
  for (let i = 0; i < 100; i++) {
    console.log(randomstring.generate())
  }
})

function curl(url) {
  const proc = require('child_process')
  return new Promise((resolve, reject) => {
    proc.exec(`curl ${url} -H 'Content-Type: text/html; charset=utf-8' -H 'Accept-Encoding: gzip' | gunzip`, function(error, stdout, stderr) {
      if (error !== null) {
        console.error('exec error: ' + error)
        reject(new Error(`error: ${url}`))
      } else {
        resolve(stdout)
      }
    })
  })
}

gulp.task('城市代码', async() => {
  // 国家统计局：统计用区划和城乡划分代码： http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/
  // 2021 区划代码：  http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/index.html
  // 引入cheerio模块
  const cheerio = require('cheerio')
  const baseUrl = 'http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/'
  const body = await curl(`${baseUrl}index.html`)
  fs.writeFileSync('.temp/index.html', body)
  // console.log(body)

  const l1List = [] // {id: '110000', name: '北京市', url: 'http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/11.html'}
  {
    const nameMapping = {
      '内蒙古自治区': '内蒙古',
      '广西壮族自治区': '广西',
      '西藏自治区': '西藏',
      '宁夏回族自治区': '宁夏',
      '新疆维吾尔自治区': '新疆'
    }
    // 加载HTML字符串
    const $ = cheerio.load(body)
    $('a').each((idx, ele) => {
      const href = $(ele).attr('href')
      if (href && /\d+\.html/.test(href)) {
        const row = {
          id: href.replace(/^(\d+).*/, '$1').padEnd(6, '0'),
          pid: '',
          name: $(ele).text().replace(/市$/, ''),
          url: `${baseUrl}${href}`
        }
        if (nameMapping[row.name]) {
          row.name = nameMapping[row.name]
        }
        l1List.push(row)
        console.log(JSON.stringify(row))
      } else {
        console.error(`无效href：${href}`)
      }
    })
  }
  console.log(l1List.length)

  const l2List = []
  for (let i = 0; i < l1List.length; i++) {
    const {id, name, url} = l1List[i]
    console.log(`${id}(${name})：${url}`)
    if (fs.existsSync(`.temp/${id}_${name}.html`)) {
      continue
    }
    const body = await curl(url)
    fs.writeFileSync(`.temp/${id}_${name}.html`, body)
  }
  for (let i = 0; i < l1List.length; i++) {
    const {id, name, url} = l1List[i]
    console.log(`${id}(${name})：${url}`)
    // 加载HTML字符串
    const $ = cheerio.load(fs.readFileSync(`.temp/${id}_${name}.html`).toString())
    $('.citytable tr').each((idx, ele) => {
      const len = $(ele).find('a').length
      // console.log('len: ' + len)
      if (len !== 2) {
        return
      }
      const href = $(ele).find('a:eq(1)').attr('href')
      if (href && /\d+\.html/.test(href)) {
        const row = {
          id: href.replace(/^\d+\/(\d{4}).*/, '$1').padEnd(6, '0'),
          pid: href.replace(/^(\d{2}).*/, '$1').padEnd(6, '0'),
          name: $(ele).find('a:eq(1)').text(),
          url: `${baseUrl}${href}`
        }
        if ('市辖区' === row.name) {
          row.name = l1List.filter(l1 => l1.id === row.pid).map(l1 => l1.name) + '市'
        }
        l2List.push(row)
        console.log(JSON.stringify(row))
      } else {
        console.error(`无效href：${href}`)
      }
    })
  }
  console.log(l2List.length)

  const l3List = []
  for (let i = 0; i < l2List.length; i++) {
    const {id, name, url} = l2List[i]
    console.log(`${id}(${name})：${url}`)
    if (fs.existsSync(`.temp/${id}_${name}.html`)) {
      continue
    }
    const body = await curl(url)
    fs.writeFileSync(`.temp/${id}_${name}.html`, body)
  }
  for (let i = 0; i < l2List.length; i++) {
    const {id, name, url} = l2List[i]
    console.log(`${id}(${name})：${url}`)
    // 加载HTML字符串
    const $ = cheerio.load(fs.readFileSync(`.temp/${id}_${name}.html`).toString())
    $('.countytable tr').each((idx, ele) => {
      const len = $(ele).find('a').length
      // console.log('len: ' + len)
      if (len !== 2) {
        return
      }
      const href = $(ele).find('a:eq(1)').attr('href')
      if (href && /\d+\.html/.test(href)) {
        const row = {
          oid: href.replace(/^.*\/(\d+).html/, '$1'),
          id: href.replace(/^.*\/(\d{6}).*.html/, '$1').padEnd(6, '0'),
          pid: href.replace(/^.*\/(\d{4}).*.html/, '$1').padEnd(6, '0'),
          name: $(ele).find('a:eq(1)').text(),
          url: `${baseUrl}${href}`
        }
        l3List.push(row)
        console.log(JSON.stringify(row))
      } else {
        console.error(`无效href：${href}`)
      }
    })
  }
  const arrs = [
    {id: '4419', count: 0},
    {id: '4420', count: 0},
    {id: '4604', count: 0}
  ]
  l3List.forEach(row => {
    arrs.forEach(ele => {
      if (row.id.startsWith(ele.id)) {
        row.id = ele.id + lodash.padStart(`${ele.count += 1}`, 2, '0')
        console.log(JSON.stringify(row))
      }
    })
  })
  console.log(l3List.length)

  const names = {
    番禺区: {py: 'PYQ', pinyin: 'PanYuQu'},
    闵行区: {py: 'MHQ', pinyin: 'MinHangQu'}
  }
  const py = {
    省: 'sheng', 县: 'xian', 区: 'qu', 内: 'nei', 宁: 'ning', 疆: 'jiang', 南: 'nan', 广: 'guang', 重: 'chong', 藏: 'zang',
    石: 'shi', 家: 'jia', 大: 'da', 长: 'chang', 和: 'he', 沈: 'shen', 齐: 'qi', 地: 'di', 港: 'gang', 蚌: 'beng', 宿: 'su',
    六: 'liu', 厦: 'xia', 许: 'xu', 信: 'xin', 勒: 'le', 阿: 'a', 朝: 'chao', 合: 'he', 景: 'jing', 泽: 'ze', 行: 'xing',
    底: 'di', 佛: 'fo', 尾: 'wei', 揭: 'jie', 都: 'dou', 红: 'hong', 那: 'na', 番: 'fan', 什: 'shen', 氏: 'shi', 术: 'shu',
    甸: 'dian', 柏: 'bai', 万: 'wan', 塞: 'sai', 厂: 'chang', 强: 'qiang', 转: 'zhuang', 屯: 'tun', 休: 'xiu', 繁: 'fan',
    勃: 'bo', 扎: 'zha', 莫: 'mo', 盖: 'gai', 圈: 'quan', 调: 'diao', 绿: 'lv', 净: 'jing', 车: 'che', 食: 'shi', 乾: 'qian',
    敦: 'dun', 伯: 'bo', 汤: 'tang', 棱: 'ling', 孙: 'sun', 奇: 'qi', 栖: 'qi', 淳: 'chun', 枝: 'zhi', 乐: 'le', 泊: 'bo',
    综: 'zong', 会: 'hui', 贾: 'jia', 射: 'she', 句: 'ju', 叶: 'ye', 涡: 'wo', 思: 'si', 屏: 'ping', 彭: 'peng', 铅: 'qian',
    度: 'du', 莘: 'shen', 单: 'dan', 尉: 'wei', 体: 'ti', 解: 'jie', 召: 'zhao', 宛: 'wan', 泌: 'mi', 曾: 'zeng', 从: 'cong',
    澄: 'cheng', 涌: 'yong', 角: 'jiao', 的: 'de', 其: 'qi', 蔓: 'man', 羊: 'yang', 夹: 'jia', 渠: 'qu', 得: 'de', 觉: 'jue',
    腊: 'la', 卡: 'ka', 迦: 'jia', 丁: 'ding', 查: 'cha', 堡: 'bao', 碌: 'lu', 助: 'zhu', 称: 'cheng', 员: 'yuan', 且: 'qie',
    硕: 'shuo', 提: 'ti', 殷: 'yin', 若: 'ruo'
  }
  l1List.push({id: '710000', pid: '', name: '台湾省'})
  l1List.push({id: '810000', pid: '', name: '香港'})
  l1List.push({id: '820000', pid: '', name: '澳门'})
  const csv = [...l1List, ...l2List, ...l3List]
    .map(({id, pid, name}) => {
      if (names[name]) {
        const {py, pinyin} = names[name]
        return `${id},${name},${pid},${py},${pinyin}`
      }
      const pinyinConvert = name.split('').map(char => {
        if (py[char]) {
          return lodash.upperFirst(py[char])
        }
        const arrs = lodash.uniq(pinyin(char, {pattern: 'pinyin', type: 'array', toneType: 'none', multiple: true}))
        return lodash.upperFirst(arrs[0])
      }).join('')
      return `${id},${name},${pid},${pinyinConvert.replace(/[a-z]/g, '')},${pinyinConvert}`
    })
    .join('\n')
  fs.writeFileSync('.temp/tab_city.csv', 'id,name,pid,py,pinyin\n' + csv)
})
gulp.task('read:html', async() => {
  const cheerio = require('cheerio')
  const $ = cheerio.load('')
  $('table tr').each((rowIdx, ele) => {
    const columns = $(ele).find('td')
    if (columns.length === 0) return
    console.log(`/**
     * ${$(columns[0]).text()}
     * <pre>
     * 数据类型：${$(columns[3]).text()}
     * 示例值：${$(columns[4]).text()}
     * 是否必填：${$(columns[2]).text()}
     * ${$(columns[5]).text()}
     * </pre>
     */
    @JsonProperty("${$(columns[1]).text()}")
    private String ${$(columns[2]).text() === '是' ? 'required_' : ''}${$(columns[1]).text()};`)
  })
})

gulp.task('拼音', async() => {
  console.log(pinyin('长', {pattern: 'pinyin', type: 'array', toneType: 'none', multiple: true}))
  console.log(pinyin('长重', {pattern: 'pinyin', type: 'array', toneType: 'none', multiple: true}))
  console.log(pinyin('长', {pattern: 'first', type: 'array', toneType: 'none', multiple: true}))
  console.log(pinyin('长重', {pattern: 'first', type: 'array', toneType: 'none', multiple: true}))
  console.log(pinyin('长', {pattern: 'pinyin', toneType: 'none', multiple: true}))
  console.log(pinyin('长重', {pattern: 'pinyin', toneType: 'none', multiple: true}))
  console.log(pinyin('长', {pattern: 'first', toneType: 'none', multiple: true}))
  console.log(pinyin('长重', {pattern: 'first', toneType: 'none', multiple: true}))

  const values = '长重重长'.split('').map(char => {
    return pinyin(char, {pattern: 'pinyin', type: 'array', toneType: 'none', multiple: true})
  })
  console.log(JSON.stringify(values))
})
