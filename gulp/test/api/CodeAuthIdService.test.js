/**
 * 后台服务请求测试：测试 AuthId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
import random from 'lodash/random'
import sample from 'lodash/sample'
import sampleSize from 'lodash/sampleSize'
import CodeAuthId from '../../src/api/entity/CodeAuthId'
import CodeAuthIdService from '../../src/api/CodeAuthIdService'
import UserServiceTest, {sessionUser} from './UserService.test'
import OrderBy from '../../src/utils/entity/OrderBy'
import Page from '../../src/utils/entity/Page'

class CodeAuthIdServiceTest {

  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2);
    (await CodeAuthIdService.pageable(new CodeAuthId({page}))).print().testAssertData();
    (await CodeAuthIdService.pageable(new CodeAuthId({
      page,
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await CodeAuthIdService.pageable(new CodeAuthId({
      page,
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData()
    return this
  }

//
//  async list() {
//    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------');
//    (await CodeAuthIdService.list(new CodeAuthId({
//      createUserId: sessionUser.id
//    }))).print().testAssertData();
//    (await CodeAuthIdService.list(new CodeAuthId({
//      createUserId: sessionUser.id,
//      orderBy: [OrderBy.desc('id')]
//    }))).print().testAssertData();
//    return this
//  }
//

  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeAuthIdService.pageable()).testAssertData()
    const {id} = sample(data);
    (await CodeAuthIdService.findById(id))
      .print()
      .testAssertData()
    return this
  }

  async save() {
    console.log('> 新增 ----------------------------------------------------------------------------------------------------')
    const obj = new CodeAuthId({
      name: `${random(10000000)}`
    })
    for (let i = 0; i < 10; i++) {
      (await CodeAuthIdService.save(obj))
        .print()
        .testAssertCode()
    }
    return this
  }

  async update() {
    console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeAuthIdService.pageable(new CodeAuthId({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const row = Object.assign(sample(data), {});
    (await CodeAuthIdService.update(new CodeAuthId(row)))
      .print()
      .testAssertCode()
    return this
  }

  async markDeleteById() {
    console.log('> 按 dv 逻辑删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeAuthIdService.pageable(new CodeAuthId({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const {dv} = sample(data);
    (await CodeAuthIdService.markDeleteById(dv))
      .print()
      .testAssertCode()
    console.log('> 按 dv 批量逻辑删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).map(row => row.dv).filter(val => val !== dv);
    (await CodeAuthIdService.markDeleteByIds(dvs))
      .print()
      .testAssertCode()
    return this
  }

//
//  async deleteById() {
//    console.log('> 按 dv 物理删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
//    const {data} = (await CodeAuthIdService.pageable(new CodeAuthId({
//      createUserId: sessionUser.id
//    })))
//      .testAssertData();
//    const {dv} = sample(data);
//    (await CodeAuthIdService.deleteById(dv))
//      .print()
//      .testAssertCode();
//    console.log('> 按 dv 批量物理删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
//    const dvs = sampleSize(data, 2).filter(val=>val !== dv);
//    (await CodeAuthIdService.deleteByIds(dvs))
//      .print()
//      .testAssertCode();
//    return this
//  }
//

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '测试 AuthId 模板'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.update()).then(() => console.log(''))
      .then(() => this.markDeleteById()).then(() => console.log(''))
      // .then(() => this.deleteById()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      // .then(() => this.list()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}\n\n\n\n\n`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new CodeAuthIdServiceTest()
