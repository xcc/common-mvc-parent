/**
 * 测试：后台服务请求：通用模块
 * @author 谢长春 2020-03-10
 */
import fs from 'fs'
import UserServiceTest from './UserService.test'
import UploadService from '../../src/api/UploadService'

class UploadServiceTest {
  // async csrfToken() {
  //   console.log('> CSRF ----------------------------------------------------------------------------------------------------')
  //   console.log(await UploadService.csrfToken())
  // }

  async uploadTempImage() {
    console.log('> 上传图片到临时目录，单个上传；上传后的图片会等比缩放和裁剪 ----------------------------------------------------------------------------------------------------');
    (await UploadService.uploadTempImage(fs.createReadStream('../doc/vim.jpeg')))
      .print()
      .testAssertData()
  }

  async uploadTemp() {
    console.log('> 上传到临时目录，单个上传 ----------------------------------------------------------------------------------------------------');
    (await UploadService.uploadTemp(fs.createReadStream('../doc/vim.jpeg')))
      .print()
      .testAssertData()
  }

  async uploadTemps() {
    console.log('> 上传到临时目录，批量上传 ----------------------------------------------------------------------------------------------------');
    (await UploadService.uploadTemps([
      fs.createReadStream('../doc/vim.jpeg'),
      fs.createReadStream('./package.json'),
      fs.createReadStream('./.eslintrc.js')
    ]))
      .print()
      .testAssertData()
  }

  async uploadUserAvatar() {
    console.log('> 上传用户头像 ----------------------------------------------------------------------------------------------------');
    (await UploadService.uploadUserAvatar(fs.createReadStream('../doc/vim.jpeg')))
      .print()
      .testAssertData()
  }

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '通用模块'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.uploadTempImage()).then(() => console.log(''))
      .then(() => this.uploadTemp()).then(() => console.log(''))
      .then(() => this.uploadTemps()).then(() => console.log(''))
      .then(() => this.uploadUserAvatar()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new UploadServiceTest()
