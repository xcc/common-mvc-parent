/**
 * 测试：后台服务请求：角色
 * @author 谢长春 2019-8-30
 */
import sample from 'lodash/sample'
import Role from '../../src/api/entity/Role'
import UserServiceTest, {sessionUser} from './UserService.test'
import sampleSize from 'lodash/sampleSize'
import RoleService from '../../src/api/RoleService'
import Page from '../../src/utils/entity/Page'

class RoleServiceTest {
  async getAuthorityTree() {
    console.log('> 查询权限指令树 ----------------------------------------------------------------------------------------------------');
    (await RoleService.getAuthorityTree())
      .print()
      .testAssertData()
  }

  async getAuthorityList() {
    console.log('> 查询权限指令列表 ----------------------------------------------------------------------------------------------------');
    (await RoleService.getAuthorityList())
      .print()
      .testAssertData()
  }

  async save() {
    console.log('> 新增 ----------------------------------------------------------------------------------------------------')
    const {data: tree} = (await RoleService.getAuthorityTree())
      .testAssertCode()
      .testAssertData()
    tree[0].checked = true // 保证有一个选中
    const arrs = [
      {
        name: '随机部分权限',
        authorityTree: JSON.parse(JSON.stringify(tree)
          .replace(/false/, 'true') // 保证有一个选中
          .replace(/false/g, () => parseInt(`${Math.random()}`.slice(-1)) % 2 === 0 ? 'true' : 'false'))
      },
      {
        name: '全部权限',
        authorityTree: JSON.parse(JSON.stringify(tree).replace(/false/g, 'true'))
      }
    ]
    for (let i = 0, len = arrs.length; i < len; i++) {
      (await RoleService.save(new Role(arrs[i])))
        .print()
        .testAssertCode();
      (await RoleService.save(new Role(arrs[i])))
        .print()
        .testAssertCode()
    }
  }

  async update() {
    console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data: tree} = (await RoleService.getAuthorityTree()).testAssertCode().testAssertData()
    tree[0].checked = true // 保证有一个选中

    const {data} = (await RoleService.pageable(new Role({
      createUserId: sessionUser.id
    })))
      .print()
      .testAssertData()
    const row = Object.assign(sample(data), {
      name: '重命名角色',
      authorityTree: JSON.parse(JSON.stringify(tree).replace(/false/g,
        () => parseInt(`${Math.random()}`.slice(-1)) % 2 === 0 ? 'true' : 'false'
      ))
    });
    (await RoleService.update(new Role(row)))
      .print()
      .testAssertCode()
  }

  async markDeleteById() {
    console.log('> 按 dv 逻辑删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await RoleService.pageable(new Role({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const {dv} = sample(data);
    (await RoleService.markDeleteById(dv))
      .print()
      .testAssertCode()
    console.log('> 按 dv 批量逻辑删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).map(row => row.dv).filter(val => val !== dv);
    (await RoleService.markDeleteByIds(dvs))
      .print()
      .testAssertCode()
    return this
  }

  /**
   *
   * @return {Promise<RoleServiceTest>}
   */
  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2);
    (await RoleService.pageable(new Role({page})))
      .print()
      .testAssertData();
    (await RoleService.pageable(new Role({
      page,
      createUserId: sessionUser.id
    })))
      .print()
      .testAssertData();
    (await RoleService.pageable(new Role({
      page,
      deleted: false
    })))
      .print()
      .testAssertData()
  }

  async options() {
    console.log('> 角色下拉列表选项 ----------------------------------------------------------------------------------------------------');
    (await RoleService.options())
      .print()
      .testAssertData()
  }

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '角色'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.getAuthorityTree()).then(() => console.log(''))
      .then(() => this.getAuthorityList()).then(() => console.log(''))
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.update()).then(() => console.log(''))
      .then(() => this.markDeleteById()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      .then(() => this.options()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new RoleServiceTest()
