/**
 * 后台服务请求测试：异常日志记录
 * @author 谢长春 on 2022-02-22 V20221105
 */
import random from 'lodash/random'
import sample from 'lodash/sample'
import sampleSize from 'lodash/sampleSize'
import ErrorLog from '../../src/api/entity/ErrorLog'
import ErrorLogService from '../../src/api/ErrorLogService'
import UserServiceTest, {sessionUser} from './UserService.test'
import OrderBy from '../../src/utils/entity/OrderBy'
import Page from '../../src/utils/entity/Page'

class ErrorLogServiceTest {
  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2);
    (await ErrorLogService.pageable(new ErrorLog({page}))).print().testAssertData();
    (await ErrorLogService.pageable(new ErrorLog({
      page,
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await ErrorLogService.pageable(new ErrorLog({
      page,
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData()
    return this
  }

  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await ErrorLogService.pageable()).testAssertData()
    const {id} = sample(data);
    (await ErrorLogService.findById(id))
      .print()
      .testAssertCode()
    return this
  }

  async save() {
    console.log('> 新增 ----------------------------------------------------------------------------------------------------')
    for (let i = 0; i < 10; i++) {
      (await ErrorLogService.save(random(10000000), {
        id: random(10000000),
        message: random(10000000)
      }))
        .print()
        .testAssertCode()
    }
    return this
  }

  async markDeleteById() {
    console.log('> 按 dv 逻辑删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await ErrorLogService.pageable(new ErrorLog({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const {dv} = sample(data);
    (await ErrorLogService.markDeleteById(dv))
      .print()
      .testAssertCode()
    console.log('> 按 dv 批量逻辑删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).map(row => row.dv).filter(val => val !== dv);
    (await ErrorLogService.markDeleteByIds(dvs))
      .print()
      .testAssertCode()
    return this
  }

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '异常日志记录'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.markDeleteById()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}\n\n\n\n\n`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new ErrorLogServiceTest()
