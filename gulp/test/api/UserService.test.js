/**
 * 测试：后台服务请求：用户
 * @author 谢长春 2019-7-28
 */
import http from '../../src/api/http'
import sample from 'lodash/sample'
import sampleSize from 'lodash/sampleSize'
import isEmpty from 'lodash/isEmpty'
import User from '../../src/api/entity/User'
import UserService from '../../src/api/UserService'
import RoleService from '../../src/api/RoleService'
import UploadService from '../../src/api/UploadService'
import fs from 'fs'
import randomstring from 'randomstring'
import Page from '../../src/utils/entity/Page'
import OrderBy from '../../src/utils/entity/OrderBy'

/**
 * 当前登录用户
 * @type {User}
 */
export const sessionUser = {}

class UserServiceTest {
  /**
   * 获取当前登录用户信息
   * @return {User}
   */
  getSessionUser() {
    return sessionUser
  }

  async login() {
    console.log('> 登录：管理员(admin-test:admin) ----------------------------------------------------------------------------------------------------');
    (await UserService.login('admin-test', 'superadmin'))
      .print()
      .testAssertData();
    (await UserService.getCurrentUser()).dataFirst(user => {
      Object.keys(sessionUser).forEach(key => {
        delete sessionUser[key]
      })
      Object.assign(sessionUser, user)
    })
  }

  async loginAdmin() {
    console.log('> 登录：管理员(admin-test:admin) ----------------------------------------------------------------------------------------------------');
    (await UserService.login('admin-test', 'superadmin'))
      .print()
      .testAssertData();
    (await UserService.getCurrentUser()).dataFirst(user => {
      Object.keys(sessionUser).forEach(key => {
        delete sessionUser[key]
      })
      Object.assign(sessionUser, user)
    })
  }

  async loginUser() {
    console.log('> 登录：用户(user-test:111111) ----------------------------------------------------------------------------------------------------');
    (await UserService.login('user-test', '111111'))
      .print()
      .testAssertData();
    (await UserService.getCurrentUser()).dataFirst(user => {
      Object.keys(sessionUser).forEach(key => {
        delete sessionUser[key]
      })
      Object.assign(sessionUser, user)
    })
  }

  async logout() {
    console.log('> 退出 ----------------------------------------------------------------------------------------------------');
    (await UserService.logout())
      .print()
      .testAssertCode()
    http.defaults.auth = {}
    Object.keys(sessionUser).forEach(key => {
      delete sessionUser[key]
    })
  }

  async getCurrentUser() {
    console.log('> 获取当前登录用户信息 ----------------------------------------------------------------------------------------------------');
    (await UserService.getCurrentUser())
      .print()
      .testAssertData()
  }

  async updateNickname() {
    console.log('> 修改昵称 ----------------------------------------------------------------------------------------------------');
    (await UserService.updateNickname(`新昵称${randomstring.generate(8)}`))
      .print()
      .testAssertCode()
  }

  async save() {
    console.log('> 新增用户 ----------------------------------------------------------------------------------------------------')
    const {data: roles} = await RoleService.options()
    const roleIds = roles.map(row => row.id)

    for (let i = 0; i < 2; i++) {
      (await UserService.save(new User({
        username: `${new Date().getTime()}`,
        password: '111111',
        nickname: '随机单个角色',
        phone: '1' + randomstring.generate({
          length: 10,
          charset: '1234567890'
        }),
        domain: 'test',
        roles: [sample(roleIds)]
      })))
        .print()
        .testAssertCode();
      (await UserService.save(new User({
        username: `${new Date().getTime()}`,
        password: '111111',
        nickname: '随机多个角色',
        phone: '1' + randomstring.generate({
          length: 10,
          charset: '1234567890'
        }),
        domain: 'test',
        roles: sampleSize(roleIds, parseInt(`${Math.random()}`.slice(-1)) + 1)
      })))
        .print()
        .testAssertCode()
    }

    const {data: [avatar]} = (await UploadService.uploadUserAvatar(fs.createReadStream('../doc/vim.jpeg')));
    (await UserService.save(new User({
      username: `${new Date().getTime()}`,
      password: '111111',
      nickname: '随机单个角色',
      phone: '1' + randomstring.generate({
        length: 10,
        charset: '1234567890'
      }),
      domain: 'test',
      roles: [sample(roleIds)],
      avatar
    })))
      .print()
      .testAssertCode()
  }

  async update() {
    console.log('> 修改用户 ----------------------------------------------------------------------------------------------------')
    const {data: roles} = await RoleService.options()
    const roleIds = roles.map(row => row.id)
    const {data: users} = await UserService.pageable(new User({
      domain: 'test',
      createUserId: sessionUser.id
    }))
    const {data: [avatar]} = (await UploadService.uploadUserAvatar(fs.createReadStream('../doc/vim.jpeg')));
    (await UserService.update(new User(Object.assign(sample(users), {
      nickname: `编辑用户-${new Date().formatDate()}`,
      roles: sampleSize(roleIds, parseInt(`${Math.random()}`.slice(-1)) + 1),
      avatar
    }))))
      .print()
      .testAssertCode()
  }

  async markDeleteById() {
    console.log('> 按 dv 逻辑删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await UserService.pageable(new User({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const {dv} = sample(data);
    (await UserService.markDeleteById(dv))
      .print()
      .testAssertCode()
    console.log('> 按 dv 批量逻辑删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).map(row => row.dv).filter(val => val !== dv);
    (await UserService.markDeleteByIds(dvs))
      .print()
      .testAssertCode()
    return this
  }

  async disable() {
    console.log('> 按 dv 禁用账号：在查询结果集中随机选取 1 条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await UserService.pageable(new User({
      createUserId: sessionUser.id,
      disabled: false
    })))
    if (!isEmpty(data)) {
      (await UserService.disable(sample(data).dv))
        .print()
        .testAssertCode()
    }
  }

  async enable() {
    console.log('> 按 dv 启用账号：在查询结果集中随机选取 1 条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await UserService.pageable(new User({
      createUserId: sessionUser.id,
      disabled: true
    })))
    if (!isEmpty(data)) {
      (await UserService.enable(sample(data).dv))
        .print()
        .testAssertCode()
    } else {
      console.warn('未查询到已禁用的账号')
    }
  }

  async resetPassword() {
    console.log('> 按 dv 重置密码：在查询结果集中随机选取 1 条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await UserService.pageable(new User({
      createUserId: sessionUser.id,
      disabled: false
    })))
    if (!isEmpty(data)) {
      (await UserService.resetPassword(sample(data).dv))
        .print()
        .testAssertCode()
    }
  }

  async pageable() {
    console.log('> 分页查询用户 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2)
    const orderBy = [OrderBy.desc('id')];
    (await UserService.pageable(new User({page, orderBy})))
      .print()
      .testAssertCode()
      .testAssertData();
    (await UserService.pageable(new User({
      page,
      hidden: true,
      roles: ['5j3m2ypc1y6zA0p37N', '5j3m2ypc1y61GZGSOO']
    })))
      .print()
      .testAssertCode()
      .testAssertData()
  }

  // async getUsersFromRole() {
  //   console.log('> 按角色查询用户集合 ----------------------------------------------------------------------------------------------------');
  //   (await UserService.getUsersFromRole(new User({roleId: 1}))).print().testAssertCode().testAssertData();
  // }

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '用户'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // 开始
      .then(() => this.login()).then(() => console.log(''))
      .then(() => this.loginAdmin())
      .then(() => this.getCurrentUser()).then(() => console.log(''))
      .then(() => this.logout()).then(() => console.log(''))
      .then(() => this.loginUser())
      .then(() => this.getCurrentUser()).then(() => console.log(''))
      .then(() => this.updateNickname()).then(() => console.log(''))
      .then(() => this.logout()).then(() => console.log(''))
      .then(() => this.loginAdmin())
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      .then(() => this.update()).then(() => console.log(''))
      .then(() => this.markDeleteById()).then(() => console.log(''))
      .then(() => this.disable()).then(() => console.log(''))
      .then(() => this.enable()).then(() => console.log(''))
      .then(() => this.resetPassword()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new UserServiceTest()
