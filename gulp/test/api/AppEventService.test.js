/**
 * 后台服务请求测试：埋点表 V20221105
 * @author 谢长春 on 2022-06-27
 */
import AppEvent from '../../src/api/entity/AppEvent'
import AppEventService from '../../src/api/AppEventService'
import UserServiceTest, {sessionUser} from './UserService.test'
import OrderBy from '../../src/utils/entity/OrderBy'
import Page from '../../src/utils/entity/Page'

class AppEventServiceTest {
  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2);
    (await AppEventService.pageable(new AppEvent({page}))).print().testAssertData();
    (await AppEventService.pageable(new AppEvent({
      page,
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await AppEventService.pageable(new AppEvent({
      page,
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData()
    return this
  }

//
//  async list() {
//    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------');
//    (await AppEventService.list(new AppEvent({
//      createUserId: sessionUser.id
//    }))).print().testAssertData();
//    (await AppEventService.list(new AppEvent({
//      createUserId: sessionUser.id,
//      orderBy: [OrderBy.desc('id')]
//    }))).print().testAssertData();
//    return this
//  }
//
//
//   async findById() {
//     console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
//     const {data} = (await AppEventService.pageable()).testAssertData()
//     const {dv} = sample(data);
//     (await AppEventService.findById(id))
//       .print()
//       .testAssertCode()
//     return this
//   }

//   async save() {
//     console.log('> 新增 ----------------------------------------------------------------------------------------------------')
//     const obj = new AppEvent({
//       event: 0,
//       eventTime: `${random(10000000)}`,
// //  data: `${random(10000000)}`,
//       userId: 0,
//       refLongId: 0,
//       refStringId: `${random(10000000)}`
//     })
//     for (let i = 0; i < 10; i++) {
//       (await AppEventService.save(obj))
//         .print()
//         .testAssertData()
//     }
//     return this
//   }

  //
  // async update() {
  //   console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
  //   const {data} = (await AppEventService.pageable(new AppEvent({
  //     createUserId: sessionUser.id
  //   })))
  //     .testAssertData()
  //   const row = Object.assign(sample(data), {});
  //   (await AppEventService.update(new AppEvent(row)))
  //     .print()
  //     .testAssertCode()
  //   return this
  // }

//
//  async deleteById() {
//    console.log('> 按 dv 物理删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
//    const {data} = (await AppEventService.pageable(new AppEvent({
//      createUserId: sessionUser.id
//    })))
//      .testAssertData();
//    const {dv} = sample(data);
//    (await AppEventService.deleteById(dv))
//      .print()
//      .testAssertCode();
//    console.log('> 按 dv 批量物理删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
//    const dvs = sampleSize(data, 2).filter(val=>val !== dv);
//    (await AppEventService.deleteByIds(dvs))
//      .print()
//      .testAssertCode();
//    return this
//  }
//

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '埋点表'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      // .then(() => this.save()).then(() => console.log(''))
      // .then(() => this.update()).then(() => console.log(''))
      // .then(() => this.markDeleteById()).then(() => console.log(''))
      // .then(() => this.deleteById()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      // .then(() => this.list()).then(() => console.log(''))
      // .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}\n\n\n\n\n`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new AppEventServiceTest()
