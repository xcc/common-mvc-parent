/**
 * 后台服务请求测试：城市表 V20221105
 * @author 谢长春 on 2022-06-28
 */
import sample from 'lodash/sample'
import CityService from '../../src/api/CityService'
import UserServiceTest from './UserService.test'

class CityServiceTest {
  async listByPid() {
    console.log('> 按编码查询子节点，根节点传空字符串 ----------------------------------------------------------------------------------------------------');
    (await CityService.listByPid('')).print().testAssertData();
    (await CityService.listByPid('110000')).print().testAssertData()
    return this
  }

//
//  async list() {
//    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------');
//    (await CityService.list(new City({
//      createUserId: sessionUser.id
//    }))).print().testAssertData();
//    (await CityService.list(new City({
//      createUserId: sessionUser.id,
//      orderBy: [OrderBy.desc('id')]
//    }))).print().testAssertData();
//    return this
//  }
//

  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CityService.listByPid('')).testAssertData()
    const {id} = sample(data);
    (await CityService.findById(id))
      .print()
      .testAssertCode()
    return this
  }

  // async save() {
  //   console.log('> 新增 ----------------------------------------------------------------------------------------------------')
  //   const obj = new City({
  //     pid: `${random(10000000)}`,
  //     name: `${random(10000000)}`,
  //     py: `${random(10000000)}`,
  //     pinyin: `${random(10000000)}`
  //   })
  //   for (let i = 0; i < 10; i++) {
  //     (await CityService.save(obj))
  //       .print()
  //       .testAssertData()
  //   }
  //   return this
  // }
  //
  // async update() {
  //   console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
  //   const {data} = (await CityService.pageable(new City({
  //     createUserId: sessionUser.id
  //   })))
  //     .testAssertData()
  //   const row = Object.assign(sample(data), {});
  //   (await CityService.update(new City(row)))
  //     .print()
  //     .testAssertCode()
  //   return this
  // }

//
//  async deleteById() {
//    console.log('> 按 dv 物理删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
//    const {data} = (await CityService.pageable(new City({
//      createUserId: sessionUser.id
//    })))
//      .testAssertData();
//    const {dv} = sample(data);
//    (await CityService.deleteById(dv))
//      .print()
//      .testAssertCode();
//    console.log('> 按 dv 批量物理删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
//    const dvs = sampleSize(data, 2).filter(val=>val !== dv);
//    (await CityService.deleteByIds(dvs))
//      .print()
//      .testAssertCode();
//    return this
//  }
//

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '城市表'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      // .then(() => this.save()).then(() => console.log(''))
      // .then(() => this.update()).then(() => console.log(''))
      // .then(() => this.markDeleteById()).then(() => console.log(''))
      // .then(() => this.deleteById()).then(() => console.log(''))
      .then(() => this.listByPid()).then(() => console.log(''))
      // .then(() => this.list()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}\n\n\n\n\n`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new CityServiceTest()
