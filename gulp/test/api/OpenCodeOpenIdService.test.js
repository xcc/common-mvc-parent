/**
 * 后台服务请求测试：测试 OpenId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
import sample from 'lodash/sample'
import CodeOpenId from '../../src/api/entity/CodeOpenId'
import OpenCodeOpenIdService from '../../src/api/OpenCodeOpenIdService'
import OrderBy from '../../src/utils/entity/OrderBy'
import {sessionUser} from './UserService.test'
import Page from '../../src/utils/entity/Page'
import OpenCodeOpenSearchIdService from '../../src/api/OpenCodeOpenSearchIdService'
import CodeOpenSearchId from '../../src/api/entity/CodeOpenSearchId'

class OpenCodeOpenIdServiceTest {

  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2);
    (await OpenCodeOpenIdService.pageable(new CodeOpenId({page}))).print().testAssertData();
    (await OpenCodeOpenIdService.pageable(new CodeOpenId({
      page,
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await OpenCodeOpenIdService.pageable(new CodeOpenId({
      page,
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData()
    return this
  }

//
//  async list() {
//    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------');
//    (await OpenCodeOpenIdService.list(new CodeOpenId({
//      createUserId: sessionUser.id
//    }))).print().testAssertData();
//    (await OpenCodeOpenIdService.list(new CodeOpenId({
//      createUserId: sessionUser.id,
//      orderBy: [OrderBy.desc('id')]
//    }))).print().testAssertData();
//    return this
//  }
//

  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await OpenCodeOpenIdService.pageable()).testAssertData()
    const {id} = sample(data);
    (await OpenCodeOpenIdService.findById(id))
      .print()
      .testAssertData()
    return this
  }

  async save() {
    console.log('> 新增 ----------------------------------------------------------------------------------------------------');
    const {data} = (await OpenCodeOpenSearchIdService.pageable(new CodeOpenSearchId())).print().testAssertData();
    const {fromId, toId} = sample(data);
    const obj = new CodeOpenId({fromId, toId})
    for (let i = 0; i < 10; i++) {
      (await OpenCodeOpenIdService.save(obj))
        .print()
        .testAssertCode()
    }
    return this
  }

  async update() {
    console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await OpenCodeOpenIdService.pageable(new CodeOpenId({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const row = Object.assign(sample(data), {});
    (await OpenCodeOpenIdService.update(new CodeOpenId(row)))
      .print()
      .testAssertCode()
    return this
  }

//
//  async deleteById() {
//    console.log('> 按 dv 物理删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
//    const {data} = (await OpenCodeOpenIdService.pageable(new CodeOpenId({
//      createUserId: sessionUser.id
//    })))
//      .testAssertData();
//    const {dv} = sample(data);
//    (await OpenCodeOpenIdService.deleteById(dv))
//      .print()
//      .testAssertCode();
//    console.log('> 按 dv 批量物理删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
//    const dvs = sampleSize(data, 2).filter(val=>val !== dv);
//    (await OpenCodeOpenIdService.deleteByIds(dvs))
//      .print()
//      .testAssertCode();
//    return this
//  }
//

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '测试 OpenId 模板'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      // .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.update()).then(() => console.log(''))
      //.then(() => this.markDeleteById()).then(() => console.log(''))
      // .then(() => this.deleteById()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      // .then(() => this.list()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}\n\n\n\n\n`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new OpenCodeOpenIdServiceTest()
