/**
 * 后台服务请求测试：测试 SearchId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
import sample from 'lodash/sample'
import CodeSearchId from '../../src/api/entity/CodeSearchId'
import CodeSearchIdService from '../../src/api/CodeSearchIdService'
import UserServiceTest, {sessionUser} from './UserService.test'
import OrderBy from '../../src/utils/entity/OrderBy'
import Page from '../../src/utils/entity/Page'

class CodeSearchIdServiceTest {

  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const page = new Page(1, 2);
    (await CodeSearchIdService.pageable(new CodeSearchId({page}))).print().testAssertData();
    (await CodeSearchIdService.pageable(new CodeSearchId({
      page,
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await CodeSearchIdService.pageable(new CodeSearchId({
      page,
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData()
    return this
  }

//
//  async list() {
//    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------');
//    (await CodeSearchIdService.list(new CodeSearchId({
//      createUserId: sessionUser.id
//    }))).print().testAssertData();
//    (await CodeSearchIdService.list(new CodeSearchId({
//      createUserId: sessionUser.id,
//      orderBy: [OrderBy.desc('id')]
//    }))).print().testAssertData();
//    return this
//  }
//

  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeSearchIdService.pageable()).testAssertData()
    const {id} = sample(data);
    (await CodeSearchIdService.findById(id))
      .print()
      .testAssertData()
    return this
  }

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '测试 SearchId 模板'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.pageable()).then(() => console.log(''))
      // .then(() => this.list()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}\n\n\n\n\n`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new CodeSearchIdServiceTest()
