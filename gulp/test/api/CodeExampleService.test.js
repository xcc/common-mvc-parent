/**
 * 测试：后台服务请求：实体表操作
 * @author 谢长春 2019-7-28
 */
import random from 'lodash/random'
import sample from 'lodash/sample'
import sampleSize from 'lodash/sampleSize'
import CodeExample from '../../src/api/entity/CodeExample'
import UserServiceTest, {sessionUser} from './UserService.test'
import {DemoStatus} from '../../src/api/Enums'
import OrderBy from '../../src/utils/entity/OrderBy'
import {DateRange, NumRange} from '../../src/utils/entity/Range'
import CodeExampleService from '../../src/api/CodeExampleService'
import Page from '../../src/utils/entity/Page'

class CodeExampleServiceTest {
  async save() {
    console.log('> 新增 ----------------------------------------------------------------------------------------------------')
    const arrs = [
      {name: `name-${random(10000)}`},
      {name: `name-${random(10000)}`},
      {name: `name-${random(10000)}`},
      {name: `name-${random(10000)}`},
      {name: `name-${random(10000)}`},
      {
        name: `name-${random(10000)}`,
        content: `content-${random(1000000)}`
      },
      {
        name: `name-${random(10000)}`,
        content: `content-${random(1000000)}`,
        amount: random(10000.9).toFixed(2)
      },
      {
        name: `name-${random(10000)}`,
        content: `content-${random(1000000)}`,
        amount: random(10000.9).toFixed(2),
        status: sample(DemoStatus.options()).value
      }
    ]
    for (let i = 0, len = arrs.length; i < len; i++) {
      (await CodeExampleService.save(new CodeExample(arrs[i])))
        .print()
        .testAssertData()
    }
  }

  async update() {
    console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeExampleService.pageable())
      .print()
      .testAssertData()
    const row = Object.assign(sample(data), {
      amount: random(10000.9).toFixed(2),
      content: `content-${random(1000000)}`
    });
    (await CodeExampleService.update(new CodeExample(row)))
      .print()
      .testAssertCode()
  }

  async deleteById() {
    console.log('> 按 dv 删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeExampleService.pageable(new CodeExample({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const {dv} = sample(data);
    (await CodeExampleService.deleteById(dv))
      .print()
      .testAssertCode()
  }

  async markDeleteById() {
    console.log('> 按 dv 逻辑删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeExampleService.pageable(new CodeExample({
      createUserId: sessionUser.id
    })))
      .testAssertData()
    const {dv} = sample(data);
    (await CodeExampleService.markDeleteById(dv))
      .print()
      .testAssertCode()

    console.log('> 按 dv 批量逻辑删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).map(row => row.dv).filter(val => val !== dv);
    (await CodeExampleService.markDeleteByIds(dvs))
      .print()
      .testAssertCode()
    return this
  }

  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await CodeExampleService.pageable()).testAssertData()
    const {dv} = sample(data);
    (await CodeExampleService.findById(id))
      .print()
      .testAssertCode()
  }

  async list() {
    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------')
    const createTimeRange = new DateRange(new Date().addMonth(-1).formatDate(), new Date().formatDate());
    (await CodeExampleService.list(new CodeExample({
      createTimeRange
    })))
      .print()
      .testAssertData()
  }

  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const createTimeRange = new DateRange(new Date().addMonth(-1).formatDate(), new Date().formatDate())
    const amountRange = new NumRange(0, 9999)
    const orderBy = [OrderBy.desc('updateTime')]
    const page = new Page(1, 2);
    (await CodeExampleService.pageable(new CodeExample({page}))).print().testAssertData();
    (await CodeExampleService.pageable(new CodeExample({
      page,
      orderBy
    }))).print().testAssertData();
    (await CodeExampleService.pageable(new CodeExample({
      amountRange,
      page,
      orderBy
    }))).print().testAssertCode();
    (await CodeExampleService.pageable(new CodeExample({
      createTimeRange,
      amountRange,
      page,
      orderBy
    }))).print().testAssertCode();
    (await CodeExampleService.pageable(new CodeExample({
      createTimeRange,
      amountRange,
      page,
      orderBy: [OrderBy.desc('name')]
    }))).print().testAssertCode()
  }

  async listVO() {
    console.log('> VO 投影：多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------')
    const updateTimeRange = new DateRange(new Date().addMonth(-1).formatDate(), new Date().formatDate());
    (await CodeExampleService.listVO(new CodeExample({
      updateTimeRange
    })))
      .print()
      .testAssertData()
  }

  async pageableVO() {
    console.log('> VO 投影：分页：多条件批量查询 ----------------------------------------------------------------------------------------------------')
    const updateTimeRange = new DateRange(new Date().addMonth(-1).formatDate(), new Date().formatDate())
    const amountRange = new NumRange(0, 9999)
    const orderBy = [OrderBy.desc('updateTime')]
    const statusArray = [DemoStatus.SUCCESS.key, DemoStatus.RUNNING.key]
    const page = new Page(1, 2);
    (await CodeExampleService.pageableVO(new CodeExample({page}))).print().testAssertData();
    (await CodeExampleService.pageableVO(new CodeExample({
      page,
      orderBy
    }))).print().testAssertData();
    (await CodeExampleService.pageableVO(new CodeExample({
      page,
      amountRange,
      orderBy
    }))).print().testAssertCode();
    (await CodeExampleService.pageableVO(new CodeExample({
      page,
      updateTimeRange,
      amountRange,
      orderBy
    }))).print().testAssertCode();
    (await CodeExampleService.pageableVO(new CodeExample({
      page,
      statusArray
    }))).print().testAssertCode();
    (await CodeExampleService.pageableVO(new CodeExample({
      page,
      statusArray,
      updateTimeRange,
      amountRange,
      orderBy
    }))).print().testAssertCode()
  }

  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '实体表操作'
    console.info(`\n\n\n\n\n\n\n\n${moduleName}：start ${'*'.repeat(200)}\n${__filename}\n\n\n`)
    await Promise.resolve()
      // admin 登录
      .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.update()).then(() => console.log(''))
      .then(() => this.deleteById()).then(() => console.log(''))
      .then(() => this.markDeleteById()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      .then(() => this.list()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      .then(() => this.listVO()).then(() => console.log(''))
      .then(() => this.pageableVO()).then(() => console.log(''))
      // 结束
      .then(() => console.info(`${moduleName}：end ${'*'.repeat(200)}`))
      .catch((e) => {
        console.error(e)
        throw new Error(`${moduleName}：异常：${e.message}`)
      })
  }
}

export default new CodeExampleServiceTest()
