/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const markDeleteById = (table, auth = false, uninstall = false) => {
  const {comment, idType, existDeletedColumn, existUpdateTimeColumn, author} = table
  if (!existDeletedColumn) {
    return ''
  }
  const authUserId = auth ? ', user.getId()' : ''
  return `// <-
    @PatchMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(order = ${table.incrementAndGetOrdered()}, author = "${author}")
    @ResponseBody
    public Result<Void> markDeleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv
    ) {
        return new Result<Void>().call(() -> {
            service.markDeleteById(dv.get${idType}Id()${authUserId});
        });
    }
    
    @PatchMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(
            order = ${table.incrementAndGetOrdered()},
            author = "${author}",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDeleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDeleteByIds(
                    body.stream().map(DataVersion::get${idType}Id).collect(Collectors.toSet())
                    ${existUpdateTimeColumn ? '' : '//'}, body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const markDeleteByIdOpenInstall = table => markDeleteById(table)
export const markDeleteByIdOpenUninstall = table => markDeleteById(table, false, true)

export const markDeleteByIdAuthInstall = table => markDeleteById(table, true, false)
export const markDeleteByIdAuthUninstall = table => markDeleteById(table, true, true)
