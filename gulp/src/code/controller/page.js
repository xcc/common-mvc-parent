/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const page = (table, auth = false, uninstall = false) => {
  const {
    comment,
    date,
    existCreateUserIdColumn,
    existCreateTimeColumn,
    existUpdateUserIdColumn,
    existUpdateTimeColumn,
    existDeletedColumn,
    author,
    names: {TabName}
  } = table
  /**
   * @type {Column[]}
   */
  const queryIncludeColumns = table.getQueryIncludeColumns()
  queryIncludeColumns.map(col => col.toField())
  return `// <-
    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(order = ${table.incrementAndGetOrdered()}, author = "${author}")
    @ResponseBody
    public Result<${TabName}> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user, 
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
            ${queryIncludeColumns.map(col => col.toSwaggerApiParam()).filter(Boolean).join('\n')}
            ${existCreateUserIdColumn ? '' : '//'}@ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            ${existUpdateUserIdColumn ? '' : '//'}@ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            ${existDeletedColumn ? '' : '//'}@ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\\"name\\":\\"id\\",\\"direction\\":\\"ASC\\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<${TabName}>().execute(result -> { // Controller 方法逻辑写在这里
            final ${TabName} condition = new ${TabName}();
            ${queryIncludeColumns.map(col => col.toConditionParam()).filter(Boolean).join('\n')}
            ${existCreateUserIdColumn ? '' : '//'}condition.setCreateUserId(createUserId);
            ${existUpdateUserIdColumn ? '' : '//'}condition.setUpdateUserId(updateUserId);
            ${existDeletedColumn ? '' : '//'}condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.page(condition, Page.of(number, limit)));
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pageOpenInstall = table => page(table)
export const pageOpenUninstall = table => page(table, false, true)

export const pageAuthInstall = table => page(table, true, false)
export const pageAuthUninstall = table => page(table, true, true)
