/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findById = (table, auth = false, uninstall = false) => {
  const {comment, idType, date, author, names: {TabName}} = table
  return `// <-
    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(order = ${table.incrementAndGetOrdered()}, author = "${author}") 
    @ResponseBody
    public Result<${TabName}> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id", type = "string") @AesPathVariable${table.isIdString() ? '(clazz = String.class)' : ''} final ${idType} id
    ) {
        return new Result<${TabName}>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findByIdOpenInstall = table => findById(table)
export const findByIdOpenUninstall = table => findById(table, false, true)

export const findByIdAuthInstall = table => findById(table, true, false)
export const findByIdAuthUninstall = table => findById(table, true, true)
