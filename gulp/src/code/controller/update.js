/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const update = (table, auth = false, uninstall = false) => {
  const {comment, idType, existUpdateTimeColumn, author, names: {TabName}} = table
  const authUserId = auth ? ', user.getId()' : ''
  return `// <-
    @PutMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
    @ApiOperation(value = "修改${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(
            order = ${table.incrementAndGetOrdered()},
            author = "${author}",
            ignoreParameters = {
                    "${table.getUpdateIncludeColumnNames()}"
            })
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv,
            @RequestBody final ${TabName}UpdateDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            ${existUpdateTimeColumn ? 'body.setUpdateTime(dv.getUpdateTime());' : ''}
            service.update(dv.get${idType}Id()${authUserId}, body);
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const updateOpenInstall = table => update(table)
export const updateOpenUninstall = table => update(table, false, true)

export const updateAuthInstall = table => update(table, true, false)
export const updateAuthUninstall = table => update(table, true, true)
