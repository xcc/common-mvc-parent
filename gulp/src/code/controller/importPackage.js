/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param spare {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const importPackage = (table, auth = false, spare = false) => {
  const {pkg, names: {TabName, JavaName, pkgName}} = table
  return `
import ${pkg}.business.${pkgName}.entity.*;
import ${pkg}.business.${pkgName}.dto.*;
import ${pkg}.business.${pkgName}.service.*;
import app.common.starter.entity.*;
import app.common.starter.enums.*;
import app.encrypt.domain.starter.annotations.*;
import app.encrypt.domain.starter.model.*;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.*;
import com.github.xiaoymin.knife4j.annotations.*;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.*;
import java.util.*;
import java.util.stream.*;
import com.google.common.collect.*;
`
}
export const importPackageOpenInstall = table => importPackage(table)
export const importPackageOpenUninstall = table => importPackage(table, false, true)

export const importPackageAuthInstall = table => importPackage(table, true, false)
export const importPackageAuthUninstall = table => importPackage(table, true, true)
