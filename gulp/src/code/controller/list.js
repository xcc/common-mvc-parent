/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const list = (table, auth = false, uninstall = false) => {
  const {comment, date, author, names: {TabName}} = table
  return `// <-
// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
    @GetMapping
    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "列表查询${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(
            order = ${table.incrementAndGetOrdered()},
            author = "${author}"
    )
    @ResponseBody
    public Result<${TabName}> list(
        @ApiIgnore @AuthenticationPrincipal final IUser user
        @ApiParam(value = "排序；示例：?orderBy=[{\\"name\\":\\"id\\",\\"direction\\":\\"ASC\\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<${TabName}>().execute(result -> { // Controller 方法逻辑写在这里
            final ${TabName} condition = new ${TabName}(); 
            condition.setOrderBy(orderBy);
            result.setSuccess(service.list(condition));
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listOpenInstall = table => list(table)
export const listOpenUninstall = table => list(table, false, true)

export const listAuthInstall = table => list(table, true, false)
export const listAuthUninstall = table => list(table, true, true)
