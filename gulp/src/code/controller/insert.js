/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const insert = (table, auth = false, uninstall = false) => {
  const {comment, date, author, names: {TabName}} = table
  const authUserId = auth ? ', user.getId()' : ''
  return `// <-
    @PostMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "新增${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(
            order = ${table.incrementAndGetOrdered()},
            author = "${author}",
            includeParameters = {
                    "${table.getInsertIncludeColumnNames()}"
            })
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user, 
            @RequestBody final ${TabName}InsertDTO body
    ) {
        //return new Result<${TabName}>().execute(result -> { // Controller 方法逻辑写在这里
        //    result.setSuccess(service.insert(body${authUserId}));
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.insert(body${authUserId});
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const insertOpenInstall = table => insert(table)
export const insertOpenUninstall = table => insert(table, false, true)

export const insertAuthInstall = table => insert(table, true, false)
export const insertAuthUninstall = table => insert(table, true, true)
