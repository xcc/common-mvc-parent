/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const deleteById = (table, auth = false, uninstall = false) => {
  const {comment, idType, existUpdateTimeColumn, author} = table
  const authUserId = auth ? ', user.getId()' : ''
  return `// <-
    @DeleteMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "物理删除${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(order = ${table.incrementAndGetOrdered()}, author = "${author}")
    @ResponseBody
    public Result<Void> deleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv
    ) {
        return new Result<Void>().call(() -> {
            service.deleteById(dv.get${idType}Id()${authUserId});
        });
    }
    
    @DeleteMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "物理删除${comment}"
      , tags = {""}
      , notes = ""
    )
    @ApiOperationSupport(
            order = ${table.incrementAndGetOrdered()},
            author = "${author}",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> deleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.deleteByIds(
                    body.stream().map(DataVersion::get${idType}Id).collect(Collectors.toSet())
                    ${existUpdateTimeColumn ? '' : '//'}, body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const deleteByIdOpenInstall = table => deleteById(table)
export const deleteByIdOpenUninstall = table => deleteById(table, false, true)

export const deleteByIdAuthInstall = table => deleteById(table, true, false)
export const deleteByIdAuthUninstall = table => deleteById(table, true, true)
