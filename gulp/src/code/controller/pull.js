/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const pull = (table, auth = false, uninstall = false) => {
  const {
    comment,
    date,
    existCreateUserIdColumn,
    existCreateTimeColumn,
    existUpdateUserIdColumn,
    existUpdateTimeColumn,
    existDeletedColumn,
    author,
    names: {TabName}
  } = table
  /**
   * @type {Column[]}
   */
  const queryIncludeColumns = table.getQueryIncludeColumns()
  queryIncludeColumns.map(col => col.toField())
  return `// <-
    @GetMapping("/pull/{skip}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "${comment}瀑布流加载，按条件查询并跳过指定行数"
            , tags = {""}
            , notes = "瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）"
            + "<br>注意：该方法不会返回总数据行数，前端应该判断 rowCount != limit 时，表示瀑布流已经滚动到底部"
    )
    @ApiOperationSupport(order = ${table.incrementAndGetOrdered()}, author = "谢长春")
    @ResponseBody
    public Result<${TabName}> pull(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "跳过数据行数，示例：0", example = "1") @PathVariable final int skip,
            @ApiParam(required = true, value = "每次获取数据行数，最大值100", example = "100") @PathVariable final int limit,
            ${queryIncludeColumns.map(col => col.toSwaggerApiParam()).filter(Boolean).join('\n')}
            ${existCreateUserIdColumn ? '' : '//'}@ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            ${existUpdateUserIdColumn ? '' : '//'}@ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            ${existDeletedColumn ? '' : '//'}@ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\\"name\\":\\"id\\",\\"direction\\":\\"ASC\\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<${TabName}>().execute(result -> { // Controller 方法逻辑写在这里
             final ${TabName} condition = new ${TabName}();
            ${queryIncludeColumns.map(col => col.toConditionParam()).filter(Boolean).join('\n')}
            ${existCreateUserIdColumn ? '' : '//'}condition.setCreateUserId(createUserId);
            ${existUpdateUserIdColumn ? '' : '//'}condition.setUpdateUserId(updateUserId);
            ${existDeletedColumn ? '' : '//'}condition.setDeleted(deleted);
            result.setSuccess(service.pull(skip, limit, condition));
        });
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pullOpenInstall = table => pull(table)
export const pullOpenUninstall = table => pull(table, false, true)

export const pullAuthInstall = table => pull(table, true, false)
export const pullAuthUninstall = table => pull(table, true, true)
