/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const markDeleteById = (table, auth = false, uninstall = false) => {
  if (uninstall) {
    return ''
  }
  const {comment, existDeletedColumn, names: {java_name}} = table
  if (!existDeletedColumn) {
    return ''
  }
  return `### ${comment}: 按 dv 逻辑删除单条记录
PATCH {{domain}}${auth ? '' : '/open'}/${java_name}/{{dv}}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}

### ${comment}: 按 dv 批量逻辑删除
PATCH {{domain}}${auth ? '' : '/open'}/${java_name}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const markDeleteByIdOpenInstall = table => markDeleteById(table)
export const markDeleteByIdOpenUninstall = table => markDeleteById(table, false, true)

export const markDeleteByIdAuthInstall = table => markDeleteById(table, true, false)
export const markDeleteByIdAuthUninstall = table => markDeleteById(table, true, true)
