/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const page = (table, auth = false, uninstall = false) => {
  if (uninstall) {
    return ''
  }
  const {comment, names: {java_name}} = table
  return `### ${comment}: 分页查询
GET {{domain}}${auth ? '' : '/open'}/${java_name}/page/1/10?orderBy=[{"name":"id","direction":"DESC"}]
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const pageOpenInstall = table => page(table)
export const pageOpenUninstall = table => page(table, false, true)

export const pageAuthInstall = table => page(table, true, false)
export const pageAuthUninstall = table => page(table, true, true)
