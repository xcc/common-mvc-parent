/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
import DataType from '../core/DataType'

const update = (table, auth = false, uninstall = false) => {
  const {comment, columns, names: {java_name}} = table
  if (uninstall) {
    return ''
  }
  return `### ${comment}: 修改
# ${columns.map(col =>
    `"${col.name}":${col.dataType.java === DataType.VARCHAR.java ? '""' : null}`.padEnd(30, ' ') +
    `# ${col.comment}`
  ).join('\n# ')}
PUT {{domain}}${auth ? '' : '/open'}/${java_name}/{{dv}}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}

{

}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const updateOpenInstall = table => update(table)
export const updateOpenUninstall = table => update(table, false, true)

export const updateAuthInstall = table => update(table, true, false)
export const updateAuthUninstall = table => update(table, true, true)
