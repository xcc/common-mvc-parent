/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const deleteById = (table, auth = false, uninstall = false) => {
  if (uninstall) {
    return ''
  }
  const {comment, names: {java_name}} = table
  return `### ${comment}: 按 dv 物理删除
DELETE {{domain}}${auth ? '' : '/open'}/${java_name}/{{dv}}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const deleteByIdOpenInstall = table => deleteById(table)
export const deleteByIdOpenUninstall = table => deleteById(table, false, true)

export const deleteByIdAuthInstall = table => deleteById(table, true, false)
export const deleteByIdAuthUninstall = table => deleteById(table, true, true)
