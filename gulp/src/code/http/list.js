/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const list = (table, auth = false, uninstall = false) => {
  if (uninstall) {
    return ''
  }
  const {comment, names: {java_name}} = table
  return `### ${comment}: 列表查询，不分页
GET {{domain}}${auth ? '' : '/open'}/${java_name}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const listOpenInstall = table => list(table)
export const listOpenUninstall = table => list(table, false, true)

export const listAuthInstall = table => list(table, true, false)
export const listAuthUninstall = table => list(table, true, true)
