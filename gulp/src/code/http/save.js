/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
import DataType from '../core/DataType'

const save = (table, auth = false, uninstall = false) => {
  if (uninstall) {
    return ''
  }
  const {comment, names: {java_name}, columns} = table
  return `### ${comment}: 保存
# ${columns.map(col =>
    `"${col.name}":${col.dataType.java === DataType.VARCHAR.java ? '""' : null}`.padEnd(30, ' ') +
    `# ${col.comment}`
  ).join('\n# ')}
POST {{domain}}${auth ? '' : '/open'}/${java_name}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}

{

}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const saveOpenInstall = table => save(table)
export const saveOpenUninstall = table => save(table, false, true)

export const saveAuthInstall = table => save(table, true, false)
export const saveAuthUninstall = table => save(table, true, true)
