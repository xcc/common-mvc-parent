/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findById = (table, auth = false, uninstall = false) => {
  if (uninstall) {
    return ''
  }
  const {comment, names: {java_name}} = table
  return `### ${comment}: 按 id 查询单条记录
GET {{domain}}${auth ? '' : '/open'}/${java_name}/{{id}}
Content-Type: application/json
${auth ? 'x-token: {{token}}' : ''}
`.split('\n')
    .map(row => (uninstall ? '#' : '') + row)
    .join('\n')
}
export const findByIdOpenInstall = table => findById(table)
export const findByIdOpenUninstall = table => findById(table, false, true)

export const findByIdAuthInstall = table => findById(table, true, false)
export const findByIdAuthUninstall = table => findById(table, true, true)
