export {Entity, InsertDTO, UpdateDTO, JsEntity} from './AuthId'
/**
 *
 * @param table {Table}
 */
export const Http = async(table) => {
  const {author, comment, date, columns, existDeletedColumn} = table
  return `# **********************************************************************************************************************
# ${comment} V20221105
# @author ${author} on ${date} 
# 
# ${columns.map(col =>
    `"${col.name}":`.padEnd(30, ' ') +
    `# ${col.comment}`
  ).join('\n# ')}
# **********************************************************************************************************************
${(await import('./http/page')).pageOpenInstall(table)}
${(await import('./http/list')).listOpenUninstall(table)}
${(await import('./http/findById')).findByIdOpenInstall(table)}
${(await import('./http/save')).saveOpenInstall(table)}
${(await import('./http/update')).updateOpenInstall(table)}
${(await import('./http/deleteById')).deleteByIdOpenUninstall(table)}
${existDeletedColumn ? (await import('./http/markDeleteById')).markDeleteByIdOpenInstall(table) : ''}
`
}

/**
 *
 * @param table {Table}
 */
export const Controller = async(table) => {
  const {author, pkg, comment, date, names: {JavaName, java_name, pkgName}} = table
  return `package ${pkg}.business.${pkgName}.controller;

${(await import('./controller/importPackage')).importPackageOpenInstall(table)}

/**
 * 对外接口：${comment} V20221105
 *
 * @author ${author} on ${date} 
 */
@Api(tags = "${comment}")
//@ApiSort() // 控制接口排序// <-
@RequestMapping("/open/${java_name}")
@Controller
@Slf4j
@RequiredArgsConstructor
public class Open${JavaName}Controller {

    private final ${JavaName}Service service;

${(await import('./controller/page')).pageOpenInstall(table)}
${(await import('./controller/pull')).pullOpenUninstall(table)}
${(await import('./controller/list')).listOpenUninstall(table)}
${(await import('./controller/findById')).findByIdOpenInstall(table)}
${(await import('./controller/insert')).insertOpenInstall(table)}
${(await import('./controller/update')).updateOpenInstall(table)}
${(await import('./controller/markDeleteById')).markDeleteByIdOpenInstall(table)}
${(await import('./controller/deleteById')).deleteByIdOpenUninstall(table)}
}
`
}

/**
 *
 * @param table {Table}
 */
export const Service = async(table) => {
  const {
    author, pkg, comment, idType, date, existCreateUserIdColumn, existUpdateUserIdColumn, enableUserApiStarter,
    names: {TabName, tabName, JavaName, pkgName}
  } = table
  return `package ${pkg}.business.${pkgName}.service;
${(await import('./service/importPackage')).importPackageOpenInstall(table)}
/**
 * 服务接口实现类：${comment} V20221105// <-
 *
 * @author ${author} on ${date} 
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class ${JavaName}Service
//      , I${TabName}Cache
{
    private final ${JavaName}Repository repository;
    //private final ${TabName}Cache ${tabName}Cache;
    ${(existCreateUserIdColumn || existUpdateUserIdColumn) && enableUserApiStarter ? '' : '//'}private final IUserService userService;
    ${(existCreateUserIdColumn || existUpdateUserIdColumn) && !enableUserApiStarter ? '' : '//'}private final UserService userService;

${(await import('./service/insert')).insertOpenInstall(table)}
${(await import('./service/update')).updateOpenInstall(table)}
${(await import('./service/deleteById')).deleteByIdOpenUninstall(table)}
${(await import('./service/markDeleteById')).markDeleteByIdOpenInstall(table)}
${(await import('./service/fillUserNickname')).fillUserNicknameOpenInstall(table)}
${(await import('./service/findById')).findByIdOpenInstall(table)}
${(await import('./service/page')).pageOpenInstall(table)}
${(await import('./service/pull')).pullOpenUninstall(table)}
${(await import('./service/list')).listOpenUninstall(table)}
${(await import('./service/listByIds')).listByIdsOpenInstall(table)}
${(await import('./service/mapByIds')).mapByIdsOpenInstall(table)}
}
`.replace(/.*@param userId.*\n/g, '')
}

/**
 *
 * @param table {Table}
 */
export const Repository = async(table) => {
  const {
    author, pkg, comment, idType, date, isJpaModel, isSqlModel, modeNameLowerCase,
    names: {TabName, JavaName, tabName, pkgName}
  } = table
  return `package ${pkg}.business.${pkgName}.dao.${modeNameLowerCase};
${(await import('./repository/importPackage')).importPackageOpenInstall(table)}
/**
 * 数据操作：${comment} V20221105// <-
 *
 * @author ${author} on ${date} 
 */
${isSqlModel ? '@Repository' : ''}
${isSqlModel ? '@RequiredArgsConstructor' : ''}
public ${isJpaModel ? 'interface' : 'class'} ${JavaName}Repository 
        ${isJpaModel ? `extends        JpaRepository<${TabName}, ${idType}>,` : ''}
        ${isJpaModel ? `        org.springframework.data.querydsl.QuerydslPredicateExecutor<${TabName}>` : ''}
{
    ${isSqlModel ? '@Getter(AccessLevel.PRIVATE) private final SQLQueryFactory queryFactory;' : ''}
    
    // 每个 DAO 层顶部只能有一个查询实体,且必须以 table 命名,表示当前操作的数据库表. 当 table 作为主表的连接查询方法也必须写在这个类
    Q${TabName} table = Q${TabName}.${tabName};

${(await import('./repository/insert')).insertOpenInstall(table)}
${(await import('./repository/update')).updateOpenInstall(table)}
${(await import('./repository/deleteById')).deleteByIdOpenUninstall(table)}
${(await import('./repository/markDeleteById')).markDeleteByIdOpenInstall(table)}
${(await import('./repository/sql/findById')).findByIdOpenInstall(table)}
${(await import('./repository/sql/findAllById')).findAllByIdOpenInstall(table)}
${(await import('./repository/findOne')).findOneOpenInstall(table)}
${(await import('./repository/mapByIds')).mapByIdsOpenInstall(table)}
${(await import('./repository/count')).countOpenInstall(table)}
${(await import('./repository/pageIds')).pageIdsOpenInstall(table)}
${(await import('./repository/page')).pageOpenInstall(table)}
${(await import('./repository/listIds')).listIdsOpenInstall(table)}
${(await import('./repository/listByIds')).listByIdsOpenInstall(table)}
${(await import('./repository/list')).listOpenInstall(table)}
${(await import('./repository/forEach')).forEachOpenInstall(table)}
${(await import('./repository/pull')).pullOpenInstall(table)}
}
`.replace(/.*@param userId.*\n/g, '')
}

/**
 *
 * @param table {Table}
 */
export const JsService = async(table) => {
  const {author, comment, date, existDeletedColumn, names: {java_name, JavaName}} = table
  return `import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import ${JavaName} from './entity/${JavaName}'

const pageURL = '/open/${java_name}/page/{number}/{limit}' // 分页：多条件批量查询
// const listURL = '/open/${java_name}' // 多条件批量查询，不分页
const findByIdURL = '/open/${java_name}/{id}' // 按 id 查询单条记录
const saveURL = '/open/${java_name}' // 新增
const updateURL = '/open/${java_name}/{dv}' // 修改
// const deleteByIdURL = '/open/${java_name}/{dv}' // 按 dv 删除
${existDeletedColumn ? '' : '//'}const markDeleteByIdURL = '/open/${java_name}/{dv}' // 按 dv 逻辑删除
${existDeletedColumn ? '' : '//'}const markDeleteByIdsURL = '/open/${java_name}' // 按 dv 批量逻辑删除

/**
 * 后台服务请求：${comment} V20221105
 * @author ${author} on ${date} 
 */
export default class Open${JavaName}Service {
${(await import('./js-service/pageable')).pageableInstall(table)}
${(await import('./js-service/list')).listUninstall(table)}
${(await import('./js-service/findById')).findByIdInstall(table)}
${(await import('./js-service/save')).saveInstall(table)}
${(await import('./js-service/update')).updateInstall(table)}
${(await import('./js-service/markDeleteById')).markDeleteByIdInstall(table)}
${(await import('./js-service/deleteById')).deleteByIdUninstall(table)}
}
`
}

/**
 *
 * @param table {Table}
 */
export const JsServiceTest = async(table) => {
  const {author, comment, date, existDeletedColumn, names: {JavaName}} = table
  return `/**
 * 后台服务请求测试：${comment} V20221105// <-
 * @author ${author} on ${date} 
 */
import random from 'lodash/random'
import sample from 'lodash/sample'
import sampleSize from 'lodash/sampleSize'
import ${JavaName} from '../../src/api/entity/${JavaName}'
import Open${JavaName}Service from '../../src/api/Open${JavaName}Service'
import OrderBy from '../../src/utils/entity/OrderBy'
import {sessionUser} from './UserService.test'
import Page from '../../src/utils/entity/Page'

class Open${JavaName}ServiceTest {
  
${(await import('./js-service-test/pageable')).pageableInstall(table, 'Open')}
${(await import('./js-service-test/list')).listUninstall(table, 'Open')}
${(await import('./js-service-test/findById')).findByIdInstall(table, 'Open')}
${(await import('./js-service-test/save')).saveInstall(table, 'Open')}
${(await import('./js-service-test/update')).updateInstall(table, 'Open')}
${(await import('./js-service-test/markDeleteById')).markDeleteByIdInstall(table, 'Open')}
${(await import('./js-service-test/deleteById')).deleteByIdUninstall(table, 'Open')}
  
  /**
   * 测试全部
   * @return {Promise<void>}
   */
  async testAll() {
    const moduleName = '${comment}'
    console.info(\`\\n\\n\\n\\n\\n\\n\\n\\n\${moduleName}：start \${'*'.repeat(200)}\\n\${__filename}\\n\\n\\n\`)
    await Promise.resolve()
      // admin 登录
      // .then(() => UserServiceTest.loginAdmin())
      // 开始
      .then(() => this.save()).then(() => console.log(''))
      .then(() => this.update()).then(() => console.log(''))
      ${existDeletedColumn ? '' : '//'}.then(() => this.markDeleteById()).then(() => console.log(''))
      // .then(() => this.deleteById()).then(() => console.log(''))
      .then(() => this.pageable()).then(() => console.log(''))
      // .then(() => this.list()).then(() => console.log(''))
      .then(() => this.findById()).then(() => console.log(''))
      // 结束
      .then(() => console.info(\`\${moduleName}：end \${'*'.repeat(200)}\\n\\n\\n\\n\\n\`))
      .catch((e) => {
        console.error(e)
        throw new Error(\`\${moduleName}：异常：\${e.message}\`)
      })
  }
}
export default new Open${JavaName}ServiceTest()
`
}
