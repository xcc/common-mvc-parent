/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const update = (table, auth = false, uninstall = false) => {
  const {idType, comment, names: {TabName, tabName}} = table
  return `// <-
    /**
     * 更新 ${comment} ；  
     *
     * @param id     {@link ${idType}} 数据ID
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     * @param dto    {@link ${TabName}UpdateDTO} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(
            ${table.getIdValid()}
            ${table.getAuthUserValid(auth)} 
            ,@Valid @NotNull(message = "【dto】不能为null") final ${TabName}UpdateDTO dto) {
        final ${TabName} obj = new ${TabName}();
        BeanUtils.copyProperties(dto, obj);
        UpdateRowsException.asserts(repository.update(id${auth ? ', userId' : ''}, obj));
        // ${tabName}Cache.delete(id); // 若使用缓存需要解开代码
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const updateOpenInstall = table => update(table)
export const updateOpenUninstall = table => update(table, false, true)

export const updateAuthInstall = table => update(table, true, false)
export const updateAuthUninstall = table => update(table, true, true)
