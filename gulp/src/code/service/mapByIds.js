/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const mapByIds = (table, auth = false, uninstall = false) => {
  const {comment, idType, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<${idType}> {@link ${TabName}#getId()> 数据 id 集合
     * @return Map<${idType}, ${TabName}> {@link ${TabName}> 结果集合
     */
    public @NotNull(message = "返回值不能为null") Map<${idType}, ${TabName}> mapByIds(final Set<${idType}> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const mapByIdsOpenInstall = table => mapByIds(table)
export const mapByIdsOpenUninstall = table => mapByIds(table, false, true)

export const mapByIdsAuthInstall = table => mapByIds(table, true, false)
export const mapByIdsAuthUninstall = table => mapByIds(table, true, true)
