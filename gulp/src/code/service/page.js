/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const page = (table, auth = false, uninstall = false) => {
  const {comment, existCreateUserIdColumn, existUpdateUserIdColumn, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件分页查询列表
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param page     {@link Page} 分页排序集合
     * @return QueryResults<${TabName}> {@link ${TabName}} 分页对象
     */
    public @NotNull(message = "返回值不能为null") QueryResults<${TabName}> page(
            @NotNull(message = "【condition】不能为null") final ${TabName} condition, 
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<${TabName}> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        ${existCreateUserIdColumn || existUpdateUserIdColumn ? '' : '//'}this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pageOpenInstall = table => page(table)
export const pageOpenUninstall = table => page(table, false, true)

export const pageAuthInstall = table => page(table, true, false)
export const pageAuthUninstall = table => page(table, true, true)
