/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const importPackage = (table, auth = false, uninstall = false) => {
  const {
    pkg,
    modeNameLowerCase,
    existCreateUserIdColumn,
    existUpdateUserIdColumn,
    enableUserApiStarter,
    names: {TabName, JavaName, pkgName}
  } = table

  return `
import ${pkg}.business.${pkgName}.dao.${modeNameLowerCase}.*;
import ${pkg}.business.${pkgName}.entity.*;
import ${pkg}.business.${pkgName}.dto.*;
${(existCreateUserIdColumn || existUpdateUserIdColumn) && enableUserApiStarter ? '' : '//'}import ${pkg}.feign.user.vo.ApiUserVO;
${(existCreateUserIdColumn || existUpdateUserIdColumn) && enableUserApiStarter ? '' : '//'}import ${pkg}.feign.user.service.IUserService;
${(existCreateUserIdColumn || existUpdateUserIdColumn) && !enableUserApiStarter ? '' : '//'}import ${pkg}.business.user.entity.TabUser;
${(existCreateUserIdColumn || existUpdateUserIdColumn) && !enableUserApiStarter ? '' : '//'}import ${pkg}.business.user.service.UserService;
import app.common.starter.entity.Page;
import app.common.starter.exception.DeleteRowsException;
import app.common.starter.exception.UpdateRowsException;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;
import com.google.common.collect.Sets;
import com.google.common.base.Strings;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;
`
}
export const importPackageOpenInstall = table => importPackage(table)
export const importPackageOpenUninstall = table => importPackage(table, false, true)

export const importPackageAuthInstall = table => importPackage(table, true, false)
export const importPackageAuthUninstall = table => importPackage(table, true, true)
