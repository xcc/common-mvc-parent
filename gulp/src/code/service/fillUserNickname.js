/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const fillUserNickname = (table, auth = false, uninstall = false) => {
  const {comment, existCreateUserIdColumn, existUpdateUserIdColumn, enableUserApiStarter, names: {TabName}} = table
  if (!(existUpdateUserIdColumn || existUpdateUserIdColumn)) {
    return ''
  }
  return `// <-
    /**
     * 填充用户昵称
     *
     * @param list List<${TabName}>
     * @return List<${TabName}>
     */
    private <T extends ${TabName}> List<T> fillUserNickname(final List<T> list) {
        if (list.isEmpty()) {
            return list;
        }
        final Map<Long, ${enableUserApiStarter ? 'ApiUserVO' : 'TabUser'}> userMap = userService.mapByIds(list.stream()
                .flatMap(row -> Stream.of(
                ${existCreateUserIdColumn && existUpdateUserIdColumn ? 'row.getCreateUserId(), row.getUpdateUserId()' : existCreateUserIdColumn ? 'row.getCreateUserId()' : 'row.getUpdateUserId()'}
                ))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet())
        );
        list.forEach(row -> {
            ${existCreateUserIdColumn ? '' : '//'}Optional.ofNullable(userMap.get(row.getCreateUserId())).ifPresent(user -> row.setCreateUserNickname(user.getNickname()));
            ${existUpdateUserIdColumn ? '' : '//'}Optional.ofNullable(userMap.get(row.getUpdateUserId())).ifPresent(user -> row.setUpdateUserNickname(user.getNickname()));
        });
        return list;
    }
    /**
     * 填充用户昵称
     *
     * @param row ${TabName}
     * @return ${TabName}
     */
    private <T extends ${TabName}> T fillUserNickname(final T row) {
        if (Objects.isNull(row)) {
            return row;
        }
        final Map<Long, ${enableUserApiStarter ? 'ApiUserVO' : 'TabUser'}> userMap = userService.mapByIds(
                Stream.of(
                        ${existCreateUserIdColumn && existUpdateUserIdColumn ? 'row.getCreateUserId(), row.getUpdateUserId()' : existCreateUserIdColumn ? 'row.getCreateUserId()' : 'row.getUpdateUserId()'}
                        )
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet())
        );
        ${existCreateUserIdColumn ? '' : '//'}Optional.ofNullable(userMap.get(row.getCreateUserId())).ifPresent(user -> row.setCreateUserNickname(user.getNickname()));
        ${existUpdateUserIdColumn ? '' : '//'}Optional.ofNullable(userMap.get(row.getUpdateUserId())).ifPresent(user -> row.setUpdateUserNickname(user.getNickname()));
        return row;
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const fillUserNicknameOpenInstall = table => fillUserNickname(table)
export const fillUserNicknameOpenUninstall = table => fillUserNickname(table, false, true)

export const fillUserNicknameAuthInstall = table => fillUserNickname(table, true, false)
export const fillUserNicknameAuthUninstall = table => fillUserNickname(table, true, true)
