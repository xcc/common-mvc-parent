/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const insert = (table, auth = false, uninstall = false) => {
  const {comment, names: {TabName}} = table
  return `// <-
    /**
     * 新增 ${comment} 
     * 
     * @param dto    {@link ${TabName}InsertDTO} 实体对象
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     * @return {@link ${TabName}} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") ${TabName} insert(
            @Valid @NotNull(message = "【dto】不能为null") final ${TabName}InsertDTO dto
            ${table.getAuthUserValid(auth)}) {
        final ${TabName} obj = new ${TabName}();
        BeanUtils.copyProperties(dto, obj);
        return repository.insert(${auth ? 'userId, ' : ''}obj);
    }
//    /**
//     * 批量新增 ${comment}  
//     *
//     * @param list   List<${TabName}InsertDTO> {@link ${TabName}InsertDTO}  实体对象集合
//     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
//     * @return List<${TabName}> 实体对象集合
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public @NotNull(message = "返回值不能为null") List<${TabName}> insert(
//            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull ${TabName}InsertDTO> list
//            ${table.getAuthUserValid(auth)}) {
//        return repository.insert(${auth ? 'userId, ' : ''}list.stream()
//            .map(dto -> {
//                final ${TabName} obj = new ${TabName}();
//                BeanUtils.copyProperties(dto, obj);
//                return obj;
//            })
//            .collect(Collectors.toList())
//        );
//    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const insertOpenInstall = table => insert(table)
export const insertOpenUninstall = table => insert(table, false, true)

export const insertAuthInstall = table => insert(table, true, false)
export const insertAuthUninstall = table => insert(table, true, true)
