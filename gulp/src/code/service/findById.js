/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findById = (table, auth = false, uninstall = false) => {
  const {idType, comment, existCreateUserIdColumn, existUpdateUserIdColumn, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link ${idType}} 数据ID
     * @return Optional<${TabName}> {@link ${TabName}} 实体对象
     */
    public Optional<${TabName}> findById(final ${idType} id) {
        if (${table.isIdString() ? 'Strings.isNullOrEmpty(id)' : 'Objects.isNull(id) || id < 1'}) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(${TabName}::cloneObject) // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
                ${existCreateUserIdColumn || existUpdateUserIdColumn ? '.map(this::fillUserNickname)' : ''};
//         return Optional.ofNullable(repository.findCacheById(id)).map(${TabName}::cloneObject); // 若使用缓存需要解开代码
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findByIdOpenInstall = table => findById(table)
export const findByIdOpenUninstall = table => findById(table, false, true)

export const findByIdAuthInstall = table => findById(table, true, false)
export const findByIdAuthUninstall = table => findById(table, true, true)
