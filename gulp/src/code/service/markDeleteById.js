/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const markDeleteById = (table, auth = false, uninstall = false) => {
  const {idType, comment, existUpdateTimeColumn, existDeletedColumn, names: {TabName, tabName}} = table
  if (!existDeletedColumn) {
    return ''
  }
  const authUserId = auth ? ', userId' : ''
  return `// <-
    /**
     * ${comment} 按ID删除，逻辑删除
     *
     * @param id     {@link ${idType}} 数据ID
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            ${table.getIdValid()}
            ${table.getAuthUserValid(auth)}) {
        DeleteRowsException.asserts(repository.markDeleteById(id${authUserId}));
        // ${tabName}Cache.delete(id); // 若使用缓存需要解开代码
    }

${existUpdateTimeColumn ? '' : '//'}    /**
${existUpdateTimeColumn ? '' : '//'}     * ${comment} 按 id+updateTime 删除，逻辑删除
${existUpdateTimeColumn ? '' : '//'}     *
${existUpdateTimeColumn ? '' : '//'}     * @param id         {@link ${idType}} 数据ID
${existUpdateTimeColumn ? '' : '//'}     * @param updateTime {@link String} 最后一次更新时间
${existUpdateTimeColumn ? '' : '//'}     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
${existUpdateTimeColumn ? '' : '//'}     */
${existUpdateTimeColumn ? '' : '//'}    @Transactional(rollbackFor = Exception.class)
${existUpdateTimeColumn ? '' : '//'}    public void markDeleteById(
${existUpdateTimeColumn ? '' : '//'}            ${table.getIdValid()}
${existUpdateTimeColumn ? '' : '//'}            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
${existUpdateTimeColumn ? '' : '//'}            ${table.getAuthUserValid(auth)}
${existUpdateTimeColumn ? '' : '//'}    ) {
${existUpdateTimeColumn ? '' : '//'}        DeleteRowsException.asserts(repository.markDeleteById(id, updateTime${authUserId}));
${existUpdateTimeColumn ? '' : '//'}        // ${tabName}Cache.delete(id); // 若使用缓存需要解开代码
${existUpdateTimeColumn ? '' : '//'}    }

    /**
     * ${comment} 按ID删除，逻辑删除
     *
     * @param ids     Set<${idType}> {@link ${TabName}#getId()} 数据ID
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull ${idType}> ids
            ${table.getAuthUserValid(auth)}) {
        DeleteRowsException.asserts(repository.markDeleteByIds(ids${authUserId}), ids.size());
        // ${tabName}Cache.delete(ids); // 若使用缓存需要解开代码
    }

${existUpdateTimeColumn ? '' : '//'}    /**
${existUpdateTimeColumn ? '' : '//'}     * ${comment} 按 id+updateTime 删除，逻辑删除
${existUpdateTimeColumn ? '' : '//'}     *
${existUpdateTimeColumn ? '' : '//'}     * @param ids     Set<${idType}> {@link ${TabName}#getId()} 数据ID
${existUpdateTimeColumn ? '' : '//'}     * @param updateTimes     Set<String> 最后一次更新时间 
${existUpdateTimeColumn ? '' : '//'}     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
${existUpdateTimeColumn ? '' : '//'}     */
${existUpdateTimeColumn ? '' : '//'}    @Transactional(rollbackFor = Exception.class)
${existUpdateTimeColumn ? '' : '//'}    public void markDeleteByIds(
${existUpdateTimeColumn ? '' : '//'}            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull ${idType}> ids
${existUpdateTimeColumn ? '' : '//'}            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
${existUpdateTimeColumn ? '' : '//'}            ${table.getAuthUserValid(auth)}
${existUpdateTimeColumn ? '' : '//'}    ) {
${existUpdateTimeColumn ? '' : '//'}        DeleteRowsException.asserts(repository.markDeleteByIds(ids, updateTimes${authUserId}), ids.size());
${existUpdateTimeColumn ? '' : '//'}        // ${tabName}Cache.delete(ids); // 若使用缓存需要解开代码
${existUpdateTimeColumn ? '' : '//'}    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const markDeleteByIdOpenInstall = table => markDeleteById(table)
export const markDeleteByIdOpenUninstall = table => markDeleteById(table, false, true)

export const markDeleteByIdAuthInstall = table => markDeleteById(table, true, false)
export const markDeleteByIdAuthUninstall = table => markDeleteById(table, true, true)
