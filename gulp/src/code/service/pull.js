/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const pull = (table, auth = false, uninstall = false) => {
  const {comment, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件查询并跳过指定行数
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}} 列表查询结果集
     */
    public @NotNull(message = "返回值不能为null") List<${TabName}> pull(final int skip, final int limit, final ${TabName} condition) {
        List<${TabName}> list = repository.pull(skip, limit, condition);
        this.fillUserNickname(list);
        return list;
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pullOpenInstall = table => pull(table)
export const pullOpenUninstall = table => pull(table, false, true)

export const pullAuthInstall = table => pull(table, true, false)
export const pullAuthUninstall = table => pull(table, true, true)
