/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const listByIds = (table, auth = false, uninstall = false) => {
  const {comment, idType, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<${idType}> {@link ${TabName}#getId()> 数据 id 集合
     * @return List<${TabName}> {@link ${TabName}> 结果集合
     */
    public @NotNull(message = "返回值不能为null") List<${TabName}> listByIds(final Collection<${idType}> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listByIdsOpenInstall = table => listByIds(table)
export const listByIdsOpenUninstall = table => listByIds(table, false, true)

export const listByIdsAuthInstall = table => listByIds(table, true, false)
export const listByIdsAuthUninstall = table => listByIds(table, true, true)
