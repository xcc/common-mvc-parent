/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const list = (table, auth = false, uninstall = false) => {
  const {comment, names: {TabName}} = table
  return `// <-
// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
    /**
     * ${comment} 按条件查询列表
     *
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}> 结果集合
     */
    public @NotNull(message = "返回值不能为null") List<${TabName}> list(
            @NotNull(message = "【condition】不能为null") final ${TabName} condition) {
        return repository.list(condition);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listOpenInstall = table => list(table)
export const listOpenUninstall = table => list(table, false, true)

export const listAuthInstall = table => list(table, true, false)
export const listAuthUninstall = table => list(table, true, true)
