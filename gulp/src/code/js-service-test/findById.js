/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findById = (table, openServiceName = '', uninstall = false) => {
  const {names: {JavaName}} = table
  return `
  async findById() {
    console.log('> 按 id 查询单条记录 ----------------------------------------------------------------------------------------------------')
    const {data} = (await ${openServiceName}${JavaName}Service.pageable()).testAssertData();
    const {id} = sample(data);
    (await ${openServiceName}${JavaName}Service.findById(id))
      .print()
      .testAssertData();
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findByIdInstall = (table, openServiceName) => findById(table, openServiceName)
export const findByIdUninstall = (table, openServiceName) => findById(table, openServiceName, true)
