/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const markDeleteById = (table, openServiceName = '', uninstall = false) => {
  const {existDeletedColumn, names: {JavaName}} = table
  if (!existDeletedColumn) {
    return ''
  }
  return `
  async markDeleteById() {
    console.log('> 按 dv 逻辑删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await ${openServiceName}${JavaName}Service.pageable(new ${JavaName}({
      createUserId: sessionUser.id
    })))
      .testAssertData();
    const {dv} = sample(data);
    (await ${openServiceName}${JavaName}Service.markDeleteById(dv))
      .print()
      .testAssertCode();
    console.log('> 按 dv 批量逻辑删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).map(row => row.dv).filter(val=>val !== dv);
    (await ${openServiceName}${JavaName}Service.markDeleteByIds(dvs))
      .print()
      .testAssertCode();
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const markDeleteByIdInstall = (table, openServiceName) => markDeleteById(table, openServiceName)
export const markDeleteByIdUninstall = (table, openServiceName) => markDeleteById(table, openServiceName, true)
