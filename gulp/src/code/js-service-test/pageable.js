/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const pageable = (table, openServiceName = '', uninstall = false) => {
  const {names: {JavaName}} = table
  return `
  async pageable() {
    console.log('> 分页：多条件批量查询 ----------------------------------------------------------------------------------------------------');
    const page = new Page(1, 2);
    (await ${openServiceName}${JavaName}Service.pageable(new ${JavaName}({page}))).print().testAssertData();
    (await ${openServiceName}${JavaName}Service.pageable(new ${JavaName}({
      page,
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await ${openServiceName}${JavaName}Service.pageable(new ${JavaName}({
      page,
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData();
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pageableInstall = (table, openServiceName) => pageable(table, openServiceName)
export const pageableUninstall = (table, openServiceName) => pageable(table, openServiceName, true)
