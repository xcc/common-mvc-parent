/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const save = (table, openServiceName = '', uninstall = false) => {
  const {names: {JavaName}, columns, saveTestExcludeColumnNames} = table
  const saveArgs = columns
    .filter(({name}) => !saveTestExcludeColumnNames.includes(name))
    .map(({name, notNull, dataType, defaultValue}) => {
      switch (dataType.java) {
        case 'Byte':
          return `${notNull ? '' : '// '} ${name}: ${defaultValue || 'random(100)'}`
        case 'Short':
          return `${notNull ? '' : '// '} ${name}: ${defaultValue || 'random(65535)'}`
        case 'Integer':
        case 'Long':
          return `${notNull ? '' : '// '} ${name}: ${defaultValue || 'random(10000000)'}`
        case 'BigDecimal':
        case 'Double':
        case 'Float':
          return `${notNull ? '' : '// '} ${name}: ${defaultValue || 'Number(`${random(10000000)}.${random(99)}`)'}`
        case 'String':
          return `${notNull ? '' : '// '} ${name}: ${defaultValue || '`${random(10000000)}`'}`
        case 'Date':
          return `${notNull ? '' : '// '} ${name}: new Date()`
      }
      return `// ${name}: ${defaultValue || ''},`
    })
    .join(',\n')
  return `
  async save() {
    console.log('> 新增 ----------------------------------------------------------------------------------------------------')
    const obj = new ${JavaName}({
      ${saveArgs}
    })
    for (let i = 0; i < 10; i++) {
      (await ${openServiceName}${JavaName}Service.save(obj))
        .print()
        .testAssertCode()
    }
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const saveInstall = (table, openServiceName) => save(table, openServiceName)
export const saveUninstall = (table, openServiceName) => save(table, openServiceName, true)
