/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const update = (table, openServiceName = '', uninstall = false) => {
  const {names: {JavaName}} = table
  return `
  async update() {
    console.log('> 修改：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await ${openServiceName}${JavaName}Service.pageable(new ${JavaName}({
      createUserId: sessionUser.id
    })))
      .testAssertData();
    const row = Object.assign(sample(data), {});
    (await ${openServiceName}${JavaName}Service.update(new ${JavaName}(row)))
      .print()
      .testAssertCode();
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const updateInstall = (table, openServiceName) => update(table, openServiceName)
export const updateUninstall = (table, openServiceName) => update(table, openServiceName, true)
