/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const deleteById = (table, openServiceName = '', uninstall = false) => {
  const {names: {JavaName}} = table
  return `
  async deleteById() {
    console.log('> 按 dv 物理删除：在查询结果集中随机选取一条数据 ----------------------------------------------------------------------------------------------------')
    const {data} = (await ${openServiceName}${JavaName}Service.pageable(new ${JavaName}({
      createUserId: sessionUser.id
    })))
      .testAssertData();
    const {dv} = sample(data);
    (await ${openServiceName}${JavaName}Service.deleteById(dv))
      .print()
      .testAssertCode();
    console.log('> 按 dv 批量物理删除：在查询结果集中随机选取两条数据 ----------------------------------------------------------------------------------------------------')
    const dvs = sampleSize(data, 2).filter(val=>val !== dv);
    (await ${openServiceName}${JavaName}Service.deleteByIds(dvs))
      .print()
      .testAssertCode();
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const deleteByIdInstall = (table, openServiceName) => deleteById(table, openServiceName)
export const deleteByIdUninstall = (table, openServiceName) => deleteById(table, openServiceName, true)
