/**
 * @param table {Table}
 * @param openServiceName {string}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const list = (table, openServiceName = '', uninstall = false) => {
  const {names: {JavaName}} = table
  return `
  async list() {
    console.log('> 多条件批量查询，不分页 ----------------------------------------------------------------------------------------------------');
    (await ${openServiceName}${JavaName}Service.list(new ${JavaName}({
      createUserId: sessionUser.id
    }))).print().testAssertData();
    (await ${openServiceName}${JavaName}Service.list(new ${JavaName}({
      createUserId: sessionUser.id,
      orderBy: [OrderBy.desc('id')]
    }))).print().testAssertData();
    return this
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listInstall = (table, openServiceName) => list(table, openServiceName)
export const listUninstall = (table, openServiceName) => list(table, openServiceName, true)
