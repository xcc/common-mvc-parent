/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const count = (table, auth = false, uninstall = false) => {
  const {comment, isJpaModel} = table
  return `
    /**
     * ${comment} 求总数
     *
     * @param where QdslWhere 查询条件
     * @return long 总数
     */
    ${isJpaModel ? 'default' : 'public'} long count(final QdslWhere where) {
        return getQueryFactory()
                .select(table.id.count())
                .from(table)
                .where(where.toPredicate())
                .fetch()
                .stream()
                .findFirst()
                .orElse(0L);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const countOpenInstall = table => count(table)
export const countOpenUninstall = table => count(table, false, true)

export const countAuthInstall = table => count(table, true, false)
export const countAuthUninstall = table => count(table, true, true)
