/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const deleteById = (table, auth = false, uninstall = false) => {
  const {idType, comment, existUpdateTimeColumn, names: {TabName}} = table
  const authUser = auth ? ', final Long userId' : ''
  return `// <-
    /**
     * ${comment} 按 id 物理删除
     *
     * @param id     {@link ${idType}} 数据id
     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
     * @return {@link Long} 物理删除行数
     */
    default long deleteById(final ${idType} id${authUser}) {
        return getQueryFactory()
                .delete(table)
                .where(table.id.eq(id))
                .execute();
    }

${existUpdateTimeColumn ? '' : '//'}    /**
${existUpdateTimeColumn ? '' : '//'}     * ${comment} 按 id+updateTime 物理删除
${existUpdateTimeColumn ? '' : '//'}     *
${existUpdateTimeColumn ? '' : '//'}     * @param id         {@link ${idType}} 数据id
${existUpdateTimeColumn ? '' : '//'}     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
${existUpdateTimeColumn ? '' : '//'}     * @param updateTime {@link String} 数据最后一次更新时间
${existUpdateTimeColumn ? '' : '//'}     * @return {@link Long} 物理删除行数
${existUpdateTimeColumn ? '' : '//'}     */
${existUpdateTimeColumn ? '' : '//'}    default long deleteById(final ${idType} id, final String updateTime${authUser}) {
${existUpdateTimeColumn ? '' : '//'}        return getQueryFactory()
${existUpdateTimeColumn ? '' : '//'}                .delete(table)
${existUpdateTimeColumn ? '' : '//'}                .where(QdslWhere.of().and(table.id.eq(id)).andIfNonBlank(updateTime, ()->table.updateTime.eq(updateTime)).toArray())
${existUpdateTimeColumn ? '' : '//'}                .execute();
${existUpdateTimeColumn ? '' : '//'}    }

    /**
     * ${comment} 按 id 批量物理删除
     *
     * @param ids    Set<${idType}> {@link ${TabName}#getId()} 数据 id 集合
     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
     * @return {@link Long} 物理删除行数
     */
    default long deleteByIds(final Set<${idType}> ids${authUser}) {
        return getQueryFactory()
                .delete(table)
                .where(table.id.in(ids))
                .execute();
    }

${existUpdateTimeColumn ? '' : '//'}    /**
${existUpdateTimeColumn ? '' : '//'}     * ${comment} 按 id+updateTime 批量物理删除
${existUpdateTimeColumn ? '' : '//'}     *
${existUpdateTimeColumn ? '' : '//'}     * @param ids         Set<${idType}> {@link ${TabName}#getId()} 数据ID
${existUpdateTimeColumn ? '' : '//'}     * @param updateTimes Set<String> 数据最后更新时间
${existUpdateTimeColumn ? '' : '//'}     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
${existUpdateTimeColumn ? '' : '//'}     * @return {@link Long} 物理删除行数
${existUpdateTimeColumn ? '' : '//'}     */
${existUpdateTimeColumn ? '' : '//'}    default long deleteByIds(final Set<${idType}> ids, final Set<String> updateTimes${authUser}) {
${existUpdateTimeColumn ? '' : '//'}        return getQueryFactory()
${existUpdateTimeColumn ? '' : '//'}                .delete(table)
${existUpdateTimeColumn ? '' : '//'}                .where(table.id.in(ids)
${existUpdateTimeColumn ? '' : '//'}                        .and(CollectionUtils.isEmpty(updateTimes) ? null : table.updateTime.in(updateTimes))
${existUpdateTimeColumn ? '' : '//'}                )
${existUpdateTimeColumn ? '' : '//'}                .execute();
${existUpdateTimeColumn ? '' : '//'}    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const deleteByIdOpenInstall = table => deleteById(table)
export const deleteByIdOpenUninstall = table => deleteById(table, false, true)

export const deleteByIdAuthInstall = table => deleteById(table, true, false)
export const deleteByIdAuthUninstall = table => deleteById(table, true, true)
