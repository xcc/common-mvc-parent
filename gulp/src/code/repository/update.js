/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const update = (table, auth = false, uninstall = false) => {
  const {idType, comment, existUpdateUserIdColumn, existUpdateTimeColumn, existDeletedColumn, isJpaModel, names: {TabName}} = table
  const authUser = auth ? ', final Long userId' : ''
  return `// <-
    /**
     * ${comment} 更新数据
     *
     * @param id   {@link ${idType}} 数据ID
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     * @param obj   {@link ${TabName}} 更新对象
     * @return {@link Long} 更新行数
     */
    ${isJpaModel ? 'default' : 'public'} long update(
             final ${idType} id
             ${authUser}
             , final ${TabName} obj) {
        return update(id${auth ? ',userId' : ''},obj, (Predicate)null);
    }
    /**
     * ${comment} 更新数据
     *
     * @param id   {@link ${idType}} 数据ID
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     * @param obj   {@link ${TabName}} 更新对象
     * @param appendWhere {@link QdslWhere} 追加更新条件
     * @return {@link Long} 更新行数
     */
    ${isJpaModel ? 'default' : 'public'} long update(
             final ${idType} id
             ${authUser}
             , final ${TabName} obj
             , final QdslWhere appendWhere) {
        return update(id${auth ? ',userId' : ''},obj, appendWhere);
    }
    /**
     * ${comment} 更新数据
     *
     * @param id   {@link ${idType}} 数据ID
     ${auth ? '* @param userId {@link Long} 操作用户ID' : ''}
     * @param obj   {@link ${TabName}} 更新对象
     * @param appendWhere {@link Predicate} 追加更新条件
     * @return {@link Long} 更新行数
     */
    ${isJpaModel ? 'default' : 'public'} long update(
             final ${idType} id
             ${authUser}
             , final ${TabName} obj
             , final Predicate appendWhere) {
        final Predicate where = QdslWhere.of()
                .and(table.id.eq(id))
                ${existUpdateTimeColumn ? '' : '//'}.andIfNonBlank(obj.getUpdateTime(), () -> table.updateTime.eq(obj.getUpdateTime()))
                ${existDeletedColumn ? '' : '//'}.and(table.deleted.eq(false))
                .toPredicate();
        ${existUpdateUserIdColumn ? '' : '//'}obj.setUpdateUserId(userId); // 设置更新操作人
        ${existUpdateTimeColumn ? '' : '//'}obj.setUpdateTime(yyyyMMddHHmmssSSS.now()); // 设置更新时间
        return obj.update(getQueryFactory().update(table))
                .get()
                ${existUpdateUserIdColumn ? '' : '//'}.set(table.updateUserId, userId) 
                ${existUpdateTimeColumn ? '' : '//'}.set(table.updateTime, obj.getUpdateTime()) 
                .where(where)
                .where(appendWhere)
                .execute();
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const updateOpenInstall = table => update(table)
export const updateOpenUninstall = table => update(table, false, true)

export const updateAuthInstall = table => update(table, true, false)
export const updateAuthUninstall = table => update(table, true, true)
