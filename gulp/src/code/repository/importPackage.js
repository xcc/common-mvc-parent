/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param spare {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const importPackage = (table, auth = false, spare = false) => {
  const {pkg, isJpaModel, names: {TabName, pkgName}} = table
  if (isJpaModel) { // JPA 模式 *****************************************************************************************
    return `
import ${pkg}.business.${pkgName}.entity.${TabName};
import ${pkg}.business.${pkgName}.entity.Q${TabName};

import app.common.starter.entity.Page;
import app.common.starter.util.Dates;
import app.dao.jpa.starter.enums.Limit;
import app.dao.jpa.starter.where.QdslWhere;
import com.google.common.collect.*;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.CollectionUtils;

import javax.persistence.NonUniqueResultException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmss;
import static app.common.starter.util.Dates.Pattern.yyyyMMddHHmmssSSS;
import static app.dao.jpa.starter.JPAConfiguration.getQueryFactory;
`
  }
  // SQL 模式 ***********************************************************************************************************
  return `
import ${pkg}.business.${pkgName}.entity.${TabName};

import com.utils.enums.Limit;
import com.common.util.Dates;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.ObjectArrays;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.sql.*;
import com.querydsl.sql.dml.SQLInsertClause;
import com.support.mvc.entity.base.Page;
import lombok.*;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static ${pkg}.config.init.AppInit.getQueryFactory;
import static com.common.util.Dates.Pattern.yyyyMMddHHmmss;
import static com.common.util.Dates.Pattern.yyyyMMddHHmmssSSS;
`
}
export const importPackageOpenInstall = table => importPackage(table)
export const importPackageOpenUninstall = table => importPackage(table, false, true)

export const importPackageAuthInstall = table => importPackage(table, true, false)
export const importPackageAuthUninstall = table => importPackage(table, true, true)
