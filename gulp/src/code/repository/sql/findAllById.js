/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findAllById = (table, auth = false, uninstall = false) => {
  const {comment, idType, isJpaModel, names: {TabName}} = table
  if (isJpaModel) {
    return ''
  }
  return `// <-
    /**
     * ${comment} 按 id 批量查询
     *
     * @param ids   Set<${idType}> {@link ${TabName}#getId()} id 集合
     * @return List<${TabName}> {@link ${TabName}> 实体对象集合
     */
    public List<${TabName}> findAllById(final Set<${idType}> ids) {
        return getQueryFactory()
                .select(Projections.bean(${TabName}.class, table.all()))
                .from(table)
                .where(table.id.in(ids))
                .fetch();
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findAllByIdOpenInstall = table => findAllById(table)
export const findAllByIdOpenUninstall = table => findAllById(table, false, true)

export const findAllByIdAuthInstall = table => findAllById(table, true, false)
export const findAllByIdAuthUninstall = table => findAllById(table, true, true)
