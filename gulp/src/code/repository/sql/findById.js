/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findById = (table, auth = false, uninstall = false) => {
  const {idType, comment, isJpaModel, names: {TabName}} = table
  if (isJpaModel) {
    return ''
  }
  return `// <-
    /**
     * ${comment} 按 ID 查询
     *
     * @param id   {@link ${idType}} 数据ID
     * @return Optional<${TabName}> {@link ${TabName}> 实体对象
     */
     public Optional<${TabName}> findById(final ${idType} id){
        return Optional.ofNullable(getQueryFactory()
                .select(Projections.bean(${TabName}.class, table.all()))
                .from(table)
                .where(table.id.eq(id))
                .fetchOne()
        );
     }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findByIdOpenInstall = table => findById(table)
export const findByIdOpenUninstall = table => findById(table, false, true)

export const findByIdAuthInstall = table => findById(table, true, false)
export const findByIdAuthUninstall = table => findById(table, true, true)
