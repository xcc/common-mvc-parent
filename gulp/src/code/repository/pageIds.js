/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const pageIds = (table, auth = false, uninstall = false) => {
  const {idType, comment, isJpaModel, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件分页查询， 仅返回 id 字段，用于分页查询优化
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param page      {@link Page} 分页
     * @return QueryResults<${idType}> {@link ${TabName}#getId()} 分页结果集
     */
    ${isJpaModel ? 'default' : 'public'} QueryResults<${idType}> pageIds(final ${TabName} condition, final Page page) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<${idType}> list = getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .offset(page.offset())
                .limit(page.limit())
                .fetch();
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pageIdsOpenInstall = table => pageIds(table)
export const pageIdsOpenUninstall = table => pageIds(table, false, true)

export const pageIdsAuthInstall = table => pageIds(table, true, false)
export const pageIdsAuthUninstall = table => pageIds(table, true, true)
