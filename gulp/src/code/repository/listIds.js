/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const listIds = (table, auth = false, uninstall = false) => {
  const {idType, comment, isJpaModel, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件查询返回 id ，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${idType}> {@link ${TabName}#getId()} ID集合
     */
    default List<${idType}> listIds(final ${TabName} condition) {
        return listIds(Limit.L1000.value, condition);
    }

    /**
     * ${comment} 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${idType}> {@link ${TabName}#getId()} ID集合
     */
    default List<${idType}> listIds(final long limit, final ${TabName} condition) {
        return getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }
    /**
     * ${comment} 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param offset    {@link Long} 跳过行数
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${idType}> {@link ${TabName}#getId()} ID集合
     */
    default List<${idType}> listIds(final long offset, final long limit, final ${TabName} condition) {
        return getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .offset(offset)
                .limit(limit)
                .fetch();
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listIdsOpenInstall = table => listIds(table)
export const listIdsOpenUninstall = table => listIds(table, false, true)

export const listIdsAuthInstall = table => listIds(table, true, false)
export const listIdsAuthUninstall = table => listIds(table, true, true)
