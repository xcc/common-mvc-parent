/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const markDeleteById = (table, auth = false, uninstall = false) => {
  const {idType, comment, existDeletedColumn, existUpdateUserIdColumn, existUpdateTimeColumn, names: {TabName}} = table
  if (!existDeletedColumn) {
    return ''
  }
  const authUser = auth ? ', final Long userId' : ''
  const authUserId = auth && existUpdateUserIdColumn ? '.set(table.updateUserId, userId)' : ''
  return `// <-
    /**
     * ${comment} 按 id 逻辑删除
     *
     * @param id     {@link ${idType}} 数据id
     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
     * @return {@link Long} 逻辑删除行数
     */
    default long markDeleteById(final ${idType} id${authUser}) {
        return getQueryFactory()
                .update(table)
                .set(table.deleted, true)
                ${authUserId}
                ${existUpdateTimeColumn ? '' : '//'}.set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
                .where(table.id.eq(id).and(table.deleted.eq(false)))
                .execute();
    }

${existUpdateTimeColumn ? '' : '//'}    /**
${existUpdateTimeColumn ? '' : '//'}     * ${comment} 按 id+updateTime 逻辑删除
${existUpdateTimeColumn ? '' : '//'}     *
${existUpdateTimeColumn ? '' : '//'}     * @param id         {@link ${idType}} 数据id
${existUpdateTimeColumn ? '' : '//'}     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
${existUpdateTimeColumn ? '' : '//'}     * @param updateTime {@link String} 数据最后一次更新时间
${existUpdateTimeColumn ? '' : '//'}     * @return {@link Long} 逻辑删除行数
${existUpdateTimeColumn ? '' : '//'}     */
${existUpdateTimeColumn ? '' : '//'}    default long markDeleteById(final ${idType} id, final String updateTime${authUser}) {
${existUpdateTimeColumn ? '' : '//'}        return getQueryFactory()
${existUpdateTimeColumn ? '' : '//'}                .update(table)
${existUpdateTimeColumn ? '' : '//'}                .set(table.deleted, true)
${existUpdateTimeColumn ? '' : '//'}                ${authUserId}
${existUpdateTimeColumn ? '' : '//'}                .set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
${existUpdateTimeColumn ? '' : '//'}                .where(QdslWhere.of().and(table.id.eq(id)).and(table.deleted.eq(false)).andIfNonBlank(updateTime, ()->table.updateTime.eq(updateTime)).toArray())
${existUpdateTimeColumn ? '' : '//'}                .execute();
${existUpdateTimeColumn ? '' : '//'}    }

    /**
     * ${comment} 按 id 批量逻辑删除
     *
     * @param ids    Set<${idType}> {@link ${TabName}#getId()} 数据 id 集合
     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
     * @return {@link Long} 逻辑删除行数
     */
    default long markDeleteByIds(final Set<${idType}> ids${authUser}) {
        return getQueryFactory()
                .update(table)
                .set(table.deleted, true)
                ${authUserId}
                ${existUpdateTimeColumn ? '' : '//'}.set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
                .where(table.id.in(ids).and(table.deleted.eq(false)))
                .execute();
    }

${existUpdateTimeColumn ? '' : '//'}    /**
${existUpdateTimeColumn ? '' : '//'}     * ${comment} 按 id+updateTime 批量逻辑删除
${existUpdateTimeColumn ? '' : '//'}     *
${existUpdateTimeColumn ? '' : '//'}     * @param ids         Set<${idType}> {@link ${TabName}#getId()} 数据ID
${existUpdateTimeColumn ? '' : '//'}     * @param updateTimes Set<String> 数据最后更新时间
${existUpdateTimeColumn ? '' : '//'}     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
${existUpdateTimeColumn ? '' : '//'}     * @return {@link Long} 逻辑删除行数
${existUpdateTimeColumn ? '' : '//'}     */
${existUpdateTimeColumn ? '' : '//'}    default long markDeleteByIds(final Set<${idType}> ids, final Set<String> updateTimes${authUser}) {
${existUpdateTimeColumn ? '' : '//'}        return getQueryFactory()
${existUpdateTimeColumn ? '' : '//'}                .update(table)
${existUpdateTimeColumn ? '' : '//'}                .set(table.deleted, true)
${existUpdateTimeColumn ? '' : '//'}                ${authUserId}
${existUpdateTimeColumn ? '' : '//'}                .set(table.updateTime, yyyyMMddHHmmssSSS.now()) // 设置更新时间，注意这里有带3位毫秒数，数据库需要设置支持存储3位毫秒
${existUpdateTimeColumn ? '' : '//'}                .where(table.id.in(ids)
${existUpdateTimeColumn ? '' : '//'}                        .and(table.deleted.eq(false))
${existUpdateTimeColumn ? '' : '//'}                        .and(CollectionUtils.isEmpty(updateTimes) ? null : table.updateTime.in(updateTimes))
${existUpdateTimeColumn ? '' : '//'}                )
${existUpdateTimeColumn ? '' : '//'}                .execute();
${existUpdateTimeColumn ? '' : '//'}    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const markDeleteByIdOpenInstall = table => markDeleteById(table)
export const markDeleteByIdOpenUninstall = table => markDeleteById(table, false, true)

export const markDeleteByIdAuthInstall = table => markDeleteById(table, true, false)
export const markDeleteByIdAuthUninstall = table => markDeleteById(table, true, true)
