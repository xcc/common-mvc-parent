/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findOne = (table, auth = false, uninstall = false) => {
  const {comment, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按 条件查询单条数据，如果查询结果超过 1 条数据则抛出异常
     *
     * @param condition {@link ${TabName}} 查询条件
     * @return Optional<${TabName}> {@link ${TabName}} 返回实体对象
     */
    default Optional<${TabName}> findOne(final ${TabName} condition) {
        return findOne(condition.where());
    }
    /**
     * ${comment} 按 条件查询单条数据，如果查询结果超过 1 条数据则抛出异常
     *
     * @param where {@link QdslWhere} 查询条件
     * @return Optional<${TabName}> {@link ${TabName}} 返回实体对象
     */
    default Optional<${TabName}> findOne(final QdslWhere where) {
        final List<${TabName}> list = getQueryFactory()
                .selectFrom(table)
                .where(where.toPredicate())
                .limit(Limit.L2.value)
                .fetch();
        if (list.size() > 1) {
            throw new NonUniqueResultException("预期查询结果行数为1，实际查询结果行数大于1");
        }
        return list.stream().findFirst();
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findOneOpenInstall = table => findOne(table)
export const findOneOpenUninstall = table => findOne(table, false, true)

export const findOneAuthInstall = table => findOne(table, true, false)
export const findOneAuthUninstall = table => findOne(table, true, true)
