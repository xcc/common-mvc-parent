/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const insert = (table, auth = false, uninstall = false) => {
  const {comment, isJpaModel, names: {TabName}} = table
  const authUser = auth ? 'final Long userId, ' : ''
  return `// <-
    /**
     * ${comment} 新增数据
     * <pre>
     * 注意：
     *   原来通过 AOP 和数据库设置的默认值迁移到这里。
     *   因为 JPA 特性导致新增之后获取不到时间和数据库的默认值，
     *   所以这里把需要有默认值的数据库字段都预设值好，同时兼容不支持设置默认值的数据库
     * </pre>
     *
     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
     * @param obj    {@link ${TabName}} 新增对象
     * @return {@link ${TabName}}
     */
    ${isJpaModel ? 'default' : 'public'} ${TabName} insert(${authUser}final ${TabName} obj) {
        final Dates now = Dates.now();
        obj.insert(now${auth ? ', userId' : ''});
        return save(obj);
    }

    /**
     * ${comment} 新增数据
     * <pre>
     * 注意：
     *   原来通过 AOP 和数据库设置的默认值迁移到这里。
     *   因为 JPA 特性导致新增之后获取不到时间和数据库的默认值，
     *   所以这里把需要有默认值的数据库字段都预设值好，同时兼容不支持设置默认值的数据库
     * </pre>
     *
     * ${auth ? '@param userId {@link Long} 操作用户ID' : ''}
     * @param list   List<${TabName}> {@link ${TabName}} 新增对象
     * @return List<${TabName}> {@link ${TabName}}
     */
    ${isJpaModel ? 'default' : 'public'} List<${TabName}> insert(${authUser}final List<${TabName}> list) {
        final Dates now = Dates.now();
        list.forEach(obj -> obj.insert(now${auth ? ', userId' : ''}));
        return saveAll(list);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
//   return `// <-
//     /**
//      * ${comment} 新增数据
//      *
//      * @param obj {@link ${TabName}} 新增对象
//      * @return {@link ${TabName}} 新增对象
//      */
//     public ${TabName} save(final ${TabName} obj) {
//         final ${idType} id = obj.insert(getQueryFactory().insert(table))
//                 .get()
//                 .executeWithKey(table.id); // 警告：如果 id 不是数据库自动生成的，这里返回的 id 则为 null；需要换成 execute() 方法，并校验插入行数
//         obj.setId(id);
//         return obj;
//     }
// `.split('\n')
//     .map(row => (uninstall ? '//' : '') + row)
//     .join('\n')
// }
//   return `// <-
//     /**
//      * ${comment} 批量新增数据
//      *
//      * @param list List<${TabName}> {@link ${TabName}> 新增对象集合
//      * @return List<${TabName}> {@link ${TabName}> 新增对象集合
//      */
//     public List<${TabName}> saveAll(final List<${TabName}> list) {
//         final SQLInsertClause batchInsert = getQueryFactory().insert(table);
//         list.forEach(obj -> obj.insert(batchInsert).get().addBatch());
//         final List<${idType}> ids = batchInsert.executeWithKeys(table.id);  // 警告：如果 id 不是数据库自动生成的，这里返回的 id 集合则为 null；需要换成 execute() 方法，并校验插入行数
//         for (int i = 0; i < ids.size(); i++) {
//             list.get(i).setId(ids.get(i));
//         }
//         return list;
//     }
// `.split('\n')
//     .map(row => (uninstall ? '//' : '') + row)
//     .join('\n')
}
export const insertOpenInstall = table => insert(table)
export const insertOpenUninstall = table => insert(table, false, true)

export const insertAuthInstall = table => insert(table, true, false)
export const insertAuthUninstall = table => insert(table, true, true)
