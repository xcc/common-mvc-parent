/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const listByIds = (table, auth = false, uninstall = false) => {
  const {idType, comment, isJpaModel, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按 ID 批量查询
     *
     * @param ids {@link ${idType}} 数据ID
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default List<${TabName}> listByIds(final Collection<${idType}> ids) {
        final Map<${idType}, ${TabName}> map = findAllById(Sets.newHashSet(ids)).stream().collect(Collectors.toMap(${TabName}::getId, ${TabName}::cloneObject));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * ${comment} 按 ID 批量查询 id + 指定字段，返回 list , 返回结果保持同传入的 id 顺序一致
     *
     * @param ids  {@link ${idType}} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default List<${TabName}> listByIds(final Collection<${idType}> ids, final Expression<?>... exps) {
        final Map<${idType}, ${TabName}> map = getQueryFactory()
                .select(Projections.bean(${TabName}.class, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(${TabName}::getId, row -> row));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * ${comment} 按 ID 批量查询 id + 指定字段，返回 list , 返回结果保持同传入的 id 顺序一致
     *
     * @param ids  {@link ${idType}} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return List<R> 实体对象集合
     */
    default <R> List<R> listByIds(final Collection<${idType}> ids, final Class<R> clazz, final Expression<?>... exps) {
        final Map<${idType}, R> map = getQueryFactory()
                .select(table.id, Projections.bean(clazz, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(tuple -> tuple.get(table.id), tuple -> Objects.requireNonNull(tuple.get(1, clazz))));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listByIdsOpenInstall = table => listByIds(table)
export const listByIdsOpenUninstall = table => listByIds(table, false, true)

export const listByIdsAuthInstall = table => listByIds(table, true, false)
export const listByIdsAuthUninstall = table => listByIds(table, true, true)
