/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const pull = (table, auth = false, uninstall = false) => {
  const {comment, idType, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件查询并跳过指定行数
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}} 列表查询结果集
     */
    default List<${TabName}> pull(final int skip, final int limit, final ${TabName} condition) {
        return pull(skip, limit, condition, ${TabName}.class, ${TabName}.allColumnAppends());
    }

    /**
     * ${comment} 按条件查询并跳过指定行数，投影到 VO 类
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link ${TabName}} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return List<${TabName}> {@link ${TabName}} 列表查询结果集
     */
    default <R extends ${TabName}> List<R> pull(final int skip, final int limit, final ${TabName} condition, final Class<R> clazz) {
        return pull(skip, limit, condition, clazz, ${TabName}.allColumnAppends());
    }

    /**
     * ${comment} 按条件查询并跳过指定行数，查询指定字段，投影到 VO 类
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link ${TabName}} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return List<${TabName}> {@link ${TabName}} 列表查询结果集
     */
    default <R extends ${TabName}> List<R> pull(final int skip, final int limit, final ${TabName} condition, final Class<R> clazz, final Expression<?>... exps) {
        if (limit < 1) {
            return Collections.emptyList();
        }
        final List<${idType}> ids = listIds(skip, limit, condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        return listByIds(ids, clazz, exps);  // 再按 id 批量查询
    }

`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pullOpenInstall = table => pull(table)
export const pullOpenUninstall = table => pull(table, false, true)

export const pullAuthInstall = table => pull(table, true, false)
export const pullAuthUninstall = table => pull(table, true, true)

