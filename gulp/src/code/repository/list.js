/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const list = (table, auth = false, uninstall = false) => {
  const {comment, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default List<${TabName}> list(final ${TabName} condition) {
        return list(Limit.L1000.value, condition);
    }

    /**
     * ${comment} 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default List<${TabName}> list(final long limit, final ${TabName} condition) {
        return getQueryFactory()
                .selectFrom(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * ${comment} 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default <R> List<R> list(final ${TabName} condition, final Expression<R> exp) {
        return list(Limit.L1000.value, condition, exp);
    }

    /**
     * ${comment} 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default <R> List<R> list(final long limit, final ${TabName} condition, final Expression<R> exp) {
        return getQueryFactory()
                .select(exp)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * ${comment} 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default List<${TabName}> list(final ${TabName} condition, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, exps);
    }

    /**
     * ${comment} 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default List<${TabName}> list(final long limit, final ${TabName} condition, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(${TabName}.class, exps))
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * ${comment} 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default <T extends ${TabName}> List<T> list(final ${TabName} condition, final Class<T> clazz) {
        return list(Limit.L1000.value, condition, clazz, ${TabName}.allColumnAppends());
    }

    /**
     * ${comment} 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default <T extends ${TabName}> List<T> list(final long limit, final ${TabName} condition, final Class<T> clazz) {
        return list(limit, condition, clazz, ${TabName}.allColumnAppends());
    }

    /**
     * ${comment} 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default <T extends ${TabName}> List<T> list(final ${TabName} condition, final Class<T> clazz, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, clazz, exps);
    }

    /**
     * ${comment} 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link ${TabName}} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return List<${TabName}> {@link ${TabName}} 实体对象集合
     */
    default <T extends ${TabName}> List<T> list(final long limit, final ${TabName} condition, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listOpenInstall = table => list(table)
export const listOpenUninstall = table => list(table, false, true)

export const listAuthInstall = table => list(table, true, false)
export const listAuthUninstall = table => list(table, true, true)
// /**
//  *
//  * @param table {Table}
//  * @param auth {boolean} 接口是否需要鉴权
//  * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
//  * @returns {string}
//  */
// const list = (table, auth = false, uninstall = false) => {
//   const {
//     comment,
//     names: {TabName}
//   } = table
//   return `// <-
//     /**
//      * ${comment} 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
//      *
//      * @param condition   {@link ${TabName}} 查询条件
//      * @return List<${TabName}> {@link ${TabName}> 实体对象集合
//      */
//     public List<${TabName}> list(final ${TabName} condition) {
//         return list(Limit.L1000.value, condition);
//     }
//     /**
//      * ${comment} 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
//      *
//      * @param limit   {@link Long} 指定返回行数
//      * @param condition   {@link ${TabName}} 查询条件
//      * @return List<${TabName}> {@link ${TabName}> 实体对象集合
//      */
//     public List<${TabName}> list(final long limit, final ${TabName} condition) {
//         return getQueryFactory()
//                 .select(Projections.bean(${TabName}.class, table.all()))
//                 .from(table)
//                 .where(condition.where().toArray())
//                 .orderBy(condition.qdslOrderBy())
//                 .limit(limit)
//                 .fetch();
//     }
//     /**
//      * ${comment} 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
//      *
//      * @param condition {@link ${TabName}} 查询条件
//      * @return List<${TabName}> {@link ${TabName}> 实体对象集合
//      */
//     public <R> List<R> list(final ${TabName} condition, final Expression<R> exp) {
//         return list(Limit.L1000.value, condition, exp);
//     }
//     /**
//      * ${comment} 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
//      *
//      * @param limit     {@link Long} 指定返回行数
//      * @param condition {@link ${TabName}} 查询条件
//      * @return List<${TabName}> {@link ${TabName}> 实体对象集合
//      */
//     public <R> List<R> list(final long limit, final ${TabName} condition, final Expression<R> exp) {
//         return getQueryFactory()
//                 .select(exp)
//                 .from(table)
//                 .where(condition.where().toArray())
//                 .orderBy(condition.qdslOrderBy())
//                 .limit(limit)
//                 .fetch();
//     }
//     /**
//      * ${comment} 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
//      *
//      * @param condition   {@link ${TabName}} 查询条件
//      * @param exps   {@link Expression} 指定返回字段
//      * @return List<${TabName}> {@link ${TabName}> 实体对象集合
//      */
//     public List<${TabName}> list(final ${TabName} condition, final Expression<?>... exps) {
//         return list(Limit.L1000.value, condition, exps);
//     }
//     /**
//      * ${comment} 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
//      *
//      * @param limit   {@link Long} 指定返回行数
//      * @param condition   {@link ${TabName}} 查询条件
//      * @param exps   {@link Expression} 指定返回字段
//      * @return List<${TabName}> {@link ${TabName}> 实体对象集合
//      */
//     public List<${TabName}> list(final long limit, final ${TabName} condition, final Expression<?>... exps) {
//         return getQueryFactory()
//                 .select(Projections.bean(${TabName}.class, exps))
//                 .from(table)
//                 .where(condition.where().toArray())
//                 .orderBy(condition.qdslOrderBy())
//                 .limit(limit)
//                 .fetch();
//     }
// `.split('\n')
//     .map(row => (uninstall ? '//' : '') + row)
//     .join('\n')
// }
// export const listOpenInstall = table => list(table)
// export const listOpenUninstall = table => list(table, false, true)
//
// export const listAuthInstall = table => list(table, true, false)
// export const listAuthUninstall = table => list(table, true, true)
