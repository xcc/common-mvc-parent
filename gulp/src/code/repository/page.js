/**
 *
 * @param table {Table}
 * @param auth {boolean} 接口是否需要鉴权
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const page = (table, auth = false, uninstall = false) => {
  const {comment, idType, names: {TabName}} = table
  return `// <-
    /**
     * ${comment} 按条件分页查询
     *
     * @param condition   {@link ${TabName}} 查询条件
     * @param page   {@link Page} 分页
     * @return QueryResults<${TabName}> {@link ${TabName}} 分页结果集
     */
    default QueryResults<${TabName}> page(final ${TabName} condition, final Page page) {
        { // 直接查询
            // return getQueryFactory()
            //        .selectFrom(table)
            //        .where(condition.where().toArray())
            //        .offset(page.offset())
            //        .limit(page.limit())
            //        .orderBy(condition.qdslOrderBy())
            //        .fetchResults();
        }
        { // 查询优化版
            final long count = count(condition.where());
            if (count == 0L) {
                return QueryResults.emptyResults();
            }
            final List<${idType}> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
            final List<${TabName}> list = listByIds(ids); // 再按 id 批量查询
            return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
        }
   }

    /**
     * ${comment} 按条件分页查询
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param page      {@link Page} 分页
     * @param exps      {@link Expression} 指定返回字段
     * @return QueryResults<${TabName}> {@link ${TabName}} 分页结果集
     */
    default QueryResults<${TabName}> page(final ${TabName} condition, final Page page, final Expression<?>... exps) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<${idType}> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        final List<${TabName}> list = listByIds(ids, exps); // 再按 id 批量查询
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * ${comment} 按条件分页查询，投影到 VO 类
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param page      {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @return QueryResults<${TabName}> {@link ${TabName}} 分页结果集
     */
    default <T extends ${TabName}> QueryResults<T> page(final ${TabName} condition, final Page page, final Class<T> clazz) {
        return page(condition, page, clazz, ${TabName}.allColumnAppends());
    }

    /**
     * ${comment} 按条件分页查询，查询指定字段，投影到 VO 类
     *
     * @param condition {@link ${TabName}} 查询条件
     * @param page      {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return QueryResults<${TabName}> {@link ${TabName}} 分页结果集
     */
    default <T extends ${TabName}> QueryResults<T> page(final ${TabName} condition, final Page page, final Class<T> clazz, final Expression<?>... exps) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<${idType}> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        final List<T> list = listByIds(ids, clazz, exps); // 再按 id 批量查询
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const pageOpenInstall = table => page(table)
export const pageOpenUninstall = table => page(table, false, true)

export const pageAuthInstall = table => page(table, true, false)
export const pageAuthUninstall = table => page(table, true, true)

// return `// <-
//     /**
//      * ${comment} 按条件分页查询
//      *
//      * @param condition   {@link ${TabName}} 查询条件
//      * @param page   {@link Page} 分页
//      * @return QueryResults<${TabName}> {@link ${TabName}} 分页结果集
//      */
//     public QueryResults<${TabName}> page(final ${TabName} condition, final Page page) {
//         { // 直接查询
//             // return getQueryFactory()
//             //        .select(Projections.bean(${TabName}.class, table.all()))
//             //        .from(table)
//             //        .where(condition.where().toArray())
//             //        .offset(page.offset())
//             //        .limit(page.limit())
//             //        .orderBy(condition.qdslOrderBy())
//             //        .fetchResults();
//         }
//         { // 查询优化版
//             final QueryResults<${idType}> queryResults = pageIds(condition, page); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
//             if (queryResults.isEmpty()) {
//                 return QueryResults.emptyResults();
//             }
//             final List<${TabName}> list = listByIds(queryResults.getResults()); // 再按 id 批量查询
//             return new QueryResults<>(
//                     list,
//                     queryResults.getLimit(),
//                     queryResults.getOffset(),
//                     queryResults.getTotal()
//             );
//         }
//     }
//     /**
//      * ${comment} 按条件分页查询
//      *
//      * @param condition   {@link ${TabName}} 查询条件
//      * @param page   {@link Page} 分页
//      * @param exps   {@link Expression} 指定返回字段
//      * @return QueryResults<${TabName}> {@link ${TabName}} 分页结果集
//      */
//     public QueryResults<${TabName}> page(final ${TabName} condition, final Page page, final Expression<?>... exps) {
//         { // 直接查询
//             // return getQueryFactory()
//             //        .select(Projections.bean(${TabName}.class, exps))
//             //        .from(table)
//             //        .where(condition.where().toArray())
//             //        .offset(page.offset())
//             //        .limit(page.limit())
//             //        .orderBy(condition.qdslOrderBy())
//             //        .fetchResults();
//         }
//         { // 查询优化版
//             final QueryResults<${idType}> queryResults = pageIds(condition, page); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
//             if (queryResults.isEmpty()) {
//                 return QueryResults.emptyResults();
//             }
//             final List<${TabName}> list = listByIds(queryResults.getResults(), exps); // 再按 id 批量查询
//             return new QueryResults<>(
//                     list,
//                     queryResults.getLimit(),
//                     queryResults.getOffset(),
//                     queryResults.getTotal()
//             );
//         }
//     }
// `.split('\n')
//   .map(row => (uninstall ? '//' : '') + row)
//   .join('\n')
