/**
 * 基于表名生成各种命名
 */
export default class Names {
  /**
   * 构造表名各种规则
   * @param tableName {string}
   */
  constructor(tableName) {
    /**
     * tab_code_example => tab_code_example
     * @type {string}
     */
    this.tab_name = tableName
    /**
     * tab_code_example => TabCodeExample
     * @type {string}
     */
    this.TabName = tableName.split('_').map(v => v.replace(/^\S/, m => m.toUpperCase())).join('')
    /**
     * tab_code_example => tabCodeExample
     * @type {string}
     */
    this.tabName = this.TabName.replace(/^\S/, m => m.toLowerCase())
    /**
     * tab_code_example => code-example
     * @type {string}
     */
    this.java_name = tableName.replace(/^[a-zA-Z]+_/, '').replace(/_/g, '-')
    /**
     * tab_code_example => CodeExample
     * @type {string}
     */
    this.JavaName = this.java_name.split('-').map(v => v.replace(/^\S/, m => m.toUpperCase())).join('')
    /**
     * tab_code_example => codeExample
     * @type {string}
     */
    this.javaName = this.JavaName.replace(/^\S/, m => m.toLowerCase())
    /**
     * tab_code_example => code
     * @type {string}
     */
    this.pkgName = this.java_name.split('-')[0]
  }

  /**
   * 转换为对象
   * @return {Object}
   */
  toObject() {
    return JSON.parse(JSON.stringify(this))
  }

  toString() {
    return JSON.stringify(this)
  }
}
