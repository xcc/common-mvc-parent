import DataType from './DataType'
import isNull from 'lodash/isNull'
import upperFirst from 'lodash/upperFirst'

const TINYINT_MIN_VALUE = -128
const TINYINT_MAX_VALUE = 127
const SHORT_MIN_VALUE = -32768
const SHORT_MAX_VALUE = 32767
const INTEGER_MIN_VALUE = -2147483648
const INTEGER_MAX_VALUE = 2147483647
const LONG_MIN_VALUE = -9223372036854775808
// eslint-disable-next-line no-loss-of-precision
const LONG_MAX_VALUE = 9223372036854775807
/**
 * 数据库字段信息
 */
export default class Column {
  /**
   * 通过 SHOW FULL COLUMNS FROM #{tableName} 获取到的参数
   * @param index {number} 字段序号
   * @param Field {string} 字段名
   * @param Type {string} 数据库类型
   * @param Collation {string} 字符集
   * @param Null {string} 是否允许 null ，【YES:允许 null 值，NO:不允许 null 值】
   * @param Key {string} 是否有索引
   * @param Default {string} 默认值
   * @param Comment {string} 字段说明
   */
  constructor({index, Field, Type, Collation, Null, Key, Default, Comment}) {
    /**
     * 字段排序，@ApiModelProperty position 需要该值
     * @type {number}
     */
    this.index = index + 1
    /**
     * 实体属性名，驼峰命名法
     * @type {string}
     */
    this.name = Field.replace(/^is_/, '') // 数据库中 is 开头的字段生成实体需要去掉 is_ ，然后在属性头上注解数据库字段名
      .replace(/_([a-zA-Z])/g, (m, $1) => $1.toUpperCase())
    /**
     * get 方法名
     * @type {string}
     */
    this.nameGet = 'get' + this.name.replace(/^[a-z]/g, m => m.toUpperCase())
    /**
     * set 方法名
     * @type {string}
     */
    this.nameSet = 'set' + this.name.replace(/^[a-z]/g, m => m.toUpperCase())
    /**
     * 数据库字段名，带下划线
     * @type {string}
     */
    this.db_name = Field
    const [_matchFull, type, _matchLength, length, fixed] = Type.match(/^(\w+)(\((\d+),?(\d+)?\))?.*/)
    /**
     * 数据类型
     * @type {string}
     */
    this.type = type.toUpperCase()
    /**
     * 长度
     * @type {number}
     */
    this.length = length && parseInt(length)
    /**
     * 小数位长度
     * @type {number}
     */
    this.fixed = fixed && parseInt(fixed)
    /**
     * 是否允许负数
     * @type {boolean}
     */
    this.unsigned = Type.toUpperCase().includes('UNSIGNED')
    /**
     * 字段字符集
     * @type {string}
     */
    this.collation = Collation
    this.defaultValue = Default
    /**
     * 是否必填，数据库中 not null
     * @type {boolean} true:是，false:否
     */
    this.notNull = Null === 'NO'
    /**
     * 是否必填，数据库中 not null 且没有指定默认值时，判断为必填项，用于校验注解
     * @type {boolean} true:是，false:否
     */
    this.notNullAndNotDefault = this.notNull && isNull(Default)
    /**
     * 字段说明
     * @type {string}
     */
    this.comment = Comment.replace(/\s/g, ' ').replace(/"/g, '\'')
    /**
     * 数据库与java数据类型映射
     * @type {Type}
     */
    this.dataType = DataType[this.type]
    /**
     * where 条件顺序，数字越小 where 条件越靠前
     * @type {number}
     */
    this.ordinal = 9999
    if (this.name === 'id') {
      this.ordinal = -1
    } else if (Key) {
      this.ordinal = this.dataType.ordinal / 10
    }
  }

  /**
   * 转换为实体字段属性
   * @returns {string}
   */
  toField() {
    if ('id' === this.name) {
      const idType = this.dataType.java === DataType.VARCHAR.java ? this.dataType.java : DataType.BIGINT.java
      if ([DataType.BIGINT.mysql, DataType.INT.mysql].includes(this.dataType.mysql)) {
        return `    @AesId${idType}JsonConverter\n    @Id\n    @GeneratedValue(strategy = GenerationType.IDENTITY)\n    @Column(name = "${this.db_name}")\n    @Positive\n    @ApiModelProperty(value = "数据ID", position = ${this.index})\n    private Long ${this.name};`
      } else {
        return `    @AesId${idType}JsonConverter\n    @Id\n    @GeneratedValue(generator = "base36")\n    @GenericGenerator(name = "base36", strategy = "app.dao.jpa.starter.generator.Base36Generator")\n    @Size(max = ${this.length})\n    @ApiModelProperty(value = "数据id", position = ${this.index})\n    private String ${this.name};`
      }
    } else if ('deleted' === this.name) {
      return `    @Column(name = "${this.db_name}")\n    @ApiModelProperty(value = "是否逻辑删除。 [0:否,1:是]", position = ${this.index})\n    private Boolean ${this.name};`
    } else if ('createTime' === this.name) {
      return `    @Column(name = "${this.db_name}")\n    @Size(min = 14, max = 14)\n    @ApiModelProperty(value = "数据新增时间，格式：yyyyMMddHHmmss", example = "20200202020202", position = ${this.index})\n    private String ${this.name};`
    } else if ('updateTime' === this.name) {
      return `    @Column(name = "${this.db_name}")\n    @Size(min = 17, max = 17)\n    @ApiModelProperty(value = "数据最后一次更新时间，格式：yyyyMMddHHmmssSSS", example = "20200202020202002", position = ${this.index})\n    private String ${this.name};`
    } else if ('createUserId' === this.name) {
      return `    @AesIdLongJsonConverter // id 使用 aes 加密\n    @Column(name = "${this.db_name}")\n    @PositiveOrZero\n    @ApiModelProperty(value = "新增操作人id", position = ${this.index})\n    private Long ${this.name};`
    } else if ('updateUserId' === this.name) {
      return `    @AesIdLongJsonConverter // id 使用 aes 加密\n    @Column(name = "${this.db_name}")\n    @PositiveOrZero\n    @ApiModelProperty(value = "更新操作人id", position = ${this.index})\n    private Long ${this.name};`
      // } else if ('' === this.name) {
      //   return ``
    }
    const list = [`    @Column(name = "${this.db_name}")`]
    // if (notNullAndNotDefault) list.push('    @NotNull(groups = {ISave.class})')
    switch (this.dataType.mysql) {
      case 'TINYINT':
        list.push('    // FIXME: 请使用枚举修正属性数据类型，定义枚举之后记得删除　@Min　@Max 注解')
        if (this.unsigned) {
          list.push('    @Min(0)')
          if (this.length) list.push(`    @Max(${Math.min(TINYINT_MAX_VALUE, parseInt('9'.repeat(this.length)))})`)
        } else if (this.length) { // 有符号表示有负数，所以要除 2
          list.push(`    @Min(${Math.floor(Math.max(TINYINT_MIN_VALUE, parseInt('-'.concat('9'.repeat(this.length))) / 2))})`)
          list.push(`    @Max(${Math.floor(Math.min(TINYINT_MAX_VALUE, parseInt('9'.repeat(this.length)) / 2))})`)
        }
        break
      case 'SMALLINT':
        if (this.unsigned) {
          list.push('    @Min(0)')
          if (this.length) list.push(`    @Max(${Math.min(SHORT_MAX_VALUE, parseInt('9'.repeat(this.length)))})`)
        } else if (this.length) { // 有符号表示有负数，所以要除 2
          list.push(`    @Min(${Math.floor(Math.max(SHORT_MIN_VALUE, parseInt('-'.concat('9'.repeat(this.length))) / 2))})`)
          list.push(`    @Max(${Math.floor(Math.min(SHORT_MAX_VALUE, parseInt('9'.repeat(this.length)) / 2))})`)
        }
        break
      case 'MEDIUMINT':
      case 'INT':
        if (this.unsigned) {
          list.push('    @Min(0)')
          if (this.length) list.push(`    @Max(${Math.min(INTEGER_MAX_VALUE, parseInt('9'.repeat(this.length)))})`)
        } else if (this.length) { // 有符号表示有负数，所以要除 2
          list.push(`    @Min(${Math.floor(Math.max(INTEGER_MIN_VALUE, parseInt('-'.concat('9'.repeat(this.length))) / 2))})`)
          list.push(`    @Max(${Math.floor(Math.min(INTEGER_MAX_VALUE, parseInt('9'.repeat(this.length)) / 2))})`)
        }
        break
      case 'BIGINT':
        list.push('    // @AesIdLongJsonConverter // FIXME: 检查是否为关联主键字段，是则使用 @AesIdLongJsonConverter、@AesIdStringJsonConverter 注解，响应报文会使用 aes 加密，避免暴露数据库自增id到前端；如果不是关联主键则删除这一行')
        if (this.unsigned) {
          list.push('    @Min(0)')
          if (this.length) list.push(`    @DecimalMax("${Math.min(LONG_MAX_VALUE, parseInt('9'.repeat(this.length)))}")`)
        } else if (this.length) { // 有符号表示有负数，所以要除 2
          list.push(`    @DecimalMin("${Math.floor(Math.max(LONG_MIN_VALUE, parseInt('-'.concat('9'.repeat(this.length))) / 2))}")`)
          list.push(`    @DecimalMax("${Math.floor(Math.min(LONG_MAX_VALUE, parseInt('9'.repeat(this.length)) / 2))}")`)
        }
        break
      case 'DECIMAL':
      case 'DOUBLE':
      case 'FLOAT':
        list.push(`    @Digits(integer = ${this.length - (this.fixed || 0)}, fraction = ${this.fixed || 0})`)
        break
      case 'CHAR':
        list.push(`    @Size(min = ${this.length}, max = ${this.length})`)
        break
      case 'VARCHAR':
        list.push(`    @Size(max = ${this.length})`)
        break
      case 'TEXT':
        list.push(`    @Size(max = ${Math.min(65535, this.length || 65535)})`)
        break
      case 'MEDIUMTEXT':
        list.push(`    @Size(max = ${Math.min(65535 * 2, this.length || 65535 * 2)})`)
        break
      case 'LONGTEXT':
        list.push(`    @Size(max = ${Math.min(65535 * 4, this.length || 65535 * 4)})`)
        break
      case 'DATE':
        list.push('    @JsonFormat(pattern = "yyyy-MM-dd")')
        break
      case 'DATETIME':
      case 'TIMESTAMP':
        list.push('    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")')
        break
    }
    list.push(`    @ApiModelProperty(value = "${this.comment}", position = ${this.index})`)
    list.push(`    private ${this.dataType.java} ${this.name};`)
    return list.join('\n')
  }

  /**
   * 转换为实体字段属性
   * @returns {string}
   */
  toWhere() {
    if ('id' === this.name) {
      return `                .and(${this.name}, () -> table.${this.name}.eq(${this.name}))\n                .andIfNonEmpty(ids, () -> table.id.in(ids))`
    } else if ('deleted' === this.name) {
      return '                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))'
    }
    switch (this.dataType.mysql) {
      case 'CHAR':
        return `//                .and(${this.name}, () -> ${this.name}.endsWith("%") || ${this.name}.startsWith("%") ? table.${this.name}.like(${this.name}) : table.${this.name}.eq(${this.name}))`
      case 'VARCHAR':
        return this.length < 100
          ? `//                .and(${this.name}, () -> ${this.name}.endsWith("%") || ${this.name}.startsWith("%") ? table.${this.name}.like(${this.name}) : table.${this.name}.eq(${this.name}))`
          : `//                .and(${this.name}, () -> ${this.name}.endsWith("%") || ${this.name}.startsWith("%") ? table.${this.name}.like(${this.name}) : table.${this.name}.startsWith(${this.name}))`
      case 'TEXT':
      case 'MEDIUMTEXT':
      case 'LONGTEXT':
        return `//                .and(${this.name}, () -> ${this.name}.endsWith("%") || ${this.name}.startsWith("%") ? table.${this.name}.like(${this.name}) : table.${this.name}.contains(${this.name}))`
      case 'DECIMAL':
      case 'DOUBLE':
      case 'FLOAT':
        return `//                .and(${this.name}Range, () -> table.${this.name}.between(${this.name}Range.getMin(), ${this.name}Range.getMax()))`
      case 'DATE':
      case 'DATETIME':
      case 'TIMESTAMP':
        return `//                .and(${this.name}Range, () -> table.${this.name}.between(${this.name}Range.rebuild().getBegin(), ${this.name}Range.getEnd()))`
      case 'JSON':
        return `//                .and(${this.name}, () -> Expressions.booleanTemplate("JSON_CONTAINS({0},{1})>0", table.${this.name}, JSON.toJsonString(${this.name})))`
    }
    return `//                .and(${this.name}, () -> table.${this.name}.eq(${this.name}))`
  }

  /**
   * 转换为swagger查询参数
   * @returns {string}
   */
  toSwaggerApiParam() {
    switch (this.dataType.java) {
      case 'String':
        return `            //@ApiParam(value = "${this.comment}；示例：AaBb1111", type = "string") @RequestParam(required = false) final String ${this.name},`
      case 'Long':
        return `            //@ApiParam(value = "${this.comment}；示例：54775807") @AesRequestParam(required = false) final Long ${this.name},`
      case 'Integer':
        return `            //@ApiParam(value = "${this.comment}；示例：7483647") @RequestParam(required = false) final Integer ${this.name},`
      case 'Short':
        return `            //@ApiParam(value = "${this.comment}；示例：32767") @RequestParam(required = false) final Short ${this.name},`
      case 'Byte':
        return `            //@ApiParam(value = "${this.comment}；示例：1") @RequestParam(required = false) final Byte ${this.name},`
      default:
        return ''
    }
  }

  /**
   * 转换为查询条件
   * @returns {string}
   */
  toConditionParam() {
    switch (this.dataType.java) {
      case 'String':
      case 'Long':
      case 'Integer':
      case 'Short':
      case 'Byte':
        return `            //condition.set${upperFirst(this.name)}(${this.name});`
      default:
        return ''
    }
  }
}
