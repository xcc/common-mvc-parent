import Type from './Type'

/**
 * mysql 与 java 数据类型映射配置
 * @type {{FLOAT: Type, LONGTEXT: Type, DECIMAL: Type, CHAR: Type, BIGINT: Type, TEXT: Type, JSON: Type, MEDIUMINT: Type, MEDIUMTEXT: Type, INT: Type, DATE: Type, DATETIME: Type, SMALLINT: Type, TIMESTAMP: Type, VARCHAR: Type, DOUBLE: Type, TINYINT: Type}}
 */
const DataType = {
  BIGINT: new Type('BIGINT', 'Long', 'number', 1000),
  INT: new Type('INT', 'Integer', 'number', 1100),
  MEDIUMINT: new Type('MEDIUMINT', 'Integer', 'number', 1200),
  SMALLINT: new Type('SMALLINT', 'Short', 'number', 1300),
  TINYINT: new Type('TINYINT', 'Byte', 'TINYINT', 1400),
  DECIMAL: new Type('DECIMAL', 'BigDecimal', 'number', 1500),
  DOUBLE: new Type('DOUBLE', 'Double', 'number', 1600),
  FLOAT: new Type('FLOAT', 'Float', 'number', 1700),
  CHAR: new Type('CHAR', 'String', 'string', 2100),
  VARCHAR: new Type('VARCHAR', 'String', 'string', 2200),
  TEXT: new Type('TEXT', 'String', 'string'),
  MEDIUMTEXT: new Type('MEDIUMTEXT', 'String', 'string'),
  LONGTEXT: new Type('LONGTEXT', 'String', 'string'),
  TIMESTAMP: new Type('TIMESTAMP', 'Date', 'string', 3000),
  DATE: new Type('DATE', 'Date', 'string', 3100),
  DATETIME: new Type('DATETIME', 'Date', 'string', 3200),
  JSON: new Type('JSON', 'String', 'JSONArrayObject')
}
export default DataType
