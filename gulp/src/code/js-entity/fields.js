import DataType from '../core/DataType'

/**
 * 生成实体类属性字段定义代码
 * @param table {Table}
 * @returns {string}
 */
const fields = table => {
  const {columns} = table
  return columns.map(({name, dataType, comment}) => {
    if (table.isDeleteColumn(name)) {
      return '    /** \n     * 是否逻辑删除。 [0:否,1:是] \n     * @type {boolean} \n     */ \n    this.deleted = deleted'
    } else if (table.isCreateTimeColumn(name)) {
      return `    /**
     * ${comment}
     * @type {string}
     */
    this.createTime = createTime
    /**
     * ${comment}
     * @type {string}
     */
    this.createTimeFormat = createTimeFormat`
    } else if (table.isUpdateTimeColumn(name)) {
      return `    /**
     * ${comment}
     * @type {string}
     */
    this.updateTime = updateTime
    /**
     * ${comment}
     * @type {string}
     */
    this.updateTimeFormat = updateTimeFormat`
    } else if (dataType.javascript === DataType.TINYINT.javascript) {
      return `    /** \n     * ${comment.replace('@link ', '')}, FIXME: 该字段可能是枚举，需要修正 \n     * @type {${dataType.javascript}} \n     */ \n    this.${name} = ${name}`
    } else if (dataType.javascript === DataType.JSON.javascript) {
      return `    /** \n     * ${comment.replace('@link ', '')}, FIXME: 该字段可能是 Json 对象 或 Json 数组，需要修正 \n     * @type {${dataType.javascript}} \n     */ \n    this.${name} = ${name}`
    }
    return `    /** \n     * ${comment.replace('@link ', '')} \n     * @type {${dataType.javascript}} \n     */ \n    this.${name} = ${name}`
  }).join('\n')
}
export default fields
