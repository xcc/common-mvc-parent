import DataType from '../core/DataType'

/**
 * 实体构造函数参数说明
 * @param table {Table}
 * @returns {string}
 */
const param = table => {
  const {columns} = table
  return columns.map(({name, dataType, comment}) => {
    if (table.isDeleteColumn(name)) {
      return '   * @param deleted {boolean} 是否逻辑删除。 [0:否,1:是]'
    } else if (dataType.javascript === DataType.TINYINT.javascript) {
      return `   * @param ${name} {${dataType.javascript}} ${comment.replace('@link ', '')}, FIXME: 该字段可能是枚举，需要修正`
    } else if (dataType.javascript === DataType.JSON.javascript) {
      return `   * @param ${name} {${dataType.javascript}} ${comment.replace('@link ', '')}, FIXME: 该字段可能是 Json 对象 或 Json 数组，需要修正`
    }
    return `   * @param ${name} {${dataType.javascript}} ${comment.replace('@link ', '')}`
  }).join('\n')
}
export default param
