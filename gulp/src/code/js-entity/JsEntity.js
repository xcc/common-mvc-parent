import param from './param'
import args from './args'
import fields from './fields'

/**
 * JS 实体模板
 * @param table {Table}
 */
const JsEntity = table => {
  const {
    author,
    comment,
    date,
    existCreateUserIdColumn,
    existCreateTimeColumn,
    existUpdateUserIdColumn,
    existUpdateTimeColumn,
    names: {JavaName}
  } = table
  return `// <-
/** 
 * 实体：${comment} V20221105
 * @author ${author} on ${date}
 */
export default class ${JavaName} {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {${JavaName}}
   * @return {${JavaName}}
   */
  static of(obj) {
    return new ${JavaName}(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
${param(table)}
   * @param dv {string} 数据版本
   ${existCreateUserIdColumn ? '* @param createUserNickname {string} 创建用户昵称' : ''}
   ${existUpdateUserIdColumn ? '* @param createTimeFormat {string} 创建时间格式化，仅用于前端展示' : ''}
   ${existCreateTimeColumn ? '* @param updateUserNickname {string} 修改用户昵称' : ''}
   ${existUpdateTimeColumn ? '* @param updateTimeFormat {string} 修改时间格式化，仅用于前端展示' : ''}
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
${args(table)}
                dv = undefined,
                ${existCreateUserIdColumn ? 'createUserNickname = undefined,' : ''}
                ${existUpdateUserIdColumn ? 'createTimeFormat = undefined,' : ''}
                ${existCreateTimeColumn ? 'updateUserNickname = undefined,' : ''}
                ${existUpdateTimeColumn ? 'updateTimeFormat = undefined,' : ''}
                orderBy = undefined,
                page = undefined
              } = {}) {
${fields(table)}
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv
    ${existCreateUserIdColumn ? '    /**    \n* 创建用户昵称    \n* @type {string}    \n*/    \nthis.createUserNickname=createUserNickname' : ''}
    ${existUpdateUserIdColumn ? '    /**    \n* 创建时间格式化，仅用于前端展示    \n* @type {string}    \n*/    \nthis.createTimeFormat=createTimeFormat' : ''}
    ${existCreateTimeColumn ? '    /**    \n* 修改用户昵称    \n* @type {string}    \n*/    \nthis.updateUserNickname=updateUserNickname' : ''}
    ${existUpdateTimeColumn ? '    /**    \n* 修改时间格式化，仅用于前端展示    \n* @type {string}    \n*/    \nthis.updateTimeFormat=updateTimeFormat' : ''}
    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象。 前端组件输入框默认值为 "" 空字符串的情况，需要把数据类型不匹配的属性重置为 null
   * @return {${JavaName}}
   */
  clearBlankValue() {
    const obj = new ${JavaName}(JSON.parse(JSON.stringify(this)))
    ${table.clearBlankValue()}
    return obj
  }
}
`
}
export default JsEntity
