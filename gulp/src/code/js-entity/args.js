/**
 * 实体构造函数参数
 * @param table {Table}
 * @returns {string}
 */
const args = table => {
  const {columns} = table
  return columns.map(({name}) => {
    return `                ${name} = undefined,`
  }).join('\n')
}
export default args
