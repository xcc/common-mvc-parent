import DataType from '../../core/DataType'

const TINYINT_MIN_VALUE = -128
const TINYINT_MAX_VALUE = 127
const SHORT_MIN_VALUE = -32768
const SHORT_MAX_VALUE = 32767
const INTEGER_MIN_VALUE = -2147483648
const INTEGER_MAX_VALUE = 2147483647
const LONG_MIN_VALUE = -9223372036854775808
// eslint-disable-next-line no-loss-of-precision
const LONG_MAX_VALUE = 9223372036854775807

/**
 * 固定字段处理策略
 * ^.*\("\\\\t(.*)\\\\n.*
 *     $1
 * @deprecated
 */
export default class BaseAdapter {
  constructor() {
    this.fields = {
      // 默认字段生成策略
      default: ({index, name, db_name, dataType, notNullAndNotDefault, unsigned, length, fixed, comment}) => {
        const list = [`    @Column(name = "${db_name}")`]
        // if (notNullAndNotDefault) list.push('    @NotNull(groups = {ISave.class})')
        switch (dataType.mysql) {
          case 'TINYINT':
            list.push('    // FIXME: 请使用枚举修正属性数据类型，定义枚举之后记得删除　@Min　@Max 注解')
            if (unsigned) {
              list.push('    @Min(0)')
              if (length) list.push(`    @Max(${Math.min(TINYINT_MAX_VALUE, parseInt('9'.repeat(length)))})`)
            } else if (length) { // 有符号表示有负数，所以要除 2
              list.push(`    @Min(${Math.floor(Math.max(TINYINT_MIN_VALUE, parseInt('-'.concat('9'.repeat(length))) / 2))})`)
              list.push(`    @Max(${Math.floor(Math.min(TINYINT_MAX_VALUE, parseInt('9'.repeat(length)) / 2))})`)
            }
            break
          case 'SMALLINT':
            if (unsigned) {
              list.push('    @Min(0)')
              if (length) list.push(`    @Max(${Math.min(SHORT_MAX_VALUE, parseInt('9'.repeat(length)))})`)
            } else if (length) { // 有符号表示有负数，所以要除 2
              list.push(`    @Min(${Math.floor(Math.max(SHORT_MIN_VALUE, parseInt('-'.concat('9'.repeat(length))) / 2))})`)
              list.push(`    @Max(${Math.floor(Math.min(SHORT_MAX_VALUE, parseInt('9'.repeat(length)) / 2))})`)
            }
            break
          case 'MEDIUMINT':
          case 'INT':
            if (unsigned) {
              list.push('    @Min(0)')
              if (length) list.push(`    @Max(${Math.min(INTEGER_MAX_VALUE, parseInt('9'.repeat(length)))})`)
            } else if (length) { // 有符号表示有负数，所以要除 2
              list.push(`    @Min(${Math.floor(Math.max(INTEGER_MIN_VALUE, parseInt('-'.concat('9'.repeat(length))) / 2))})`)
              list.push(`    @Max(${Math.floor(Math.min(INTEGER_MAX_VALUE, parseInt('9'.repeat(length)) / 2))})`)
            }
            break
          case 'BIGINT':
            if (unsigned) {
              list.push('    @Min(0)')
              if (length) list.push(`    @DecimalMax("${Math.min(LONG_MAX_VALUE, parseInt('9'.repeat(length)))}")`)
            } else if (length) { // 有符号表示有负数，所以要除 2
              list.push(`    @DecimalMin("${Math.floor(Math.max(LONG_MIN_VALUE, parseInt('-'.concat('9'.repeat(length))) / 2))}")`)
              list.push(`    @DecimalMax("${Math.floor(Math.min(LONG_MAX_VALUE, parseInt('9'.repeat(length)) / 2))}")`)
            }
            break
          case 'DECIMAL':
          case 'DOUBLE':
          case 'FLOAT':
            list.push(`    @Digits(integer = ${length - (fixed || 0)}, fraction = ${fixed || 0})`)
            break
          case 'CHAR':
            list.push(`    @Size(min = ${length}, max = ${length})`)
            break
          case 'VARCHAR':
            list.push(`    @Size(max = ${length})`)
            break
          case 'TEXT':
            list.push(`    @Size(max = ${Math.min(65535, length || 65535)})`)
            break
          case 'MEDIUMTEXT':
            list.push(`    @Size(max = ${Math.min(65535 * 2, length || 65535 * 2)})`)
            break
          case 'LONGTEXT':
            list.push(`    @Size(max = ${Math.min(65535 * 4, length || 65535 * 4)})`)
            break
          case 'DATE':
            list.push('    @JsonFormat(pattern = "yyyy-MM-dd")')
            break
          case 'DATETIME':
          case 'TIMESTAMP':
            list.push('    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")')
            break
        }
        list.push(`    @ApiModelProperty(value = "${comment}", position = ${index})`)
        list.push(`    private ${dataType.java} ${name};`)
        return list.join('\n')
      },
      id: ({index, name, db_name, dataType, length}) => {
        if ([DataType.BIGINT.mysql, DataType.INT.mysql].includes(dataType.mysql)) {
          return `    @Id\n    @GeneratedValue(strategy = GenerationType.IDENTITY)\n    @Column(name = "${db_name}")\n    @Positive\n    @ApiModelProperty(value = "数据ID", position = ${index})\n    private Long ${name};`
        } else {
          return `    @Id\n    @GeneratedValue(generator = "base36")\n    @GenericGenerator(name = "base36", strategy = "com.common.db.generator.Base36Generator")\n    @Size(max = ${length})\n    @ApiModelProperty(value = "数据id", position = ${index})\n    private String ${name};`
        }
      },
      deleted: ({index, name, db_name}) => `    @Column(name = "${db_name}")\n    @ApiModelProperty(value = "是否逻辑删除。 [0:否,1:是]", position = ${index})\n    private Boolean ${name};`,
      createTime: ({index, name, db_name}) => `    @Column(name = "${db_name}")\n    @Size(min = 14, max = 14)\n    @ApiModelProperty(value = "数据新增时间，格式：yyyyMMddHHmmss", example = "20200202020202", position = ${index})\n    private String ${name};`,
      updateTime: ({index, name, db_name}) => `    @Column(name = "${db_name}")\n    @Size(min = 17, max = 17)\n    @ApiModelProperty(value = "数据最后一次更新时间，格式：yyyyMMddHHmmssSSS", example = "20200202020202002", position = ${index})\n    private String ${name};`,
      createUserId: ({index, name, db_name}) => `    @Column(name = "${db_name}")\n    @PositiveOrZero\n    @ApiModelProperty(value = "新增操作人id", position = ${index})\n    private Long ${name};`,
      updateUserId: ({index, name, db_name}) => `    @Column(name = "${db_name}")\n    @PositiveOrZero\n    @ApiModelProperty(value = "更新操作人id", position = ${index})\n    private Long ${name};`
    }
    this.where = {
      id: ({name}) => `                .and(${name}, () -> table.${name}.eq(${name}))`,
      deleted: () => '                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))'
    }
    this.props = {
      default: ({name, dataType, notNullAndNotDefault, comment}) => `        ${name}(${dataType.java.toUpperCase()}.build(${notNullAndNotDefault ? 'true, ' : ''}"${comment}"))`,
      id: ({name, dataType, comment}) => `        ${name}(${[DataType.BIGINT.mysql, DataType.INT.mysql].includes(dataType.mysql) ? 'LONG' : 'STRING'}.build(true, "${comment}"))`,
      deleted: ({name}) => `        ${name}(ENUM.build("是否逻辑删除"))`,
      createTime: ({name, comment}) => `        ${name}(TIMESTAMP.build("${comment}"))`,
      updateTime: (column) => this.props.createTime(column),
      createUserId: ({name, comment}) => `        ${name}(LONG.build("${comment}"))`,
      updateUserId: (column) => this.props.createUserId(column)
      // createUserNickname: ({
      //                    name,
      //                    comment
      //                  }) => `        ${name}(STRING.build("${comment}"))`,
      // updateUserNickname: (column) => this.props.createUserNickname(column)
    }
  }
}
