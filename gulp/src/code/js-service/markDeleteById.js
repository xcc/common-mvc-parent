/**
 *
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const markDeleteById = (table, uninstall = false) => {
  const {existDeletedColumn,names: {JavaName}} = table
  if (!existDeletedColumn) {
    return ''
  }
  return `// <-
  /**
   * 按 dv 逻辑删除
   * @param dv {string} {@link ${JavaName}#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteById(dv) {
    return await http
      .patch(markDeleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }
  /**
   * 按 dv 批量逻辑删除
   * @param dvs {string[]} {@link ${JavaName}#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteByIds(dvs) {
    return await http
      .patch(markDeleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const markDeleteByIdInstall = table => markDeleteById(table)
export const markDeleteByIdUninstall = table => markDeleteById(table, true)
