/**
 *
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const update = (table, uninstall = false) => {
  const {names: {JavaName}} = table
  return `// <-
  /**
   * 修改
   * @param vo {${JavaName}}
   * @return {Promise<Result>}
   */
  static async update(vo = new ${JavaName}()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const updateInstall = table => update(table)
export const updateUninstall = table => update(table, true)
