/**
 *
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const pageable = (table, uninstall = false) => {
  const {names: {JavaName}} = table
  return `// <-
  /**
   * 分页：多条件批量查询
   * @param vo {${JavaName}}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new ${JavaName}()) {
    const {page, ...params} = vo
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new ${JavaName}(row))
  }
`
}
export const pageableInstall = table => pageable(table)
export const pageableUninstall = table => pageable(table, true)
