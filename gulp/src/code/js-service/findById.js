/**
 *
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const findById = (table, uninstall = false) => {
  const {
    jsIdType,
    names: {JavaName}
  } = table
  return `// <-
  /**
   * 按 id 查询单条记录
   * @param id {string} {@link ${JavaName}#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new ${JavaName}(row))
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const findByIdInstall = table => findById(table)
export const findByIdUninstall = table => findById(table, true)
