/**
 *
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const list = (table, uninstall = false) => {
  const {names: {JavaName}} = table
  return `// <-
  /**
   * 多条件批量查询，不分页
   * @param vo {${JavaName}}
   * @return {Promise<Result>}
   */
  static async list(vo = new ${JavaName}()) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(listURL, {params: vo})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new ${JavaName}(row))
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const listInstall = table => list(table)
export const listUninstall = table => list(table, true)
