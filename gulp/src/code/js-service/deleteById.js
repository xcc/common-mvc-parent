/**
 *
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const deleteById = (table, uninstall = false) => {
  const {names: {JavaName}} = table
  return `// <-
  /**
   * 按 dv 删除
   * @param dv {string} {@link ${JavaName}#dv}
   * @return {Promise<Result>}
   */
  static async deleteById(dv) {
    return await http
      .delete(deleteByIdURL.format(dv))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }
  /**
   * 按 dv 批量删除
   * @param dvs {string[]} {@link ${JavaName}#dv}
   * @return {Promise<Result>}
   */
  static async deleteByIds(dvs) {
    return await http
      .delete(deleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const deleteByIdInstall = table => deleteById(table)
export const deleteByIdUninstall = table => deleteById(table, true)
