/**
 * @param table {Table}
 * @param uninstall {boolean} 是否在生成代码时注释该方法，用于备用
 * @returns {string}
 */
const save = (table, uninstall = false) => {
  const {names: {JavaName}} = table
  return `// <-
  /**
   * 新增
   * @param vo {${JavaName}}
   * @return {Promise<Result>}
   */
  static async save(vo = new ${JavaName}()) {
    const result = await http
      .post(saveURL, vo.clearBlankValue())
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new ${JavaName}(row))
  }
`.split('\n')
    .map(row => (uninstall ? '//' : '') + row)
    .join('\n')
}
export const saveInstall = table => save(table)
export const saveUninstall = table => save(table, true)
