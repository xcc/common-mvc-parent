/**
 * 生成 update 代码，实体动态更新
 * @param table {Table}
 * @param excludes {String[]} 指定不在更新范围的字段
 * @returns {string}
 */
const update = (table, excludes = []) => {
  excludes.push(...table.updateExcludeColumnNames)
  const {columns} = table
  return columns.map(column => {
    const {name} = column
    return excludes.includes(name)
      ? ''
      : `//                .then(${name}, update -> update.set(table.${name}, ${name}))`
  }).filter(Boolean).join('\n')
}
export default update
