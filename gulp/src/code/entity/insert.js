/**
 * 生成 insert 代码，实体动态更新
 * @param table {Table}
 * @returns {string}
 */
const insert = (table) => {
  const {columns} = table
  return columns.map(column => {
    const {name} = column
    return `                .then(${name}, insert -> insert.set(table.${name}, ${name}))`
  }).filter(Boolean).join('\n')
}
export default insert
