/**
 * 生成实体类属性字段定义代码
 * @param table {Table}
 * @returns {string}
 */
const fields = table => {
  const {columns} = table
  return columns.map(column => {
    const {comment} = column
    return `    /**\n     * ${comment}\n     */\n${column.toField()}`
  }).filter(Boolean).join('\n')
}
export default fields
