/**
 * JPA 实体模板
 * @param table {Table}
 */
import fields from './fields'
import orderBy from './orderBy'
import update from './update'
import where from './where'

/**
 * @param table {Table}
 */
const JpaEntity = table => {
  const {
    author,
    pkg,
    comment,
    idType,
    date,
    existUserIdColumn,
    existCreateUserIdColumn,
    existCreateTimeColumn,
    existUpdateUserIdColumn,
    existUpdateTimeColumn,
    existDeletedColumn,
    insertOptionalDefault,
    names: {
      TabName,
      tabName,
      tab_name,
      JavaName,
      pkgName
    }
  } = table
  return `// <-
package ${pkg}.business.${pkgName}.entity;
import app.common.starter.entity.OrderBy;
import app.common.starter.interfaces.*;
import app.common.starter.util.*;
import app.common.starter.dto.*;
import app.common.starter.entity.*;
import app.common.starter.enums.*;
import app.dao.jpa.starter.where.*;
import app.encrypt.domain.starter.annotations.*;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.*;
import com.querydsl.core.annotations.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import io.swagger.annotations.*;
import lombok.*;
import org.hibernate.annotations.*;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.*;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.google.common.collect.Lists;
import java.sql.Timestamp;
import java.math.BigDecimal;

import static ${pkg}.business.${pkgName}.entity.Q${TabName}.${tabName};
import static app.common.starter.util.Dates.Pattern.*;
import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 实体类：${comment} V20221105
 *
 * @author ${author} on ${date} 
 */
@Table(name = "${tab_name}")
@Entity
@QueryEntity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@ToString
@ApiModel(description = "${comment}")
public class ${TabName} implements
        // 所有与数据库表 - 实体类映射的表都实现该接口；方便后续一键查看所有表的实体
        // JPAUpdateClause => 需要的动态更新字段；采用 方案2 时需要实现该接口
        // QdslWhere       => 动态查询条件
        ITable<JPAUpdateClause, QdslWhere> 
{
    private static final long serialVersionUID = 1L;

${fields(table)}
    ${existCreateUserIdColumn ? '' : '//'}/**
    ${existCreateUserIdColumn ? '' : '//'} * 新增操作人昵称
    ${existCreateUserIdColumn ? '' : '//'} */
    ${existCreateUserIdColumn ? '' : '//'}@Transient
    ${existCreateUserIdColumn ? '' : '//'}@QueryTransient
    ${existCreateUserIdColumn ? '' : '//'}@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    ${existCreateUserIdColumn ? '' : '//'}@ApiModelProperty(value = "新增操作人昵称", accessMode = READ_ONLY)
    ${existCreateUserIdColumn ? '' : '//'}private String createUserNickname;
    ${existUpdateUserIdColumn ? '' : '//'}/**
    ${existUpdateUserIdColumn ? '' : '//'} * 更新操作人昵称
    ${existUpdateUserIdColumn ? '' : '//'} */
    ${existUpdateUserIdColumn ? '' : '//'}@Transient
    ${existUpdateUserIdColumn ? '' : '//'}@QueryTransient
    ${existUpdateUserIdColumn ? '' : '//'}@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    ${existUpdateUserIdColumn ? '' : '//'}@ApiModelProperty(value = "更新操作人昵称", accessMode = READ_ONLY)
    ${existUpdateUserIdColumn ? '' : '//'}private String updateUserNickname;
    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Set<${idType}> ids;
    /**
     * 排序字段
     */
    @QueryTransient
    @Transient
    @ApiModelProperty(value = "查询排序字段")
    private List<OrderBy> orderBy;
    
    /**
     * 防止踩坑。
     * 当 JPA 查询对象没有 clone 时，执行 setValue 操作时，会触发更新动作，这里拦截之后抛出异常
     */
    @PreUpdate
    public void onPreUpdate() {
        throw Code.A00003.toCodeException("防止踩坑，谨慎使用JPA默认的 update 方法。 如果业务逻辑确定需要使用 setValue 方式更新对象时，去掉这个异常");
    }

    /**
     * 数据版本号，用于更新和删除URL参数，规避脏数据更新
     *
     * @return String
     */
    @Transient
    @QueryTransient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY) // 实体类只读方法一定要加禁止反序列化注解
    @JsonSerialize(converter = app.encrypt.domain.starter.convert.AesDataVersionJsonConverter.Serializer.class)
    @ApiModelProperty(value = "数据版本号，用于更新和删除参数，规避脏数据更新", position = 0, accessMode = READ_ONLY)
    public String getDv() {
        return String.format("%s@%s", id, Optional.ofNullable(getUpdateTime()).orElse(""));
    }

    ${existCreateTimeColumn ? '' : '//'}/**
    ${existCreateTimeColumn ? '' : '//'} * 创建时间格式化，仅用于前端展示
    ${existCreateTimeColumn ? '' : '//'} *
    ${existCreateTimeColumn ? '' : '//'} * @return String
    ${existCreateTimeColumn ? '' : '//'} */
    ${existCreateTimeColumn ? '' : '//'}@Transient
    ${existCreateTimeColumn ? '' : '//'}@QueryTransient
    ${existCreateTimeColumn ? '' : '//'}@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    ${existCreateTimeColumn ? '' : '//'}@ApiModelProperty(value = "创建时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
    ${existCreateTimeColumn ? '' : '//'}public String getCreateTimeFormat() {
    ${existCreateTimeColumn ? '' : '//'}    return yyyyMMddHHmmss.parseOfNullable(createTime).map(yyyy_MM_dd_HH_mm_ss::format).orElse(null);
    ${existCreateTimeColumn ? '' : '//'}}

    ${existUpdateTimeColumn ? '' : '//'}/**
    ${existUpdateTimeColumn ? '' : '//'} * 修改时间格式化，仅用于前端展示
    ${existUpdateTimeColumn ? '' : '//'} *
    ${existUpdateTimeColumn ? '' : '//'} * @return String
    ${existUpdateTimeColumn ? '' : '//'} */
    ${existUpdateTimeColumn ? '' : '//'}@Transient
    ${existUpdateTimeColumn ? '' : '//'}@QueryTransient
    ${existUpdateTimeColumn ? '' : '//'}@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    ${existUpdateTimeColumn ? '' : '//'}@ApiModelProperty(value = "修改时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
    ${existUpdateTimeColumn ? '' : '//'}public String getUpdateTimeFormat() {
    ${existUpdateTimeColumn ? '' : '//'}    return yyyyMMddHHmmssSSS.parseOfNullable(updateTime).map(yyyy_MM_dd_HH_mm_ss_SSS::format).orElse(null);
    ${existUpdateTimeColumn ? '' : '//'}}
    
    @SneakyThrows
    public ${TabName} cloneObject() {
        return (${TabName}) super.clone();
    }

// Enum Start **********************************************************************************************************

    /**
     * 枚举：定义排序字段
     */
    public enum OrderByColumn {
        // 按 id 排序可替代按创建时间排序
${orderBy(table)},
        ;
        private final ComparableExpressionBase<?> column;
        public OrderBy asc() {
            return new OrderBy().setName(this.name());
        }
        public OrderBy desc() {
            return new OrderBy().setName(this.name()).setDirection(Sort.Direction.DESC);
        }
        /**
         * 获取所有排序字段名
         *
         * @return String[]
         */
        public static String[] names() {
            return Stream.of(OrderByColumn.values()).map(Enum::name).toArray(String[]::new);
        }
        OrderByColumn(final ComparableExpressionBase<?> column) {
            this.column = column;
        }
    }

// Enum End : DB Start *************************************************************************************************

    /**
     * DAO层新增数据时，设置字段默认值
     *
     * @param now    当前时间
     * ${existCreateUserIdColumn || existUpdateUserIdColumn ? '@param userId 当前操作用户id' : ''}
     */
    public void insert(final Dates now${existUserIdColumn || existCreateUserIdColumn || existUpdateUserIdColumn ? ', final Long userId' : ''}) {
        this.id = null;
        ${existDeletedColumn ? '' : '//'}this.deleted = Optional.ofNullable(this.deleted).orElse(false);
        ${existUserIdColumn ? '' : '//'}this.userId = userId;
        ${existCreateUserIdColumn ? '' : '//'}this.createUserId = userId;
        ${existCreateTimeColumn ? '' : '//'}this.createTime = now.format(yyyyMMddHHmmss);
        ${existUpdateUserIdColumn ? '' : '//'}this.updateUserId = userId;
        ${existUpdateTimeColumn ? '' : '//'}this.updateTime = now.format(yyyyMMddHHmmssSSS);
        ${insertOptionalDefault}
    }

    @Override
    public Then<JPAUpdateClause> update(final JPAUpdateClause jpaUpdateClause) {
        final Q${TabName} table = ${tabName};
        // 动态拼接 update 语句
        // 以下案例中 只有 name 属性 为 null 时才不会加入 update 语句；
        return Then.of(jpaUpdateClause)
        // FIXME: 请放开需要更新的字段，如果这个对象不存在更新逻辑，请删除这一行 FIXME 说明
${update(table)}
////                // 当 name != null 时更新 name 属性
////                .then(name, update -> update.set(table.name, name))
////                .then(update -> update.set(table.updateUserId, updateUserId))
////                // 假设数据库中 content is not null；可以在属性为null时替换为 ""
////                .then(update -> update.set(table.content, Optional.ofNullable(content).orElse("")))
////                // 数据库中 amount 可以为 null
////                .then(update -> update.set(table.amount, amount))
                ;
    }

    @Override
    public QdslWhere where() {
        final Q${TabName} table = ${tabName};
        // 构建查询顺序规则请参考：com.support.mvc.entity.IWhere#where
        return QdslWhere.of()
${where(table)}
////                .and(phone, () -> table.phone.eq(phone))
////                .and(createUserId, () -> table.createUserId.eq(createUserId))
////                .and(updateUserId, () -> table.updateUserId.eq(updateUserId))
////                // 强制带默认值的查询字段
////                .and(table.deleted.eq(Optional.ofNullable(deleted).orElse(false)))
////                // 数字区间查询
////                .and(amountRange, () -> table.amount.between(amountRange.getMin(), amountRange.getMax()))
////                // 日期区间查询；Range.rebuild() : 先将时间区间重置到 00:00:00.000 - 23:59:59.999 ; 大多数情况都需要重置时间
////                .and(createTimeRange, () -> ExpressionUtils.and(createTimeRange.rebuild().ifPresentCreateTimeBegin(table.createTime::goe), createTimeRange.ifPresentCreateTimeEnd(table.createTime::loe)))
////                .and(updateTimeRange, () -> ExpressionUtils.and(updateTimeRange.rebuild().ifPresentUpdateTimeBegin(table.createTime::goe), updateTimeRange.ifPresentUpdateTimeEnd(table.createTime::loe)))
////                // 模糊匹配查询：后面带 % ；建议优先使用
////                .and(name, () -> table.name.startsWith(name)) // 模糊匹配查询：后面带 %
////                .and(name, () -> table.name.endsWith(name)) // 模糊匹配查询：前面带 %
////                .and(name, () -> table.name.contains(name)) // 模糊匹配查询：前后带 %,同 MessageFormat.format("%{0}%", name)
////                .and(name, () -> table.name.like(MessageFormat.format("%{0}%", name))) 模糊匹配查询：前后带 %
                ;
    }

    /**
     * 排序参数构建：QueryDSL 查询模式；QueryDSL 模式构建排序对象返回 null 则会抛出异常
     *
     * @return OrderSpecifier[]
     */
    public OrderSpecifier<?>[] qdslOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return new OrderSpecifier<?>[]{OrderByColumn.id.column.asc()}; // 指定默认排序字段
                return new OrderSpecifier<?>[]{};
            }
            return orderBy.stream().map((OrderBy by) -> {
                final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                // 自定义排序
                // new OrderSpecifier<>(Order.valueOf(by.getDirection().name()) , Expressions.stringTemplate("convert_gbk({0})", column.column));
                
                // return Objects.equals(by.getDirection(), Sort.Direction.DESC) ? column.column.desc(): column.column.asc();
                return new OrderSpecifier<>(Order.valueOf(by.getDirection().name()), column.column);
            }).toArray(OrderSpecifier<?>[]::new);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 排序参数构建：Spring JPA 查询模式；JPA 模式构建排序对象返回 null 表示不排序
     *
     * @return {@link Sort}
     */
    public Sort jpaOrderBy() {
        try {
            if (CollectionUtils.isEmpty(orderBy)) {
                // return Sort.by(Sort.Direction.ASC, OrderByColumn.id.name()); // 指定默认排序字段
                return Sort.unsorted();
            }
            return orderBy.stream()
                    .map((OrderBy by) -> {
                        final OrderByColumn column = OrderByColumn.valueOf(by.getName());
                        // 自定义排序
                        // JpaSort.unsafe(by.getDirection(), String.format("convert_gbk(%s)", column.name()));
                        return Sort.by(by.getDirection(), column.name());
                    })
                    .reduce(Sort::and)
                    .orElseGet(Sort::unsorted);
        } catch (Exception e) {
            throw Code.A00003.toCodeException("排序字段可选范围：".concat(JSON.toJsonString(OrderByColumn.names())));
        }
    }

    /**
     * 获取查询实体与数据库表映射的所有字段,用于投影到 VO 类
     * 支持追加扩展字段,追加扩展字段一般用于连表查询
     *
     * @param appends {@link Expression}[] 追加扩展连表查询字段
     * @return {@link Expression}[]
     */
    public static Expression<?>[] allColumnAppends(final Expression<?>... appends) {
        final List<Expression<?>> columns = Lists.newArrayList(appends);
        final Class<?> clazz = ${tabName}.getClass();
        try {
            for (Field field : clazz.getDeclaredFields()) {
                if (field.getType().isPrimitive()) continue;
                final Object o = field.get(${tabName});
                if (o instanceof EntityPath || o instanceof BeanPath) continue;
                if (o instanceof Path) {
                    columns.add((Path<?>) o);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("获取查询实体属性与数据库映射的字段异常", e);
        }
        return columns.toArray(new Expression<?>[0]);
    }

// DB End **************************************************************************************************************

}
/* yml 缓存配置
spring:
  app:
    cache:
      # ${JavaName}Cache
      ${tab_name.replace(/_/g, '-')}: { expired: 30d, nullValueExpired: 1m }
*/
/*

import ${pkg}.business.${pkgName}.entity.${JavaName};
import app.cache.starter.interfaces.ITableCache;
import app.cache.starter.util.JsonCache;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static app.common.starter.AppContext.getBean;

\\/**
 * 缓存：${comment}
 *
 * @author ${author} on ${date} 
 *\\/
@Slf4j
@Component
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.cache.${tab_name.replace(/_/g, '-')}")
public class ${JavaName}Cache implements ITableCache<${idType}, ${JavaName}> {

    @JsonFilter("${JavaName}CacheObjectFieldIgnoreFilter")
    private interface ${JavaName}CacheObjectFieldIgnoreFilter {
    }

    static {
        // 注册自定义过滤器，排除缓存对象中的只读属性和不需要写入缓存的属性
        JsonCache.addFilter(${JavaName}CacheObjectFieldIgnoreFilter.class.getSimpleName()
                , SimpleBeanPropertyFilter.serializeAllExcept(
                        // 缓存序列化时排除字段,
                        "dv"
                        , "createTimeFormat"
                        , "updateTimeFormat"
                        , "createUserNickname"
                        , "updateUserNickname"
                )
        );
        JsonCache.get().addMixIn(${JavaName}.class, ${JavaName}CacheObjectFieldIgnoreFilter.class);
    }
    
    private static final String CACHE_KEY = ${JavaName}Cache.class.getSimpleName() + ":${table.isIdNumber() ? '%d' : '%s'}";
    private static volatile ${JavaName}Cache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    private final CacheRepository<${idType}, ${JavaName}> cacheRepository;

    @Override
    public CacheRepository<${idType}, ${JavaName}> getCacheRepository() {
        return cacheRepository;
    }

    @Override
    public Class<${JavaName}> getEntityClazz() {
        return ${JavaName}.class;
    }

    \\/**
     * 缓存过期时间
     *\\/
    @Setter
    @Getter
    private Duration expired;

    \\/**
     * 缓存过期时间
     *\\/
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;

    \\/**
     * 获取 {@link ${JavaName}Cache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link ${JavaName}Cache}
     *\\/
    public static ${JavaName}Cache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (${JavaName}Cache.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = getBean(${JavaName}Cache.class);
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.${tab_name.replace(/_/g, '-')}.expired: {}", expired);
        log.info("spring.app.cache.${tab_name.replace(/_/g, '-')}.nullValueExpired: {}", nullValueExpired);
    }

    @Override
    public String cacheKey(final ${idType} id) {
        return String.format(CACHE_KEY, id);
    }

    private RBucket<String> getBucket(final ${idType} id) {
        return redissonClient.getBucket(cacheKey(id));
    }

}

*/
`
}
export default JpaEntity
