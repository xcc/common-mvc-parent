/**
 * JPA 实体模板
 * @param table {Table}
 */

export const UpdateDTO = table => {
  const {author, pkg, comment, date, names: {TabName, pkgName}} = table
  return `// <-
package ${pkg}.business.${pkgName}.dto;

import ${pkg}.business.${pkgName}.entity.${TabName};
import app.common.starter.interfaces.*;
import app.common.starter.util.*;
import app.common.starter.dto.*;
import app.common.starter.entity.*;
import app.common.starter.enums.*;

import com.fasterxml.jackson.annotation.*;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.*;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAUpdateClause;
import io.swagger.annotations.*;
import lombok.*;
import org.hibernate.annotations.*;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import java.sql.Timestamp;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.*;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * DTO：${comment}修改 V20221105
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义${comment}修改时的扩展属性
 *
 * @author ${author} on ${date}
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "${comment}")
public class ${TabName}UpdateDTO extends ${TabName} {
    private static final long serialVersionUID = 1L;
    
    ${table.validateGetColumn()}
}`
}
export default UpdateDTO
