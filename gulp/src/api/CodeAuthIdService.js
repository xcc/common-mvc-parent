import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import CodeAuthId from './entity/CodeAuthId'

const pageURL = '/code-auth-id/page/{number}/{limit}' // 分页：多条件批量查询
// const listURL = '/code-auth-id' // 多条件批量查询，不分页
const findByIdURL = '/code-auth-id/{id}' // 按 id 查询单条记录
const saveURL = '/code-auth-id' // 新增
const updateURL = '/code-auth-id/{dv}' // 修改
// const deleteByIdURL = '/code-auth-id/{dv}' // 按 dv 删除
// const deleteByIdsURL = '/code-auth-id' // 按 dv 批量删除
const markDeleteByIdURL = '/code-auth-id/{dv}' // 按 dv 逻辑删除
const markDeleteByIdsURL = '/code-auth-id' // 按 dv 批量逻辑删除

/**
 * 后台服务请求：测试 AuthId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
export default class CodeAuthIdService {

  /**
   * 分页：多条件批量查询
   * @param vo {CodeAuthId}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new CodeAuthId()) {
    const {page, ...params} = vo
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeAuthId(row))
  }

//  /**
//   * 多条件批量查询，不分页
//   * @param vo {CodeAuthId}
//   * @return {Promise<Result>}
//   */
//  static async list(vo = new CodeAuthId()) {
//    /**
//     * @type {Result<any>}
//     */
//    const result = await http
//      .get(listURL, {params: vo})
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//    // 将响应结果转换为 js 类对象
//    return result.dataTransform(row => new CodeAuthId(row))
//  }
//

  /**
   * 按 id 查询单条记录
   * @param id {string} {@link CodeAuthId#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeAuthId(row))
  }

  /**
   * 新增
   * @param vo {CodeAuthId}
   * @return {Promise<Result>}
   */
  static async save(vo = new CodeAuthId()) {
    const result = await http
      .post(saveURL, vo.clearBlankValue())
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeAuthId(row))
  }

  /**
   * 修改
   * @param vo {CodeAuthId}
   * @return {Promise<Result>}
   */
  static async update(vo = new CodeAuthId()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 逻辑删除
   * @param dv {string} {@link CodeAuthId#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteById(dv) {
    return await http
      .patch(markDeleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 批量逻辑删除
   * @param dvs {string[]} {@link CodeAuthId#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteByIds(dvs) {
    return await http
      .patch(markDeleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

//  /**
//   * 按 dv 删除
//   * @param dv {string} {@link CodeAuthId#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteById(dv) {
//    return await http
//      .delete(deleteByIdURL.format(dv))
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//  /**
//   * 按 dv 批量删除
//   * @param dvs {string[]} {@link CodeAuthId#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteByIds(dvs) {
//    return await http
//      .delete(deleteByIdsURL, dvs)
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//
}
