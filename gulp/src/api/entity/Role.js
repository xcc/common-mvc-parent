/**
 * 实体：角色
 * @author 谢长春 2019-7-28
 */
export default class Role {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {Role}
   * @return {Role}
   */
  static of(obj) {
    return new Role(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据 ID
   * @param dv {string} 数据版本
   * @param name {string} 名称
   * @param remark {string} 备注
   * @param authorities {string[]} 权限指令代码集合，同 {@link Authority#code}
   * @param authorityTree {Authority[]} 权限指令树
   * @param createTime {string} 创建时间
   * @param createUserId {string} 创建用户ID
   * @param createUserNickname {string} 创建用户昵称
   * @param updateTime {string} 修改时间
   * @param updateUserId {string} 修改用户ID
   * @param updateUserNickname {string} 修改用户昵称
   * @param createTimeFormat {string} 创建时间格式化，仅用于前端展示
   * @param updateTimeFormat {string} 修改时间格式化，仅用于前端展示
   * @param deleted {boolean} 是否逻辑删除。 [0:否,1:是]
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
                id = undefined,
                dv = undefined,
                name = undefined,
                remark = undefined,
                authorities = undefined,
                authorityTree = undefined,
                createTime = undefined,
                createUserId = undefined,
                createUserNickname = undefined,
                updateTime = undefined,
                updateUserId = undefined,
                updateUserNickname = undefined,
                createTimeFormat = undefined,
                updateTimeFormat = undefined,
                deleted = undefined,
                orderBy = undefined,
                page = undefined
              } = {}) {
    /**
     * 数据 ID
     * @type {string}
     */
    this.id = id
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv
    /**
     * 名称
     * @type {string}
     */
    this.name = name
    /**
     * 备注
     * @type {string}
     */
    this.remark = remark
    /**
     * 权限指令代码集合，同 {@link Authority#code}
     * @type {string[]}
     */
    this.authorities = authorities
    /**
     * 权限指令树
     * @type {Authority[]}
     */
    this.authorityTree = authorityTree
    /**
     * 创建时间
     * @type {string}
     */
    this.createTime = createTime
    /**
     * 创建用户ID
     * @type {number}
     */
    this.createUserId = createUserId
    /**
     * 创建用户昵称
     * @type {string}
     */
    this.createUserNickname = createUserNickname
    /**
     * 修改时间
     * @type {string}
     */
    this.updateTime = updateTime
    /**
     * 修改用户ID
     * @type {number}
     */
    this.updateUserId = updateUserId
    /**
     * 修改用户昵称
     * @type {string}
     */
    this.updateUserNickname = updateUserNickname
    /**
     * 创建时间格式化，仅用于前端展示
     * @type {string}
     */
    this.createTimeFormat = createTimeFormat
    /**
     * 修改时间格式化，仅用于前端展示
     * @type {string}
     */
    this.updateTimeFormat = updateTimeFormat
    /**
     * 是否逻辑删除。 [0:否,1:是]
     * @type {boolean}
     */
    this.deleted = deleted
    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象
   * @return {Role}
   */
  clearBlankValue() {
    const obj = new Role(JSON.parse(JSON.stringify(this)))
    if (obj.id === '') delete obj.id
    if (obj.createUserId === '') delete obj.createUserId
    if (obj.updateUserId === '') delete obj.updateUserId
    return obj
  }
}
