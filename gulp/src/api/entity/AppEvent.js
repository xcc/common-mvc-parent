/**
 * 实体：埋点表 V20221105
 * @author 谢长春 on 2022-06-27
 */
export default class AppEvent {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {AppEvent}
   * @return {AppEvent}
   */
  static of(obj) {
    return new AppEvent(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据ID，主键自增
   * @param event {number} 埋点事件，参考：AppEvent
   * @param eventTime {string} 埋点时间，yyyyMMddHHmmss
   * @param data {object} 埋点附加数据
   * @param userId {number} 用户ID。 tab_user.id
   * @param refLongId {number} 埋点关联数据id，用于简单业务埋点
   * @param refStringId {string} 埋点关联数据id，用于简单业务埋点
   * @param dv {string} 数据版本
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
                id = undefined,
                event = undefined,
                eventTime = undefined,
                data = undefined,
                userId = undefined,
                refLongId = undefined,
                refStringId = undefined,
                dv = undefined,
                orderBy = undefined,
                page = undefined
              } = {}) {
    /**
     * 数据ID，主键自增
     * @type {number}
     */
    this.id = id
    /**
     * 埋点事件，参考：AppEvent
     * @type {number}
     */
    this.event = event
    /**
     * 埋点时间，yyyyMMddHHmmss
     * @type {string}
     */
    this.eventTime = eventTime
    /**
     * 埋点附加数据
     * @type {object}
     */
    this.data = data
    /**
     * 用户ID。 tab_user.id
     * @type {number}
     */
    this.userId = userId
    /**
     * 埋点关联数据id，用于简单业务埋点
     * @type {number}
     */
    this.refLongId = refLongId
    /**
     * 埋点关联数据id，用于简单业务埋点
     * @type {string}
     */
    this.refStringId = refStringId
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv

    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象。 前端组件输入框默认值为 "" 空字符串的情况，需要把数据类型不匹配的属性重置为 null
   * @return {AppEvent}
   */
  clearBlankValue() {
    const obj = new AppEvent(JSON.parse(JSON.stringify(this)))
    if (obj.id === '') delete obj.id
    if (obj.event === '') delete obj.event
    if (obj.userId === '') delete obj.userId
    if (obj.refLongId === '') delete obj.refLongId
    return obj
  }
}
