/**
 * 实体：测试 SearchId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
export default class CodeOpenSearchId {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {CodeOpenSearchId}
   * @return {CodeOpenSearchId}
   */
  static of(obj) {
    return new CodeOpenSearchId(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据ID，主键
   * @param fromId {number} fromId
   * @param toId {number} toId
   * @param dv {string} 数据版本




   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
                id = undefined,
                fromId = undefined,
                toId = undefined,
                dv = undefined,

                orderBy = undefined,
                page = undefined
              } = {}) {
    /**
     * 数据ID，主键
     * @type {string}
     */
    this.id = id
    /**
     * fromId
     * @type {number}
     */
    this.fromId = fromId
    /**
     * toId
     * @type {number}
     */
    this.toId = toId
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv

    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象。 前端组件输入框默认值为 "" 空字符串的情况，需要把数据类型不匹配的属性重置为 null
   * @return {CodeOpenSearchId}
   */
  clearBlankValue() {
    const obj = new CodeOpenSearchId(JSON.parse(JSON.stringify(this)))
    if (obj.fromId === '') delete obj.fromId
    if (obj.toId === '') delete obj.toId
    return obj
  }
}
