/**
 * 实体：参考案例：实体表操作
 * @author 谢长春 2019-7-28
 */
export default class CodeExample {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {CodeExample}
   * @return {CodeExample}
   */
  static of(obj) {
    return new CodeExample(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据 ID
   * @param dv {string} 数据版本
   * @param name {string} 名称
   * @param content {string} 内容
   * @param amount {number} 金额
   * @param status {string} 状态，参考 {@link DemoStatus}.*.value
   * @param createTime {string} 创建时间
   * @param createUserId {string} 创建用户ID
   * @param createUserNickname {string} 创建用户昵称
   * @param updateTime {string} 修改时间
   * @param updateUserId {string} 修改用户ID
   * @param updateUserNickname {string} 修改用户昵称
   * @param deleted {boolean} 是否逻辑删除。 [0:否,1:是]
   * @param amountRange {NumRange} 金额查询区间
   * @param createTimeRange {DateRange} 创建时间查询区间
   * @param statusArray {DemoStatus[]} VO查询参数：状态集合
   * @param updateTimeRange {DateRange} VO查询参数：更新时间区间
   * @param createUser {User} VO扩展参数：数据新增用户信息
   * @param updateUser {User} VO扩展参数：数据修改用户信息
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
                id = undefined,
                dv = undefined,
                name = undefined,
                content = undefined,
                amount = undefined,
                status = undefined,
                createTime = undefined,
                createUserId = undefined,
                createUserNickname = undefined,
                updateTime = undefined,
                updateUserId = undefined,
                updateUserNickname = undefined,
                deleted = undefined,
                amountRange = undefined,
                createTimeRange = undefined,
                statusArray = undefined,
                updateTimeRange = undefined,
                createUser = undefined,
                updateUser = undefined,
                orderBy = undefined,
                page = undefined
              } = {}) {
    /**
     * 数据 ID
     * @type {number}
     */
    this.id = id
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv
    /**
     * 名称
     * @type {string}
     */
    this.name = name
    /**
     * 内容
     * @type {string}
     */
    this.content = content
    /**
     * 金额
     * @type {number}
     */
    this.amount = amount
    /**
     * 状态，参考 {@link DemoStatus}.*.value
     * @type {string}
     */
    this.status = status
    /**
     * 创建时间
     * @type {string}
     */
    this.createTime = createTime
    /**
     * 创建用户ID
     * @type {number}
     */
    this.createUserId = createUserId
    /**
     * 创建用户昵称
     * @type {string}
     */
    this.createUserNickname = createUserNickname
    /**
     * 修改时间
     * @type {string}
     */
    this.updateTime = updateTime
    /**
     * 修改用户ID
     * @type {number}
     */
    this.updateUserId = updateUserId
    /**
     * 修改用户昵称
     * @type {string}
     */
    this.updateUserNickname = updateUserNickname
    /**
     * 是否逻辑删除。 [0:否,1:是]
     * @type {boolean}
     */
    this.deleted = deleted
    /**
     * 金额查询区间
     * @type {NumRange}
     */
    this.amountRange = amountRange
    /**
     * 创建时间查询区间
     * @type {DateRange}
     */
    this.createTimeRange = createTimeRange
    /**
     * VO查询参数：状态集合
     * @type {DemoStatus[]}
     */
    this.statusArray = statusArray
    /**
     * VO查询参数：更新时间区间
     * @type {DateRange}
     */
    this.updateTimeRange = updateTimeRange
    /**
     * VO扩展参数：数据新增用户信息
     * @type {User}
     */
    this.createUser = createUser
    /**
     * VO扩展参数：数据修改用户信息
     * @type {User}
     */
    this.updateUser = updateUser
    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象。 前端组件输入框默认值为 "" 空字符串的情况，需要把数据类型不匹配的属性重置为 null
   * @return {CodeExample}
   */
  clearBlankValue() {
    const obj = new CodeExample(JSON.parse(JSON.stringify(this)))
    if (obj.id === '') delete obj.id
    if (obj.amount === '') delete obj.amount
    if (obj.status === '') delete obj.status
    if (obj.createUserId === '') delete obj.createUserId
    if (obj.updateUserId === '') delete obj.updateUserId
    if (obj.deleted === '') delete obj.deleted
    return obj
  }
}
