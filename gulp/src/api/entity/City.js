/**
 * 实体：城市表 V20221105
 * @author 谢长春 on 2022-06-28
 */
export default class City {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {City}
   * @return {City}
   */
  static of(obj) {
    return new City(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据ID
   * @param pid {string} 父节点ID
   * @param name {string} 名称
   * @param py {string} 名称拼音首字母
   * @param pinyin {string} 名称拼音
   * @param dv {string} 数据版本
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
                id = undefined,
                pid = undefined,
                name = undefined,
                py = undefined,
                pinyin = undefined,
                dv = undefined,
                orderBy = undefined,
                page = undefined
              } = {}) {
    /**
     * 数据ID，主键自增
     * @type {string}
     */
    this.id = id
    /**
     * 父节点ID
     * @type {string}
     */
    this.pid = pid
    /**
     * 名称
     * @type {string}
     */
    this.name = name
    /**
     * 名称拼音首字母
     * @type {string}
     */
    this.py = py
    /**
     * 名称拼音
     * @type {string}
     */
    this.pinyin = pinyin
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv
    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象。 前端组件输入框默认值为 "" 空字符串的情况，需要把数据类型不匹配的属性重置为 null
   * @return {City}
   */
  clearBlankValue() {
    const obj = new City(JSON.parse(JSON.stringify(this)))
    return obj
  }
}
