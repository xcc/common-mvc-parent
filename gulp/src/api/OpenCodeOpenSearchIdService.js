import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import CodeOpenSearchId from './entity/CodeOpenSearchId'

const pageURL = '/open/code-open-search-id/page/{number}/{limit}' // 分页：多条件批量查询
// const listURL = '/open/code-open-search-id' // 多条件批量查询，不分页
const findByIdURL = '/open/code-open-search-id/{id}' // 按 id 查询单条记录

/**
 * 后台服务请求：测试 SearchId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
export default class OpenCodeOpenSearchIdService {

  /**
   * 分页：多条件批量查询
   * @param vo {CodeOpenSearchId}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new CodeOpenSearchId()) {
    const {page, ...params} = vo
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeOpenSearchId(row))
  }

//  /**
//   * 多条件批量查询，不分页
//   * @param vo {CodeOpenSearchId}
//   * @return {Promise<Result>}
//   */
//  static async list(vo = new CodeOpenSearchId()) {
//    /**
//     * @type {Result<any>}
//     */
//    const result = await http
//      .get(listURL, {params: vo})
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//    // 将响应结果转换为 js 类对象
//    return result.dataTransform(row => new CodeOpenSearchId(row))
//  }
//

  /**
   * 按 id 查询单条记录
   * @param id {string} {@link CodeOpenSearchId#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeOpenSearchId(row))
  }

}
