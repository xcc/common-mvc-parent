import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import CodeExample from './entity/CodeExample'

const saveURL = '/code-example' // 新增
const updateURL = '/code-example/{dv}' // 修改
const deleteByIdURL = '/code-example/{dv}' // 按 dv 删除
const markDeleteByIdURL = '/code-example/{dv}' // 按 dv 逻辑删除
const markDeleteByIdsURL = '/code-example' // 按 dv 批量逻辑删除
const findByIdURL = '/code-example/{id}' // 按 id 查询单条记录
const listURL = '/code-example' // 多条件批量查询，不分页
const pageURL = '/code-example/page/{number}/{limit}' // 分页：多条件批量查询
const listVoURL = '/code-example/vo' // 多条件批量查询，不分页，投影到 VO 类
const pageVoURL = '/code-example/page/vo/{number}/{limit}' // 分页：多条件批量查询，投影到 VO 类

/**
 * 后台服务请求：参考案例：实体表操作
 * @author 谢长春 2019-7-28
 */
export default class CodeExampleService {
  /**
   * 新增
   * @param vo {CodeExample}
   * @return {Promise<Result>}
   */
  static async save(vo = new CodeExample()) {
    return await http
      .post(saveURL, vo.clearBlankValue())
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 修改
   * @param vo {CodeExample}
   * @return {Promise<Result>}
   */
  static async update(vo = new CodeExample()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 删除
   * @param dv {string} {@link CodeExample#dv}
   * @return {Promise<Result>}
   */
  static async deleteById(dv) {
    return await http
      .delete(deleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 逻辑删除
   * @param dv {string} {@link CodeExample#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteById(dv) {
    return await http
      .patch(markDeleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 批量逻辑删除
   * @param dvs {string[]} {@link CodeExample#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteByIds(dvs) {
    return await http
      .patch(markDeleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 id 查询单条记录
   * @param id {string} {@link CodeExample#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new CodeExample(row))
  }

  /**
   * 多条件批量查询，不分页
   * @param vo {CodeExample}
   * @return {Promise<Result>}
   */
  static async list(vo = new CodeExample()) {
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(listURL, {params: vo})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new CodeExample(row))
  }

  /**
   * 分页：多条件批量查询
   * @param vo {CodeExample}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new CodeExample()) {
    const {page, ...params} = vo
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new CodeExample(row))
  }

  /**
   * 多条件批量查询，不分页
   * @param vo {CodeExample}
   * @return {Promise<Result>}
   */
  static async listVO(vo = new CodeExample()) {
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(listVoURL, {params: vo})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new CodeExample(row))
  }

  /**
   * 分页：多条件批量查询
   * @return {Promise<Result>}
   */
  static async pageableVO(vo = new CodeExample()) {
    const {page, ...params} = vo
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(pageVoURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new CodeExample(row))
  }
}
