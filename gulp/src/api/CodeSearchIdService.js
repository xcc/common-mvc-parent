import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import CodeSearchId from './entity/CodeSearchId'

const pageURL = '/code-search-id/page/{number}/{limit}' // 分页：多条件批量查询
// const listURL = '/code-search-id' // 多条件批量查询，不分页
const findByIdURL = '/code-search-id/{id}' // 按 id 查询单条记录

/**
 * 后台服务请求：测试 SearchId 模板 V20221105
 * @author 谢长春 on 2022-11-04
 */
export default class CodeSearchIdService {

  /**
   * 分页：多条件批量查询
   * @param vo {CodeSearchId}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new CodeSearchId()) {
    const {page, ...params} = vo
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeSearchId(row))
  }

//  /**
//   * 多条件批量查询，不分页
//   * @param vo {CodeSearchId}
//   * @return {Promise<Result>}
//   */
//  static async list(vo = new CodeSearchId()) {
//    /**
//     * @type {Result<any>}
//     */
//    const result = await http
//      .get(listURL, {params: vo})
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//    // 将响应结果转换为 js 类对象
//    return result.dataTransform(row => new CodeSearchId(row))
//  }
//

  /**
   * 按 id 查询单条记录
   * @param id {string} {@link CodeSearchId#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new CodeSearchId(row))
  }

}
