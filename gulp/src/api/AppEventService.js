import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import AppEvent from './entity/AppEvent'

const pageURL = '/app-event/page/{number}/{limit}' // 分页：多条件批量查询
// const listURL = '/app-event' // 多条件批量查询，不分页
const findByIdURL = '/app-event/{id}' // 按 id 查询单条记录
const saveURL = '/app-event' // 新增
const updateURL = '/app-event/{dv}' // 修改
// const deleteByIdURL = '/app-event/{dv}' // 按 dv 删除
// const deleteByIdsURL = '/app-event' // 按 dv 批量删除
// const markDeleteByIdURL = '/app-event/{dv}' // 按 dv 逻辑删除
// const markDeleteByIdsURL = '/app-event' // 按 dv 批量逻辑删除
process.env.VUE_APP_AES_ENABLED
/**
 * 后台服务请求：埋点表 V20221105
 * @author 谢长春 on 2022-06-27
 */
export default class AppEventService {
  /**
   * 分页：多条件批量查询
   * @param vo {AppEvent}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new AppEvent()) {
    const {page, ...params} = vo
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new AppEvent(row))
  }

//  /**
//   * 多条件批量查询，不分页
//   * @param vo {AppEvent}
//   * @return {Promise<Result>}
//   */
//  static async list(vo = new AppEvent()) {
//    /**
//     * @type {Result<any>}
//     */
//    const result = await http
//      .get(listURL, {params: vo})
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//    // 将响应结果转换为 js 类对象
//    return result.dataTransform(row => new AppEvent(row))
//  }
//
//
//   /**
//    * 按 id 查询单条记录
//    * @param id {string} {@link AppEvent#id}
//    * @return {Promise<Result>}
//    */
//   static async findById(id) {
//     /**
//      * @type {Result<any>}
//      */
//     const result = await http
//       .get(findByIdURL.format(id || 0))
//       .then(Result.ofResponse)
//       .catch(Result.ofCatch)
//     // 将响应结果转换为 js 类对象
//     return result.dataTransform(row => new AppEvent(row))
//   }
//
//   /**
//    * 新增
//    * @param vo {AppEvent}
//    * @return {Promise<Result>}
//    */
//   static async save(vo = new AppEvent()) {
//     const result = await http
//       .post(saveURL, vo.clearBlankValue())
//       .then(Result.ofResponse)
//       .catch(Result.ofCatch)
//     // 将响应结果转换为 js 类对象
//     return result.dataTransform(row => new AppEvent(row))
//   }
  //
  // /**
  //  * 修改
  //  * @param vo {AppEvent}
  //  * @return {Promise<Result>}
  //  */
  // static async update(vo = new AppEvent()) {
  //   const {id, dv, ...body} = vo
  //   return await http
  //     .put(updateURL.format(dv || 0), body)
  //     .then(Result.ofResponse)
  //     .catch(Result.ofCatch)
  // }

  // /**
  //  * 按 dv 逻辑删除
  //  * @param dv {string} {@link AppEvent#dv}
  //  * @return {Promise<Result>}
  //  */
  // static async markDeleteById(dv) {
  //   return await http
  //     .patch(markDeleteByIdURL.format(dv || 0))
  //     .then(Result.ofResponse)
  //     .catch(Result.ofCatch)
  // }
  // /**
  //  * 按 dv 批量逻辑删除
  //  * @param dvs {string[]} {@link AppEvent#dv}
  //  * @return {Promise<Result>}
  //  */
  // static async markDeleteByIds(dvs) {
  //   return await http
  //     .patch(markDeleteByIdsURL, dvs)
  //     .then(Result.ofResponse)
  //     .catch(Result.ofCatch)
  // }

//  /**
//   * 按 dv 删除
//   * @param dv {string} {@link AppEvent#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteById(dv) {
//    return await http
//      .delete(deleteByIdURL.format(id || 0))
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//  /**
//   * 按 dv 批量删除
//   * @param dvs {string[]} {@link AppEvent#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteByIds(dvs) {
//    return await http
//      .delete(deleteByIdsURL, dvs)
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//
}
