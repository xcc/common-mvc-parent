import http from './http'
import Result from '../utils/entity/Result'

const stringKeyURL = '/admin-config/cache/string/{name}/{key}' // 字符串作为缓存key
const numberKeyURL = '/admin-config/cache/long/{name}/{key}' // 数字作为缓存key

/**
 * 后台服务请求：缓存查看
 * @author 谢长春 2020-04-02
 */
export default class CacheService {
  /**
   * 查询缓存
   * @param moduleName {String} 缓存模块前缀
   * @param key {String} 缓存 key
   * @return {Promise<Result>}
   */
  static async stringKey(moduleName, key) {
    return await http
      .get(stringKeyURL.format(moduleName, key))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 查询缓存
   * @param moduleName {String} 缓存模块前缀
   * @param key {number} 缓存 key
   * @return {Promise<Result>}
   */
  static async numberKey(moduleName, key) {
    return await http
      .get(numberKeyURL.format(moduleName, key))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }
}
