import http from './http'
import Result from '../utils/entity/Result'
import City from './entity/City'

const listByPid = '/city/list/{pid}' // 按编码查询子节点，根节点传空字符串
// const listURL = '/city' // 多条件批量查询，不分页
const findByIdURL = '/city/{id}' // 按 id 查询单条记录
const saveURL = '/city' // 新增
const updateURL = '/city/{dv}' // 修改
// const deleteByIdURL = '/city/{dv}' // 按 dv 删除
// const deleteByIdsURL = '/city' // 按 dv 批量删除
// const markDeleteByIdURL = '/city/{dv}' // 按 dv 逻辑删除
// const markDeleteByIdsURL = '/city' // 按 dv 批量逻辑删除

/**
 * 后台服务请求：城市表 V20221105
 * @author 谢长春 on 2022-06-28
 */
export default class CityService {
  /**
   * 按编码查询子节点，根节点传空字符串
   * @param pid {string} {@link City#pid}
   * @return {Promise<Result>}
   */
  static async listByPid(pid) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(listByPid.format(pid || ''))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new City(row))
  }

//  /**
//   * 多条件批量查询，不分页
//   * @param vo {City}
//   * @return {Promise<Result>}
//   */
//  static async list(vo = new City()) {
//    /**
//     * @type {Result<any>}
//     */
//    const result = await http
//      .get(listURL, {params: vo})
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//    // 将响应结果转换为 js 类对象
//    return result.dataTransform(row => new City(row))
//  }
//

  /**
   * 按 id 查询单条记录
   * @param id {string} {@link City#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new City(row))
  }

  /**
   * 新增
   * @param vo {City}
   * @return {Promise<Result>}
   */
  static async save(vo = new City()) {
    const result = await http
      .post(saveURL, vo.clearBlankValue())
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new City(row))
  }

  /**
   * 修改
   * @param vo {City}
   * @return {Promise<Result>}
   */
  static async update(vo = new City()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  // /**
  //  * 按 dv 逻辑删除
  //  * @param dv {string} {@link City#dv}
  //  * @return {Promise<Result>}
  //  */
  // static async markDeleteById(dv) {
  //   return await http
  //     .patch(markDeleteByIdURL.format(dv || 0))
  //     .then(Result.ofResponse)
  //     .catch(Result.ofCatch)
  // }
  // /**
  //  * 按 dv 批量逻辑删除
  //  * @param dvs {string[]} {@link City#dv}
  //  * @return {Promise<Result>}
  //  */
  // static async markDeleteByIds(dvs) {
  //   return await http
  //     .patch(markDeleteByIdsURL, dvs)
  //     .then(Result.ofResponse)
  //     .catch(Result.ofCatch)
  // }

//  /**
//   * 按 dv 删除
//   * @param dv {string} {@link City#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteById(dv) {
//    return await http
//      .delete(deleteByIdURL.format(id || 0))
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//  /**
//   * 按 dv 批量删除
//   * @param dvs {string[]} {@link City#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteByIds(dvs) {
//    return await http
//      .delete(deleteByIdsURL, dvs)
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//
}
