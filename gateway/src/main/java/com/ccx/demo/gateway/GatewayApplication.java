package com.ccx.demo.gateway;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.retry.annotation.EnableRetry;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Slf4j
@SpringBootApplication
@EnableRetry
@EnableConfigurationProperties
public class GatewayApplication {
    public static void main(String[] args) {
//        // 客户端缓存目录: com.alibaba.nacos.client.naming.NacosNamingService
//        System.setProperty("com.alibaba.nacos.naming.cache.dir", "/tmp/nacos/cache");
//        // 客户端日志的目录: com.alibaba.nacos.client.logging.AbstractNacosLogging.NACOS_LOGGING_PATH_PROPERTY
//        System.setProperty("nacos.logging.path", "/tmp/nacos/logs");
        final SpringApplication application = new SpringApplication(GatewayApplication.class);
        // 开启Banner打印方式(OFF：关闭，CONSOLE：控制台输出，LOG：日志输出)
        // 特效字生成 https://www.bootschool.net/qrcode-terminal
        application.setBannerMode(Banner.Mode.CONSOLE);
        application.addListeners(
                (ApplicationListener<ApplicationEnvironmentPreparedEvent>) event -> {
                    final Properties properties = System.getProperties();
                    final List<String> props = new ArrayList<>();
                    properties.forEach((key, value) -> props.add(String.format("%s: %s", key, value)));
                    log.info("\n{}\n* 系统环境变量\n* {}\n{}"
                            , StringUtils.repeat("*", 100)
                            , StringUtils.join(props, "\n* ")
                            , StringUtils.repeat("*", 100)
                    );
                }
//                (ApplicationListener<ApplicationContextInitializedEvent>) event -> {
//                },
//                (ApplicationListener<ApplicationPreparedEvent>) event -> {
//                }
        );
        application.run(args);
    }

}
