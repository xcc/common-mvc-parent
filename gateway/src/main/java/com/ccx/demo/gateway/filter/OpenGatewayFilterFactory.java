package com.ccx.demo.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

/**
 * /open 前缀的接口不用token也可以访问
 * 网关过滤器：https://docs.spring.io/spring-cloud-gateway/docs/current/reference/html
 *
 * @author 谢长春 2022-11-16
 */
@Slf4j
public class OpenGatewayFilterFactory extends AbstractTokenGatewayFilterFactory {
    public OpenGatewayFilterFactory(ApplicationContext applicationContext) {
        super(applicationContext);
    }

    @Override
    protected boolean assertToken() {
        return false;
    }
}
