package com.ccx.demo.gateway.vo;

import app.enums.starter.TokenClient;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.util.Objects;

/**
 * 自定义 token 认证对象：只验证 token ， 从 token 解析用户信息，不查用户完整信息，用于微服务认证
 *
 * @author 谢长春 2019-12-16
 */
@NoArgsConstructor
@Getter
@Setter
@Slf4j
@ToString
@Accessors(chain = true)
public class TokenUserVO implements Serializable {
    private static final long serialVersionUID = 4742500780168126655L;
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    /**
     * 密钥， 长度:32字节(256位)
     *
     * @alias RandomStringUtils.randomAlphanumeric(32)
     * @alias RandomStringUtils.randomAscii(32)
     */
    private static final SecretKeySpec SK;
    /**
     * 固定 IV 。 设置固定 IV 之后相同内容每次加密结果一样
     */
    private static final IvParameterSpec IV;

    static {
        try {
            final String md5 = DigestUtils.md5Hex(TokenUserVO.class.getSimpleName() + Objects.toString(serialVersionUID));
            SK = new SecretKeySpec(md5.getBytes(), "AES");
            IV = new IvParameterSpec(StringUtils.right(md5, 16).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * aes 解码并转换为 {@link TokenUserVO} 实体，执行 token 认证，认证失败会抛出异常，认证成功则更新 token 过期时间
     *
     * @param token {@link String} HTTP 请求头中获取到的 token
     * @return {@link TokenUserVO}
     */
    @SneakyThrows
    public static TokenUserVO parse(final String token) {
        if (log.isInfoEnabled()) {
            log.info("x-token: {}", token);
        }
        if (StringUtils.isBlank(token)) {
            throw new NullPointerException("token 不能为空");
        }
        try {
            // 初始化解密参数，设置为解密模式，指定密钥，设置IV
            final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, SK, IV);
//        final String payload = new String(cipher.doFinal(org.apache.commons.codec.binary.Hex.decodeHex(token)));
            final String payload = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(token)));
            if (log.isDebugEnabled()) {
                log.debug("{} => {}", token, payload);
            }
            // 解析 payload 内容
            final String[] payloads = payload.split("@");
            return new TokenUserVO()
                    .setClient(TokenClient.values()[Integer.parseInt(payloads[0])])
                    .setTimestamp(Long.parseLong(payloads[1]))
                    .setUserId(Long.parseLong(payloads[2]))
                    .setUsername(payloads[3])
                    .setPhone(payloads[4])
                    ;
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("token 解析失败: %s", token), e);
        }
    }

    /**
     * 请求头携带的 x-token
     */
    private String token;
    /**
     * 用户真实IP地址
     */
    private String ipAddress;
    /**
     * 登录设备
     */
    private TokenClient client;
    /**
     * 用户 ID
     */
    private long userId;
    /**
     * 用户登录名
     */
    private String username;
    /**
     * 用户手机号
     */
    private String phone;
    /**
     * 授权时间
     */
    private Long timestamp;
    /**
     * 登录过期时间
     */
    private Long expired;


}
