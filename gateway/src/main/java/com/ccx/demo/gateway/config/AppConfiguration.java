package com.ccx.demo.gateway.config;

import com.ccx.demo.gateway.filter.OpenGatewayFilterFactory;
import com.ccx.demo.gateway.filter.TokenGatewayFilterFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.Optional;

import static com.ccx.demo.gateway.filter.TokenGatewayFilterFactory.HEADER_X_TOKEN;

/**
 * 应用基础配置
 *
 * @author 谢长春 2022-11-16
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class AppConfiguration {
    private final ApplicationContext applicationContext;

    /**
     * 网关过滤器，断言 token 有效性
     *
     * @return {@link TokenGatewayFilterFactory}
     */
    @Bean
    public TokenGatewayFilterFactory tokenGatewayFilterFactory() {
        return new TokenGatewayFilterFactory(applicationContext);
    }

    /**
     * 网关过滤器，/open 前缀的接口不用 token 也可以访问，只解析 token 不强制有效性
     *
     * @return {@link OpenGatewayFilterFactory}
     */
    @Bean
    public OpenGatewayFilterFactory openGatewayFilterFactory() {
        return new OpenGatewayFilterFactory(applicationContext);
    }
//
//        /**
//     * 网关过滤器，处理 URL
//     *
//     * @return {@link OpenGatewayFilterFactory}
//     */
//    @Primary
//    @ConditionalOnEnabledGlobalFilter
//    @Bean
//    public EncodeURIGatewayFilterFactory encodeURIGatewayFilterFactory() {
//        return new EncodeURIGatewayFilterFactory();
//    }
//
//    /**
//     * 自定义实现限流
//     *
//     * @return {@link ConsumerRedisRateLimiter}
//     */
//    @Bean
//    @Primary
//    public RedisRateLimiter redisRateLimiter(@Autowired ConfigurationService configurationService) {
//        return new RedisRateLimiter(configurationService);
//    }
//
//    /**
//     * 全局异常处理
//     */
//    @Primary
//    @Bean
//    @Order(Ordered.HIGHEST_PRECEDENCE)
//    public ErrorWebExceptionHandler errorWebExceptionHandler(
//            ObjectProvider<List<ViewResolver>> viewResolversProvider,
//            ServerCodecConfigurer serverCodecConfigurer) {
//        ErrorWebExceptionHandler errorWebExceptionHandler = new ErrorWebExceptionHandler();
//        ExceptionHandler jsonExceptionHandler = new ExceptionHandler();
//        jsonExceptionHandler.setViewResolvers(viewResolversProvider.getIfAvailable(Collections::emptyList));
//        jsonExceptionHandler.setMessageWriters(serverCodecConfigurer.getWriters());
//        jsonExceptionHandler.setMessageReaders(serverCodecConfigurer.getReaders());
//        return jsonExceptionHandler;
//    }

    /**
     * ip限流
     *
     * @return {@link KeyResolver}
     */
    @Bean
    @Primary
    public KeyResolver ipKeyResolver() {
        return exchange -> {
            final String hostAddress = Objects.requireNonNull(exchange.getRequest().getRemoteAddress()).getAddress().getHostAddress();
            if (log.isDebugEnabled()) {
                log.debug("ip 限流:{}", hostAddress);
            }
            return Mono.just(hostAddress);
        };
    }

    /**
     * 根据url限流
     *
     * @return {@link KeyResolver}
     */
    @Bean
    public KeyResolver uriKeyResolver() {
        return exchange -> {
            final String path = exchange.getRequest().getURI().getPath();
            if (log.isDebugEnabled()) {
                log.debug("url 限流:{}", path);
            }
            return Mono.just(path);
        };
    }

    /**
     * 根据 token 限流
     *
     * @return {@link KeyResolver}
     */
    @Bean
    public KeyResolver tokenKeyResolver() {
        return exchange -> {
            // DefaultGatewayFilterFactory 会往头部追加 x-token
            final String token = Objects.requireNonNull(
                    Optional.ofNullable(exchange.getRequest().getHeaders().getFirst(HEADER_X_TOKEN))
                            .orElseGet(() -> exchange.getRequest().getQueryParams().getFirst(HEADER_X_TOKEN))
                    , "token 不能为 null"
            );
//            // token 过长，使用 md5 缩短长度
//            final String tokenMD5 = DigestUtils.md5Hex(token);
            if (log.isInfoEnabled()) {
                log.info("用户 token 限流:{}", token);
            }
            return Mono.just(token);
        };
    }
}
