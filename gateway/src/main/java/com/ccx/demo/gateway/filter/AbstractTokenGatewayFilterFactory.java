package com.ccx.demo.gateway.filter;

import com.ccx.demo.gateway.vo.TokenUserVO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Stream;


/**
 * 网关过滤器：https://docs.spring.io/spring-cloud-gateway/docs/current/reference/html
 *
 * @author 谢长春 2022-11-16
 */
@Slf4j
abstract class AbstractTokenGatewayFilterFactory extends AbstractGatewayFilterFactory<AbstractTokenGatewayFilterFactory.Config> implements Ordered {
    public static final String HEADER_X_TOKEN = "x-token";
    public static final String HEADER_X_USER_ID = "x-user-id";

    public static final String HEADER_X_TRACE_ID = "X-Trace-Id";
    public static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
    public static final String HEADER_X_REAL_IP = "X-Real-Ip";
    private final ApplicationContext applicationContext;

    /**
     * 是否断言token有效，/open 前缀的接口不用token也可以访问
     */
    protected abstract boolean assertToken();

    public static class Config {
        //Put the configuration properties for your filter here
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    public AbstractTokenGatewayFilterFactory(final ApplicationContext applicationContext) {
        super(Config.class);
        this.applicationContext = applicationContext;
    }

    static final Pattern REG_TOKEN = Pattern.compile("^[\\w-]+$");
    static final Pattern REG_IP = Pattern.compile("[0-9.]+");

    @Override
    public GatewayFilter apply(Config config) {
        return new GatewayFilter() {
            @SneakyThrows
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                final Instant beginTimeMillis = Instant.now(); // 记录开始时间
                final ServerHttpRequest request = exchange.getRequest();
                final ServerHttpResponse response = exchange.getResponse();
                final HttpHeaders headers = request.getHeaders();

                // 获取 sleuth 链路追踪唯一标识
                final String traceId = MDC.get(HEADER_X_TRACE_ID);
                // 将请求链标识写入响应头，前端通过响应头获取后台请求链路追踪
                exchange.getResponse().getHeaders().add(HEADER_X_TRACE_ID, traceId);
                final String headerToken = headers.getFirst(HEADER_X_TOKEN);
                TokenUserVO tokenUser = null;
                try {
                    if (!REG_TOKEN.matcher(Objects.requireNonNull(headerToken)).matches()) {
                        throw new IllegalArgumentException("token 格式较验失败");
                    }
                    tokenUser = TokenUserVO.parse(headerToken);
                    MDC.put(HEADER_X_TOKEN, headerToken);
                    MDC.put(HEADER_X_USER_ID, Objects.toString(tokenUser.getUserId()));
                } catch (Exception e) {
                    if (assertToken()) {
                        log.error("token 认证失败：{}", headerToken, e);
                        response.setStatusCode(HttpStatus.UNAUTHORIZED);
                        return response.setComplete();
                    }
                }
                Stream
                        .of(headers.getFirst(HEADER_X_FORWARDED_FOR), headers.getFirst(HEADER_X_REAL_IP))
                        .filter(StringUtils::isNoneBlank)
                        .filter(Objects::nonNull)
                        .map(val -> val.split(",")[0])
                        .filter(val -> REG_IP.matcher(val).matches())
                        .findFirst()
                        .ifPresent(ip -> MDC.put(HEADER_X_REAL_IP, ip));
                if (log.isDebugEnabled()) {
                    log.debug("token:{}， user:{}", headerToken, tokenUser);
                }
                return chain
                        .filter(exchange
//                                        .mutate()
//                            .request(request.mutate()
//                                    .header(HEADER_X_TOKEN, headerToken)
//                                    .header(HEADER_X_USER_ID, Objects.toString(tokenUserVO.getUserId()))
//                                    .header(HEADER_X_REAL_IP, "")
//                                    .build()
//                            )
//                                        .build()
                        )
                        .doFinally(signalType -> {
                            final Instant endTimeMillis = Instant.now();
                            if (log.isDebugEnabled()) {
                                log.debug("doFinally:{}:{}:{}", signalType, traceId, beginTimeMillis);
                                log.info("请求耗时：{}ms", endTimeMillis.toEpochMilli() - beginTimeMillis.toEpochMilli());
                            }
//                        MDC.put(MDC_TIMESTAMP_BEGIN, yyyy_MM_dd_HH_mm_ss_SSS.format(beginTimeMillis));
//                        MDC.put(MDC_TIMESTAMP_END, yyyy_MM_dd_HH_mm_ss_SSS.format(endTimeMillis));
//                        MDC.put(MDC_MILLI_SECOND, Objects.toString(endTimeMillis.toEpochMilli() - beginTimeMillis.toEpochMilli()));
//                        MDC.put(ClassicConstants.REQUEST_REQUEST_URI, Objects.toString(request.getURI(), ""));
//                        MDC.put(ClassicConstants.REQUEST_REQUEST_URL, Objects.toString(request.getPath(), ""));
//                        MDC.put(ClassicConstants.REQUEST_METHOD, Objects.toString(request.getMethod(), ""));
//                        MDC.put(ClassicConstants.REQUEST_QUERY_STRING, request.getURI().getQuery());
//                        MDC.put(ClassicConstants.REQUEST_USER_AGENT_MDC_KEY, headers.getFirst("User-Agent"));
//                        MDC.put(ClassicConstants.REQUEST_X_FORWARDED_FOR, headers.getFirst("X-Forwarded-For"));
//                        MDC.remove(MDC_TIMESTAMP_BEGIN);
//                        MDC.remove(MDC_TIMESTAMP_END);
//                        MDC.remove(MDC_MILLI_SECOND);
//                        MDC.remove(ClassicConstants.REQUEST_REQUEST_URI);
//                        MDC.remove(ClassicConstants.REQUEST_REQUEST_URL);
//                        MDC.remove(ClassicConstants.REQUEST_METHOD);
//                        MDC.remove(ClassicConstants.REQUEST_QUERY_STRING);
//                        MDC.remove(ClassicConstants.REQUEST_USER_AGENT_MDC_KEY);
//                        MDC.remove(ClassicConstants.REQUEST_X_FORWARDED_FOR);
                        })
                        ;
            }
        };
    }

}
