package app.starter;

import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.common.starter.event.ErrorLogEvent;
import app.common.starter.exception.CodeException;
import app.security.starter.vo.IUser;
import com.fasterxml.jackson.core.JacksonException;
import io.undertow.server.DefaultByteBufferPool;
import io.undertow.websockets.jsr.WebSocketDeploymentInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ValidationException;
import java.util.Optional;

import static app.security.starter.SecurityConfiguration.MDC_TRACE_ID;

/**
 * <pre>
 * Spring Core，参考：
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html
 *
 * Spring MVC 配置，参考 ：
 * https://linesh.gitbooks.io/spring-mvc-documentation-linesh-translation/content/
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html
 *
 * Spring Thymeleaf 配置，参考 ：
 * https://www.thymeleaf.org/doc/tutorials/3.0/thymeleafspring.html#spring-mvc-configuration
 *
 * Spring validator 配置，参考
 * https://beanvalidation.org/2.0/spec/#introduction
 * https://www.baeldung.com/javax-validation-method-constraints
 * http://rubygems.torquebox.org/proposals/BVAL-241/
 * https://juejin.im/entry/5b5a94d2f265da0f7c4fd2b2
 * https://www.cnblogs.com/mr-yang-localhost/p/7812038.html
 *
 * @author 谢长春 2018-10-3
 */
@RestControllerAdvice
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableDiscoveryClient
//@EnableWebMvc
@ComponentScan({"com.ccx.demo", "app.starter"})
public class ApplicationConfiguration {
//    /**
//     * 服务端 500 异常处理
//     * 需要自定义 Controller 继承 {@link AbstractMvcConfig.ErrorController}
//     * spring security 需要添加 http.antMatchers("/error").permitAll()
//     *
//     * @author 谢长春
//     */
//    public static class ErrorController extends AbstractErrorController {
//        public ErrorController(ErrorAttributes errorAttributes) {
//            super(errorAttributes);
//        }
//
//        @Override
//        public String getErrorPath() {
//            return "/error";
//        }
//
//        /**
//         * 处理服务端 500 异常
//         */
//        @RequestMapping(value = "/error", method = {GET, POST, PUT, PATCH, DELETE})
//        @ResponseBody
//        public Result<Void> error() {
//            return Code.A00001.toResult("500：请求失败，不明确的异常");
//        }
//    }

//    /**
//     * 启用 FastJson
//     */
//    @Bean
//    public FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
//        JSON.DEFAULT_GENERATE_FEATURE |= SerializerFeature.DisableCircularReferenceDetect.getMask(); // 解决循环引用问题
//        final FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//        converter.setDefaultCharset(UTF_8);
//        converter.getFastJsonConfig().setFeatures(Feature.OrderedField);
//        if (Objects.equals(AppEnv.prod, appConfig.getEnv())) {  // 生产环境不返回 Result#exception
//            final SimplePropertyPreFilter simplePropertyPreFilter = new SimplePropertyPreFilter();
//            simplePropertyPreFilter.getExcludes().add("exception");
//            converter.getFastJsonConfig().setSerializeFilters(simplePropertyPreFilter);
//        }
//        converter.setSupportedMediaTypes(Collections.singletonList(
//                MediaType.APPLICATION_JSON
//        ));
//        return converter;
//    }

//    /**
//     * 注册过滤器
//     * <pre>
//     * 添加自定义过滤器：设置链路追踪，在日志打印和响应头中追加该标记
//     * 警告：多线程时需要特殊处理
//     * final Map<String, String> mdc = MDC.getCopyOfContextMap(); // 复制主线程 ThreadLocal
//     * new Thread(() -> {
//     *     try {
//     *         MDC.setContextMap(mdc); // 设置子线程 ThreadLocal
//     *         // 子线程代码
//     *     } finally {
//     *         MDC.clear(); // 清除子线程 ThreadLocal
//     *     }
//     * }).start();
//     *
//     * @return FilterRegistrationBean
//     */
//    @Order(0)
//    @Bean
//    public FilterRegistrationBean<Filter> requestIdFilter() {
//        final FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>();
//        bean.setOrder(0);
//        bean.setFilter(new RequestIdFilter());
//        bean.addUrlPatterns("/*");
//        return bean;
//    }

    private final ApplicationContext applicationContext;

    @Value("${spring.app.name}")
    @Setter
    @Getter
    private String appName;

    @ExceptionHandler
    public Result<Void> exception(final Exception e) {
        log.error("{}"
                , Optional
                        .ofNullable(SecurityContextHolder.getContext().getAuthentication())
                        .map(Authentication::getPrincipal)
                        .map(obj -> (IUser) obj)
                        .map(obj -> {
                            final String msg = String.format("%s:%s(%d:%s:%s)："
                                    , (e instanceof CodeException) ? ((CodeException) e).getCode() : "-"
                                    , (e instanceof CodeException) ? e.getMessage() : "-"
                                    , obj.getId()
                                    , obj.getUsername()
                                    , obj.getPhone()
                            );
                            try {
                                // 发布 ErrorLogEvent 事件
                                // 注册监听实现方法 @EventListener public void listenerErrorLogEvent(ErrorLogEvent event){}
                                applicationContext.publishEvent(new ErrorLogEvent(
                                        this
                                        , appName
                                        , MDC.get(MDC_TRACE_ID)
                                        , Optional.ofNullable(obj.getId()).orElse(0L)
                                        , Strings.left(msg + e.getMessage(), 255000)
                                ));
                            } catch (Exception ex) {
                                log.warn("异常日志写入失败：{}", ex.getMessage());
                            }
                            return msg;
                        })
                        .orElse("")
                , e
        );
        if (e instanceof CodeException) {
            return new Result<Void>().setException(e);
        } else if (e instanceof BadCredentialsException) {
            return new Result<Void>(Code.A00006).setException("401：无操作权限");
        } else if (e instanceof AccessDeniedException) {
            return new Result<Void>(Code.A00006).setException("403：无操作权限");
        } else if (e instanceof ValidationException) {
            return new Result<Void>(Code.A00003).setException(e.getMessage());
        } else if (e instanceof HttpMediaTypeNotSupportedException) {
            return new Result<Void>(Code.A00003).setException(String.format("500：content-type 不支持：%s", e.getMessage()));
        } else if (e instanceof MaxUploadSizeExceededException) {
            return new Result<Void>(Code.A00005).setException("文件大小超过限制");
        } else if (e instanceof IllegalArgumentException) {
            return new Result<Void>(Code.A00003).setException("400：请求不存在");
        } else if (e instanceof MissingServletRequestParameterException) {
            return new Result<Void>(Code.A00003).setException(String.format("500：请求 url 映射的方法缺少必要的参数：%s", e.getMessage()));
        } else if (e instanceof HttpMessageNotReadableException) {
            return new Result<Void>(Code.A00003).setException(String.format("500：请求参数解析失败:%s", e.getMessage()));
        } else if (e instanceof JacksonException) {
            return new Result<Void>(Code.A00003).setException(String.format("500：Json 序列化或反序列化异常：%s", e.getMessage()));
        } else if (e instanceof BindException || e instanceof MethodArgumentTypeMismatchException) {
            return new Result<Void>(Code.A00003).setException(String.format("500：参数转换异常：%s", e.getMessage()));
        } else if (e instanceof HttpRequestMethodNotSupportedException) {
            return new Result<Void>(Code.A00003).setException(String.format("405：请求方式不被该接口支持，或者请求url错误未映射到正确的方法：%s", e.getMessage()));
        } else if (e instanceof HttpMessageNotWritableException) {
            return new Result<Void>(Code.A00003).setException(String.format("500：请求缺少必要的参数:%s", e.getMessage()));
        } else if (e instanceof NoHandlerFoundException) {
            return new Result<Void>(Code.A00003).setException("404：请求url不存在");
        }
        return new Result<Void>(Code.A00003).setException(String.format("500：请求失败，不明确的异常：%s", e.getMessage()));
    }

    @Bean
    public WebServerFactoryCustomizer<UndertowServletWebServerFactory> undertowServletWebServerFactoryWebServerFactoryCustomizer() {
        return factory -> factory.addDeploymentInfoCustomizers(deploymentInfo -> {
            final WebSocketDeploymentInfo webSocketDeploymentInfo = new WebSocketDeploymentInfo();
            webSocketDeploymentInfo.setBuffers(new DefaultByteBufferPool(false, 2048));
            deploymentInfo.addServletContextAttribute("io.undertow.websockets.jsr.WebSocketDeploymentInfo", webSocketDeploymentInfo);
        });
    }
}
