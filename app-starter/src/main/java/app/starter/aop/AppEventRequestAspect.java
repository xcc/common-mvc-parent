package app.starter.aop;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.dto.AppEventDTO;
import app.common.starter.event.AppEvent;
import app.common.starter.util.ChainMap;
import app.enums.starter.AppEventKey;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * 拦截所有 controller 层方法记录到日志表
 *
 * @author 谢长春 2022-04-22
 */
@Component
@Aspect
@Slf4j
@Order(9999)
@RequiredArgsConstructor
public class AppEventRequestAspect {

    public static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
    public static final String HEADER_X_REAL_IP = "X-Real-Ip";
    private static final Pattern REG_HOST = Pattern.compile("[0-9a-z.]+");
    private static final Pattern REG_IP = Pattern.compile("[0-9.]+");
    private static final SpelExpressionParser SPEL_EXPRESSION_PARSER;
    private static final DefaultParameterNameDiscoverer DEFAULT_PARAMETER_NAME_DISCOVERER;

    static {
        SPEL_EXPRESSION_PARSER = new SpelExpressionParser();
        DEFAULT_PARAMETER_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();
    }

    @Value("${spring.app.name}")
    @Setter
    @Getter
    private String appName;
    private final ApplicationContext applicationContext;

    @Before("@annotation(org.springframework.web.bind.annotation.RequestMapping)"
            + "||@annotation(org.springframework.web.bind.annotation.GetMapping)"
            + "||@annotation(org.springframework.web.bind.annotation.PostMapping)"
            + "||@annotation(org.springframework.web.bind.annotation.PutMapping)"
            + "||@annotation(org.springframework.web.bind.annotation.PatchMapping)"
            + "||@annotation(org.springframework.web.bind.annotation.DeleteMapping)"
    )
    public void before(final JoinPoint joinPoint) {
        Long userId = 0L;
        try {
            final Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
            final AppEventRequest annotation = Optional
                    // 获取方法上的注解
                    .ofNullable(method.getAnnotation(AppEventRequest.class))
                    // 获取类上的注解
                    .orElseGet(() -> method.getDeclaringClass().getAnnotation(AppEventRequest.class));
            if (Objects.nonNull(annotation)) {
                if (annotation.skip()) { // 跳过埋点
                    return;
                }
            }
            if (!Optional.ofNullable(annotation).map(AppEventRequest::ignoreUserId).orElse(false)) {
                if (ArrayUtils.isNotEmpty(joinPoint.getArgs())) { // 检查埋点方法是否有参数
                    final Object[] args = joinPoint.getArgs();
                    userId = Optional
                            .ofNullable(DEFAULT_PARAMETER_NAME_DISCOVERER.getParameterNames(method))
                            .map(argNames -> {
                                final EvaluationContext context = new StandardEvaluationContext();
                                for (int i = 0; i < args.length; i++) {
                                    context.setVariable(argNames[i], args[i]);
                                }
                                return SPEL_EXPRESSION_PARSER.parseExpression("#user.id").getValue(context, Long.class);
                            })
                            .orElse(0L);
                }
            }
        } catch (Exception e) {
            log.warn("未获取到操作用户:{}", joinPoint);
        }
        final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        final AppEventDTO eventDTO = new AppEventDTO()
                .setAppName(appName)
                .setUserId(userId)
                .setEvent(AppEventKey.AE0)
                .setData(ChainMap.<String, Object>create()
                        .put("method", request.getMethod())
                        .put("uri", request.getRequestURI())
                        .putIfAbsent("host", Optional.ofNullable(request.getHeader("Host"))
                                .filter(val -> REG_HOST.matcher(val).matches())
                                .orElse(null)
                        )
                        .putIfAbsent("ip",
                                Stream
                                        .of(request.getHeader(HEADER_X_FORWARDED_FOR), request.getHeader(HEADER_X_REAL_IP))
                                        .filter(StringUtils::isNoneBlank)
                                        .map(val -> val.split(",")[0])
                                        .filter(val -> REG_IP.matcher(val).matches())
                                        .findFirst()
                                        .orElse(null)
                        )
                        .toMapObject());
        // 发布 AppEvent 事件
        // 注册监听实现方法 @EventListener public void listenerAppEvent(AppEvent event){}
        applicationContext.publishEvent(new AppEvent(this, eventDTO));
    }
}
