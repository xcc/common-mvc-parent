package com.ccx.demo.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 拦截所有方法调用链路入参打印到日志
 *
 * @author 谢长春 2018-10-4
 */
@Component
@Aspect
@Slf4j
@Order(9999)
public class TraceLogAspect {
    /**
     * 打印参数
     */
    @Before("(execution(* com.ccx.demo..*.controller..*.*(..))" +
            "||execution(* com.ccx.demo..*.service..*.*(..))" +
            "||execution(* com.ccx.demo..*.cache..*.*(..))" +
            "||execution(* com.ccx.demo..*.feign..*.*(..))" +
            "||execution(* com.ccx.demo..*.api..*.*(..))" +
            "||execution(* com.ccx.demo..*.dao..*.*(..)))" +
            "&&!@annotation(app.common.starter.aop.IgnoreTraceLog)")
    public void before(final JoinPoint joinPoint) {
        if (log.isInfoEnabled()) {
            log.info("{} => {}", joinPoint, Arrays.toString(joinPoint.getArgs()).replaceAll("\\w+=null, ", ""));
        }
    }

}
