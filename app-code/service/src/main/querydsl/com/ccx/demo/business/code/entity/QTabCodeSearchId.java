package com.ccx.demo.business.code.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCodeSearchId is a Querydsl query type for TabCodeSearchId
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCodeSearchId extends EntityPathBase<TabCodeSearchId> {

    private static final long serialVersionUID = 1141148437L;

    public static final QTabCodeSearchId tabCodeSearchId = new QTabCodeSearchId("tabCodeSearchId");

    public final NumberPath<Long> fromId = createNumber("fromId", Long.class);

    public final StringPath id = createString("id");

    public final NumberPath<Long> toId = createNumber("toId", Long.class);

    public QTabCodeSearchId(String variable) {
        super(TabCodeSearchId.class, forVariable(variable));
    }

    public QTabCodeSearchId(Path<? extends TabCodeSearchId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCodeSearchId(PathMetadata metadata) {
        super(TabCodeSearchId.class, metadata);
    }

}

