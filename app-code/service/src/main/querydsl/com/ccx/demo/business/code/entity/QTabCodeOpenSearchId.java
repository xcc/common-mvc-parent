package com.ccx.demo.business.code.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCodeOpenSearchId is a Querydsl query type for TabCodeOpenSearchId
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCodeOpenSearchId extends EntityPathBase<TabCodeOpenSearchId> {

    private static final long serialVersionUID = 506643071L;

    public static final QTabCodeOpenSearchId tabCodeOpenSearchId = new QTabCodeOpenSearchId("tabCodeOpenSearchId");

    public final NumberPath<Long> fromId = createNumber("fromId", Long.class);

    public final StringPath id = createString("id");

    public final NumberPath<Long> toId = createNumber("toId", Long.class);

    public QTabCodeOpenSearchId(String variable) {
        super(TabCodeOpenSearchId.class, forVariable(variable));
    }

    public QTabCodeOpenSearchId(Path<? extends TabCodeOpenSearchId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCodeOpenSearchId(PathMetadata metadata) {
        super(TabCodeOpenSearchId.class, metadata);
    }

}

