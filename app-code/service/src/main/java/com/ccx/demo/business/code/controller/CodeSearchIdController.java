package com.ccx.demo.business.code.controller;

import app.common.starter.entity.OrderBy;
import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.encrypt.domain.starter.annotations.AesPathVariable;
import app.security.starter.vo.IUser;
import com.ccx.demo.business.code.entity.TabCodeSearchId;
import com.ccx.demo.business.code.service.CodeSearchIdService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 对外接口：测试 SearchId 模板 V20221105
 *
 * @author 谢长春 on 2022-11-18
 */
@Api(tags = "测试 SearchId 模板")
//@ApiSort() // 控制接口排序
@RequestMapping("/code-search-id")
@Controller
@Slf4j
@RequiredArgsConstructor
public class CodeSearchIdController {

    private final CodeSearchIdService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询测试 SearchId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ResponseBody
    public Result<TabCodeSearchId> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
            //@ApiParam(value = "fromId；示例：54775807") @AesRequestParam(required = false) final Long fromId,
            //@ApiParam(value = "toId；示例：54775807") @AesRequestParam(required = false) final Long toId,
            //@ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            //@ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            //@ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<TabCodeSearchId>().execute(result -> { // Controller 方法逻辑写在这里
            final TabCodeSearchId condition = new TabCodeSearchId();
            //condition.setFromId(fromId);
            //condition.setToId(toId);
            //condition.setCreateUserId(createUserId);
            //condition.setUpdateUserId(updateUserId);
            //condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.page(condition, Page.of(number, limit)));
        });
    }

//    @GetMapping("/pull/{skip}/{limit}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "测试 SearchId 模板瀑布流加载，按条件查询并跳过指定行数"
//            , tags = {""}
//            , notes = "瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）"
//            + "<br>注意：该方法不会返回总数据行数，前端应该判断 rowCount != limit 时，表示瀑布流已经滚动到底部"
//    )
//    @ApiOperationSupport(order = 2, author = "谢长春")
//    @ResponseBody
//    public Result<TabCodeSearchId> pull(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "跳过数据行数，示例：0", example = "1") @PathVariable final int skip,
//            @ApiParam(required = true, value = "每次获取数据行数，最大值100", example = "100") @PathVariable final int limit,
//                        //@ApiParam(value = "fromId；示例：54775807") @AesRequestParam(required = false) final Long fromId,
//            //@ApiParam(value = "toId；示例：54775807") @AesRequestParam(required = false) final Long toId,
//            //@ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
//            //@ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
//            //@ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
//            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
//    ) {
//        return new Result<TabCodeSearchId>().execute(result -> { // Controller 方法逻辑写在这里
//             final TabCodeSearchId condition = new TabCodeSearchId();
//                        //condition.setFromId(fromId);
//            //condition.setToId(toId);
//            //condition.setCreateUserId(createUserId);
//            //condition.setUpdateUserId(updateUserId);
//            //condition.setDeleted(deleted);
//            result.setSuccess(service.pull(skip, limit, condition));
//        });
//    }
//

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询测试 SearchId 模板"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 3,
//            author = "谢长春"
//    )
//    @ResponseBody
//    public Result<TabCodeSearchId> list(
//        @ApiIgnore @AuthenticationPrincipal final IUser user
//        @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
//    ) {
//        return new Result<TabCodeSearchId>().execute(result -> { // Controller 方法逻辑写在这里
//            final TabCodeSearchId condition = new TabCodeSearchId();
//            condition.setOrderBy(orderBy);
//            result.setSuccess(service.list(condition));
//        });
//    }
//

    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询测试 SearchId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 4, author = "谢长春")
    @ResponseBody
    public Result<TabCodeSearchId> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id", type = "string") @AesPathVariable(clazz = String.class) final String id
    ) {
        return new Result<TabCodeSearchId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

}
