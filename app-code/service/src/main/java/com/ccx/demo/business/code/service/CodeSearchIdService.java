package com.ccx.demo.business.code.service;

import app.common.starter.entity.Page;
import com.ccx.demo.business.code.dao.jpa.CodeSearchIdRepository;
import com.ccx.demo.business.code.entity.TabCodeSearchId;
import com.google.common.base.Strings;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * 服务接口实现类：测试 SearchId 模板 V20221105
 *
 * @author 谢长春 on 2022-11-18
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class CodeSearchIdService
//      , ITabCodeSearchIdCache
{
    private final CodeSearchIdRepository repository;
    //private final TabCodeSearchIdCache tabCodeSearchIdCache;
    //private final IUserService userService;
    //private final UserService userService;

    /**
     * 测试 SearchId 模板 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link String} 数据ID
     * @return Optional<TabCodeSearchId> {@link TabCodeSearchId} 实体对象
     */
    public Optional<TabCodeSearchId> findById(final String id) {
        if (Strings.isNullOrEmpty(id)) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabCodeSearchId::cloneObject) // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
                ;
//         return Optional.ofNullable(repository.findCacheById(id)).map(TabCodeSearchId::cloneObject); // 若使用缓存需要解开代码
    }

    /**
     * 测试 SearchId 模板 按条件分页查询列表
     *
     * @param condition {@link TabCodeSearchId} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return QueryResults<TabCodeSearchId> {@link TabCodeSearchId} 分页对象
     */
    public @NotNull(message = "返回值不能为null") QueryResults<TabCodeSearchId> page(
            @NotNull(message = "【condition】不能为null") final TabCodeSearchId condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<TabCodeSearchId> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        //this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试 SearchId 模板 按条件查询列表
//     *
//     * @param condition {@link TabCodeSearchId} 查询条件
//     * @return List<TabCodeSearchId> {@link TabCodeSearchId> 结果集合
//     */
//    public @NotNull(message = "返回值不能为null") List<TabCodeSearchId> list(
//            @NotNull(message = "【condition】不能为null") final TabCodeSearchId condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 测试 SearchId 模板 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<String> {@link TabCodeSearchId#getId()> 数据 id 集合
     * @return List<TabCodeSearchId> {@link TabCodeSearchId> 结果集合
     */
    public @NotNull(message = "返回值不能为null") List<TabCodeSearchId> listByIds(final Collection<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }

    /**
     * 测试 SearchId 模板 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<String> {@link TabCodeSearchId#getId()> 数据 id 集合
     * @return Map<String, TabCodeSearchId> {@link TabCodeSearchId> 结果集合
     */
    public @NotNull(message = "返回值不能为null") Map<String, TabCodeSearchId> mapByIds(final Set<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }

//    /**
//     * 测试 SearchId 模板 按条件查询并跳过指定行数
//     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
//     *
//     * @param skip      int 跳过 skip 行数据
//     * @param limit     int 获取 limit  行数据
//     * @param condition {@link TabCodeSearchId} 查询条件
//     * @return List<TabCodeSearchId> {@link TabCodeSearchId} 列表查询结果集
//     */
//    public @NotNull(message = "返回值不能为null") List<TabCodeSearchId> pull(final int skip, final int limit, final TabCodeSearchId condition) {
//        List<TabCodeSearchId> list = repository.pull(skip, limit, condition);
//        this.fillUserNickname(list);
//        return list;
//    }
//
}
