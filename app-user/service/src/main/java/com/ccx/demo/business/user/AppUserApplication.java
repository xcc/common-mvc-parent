package com.ccx.demo.business.user;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationListener;

import java.util.List;
import java.util.Properties;

/**
 * <pre>
 * 参考配置：
 *   https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/
 *   https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/html/
 *
 * @author 谢长春 2019/1/21
 */
@Slf4j
@SpringBootApplication
//@EnableJpaAuditing
@EnableConfigurationProperties
public class AppUserApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        final SpringApplication application = new SpringApplication(AppUserApplication.class);
        // 开启Banner打印方式(OFF：关闭，CONSOLE：控制台输出，LOG：日志输出)
        // 特效字生成 https://www.bootschool.net/qrcode-terminal
        application.setBannerMode(Banner.Mode.CONSOLE);
        application.addListeners(
                (ApplicationListener<ApplicationEnvironmentPreparedEvent>) event -> {
                    final Properties properties = System.getProperties();
                    final List<String> props = Lists.newArrayList();
                    properties.forEach((key, value) -> props.add(String.format("%s: %s", key, value)));
                    log.info("\n{}\n* 系统环境变量\n* {}\n{}"
                            , Strings.repeat("*", 100)
                            , StringUtils.join(props, "\n* ")
                            , Strings.repeat("*", 100)
                    );
                }
//                (ApplicationListener<ApplicationContextInitializedEvent>) event -> {
//                },
//                (ApplicationListener<ApplicationPreparedEvent>) event -> {
//                }
        );
        application.run(args);
    }

}
