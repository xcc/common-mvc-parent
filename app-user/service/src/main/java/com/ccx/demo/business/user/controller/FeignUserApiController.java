package com.ccx.demo.business.user.controller;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.OrderBy;
import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.encrypt.domain.starter.model.DataVersion;
import app.security.starter.vo.IUser;
import com.ccx.demo.business.user.dto.TabUserInsertDTO;
import com.ccx.demo.business.user.dto.TabUserUpdateDTO;
import com.ccx.demo.business.user.entity.TabUser;
import com.ccx.demo.business.user.service.UserService;
import com.ccx.demo.business.user.vo.TabUserVO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 对外接口:用户
 *
 * @author 谢长春 2022-11-13
 */
@Api(tags = "用户")
@RequestMapping("/feign/user")
@Controller
@Slf4j
@RequiredArgsConstructor
@AppEventRequest(skip = true)
//@ConditionalOnProperty(value = "spring.app.feign.user.enabled", havingValue = "false")
public class FeignUserApiController {
    private final UserService service;

    @GetMapping("/page/{number}/{limit}")
    @ApiOperation(value = "分页查询用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 2, author = "谢长春")
    @ResponseBody
    public Result<TabUserVO> pageVO(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数", example = "10") @PathVariable final int limit,
            @ApiParam(value = "模糊匹配：用户名/昵称/手机号/邮箱；示例：187888888888") @RequestParam(required = false) final String containsAll,
            @ApiParam(value = "昵称；示例：老王") @RequestParam(required = false) final String nickname,
            @ApiParam(value = "登录名；示例：admin") @RequestParam(required = false) final String username,
            @ApiParam(value = "手机号；示例：187888888888") @RequestParam(required = false) final String phone,
            @ApiParam(value = "邮箱；示例：xx@mail.com") @RequestParam(required = false) final String email,
            @ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @RequestParam(required = false) final Long createUserId,
            @ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @RequestParam(required = false) final Long updateUserId,
            @ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<TabUserVO>().execute(result -> {
            final TabUserVO condition = new TabUserVO();
            condition.setContainsAll(containsAll);
            condition.setNickname(nickname);
            condition.setUsername(username);
            condition.setEmail(email);
            condition.setPhone(phone);
            condition.setCreateUserId(createUserId);
            condition.setUpdateUserId(updateUserId);
            condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.pageVO(
                    condition
                    , Page.builder().number(number).limit(limit).build()
            ));
        });
    }

    @PostMapping("/listByIds")
    @ApiOperation(value = "按 id 批量查询用户"
            , tags = {""}
            , notes = ""
    )
    @ApiImplicitParam(name = "body", dataTypeClass = String[].class, required = true)
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<TabUser> listByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final List<Long> ids
    ) {
        return new Result<TabUser>().execute(result -> {
            result.setSuccess(service.listByIds(ids));
        });
    }

    @PostMapping
    @ApiOperation(value = "新增用户"
            , tags = {""}
            , notes = ""
    )
    @ApiImplicitParam(name = "body", dataType = "TabUserInsertDTO", dataTypeClass = TabUserInsertDTO.class, required = true)
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            includeParameters = {
                    "tabUserInsertDTO.username", "tabUserInsertDTO.password", "tabUserInsertDTO.nickname", "tabUserInsertDTO.phone", "tabUserInsertDTO.email", "tabUserInsertDTO.avatar", "tabUserUpdateDTO.roleIds",
            }
    )
    @ResponseBody
    public Result<Void> insert(@ApiIgnore @AuthenticationPrincipal final IUser user,
                               @RequestBody final TabUserInsertDTO body) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertHasTrue(Objects.nonNull(body.getPhone()), "注册手机号码不能为空");
            service.insert(body, user.getId());
        });
    }

    @PutMapping
    @ApiOperation(value = "修改用户"
            , tags = {""}
            , notes = ""
    )
    @ApiImplicitParam(name = "body", dataType = "TabUserUpdateDTO", dataTypeClass = TabUserUpdateDTO.class, required = true)
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            includeParameters = {
                    "tabUserUpdateDTO.id", "tabUserUpdateDTO.nickname", "tabUserUpdateDTO.phone", "tabUserUpdateDTO.email", "tabUserUpdateDTO.avatar", "tabUserUpdateDTO.roleIds", "tabUserUpdateDTO.updateTime",
            }
    )
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final TabUserUpdateDTO body) {
        return new Result<Void>().call(() -> {
            service.update(body.getId(), user.getId(), body);
        });
    }

    @PatchMapping("/markDeleteByIds")
    @ApiOperation(value = "按 id 批量逻辑删除用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 7,
            author = "谢长春",
            params = @DynamicParameters(name = "ids", properties = {
                    @DynamicParameter(name = "ids", value = "数据版ID", example = "[11,10]", required = true, dataTypeClass = Long[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDeleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<Long> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDelete(body, null, user.getId());
        });
    }

    @PatchMapping("/markDeleteByDvs")
    @ApiOperation(value = "按 dv 批量逻辑删除用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 7,
            author = "谢长春",
            params = @DynamicParameters(name = "ids", properties = {
                    @DynamicParameter(name = "ids", value = "数据版ID", example = "[11,10]", required = true, dataTypeClass = Long[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDeleteByDvs(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDelete(
                    body.stream().map(DataVersion::getLongId).collect(Collectors.toSet())
                    , body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }
//
//    @PatchMapping("/nickname")
//    @ApiOperation(value = "修改昵称"
//            , tags = {""}
//            , notes = "请求体（body）直接给新昵称，不需要使用对象包在外层"
//    )
//    @ApiOperationSupport(order = 8, author = "谢长春")
//    @ResponseBody
//    public Result<Void> updateNickname(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final String nickname) {
//        return new Result<Void>().call(() -> service.updateNickname(
//                user.getId(),
////                body.getString("nickname"),
//                nickname,
//                user.getId()
//        ));
//    }
//
//    @PatchMapping("/reset-password/{dv}")
//    @ApiOperation(value = "重置密码，返回新密码"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(order = 9, author = "谢长春")
//    @ResponseBody
//    public Result<String> resetPassword(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
//        return new Result<String>().execute(result -> {
////            String newPassword = service.resetPassword(decrypt.getLongId(), user.getId());
////            if (appConfig.getEncrypt().getPwd().isEnabled()) {
////                // rsa 加密传输密码， 需要使用公钥解密
////                newPassword = Rsa.encryptByPrivateKey(newPassword, appConfig.getEncrypt().getPwd().getPrivateKey());
////            }
//            result.setSuccess(service.resetPassword(dv.getLongId(), user.getId()));
//        });
//    }
//
//    @PatchMapping("/change-password")
//    @ApiOperation(value = "修改密码", tags = {""}, notes = "密码需要使用 RSA 加密")
//    @ApiOperationSupport(order = 10, author = "谢长春")
//    @ResponseBody
//    public Result<Void> changePassword(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody(required = false) final ChangePasswordDTO body
//    ) {
//        return new Result<Void>().call(() -> service.changePassword(
//                user.getId(),
//                body.getPasswordOld(),
//                body.getPasswordNew()
//        ));
//    }
//
//    @PatchMapping("/change-avatar")
//    @ApiOperation(value = "修改头像"
//            , tags = {""}
//            , notes = "需要调用文件上传接口获得文件信息"
//    )
//    @ApiOperationSupport(order = 11, author = "谢长春")
//    @ResponseBody
//    public Result<Void> changeAvatar(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody(required = false) final UserAvatarImageDTO body
//    ) {
//        return new Result<Void>().call(() -> service.changeAvatar(user.getId(), body));
//    }
//
//    @PatchMapping("/enable/{dv}")
//    @ApiOperation(value = "启用"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(order = 12, author = "谢长春")
//    @ResponseBody
//    public Result<Void> enable(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
//        return new Result<Void>().call(() -> {
//            service.updateDisabled(dv.getLongId(), false, user.getId());
//        });
//    }
//
//    @PatchMapping("/disable/{dv}")
//    @ApiOperation(value = "禁用"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(order = 13, author = "谢长春")
//    @ResponseBody
//    public Result<Void> disable(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
//        return new Result<Void>().call(() -> {
//            service.updateDisabled(dv.getLongId(), true, user.getId());
//        });
//    }
//
//    @ApiOperation(value = "账号销毁"
//            , tags = {""}
//            , notes = "账号销毁之后可以用手机号重新注册帐号"
//            + "<br>"
//    )
//    @PostMapping("/clear")
//    @ApiOperationSupport(order = 29, author = "谢长春")
//    @ResponseBody
//    public Result<Void> clearUser(@ApiIgnore @AuthenticationPrincipal final IUser user) {
//        return new Result<Void>().call(() -> {
//            service.clear(
//                    user.getPhone()
//                    , Collections.singletonList((u) -> log.debug("执行账号销毁关联的其他逻辑"))
//            );
//        });
//    }
}
