package com.ccx.demo.business.user.cache;

import app.cache.starter.interfaces.ITableCache;
import app.cache.starter.util.JsonCache;
import com.ccx.demo.business.user.entity.TabUser;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static app.common.starter.AppContext.getBean;


/**
 * 缓存：用户
 *
 * @author 谢长春 2022-06-14
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.cache.tab-user")
public class TabUserCache implements ITableCache<Long, TabUser> {

    @JsonFilter("TabUserCacheObjectFieldIgnoreFilter")
    private interface TabUserCacheObjectFieldIgnoreFilter {
    }

    static {
        // 注册自定义过滤器，排除缓存对象中的只读属性和不需要写入缓存的属性
        JsonCache.addFilter(TabUserCacheObjectFieldIgnoreFilter.class.getSimpleName()
                , SimpleBeanPropertyFilter.serializeAllExcept(
                        // 缓存序列化时排除字段,
                        "dv"
                        , "createTimeFormat"
                        , "updateTimeFormat"
                        , "createUserNickname"
                        , "updateUserNickname"
                )
        );
        JsonCache.get().addMixIn(TabUser.class, TabUserCacheObjectFieldIgnoreFilter.class);
    }

    private static final String CACHE_KEY = TabUserCache.class.getSimpleName() + ":%d";
    private static volatile TabUserCache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    private final CacheRepository<Long, TabUser> cacheRepository;

    @Override
    public CacheRepository<Long, TabUser> getCacheRepository() {
        return cacheRepository;
    }

    @Override
    public Class<TabUser> getEntityClazz() {
        return TabUser.class;
    }

    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration expired;

    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;

    /**
     * 获取 {@link TabUserCache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link TabUserCache}
     */
    public static TabUserCache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (TabUserCache.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = getBean(TabUserCache.class);
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.tab-user.expired: {}", expired);
        log.info("spring.app.cache.tab-user.nullValueExpired: {}", nullValueExpired);
    }

    //    public void test(){
//        clear();
//        get(1000L);
//        mapByIds(ImmutableSet.of(1001L,1002L));
//        delete(1000L);
//        get(1000L);
//        forEachKeys(keys -> log.info("forEachKeys: {} : {}", keys.size(), JsonCache.toJsonString(keys)));
//        forEachValues(values -> log.info("forEachValues: {} : {}", values.size(), JsonCache.toJsonString(values.get(0))));
//    }
    @Override
    public String cacheKey(final Long id) {
        return String.format(CACHE_KEY, id);
    }

    private RBucket<String> getBucket(final Long id) {
        return redissonClient.getBucket(cacheKey(id));
    }

    /**
     * 获取缓存用户昵称
     *
     * @param userId {@link TabUser#getId()}
     * @return {@link String}
     */
    public String getNickname(final Long userId) {
        return get(userId).map(TabUser::getNickname).orElse("");
    }

    /**
     * 获取缓存用户昵称
     *
     * @param userIds {@link TabUser#getId()}
     * @return Map<Long, String>
     */
    public Map<Long, String> mapNicknameByIds(final Set<Long> userIds) {
        return mapByIds(userIds).entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        row -> row.getValue().getNickname()
                ));
    }

}
