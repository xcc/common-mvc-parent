package com.ccx.demo.business.user.cache;

import app.cache.starter.interfaces.ITableCache;
import app.cache.starter.util.JsonCache;
import com.ccx.demo.business.user.entity.TabRole;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Objects;

import static app.common.starter.AppContext.getBean;

/**
 * 缓存：角色
 *
 * @author 谢长春 2022-06-17
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "spring.app.cache.tab-role")
public class TabRoleCache implements ITableCache<String, TabRole> {
    @JsonFilter("TabRoleCacheObjectFieldIgnoreFilter")
    private interface TabRoleCacheObjectFieldIgnoreFilter {
    }

    static {
        // 注册自定义过滤器，排除缓存对象中的只读属性和不需要写入缓存的属性
        JsonCache.addFilter("TabRoleCacheObjectFieldIgnoreFilter"
                , SimpleBeanPropertyFilter.serializeAllExcept(
                        // 缓存序列化时排除字段,
                        "dv"
                        , "createTimeFormat"
                        , "updateTimeFormat"
                        , "createUserNickname"
                        , "updateUserNickname"
                )
        );
        JsonCache.get().addMixIn(TabRole.class, TabRoleCacheObjectFieldIgnoreFilter.class);
    }

    private static final String CACHE_KEY = TabRoleCache.class.getSimpleName() + ":%s";

    private static volatile TabRoleCache INSTANCE = null;
    @Getter
    private final RedissonClient redissonClient;
    private final CacheRepository<String, TabRole> cacheRepository;

    @Override
    public CacheRepository<String, TabRole> getCacheRepository() {
        return cacheRepository;
    }

    @Override
    public Class<TabRole> getEntityClazz() {
        return TabRole.class;
    }

    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration expired;

    /**
     * 缓存过期时间
     */
    @Setter
    @Getter
    private Duration nullValueExpired = NULL_VALUE_EXPIRED;

    /**
     * 获取 {@link TabRoleCache} 实例，用于静态类或实体类获取配置参数
     *
     * @return {@link TabRoleCache}
     */
    public static TabRoleCache instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (TabRoleCache.class) {
                if (Objects.isNull(INSTANCE)) {
                    INSTANCE = getBean(TabRoleCache.class);
                }
            }
        }
        return INSTANCE;
    }

    @PostConstruct
    private void init() {
        log.info("CACHE_KEY: {}", CACHE_KEY);
        log.info("spring.app.cache.tab-role.expired: {}", expired);
        log.info("spring.app.cache.tab-role.nullValueExpired: {}", nullValueExpired);
    }

    //    public void test(){
//        clear();
//        get(1000L);
//        mapByIds(ImmutableSet.of(1001L,1002L));
//        delete(1000L);
//        get(1000L);
//        forEachKeys(keys -> log.info("forEachKeys: {} : {}", keys.size(), JsonCache.toJsonString(keys)));
//        forEachValues(values -> log.info("forEachValues: {} : {}", values.size(), JsonCache.toJsonString(values.get(0))));
//    }
    @Override
    public String cacheKey(final String id) {
        return String.format(CACHE_KEY, id);
    }

    private RBucket<String> getBucket(final String id) {
        return redissonClient.getBucket(cacheKey(id));
    }

//    /**
//     * 从缓存获取角色权限
//     *
//     * @param ids Set<String> 角色ID
//     * @return List<String> 角色权限集合，参考：{@link Authority#getCode()}
//     */
//    @Transient
//    @QueryTransient
//    @JsonIgnore
//    public List<String> listRoleAuthorities(final Set<String> ids) {
//        return listByIds(ids)
//                .stream()
//                .filter(obj -> Objects.equals(obj.getDeleted(), false))
//                .flatMap(row -> Stream.of(row.getAuthorities()))
//                .distinct()
//                .collect(Collectors.toList());
//    }
//
//    /**
//     * 从缓存获取角色名称
//     *
//     * @param id {@link String} 角色ID
//     * @return {@link String} 角色名称，{@link TabRole#getName()}
//     */
//    @Transient
//    @QueryTransient
//    @JsonIgnore
//    public String getRoleNameCacheById(final String id) {
//        return get(id).map(TabRole::getName).orElse(null);
//    }
//
//    /**
//     * 从缓存获取角色权限
//     *
//     * @param ids {@link String} 角色 ID 集合
//     * @return {@link List <String>} 角色权限集合，{@link TabRole#getName()}
//     */
//    @Transient
//    @QueryTransient
//    @JsonIgnore
//    public List<String> listRoleNameCacheByIds(final Collection<String> ids) {
//        if (CollectionUtils.isEmpty(ids)) return Collections.emptyList();
//        return listByIds(ids).stream().map(TabRole::getName).collect(Collectors.toList());
//    }
}
