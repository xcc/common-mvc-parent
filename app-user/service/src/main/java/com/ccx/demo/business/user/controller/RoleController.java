package com.ccx.demo.business.user.controller;

import app.common.starter.entity.OrderBy;
import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.encrypt.domain.starter.annotations.AesPathVariable;
import app.encrypt.domain.starter.annotations.AesRequestParam;
import app.encrypt.domain.starter.model.DataVersion;
import app.security.starter.service.AuthorityService;
import app.security.starter.vo.Authority;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.VueSwaggerGroup;
import com.ccx.demo.business.user.dto.TabRoleInsertDTO;
import com.ccx.demo.business.user.dto.TabRoleUpdateDTO;
import com.ccx.demo.business.user.entity.TabRole;
import com.ccx.demo.business.user.service.RoleService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 对外接口：角色
 *
 * @author 谢长春 on 2019-08-29
 */
@Api(tags = "角色")
@ApiSort(3) // 控制接口排序
@RequestMapping("/role")
@Controller
@Slf4j
@RequiredArgsConstructor
public class RoleController {

    private final RoleService service;
    private final AuthorityService authorityService;

    @VueSwaggerGroup
    @ApiOperation(value = "获取全部权限配置树集合"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 1, author = "谢长春")
    @GetMapping("/authority/tree")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_load')")
    @ResponseBody
    public Result<Authority> tree(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Authority>().execute(result -> result.setSuccess(Lists.newArrayList(authorityService.getTree())));
    }

    @VueSwaggerGroup
    @ApiOperation(value = "获取全部展开后的权限配置集合，可通过 parentCode 构造成权限树"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 2, author = "谢长春")
    @GetMapping("/authority/list")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_load')")
    @ResponseBody
    public Result<Authority> list(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Authority>().execute(result -> result.setSuccess(authorityService.getList()));
    }

    @VueSwaggerGroup
    @GetMapping("/page/{number}/{limit}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_load')")
    @ApiOperation(value = "分页查询角色"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            includeParameters = {"number", "limit", "orderBy", "deleted", "name", "createUserId", "updateUserId"}
    )
    @ResponseBody
    public Result<TabRole> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码；示例：1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100；示例：10") @PathVariable final int limit,
            @ApiParam(value = "名称；示例：普通用户") @RequestParam(required = false) final String name,
            @ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            @ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            @ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<TabRole>().execute(result -> {
            final TabRole condition = new TabRole();
            condition.setName(name);
            condition.setCreateUserId(createUserId);
            condition.setUpdateUserId(updateUserId);
            condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.page(
                    condition,
                    Page.builder().number(number).limit(limit).build()
            ));
        });
    }

    @VueSwaggerGroup
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_load')")
    @ApiOperation(value = "按 id 查询角色"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 5, author = "谢长春")
    @ResponseBody
    public Result<TabRole> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id", type = "string") @AesPathVariable(clazz = String.class) final String id
    ) {
        return new Result<TabRole>().execute(result -> {
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

    @VueSwaggerGroup
    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_insert')")
    @ApiOperation(value = "新增角色"
            , tags = {""}
            , notes = ""
    )
//    @ApiImplicitParam(name = "body", dataType = "TabRoleInsertDTO", dataTypeClass = TabRoleInsertDTO.class, required = true)
    @ApiOperationSupport(
            order = 6,
            author = "谢长春",
            includeParameters = {
                    "body.name", "body.authorityTree"
            })
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final TabRoleInsertDTO body) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.insert(body, user.getId());
        });
    }

    @VueSwaggerGroup
    @PutMapping("/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_update')")
    @ApiOperation(value = "修改角色"
            , tags = {""}
            , notes = ""
    )
    @ApiImplicitParam(name = "body", dataType = "TabRoleUpdateDTO", dataTypeClass = TabRoleUpdateDTO.class, required = true)
    @ApiOperationSupport(
            order = 7,
            author = "谢长春",
            includeParameters = {
                    "body.name", "body.remark", "body.authorityTree"
            })
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv,
            @RequestBody final TabRoleUpdateDTO body) {
        return new Result<Void>().call(() -> {
            body.setUpdateTime(dv.getUpdateTime());
            service.update(dv.getStringId(), user.getId(), body);
        });
    }

    @VueSwaggerGroup
    @PatchMapping("/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_delete')")
    @ApiOperation(value = "逻辑删除角色"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 8, author = "谢长春")
    @ResponseBody
    public Result<Void> markDeleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
        return new Result<Void>().call(() -> {
            service.markDeleteById(dv.getStringId(), dv.getUpdateTime(), user.getId());
        });
    }

    @VueSwaggerGroup
    @PatchMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_delete')")
    @ApiOperation(value = "批量逻辑删除角色"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 9,
            author = "谢长春",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
//    @ApiImplicitParam(name = "body", dataType = "TabRoleUpdateDTO", dataTypeClass = TabRoleUpdateDTO.class, required = true)
    @ResponseBody
    public Result<Void> markDelete(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDelete(
                    body.stream().map(DataVersion::getStringId).collect(Collectors.toSet())
                    , body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }

    @VueSwaggerGroup
    @GetMapping("/options")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'role_list_load')")
    @ApiOperation(value = "获取所有有效角色列表"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 10, author = "谢长春")
    @ResponseBody
    public Result<TabRole> options(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<TabRole>().execute(result -> result.setSuccess(service.getOptions()));
    }

}
