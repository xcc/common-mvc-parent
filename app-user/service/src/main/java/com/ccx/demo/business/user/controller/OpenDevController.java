package com.ccx.demo.business.user.controller;

import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.enums.starter.TokenClient;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.ccx.demo.business.user.enums.AppCode;
import com.ccx.demo.business.user.enums.RegisterSource;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 开发环境支撑
 *
 * @author 谢长春 on 2017/11/13.
 */
@Api(tags = "开发环境支撑")
@ApiSort(0)
@RequestMapping("/open/dev")
@Controller
@Slf4j
@RequiredArgsConstructor
@ConditionalOnExpression("!'prod'.equals('${spring.app.env}')") // 生产环境不初始化这个 controller
public class OpenDevController {
    @VueSwaggerGroup
    @AppSwaggerGroup
    @WechatSwaggerGroup
    @GetMapping("/enums")
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ApiOperation(value = "查看应用所有枚举值说明"
            , tags = {""}
            , notes = ""
    )
    @ResponseBody
    public Result<Map<String, Map<String, String>>> enums(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Map<String, Map<String, String>>>().execute(result -> {
//            final HashBasedTable<String, String, String> table = HashBasedTable.create();
//            for (TokenClient ele : TokenClient.values()) {
//                table.put(TokenClient.class.getSimpleName(), ele.name(), ele.comment);
//            }
            final Map<String, Map<String, String>> table = Maps.newHashMap();
            {
                table.put(TokenClient.class.getSimpleName(), Maps.newLinkedHashMap());
                for (TokenClient ele : TokenClient.values()) {
                    table.get(TokenClient.class.getSimpleName()).put(ele.name(), ele.comment);
                }
            }
            {
                table.put(Code.class.getSimpleName(), Maps.newLinkedHashMap());
                for (Code ele : Code.values()) {
                    table.get(Code.class.getSimpleName()).put(ele.name(), ele.comment);
                }
                for (AppCode ele : AppCode.values()) {
                    table.get(Code.class.getSimpleName()).put(ele.name(), ele.comment);
                }
            }
            {
                table.put(RegisterSource.class.getSimpleName(), Maps.newLinkedHashMap());
                for (RegisterSource ele : RegisterSource.values()) {
                    table.get(RegisterSource.class.getSimpleName()).put(ele.name(), ele.comment);
                }
            }
            result.setSuccess(table);
        });
    }

    @GetMapping("/headers")
    @ResponseBody
    public Result<LinkedHashMap<String, String>> headers(final HttpServletRequest request) {
        return new Result<LinkedHashMap<String, String>>().execute(result -> {
            final Enumeration<String> headerNames = request.getHeaderNames();
            final LinkedHashMap<String, String> map = new LinkedHashMap<>();
            while (headerNames.hasMoreElements()) {
                String key = headerNames.nextElement();
                map.put(key, request.getHeader(key));
            }
            HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            map.put("req.getRequestURI", req.getRequestURI());
            map.put("req.getRemoteHost", req.getRemoteHost());
            map.put("req.getRemoteAddr", req.getRemoteAddr());
            result.setSuccess(map);
        });
    }
}
