package com.ccx.demo.feign.user.service;

import com.ccx.demo.feign.user.vo.ApiUserVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 挡板：用户服务调用包装
 *
 * @author 谢长春 2022-11-06
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.feign.user") // user 服务属性已配置
@ConditionalOnMissingBean(UserServiceEnabledImpl.class) // 且 user 服务开关未打开时才加载挡板实现
public class UserServiceDisabledImpl implements IUserService {

    @PostConstruct
    private void postConstruct() {
        log.warn("spring.app.feign.user.enabled 配置项开关未开启，加载挡板逻辑实现");
    }

    @Override
    public List<ApiUserVO> listByIds(Collection<Long> ids) {
        log.warn("spring.app.feign.user.enabled 配置项开关未开启");
        return Collections.emptyList();
    }

}
