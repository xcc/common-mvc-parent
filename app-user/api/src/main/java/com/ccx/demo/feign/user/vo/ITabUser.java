package com.ccx.demo.feign.user.vo;

import app.common.starter.entity.OrderBy;
import com.ccx.demo.feign.user.dto.UserAvatarImageDTO;

import java.util.List;
import java.util.Set;

/**
 * 用户服务实体规范
 *
 * @author 谢长春 2022-11-06
 */
public interface ITabUser {
    /**
     * 数据ID
     */
    void setId(Long id);

    /**
     * 登录名
     */
    void setUsername(String username);

    /**
     * 登录密码
     */
    void setPassword(String password);

    /**
     * 用户昵称
     */
    void setNickname(String nickname);

    /**
     * 手机号
     */
    void setPhone(String phone);

    /**
     * 原手机号
     */
    void setOldPhone(String oldPhone);

    /**
     * 邮箱
     */
    void setEmail(String email);

    /**
     * 用户头像
     */
    void setAvatar(UserAvatarImageDTO avatar);

    /**
     * 角色 ID 集合，tab_role.id，{@link String}[]
     * 角色 ID 集合，tab_role.id TabRole#getId()
     */
    void setRoles(String[] roles);

    /**
     * 创建时间
     */
    void setCreateTime(String createTime);

    /**
     * 创建用户ID
     */
    void setCreateUserId(Long createUserId);

    /**
     * 修改时间
     */
    void setUpdateTime(String updateTime);

    /**
     * 修改用户ID
     */
    void setUpdateUserId(Long updateUserId);

    /**
     * 是否禁用账号，禁用账号无法登录。 [0:否,1:是]
     */
    void setDisabled(Boolean disabled);

    /**
     * 是否逻辑删除。 [0:否,1:是]
     */
    void setDeleted(Boolean deleted);

    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    void setIds(Set<Long> ids);

    /**
     * 排序字段
     */
    void setOrderBy(List<OrderBy> orderBy);

    /**
     * 新增操作人昵称
     */
    void setCreateUserNickname(String createUserNickname);

    /**
     * 更新操作人昵称
     */
    void setUpdateUserNickname(String updateUserNickname);

    /**
     * 创建时间格式化，仅用于前端展示
     *
     * @return String
     */
    void setCreateTimeFormat(String createTimeFormat);

    /**
     * 修改时间格式化，仅用于前端展示
     *
     * @return String
     */
    void setUpdateTimeFormat(String updateTimeFormat);

    /**
     * 数据版本号，用于更新和删除参数，规避脏数据更新
     *
     * @return String
     */
    void setDv(String dv);
//    /**
//     * 创建时间格式化，仅用于前端展示
//     *
//     * @return String
//     */
//    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
//    @ApiModelProperty(value = "创建时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
//    default String getCreateTimeFormat() {
//        return yyyyMMddHHmmss.parseOfNullable(createTime).map(yyyy_MM_dd_HH_mm_ss::format).orElse(null);
//    }
//
//    /**
//     * 修改时间格式化，仅用于前端展示
//     *
//     * @return String
//     */
//    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
//    @ApiModelProperty(value = "修改时间格式化，仅用于前端展示", position = 1, accessMode = READ_ONLY)
//    default String getUpdateTimeFormat() {
//        return yyyyMMddHHmmssSSS.parseOfNullable(updateTime).map(yyyy_MM_dd_HH_mm_ss_SSS::format).orElse(null);
//    }

}
