package com.ccx.demo.feign.user.vo;

import app.common.starter.entity.OrderBy;
import app.encrypt.domain.starter.annotations.AesIdLongJsonConverter;
import app.encrypt.domain.starter.annotations.AesIdStringArrayJsonConverter;
import com.ccx.demo.feign.user.dto.UserAvatarImageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 暴露给外部访问的用户属性
 *
 * @author 谢长春 2022-11-06
 */
@ApiModel(description = "用户")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ApiUserVO implements ITabUser {
    /**
     * 数据ID
     */
    @AesIdLongJsonConverter // id 使用 aes 加密@Positive@ApiModelProperty(value = "数据ID", position = 1, dataType = "string")
    private Long id;
    /**
     * 登录名
     */
    @ApiModelProperty(value = "登录名", position = 3)
    private String username;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "昵称", position = 5)
    private String nickname;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", position = 6)
    private String phone;
    /**
     * 原手机号
     */
    @ApiModelProperty(value = "原手机号", position = 6)
    private String oldPhone;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱", position = 7)
    private String email;
    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像", position = 8)
    private UserAvatarImageDTO avatar;
    /**
     * 角色 ID 集合，tab_role.id，{@link String}[]
     * 角色 ID 集合，tab_role.id TabRole#getId()
     */
    @AesIdStringArrayJsonConverter
    // id 使用 aes 加密@ApiModelProperty(value = "角色 ID 集合，tab_role.id，{@link String}[]", position = 9)
    private String[] roles;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "数据新增时间，格式:yyyyMMddHHmmss", example = "20200202020202", position = 11)
    private String createTime;
    /**
     * 创建用户ID
     */
    @AesIdLongJsonConverter // id 使用 aes 加密
    @ApiModelProperty(value = "新增操作人id", position = 12)
    private Long createUserId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "数据最后一次更新时间，格式:yyyyMMddHHmmssSSS", example = "20200202020202002", position = 13)
    private String updateTime;
    /**
     * 修改用户ID
     */
    @AesIdLongJsonConverter // id 使用 aes 加密
    @ApiModelProperty(value = "更新操作人id", position = 14)
    private Long updateUserId;
    /**
     * 是否为隐藏用户，禁止编辑且前端列表不展示。 [0:否,1:是]
     */
    @ApiModelProperty(hidden = true)
    private Boolean hidden;
    /**
     * 是否禁用账号，禁用账号无法登录。 [0:否,1:是]
     */
    @ApiModelProperty(value = "是否禁用账号，禁用账号无法登录。 [0:否,1:是]", position = 16)
    private Boolean disabled;
    /**
     * 是否逻辑删除。 [0:否,1:是]
     */
    @ApiModelProperty(value = "是否逻辑删除。 [0:否,1:是]", position = 16)
    private Boolean deleted;

    /**
     * 新增操作人昵称
     */
    @ApiModelProperty(value = "新增操作人昵称", position = 14, accessMode = READ_ONLY)
    private String createUserNickname;
    /**
     * 更新操作人昵称
     */
    @ApiModelProperty(value = "更新操作人昵称", position = 14, accessMode = READ_ONLY)
    private String updateUserNickname;

    /**
     * 创建时间格式化，仅用于前端展示
     */
    @ApiModelProperty(value = "创建时间格式化，仅用于前端展示", position = 15, accessMode = READ_ONLY)
    private String createTimeFormat;
    /**
     * 修改时间格式化，仅用于前端展示
     */
    @ApiModelProperty(value = "修改时间格式化，仅用于前端展示", position = 16, accessMode = READ_ONLY)
    private String updateTimeFormat;
    /**
     * 角色名称集合
     */
    @ApiModelProperty(value = "角色名称集合", position = 28, accessMode = READ_ONLY)
    private String[] roleNames;

    /**
     * 数据版本号，用于更新和删除参数，规避脏数据更新
     */
    @ApiModelProperty(value = "数据版本号，用于更新和删除参数，规避脏数据更新", position = 17, accessMode = READ_ONLY)
    private String dv;

    @Deprecated
    @Override
    public void setPassword(String password) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setIds(Set<Long> ids) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setOrderBy(List<OrderBy> orderBy) {
        // 不需要的属性
    }
}
