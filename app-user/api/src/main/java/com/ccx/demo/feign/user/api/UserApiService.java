package com.ccx.demo.feign.user.api;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.Result;
import com.ccx.demo.feign.user.dto.ApiUserDTO;
import com.ccx.demo.feign.user.vo.ApiUserVO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * 用户服务调用
 *
 * @author 谢长春 2022-11-06
 */
@AppEventRequest(skip = true)
@ConditionalOnProperty(value = "spring.app.feign.user.enabled", havingValue = "true")
@FeignClient(name = "userApiService", url = "${spring.app.feign.user.api}/feign/user")
public interface UserApiService {
    /**
     * 分页查询用户
     *
     * @param dto {@link ApiUserDTO}
     * @return {@link Result}
     */
    @GetMapping(value = "/page/{number}/{limit}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<ApiUserVO> page(
            @PathVariable(name = "number") final int number,
            @PathVariable(name = "limit") final int limit,
            @SpringQueryMap final ApiUserDTO dto);

    /**
     * 按 id 批量查询用户
     *
     * @param ids Collection<Long> {@link ApiUserDTO#getId()}
     * @return {@link Result}
     */
    @PostMapping(value = "/listByIds", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<ApiUserVO> listByIds(@RequestBody final Collection<Long> ids);

    /**
     * 新增用户
     *
     * @param dto {@link ApiUserDTO}
     * @return {@link Result}
     */
    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<Void> insert(@RequestBody final ApiUserDTO dto);

    /**
     * 修改用户
     *
     * @param dto {@link ApiUserDTO}
     * @return {@link Result}
     */
    @PutMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<Void> update(@RequestBody final ApiUserDTO dto);

    /**
     * 按 id 批量逻辑删除用户
     *
     * @param ids Long[] {@link ApiUserVO#getId()}
     * @return {@link Result}
     */
    @PatchMapping(value = "/markDeleteByIds", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<Void> markDeleteByIds(@RequestBody final Long[] ids);

    /**
     * 按 dv 批量逻辑删除用户
     *
     * @param dvs String[] {@link ApiUserVO#getDv()}
     * @return {@link Result}
     */
    @PatchMapping(value = "/markDeleteByDvs", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    Result<Void> markDeleteByDvs(@RequestBody final String[] dvs);

}
