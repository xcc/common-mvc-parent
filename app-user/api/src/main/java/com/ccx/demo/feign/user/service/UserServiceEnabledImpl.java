package com.ccx.demo.feign.user.service;

import app.common.starter.entity.Result;
import com.ccx.demo.feign.user.api.UserApiService;
import com.ccx.demo.feign.user.dto.ApiUserDTO;
import com.ccx.demo.feign.user.vo.ApiUserVO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * 实现：用户服务调用包装
 *
 * @author 谢长春 2022-11-06
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConfigurationProperties("spring.app.feign.user")
@ConditionalOnProperty(value = "spring.app.feign.user.enabled", havingValue = "true") // user 服务开关打开时才加载具体实现
public class UserServiceEnabledImpl implements IUserService {

    /**
     * 用户服务调用开关；true：开，false：关，关闭之后会启用挡板
     */
    @Setter
    @Getter
    private boolean enabled = true;
    /**
     * 用户服务API地址，默认值：http://app-user:8080
     */
    @Setter
    @Getter
    private String api = "http://app-user:8080";

    private final UserApiService userApiService;

    @PostConstruct
    private void postConstruct() {
        log.info("spring.app.feign.user.enabled: {}", enabled);
        log.info("spring.app.feign.user.api: {}", api);
    }

    /**
     * 用户 按 id 批量查询列表，返回 List， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Long {@link ApiUserDTO#getId()}  数据 id 集合
     * @return List<ApiUserVO> 结果集合
     */
    @Override
    public @NotNull(message = "返回值不能为null") List<ApiUserVO> listByIds(final Collection<Long> ids) {
        final Result<ApiUserVO> result = userApiService.listByIds(ids).assertSuccess();
        return result.getData();
    }
}
