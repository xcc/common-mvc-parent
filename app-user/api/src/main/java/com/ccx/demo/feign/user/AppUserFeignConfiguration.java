package com.ccx.demo.feign.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

/**
 * 用户服务 feign 自定义配置
 *
 * @author 谢长春 2018-10-3
 */
@Slf4j
@EnableRetry
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@ComponentScan("com.ccx.demo.feign.user")
@EnableFeignClients(basePackages = "com.ccx.demo.feign.user")
public class AppUserFeignConfiguration {
//    public static final String HEADER_X_TOKEN = "x-token";
//
//    @Bean // 已通过配置项实现 spring.sleuth.baggage.remote-fields: x-token
//    public RequestInterceptor appUserFeignClientRequestInterceptor() {
//        return requestTemplate -> {
//            // 读取线程上下文对象中存储的 token ， 追加到 feign 调用请求头
//            requestTemplate.header(HEADER_X_TOKEN, MDC.get(HEADER_X_TOKEN));
//        };
//    }

}
