package com.ccx.demo.feign.user.service;

import com.ccx.demo.feign.user.dto.ApiUserDTO;
import com.ccx.demo.feign.user.vo.ApiUserVO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 接口：用户服务调用包装
 *
 * @author 谢长春 2022-11-06
 */
public interface IUserService {
    /**
     * 用户 按 id 批量查询列表，返回 List， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Long {@link ApiUserDTO#getId()}  数据 id 集合
     * @return List<ApiUserVO> 结果集合
     */
    List<ApiUserVO> listByIds(final Collection<Long> ids);

    /**
     * 用户 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids {@link Long} 数据 id 集合
     * @return Map<Long, ApiUserVO> 结果集合
     */
    default Map<Long, ApiUserVO> mapByIds(final Collection<Long> ids) {
        return this.listByIds(ids).stream().collect(Collectors.toMap(ApiUserVO::getId, Function.identity()));
    }
}
