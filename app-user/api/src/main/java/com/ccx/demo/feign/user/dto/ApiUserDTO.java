package com.ccx.demo.feign.user.dto;

import app.common.starter.entity.OrderBy;
import app.encrypt.domain.starter.annotations.AesIdLongJsonConverter;
import app.encrypt.domain.starter.annotations.AesIdStringArrayJsonConverter;
import com.ccx.demo.feign.user.vo.ITabUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

/**
 * 暴露给外部操作的用户属性
 *
 * @author 谢长春 2022-11-06
 */
@ApiModel(description = "用户")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ApiUserDTO implements ITabUser {
    /**
     * 数据ID
     */
    @AesIdLongJsonConverter // id 使用 aes 加密@Positive@ApiModelProperty(value = "数据ID", position = 1, dataType = "string")
    private Long id;
    /**
     * 登录名
     */
    @ApiModelProperty(value = "登录名", position = 3)
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", position = 4)
    private String password;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "昵称", position = 5)
    private String nickname;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", position = 6)
    private String phone;
    /**
     * 原手机号
     */
    @ApiModelProperty(value = "原手机号", position = 6)
    private String oldPhone;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱", position = 7)
    private String email;
    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像", position = 8)
    private UserAvatarImageDTO avatar;
    /**
     * 角色 ID 集合，tab_role.id，{@link String}[]
     * 角色 ID 集合，tab_role.id TabRole#getId()
     */
    @AesIdStringArrayJsonConverter
    // id 使用 aes 加密@ApiModelProperty(value = "角色 ID 集合，tab_role.id，{@link String}[]", position = 9)
    private String[] roles;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "数据新增时间，格式:yyyyMMddHHmmss", example = "20200202020202", position = 11)
    private String createTime;
    /**
     * 创建用户ID
     */
    @AesIdLongJsonConverter // id 使用 aes 加密
    @ApiModelProperty(value = "新增操作人id", position = 12)
    private Long createUserId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "数据最后一次更新时间，格式:yyyyMMddHHmmssSSS", example = "20200202020202002", position = 13)
    private String updateTime;
    /**
     * 修改用户ID
     */
    @AesIdLongJsonConverter // id 使用 aes 加密
    @ApiModelProperty(value = "更新操作人id", position = 14)
    private Long updateUserId;
    /**
     * 扩展查询字段：按 id 批量查询，仅后端使用，对前端隐藏
     */
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Set<Long> ids;
    /**
     * 排序字段
     */
    @ApiModelProperty(value = "查询排序字段", position = 18)
    private List<OrderBy> orderBy;

    @Deprecated
    @Override
    public void setDisabled(Boolean disabled) {
        // 不需要的属性
    }

    @Override
    public void setDeleted(Boolean deleted) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setCreateUserNickname(String createUserNickname) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setUpdateUserNickname(String updateUserNickname) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setCreateTimeFormat(String createTimeFormat) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setUpdateTimeFormat(String updateTimeFormat) {
        // 不需要的属性
    }

    @Deprecated
    @Override
    public void setDv(String dv) {
        // 不需要的属性
    }
}
