package com.ccx.demo.business.common.controller;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.Result;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Objects;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 缓存查询
 *
 * @author 谢长春 2022-06-17
 */
@ApiIgnore
@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/admin/cache")
@Slf4j
@RequiredArgsConstructor
public class AdminCacheController {
    @AppEventRequest
    @ApiIgnore
    @GetMapping("/system")
    @ApiOperation(value = "系统环境变量")
    @ApiOperationSupport(author = "谢长春")
    @ResponseBody
    public Result<SortedMap<String, Object>> system() {
        return new Result<SortedMap<String, Object>>().execute(result -> {
            final Properties properties = System.getProperties();
            final SortedMap<String, Object> map = new TreeMap<>();
            properties.forEach((key, value) -> map.put(Objects.toString(key), value));
            result.setSuccess(map);
        });
    }

//    private final TokenCache tokenCache;
//    private final LoginCache loginCache;
//    private final TabUserCache userCache;
//    private final TabRoleCache roleCache;
//    private final TabGoodsCache goodsCache;
//
//    @GetMapping("/token/{id}")
//    @ResponseBody
//    public Result<Token> getTokenCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final Long id) {
//        return new Result<Token>().execute(result -> {
//            final List<Token> list = Lists.newArrayList();
//            for (TokenClient client : TokenClient.values()) {
//                tokenCache.get(new Token().setClient(client).setUserId(id)).ifPresent(list::add);
//            }
//            result.setSuccess(list);
//        });
//    }
//
//    @DeleteMapping("/token/{client}/{id}")
//    @ResponseBody
//    public Result<Void> deleteTokenCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final TokenClient client
//            , @PathVariable final Long id
//    ) {
//        return new Result<Void>().call(() -> {
//            tokenCache.delete(new Token().setClient(client).setUserId(id));
//        });
//    }
//
//    @GetMapping("/login/{username}")
//    @ResponseBody
//    public Result<TabUser> getLoginCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final String username) {
//        return new Result<TabUser>().execute(result -> {
//            result.setSuccess(loginCache.get(username).orElse(null));
//        });
//    }
//
//    @DeleteMapping("/login/clear")
//    @ResponseBody
//    public Result<Void> clearLoginCache(
//            @AuthenticationPrincipal final TabUser user
//    ) {
//        return new Result<Void>().call(loginCache::clear);
//    }
//
//    @GetMapping("/user/{id}")
//    @ResponseBody
//    public Result<TabUser> getUserCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final Long id) {
//        return new Result<TabUser>().execute(result -> {
//            result.setSuccess(userCache.get(id).orElse(null));
//        });
//    }
//
//    @DeleteMapping("/user/{id}")
//    @ResponseBody
//    public Result<Void> deleteUserCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final Long id) {
//        return new Result<Void>().call(() -> userCache.delete(id));
//    }
//
//    @DeleteMapping("/user/clear")
//    @ResponseBody
//    public Result<Void> clearUserCache(
//            @AuthenticationPrincipal final TabUser user
//    ) {
//        return new Result<Void>().call(userCache::clear);
//    }
//
//    @GetMapping("/role/{id}")
//    @ResponseBody
//    public Result<TabRole> getRoleCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final String id) {
//        return new Result<TabRole>().execute(result -> {
//            result.setSuccess(roleCache.get(id).orElse(null));
//        });
//    }
//
//    @DeleteMapping("/role/{id}")
//    @ResponseBody
//    public Result<Void> deleteRoleCache(
//            @AuthenticationPrincipal final TabUser user
//            , @PathVariable final String id) {
//        return new Result<Void>().call(() -> roleCache.delete(id));
//    }
//
//    @DeleteMapping("/role/clear")
//    @ResponseBody
//    public Result<Void> clearRoleCache(
//            @AuthenticationPrincipal final TabUser user
//    ) {
//        return new Result<Void>().call(roleCache::clear);
//    }
//
}
