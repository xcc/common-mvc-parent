package com.ccx.demo.business.common.controller;

import app.common.starter.entity.OrderBy;
import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.encrypt.domain.starter.annotations.AesPathVariable;
import app.encrypt.domain.starter.annotations.AesRequestParam;
import app.encrypt.domain.starter.model.DataVersion;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.VueSwaggerGroup;
import com.ccx.demo.business.common.entity.TabErrorLog;
import com.ccx.demo.business.common.service.ErrorLogService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 对外接口：异常日志记录
 *
 * @author 谢长春 on 2022-02-16 V20221105
 */
@Api(tags = "异常日志记录")
//@ApiSort() // 控制接口排序
@RequestMapping("/error-log")
@Controller
@Slf4j
@RequiredArgsConstructor
public class ErrorLogController {

    private final ErrorLogService service;

    @VueSwaggerGroup
    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询异常日志记录"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 1,
            author = "谢长春",
            includeParameters = {"traceId"}
    )
    @ResponseBody
    public Result<TabErrorLog> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
            @ApiParam(value = "链路追踪id；示例：abd1es55", type = "string") @RequestParam(required = false) final String traceId,
            @ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            @ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            @ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<TabErrorLog>().execute(result -> { // Controller 方法逻辑写在这里
            final TabErrorLog condition = new TabErrorLog();
            condition.setTraceId(traceId);
            condition.setCreateUserId(createUserId);
            condition.setUpdateUserId(updateUserId);
            condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.page(condition, Page.of(number, limit)));
        });
    }

    //// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询异常日志记录"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 2,
//            author = "谢长春",
//            includeParameters = {"orderBy", "traceId", "createUserId", "updateUserId", "deleted"}
//    )
//    @ResponseBody
//    public Result<TabErrorLog> list(@ApiIgnore @AuthenticationPrincipal final IUser user, final TabErrorLog condition) {
//        return new Result<TabErrorLog>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.list( condition ));
//        });
//    }
//
    @VueSwaggerGroup
    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询异常日志记录"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<TabErrorLog> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id", type = "string") @AesPathVariable final Long id
    ) {
        return new Result<TabErrorLog>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

    //    @PostMapping
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
//    @ApiOperation(value = "新增异常日志记录"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 4,
//            author = "谢长春",
//            includeParameters = {
//                    "body.traceId", "body.message"
//            })
//    @ResponseBody
//    public Result<TabErrorLog> insert(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final TabErrorLogInsertDTO body
//    ) {
//        return new Result<TabErrorLog>().execute(result -> { // Controller 方法逻辑写在这里
//            result.setSuccess(service.insert(body, user.getId()));
//        });
//    }
//
//    @PutMapping("/{dv}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
//    @ApiOperation(value = "修改异常日志记录"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 5,
//            author = "谢长春",
//            ignoreParameters = {
//                    "body.traceId", "body.message"
//            })
//    @ResponseBody
//    public Result<Void> update(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv,
//            @RequestBody final TabErrorLogUpdateDTO body
//    ) {
//        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
//            final AesApiId.AesId decrypt = AesApiId.decrypt(dv);
//            body.setUpdateTime(decrypt.getUpdateTime());
//            service.update(decrypt.getLongId(), user.getId(), body);
//        });
//    }
    @VueSwaggerGroup
    @PatchMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除异常日志记录"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 6, author = "谢长春")
    @ResponseBody
    public Result<Void> markDeleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv
    ) {
        return new Result<Void>().call(() -> {
            service.markDeleteById(dv.getLongId(), dv.getUpdateTime(), user.getId());
        });
    }

    @VueSwaggerGroup
    @PatchMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除异常日志记录"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 7,
            author = "谢长春",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDeleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDeleteByIds(
                    body.stream().map(DataVersion::getLongId).collect(Collectors.toSet())
                    , body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }

//    @PatchMapping("/{dv}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除异常日志记录"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(order = 8, author = "谢长春")
//    @ResponseBody
//    public Result<Void> deleteById(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv") @PathVariable final String dv) {
//        return new Result<Void>().call(() -> {
//            final AesApiId.AesId decrypt = AesApiId.decrypt(dv);
//            service.deleteById(decrypt.getLongId(), user.getId());
//        });
//    }
//
//    @PatchMapping
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除异常日志记录"
//            , tags = {""}
//            , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 9,
//            author = "谢长春",
//            params = @DynamicParameters(name = "DvArray", properties = {
//                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
//            })
//    )
//    @ResponseBody
//    public Result<Void> deleteByIds(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final Set<String> body
//    ) {
//        return new Result<Void>().call(() -> {
//            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
//            final List<AesApiId.AesId> decryptList = body.stream().map(AesApiId::decrypt).collect(Collectors.toList());
//            service.deleteByIds(
//                    decryptList.stream().map(AesApiId.AesId::getLongId).collect(Collectors.toSet())
//                    , decryptList.stream().map(AesApiId.AesId::getUpdateTime).collect(Collectors.toSet())
//                    , user.getId()
//            );
//        });
//    }
//
}
