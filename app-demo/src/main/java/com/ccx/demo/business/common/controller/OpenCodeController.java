package com.ccx.demo.business.common.controller;

import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.Result;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.app.image.code.starter.service.ImageCodeService;
import com.app.image.code.starter.vo.ImageCodeVO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 验证码接口
 *
 * @author 谢长春 on 2017-9-18
 */
@Api(tags = "图片验证码")
@ApiSort(2)
@RequestMapping("/open/code")
@Controller
@Slf4j
@RequiredArgsConstructor
public class OpenCodeController {
    private final ImageCodeService imageCodeService;

    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @GetMapping
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ApiOperation(value = "获取图片验证码"
            , tags = {""}
            , notes = "验证码图片以 base64 格式放在 data 数组中返回"
    )
    @ResponseBody
    public Result<ImageCodeVO> getImageCode() {
        return new Result<ImageCodeVO>().execute(result -> {
            result.setSuccess(imageCodeService.getImageCode());
        });
    }

    @AppEventRequest
    @ApiOperation(value = "校验图片验证码"
            , tags = {""}
            , notes = "仅用于开发环境测试图片验证码输入"
    )
    @GetMapping("/check/{uuid}/{code}")
    @ResponseBody
    @Deprecated
    public Result<Void> checkImageCode(
            @ApiParam(required = true, value = "唯一值", example = "uuid") @PathVariable String uuid
            , @ApiParam(required = true, value = "验证码", example = "a1b2") @PathVariable String code
    ) {
        return new Result<Void>().call(() -> {
            imageCodeService.asserts(uuid, code);
        });
    }

}
