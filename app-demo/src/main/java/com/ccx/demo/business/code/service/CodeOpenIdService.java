package com.ccx.demo.business.code.service;

import app.common.starter.entity.Page;
import app.common.starter.exception.UpdateRowsException;
import com.ccx.demo.business.code.dao.jpa.CodeOpenIdRepository;
import com.ccx.demo.business.code.dto.TabCodeOpenIdInsertDTO;
import com.ccx.demo.business.code.dto.TabCodeOpenIdUpdateDTO;
import com.ccx.demo.business.code.entity.TabCodeOpenId;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.*;

/**
 * 服务接口实现类：测试 OpenId 模板 V20221105
 *
 * @author 谢长春 on 2022-11-02
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class CodeOpenIdService
//      , ITabCodeOpenIdCache
{
    private final CodeOpenIdRepository repository;
    //private final TabCodeOpenIdCache tabCodeOpenIdCache;
    //private final UserService userService;

    /**
     * 新增 测试 OpenId 模板
     *
     * @param dto {@link TabCodeOpenIdInsertDTO} 实体对象
     * @return {@link TabCodeOpenId} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") TabCodeOpenId insert(
            @Valid @NotNull(message = "【dto】不能为null") final TabCodeOpenIdInsertDTO dto
    ) {
        final TabCodeOpenId obj = new TabCodeOpenId();
        BeanUtils.copyProperties(dto, obj);
        return repository.insert(obj);
    }
//    /**
//     * 批量新增 测试 OpenId 模板
//     *
//     * @param list   List<TabCodeOpenIdInsertDTO> {@link TabCodeOpenIdInsertDTO}  实体对象集合
//
//     * @return List<TabCodeOpenId> 实体对象集合
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public @NotNull(message = "返回值不能为null") List<TabCodeOpenId> insert(
//            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull TabCodeOpenIdInsertDTO> list
//            ) {
//        return repository.insert(list.stream()
//            .map(dto -> {
//                final TabCodeOpenId obj = new TabCodeOpenId();
//                BeanUtils.copyProperties(dto, obj);
//                return obj;
//            })
//            .collect(Collectors.toList())
//        );
//    }

    /**
     * 更新 测试 OpenId 模板 ；
     *
     * @param id  {@link Long} 数据ID
     * @param dto {@link TabCodeOpenIdUpdateDTO} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(
            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id

            , @Valid @NotNull(message = "【dto】不能为null") final TabCodeOpenIdUpdateDTO dto) {
        final TabCodeOpenId obj = new TabCodeOpenId();
        BeanUtils.copyProperties(dto, obj);
        UpdateRowsException.asserts(repository.update(id, obj));
        // tabCodeOpenIdCache.delete(id); // 若使用缓存需要解开代码
    }

//    /**
//     * 测试 OpenId 模板 按ID删除，物理删除
//     *
//     * @param id     {@link Long} 数据ID
//
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
//            ) {
//        DeleteRowsException.asserts(repository.deleteById(id));
//        // tabCodeOpenIdCache.delete(id); // 若使用缓存需要解开代码
//    }
//
////    /**
////     * 测试 OpenId 模板 按 id+updateTime 删除，物理删除
////     *
////     * @param id         {@link Long} 数据ID
////     * @param updateTime {@link String} 最后一次更新时间
////
////     */
////    @Transactional(rollbackFor = Exception.class)
////    public void deleteById(
////            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
////            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
////
////    ) {
////        DeleteRowsException.asserts(repository.deleteById(id, updateTime), ids.size());
////        // tabCodeOpenIdCache.delete(id); // 若使用缓存需要解开代码
////    }
//
//    /**
//     * 测试 OpenId 模板 按ID删除，物理删除
//     *
//     * @param ids     Set<Long> {@link TabCodeOpenId#getId()} 数据ID
//
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
//            ) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids), ids.size());
//        // tabCodeOpenIdCache.delete(ids); // 若使用缓存需要解开代码
//    }
//
////    /**
////     * 测试 OpenId 模板 按 id+updateTime 删除，物理删除
////     *
////     * @param ids     Set<Long> {@link TabCodeOpenId#getId()} 数据ID
////     * @param updateTimes     Set<String> 最后一次更新时间
////
////     */
////    @Transactional(rollbackFor = Exception.class)
////    public void deleteByIds(
////            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
////            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
////
////    ) {
////        DeleteRowsException.asserts(repository.deleteByIds(ids, updateTimes), ids.size());
////        // tabCodeOpenIdCache.delete(ids); // 若使用缓存需要解开代码
////    }
//

    /**
     * 测试 OpenId 模板 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link Long} 数据ID
     * @return Optional<TabCodeOpenId> {@link TabCodeOpenId} 实体对象
     */
    public Optional<TabCodeOpenId> findById(final Long id) {
        if (Objects.isNull(id) || id < 1) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabCodeOpenId::cloneObject) // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
                ;
//         return Optional.ofNullable(repository.findCacheById(id)).map(TabCodeOpenId::cloneObject); // 若使用缓存需要解开代码
    }

    /**
     * 测试 OpenId 模板 按条件分页查询列表
     *
     * @param condition {@link TabCodeOpenId} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return QueryResults<TabCodeOpenId> {@link TabCodeOpenId} 分页对象
     */
    public @NotNull(message = "返回值不能为null") QueryResults<TabCodeOpenId> page(
            @NotNull(message = "【condition】不能为null") final TabCodeOpenId condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<TabCodeOpenId> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        //this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }

//    /**
//     * 测试 OpenId 模板 按条件查询并跳过指定行数
//     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
//     *
//     * @param skip      int 跳过 skip 行数据
//     * @param limit     int 获取 limit  行数据
//     * @param condition {@link TabCodeOpenId} 查询条件
//     * @return List<TabCodeOpenId> {@link TabCodeOpenId} 列表查询结果集
//     */
//    public @NotNull(message = "返回值不能为null") List<TabCodeOpenId> pull(final int skip, final int limit, final TabCodeOpenId condition) {
//        List<TabCodeOpenId> list = repository.pull(skip, limit, condition);
//        this.fillUserNickname(list);
//        return list;
//    }
//

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试 OpenId 模板 按条件查询列表
//     *
//     * @param condition {@link TabCodeOpenId} 查询条件
//     * @return List<TabCodeOpenId> {@link TabCodeOpenId} 结果集合
//     */
//    public @NotNull(message = "返回值不能为null") List<TabCodeOpenId> list(
//            @NotNull(message = "【condition】不能为null") final TabCodeOpenId condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 测试 OpenId 模板 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<Long> {@link TabCodeOpenId#getId()> 数据 id 集合
     * @return List<TabCodeOpenId> {@link TabCodeOpenId} 结果集合
     */
    public @NotNull(message = "返回值不能为null") List<TabCodeOpenId> listByIds(final Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }

    /**
     * 测试 OpenId 模板 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<Long> {@link TabCodeOpenId#getId()> 数据 id 集合
     * @return Map<Long, TabCodeOpenId> {@link TabCodeOpenId} 结果集合
     */
    public @NotNull(message = "返回值不能为null") Map<Long, TabCodeOpenId> mapByIds(final Set<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }

}
