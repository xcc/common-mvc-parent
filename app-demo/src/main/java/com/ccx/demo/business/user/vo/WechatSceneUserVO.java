package com.ccx.demo.business.user.vo;

import app.enums.starter.TokenClient;
import com.ccx.demo.business.user.entity.TabRole;
import com.ccx.demo.business.user.entity.TabUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 微信邀请用户
 *
 * @author 谢长春 2022-06-18
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "微信邀请用户")
public class WechatSceneUserVO extends TabUser {
    /**
     * 登录客户端
     */
    @ApiModelProperty(value = "登录客户端", example = "OM_PC")
    private TokenClient client;
    /**
     * 权限代码
     */
    @ApiModelProperty(value = "权限代码", example = "user_list_load")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<String> authorityList;

    /**
     * 角色名称集合 {@link TabRole#getName()}
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "角色名称集合", accessMode = READ_ONLY, position = 23)
    private List<String> roleNames;

    @JsonIgnore
    @Override
    public String getPassword() {
        return super.getPassword();
    }

    /**
     * 权限
     */
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (CollectionUtils.isEmpty(authorityList)) {
            return Collections.emptyList();
        }
        return authorityList.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
