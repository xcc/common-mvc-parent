package com.ccx.demo.business.common.service;

import app.common.starter.entity.Result;
import app.common.starter.vo.AppStaticVO;
import app.nfs.starter.dto.JsonFileDTO;
import app.nfs.starter.service.FileService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 应用全局配置
 *
 * @author 谢长春 2022-06-28
 */
@Slf4j
@Service
@RequiredArgsConstructor
@ConfigurationProperties("spring.app.static")
public class AppService {

    private static final JsonFileDTO appStaticJsonFileDTO;

    static {
        appStaticJsonFileDTO = new JsonFileDTO("static.json", "app/static.json");
    }

    /**
     * 应用静态资源
     * ${spring.app.static.list}
     */
    @Setter
    @Getter
    private List<AppStaticVO> list;
    private final FileService fileService;

    /**
     * 静态资源写入文件
     *
     * @return {@link CommandLineRunner}
     */
    @Bean
    public CommandLineRunner writeAppStaticJsonFileCommandLineRunner() {
        return args -> {
            log.info("写入应用静态资源：{}", appStaticJsonFileDTO);
            final String jsonText = new Result<AppStaticVO>()
                    .execute(result -> result.setSuccess(list))
                    .json();
            fileService.writeAndReplace(jsonText, appStaticJsonFileDTO);
        };
    }

    /**
     * 获取静态资源文件对象
     *
     * @return {@link JsonFileDTO}
     */
    public JsonFileDTO getAppStaticJsonFileDTO() {
        return appStaticJsonFileDTO;
    }

}
