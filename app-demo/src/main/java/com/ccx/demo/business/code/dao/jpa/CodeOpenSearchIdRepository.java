package com.ccx.demo.business.code.dao.jpa;

import app.common.starter.entity.Page;
import app.dao.jpa.starter.enums.Limit;
import app.dao.jpa.starter.where.QdslWhere;
import com.ccx.demo.business.code.entity.QTabCodeOpenSearchId;
import com.ccx.demo.business.code.entity.TabCodeOpenSearchId;
import com.google.common.collect.ObjectArrays;
import com.google.common.collect.Sets;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.NonUniqueResultException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static app.dao.jpa.starter.JPAConfiguration.getQueryFactory;

/**
 * 数据操作：测试 SearchId 模板 V20221105
 *
 * @author 谢长春 on 2022-11-02
 */

public interface CodeOpenSearchIdRepository
        extends JpaRepository<TabCodeOpenSearchId, String>,
        org.springframework.data.querydsl.QuerydslPredicateExecutor<TabCodeOpenSearchId> {


    // 每个 DAO 层顶部只能有一个查询实体,且必须以 table 命名,表示当前操作的数据库表. 当 table 作为主表的连接查询方法也必须写在这个类
    QTabCodeOpenSearchId table = QTabCodeOpenSearchId.tabCodeOpenSearchId;

    /**
     * 测试 SearchId 模板 按 条件查询单条数据，如果查询结果超过 1 条数据则抛出异常
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return Optional<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 返回实体对象
     */
    default Optional<TabCodeOpenSearchId> findOne(final TabCodeOpenSearchId condition) {
        return findOne(condition.where());
    }

    /**
     * 测试 SearchId 模板 按 条件查询单条数据，如果查询结果超过 1 条数据则抛出异常
     *
     * @param where {@link QdslWhere} 查询条件
     * @return Optional<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 返回实体对象
     */
    default Optional<TabCodeOpenSearchId> findOne(final QdslWhere where) {
        final List<TabCodeOpenSearchId> list = getQueryFactory()
                .selectFrom(table)
                .where(where.toPredicate())
                .limit(Limit.L2.value)
                .fetch();
        if (list.size() > 1) {
            throw new NonUniqueResultException("预期查询结果行数为1，实际查询结果行数大于1");
        }
        return list.stream().findFirst();
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询，以 id 为 key 返回map
     *
     * @param ids Collection<String> 数据ID
     * @return Map<String, TabCodeOpenSearchId> Map 对象集合
     */
    default Map<String, TabCodeOpenSearchId> mapByIds(final Collection<String> ids) {
        return findAllById(Sets.newHashSet(ids))
                .stream()
                .collect(Collectors.toMap(TabCodeOpenSearchId::getId, TabCodeOpenSearchId::cloneObject));
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询 id + 指定字段，返回 map ， id 为 key ， 指定字段为 value
     *
     * @param ids Collection<String> 数据ID
     * @param exp {@link QTabCodeOpenSearchId} 指定单个字段
     * @return Map<String, R> Map 对象集合
     */
    default <R> Map<String, R> mapByIds(final Collection<String> ids, final Expression<R> exp) {
        final Map<String, R> map = new HashMap<>(ids.size());
        getQueryFactory()
                .select(table.id, exp)
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .forEach(tuple -> {
                    if (Objects.nonNull(tuple.get(exp))) {
                        map.put(tuple.get(table.id), tuple.get(exp));
                    }
                });
        return map;
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询 id + 指定字段，返回 map ， id 为 key ， 指定字段投影到 {@link TabCodeOpenSearchId}
     *
     * @param ids  Collection<String> 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return Map<String, TabCodeOpenSearchId> Map 对象集合
     */
    default Map<String, TabCodeOpenSearchId> mapByIds(final Collection<String> ids, final Expression<?>... exps) {
        return getQueryFactory()
                //.select(Projections.bean(TabCodeOpenSearchId.class, ObjectArrays.concat(table.id, exps))) // 如果 exps 带了 id 这里再加一个 id 查询会报错 ：  Multiple entries with same key
                .select(Projections.bean(TabCodeOpenSearchId.class, exps))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(TabCodeOpenSearchId::getId, row -> row));
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询 id + 指定字段，返回 map ， id 为 key ， 指定字段投影到 Class<R>
     *
     * @param ids  Collection<String> 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return Map<String, R> Map 对象集合
     */
    default <R> Map<String, R> mapByIds(final Collection<String> ids, final Class<R> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                //.select(table.id, Projections.bean(clazz, ObjectArrays.concat(table.id, exps))) //  如果 exps 带了 id 这里再加一个 id 查询会报错 ：  Multiple entries with same key
                .select(table.id, Projections.bean(clazz, exps))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(tuple -> tuple.get(table.id), tuple -> Objects.requireNonNull(tuple.get(1, clazz))));
    }

    /**
     * 测试 SearchId 模板 求总数
     *
     * @param where QdslWhere 查询条件
     * @return long 总数
     */
    default long count(final QdslWhere where) {
        return getQueryFactory()
                .select(table.id.count())
                .from(table)
                .where(where.toPredicate())
                .fetch()
                .stream()
                .findFirst()
                .orElse(0L);
    }

    /**
     * 测试 SearchId 模板 按条件分页查询， 仅返回 id 字段，用于分页查询优化
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param page      {@link Page} 分页
     * @return QueryResults<String> {@link TabCodeOpenSearchId#getId()} 分页结果集
     */
    default QueryResults<String> pageIds(final TabCodeOpenSearchId condition, final Page page) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<String> list = getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .offset(page.offset())
                .limit(page.limit())
                .fetch();
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * 测试 SearchId 模板 按条件分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param page      {@link Page} 分页
     * @return QueryResults<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 分页结果集
     */
    default QueryResults<TabCodeOpenSearchId> page(final TabCodeOpenSearchId condition, final Page page) {
        { // 直接查询
            // return getQueryFactory()
            //        .selectFrom(table)
            //        .where(condition.where().toArray())
            //        .offset(page.offset())
            //        .limit(page.limit())
            //        .orderBy(condition.qdslOrderBy())
            //        .fetchResults();
        }
        { // 查询优化版
            final long count = count(condition.where());
            if (count == 0L) {
                return QueryResults.emptyResults();
            }
            final List<String> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
            final List<TabCodeOpenSearchId> list = listByIds(ids); // 再按 id 批量查询
            return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
        }
    }

    /**
     * 测试 SearchId 模板 按条件分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param page      {@link Page} 分页
     * @param exps      {@link Expression} 指定返回字段
     * @return QueryResults<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 分页结果集
     */
    default QueryResults<TabCodeOpenSearchId> page(final TabCodeOpenSearchId condition, final Page page, final Expression<?>... exps) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<String> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        final List<TabCodeOpenSearchId> list = listByIds(ids, exps); // 再按 id 批量查询
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * 测试 SearchId 模板 按条件分页查询，投影到 VO 类
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param page      {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @return QueryResults<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 分页结果集
     */
    default <T extends TabCodeOpenSearchId> QueryResults<T> page(final TabCodeOpenSearchId condition, final Page page, final Class<T> clazz) {
        return page(condition, page, clazz, TabCodeOpenSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板 按条件分页查询，查询指定字段，投影到 VO 类
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param page      {@link Page} 分页
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return QueryResults<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 分页结果集
     */
    default <T extends TabCodeOpenSearchId> QueryResults<T> page(final TabCodeOpenSearchId condition, final Page page, final Class<T> clazz, final Expression<?>... exps) {
        final long count = count(condition.where());
        if (count == 0L) {
            return QueryResults.emptyResults();
        }
        final List<String> ids = listIds(page.offset(), page.limit(), condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        final List<T> list = listByIds(ids, clazz, exps); // 再按 id 批量查询
        return new QueryResults<>(list, (long) page.limit(), (long) page.offset(), count);
    }

    /**
     * 测试 SearchId 模板 按条件查询返回 id ，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<String> {@link TabCodeOpenSearchId#getId()} ID集合
     */
    default List<String> listIds(final TabCodeOpenSearchId condition) {
        return listIds(Limit.L1000.value, condition);
    }

    /**
     * 测试 SearchId 模板 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<String> {@link TabCodeOpenSearchId#getId()} ID集合
     */
    default List<String> listIds(final long limit, final TabCodeOpenSearchId condition) {
        return getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板 按条件查询返回 id ，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param offset    {@link Long} 跳过行数
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<String> {@link TabCodeOpenSearchId#getId()} ID集合
     */
    default List<String> listIds(final long offset, final long limit, final TabCodeOpenSearchId condition) {
        return getQueryFactory()
                .select(table.id)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .offset(offset)
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询
     *
     * @param ids {@link String} 数据ID
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default List<TabCodeOpenSearchId> listByIds(final Collection<String> ids) {
        final Map<String, TabCodeOpenSearchId> map = findAllById(Sets.newHashSet(ids)).stream().collect(Collectors.toMap(TabCodeOpenSearchId::getId, TabCodeOpenSearchId::cloneObject));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询 id + 指定字段，返回 list , 返回结果保持同传入的 id 顺序一致
     *
     * @param ids  {@link String} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default List<TabCodeOpenSearchId> listByIds(final Collection<String> ids, final Expression<?>... exps) {
        final Map<String, TabCodeOpenSearchId> map = getQueryFactory()
                .select(Projections.bean(TabCodeOpenSearchId.class, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(TabCodeOpenSearchId::getId, row -> row));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 测试 SearchId 模板 按 ID 批量查询 id + 指定字段，返回 list , 返回结果保持同传入的 id 顺序一致
     *
     * @param ids  {@link String} 数据ID
     * @param exps {@link Expression} 指定多个字段
     * @return List<R> 实体对象集合
     */
    default <R> List<R> listByIds(final Collection<String> ids, final Class<R> clazz, final Expression<?>... exps) {
        final Map<String, R> map = getQueryFactory()
                .select(table.id, Projections.bean(clazz, ObjectArrays.concat(table.id, exps)))
                .from(table)
                .where(table.id.in(Sets.newHashSet(ids)))
                .fetch()
                .stream()
                .collect(Collectors.toMap(tuple -> tuple.get(table.id), tuple -> Objects.requireNonNull(tuple.get(1, clazz))));
        // 保证返回的顺序与传入的顺序一致
        return ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default List<TabCodeOpenSearchId> list(final TabCodeOpenSearchId condition) {
        return list(Limit.L1000.value, condition);
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default List<TabCodeOpenSearchId> list(final long limit, final TabCodeOpenSearchId condition) {
        return getQueryFactory()
                .selectFrom(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default <R> List<R> list(final TabCodeOpenSearchId condition, final Expression<R> exp) {
        return list(Limit.L1000.value, condition, exp);
    }

    /**
     * 测试 SearchId 模板 按条件查询指定字段返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default <R> List<R> list(final long limit, final TabCodeOpenSearchId condition, final Expression<R> exp) {
        return getQueryFactory()
                .select(exp)
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default List<TabCodeOpenSearchId> list(final TabCodeOpenSearchId condition, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, exps);
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param exps      {@link Expression} 指定返回字段
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default List<TabCodeOpenSearchId> list(final long limit, final TabCodeOpenSearchId condition, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(TabCodeOpenSearchId.class, exps))
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default <T extends TabCodeOpenSearchId> List<T> list(final TabCodeOpenSearchId condition, final Class<T> clazz) {
        return list(Limit.L1000.value, condition, clazz, TabCodeOpenSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default <T extends TabCodeOpenSearchId> List<T> list(final long limit, final TabCodeOpenSearchId condition, final Class<T> clazz) {
        return list(limit, condition, clazz, TabCodeOpenSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，投影到指定 VO 类，默认最大返回行数为 1000 ，返回行数过大时请使用分页查询
     *
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default <T extends TabCodeOpenSearchId> List<T> list(final TabCodeOpenSearchId condition, final Class<T> clazz, final Expression<?>... exps) {
        return list(Limit.L1000.value, condition, clazz, exps);
    }

    /**
     * 测试 SearchId 模板 按条件查询返回列表，投影到指定 VO 类，必须指定返回行数，返回行数过大时请使用分页查询
     *
     * @param limit     {@link Long} 指定返回行数
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 实体对象集合
     */
    default <T extends TabCodeOpenSearchId> List<T> list(final long limit, final TabCodeOpenSearchId condition, final Class<T> clazz, final Expression<?>... exps) {
        return getQueryFactory()
                .select(Projections.bean(clazz, exps))
                .from(table)
                .where(condition.where().toArray())
                .orderBy(condition.qdslOrderBy())
                .limit(limit)
                .fetch();
    }

    /**
     * 测试 SearchId 模板 循环查询列表，使用 id 正序排列，用于批处理数据，类似分页查询批量导出。
     * <pre>
     * 注意：
     *   这里必须使用 id 排序(正序|倒序)，否则会导致数据重复查询，也可能漏掉一部分数据。
     * </pre>
     *
     * @param listConsumer List<TabCodeOpenSearchId> 查询结果
     * @param where        QdslWhere 查询条件
     * @param exps         Expression 查询字段
     */
    default void forEach(final Consumer<List<TabCodeOpenSearchId>> listConsumer,
                         final QdslWhere where,
                         final Expression<?>... exps
    ) {
        String id = null;
        final int limit = Limit.L1000.value;
        List<TabCodeOpenSearchId> list;
        final Expression<?>[] columns = Optional.ofNullable(exps)
                .filter(arr -> arr.length > 0)
                .orElseGet(TabCodeOpenSearchId::allColumnAppends);
        do {
            list = getQueryFactory()
                    .select(Projections.bean(TabCodeOpenSearchId.class, columns))
                    .from(table)
                    .where(Optional.ofNullable(id).map(table.id::gt).orElse(null))
                    .where(where.toPredicate())
                    .orderBy(table.id.asc())
                    .limit(limit)
                    .fetch();
            if (!list.isEmpty()) {
                id = Objects.requireNonNull(list.get(list.size() - 1).getId());
                listConsumer.accept(list);
            }
        } while (Objects.equals(list.size(), limit));
    }

    /**
     * 测试 SearchId 模板 按条件查询并跳过指定行数
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 列表查询结果集
     */
    default List<TabCodeOpenSearchId> pull(final int skip, final int limit, final TabCodeOpenSearchId condition) {
        return pull(skip, limit, condition, TabCodeOpenSearchId.class, TabCodeOpenSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板 按条件查询并跳过指定行数，投影到 VO 类
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 列表查询结果集
     */
    default <R extends TabCodeOpenSearchId> List<R> pull(final int skip, final int limit, final TabCodeOpenSearchId condition, final Class<R> clazz) {
        return pull(skip, limit, condition, clazz, TabCodeOpenSearchId.allColumnAppends());
    }

    /**
     * 测试 SearchId 模板 按条件查询并跳过指定行数，查询指定字段，投影到 VO 类
     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
     *
     * @param skip      int 跳过 skip 行数据
     * @param limit     int 获取 limit  行数据
     * @param condition {@link TabCodeOpenSearchId} 查询条件
     * @param clazz     {@link Class} 投影 VO 类
     * @param exps      {@link Expression} 指定返回字段
     * @return List<TabCodeOpenSearchId> {@link TabCodeOpenSearchId} 列表查询结果集
     */
    default <R extends TabCodeOpenSearchId> List<R> pull(final int skip, final int limit, final TabCodeOpenSearchId condition, final Class<R> clazz, final Expression<?>... exps) {
        if (limit < 1) {
            return Collections.emptyList();
        }
        final List<String> ids = listIds(skip, limit, condition); // 先查 id，避免回表，减少分页数据归集时间，大宽表或大量数据的情况下优化比较明显，分页页码越往后优化越明显
        return listByIds(ids, clazz, exps);  // 再按 id 批量查询
    }

}
