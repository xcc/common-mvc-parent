package com.ccx.demo.business.common.controller;

import app.common.starter.entity.Item;
import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.common.starter.util.JSON;
import app.nfs.starter.dto.TempFileDTO;
import app.nfs.starter.dto.TempImageFileDTO;
import app.nfs.starter.service.FileService;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.ccx.demo.business.user.dto.UserAvatarImageDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

@Api(tags = "文件上传")
@ApiSort(5)
@Controller
@RequestMapping("/upload")
@Slf4j
@RequiredArgsConstructor
public class UploadController {
    private final FileService fileService;

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @ApiOperation(value = "", hidden = true)
    @PostMapping("/test")
    @ResponseBody
    public Result<Void> test(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<Void>().call(() -> {
            try {
                log.info("{}", JSON.toJsonString(fileService.write(
                        file,
                        new TempFileDTO(Objects.requireNonNull(file.getOriginalFilename())).buildMd5Uname(file.getInputStream(), "level-1", "level-2")
                )));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String content = "文本内容";
            log.info("{}", JSON.toJsonString(fileService.write(
                    content,
                    new TempFileDTO("text.txt").buildMd5Uname(content)
            )));
            byte[] bytes = "二进制内容".getBytes(UTF_8);
            log.info("{}", JSON.toJsonString(fileService.write(
                    bytes,
                    new TempFileDTO("bytes.txt").buildMd5Uname(bytes)
            )));
            File file1 = new File("/home/ccx/git-repository/code-projects/common-mvc-parent/doc/nginx.conf");
            log.info("{}", JSON.toJsonString(fileService.write(
                    file1,
                    new TempFileDTO("nginx.conf").buildMd5Uname(file1, "level-1", "config")
            )));
            final TempFileDTO resultFileInfo = fileService.writeJson(
                    new Result<Item>()
                            .setSuccess(new Item()
                                    .setKey("item key")
                                    .setValue("1")
                                    .setComment("说明")
                            ),
                    new TempFileDTO("result.json")
            );
            log.info("{}", JSON.toJsonString(resultFileInfo));
            log.info("{}", fileService
                    .readJson(
                            resultFileInfo,
                            new TypeReference<Result<Item>>() {
                            }
                    )
                    .map(JSON::toJsonString)
                    .orElse("文件不存在")
            );

            try {
                Files.list(Paths.get("/home/ccx/git-repository/code-projects/common-mvc-parent/.temp"))
                        .filter(path -> !Objects.equals("app-demo.log", path.toFile().getName()))
                        .forEach(path -> {
                            log.info("{}", JSON.toJsonString(fileService.write(
                                    path.toFile(),
                                    new TempFileDTO(path.toFile().getName()).buildMd5Uname(path.toFile(), ".temp")
                            )));
                        });
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传图片到临时目录，单个上传；上传后的图片会等比缩放和裁剪")
    @ApiOperationSupport(order = 1, author = "谢长春")
    @PostMapping("/temp/image")
    @ResponseBody
    public Result<TempImageFileDTO> uploadTempImage(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<TempImageFileDTO>().execute(result -> {
            if (Objects.isNull(file) || StringUtils.isBlank(file.getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【file】只能是单个文件");
            }
            Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
            Code.A00002.assertHasTrue(Objects.requireNonNull(file.getContentType()).startsWith("image/"), "上传文件必须是图片");
            try {
                final TempImageFileDTO image = new TempImageFileDTO(file.getOriginalFilename()).buildMd5Uname(file.getInputStream());
                final boolean exists = fileService.exists(image);
                if (!exists) {
                    fileService.write(file, image);
                    image.writeMiniAndCropImage(fileService, file.getInputStream());
                }
                result.setSuccess(image);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传到临时目录，单个上传")
    @ApiOperationSupport(order = 1, author = "谢长春")
    @PostMapping("/temp")
    @ResponseBody
    public Result<TempFileDTO> uploadTempFile(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<TempFileDTO>().execute(result -> {
            if (Objects.isNull(file) || StringUtils.isBlank(file.getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【file】只能是单个文件");
            }
            Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
            try {
                final TempFileDTO fileDTO = new TempFileDTO(file.getOriginalFilename()).buildMd5Uname(file.getInputStream());
                result.setSuccess(fileService.write(file, fileDTO));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传到临时目录，批量上传")
    @ApiOperationSupport(order = 2, author = "谢长春")
    @PostMapping("/temps")
    @ResponseBody
    public Result<TempFileDTO> uploadTempFiles(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile[] files) {
        return new Result<TempFileDTO>().execute(result -> {
            if (Objects.isNull(files) || StringUtils.isBlank(files[0].getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【files】为数组");
            }
            result.setSuccess(Stream.of(files)
                    .map(file -> {
                                Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
                                try {
                                    final TempFileDTO fileDTO = new TempFileDTO(Objects.requireNonNull(file.getOriginalFilename()))
                                            .buildMd5Uname(file.getInputStream());
                                    return fileService.write(file, fileDTO);
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                    )
                    .collect(Collectors.toList())
            );
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "上传用户头像"
            , tags = {""}
            , notes = "上传用户头像"
    )
    @ApiOperationSupport(order = 3, author = "谢长春")
    @PostMapping("/user/avatar")
    @ResponseBody
    public Result<UserAvatarImageDTO> uploadUserAvatar(@ApiIgnore @AuthenticationPrincipal final IUser user, @RequestParam(required = false) final MultipartFile file) {
        return new Result<UserAvatarImageDTO>().execute(result -> {
            if (Objects.isNull(file) || StringUtils.isBlank(file.getOriginalFilename())) {
                throw Code.A00009.toCodeException("上传文件为空,字段【file】只能是单个文件");
            }
            Code.A00001.assertNonBlank(file.getOriginalFilename(), "参数【originalFilename】不能为空");
            try {
                final UserAvatarImageDTO image = fileService.write(
                        file,
                        new UserAvatarImageDTO(file.getOriginalFilename())
                                .buildMd5Uname(file.getInputStream())
                );
                result.setSuccess(image);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
