package com.ccx.demo.business.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO：修改密码
 *
 * @author 谢长春 on 2022-02-03
 */
@ApiModel(description = "修改密码")
@Getter
@Setter
@ToString
public class ChangePasswordDTO {
    /**
     * 原密码
     */
    @ApiModelProperty(position = 1, value = "原密码，RSA加密", example = "RSA(passwordOld)")
    private String passwordOld;
    /**
     * 新密码
     */
    @ApiModelProperty(position = 2, value = "新密码，RSA加密", example = "RSA(passwordNew)")
    private String passwordNew;

}
