package com.ccx.demo.business.common.controller;

import lombok.*;

import java.io.Serializable;

/**
 * @Author: guogangqiang
 * @Date: 2022/3/25 17:50
 * @Description: 博世回调参数
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PanoramaPcdLocationCallbackDTO implements Serializable {


    private String msg;


    private String orderId;


    private locations data;

    @Getter
    @Setter
    @ToString
    public static class locations implements Serializable {


        private String[] pcdCoordinate;


        private PanoramaPcdLocationFileInfo[] groupCoordinates;
    }

}
