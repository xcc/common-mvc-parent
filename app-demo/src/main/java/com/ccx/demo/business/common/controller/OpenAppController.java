package com.ccx.demo.business.common.controller;

import app.common.starter.CommonConfiguration;
import app.common.starter.aop.AppEventRequest;
import app.common.starter.entity.Result;
import app.common.starter.vo.AppStaticVO;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.ccx.demo.business.common.service.AppService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.OPTIONS;

/**
 * 应用全局配置
 *
 * @author 谢长春 2022-06-28
 */
@RequiredArgsConstructor
@Controller
public class OpenAppController {

    private final CommonConfiguration commonConfiguration;
    private final AppService appService;

    /**
     * swagger 文档
     *
     * @return {@link String}
     */
    @ApiIgnore
    @AppEventRequest
    @GetMapping(value = "/")
    public String doc() {
        return "redirect:/doc.html";
    }

    /**
     * 用于获取 csrf token
     *
     * @return {@link String}
     */
    @ApiIgnore
    @AppEventRequest
    @RequestMapping(value = "/", method = {HEAD, OPTIONS})
    @ResponseBody
    public String csrf() {
        return "ok";
    }

    /**
     * 查看应用版本号
     *
     * @return {@link Result}
     */
    @AppEventRequest(skip = true)
    @ApiIgnore
    @GetMapping("/version")
    @ResponseBody
    public Result<String[]> version() {
        return new Result<String[]>().execute(result -> {
            result.setSuccess(new String[]{
                    commonConfiguration.getGitCommitId(),
                    commonConfiguration.getBuildTime(),
                    commonConfiguration.getHostIp(),
                    commonConfiguration.getHostName(),
                    commonConfiguration.getVersion(),
                    commonConfiguration.getStartTime()
            });
        });
    }

    /**
     * 应用静态资源
     */
    @AppEventRequest
    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @GetMapping("/open/app/static")
    @ApiOperation(value = "应用静态资源"
            , tags = {""}
            , notes = "h5、json静态资源"
            + "<br>应用每次打开都要通过这个接口检查静态资源版本号"
            + "<br>这个文件内容应该放到nginx下，现在放在这里的目的是可以通过接口文档查看"
    )
    @ApiOperationSupport(author = "谢长春")
    @ResponseBody
    public Result<AppStaticVO> appStatic() {
        return new Result<AppStaticVO>().execute(result -> {
            result.setSuccess(appService.getList());
        });
//    public RedirectView appStatic() {
//        final RedirectView redirectView = new RedirectView(appService.getAppStaticJsonFileDTO().getUrl());
//        //redirectView.setStatusCode(HttpStatus.MOVED_PERMANENTLY); // 301
//        redirectView.setStatusCode(HttpStatus.FOUND); // 302
//        return redirectView;
    }
}
