package com.ccx.demo.business.user.controller;

import app.common.starter.entity.OrderBy;
import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.common.starter.util.JSON;
import app.common.starter.util.Rsa;
import app.encrypt.domain.starter.annotations.AesPathVariable;
import app.encrypt.domain.starter.annotations.AesRequestParam;
import app.encrypt.domain.starter.model.DataVersion;
import app.enums.starter.TokenClient;
import app.security.starter.SecurityConfiguration;
import app.security.starter.cache.TokenCache;
import app.security.starter.enums.Role;
import app.security.starter.vo.IUser;
import app.security.starter.vo.LoginUserVO;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.ccx.demo.business.user.cache.TabRoleCache;
import com.ccx.demo.business.user.dto.ChangePasswordDTO;
import com.ccx.demo.business.user.dto.TabUserInsertDTO;
import com.ccx.demo.business.user.dto.TabUserUpdateDTO;
import com.ccx.demo.business.user.dto.UserAvatarImageDTO;
import com.ccx.demo.business.user.entity.TabRole;
import com.ccx.demo.business.user.service.UserService;
import com.ccx.demo.business.user.vo.TabUserVO;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.google.common.collect.Sets;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 对外接口:用户
 *
 * @author 谢长春 on 2017/11/13.
 */
@Api(tags = "用户")
@ApiSort(4) // 控制接口排序
@RequestMapping("/user")
@Controller
@Slf4j
@RequiredArgsConstructor
public class UserController {
    private final UserService service;
    private final TokenCache tokenCache;
    private final SecurityConfiguration securityConfiguration;
    private final TabRoleCache tabRoleCache;

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "获取当前登录用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 1, author = "谢长春")
    @GetMapping("/current")
    @ResponseBody
    public Result<TabUserVO> current(@ApiIgnore @AuthenticationPrincipal final LoginUserVO user) {
        return new Result<TabUserVO>().execute(result -> {
            tokenCache.expireDelay(user.getClient(), user.getId()); // token 延期
            final List<TabRole> tabRoles = tabRoleCache.listByIds(Sets.newHashSet(user.getRoles())).stream()
                    .filter(row -> Objects.equals(false, row.getDeleted()))
                    .collect(Collectors.toList());
            result.setSuccess(service.findById(user.getId())
                    .map(row -> {
                        final TabUserVO currentUser = new TabUserVO();
                        BeanUtils.copyProperties(row, currentUser);
                        if (!tabRoles.isEmpty()) {
                            currentUser.setAuthorityList(tabRoles.stream()
                                    .map(TabRole::getAuthorities)
                                    .filter(Objects::nonNull)
                                    .flatMap(Arrays::stream)
                                    .distinct()
                                    .collect(Collectors.toList())
                            );
                            currentUser.setRoleNames(tabRoles.stream()
                                    .map(TabRole::getName)
                                    .filter(StringUtils::isNoneBlank)
                                    .collect(Collectors.toList()));
                        }
                        return currentUser;
                    })
                    .orElseThrow(() -> Code.A00001.toCodeException("用户详细信息加载失败"))
            );
        });
    }

    @AppSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "切换登录客户端，获取新 token"
            , tags = {""}
            , notes = "登录状态下在各端切换"
    )
    @GetMapping("/switch/{client}")
    @ApiOperationSupport(order = 20, author = "谢长春")
    @ResponseBody
    public Result<LoginUserVO> switchClient(
            @ApiIgnore @AuthenticationPrincipal final LoginUserVO user,
            @ApiParam(required = true, value = "目标客户端，参考：TokenClient", example = "OM_PC") @PathVariable final TokenClient client,
            HttpServletResponse response
    ) {
        return new Result<LoginUserVO>().execute(result -> {
            final List<String> authorityList = user.getAuthorityList();
            if (authorityList.contains(Role.ROLE_ADMIN.name())) {
                // 硬编码权限可以登录
            } else if (!authorityList.contains(client.name())) {
                log.error("{}=>{}", client, JSON.toJsonString(authorityList));
                throw Code.A00010.toCodeException(user.getUsername());
            }
            final List<TabRole> tabRoles = tabRoleCache.listByIds(Sets.newHashSet(user.getRoles())).stream()
                    .filter(row -> Objects.equals(false, row.getDeleted()))
                    .collect(Collectors.toList());
            if (!tabRoles.isEmpty()) {
                user.setAuthorityList(tabRoles.stream()
                        .map(TabRole::getAuthorities)
                        .filter(Objects::nonNull)
                        .flatMap(Arrays::stream)
                        .distinct()
                        .collect(Collectors.toList())
                );
                user.setRoleNames(tabRoles.stream()
                        .map(TabRole::getName)
                        .filter(StringUtils::isNoneBlank)
                        .collect(Collectors.toList()));
            }
            // 生成 token 放在响应头
            final String token = user.token(client);
            response.setHeader(SecurityConfiguration.HEADER_X_TOKEN, token);
            result.setSuccess(user).addExtras(SecurityConfiguration.HEADER_X_TOKEN, token);
        });
    }

    @VueSwaggerGroup
    @GetMapping("/page/{number}/{limit}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_load')")
    @ApiOperation(value = "分页查询用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 2, author = "谢长春")
    @ResponseBody
    public Result<TabUserVO> pageVO(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数", example = "10") @PathVariable final int limit,
            @ApiParam(value = "模糊匹配：用户名/昵称/手机号/邮箱；示例：187888888888") @RequestParam(required = false) final String containsAll,
            @ApiParam(value = "昵称；示例：老王") @RequestParam(required = false) final String nickname,
            @ApiParam(value = "登录名；示例：admin") @RequestParam(required = false) final String username,
            @ApiParam(value = "手机号；示例：187888888888") @RequestParam(required = false) final String phone,
            @ApiParam(value = "邮箱；示例：xx@mail.com") @RequestParam(required = false) final String email,
            @ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            @ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            @ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<TabUserVO>().execute(result -> {
            final TabUserVO condition = new TabUserVO();
            condition.setContainsAll(containsAll);
            condition.setNickname(nickname);
            condition.setUsername(username);
            condition.setEmail(email);
            condition.setPhone(phone);
            condition.setCreateUserId(createUserId);
            condition.setUpdateUserId(updateUserId);
            condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.pageVO(
                    condition
                    , Page.builder().number(number).limit(limit).build()
            ));
        });
    }

    @VueSwaggerGroup
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_load')")
    @ApiOperation(value = "按 id 查询用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 3, author = "谢长春")
    @ResponseBody
    public Result<TabUserVO> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id", type = "string") @AesPathVariable final Long id
    ) {
        return new Result<TabUserVO>().execute(result -> {
            result.setSuccess(TabUserVO.ofTabUser(service.findById(id).orElse(null)));
        });
    }

    @VueSwaggerGroup
    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_insert')")
    @ApiOperation(value = "新增用户"
            , tags = {""}
            , notes = ""
    )
    @ApiImplicitParam(name = "body", dataType = "TabUserInsertDTO", dataTypeClass = TabUserInsertDTO.class, required = true)
    @ApiOperationSupport(
            order = 4,
            author = "谢长春",
            includeParameters = {
                    "tabUserInsertDTO.username", "tabUserInsertDTO.password", "tabUserInsertDTO.nickname", "tabUserInsertDTO.phone", "tabUserInsertDTO.email", "tabUserInsertDTO.avatar", "tabUserUpdateDTO.roleIds",
            }
    )
    @ResponseBody
    public Result<Void> insert(@ApiIgnore @AuthenticationPrincipal final IUser user,
                               @RequestBody final TabUserInsertDTO body) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertHasTrue(Objects.nonNull(body.getPhone()), "注册手机号码不能为空");
            if (securityConfiguration.isEnabled()) {
                // rsa 加密传输密码， 需要使用私钥解密
                body.setPassword(
                        Rsa.decryptByPrivateKey(
                                body.getPassword(),
                                securityConfiguration.getPrivateKey()
                        )
                );
            }
            service.insert(body, user.getId());
        });
    }

    @VueSwaggerGroup
    @PutMapping("/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_update')")
    @ApiOperation(value = "修改用户"
            , tags = {""}
            , notes = ""
    )
    @ApiImplicitParam(name = "body", dataType = "TabUserUpdateDTO", dataTypeClass = TabUserUpdateDTO.class, required = true)
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            includeParameters = {
                    "tabUserUpdateDTO.nickname", "tabUserUpdateDTO.phone", "tabUserUpdateDTO.email", "tabUserUpdateDTO.avatar", "tabUserUpdateDTO.roleIds",
            }
    )
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv,
            @RequestBody final TabUserUpdateDTO body) {
        return new Result<Void>().call(() -> {
            body.setUpdateTime(dv.getUpdateTime());
            service.update(dv.getLongId(), user.getId(), body);
        });
    }

    @VueSwaggerGroup
    @PatchMapping("/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_delete')")
    @ApiOperation(value = "逻辑删除用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 6, author = "谢长春")
    @ResponseBody
    public Result<Void> markDeleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
        return new Result<Void>().call(() -> {
            service.markDeleteById(dv.getLongId(), dv.getUpdateTime(), user.getId());
        });
    }

    @VueSwaggerGroup
    @PatchMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_delete')")
    @ApiOperation(value = "批量逻辑删除用户"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 7,
            author = "谢长春",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDelete(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDelete(
                    body.stream().map(DataVersion::getLongId).collect(Collectors.toSet())
                    , body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @PatchMapping("/nickname")
    @ApiOperation(value = "修改昵称"
            , tags = {""}
            , notes = "请求体（body）直接给新昵称，不需要使用对象包在外层"
    )
    @ApiOperationSupport(order = 8, author = "谢长春")
    @ResponseBody
    public Result<Void> updateNickname(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final String nickname) {
        return new Result<Void>().call(() -> service.updateNickname(
                user.getId(),
//                body.getString("nickname"),
                nickname,
                user.getId()
        ));
    }

    @VueSwaggerGroup
    @PatchMapping("/reset-password/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_reset_password')")
    @ApiOperation(value = "重置密码，返回新密码"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 9, author = "谢长春")
    @ResponseBody
    public Result<String> resetPassword(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
        return new Result<String>().execute(result -> {
//            String newPassword = service.resetPassword(decrypt.getLongId(), user.getId());
//            if (appConfig.getEncrypt().getPwd().isEnabled()) {
//                // rsa 加密传输密码， 需要使用公钥解密
//                newPassword = Rsa.encryptByPrivateKey(newPassword, appConfig.getEncrypt().getPwd().getPrivateKey());
//            }
            result.setSuccess(service.resetPassword(dv.getLongId(), user.getId()));
        });
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @PatchMapping("/change-password")
    @ApiOperation(value = "修改密码", tags = {""}, notes = "密码需要使用 RSA 加密")
    @ApiOperationSupport(order = 10, author = "谢长春")
    @ResponseBody
    public Result<Void> changePassword(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody(required = false) final ChangePasswordDTO body
    ) {
        return new Result<Void>().call(() -> service.changePassword(
                user.getId(),
                body.getPasswordOld(),
                body.getPasswordNew()
        ));
    }

    @AppSwaggerGroup
    @VueSwaggerGroup
    @WechatSwaggerGroup
    @PatchMapping("/change-avatar")
    @ApiOperation(value = "修改头像"
            , tags = {""}
            , notes = "需要调用文件上传接口获得文件信息"
    )
    @ApiOperationSupport(order = 11, author = "谢长春")
    @ResponseBody
    public Result<Void> changeAvatar(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody(required = false) final UserAvatarImageDTO body
    ) {
        return new Result<Void>().call(() -> service.changeAvatar(user.getId(), body));
    }

    @VueSwaggerGroup
    @PatchMapping("/enable/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_enable')")
    @ApiOperation(value = "启用"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 12, author = "谢长春")
    @ResponseBody
    public Result<Void> enable(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
        return new Result<Void>().call(() -> {
            service.updateDisabled(dv.getLongId(), false, user.getId());
        });
    }

    @VueSwaggerGroup
    @PatchMapping("/disable/{dv}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'user_list_disable')")
    @ApiOperation(value = "禁用"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 13, author = "谢长春")
    @ResponseBody
    public Result<Void> disable(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv) {
        return new Result<Void>().call(() -> {
            service.updateDisabled(dv.getLongId(), true, user.getId());
        });
    }

    @AppSwaggerGroup
    @WechatSwaggerGroup
    @ApiOperation(value = "账号销毁"
            , tags = {""}
            , notes = "账号销毁之后可以用手机号重新注册帐号"
            + "<br>"
    )
    @PostMapping("/clear")
    @ApiOperationSupport(order = 29, author = "谢长春")
    @ResponseBody
    public Result<Void> clearUser(@ApiIgnore @AuthenticationPrincipal final IUser user) {
        return new Result<Void>().call(() -> {
            service.clear(
                    user.getPhone()
                    , Collections.singletonList((u) -> log.debug("执行账号销毁关联的其他逻辑"))
            );
        });
    }
//    @AppSwaggerGroup
//    @ApiOperation(value = "移动应用微信第三方登录集成：账号绑定"
//            , tags = {""}
//            , notes = "app 调用微信 sdk 获取 code 之后，通过这个接口与当前登录账号绑定"
//            + "<br> 移动应用微信登录开发指南： https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_Login/Development_Guide.html"
//            + "<br> iOS接入指南： https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Access_Guide/iOS.html"
//            + "<br> Android接入指南： https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Access_Guide/Android.html"
//            + "<br> 网站应用微信登录开发指南： https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Wechat_Login.html"
//            + "<br> 1、 微信认证接口"
//            + "<br> 2、 openId+unionId 查询绑定记录"
//            + "<br> 3、 检查微信账号是否有绑定记录，有绑定记录则抛出异常"
//            + "<br> 4、 无绑定记录则执行绑定逻辑"
//    )
//    @PostMapping("/app/wechat-bind/{wxCode}")
//    @ApiOperationSupport(order = 21, author = "谢长春")
//    @ResponseBody
//    public Result<Void> wechatBind(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "微信code", example = "0000") @PathVariable final String wxCode
//    ) {
//        return new Result<Void>().call(() -> {
//            final AppWechatUserInfoVO appWechatUserInfoVO = wechatService.oauth2(wxCode);
////            final AppWechatUserInfoVO appWechatUserInfoVO = JSON.parseObject("{\"appId\":\"wxe375\",\"city\":\"\",\"country\":\"\",\"headimgurl\":\"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j/132\",\"nickname\":\"小\",\"openid\":\"o_yig6g\",\"privilege\":\"[]\",\"province\":\"\",\"sex\":0,\"unionid\":\"om-7c6gG4qCUUX4DTx7zGKvePcJQ\"}",AppWechatUserInfoVO.class);
//
//            final Optional<TabUserWechat> userWechatOptional = Optional.ofNullable(userWechatService
//                    // 查询微信返回的 openId 是否已经绑定过用户，已经绑定。 生成 token 返回登录成功
//                    .findById(appWechatUserInfoVO.buildUserWechatId()) // md5(appId+openId)
//                    .orElseGet(() -> userWechatService
//                            // 查询微信返回的 unionId 是否已经绑定过用户，已经绑定。 生成 token 返回登录成功
//                            .findByUnionId(appWechatUserInfoVO.getUnionid())
//                            .orElse(null)
//                    )
//            );
//            if (userWechatOptional.isPresent()) {
//                Code.B00006.assertEquals(user.getId(), userWechatOptional.get().getUserId(), user.getPhone());
//            } else { // 写入微信账号绑定信息
//                final TabUserWechatInsertDTO dto = new TabUserWechatInsertDTO();
//                dto.setAppId(appWechatUserInfoVO.getAppId());
//                dto.setOpenId(appWechatUserInfoVO.getOpenid());
//                dto.setUnionId(appWechatUserInfoVO.getUnionid());
//                dto.setUserId(user.getId());
//                // dto.setSessionKey(appWechatUserInfoVO.get);
//                // dto.setRefreshToken(appWechatUserInfoVO.get);
//                userWechatService.insert(dto);
//            }
//        });
//    }
//
//    @AppSwaggerGroup
//    @ApiOperation(value = "获取微信小程序分享二维码"
//            , tags = {""}
//            , notes = "分享微信二维码，邀请用户。每个用户生成的二维码不一样"
//    )
//    @PostMapping("/wechat-invite-qrcode-url")
//    @ApiOperationSupport(order = 22, author = "谢长春")
//    @ResponseBody
//    public Result<String> getWechatInviteQrcodeUrl(@ApiIgnore @AuthenticationPrincipal final IUser user) {
//        //
//        //  用户邀请小程序二维码逻辑：
//        //  + 检查用户是否已经生成过二维码
//        //  ++ 已生成，直接返回二维码 url
//        //  ++ 未生成：
//        //  +++ 基于用户手机号加密获得32位 scene
//        //  +++ 调用微信小程序二维码生成接口，获得二维码图片buffer
//        //  +++ 将buffer写入文件，命名格式：{scene}.jpeg
//        //  +++ 返回二维码url
//        //
//        return new Result<String>().execute((result) -> {
//            final String scene = service.encryptWechatScene(user.getPhone(), user.getDoctorId());
//            WechatQrcodeFileInfo fileDTO = new WechatQrcodeFileInfo();
//            fileDTO.setName("邀请码.jpeg");
//            fileDTO.setUname(String.format("%s.jpeg", scene));
//            final boolean exists = fileService.exists(fileDTO);
//            if (!exists) {
//                final WechatMiniAppQrcodeDTO dto = new WechatMiniAppQrcodeDTO()
//                        .setRequiredScene(scene)
//                        .setRequiredPage("pages/index");
//                if (!appConfig.isProd()) {
//                    dto.setCheckPath(false)
//                            // 正式版为 release，体验版为 trial，开发版为 develop
//                            .setEnvVersion("trial");
//                }
//                try (final InputStream inputStream = wechatMiniAppService.getMiniAppQrcode(dto)) {
////                    ByteStreams.copy(inputStream, Files.newOutputStream(Paths.get(String.format("/tmp/%s", fileDTO.getUname()))));
//                    fileService.write(inputStream, fileDTO, true);
//                } catch (Exception e) {
//                    throw new RuntimeException(e);
//                }
//            }
//            result.setSuccess(fileDTO.getUrl());
//        });
//    }
}
