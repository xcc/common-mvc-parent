package com.ccx.demo.business.code.dto;

import com.ccx.demo.business.code.entity.TabCodeAuthId;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO：测试 AuthId 模板修改 V20221105
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义测试 AuthId 模板修改时的扩展属性
 *
 * @author 谢长春 on 2022-11-02
 */
@Getter
@Setter
@ToString(callSuper = true)
@ApiModel(description = "测试 AuthId 模板")
public class TabCodeAuthIdUpdateDTO extends TabCodeAuthId {
    private static final long serialVersionUID = 1L;
}
