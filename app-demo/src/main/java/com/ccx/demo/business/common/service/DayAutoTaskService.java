package com.ccx.demo.business.common.service;

import app.auto.task.starter.interfaces.IAutoTask;
import app.common.starter.interfaces.ICache;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * 定时任务，每天凌晨执行一次
 *
 * @author 谢长春 on 2020-03-08
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
@ConditionalOnProperty(value = "spring.app.autoTask.services.dayAutoTaskService.enabled", havingValue = "true")
public class DayAutoTaskService implements IAutoTask {

    private final ApplicationContext applicationContext;

    public void call(final String args) {
        applicationContext.getBeansOfType(ICache.ILocalCache.class).forEach((name, service) -> {
            try {
                log.info("开始：{}", name);
                service.refreshCache();
                log.info("结束：{}", name);
            } catch (Exception e) {
                log.error("异常：{}", name);
            }
        });
//        try {
//            log.info("开始：清理 30 天前的临时文件夹");
//            log.info("成功：清理 30 天前的临时文件夹");
//        } catch (Exception e) {
//            log.info("异常：清理 30 天前的临时文件夹");
//        }
    }
}
