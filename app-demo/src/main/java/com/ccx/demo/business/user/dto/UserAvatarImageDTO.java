package com.ccx.demo.business.user.dto;

import app.nfs.starter.NfsConfiguration;
import app.nfs.starter.dto.ImageFileDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static io.swagger.annotations.ApiModelProperty.AccessMode.READ_ONLY;

/**
 * 用户头像文件对象
 *
 * @author 谢长春 on 2022-05-05
 */
@Component
@ToString(callSuper = true)
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "用户头像文件对象")
public class UserAvatarImageDTO extends ImageFileDTO {

    private static final long serialVersionUID = -6779893372268172821L;
    public static final String DIR_NAME = UserAvatarImageDTO.class.getSimpleName();

    public UserAvatarImageDTO(final String uname) {
        super(uname);
    }

    public UserAvatarImageDTO(final String name, final String uname) {
        super(name, uname);
    }

//    @JsonIgnore
//    @ApiModelProperty(hidden = true)
//    private final String className = UserAvatarImageDTO.class.getSimpleName();

    @ApiModelProperty(value = "缩略图最大宽度，取长边等比压缩，单位：px", example = "200", accessMode = READ_ONLY, hidden = true)
    public Integer getMiniMaxWidth() {
        return 100;
    }

    @ApiModelProperty(value = "缩略图最大高度，取长边等比压缩，单位：px", example = "200", accessMode = READ_ONLY, hidden = true)
    public Integer getMiniMaxHeight() {
        return 100;
    }

    @ApiModelProperty(value = "裁减方图宽度，单位：px", example = "80", accessMode = READ_ONLY, hidden = true)
    public Integer getCropWidth() {
        return 80;
    }

    @ApiModelProperty(value = "裁减方图高度，单位：px", example = "80", accessMode = READ_ONLY, hidden = true)
    public Integer getCropHeight() {
        return 80;
    }


    @Override
    public String getAbsolutePath() {
        return StringUtils.isEmpty(uname) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, uname);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getMiniAbsolutePath() {
        return StringUtils.isEmpty(mini) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, mini);
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    public String getCropAbsolutePath() {
        return StringUtils.isEmpty(crop) ? null : NfsConfiguration.getFileService().appendPath(DIR_NAME, crop);
    }

    @JsonIgnore
    @Override
    public String getUrl() {
        // 头像在前端裁剪，所以 mini 图 和 crop 图跟原图一样
        return StringUtils.isEmpty(uname) ? getUrl() : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, uname);
    }

    @JsonIgnore
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getMiniUrl() {
        // 头像在前端裁剪，所以 mini 图 和 crop 图跟原图一样
        return StringUtils.isEmpty(uname) ? getUrl() : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, uname);
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getCropUrl() {
        // 头像在前端裁剪，所以 mini 图 和 crop 图跟原图一样
        return StringUtils.isEmpty(uname) ? getUrl() : NfsConfiguration.getFileService().appendUrl(isPrivateFile(), DIR_NAME, uname);
    }

    @SneakyThrows
    public UserAvatarImageDTO cloneObject() {
        return (UserAvatarImageDTO) super.clone();
    }
}
