package com.ccx.demo.business.common.controller;

import lombok.*;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PanoramaPcdLocationFileInfo implements Serializable {
    private static final long serialVersionUID = -6779893372268172821L;

    private String fileName;

    private String[] coordinates;
}
