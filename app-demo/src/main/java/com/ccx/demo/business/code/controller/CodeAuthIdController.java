package com.ccx.demo.business.code.controller;

import app.common.starter.entity.OrderBy;
import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.common.starter.enums.Code;
import app.encrypt.domain.starter.annotations.AesPathVariable;
import app.encrypt.domain.starter.annotations.AesRequestParam;
import app.encrypt.domain.starter.model.DataVersion;
import app.security.starter.vo.IUser;
import com.ccx.demo.business.code.dto.TabCodeAuthIdInsertDTO;
import com.ccx.demo.business.code.dto.TabCodeAuthIdUpdateDTO;
import com.ccx.demo.business.code.entity.TabCodeAuthId;
import com.ccx.demo.business.code.service.CodeAuthIdService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 对外接口：测试 AuthId 模板 V20221105
 *
 * @author 谢长春 on 2022-11-02
 */
@Api(tags = "测试 AuthId 模板")
//@ApiSort() // 控制接口排序
@RequestMapping("/code-auth-id")
@Controller
@Slf4j
@RequiredArgsConstructor
public class CodeAuthIdController {

    private final CodeAuthIdService service;

    @GetMapping("/page/{number}/{limit}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "分页查询测试 AuthId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 1, author = "谢长春")
    @ResponseBody
    public Result<TabCodeAuthId> page(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "页码", example = "1") @PathVariable final int number,
            @ApiParam(required = true, value = "每页条数，最大值100", example = "10") @PathVariable final int limit,
            //@ApiParam(value = "名称；示例：AaBb1111", type = "string") @RequestParam(required = false) final String name,
            @ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
            @ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
            @ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
    ) {
        return new Result<TabCodeAuthId>().execute(result -> { // Controller 方法逻辑写在这里
            final TabCodeAuthId condition = new TabCodeAuthId();
            //condition.setName(name);
            condition.setCreateUserId(createUserId);
            condition.setUpdateUserId(updateUserId);
            condition.setDeleted(deleted);
            condition.setOrderBy(orderBy);
            result.setSuccess(service.page(condition, Page.of(number, limit)));
        });
    }

//    @GetMapping("/pull/{skip}/{limit}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "测试 AuthId 模板瀑布流加载，按条件查询并跳过指定行数"
//            , tags = {""}
//            , notes = "瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）"
//            + "<br>注意：该方法不会返回总数据行数，前端应该判断 rowCount != limit 时，表示瀑布流已经滚动到底部"
//    )
//    @ApiOperationSupport(order = 2, author = "谢长春")
//    @ResponseBody
//    public Result<TabCodeAuthId> pull(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "跳过数据行数，示例：0", example = "1") @PathVariable final int skip,
//            @ApiParam(required = true, value = "每次获取数据行数，最大值100", example = "100") @PathVariable final int limit,
//                        //@ApiParam(value = "名称；示例：AaBb1111", type = "string") @RequestParam(required = false) final String name,
//            @ApiParam(value = "新增操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long createUserId,
//            @ApiParam(value = "更新操作人id；示例：MNB08Jz8", type = "string") @AesRequestParam(required = false) final Long updateUserId,
//            @ApiParam(value = "是否逻辑删除；示例：false") @RequestParam(required = false) final Boolean deleted,
//            @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
//    ) {
//        return new Result<TabCodeAuthId>().execute(result -> { // Controller 方法逻辑写在这里
//             final TabCodeAuthId condition = new TabCodeAuthId();
//                        //condition.setName(name);
//            condition.setCreateUserId(createUserId);
//            condition.setUpdateUserId(updateUserId);
//            condition.setDeleted(deleted);
//            result.setSuccess(service.pull(skip, limit, condition));
//        });
//    }
//

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    @GetMapping
//    // @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
//    @ApiOperation(value = "列表查询测试 AuthId 模板"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 3,
//            author = "谢长春"
//    )
//    @ResponseBody
//    public Result<TabCodeAuthId> list(
//        @ApiIgnore @AuthenticationPrincipal final IUser user
//        @ApiParam(value = "排序；示例：?orderBy=[{\"name\":\"id\",\"direction\":\"ASC\"}]") @RequestParam(required = false) final List<OrderBy> orderBy
//    ) {
//        return new Result<TabCodeAuthId>().execute(result -> { // Controller 方法逻辑写在这里
//            final TabCodeAuthId condition = new TabCodeAuthId();
//            condition.setOrderBy(orderBy);
//            result.setSuccess(service.list(condition));
//        });
//    }
//

    @GetMapping("/{id}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_load')")
    @ApiOperation(value = "按 id 查询测试 AuthId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 4, author = "谢长春")
    @ResponseBody
    public Result<TabCodeAuthId> findById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据id", example = "id", type = "string") @AesPathVariable(clazz = String.class) final String id
    ) {
        return new Result<TabCodeAuthId>().execute(result -> { // Controller 方法逻辑写在这里
            result.setSuccess(service.findById(id).orElse(null));
        });
    }

    @PostMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "新增测试 AuthId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 5,
            author = "谢长春",
            includeParameters = {
                    "body.name"
            })
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final TabCodeAuthIdInsertDTO body
    ) {
        //return new Result<TabCodeAuthId>().execute(result -> { // Controller 方法逻辑写在这里
        //    result.setSuccess(service.insert(body, user.getId()));
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            service.insert(body, user.getId());
        });
    }

    @PutMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_update')")
    @ApiOperation(value = "修改测试 AuthId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 6,
            author = "谢长春",
            ignoreParameters = {
                    "body.name"
            })
    @ResponseBody
    public Result<Void> update(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv,
            @RequestBody final TabCodeAuthIdUpdateDTO body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            body.setUpdateTime(dv.getUpdateTime());
            service.update(dv.getStringId(), user.getId(), body);
        });
    }

    @PatchMapping("/{dv}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除测试 AuthId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(order = 7, author = "谢长春")
    @ResponseBody
    public Result<Void> markDeleteById(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv
    ) {
        return new Result<Void>().call(() -> {
            service.markDeleteById(dv.getStringId(), user.getId());
        });
    }

    @PatchMapping
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
    @ApiOperation(value = "逻辑删除测试 AuthId 模板"
            , tags = {""}
            , notes = ""
    )
    @ApiOperationSupport(
            order = 8,
            author = "谢长春",
            params = @DynamicParameters(name = "DvArray", properties = {
                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
            })
    )
    @ResponseBody
    public Result<Void> markDeleteByIds(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @RequestBody final Set<DataVersion> body
    ) {
        return new Result<Void>().call(() -> {
            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
            service.markDeleteByIds(
                    body.stream().map(DataVersion::getStringId).collect(Collectors.toSet())
                    , body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
                    , user.getId()
            );
        });
    }

//    @DeleteMapping("/{dv}")
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除测试 AuthId 模板"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(order = 9, author = "谢长春")
//    @ResponseBody
//    public Result<Void> deleteById(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @ApiParam(required = true, value = "数据版本号", example = "dv", type = "string") @AesPathVariable(clazz = DataVersion.class) final DataVersion dv
//    ) {
//        return new Result<Void>().call(() -> {
//            service.deleteById(dv.getStringId(), user.getId());
//        });
//    }
//
//    @DeleteMapping
//    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_delete')")
//    @ApiOperation(value = "物理删除测试 AuthId 模板"
//      , tags = {""}
//      , notes = ""
//    )
//    @ApiOperationSupport(
//            order = 10,
//            author = "谢长春",
//            params = @DynamicParameters(name = "DvArray", properties = {
//                    @DynamicParameter(name = "DvArray", value = "数据版本号", example = "[dv,dv]", required = true, dataTypeClass = String[].class)
//            })
//    )
//    @ResponseBody
//    public Result<Void> deleteByIds(
//            @ApiIgnore @AuthenticationPrincipal final IUser user,
//            @RequestBody final Set<DataVersion> body
//    ) {
//        return new Result<Void>().call(() -> {
//            Code.A00002.assertNonEmpty(body, "请至少选择一条数据");
//            service.deleteByIds(
//                    body.stream().map(DataVersion::getStringId).collect(Collectors.toSet())
//                    , body.stream().map(DataVersion::getUpdateTime).collect(Collectors.toSet())
//                    , user.getId()
//            );
//        });
//    }
//
}
