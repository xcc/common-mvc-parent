package com.ccx.demo.business.common.controller;

import app.common.starter.entity.Result;
import app.security.starter.vo.IUser;
import app.swagger.starter.annotations.AppSwaggerGroup;
import app.swagger.starter.annotations.VueSwaggerGroup;
import app.swagger.starter.annotations.WechatSwaggerGroup;
import com.ccx.demo.business.common.dto.TabErrorLogInsertDTO;
import com.ccx.demo.business.common.service.ErrorLogService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.google.common.base.Strings;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 异常信息上报
 *
 * @author 谢长春 on 2022-03-01
 */
@Api(tags = "异常信息上报", hidden = true)
@RequestMapping("/open/error-log")
@Controller
@Slf4j
@RequiredArgsConstructor
public class OpenErrorLogController {
    private final ErrorLogService service;

    @VueSwaggerGroup
    @AppSwaggerGroup
    @WechatSwaggerGroup
    @PostMapping("/{traceId}")
    //@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', '{}_insert')")
    @ApiOperation(value = "异常信息上报"
            , tags = {""}
            , notes = "用于记录前端异常，body数据格式为 json 对象"
            + "<br>1、响应头有 X-Trace-Id 一定要保存到后端，如果没有 X-Trace-Id 则自己生成一个唯一的 id"
            + "<br>2.1、拦截接口响应状态（注意要设置规则跳过异常上报接口），非 200 状态都上报到后台，方便收集异常日志。 "
            + "<br>2.2、http响应状态 = 200，判断 code != A00000 的接口都上报到这个接口"
            + "<br>3.上报内容基础参数参考，也可以随意加自定参数：{\"client\": \"IOS、Android\",\"version\": \"应用版本\",\"url\": \"请求地址\", \"method\": \"请求方式【GET、POST、PUT、PATCH、DELETE】\", \"status\": \"http 响应状态\", \"reqParams\": \"http get请求查询参数\", \"reqData\": \"http post请求参数\", \"resData\": \"http 响应数据包\"}"
    )
    @ApiOperationSupport(author = "谢长春")
    @ResponseBody
    public Result<Void> insert(
            @ApiIgnore @AuthenticationPrincipal final IUser user,
            @ApiParam(required = true, value = "链路追踪ID，获取不到的情况下前端可以用uuid替代", example = "X-Trace-Id") @PathVariable final String traceId,
            @RequestBody(required = false) final String body
    ) {
        return new Result<Void>().call(() -> { // Controller 方法逻辑写在这里
            try {
                if (Strings.isNullOrEmpty(body)) {
                    return;
                }
                final TabErrorLogInsertDTO dto = new TabErrorLogInsertDTO();
                dto.setTraceId(traceId);
                dto.setMessage(body);
                service.insert(dto, user.optionalUserId().orElse(0L));
            } catch (Exception e) {
                // 避免前端死循环，异常日志保存失败必须永远返回成功
                log.error(e.getMessage(), e);
            }
        });
    }

}
