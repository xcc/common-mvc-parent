package com.ccx.demo.business.code.service;

import app.common.starter.entity.Page;
import app.common.starter.exception.DeleteRowsException;
import app.common.starter.exception.UpdateRowsException;
import com.ccx.demo.business.code.dao.jpa.CodeAuthIdRepository;
import com.ccx.demo.business.code.dto.TabCodeAuthIdInsertDTO;
import com.ccx.demo.business.code.dto.TabCodeAuthIdUpdateDTO;
import com.ccx.demo.business.code.entity.TabCodeAuthId;
import com.ccx.demo.business.user.entity.TabUser;
import com.ccx.demo.business.user.service.UserService;
import com.google.common.base.Strings;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 服务接口实现类：测试 AuthId 模板 V20221105
 *
 * @author 谢长春 on 2022-11-02
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class CodeAuthIdService
//      , ITabCodeAuthIdCache
{
    private final CodeAuthIdRepository repository;
    //private final TabCodeAuthIdCache tabCodeAuthIdCache;
    private final UserService userService;

    /**
     * 新增 测试 AuthId 模板
     *
     * @param dto    {@link TabCodeAuthIdInsertDTO} 实体对象
     * @param userId {@link Long} 操作用户ID
     * @return {@link TabCodeAuthId} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") TabCodeAuthId insert(
            @Valid @NotNull(message = "【dto】不能为null") final TabCodeAuthIdInsertDTO dto
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        final TabCodeAuthId obj = new TabCodeAuthId();
        BeanUtils.copyProperties(dto, obj);
        return repository.insert(userId, obj);
    }
//    /**
//     * 批量新增 测试 AuthId 模板
//     *
//     * @param list   List<TabCodeAuthIdInsertDTO> {@link TabCodeAuthIdInsertDTO}  实体对象集合
//     * @param userId {@link Long} 操作用户ID
//     * @return List<TabCodeAuthId> 实体对象集合
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public @NotNull(message = "返回值不能为null") List<TabCodeAuthId> insert(
//            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull TabCodeAuthIdInsertDTO> list
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        return repository.insert(userId, list.stream()
//            .map(dto -> {
//                final TabCodeAuthId obj = new TabCodeAuthId();
//                BeanUtils.copyProperties(dto, obj);
//                return obj;
//            })
//            .collect(Collectors.toList())
//        );
//    }

    /**
     * 更新 测试 AuthId 模板 ；
     *
     * @param id     {@link String} 数据ID
     * @param userId {@link Long} 操作用户ID
     * @param dto    {@link TabCodeAuthIdUpdateDTO} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(
            @NotBlank(message = "【id】不能为空") final String id
            , @NotNull(message = "【userId】不能为null") final Long userId
            , @Valid @NotNull(message = "【dto】不能为null") final TabCodeAuthIdUpdateDTO dto) {
        final TabCodeAuthId obj = new TabCodeAuthId();
        BeanUtils.copyProperties(dto, obj);
        UpdateRowsException.asserts(repository.update(id, userId, obj));
        // tabCodeAuthIdCache.delete(id); // 若使用缓存需要解开代码
    }

//    /**
//     * 测试 AuthId 模板 按ID删除，物理删除
//     *
//     * @param id     {@link String} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotBlank(message = "【id】不能为空") final String id
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteById(id, userId));
//        // tabCodeAuthIdCache.delete(id); // 若使用缓存需要解开代码
//    }
//
//    /**
//     * 测试 AuthId 模板 按 id+updateTime 删除，物理删除
//     *
//     * @param id         {@link String} 数据ID
//     * @param updateTime {@link String} 最后一次更新时间
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotBlank(message = "【id】不能为空") final String id
//            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
//            ,@NotNull(message = "【userId】不能为null") final Long userId
//    ) {
//        DeleteRowsException.asserts(repository.deleteById(id, updateTime, userId), ids.size());
//        // tabCodeAuthIdCache.delete(id); // 若使用缓存需要解开代码
//    }
//
//    /**
//     * 测试 AuthId 模板 按ID删除，物理删除
//     *
//     * @param ids     Set<String> {@link TabCodeAuthId#getId()} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull String> ids
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids, userId), ids.size());
//        // tabCodeAuthIdCache.delete(ids); // 若使用缓存需要解开代码
//    }
//
//    /**
//     * 测试 AuthId 模板 按 id+updateTime 删除，物理删除
//     *
//     * @param ids     Set<String> {@link TabCodeAuthId#getId()} 数据ID
//     * @param updateTimes     Set<String> 最后一次更新时间
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull String> ids
//            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
//            ,@NotNull(message = "【userId】不能为null") final Long userId
//    ) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids, updateTimes, userId), ids.size());
//        // tabCodeAuthIdCache.delete(ids); // 若使用缓存需要解开代码
//    }
//

    /**
     * 测试 AuthId 模板 按ID删除，逻辑删除
     *
     * @param id     {@link String} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotBlank(message = "【id】不能为空") final String id
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        DeleteRowsException.asserts(repository.markDeleteById(id, userId));
        // tabCodeAuthIdCache.delete(id); // 若使用缓存需要解开代码
    }

    /**
     * 测试 AuthId 模板 按 id+updateTime 删除，逻辑删除
     *
     * @param id         {@link String} 数据ID
     * @param updateTime {@link String} 最后一次更新时间
     * @param userId     {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteById(
            @NotBlank(message = "【id】不能为空") final String id
            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
            , @NotNull(message = "【userId】不能为null") final Long userId
    ) {
        DeleteRowsException.asserts(repository.markDeleteById(id, updateTime, userId));
        // tabCodeAuthIdCache.delete(id); // 若使用缓存需要解开代码
    }

    /**
     * 测试 AuthId 模板 按ID删除，逻辑删除
     *
     * @param ids    Set<String> {@link TabCodeAuthId#getId()} 数据ID
     * @param userId {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull String> ids
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, userId), ids.size());
        // tabCodeAuthIdCache.delete(ids); // 若使用缓存需要解开代码
    }

    /**
     * 测试 AuthId 模板 按 id+updateTime 删除，逻辑删除
     *
     * @param ids         Set<String> {@link TabCodeAuthId#getId()} 数据ID
     * @param updateTimes Set<String> 最后一次更新时间
     * @param userId      {@link Long} 操作用户ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void markDeleteByIds(
            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull String> ids
            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
            , @NotNull(message = "【userId】不能为null") final Long userId
    ) {
        DeleteRowsException.asserts(repository.markDeleteByIds(ids, updateTimes, userId), ids.size());
        // tabCodeAuthIdCache.delete(ids); // 若使用缓存需要解开代码
    }

    /**
     * 填充用户昵称
     *
     * @param list List<TabCodeAuthId>
     * @return List<TabCodeAuthId>
     */
    private <T extends TabCodeAuthId> List<T> fillUserNickname(final List<T> list) {
        if (list.isEmpty()) {
            return list;
        }
        final Map<Long, TabUser> userMap = userService.mapByIds(list.stream()
                .flatMap(row -> Stream.of(
                        row.getCreateUserId(), row.getUpdateUserId()
                ))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet())
        );
        list.forEach(row -> {
            Optional.ofNullable(userMap.get(row.getCreateUserId())).ifPresent(user -> row.setCreateUserNickname(user.getNickname()));
            Optional.ofNullable(userMap.get(row.getUpdateUserId())).ifPresent(user -> row.setUpdateUserNickname(user.getNickname()));
        });
        return list;
    }

    /**
     * 填充用户昵称
     *
     * @param row TabCodeAuthId
     * @return TabCodeAuthId
     */
    private <T extends TabCodeAuthId> T fillUserNickname(final T row) {
        if (Objects.isNull(row)) {
            return row;
        }
        final Map<Long, TabUser> userMap = userService.mapByIds(
                Stream.of(
                                row.getCreateUserId(), row.getUpdateUserId()
                        )
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet())
        );
        Optional.ofNullable(userMap.get(row.getCreateUserId())).ifPresent(user -> row.setCreateUserNickname(user.getNickname()));
        Optional.ofNullable(userMap.get(row.getUpdateUserId())).ifPresent(user -> row.setUpdateUserNickname(user.getNickname()));
        return row;
    }

    /**
     * 测试 AuthId 模板 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link String} 数据ID
     * @return Optional<TabCodeAuthId> {@link TabCodeAuthId} 实体对象
     */
    public Optional<TabCodeAuthId> findById(final String id) {
        if (Strings.isNullOrEmpty(id)) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabCodeAuthId::cloneObject) // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
                .map(this::fillUserNickname);
//         return Optional.ofNullable(repository.findCacheById(id)).map(TabCodeAuthId::cloneObject); // 若使用缓存需要解开代码
    }

    /**
     * 测试 AuthId 模板 按条件分页查询列表
     *
     * @param condition {@link TabCodeAuthId} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return QueryResults<TabCodeAuthId> {@link TabCodeAuthId} 分页对象
     */
    public @NotNull(message = "返回值不能为null") QueryResults<TabCodeAuthId> page(
            @NotNull(message = "【condition】不能为null") final TabCodeAuthId condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<TabCodeAuthId> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 测试 AuthId 模板 按条件查询列表
//     *
//     * @param condition {@link TabCodeAuthId} 查询条件
//     * @return List<TabCodeAuthId> {@link TabCodeAuthId} 结果集合
//     */
//    public @NotNull(message = "返回值不能为null") List<TabCodeAuthId> list(
//            @NotNull(message = "【condition】不能为null") final TabCodeAuthId condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 测试 AuthId 模板 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<String> {@link TabCodeAuthId#getId()> 数据 id 集合
     * @return List<TabCodeAuthId> {@link TabCodeAuthId} 结果集合
     */
    public @NotNull(message = "返回值不能为null") List<TabCodeAuthId> listByIds(final Collection<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }

    /**
     * 测试 AuthId 模板 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<String> {@link TabCodeAuthId#getId()> 数据 id 集合
     * @return Map<String, TabCodeAuthId> {@link TabCodeAuthId} 结果集合
     */
    public @NotNull(message = "返回值不能为null") Map<String, TabCodeAuthId> mapByIds(final Set<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }

//    /**
//     * 测试 AuthId 模板 按条件查询并跳过指定行数
//     * 用于瀑布流加载方案（数据量大时 page 方法会先查一遍 count 性能损耗较大，用 pull 方法规避 count 性能损耗）
//     *
//     * @param skip      int 跳过 skip 行数据
//     * @param limit     int 获取 limit  行数据
//     * @param condition {@link TabCodeAuthId} 查询条件
//     * @return List<TabCodeAuthId> {@link TabCodeAuthId} 列表查询结果集
//     */
//    public @NotNull(message = "返回值不能为null") List<TabCodeAuthId> pull(final int skip, final int limit, final TabCodeAuthId condition) {
//        List<TabCodeAuthId> list = repository.pull(skip, limit, condition);
//        this.fillUserNickname(list);
//        return list;
//    }
//
}
