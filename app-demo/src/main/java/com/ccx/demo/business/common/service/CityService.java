package com.ccx.demo.business.common.service;

import app.common.starter.entity.Page;
import app.common.starter.entity.Result;
import app.common.starter.exception.UpdateRowsException;
import app.common.starter.interfaces.ICache;
import app.nfs.starter.dto.JsonFileDTO;
import app.nfs.starter.service.FileService;
import com.ccx.demo.business.common.dao.jpa.CityRepository;
import com.ccx.demo.business.common.dto.TabCityInsertDTO;
import com.ccx.demo.business.common.dto.TabCityUpdateDTO;
import com.ccx.demo.business.common.entity.TabCity;
import com.ccx.demo.business.common.vo.TabCityStaticJsonVO;
import com.google.common.base.Strings;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.slf4j.MDC;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static app.security.starter.SecurityConfiguration.MDC_TRACE_ID;

/**
 * 服务接口实现类：城市 V20221105
 *
 * @author 谢长春 on 2022-06-28
 */
@Slf4j
@Service
@Validated
@RequiredArgsConstructor

public class CityService implements ICache.ILocalCache {
    private final CityRepository repository;
    private final FileService fileService;
    private final RedissonClient redissonClient;
    // 主题名：发布/订阅 本地缓存清理消息队列
    private static final String TOPIC = String.format("topic:%s", CityService.class.getSimpleName());
    // 本地缓存清理消息
    private static final String TOPIC_PUBLISH_MESSAGE = "clear:CACHE_MAP:%s";
    private static final Map<String, TabCity> CACHE_MAP;

    private static final JsonFileDTO listJsonFileDTO;
    private static final JsonFileDTO treeJsonFileDTO;
    private static final JsonFileDTO mapJsonFileDTO;

    static {
        CACHE_MAP = new ConcurrentHashMap<>();
        listJsonFileDTO = new JsonFileDTO("city/list.json");
        treeJsonFileDTO = new JsonFileDTO("city/tree.json");
        mapJsonFileDTO = new JsonFileDTO("city/map.json");
    }

    public JsonFileDTO getListJsonFileDTO() {
        return listJsonFileDTO;
    }

    public JsonFileDTO getTreeJsonFileDTO() {
        return treeJsonFileDTO;
    }

    public JsonFileDTO getMapJsonFileDTO() {
        return mapJsonFileDTO;
    }

    /**
     * 静态资源写入文件
     *
     * @return {@link CommandLineRunner}
     */
    @Bean
    public CommandLineRunner writeCityJsonFileCommandLineRunner() {
        return args -> {
            log.info("写入城市信息静态资源；文件已存在时不执行写入动作：\n{}\n{}\n{}"
                    , listJsonFileDTO
                    , treeJsonFileDTO
                    , mapJsonFileDTO
            );
            if (!(fileService.exists(listJsonFileDTO)
                    && fileService.exists(treeJsonFileDTO)
                    && fileService.exists(mapJsonFileDTO))
            ) {
                this.refreshJson();
            }
        };
    }

    @PostConstruct
    public void postConstruct() {
        // 订阅缓存清理通知
        redissonClient.getTopic(TOPIC).addListener(String.class, (final CharSequence channel, final String msg) -> {
            log.info(msg);
            CACHE_MAP.clear();
        });
    }

    /**
     * 发送内存缓存清理通知
     */
    private void pushTopicClearCacheMap() {
        redissonClient.getTopic(TOPIC).publishAsync(String.format(TOPIC_PUBLISH_MESSAGE, MDC.get(MDC_TRACE_ID)));
    }

    /**
     * 新增 城市
     *
     * @param dto    {@link TabCityInsertDTO} 实体对象
     * @param userId {@link Long} 操作用户ID
     * @return {@link TabCity} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public @NotNull(message = "返回值不能为null") TabCity insert(
            @Valid @NotNull(message = "【dto】不能为null") final TabCityInsertDTO dto
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        final TabCity obj = new TabCity();
        BeanUtils.copyProperties(dto, obj);
        final TabCity insertResult = repository.insert(userId, obj);
        this.pushTopicClearCacheMap();
        return insertResult;
    }
//    /**
//     * 批量新增 城市
//     *
//     * @param list   List<TabCityInsertDTO> {@link TabCityInsertDTO}  实体对象集合
//     * @param userId {@link Long} 操作用户ID
//     * @return List<TabCity> 实体对象集合
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public @NotNull(message = "返回值不能为null") List<TabCity> insert(
//            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull TabCityInsertDTO> list
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        return repository.insert(userId, list.stream()
//            .map(dto -> {
//                final TabCity obj = new TabCity();
//                BeanUtils.copyProperties(dto, obj);
//                return obj;
//            })
//            .collect(Collectors.toList())
//        );
//    }

    /**
     * 更新 城市 ；
     *
     * @param id     {@link String} 数据ID
     * @param userId {@link Long} 操作用户ID
     * @param dto    {@link TabCityUpdateDTO} 实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(
            @NotBlank(message = "【id】不能为空") final String id
            , @NotNull(message = "【userId】不能为null") final Long userId
            , @Valid @NotNull(message = "【dto】不能为null") final TabCityUpdateDTO dto) {
        final TabCity obj = new TabCity();
        BeanUtils.copyProperties(dto, obj);
        UpdateRowsException.asserts(repository.update(id, userId, obj));
        this.pushTopicClearCacheMap();
        // tabCityCache.delete(id); // 若使用缓存需要解开代码
    }

//    /**
//     * 城市 按ID删除，物理删除
//     *
//     * @param id     {@link String} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotBlank(message = "【id】不能为空") final String id
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteById(id, userId));
//        // tabCityCache.delete(id); // 若使用缓存需要解开代码
//    }
//
////    /**
////     * 城市 按 id+updateTime 删除，物理删除
////     *
////     * @param id         {@link String} 数据ID
////     * @param updateTime {@link String} 最后一次更新时间
////     * @param userId {@link Long} 操作用户ID
////     */
////    @Transactional(rollbackFor = Exception.class)
////    public void deleteById(
////            @NotBlank(message = "【id】不能为空") final String id
////            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
////            ,@NotNull(message = "【userId】不能为null") final Long userId
////    ) {
////        DeleteRowsException.asserts(repository.deleteById(id, updateTime, userId), ids.size());
////        // tabCityCache.delete(id); // 若使用缓存需要解开代码
////    }
//
//    /**
//     * 城市 按ID删除，物理删除
//     *
//     * @param ids     Set<String> {@link TabCity#getId()} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull String> ids
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids, userId), ids.size());
//        // tabCityCache.delete(ids); // 若使用缓存需要解开代码
//    }
//
////    /**
////     * 城市 按 id+updateTime 删除，物理删除
////     *
////     * @param ids     Set<String> {@link TabCity#getId()} 数据ID
////     * @param updateTimes     Set<String> 最后一次更新时间
////     * @param userId {@link Long} 操作用户ID
////     */
////    @Transactional(rollbackFor = Exception.class)
////    public void deleteByIds(
////            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull String> ids
////            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
////            ,@NotNull(message = "【userId】不能为null") final Long userId
////    ) {
////        DeleteRowsException.asserts(repository.deleteByIds(ids, updateTimes, userId), ids.size());
////        // tabCityCache.delete(ids); // 若使用缓存需要解开代码
////    }
//

    /**
     * 城市 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link String} 数据ID
     * @return Optional<TabCity> {@link TabCity} 实体对象
     */

    public Optional<TabCity> findById(final String id) {
        if (Strings.isNullOrEmpty(id)) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabCity::cloneObject) // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
                ;
//         return Optional.ofNullable(repository.findCacheById(id)).map(TabCity::cloneObject); // 若使用缓存需要解开代码
    }

    /**
     * 城市 按条件分页查询列表
     *
     * @param condition {@link TabCity} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return QueryResults<TabCity> {@link TabCity} 分页对象
     */

    public @NotNull(message = "返回值不能为null") QueryResults<TabCity> page(
            @NotNull(message = "【condition】不能为null") final TabCity condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<TabCity> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        //this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }

    /**
     * 城市 按父节点编码查询子节点列表
     *
     * @param pid String {@link TabCity#getPid()} 查询条件
     * @return List<TabCity> {@link TabCity} 结果集合
     */

    public @NotNull(message = "返回值不能为null") List<TabCity> listByPid(final String pid) {
        final TabCity condition = new TabCity();
        condition.setPid(Optional.ofNullable(pid).orElse(""));
        condition.setOrderBy(Collections.singletonList(TabCity.OrderByColumn.id.asc()));
        return repository.list(condition);
    }


//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 城市 按条件查询列表
//     *
//     * @param condition {@link TabCity} 查询条件
//     * @return List<TabCity> {@link TabCity} 结果集合
//     */
//
//    public @NotNull(message = "返回值不能为null") List<TabCity> list(
//            @NotNull(message = "【condition】不能为null") final TabCity condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 城市 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<String> {@link TabCity#getId()> 数据 id 集合
     * @return List<TabCity> {@link TabCity} 结果集合
     */

    public @NotNull(message = "返回值不能为null") List<TabCity> listByIds(final Collection<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        final Map<String, TabCity> cacheMap = getCacheMap();
        return ids.stream().filter(cacheMap::containsKey).map(cacheMap::get).collect(Collectors.toList());
    }

    /**
     * 城市 按 id 批量查询列表，返回 map ， key 为数据 id
     *
     * @param ids Collection<String> {@link TabCity#getId()> 数据 id 集合
     * @return Map<String, TabCity> {@link TabCity} 结果集合
     */

    public @NotNull(message = "返回值不能为null") Map<String, TabCity> mapByIds(final Set<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        final Map<String, TabCity> cacheMap = getCacheMap();
        return ids.stream().filter(cacheMap::containsKey).collect(Collectors.toMap(id -> id, cacheMap::get));
    }

    /**
     * 城市 按 id 批量查询列表，返回 map ， key 为数据 id ， value 为 城市名称
     *
     * @param ids Collection<String> {@link TabCity#getId()> 数据 id 集合
     * @return Map<$ { idType }, $ { TabName }> {@link TabCity} 结果集合
     */

    public @NotNull(message = "返回值不能为null") Map<String, String> mapNameByIds(final Set<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        final Map<String, TabCity> cacheMap = getCacheMap();
        return ids.stream().filter(cacheMap::containsKey).collect(Collectors.toMap(id -> id, id -> cacheMap.get(id).getName()));
    }

//    /**
//     * 城市 下拉选项
//     *
//     * @return List<TabCity> {@link TabCity} 结果集合
//     */
//
//    public @NotNull(message = "返回值不能为null") List<TabCity> options() {
//        return repository.findAll();
//    }

    /**
     * 获取城市缓存集合
     *
     * @return Map<String, TabCity>
     */
    public Map<String, TabCity> getCacheMap() {
        if (CACHE_MAP.isEmpty()) {
            synchronized (this) {
                if (CACHE_MAP.isEmpty()) {
                    repository.forEach(
                            list -> list.forEach(row -> CACHE_MAP.put(row.getId(), row))
                            , new TabCity().where()
                    );
                }
            }
        }
        return CACHE_MAP;
    }

    /**
     * 城市名称缓存刷新
     */
    public void refreshCache() {
        CACHE_MAP.clear();
        getCacheMap();
    }

    /**
     * 刷新json静态文件
     */
    public void refreshJson() {
        final List<TabCity> list = repository.findAll();
        try {
            list.forEach(row -> CACHE_MAP.put(row.getId(), row));
        } catch (Exception e) {
            log.error("CACHE_MAP 缓存异常", e);
        }
        try {
            fileService.writeAndReplace(
                    new Result<TabCityStaticJsonVO>()
                            .setSuccess(list.stream()
                                    .map(row -> {
                                        TabCityStaticJsonVO vo = new TabCityStaticJsonVO();
                                        BeanUtils.copyProperties(row, vo);
                                        return vo;
                                    })
                                    .collect(Collectors.toList())
                            )
                            .json()
                    , listJsonFileDTO
            );
        } catch (Exception e) {
            log.error("{}，文件写入异常", listJsonFileDTO.getAbsolutePath(), e);
        }
        try {
            final Map<String, String> map = list.stream().collect(Collectors.toMap(TabCity::getId, TabCity::getName));
            fileService.writeAndReplace(new Result<Map<String, String>>().setSuccess(map).json(), mapJsonFileDTO);
        } catch (Exception e) {
            log.error("{}，文件写入异常", mapJsonFileDTO.getAbsolutePath(), e);
        }
        try {
            final Map<String, List<TabCity>> groupByPid = list.stream().collect(Collectors.groupingBy(TabCity::getPid));
            final List<TabCity> provinceList = groupByPid.get(""); // 省
            provinceList.forEach(province -> {
                // 市
                final List<TabCity> cityList = groupByPid.getOrDefault(province.getId(), Collections.emptyList());
                // 区
                cityList.forEach(city -> city.setNodes(groupByPid.getOrDefault(city.getId(), Collections.emptyList())));
                province.setNodes(cityList);
            });
            fileService.writeAndReplace(new Result<TabCity>().setSuccess(provinceList).json(), treeJsonFileDTO);
        } catch (Exception e) {
            log.error("{}，文件写入异常", treeJsonFileDTO.getAbsolutePath(), e);
        }
    }
}
