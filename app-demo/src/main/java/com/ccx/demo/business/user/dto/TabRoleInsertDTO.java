package com.ccx.demo.business.user.dto;

import app.security.starter.vo.Authority;
import com.ccx.demo.business.user.entity.TabRole;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * DTO：角色新增
 * + 定义非空字段校验，原来在属性头上使用 groups 不够灵活，这里通过覆写 get 方法追加非空校验
 * + 定义角色新增时的扩展属性
 *
 * @author 谢长春 on 2022-02-03
 */
@Getter
@Setter
@ToString(callSuper = true)
public class TabRoleInsertDTO extends TabRole {

    @NotBlank
    @Override
    public String getName() {
        return super.getName();
    }

    @NotEmpty
    @Override
    public List<Authority> getAuthorityTree() {
        return super.getAuthorityTree();
    }

}
