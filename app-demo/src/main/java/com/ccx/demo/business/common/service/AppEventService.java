package com.ccx.demo.business.common.service;

import app.cache.starter.cache.ClickEventCache;
import app.common.starter.dto.AppEventDTO;
import app.common.starter.entity.Page;
import app.common.starter.event.AppEvent;
import app.common.starter.util.JSON;
import app.encrypt.domain.starter.DomainEncrypt;
import app.enums.starter.AppEventKey;
import com.ccx.demo.business.common.dao.jpa.AppEventRepository;
import com.ccx.demo.business.common.entity.QTabAppEvent;
import com.ccx.demo.business.common.entity.TabAppEvent;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Pattern;

/**
 * 服务接口实现类：埋点表 V20221105
 *
 * @author 谢长春 on 2022-06-27
 */
@Primary
@Slf4j
@Service
@Validated
@RequiredArgsConstructor

public class AppEventService
//      , ITabAppEventCache
{
    private final AppEventRepository repository;
    private final ClickEventCache clickEventCache;

    //private final TabAppEventCache tabAppEventCache;
    //private final UserService userService;
    @Async
    @EventListener
    @SneakyThrows
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void listenerAppEvent(AppEvent event) {
        if (event.getEventKey().isClick()) { // 点击数只存在 redis , 不写 mysql
            clickEventCache.increment(
                    event.getEventKey()
                    , Optional.ofNullable(event.getRefLongId())
                            .map(Objects::toString)
                            .orElseGet(event::getRefStringId)
                    , event.getUserId()
            );
            return;
        }
        final TabAppEvent obj = new TabAppEvent();
//        obj.setAppName(event.getAppName());
        obj.setEvent(event.getEventKey());
        obj.setRefStringId(event.getRefStringId());
        obj.setRefLongId(event.getRefLongId());
        obj.setUserId(event.getUserId());
        obj.setData(event.getData());
        repository.insert(event.getUserId(), obj);
    }

    /**
     * 新增 埋点表
     *
     * @param dto    {@link AppEventDTO} 实体对象
     * @param userId {@link Long} 操作用户ID
     * @return {@link TabAppEvent} 实体对象
     */
    @Async
    @Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void insert(
            @Valid @NotNull(message = "【dto】不能为null") final AppEventDTO dto
            , @NotNull(message = "【userId】不能为null") final Long userId) {
        try {
            if (dto.getEvent().isClick()) { // 点击数只存在 redis , 不写 mysql
                clickEventCache.increment(
                        dto.getEvent()
                        , Optional.ofNullable(dto.getRefLongId())
                                .map(Objects::toString)
                                .orElseGet(dto::getRefStringId)
                        , userId
                );
                return;
            }
            final TabAppEvent obj = new TabAppEvent();
            BeanUtils.copyProperties(dto, obj);
            repository.insert(userId, obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 埋点
     *
     * @param event  {@link AppEventKey} 实体对象
     * @param userId {@link Long} 操作用户ID
     */
    @Async
    @Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void insert(final AppEventKey event, final Long userId) {
        try {
            if (Objects.isNull(event)) {
                log.error("event is null");
                return;
            }
            if (event.isClick()) { // 点击数只存在 redis , 不写 mysql
                clickEventCache.increment(event, event.name(), userId);
                return;
            }
            final TabAppEvent obj = new TabAppEvent();
            obj.setEvent(event);
            repository.insert(userId, obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static final Pattern REG_ID = Pattern.compile("^\\d+");

    /**
     * 埋点
     *
     * @param event  {@link AppEventKey} 实体对象
     * @param userId {@link Long} 操作用户ID
     */
    @Async
    @Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void insert(final AppEventKey event, final Long userId, final String id) {
        try {
            if (Objects.isNull(event)) {
                log.error("event is null");
                return;
            }
            if (event.isClick()) { // 点击数只存在 redis , 不写 mysql
                clickEventCache.increment(event, id, userId);
                return;
            }
            final TabAppEvent obj = new TabAppEvent();
            obj.setEvent(event);
            obj.setRefStringId(id);
            if (event.isRefLong() && StringUtils.isNotBlank(id)) {
                if (REG_ID.matcher(id).matches()) {
                    obj.setRefLongId(Long.parseLong(id));
                } else {
                    // 前端有些传的是加密id，需要解密获得id
                    obj.setRefLongId(DomainEncrypt.decryptLongId(id));
                }
            }
            repository.insert(userId, obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 埋点
     *
     * @param event  {@link AppEventKey} 实体对象
     * @param userId {@link Long} 操作用户ID
     */
    @Async
    @Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void insert(final AppEventKey event, final Long userId, final Map<String, Object> map) {
        try {
            if (Objects.isNull(event)) {
                log.error("event is null");
                return;
            }
            if (event.isClick()) { // 点击数只存在 redis , 不写 mysql
                clickEventCache.increment(event, event.name(), userId);
                return;
            }
            final TabAppEvent obj = new TabAppEvent();
            obj.setEvent(event);
            obj.setData(JSON.toJsonString(map));
            repository.insert(userId, obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 埋点
     *
     * @param event  {@link AppEventKey} 实体对象
     * @param userId {@link Long} 操作用户ID
     */
    @Async
    @Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void insert(final AppEventKey event, final Long userId, final String id, final Map<String, Object> map) {
        try {
            if (Objects.isNull(event)) {
                log.error("event is null");
                return;
            }
            if (event.isClick()) { // 点击数先写 redis
                clickEventCache.increment(event, id, userId);
            }
            final TabAppEvent obj = new TabAppEvent();
            obj.setEvent(event);
            obj.setRefStringId(id);
            obj.setData(JSON.toJsonString(map));
            repository.insert(userId, obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 埋点
     *
     * @param event  {@link AppEventKey} 实体对象
     * @param userId {@link Long} 操作用户ID
     */
    @Async
    @Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void insert(final AppEventKey event, final Long userId, final Long id, final Map<String, Object> map) {
        try {
            if (Objects.isNull(event)) {
                log.error("event is null");
                return;
            }
            if (event.isClick()) { // 点击数先写 redis
                clickEventCache.increment(event, Objects.toString(id), userId);
            }
            final TabAppEvent obj = new TabAppEvent();
            obj.setEvent(event);
            obj.setRefLongId(id);
            obj.setData(JSON.toJsonString(map));
            repository.insert(userId, obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

//    /**
//     * 批量新增 埋点表
//     *
//     * @param list   List<TabAppEventInsertDTO> {@link TabAppEventInsertDTO}  实体对象集合
//     * @param userId {@link Long} 操作用户ID
//     * @return List<TabAppEvent> 实体对象集合
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public @NotNull(message = "返回值不能为null") List<TabAppEvent> insert(
//            @NotEmpty(message = "【list】不能为空") final List<@Valid @NotNull TabAppEventInsertDTO> list
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        return repository.insert(userId, list.stream()
//            .map(dto -> {
//                final TabAppEvent obj = new TabAppEvent();
//                BeanUtils.copyProperties(dto, obj);
//                return obj;
//            })
//            .collect(Collectors.toList())
//        );
//    }

//    /**
//     * 更新 埋点表 ；
//     *
//     * @param id     {@link Long} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     * @param dto    {@link TabAppEventUpdateDTO} 实体对象
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void update(
//            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
//            ,@NotNull(message = "【userId】不能为null") final Long userId
//            ,@Valid @NotNull(message = "【dto】不能为null") final TabAppEventUpdateDTO dto) {
//        final TabAppEvent obj = new TabAppEvent();
//        BeanUtils.copyProperties(dto, obj);
//        UpdateRowsException.asserts(repository.update(id, userId, obj));
//        // tabAppEventCache.delete(id); // 若使用缓存需要解开代码
//    }

//    /**
//     * 埋点表 按ID删除，物理删除
//     *
//     * @param id     {@link Long} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteById(
//            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteById(id, userId));
//        // tabAppEventCache.delete(id); // 若使用缓存需要解开代码
//    }
//
////    /**
////     * 埋点表 按 id+updateTime 删除，物理删除
////     *
////     * @param id         {@link Long} 数据ID
////     * @param updateTime {@link String} 最后一次更新时间
////     * @param userId {@link Long} 操作用户ID
////     */
////    @Transactional(rollbackFor = Exception.class)
////    public void deleteById(
////            @NotNull(message = "【id】不能为null") @Positive(message = "【id】必须大于0") final Long id
////            , @NotBlank(message = "【updateTime】不能为null") @Size(min = 17, max = 17, message = "【updateTime】必须是 17 位") final String updateTime
////            ,@NotNull(message = "【userId】不能为null") final Long userId
////    ) {
////        DeleteRowsException.asserts(repository.deleteById(id, updateTime, userId), ids.size());
////        // tabAppEventCache.delete(id); // 若使用缓存需要解开代码
////    }
//
//    /**
//     * 埋点表 按ID删除，物理删除
//     *
//     * @param ids     Set<Long> {@link TabAppEvent#getId()} 数据ID
//     * @param userId {@link Long} 操作用户ID
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void deleteByIds(
//            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
//            ,@NotNull(message = "【userId】不能为null") final Long userId) {
//        DeleteRowsException.asserts(repository.deleteByIds(ids, userId), ids.size());
//        // tabAppEventCache.delete(ids); // 若使用缓存需要解开代码
//    }
//
////    /**
////     * 埋点表 按 id+updateTime 删除，物理删除
////     *
////     * @param ids     Set<Long> {@link TabAppEvent#getId()} 数据ID
////     * @param updateTimes     Set<String> 最后一次更新时间
////     * @param userId {@link Long} 操作用户ID
////     */
////    @Transactional(rollbackFor = Exception.class)
////    public void deleteByIds(
////            @NotEmpty(message = "【ids】不能为空") final Set<@Valid @NotNull Long> ids
////            , @NotEmpty(message = "【updateTimes】不能为空") final Set<@Valid @NotBlank String> updateTimes
////            ,@NotNull(message = "【userId】不能为null") final Long userId
////    ) {
////        DeleteRowsException.asserts(repository.deleteByIds(ids, updateTimes, userId), ids.size());
////        // tabAppEventCache.delete(ids); // 若使用缓存需要解开代码
////    }
//

    /**
     * 埋点表 按ID查询对象，注意这里可能有 deleted 为 YES 的数据
     *
     * @param id {@link Long} 数据ID
     * @return Optional<TabAppEvent> {@link TabAppEvent} 实体对象
     */

    public Optional<TabAppEvent> findById(final Long id) {
        if (Objects.isNull(id) || id < 1) {
            return Optional.empty();
        }
        return repository.findById(id)
                .map(TabAppEvent::cloneObject) // 必须要 clone ，如果直接对持久化对象调用 set 方法，会触发更新动作
                ;
//         return Optional.ofNullable(repository.findCacheById(id)).map(TabAppEvent::cloneObject); // 若使用缓存需要解开代码
    }

    /**
     * 埋点表 按条件分页查询列表
     *
     * @param condition {@link TabAppEvent} 查询条件
     * @param page      {@link Page} 分页排序集合
     * @return QueryResults<TabAppEvent> {@link TabAppEvent} 分页对象
     */

    public @NotNull(message = "返回值不能为null") QueryResults<TabAppEvent> page(
            @NotNull(message = "【condition】不能为null") final TabAppEvent condition,
            @NotNull(message = "【page】不能为null") @Valid final Page page) {
        final QueryResults<TabAppEvent> queryResults = repository.page(condition, page);
        if (queryResults.isEmpty()) {
            return QueryResults.emptyResults();
        }
        //this.fillUserNickname(queryResults.getResults());
        return queryResults;
    }

//// 非必要情况下不要开放列表查询方法，因为没有分页控制，容易内存溢出。大批量查询数据应该使用分页查询
//    /**
//     * 埋点表 按条件查询列表
//     *
//     * @param condition {@link TabAppEvent} 查询条件
//     * @return List<TabAppEvent> {@link TabAppEvent} 结果集合
//     */
//
//    public @NotNull(message = "返回值不能为null") List<TabAppEvent> list(
//            @NotNull(message = "【condition】不能为null") final TabAppEvent condition) {
//        return repository.list(condition);
//    }
//

    /**
     * 埋点表 按 id 批量查询列表，注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<Long> {@link TabAppEvent#getId()> 数据 id 集合
     * @return List<TabAppEvent> {@link TabAppEvent} 结果集合
     */

    public @NotNull(message = "返回值不能为null") List<TabAppEvent> listByIds(final Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return repository.listByIds(ids);
    }

    /**
     * 埋点表 按 id 批量查询列表，返回 map ， key 为数据 id ， 注意这里可能有 deleted 为 true 的数据
     *
     * @param ids Collection<Long> {@link TabAppEvent#getId()> 数据 id 集合
     * @return List<TabAppEvent> {@link TabAppEvent} 结果集合
     */

    public @NotNull(message = "返回值不能为null") Map<Long, TabAppEvent> mapByIds(final Set<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        return repository.mapByIds(ids);
    }


    public void forEachEvent(final Consumer<List<TabAppEvent>> listConsumer, TabAppEvent condition) {
        repository.forEach(
                listConsumer,
                condition.where(),
                QTabAppEvent.tabAppEvent.id,
                QTabAppEvent.tabAppEvent.refLongId,
                QTabAppEvent.tabAppEvent.event
        );
    }

    /**
     * 统计
     *
     * @param condition
     * @return
     */

    public Long count(final TabAppEvent condition) {
        return repository.count(condition.where());
    }


}
