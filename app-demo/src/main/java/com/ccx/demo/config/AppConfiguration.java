package com.ccx.demo.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * Spring Core，参考：
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html
 *
 * Spring MVC 配置，参考 ：
 * https://linesh.gitbooks.io/spring-mvc-documentation-linesh-translation/content/
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html
 *
 * Spring Thymeleaf 配置，参考 ：
 * https://www.thymeleaf.org/doc/tutorials/3.0/thymeleafspring.html#spring-mvc-configuration
 *
 * Spring validator 配置，参考
 * https://beanvalidation.org/2.0/spec/#introduction
 * https://www.baeldung.com/javax-validation-method-constraints
 * http://rubygems.torquebox.org/proposals/BVAL-241/
 * https://juejin.im/entry/5b5a94d2f265da0f7c4fd2b2
 * https://www.cnblogs.com/mr-yang-localhost/p/7812038.html
 *
 * @author 谢长春 2018-10-3
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
//@EnableWebMvc
public class AppConfiguration {
//    /**
//     * 服务端 500 异常处理
//     * 需要自定义 Controller 继承 {@link AbstractMvcConfig.ErrorController}
//     * spring security 需要添加 http.antMatchers("/error").permitAll()
//     *
//     * @author 谢长春
//     */
//    public static class ErrorController extends AbstractErrorController {
//        public ErrorController(ErrorAttributes errorAttributes) {
//            super(errorAttributes);
//        }
//
//        @Override
//        public String getErrorPath() {
//            return "/error";
//        }
//
//        /**
//         * 处理服务端 500 异常
//         */
//        @RequestMapping(value = "/error", method = {GET, POST, PUT, PATCH, DELETE})
//        @ResponseBody
//        public Result<Void> error() {
//            return Code.A00001.toResult("500：请求失败，不明确的异常");
//        }
//    }

//    /**
//     * 启用 FastJson
//     */
//    @Bean
//    public FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
//        JSON.DEFAULT_GENERATE_FEATURE |= SerializerFeature.DisableCircularReferenceDetect.getMask(); // 解决循环引用问题
//        final FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//        converter.setDefaultCharset(UTF_8);
//        converter.getFastJsonConfig().setFeatures(Feature.OrderedField);
//        if (Objects.equals(AppEnv.prod, appConfig.getEnv())) {  // 生产环境不返回 Result#exception
//            final SimplePropertyPreFilter simplePropertyPreFilter = new SimplePropertyPreFilter();
//            simplePropertyPreFilter.getExcludes().add("exception");
//            converter.getFastJsonConfig().setSerializeFilters(simplePropertyPreFilter);
//        }
//        converter.setSupportedMediaTypes(Collections.singletonList(
//                MediaType.APPLICATION_JSON
//        ));
//        return converter;
//    }

//    /**
//     * 注册过滤器
//     * <pre>
//     * 添加自定义过滤器：设置链路追踪，在日志打印和响应头中追加该标记
//     * 警告：多线程时需要特殊处理
//     * final Map<String, String> mdc = MDC.getCopyOfContextMap(); // 复制主线程 ThreadLocal
//     * new Thread(() -> {
//     *     try {
//     *         MDC.setContextMap(mdc); // 设置子线程 ThreadLocal
//     *         // 子线程代码
//     *     } finally {
//     *         MDC.clear(); // 清除子线程 ThreadLocal
//     *     }
//     * }).start();
//     *
//     * @return FilterRegistrationBean
//     */
//    @Order(0)
//    @Bean
//    public FilterRegistrationBean<Filter> requestIdFilter() {
//        final FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>();
//        bean.setOrder(0);
//        bean.setFilter(new RequestIdFilter());
//        bean.addUrlPatterns("/*");
//        return bean;
//    }
}
