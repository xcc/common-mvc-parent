package com.ccx.demo.business.user.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabUser is a Querydsl query type for TabUser
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabUser extends EntityPathBase<TabUser> {

    private static final long serialVersionUID = -2085389230L;

    public static final QTabUser tabUser = new QTabUser("tabUser");

    public final SimplePath<com.ccx.demo.business.user.dto.UserAvatarImageDTO> avatar = createSimple("avatar", com.ccx.demo.business.user.dto.UserAvatarImageDTO.class);

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Long> createUserId = createNumber("createUserId", Long.class);

    public final BooleanPath deleted = createBoolean("deleted");

    public final BooleanPath disabled = createBoolean("disabled");

    public final StringPath email = createString("email");

    public final BooleanPath hidden = createBoolean("hidden");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nickname = createString("nickname");

    public final StringPath oldPhone = createString("oldPhone");

    public final StringPath password = createString("password");

    public final StringPath phone = createString("phone");

    public final EnumPath<com.ccx.demo.business.user.enums.RegisterSource> registerSource = createEnum("registerSource", com.ccx.demo.business.user.enums.RegisterSource.class);

    public final ArrayPath<String[], String> roles = createArray("roles", String[].class);

    public final StringPath updateTime = createString("updateTime");

    public final NumberPath<Long> updateUserId = createNumber("updateUserId", Long.class);

    public final StringPath username = createString("username");

    public QTabUser(String variable) {
        super(TabUser.class, forVariable(variable));
    }

    public QTabUser(Path<? extends TabUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabUser(PathMetadata metadata) {
        super(TabUser.class, metadata);
    }

}

