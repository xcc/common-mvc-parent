package com.ccx.demo.business.code.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTabCodeOpenId is a Querydsl query type for TabCodeOpenId
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTabCodeOpenId extends EntityPathBase<TabCodeOpenId> {

    private static final long serialVersionUID = -992440393L;

    public static final QTabCodeOpenId tabCodeOpenId = new QTabCodeOpenId("tabCodeOpenId");

    public final NumberPath<Long> fromId = createNumber("fromId", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> toId = createNumber("toId", Long.class);

    public QTabCodeOpenId(String variable) {
        super(TabCodeOpenId.class, forVariable(variable));
    }

    public QTabCodeOpenId(Path<? extends TabCodeOpenId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTabCodeOpenId(PathMetadata metadata) {
        super(TabCodeOpenId.class, metadata);
    }

}

