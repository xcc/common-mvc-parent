#!/bin/bash
echo '将离线 jar 包统一放在该目录，自动安装到本地仓库，部分 jar 如果不含 pom 文件，则需要指定 groupId'
#if [ $file == "slf4j-api-1.7.30.jar" ]; then
# mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$file -DgroupId=org.slf4j -DartifactId=slf4j-api -Dversion=1.7.30 -Dpackaging=jar
#fi

for file in *.jar; do
  [[ -e "$file" ]] || break
  echo "mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$file"
  mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$file
done
