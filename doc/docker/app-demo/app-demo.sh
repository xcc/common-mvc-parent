#!/bin/bash

BIT="$(uname -m)"        # 系统信息
CHOOSE=$1                # 执行动作
WORKDIR=$(cd "$(dirname "$0")" || exit; pwd) # 工作目录
LOG_HOME="${WORKDIR}/logs" # 日志目录
cd $WORKDIR || exit
#export $(xargs <.env) # 加载工作目录下的环境变量
# shellcheck disable=SC2046
export $(grep -v '^#' ".env" | xargs) # 加载工作目录下的环境变量

#if [ ! -d "${WORKDIR}/logs" ]; then
#  mkdir -p "${WORKDIR}/logs" && chmod 755 "${WORKDIR}/logs"
#fi

echo "
# 系统信息 ***************************************************
BIT: $BIT
# 执行动作 ******************************************************
CHOOSE: $CHOOSE
# 工作目录 ***************************************************
WORKDIR: $WORKDIR
# 应用名字 ***************************************************
APP_NAME: $APP_NAME
# 应用端口 ***************************************************
APP_PORT: $APP_PORT
# 应用运行环境 ***************************************************
APP_ENV: $APP_ENV
# 代码仓库 ***************************************************
GIT_REPOSITORY: $GIT_REPOSITORY
# # 代码分支（也可以指定TAG） ************************************
GIT_BRANCH: $GIT_BRANCH
# 日志目录 ***************************************************
日志目录: $LOG_HOME
# 当前目录 ***************************************************
pwd: $(pwd)
"
function build() {
  echo '# build:开始(clone代码 、打jar包 、构造镜像) ---------------------------------------------------------------------------------------------------'
  # 如果文件夹不存在，创建文件夹
  if [ ! -d server ]; then
    git clone $GIT_REPOSITORY server
    cd server || exit
  else
    cd server || exit
  fi
  git fetch
  git reset $GIT_BRANCH --hard
  mvn clean package # 编译 jar 包
  VERSION=$(git log | wc -l)
  TAG=$APP_ENV-$VERSION
  echo IMAGE_TAG=$TAG >>$WORKDIR/.env                       # 追加版本号到环境变量文件
  docker build -t $APP_NAME:$TAG $WORKDIR/server/$APP_NAME/ # 编译 docker 镜像
  docker images | grep $APP_NAME | grep $APP_ENV
  echo '# build:结束(clone代码 、打jar包 、构造镜像) ---------------------------------------------------------------------------------------------------'
}
function restart() {
  echo '# restart:开始(清理多余的镜像、停止并卸载容器、重启服务、检查服务是否启动正常) ---------------------------------------------------------------------------------------------------'
  docker image prune -f
  count=$(docker images | grep $APP_NAME | grep $APP_ENV | wc -l)
  if [ $count -gt 5 ]; then
    docker images | grep $APP_NAME | grep $APP_ENV | tail -n 2 | awk '{print $1":"$2}' | xargs docker rmi
  fi
  echo '# restart:开始(重启服务) ---------------------'
  docker-compose down                         # 停止并卸载容器
  # shellcheck disable=SC2046
  export $(grep -v '^#' $WORKDIR/.env | xargs) # 重新加载镜像版本
  echo "IMAGE_TAG: $IMAGE_TAG"
  docker-compose up -d # 启动容器
  echo '# restart:结束(重启服务) ---------------------'
  echo "日志查看命令： tail -F ${LOG_HOME}/${APP_NAME}.log "
  echo '等待验证服务重启状态'
  sleep 30 # 先睡眠 30s
  # 循环 60 次（10分钟），检查服务是否启动正常
  # 正常则输出 “服务启动成功” 并跳出循环
  # 异常则输出重试消息直到 60 次重试结束
  for i in {1..60}; do
    RESULT=$(curl http://localhost:$APP_PORT/version)
    echo $RESULT
    if [[ "$RESULT" =~ '"code":"A00000"' ]]; then # 检查 curl 结果是否包含成功标志
      echo '服务启动成功'
      exit
    else
      echo "服务启动检查重试第 $i 次"
    fi
    sleep 10 # 睡眠 10s 之后再尝试
  done
  echo '# restart:(清理多余的镜像、停止并卸载容器、重启服务、检查服务是否启动正常) ---------------------------------------------------------------------------------------------------'
}
function push() {
  echo '- push:开始(推送镜像到其他服务器) ---------------------------------------------------------------------------------------------------'
  echo '镜像打包'
  echo "$(pwd)"
  echo "推送镜像：$APP_NAME:$IMAGE_TAG"

  docker save $APP_NAME:$IMAGE_TAG | gzip >/tmp/$APP_NAME-$IMAGE_TAG.tgz # 打包镜像到临时目录

  # shellcheck disable=SC2041
  for ip in '192.168.1.88'; do # 多个 ip 用空格隔开，例： for ip in '192.168.1.88' '192.168.1.89';
    echo "推送开始：${ip}"
    scp -i ~/.ssh/ssh_id_rsa $WORKDIR/.env root@${ip}:$WORKDIR                                                                      # 复制环境变量文件到目标服务器
    scp -i ~/.ssh/ssh_id_rsa $WORKDIR/docker-compose.yml root@${ip}:$WORKDIR                                                        # 复制 docker 启动文件到目标服务器
    scp -i ~/.ssh/ssh_id_rsa $WORKDIR/$APP_NAME.sh root@${ip}:$WORKDIR                                                              # 复制操作命令脚本到目标服务器
    scp -i ~/.ssh/ssh_id_rsa /tmp/$APP_NAME-$IMAGE_TAG.tgz root@${ip}:/tmp/                                                                  # 复制镜像到目标服务器
    ssh -i ~/.ssh/ssh_id_rsa root@${ip} "docker load -i /tmp/$APP_NAME-$IMAGE_TAG.tgz && rm -rf /tmp/$APP_NAME-$IMAGE_TAG.tgz && chmod +x $WORKDIR/*.sh" # 连接目标服务器，装载镜像；装载完成之后删除镜像；授权 sh 文件可执行权限
    echo "推送结束：${ip}"
  done

  rm -rf /tmp/$APP_NAME-$IMAGE_TAG.tgz # 推送完成之后删除临时文件
  echo '- push:结束(推送镜像到其他服务器) ---------------------------------------------------------------------------------------------------'
}
# 执行数据库操作命令
function db() {
  echo ''
  # 执行 mysql 命令； app-demo.sh mysql "select * from tab_name limit 1"
  #mysql -h localhost -uroot -pprod -A app_demo_prod_db -e "$2";
  # 同步prod数据库到uat数据库
  #mysqldump -h localhost -uprodadmin -pprod app_demo_prod_db | mysql -h localhost -uuatadmin -puat app_demo_uat_db;
}
# 抓多台服务器日志
function log() {
  grep ${1:-ERROR} -A ${2:-0} ${LOG_HOME}/${APP_NAME}.log
  ssh -i ~/.ssh/ssh_id_rsa root@192.168.1.88 "grep ${1:-ERROR} -A ${2:-0} ${LOG_HOME}/${APP_NAME}.log" | grep ${1:-ERROR} -A ${2:-0}
}
# 输出所有 alias ， 写入 ~/.bashrc 文件之后可以使用自定义命令执行
function alias() {
  echo "# ${APP_NAME} ${APP_ENV} 自定义命令 *****************************************************************************"
  app_name_alias="$(echo $APP_NAME | perl -pe 's/(-|_)//g')${APP_ENV}"
  echo "alias ${app_name_alias}build='call(){ ${WORKDIR}/${APP_NAME}.sh build; };call' # 用法：${app_name_alias}build"
  echo "alias ${app_name_alias}restart='call(){ ${WORKDIR}/${APP_NAME}.sh restart; };call' # 用法：${app_name_alias}restart"
  echo "alias ${app_name_alias}push='call(){ ${WORKDIR}/${APP_NAME}.sh push; };call' # 用法：${app_name_alias}push"
  echo "alias ${app_name_alias}db='call(){ ${WORKDIR}/${APP_NAME}.sh db; };call' # 用法：${app_name_alias}db \"select * from tableName limit 1\""
  echo "alias ${app_name_alias}log='call(){ ${WORKDIR}/${APP_NAME}.sh log; };call' # 用法：${app_name_alias}log traceId 50"
}

## 清理悬挂镜像
## docker container prune -f
#docker volume prune -f
## docker network prune -f
#docker image prune -f
#mvn clean package -P dev -D maven.test.skip=true
## 卸载容器
#docker-compose down
## 启动容器
#docker-compose up -d --build

case $CHOOSE in
0 | a | alias)
  alias
  ;;
1 | u | upgrade)
  build
  restart
  ;;
2 | b | build)
  build
  ;;
3 | r | restart)
  restart
  ;;
4 | p | push)
  push
  ;;
5 | d | db)
  db $2
  ;;
6 | l | log)
  log $2 $3
  ;;
7 | tl | tlog | taillog)
  tail -F ${WORKDIR}/logs/${APP_NAME}.log
  ;;
*)
  app_name_alias="$(echo $APP_NAME | perl -pe 's/(-|_)//g')${APP_ENV}"
  echo "
# **********************************************************************************************************************
# 说明文档
# 左边是自定义快捷命令，可以执行 alias 获取快捷命令，注册到 ~/.bashrc 并执行 source ~/.bashrc ，就可以通过命令直接操作
# 右边是自定义快捷命令对应的完整命令，没注册自定义快捷命令时，需要完整命令操作服务
# **********************************************************************************************************************
【 ${app_name_alias}upgrade  : ${WORKDIR}/${APP_NAME}.sh upgrade            】：串行执行 build 、restart 命令；
【 ${app_name_alias}build    : ${WORKDIR}/${APP_NAME}.sh build              】：git pull 代码、编译 jar 包、构造 docker 镜像；
【 ${app_name_alias}restart  : ${WORKDIR}/${APP_NAME}.sh restart            】：重启 java 服务；
【 ${app_name_alias}push     : ${WORKDIR}/${APP_NAME}.sh push               】：推送镜像到其他服务器；
【 ${app_name_alias}log      : ${WORKDIR}/${APP_NAME}.sh log traceId 100    】：日志查看命令，可以查看多台服务器日志；
【 ${app_name_alias}taillog  : ${WORKDIR}/${APP_NAME}.sh taillog            】：实时日志滚动；
"
  ;;
esac
