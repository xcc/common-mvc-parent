#!/bin/bash
# echo "alias app-demo-om-upgrade='~/docker/nginx/app-demo.cccc6666.com.upgrade.sh'" >> ~/.bashrc && source ~/.bashrc
WORKDIR=~/docker/nginx
cd $WORKDIR
# 如果文件夹不存在，创建文件夹
if [ ! -d om-web ]; then
  git clone https://gitee.com/bm-projects/bm-web.git om-web
  cd om-web
  #git checkout dev
else
  cd om-web
fi
git fetch
git reset origin/master --hard
npm install
npm run build
rm -rf $WORKDIR/html/app-demo.cccc6666.com
mv dist $WORKDIR/html/app-demo.cccc6666.com
ls -l  $WORKDIR/html/app-demo.cccc6666.com
