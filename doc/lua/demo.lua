-- lua 脚本测试
print('Hello World：' .. _VERSION)
print(#('Hello World：' .. _VERSION))

print('-- 对象 ---------------------------------------------------------------------------------------------------------------')
local tableObject = {
    key = 'name',
    value = 'Conor'
}
tableObject.id = 1
print(tableObject)
for key, value in pairs(tableObject) do
    print(key .. " : " .. value)
end

print('-- 数组 ---------------------------------------------------------------------------------------------------------------')
local tableArray = { 'name', 'Conor' }
print(tableArray)
for key, value in pairs(tableArray) do
    print(key .. " : " .. value)
end

print('-- 函数 ---------------------------------------------------------------------------------------------------------------')
function call(arg)
    --if arg == nil then
    --    print('arg is nil')
    --else
    if arg == 0 then
        print('arg is false')
    else
        if arg == 1 then
            print('arg is true')
        else
            print('arg is unknow')
        end
    end
end
call(0)
