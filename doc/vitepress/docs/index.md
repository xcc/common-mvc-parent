
# 简介

教程地址： https://vitejs.cn/vitepress/

# 更新日志

## 2022-11-05

+ 自定义 starter，改造组件化依赖
  - `app-common-starter` 基础组件，工具类
  - `app-auto-task-starter` 定时任务组件；参考：[配置](/config/#spring-app-autotask)；依赖：`app-cache-starter`
  - `app-cache-starter` 缓存组件；参考：[配置](/config/#spring-app-cache)
  - `app-dao-jpa-starter` JPA+QueryDSL 数据库操作组件；参考：[配置](/config/#spring-app-encrypt-db)；依赖：`app-common-starter`
  - `app-encrypt-api-starter` API请求响应报文加解密组件；参考：[配置](/config/#spring-app-encrypt-api)；依赖：`app-common-starter`
  - `app-encrypt-domain-starter` 数据库实体对象属性解密组件；参考：[配置](/config/#spring-app-encrypt-db)；依赖：`app-common-starter`、`app-cache-starter`
  - `app-security-starter` spring security 安全配置组件；参考：[配置](/config/#spring-app-encrypt-pwd)；依赖：`app-common-starter`、`app-cache-starter`、`app-encrypt-domain-starter`
  - `app-sms-starter` 短信验证码组件；参考：[配置](/config/#smsconfiguration-java)；依赖：`app-cache-starter`
  - `app-swagger-starter` swagger API文档组件；参考：[配置](/config/#spring-app-swagger-enabled)
  - `app-nfs-disk-starter` nas或本地磁盘文件存储组件；参考：[配置](/config/#spring-app-nfs-disk)；依赖：`app-common-starter`
  - `app-nfs-alioss-starter` 阿里 oss 对象存储组件；参考：[配置](/config/#spring-app-nfs-oss)；依赖：`app-nfs-disk-starter`
  - `app-nfs-obs-starter` 华为 obs 对象存储组件；参考：[配置](/config/#spring-app-nfs-obs)；依赖：`app-nfs-disk-starter`
  - `app-nfs-minio-starter` minio 对象存储组件；参考：[配置](/config/#spring-app-nfs-minio)；依赖：`app-nfs-disk-starter`
  - `app-qrcode-starter` 二维码生成组件
  - `app-excel-starter` POI读写excel组件
  - `app-image-code-starter` 图片验证码组件；依赖：`app-common-starter`
+ 数据库实体对象新增返回字段 dv(数据版本号) 用于更新和删除，dv会拼接随机值，每次返回不一样，规避脏数据更新
+ 数据库实体对象 encryptId 逻辑优化，自定义 jackson 注解，通过注解自动加解密 id (返回的id为加密字符串)， 不再返回 encryptId；
  加密id对url友好，不需要 UrlEncoder 也可以直接在url中使用；
  - 加解密依赖配置项
    + `spring.app.encrypt.db.enabled` # 开关；true：是，false：否.
    + `spring.app.encrypt.db.secretKey` # 32 位密钥
    + `spring.app.encrypt.db.ivParameterSpec` # 固定 IV ， 16位。 设置固定 IV 之后同样内容每次加密结果一样。
  - DTO和实体对象自动加解密注解；注意：所有关联表的外键字段都需要加加解密注解
    + `@AesIdJsonConverter` 下面所有注解继承这个注解，用于缓存序列化忽略加解密
    + `@AesIdLongJsonConverter` 用法： `@AesIdLongJsonConverter private Long userId;` 返回加密字符串
    + `@AesIdLongArrayJsonConverter` 用法： `@AesIdLongArrayJsonConverter private Long[] userIds;` 数组中每一项都会单独加密，返回加密字符串数组
    + `@AesIdLongListJsonConverter` 用法： `@AesIdLongListJsonConverter private List<Long> userIds;` 集合中每一项都会单独加密，返回加密字符串数组
    + `@AesIdLongSetJsonConverter` 用法： `@AesIdLongSetJsonConverter private Set<Long> userIds;` 集合中每一项都会单独加密，返回加密字符串数组
    + `@AesIdStringJsonConverter` 用法： `@AesIdStringJsonConverter private String roleId;` 返回加密字符串
    + `@AesIdStringArrayJsonConverter` 用法： `@AesIdStringArrayJsonConverter private String[] roleIds;` 数组中每一项都会单独加密，返回加密字符串数组
    + `@AesIdStringListJsonConverter` 用法： `@AesIdStringListJsonConverter private List<String> roleIds;` 集合中每一项都会单独加密，返回加密字符串数组
    + `@AesIdStringSetJsonConverter` 用法： `@AesIdStringSetJsonConverter private Set<String> roleIds;` 集合中每一项都会单独加密，返回加密字符串数组
  - REST风格路径参数解密
    + `@AesPathVariable` 用法： `@AesPathVariable final Long userId` 例：`/{userId:前端传的加密id字符串}` => {userId}会执行解密逻辑并转换为 Long 类型
    + `@AesPathVariable(clazz = String.class)` 用法： `@AesPathVariable(clazz = String.class) final String roleId` 例：`/{roleId:前端传的加密id字符串}` => {userId}会执行解密逻辑并转换为 String 类型
    + `@AesPathVariable(clazz = DataVersion.class)` 用法： `@AesPathVariable(clazz = DataVersion.class) final DataVersion dv` 例：`/{dv:前端传的数据版本号加密字符串}` => {dv}会执行解密逻辑并转换为 DataVersion 类型
  - 查询参数解密
    + `@AesRequestParam(required = false)` 用法： `@AesRequestParam(required = false) final Long userId` 例：`/{userId:前端传的加密id字符串}` => {userId}会执行解密逻辑并转换为 Long 类型
    + `@AesRequestParam(required = false, clazz = String.class)` 用法： `@AesRequestParam(required = false, clazz = String.class) final String roleId` 例：`/{roleId:前端传的加密id字符串}` => {userId}会执行解密逻辑并转换为 String 类型
+ 查询参数不再使用通用对象接收，通用对象接收参数展示在 swagger 文档对前端不友好，接口需要 `@RequestParam` 按需暴露查询参数
  - `@ApiParam` swagger 注解字段说明 
  - `@RequestParam(required = false)` 非加密字段查询参数接收
  - `@AesRequestParam(required = false)` 加密字段查询参数接收
  - `@AesRequestParam(required = false, clazz = String.class)` 加密字段查询参数接收
+ 修复文件上传、图片裁剪bug
+ 文件存储配置项删除，application.yml 不再需要定义文件存储目录，直接取存储对象类名作为存储目录；参考：[配置](/config/#spring-app-nfs-type)
+ 集成华为OBS
+ 子线程自动填充 traceId ，不再需要 `MDC.getCopyOfContextMap();` 手动设置子线程上下文
+ 修改 nginx 配置文件
+ 埋点记录请求域名和用户真实ip
+ 补充 shell 脚本，`doc/docker/app-demo/app-demo.sh` 包含 docker 镜像构造、应用启停、日志抓取等
+ 新增docker启动frp客户端 `doc/docker/frpc`，建议每个项目单独启动frp服务，多个项目共享frp一旦其中一个项目编辑frp文件错误，会导致其他项目不可用
+ 代码模板优化，适配最新架构
+ 新增账号销毁接口
+ 响应编码规范调整；Code.java 全局响应码字符 A 前缀；AppCode.java 应用级响应码字符 B 前缀，微服务情况保证每个服务级的响应编码前缀不一样，跨服务调用时容易区分报错来源于哪个服务  
+ 应用静态资源yml文件配置，服务启动时写入文件存储对象；配置项：`spring.app.static.list` 

## 2022-07-23

+ 修复bug， jackson 日期区间参数对象反序列化报错
+ cli.sh 脚本优化
+ docker-compose 配置修改

## 2022-07-15

+ 去掉 @Transactional(readOnly = true), 会导致连接池消耗过快
+ 数据库 @Convert 实现优化
+ token 延期规则调整，优化性能
+ Code 规范修改
+ 用户默认角色 bug 修复
+ 修改角色表数据初始化脚本
+ 新增配置 spring.data.redis.repositories.enabled=false

## 2022-07-09

+ springboot 版本升级 2.7
+ spring security 废弃api升级，使用最新版本配置鉴权
+ 移除 spring cache， 使用 redisson 操作缓存， 优化批处理操作
+ liquibase 数据库脚本管理插件集成
+ 移除 fastjson， 使用 jackson 替代
+ 代码生成模板修改，后端格式化日期
+ 修改应用文件存储配置项 `spring.app.files.*` => `spring.app.nfs.*`
+ 文件上传优化，新增重载方法
+ 上传图片压缩、裁剪
+ 删除配置项 `spring.app.encrypt.api.ivRandomRule`
+ token 生成密文长度优化
+ 登录 TokenClient 调整， 改造登录接口和鉴权配置， 优化登录缓存和 token 缓存
+ encryptId 加密 id 密文长度优化
+ 数据库加密密文长度优化
+ swagger配置调整，新增注解分组接口
+ 埋点支持注解配置，通过 SPEL 获取参数写入埋点表
+ 异常日志捕获优化，写入异常日志表
+ AOP 拦截打印方法执行路径和入参日志格式调整
+ doc 目录新增常用的 docker-compose.yml
+ gulp获取国家统计局城市代码，整理成csv文件
+ 修复 thumbnailator 插件无法处理 cmyk 颜色
+ 自定义有序id生成规则 `com.common.db.generator.Base36Generator.java`
+ springboot banner 替换成二维码
