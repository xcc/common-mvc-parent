# 规范

基本参考阿里规约，有部分不一样的地方下面会另做说明  
https://mrkaorou.github.io/2018/07/25/%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4Java%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C-%E7%BB%88%E6%9E%81%E7%89%88/%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4Java%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C-%E7%BB%88%E6%9E%81%E7%89%88.pdf

### @Slf4j 日志输出规范
```java
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * 日志输出规范
 */
@Slf4j
public class LogTest {

    @SneakyThrows
    public static void main(String[] args) {
        // *************************************************************************************************************
        // ************************************************* 日志打印原则 ************************************************
        // + 默认已经打印了方法执行路径的参数，不需要为方法入参再加日志打印
        // + if判断前面一定要打印日志，方便通过日志分析进入了哪个代码分支
        // + 集合操作前要打印集合元素数量
        // + 重要业务代码执行过程一定要添加日志
        // *************************************************************************************************************

        if (log.isDebugEnabled()) { // 添加日志级别判断， 用于控制日志输出粒度
            log.debug("DEBUG 级别日志输出");
            log.debug("DEBUG 级别日志输出，带参数占位符。字符串：{}，数字：{}，对象（默认执行toString方法）：{}", "参数1", 2, new Item().setKey("name").setValue("Conor Xie"));
        }
        if (log.isInfoEnabled()) { // 添加日志级别判断， 用于控制日志输出粒度
            log.info("INFO 级别日志输出");
            log.info("INFO 级别日志输出，带参数占位符。字符串：{}，数字：{}，对象（默认执行toString方法）：{}", "参数1", 2, new Item().setKey("name").setValue("Conor Xie"));
        }
        String key = "a";
        log.info("key={}", key); // if判断前面一定要打印日志，方便通过日志分析进入了哪个代码分支
        if (Objects.equals(key, "A")) {
        } else if (Objects.equals(key, "B")) {
        } else {
        }
        try {
            Integer.parseInt("abc");
        } catch (Exception e) {
            // e.printStackTrace(); 禁止使用这种方式输出日志，这种日志不会记录到日志文件，而且会导致内存泄露
            log.error(e.getMessage(), e); // 使用 log.error 第一个参数必须是字符串，最后一个参数必须把 Exception 对象传进去，日志文件才能记录堆栈信息
            // 带占位符的异常日志，最后一个参数也一定是 Exception 且不需要加占位符
            log.error("ERROR 级别日志输出，带参数占位符。字符串：{}，数字：{}，对象（默认执行toString方法）：{}", "参数1", 2, new Item().setKey("name").setValue("Conor Xie"), e);
        }
    }

    @Getter
    @Setter
    @ToString
    @Accessors(chain = true)
    public static class Item {
        private String key;
        private String value;
    }
}
```
