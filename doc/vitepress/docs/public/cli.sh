#!/usr/bin/env bash
# **********************************************************************************************************************
# 一键初始化项目
# **********************************************************************************************************************
# cp -r cli.sh ../ && rm -rf app-* main-module demo-service && git reset origin/master --hard && cp -r ../cli.sh ./

rm -rf code-template
# 克隆代码模板
git clone https://gitee.com/xcc/common-mvc-parent.git code-template
# 进入代码模板目录
cd code-template
GIT_COMMIT_ID=$(git reflog | head -n 1 | awk '{print $1}')
# 基于代码模板检出无提交记录的新分支
git checkout --orphan clone-master

# 应用名，项目名
APP_NAME=app-demo
read -p "$(echo -e "应用名 spring.app.name=(默认值：$APP_NAME): ")" INPUT
if [[ $INPUT != '' ]]; then
  APP_NAME=$INPUT
fi
AppName=$(echo $APP_NAME | perl -pe 's/(^|-|_)(\w)/\U\2/g')
AppDir=$(echo $APP_NAME | perl -pe 's/(-|_)/\//g')

# git 仓库地址
GIT_REPOSITORY=''
read -p "$(echo -e "新项目 git 仓库地址=: ")" INPUT
if [[ $INPUT != '' ]]; then
  GIT_REPOSITORY=$INPUT
fi

# git 分支
GIT_BRANCH=master
read -p "$(echo -e "新项目 git 仓库分支=(默认值：$GIT_BRANCH): ")" INPUT
if [[ $INPUT != '' ]]; then
  GIT_BRANCH=$INPUT
fi

# 项目包名前缀
MAVEN_GROUP_ID=com.$(echo $APP_NAME | perl -pe 's/(-|_)/./g')
read -p "$(echo -e "pom 文件 groupId=(默认值：$MAVEN_GROUP_ID): ")" INPUT
if [[ $INPUT != '' ]]; then
  MAVEN_GROUP_ID=$INPUT
fi

#
MAVEN_ARTIFACT_ID=$(echo $APP_NAME | perl -pe 's/_/-/g')-parent
read -p "$(echo -e "pom 文件 artifactId=(默认值：$MAVEN_ARTIFACT_ID): ")" INPUT
if [[ $INPUT != '' ]]; then
  MAVEN_ARTIFACT_ID=$INPUT
fi

# 数据库地址
DB_HOST=localhost
read -p "$(echo -e "数据库地址=(默认值：$DB_HOST): ")" INPUT
if [[ $INPUT != '' ]]; then
  DB_HOST=$INPUT
fi

# 数据库端口
DB_PORT=3306
read -p "$(echo -e "数据库端口=(默认值：$DB_PORT): ")" INPUT
if [[ $INPUT != '' ]]; then
  DB_PORT=$INPUT
fi

# 数据库名称
DB_NAME=$(echo $APP_NAME | perl -pe 's/-/_/g')_dev_db
read -p "$(echo -e "数据库名=(默认值：$DB_NAME): ")" INPUT
if [[ $INPUT != '' ]]; then
  DB_NAME=$INPUT
fi

# 数据库账号
DB_USERNAME=root
read -p "$(echo -e "数据库用户名=(默认值：$DB_USERNAME): ")" INPUT
if [[ $INPUT != '' ]]; then
  DB_USERNAME=$INPUT
fi

# 数据库密码
DB_PASSWORD=111111
read -p "$(echo -e "数据库密码=(默认值：$DB_PASSWORD): ")" INPUT
if [[ $INPUT != '' ]]; then
  DB_PASSWORD=$INPUT
fi

echo "
# 初始化参数 *************************************************************************************************************
spring.app.name=$APP_NAME                  # 应用名
git.repository=$GIT_REPOSITORY             # 新项目 git 仓库地址
git.branch=$GIT_BRANCH                     # 新项目 git 仓库分支
pom.xml.groupId=$MAVEN_GROUP_ID            # pom 文件 groupId (应用包名前缀)
pom.xml.artifactId=$MAVEN_ARTIFACT_ID      # pom 文件 artifactId
spring.datasource.host=$DB_HOST            # 数据库地址
spring.datasource.port=$DB_PORT            # 数据库端口
spring.datasource.dbname=$DB_NAME          # 数据库名
spring.datasource.username=$DB_USERNAME    # 数据库用户名
spring.datasource.password=$DB_PASSWORD    # 数据库密码
# **********************************************************************************************************************
"

read -p "$(echo -e "按 enter 键继续（ctrl - c 退出） ")" INPUT
echo '开始初始化代码'

#find ./ -name querydsl | xargs rm -rf
find ./ -name querydsl -exec rm -rf {} + # 清除 querydsl 编译出来的文件
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/AppDemo/$AppName/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/app-demo/$APP_NAME/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/com.ccx.demo/$MAVEN_GROUP_ID/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/common-mvc-parent/$MAVEN_ARTIFACT_ID/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/localhost:3306/$DB_HOST:$DB_PORT/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/app_demo_dev_db/$DB_NAME/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/username: root/username: $DB_USERNAME/g"
find ./ -type f -regex '.*\.\(java\|xml\|js\|yml\|yaml\|properties\|json\|http\|sql\|conf\|ini\|env\|sh\|md\|factories\)$' | grep -v git | grep -v idea | grep -v logs  | grep -v node_modules | grep -v target | grep -v cli.sh | xargs perl -pi -e "s/password: 111111/password: $DB_PASSWORD/g"

mv app-demo/src/main/java/com/ccx/demo/AppDemoApplication.java app-demo/src/main/java/com/ccx/demo/${AppName}Application.java
mkdir -p app-demo/src/main/java/com/$AppDir
mv app-demo/src/main/java/com/ccx/demo/* app-demo/src/main/java/com/$AppDir/
mv app-demo $APP_NAME

mkdir -p app-starter/src/main/java/com/$AppDir/
mv app-starter/src/main/java/com/ccx/demo/* app-starter/src/main/java/com/$AppDir/
mkdir -p gateway/src/main/java/com/$AppDir
mv gateway/src/main/java/com/ccx/demo/* gateway/src/main/java/com/$AppDir/
mkdir -p app-code/service/src/main/java/com/$AppDir
mv app-code/service/src/main/java/com/ccx/demo/* app-code/service/src/main/java/com/$AppDir/
mkdir -p app-base/api/src/main/java/com/$AppDir
mv app-base/api/src/main/java/com/ccx/demo/* app-base/api/src/main/java/com/$AppDir/
mkdir -p app-base/service/src/main/java/com/$AppDir
mv app-base/service/src/main/java/com/ccx/demo/* app-base/service/src/main/java/com/$AppDir/
mkdir -p app-user/api/src/main/java/com/$AppDir
mv app-user/api/src/main/java/com/ccx/demo/* app-user/api/src/main/java/com/$AppDir/
mkdir -p app-user/service/src/main/java/com/$AppDir
mv app-user/service/src/main/java/com/ccx/demo/* app-user/service/src/main/java/com/$AppDir/
mkdir -p examples/app-quick/src/main/java/com/$AppDir
mv examples/app-quick/src/main/java/com/ccx/demo/* examples/app-quick/src/main/java/com/$AppDir/
mkdir -p examples/app-rabbitmq/src/main/java/com/$AppDir
mv examples/app-rabbitmq/src/main/java/com/ccx/demo/* examples/app-rabbitmq/src/main/java/com/$AppDir/

mv doc/docker/app-demo/app-demo.sh doc/docker/app-demo/$APP_NAME.sh
mv doc/docker/app-demo doc/docker/$APP_NAME
mv doc/docker/nginx/conf.d/app-demo.cccc6666.com.conf doc/docker/nginx/conf.d/$APP_NAME.cccc6666.com.conf
mv doc/docker/nginx/app-demo.cccc6666.com.upgrade.sh doc/docker/nginx/$APP_NAME.cccc6666.com.upgrade.sh

#mkdir -p demo-service/src/main/java/com/$AppDir
#mv demo-service/src/main/java/com/ccx/demo/* demo-service/src/main/java/com/$AppDir/

#mkdir -p main-module/src/main/java/com/$AppDir
#mv main-module/src/main/java/com/ccx/demo/* main-module/src/main/java/com/$AppDir/

# mkdir -p wechat-service/src/main/java/com/$AppDir mv wechat-service/src/main/java/com/ccx/demo/* wechat-service/src/main/java/com/$AppDir/

rm -rf backup demo-service main-module common-utils common-support app-sql-querydsl wechat-service replace.sh

# 提交代码， mvn compile 需要 git commit 记录，所以需要先提交一次
git add . && git commit -m "初始化$GIT_COMMIT_ID"

mvn clean compile -P querydsl

if [[ $GIT_REPOSITORY == '' ]]; then
  echo '未指定git仓库地址，无法自动推送代码'
else
  # 提交代码
  git add . && git commit -m "初始化QueryDSL查询实体"
  # 添加新项目仓库地址
  git remote add code-origin $GIT_REPOSITORY
  # 推送到指定分支， 本地分支:远程分支
  git push --set-upstream code-origin clone-master:$GIT_BRANCH
fi

#find ./ -name \*.iml | xargs rm -rf
#rm -rf .idea

# 回到上级目录
cd ..
# 清理 clone 的代码模板
rm -rf code-template

