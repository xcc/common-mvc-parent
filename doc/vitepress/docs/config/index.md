# 配置

应用相关的配置说明，所有的配置都可以在项目启动时通过通过环境变量覆盖 yml 文件内的配置。 例： yml 文件中： `spring.app.cors.enabled=false` ，
对应的启动环境变量 `java -Dspring.app.cors.enabled=false -jar app.jar`

## 目录

[[toc]]

## Configuration

### ApplicationConfiguration.java 
项目全局配置
`ApplicationConfiguration#exception` 全局异常捕获，异常消息写入 `tab_error_log` 表，将异常转化为自定义编码异常。

### AppConfiguration.java
服务级配置

### CommonConfiguration.java
服务基本配置信息

`CommonConfiguration#id` 服务id，启动时由 redisson 生成

`CommonConfiguration#env` 当前环境：[local:本地开发环境|dev:远程开发环境|sit:系统测试|uat:验收测试|prod:生产]

`CommonConfiguration#domain` 应用域名

`CommonConfiguration#hostIp` 主机ip

`CommonConfiguration#hostName` 主机名

`CommonConfiguration#name` 应用名

`CommonConfiguration#version` 当前运行版本号: '@version@'

`CommonConfiguration#buildTime` 项目编译打包时间，发布完成之后检查打包时间是否是一致的，避免版本更新失败: '@build.time@'

`CommonConfiguration#gitCommitId` git 提交记录 id 前 8 位: '@git.commit.id.abbrev@'

`CommonConfiguration#startTime` 项目启动时间

`CommonConfiguration#objectMapper` api 请求响应 jackson 数据序列化和反序列化规则定义

`CommonConfiguration#multiThreadExecutorService` 多线程 bean 配置初始化。

`CommonConfiguration#singleThreadExecutorService` 单线程 bean 配置初始化。

### SecurityConfiguration.java

Spring Security 权限控制。  
中文文档：https://springcloud.cc/spring-security-zhcn.html  
官方文档：https://docs.spring.io/spring-security/site/docs/current/reference/html5/

`@EnableWebSecurity` 开启 Security 权限控制

`@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)`  
`securedEnabled = true` 启用注解：@Secured； [@Secured("ROLE_USER"), @Secured("IS_AUTHENTICATED_ANONYMOUSLY")]  
`prePostEnabled = true` 启用注解：@PreAuthorize； [@PreAuthorize("hasAuthority('ROLE_USER')"), @PreAuthorize("isAnonymous()")]

`SecurityConfiguration#corsConfigurationSource` 跨域配置， `spring.app.cors.enabled=true` 时跨域才生效。

`AppConfiguration#StaticAssetConfiguration` 静态资源映射配置，swagger 页面、
druid页面等；这里配置映射之后，需要在 `SecurityConfiguration#staticSecurityFilterChain` 配置白名单放过静态资源。

`AppConfiguration#staticSecurityFilterChain` 静态资源白名单， 配合 `AppConfiguration#StaticAssetConfiguration` 一起使用。

`SecurityConfiguration#tokenOpenSecurityFilterChain` 开放接口白名单，无需登录授权，所有使用 `/open/`
前缀的接口不需要登录就能访问，如果已登录的用户访问 `/open/`，会将 token 解析得到的用户信息放到 `app.security.starter.vo.TokenUserVO` 对象， Controller
方法可以注入 `@ApiIgnore @AuthenticationPrincipal IUser user` 对象，该对象只包含 token 中携带的用户信息，不会包含全量的用户信息。

`SecurityConfiguration#feignSecurityFilterChain` feign服务调用拦截，填充用户信息

`SecurityConfiguration#loginSecurityFilterChain` 封闭接口，需登录授权，`com.ccx.demo.enums.TokenClient`
定义了所有客户端，一个用户在每个客户端有且仅有一个有效 `x-toekn`。登录成功之后响应头会返回 `x-token` ，客户端从响应头获取到 `x-token`
之后写入本地缓存，后面每次发送 http 请求时，需要将 `x-token` 加入请求头。`authTokenFilter` 会从请求头提取 `x-token` 验证 token 是否有效，如果 token 无效，则会响应 `401`
，客户端收到 `401` 状态之后需要重新执行登录。

`SecurityConfiguration#LoginUserAuthenticationToken` token 认证用户授权对象

`SecurityConfiguration#rsaBCryptPasswordEncoder` api接口用户登录、注册密码 `RSA` 解密，`spring.app.encrypt.pwd.enabled=true`
时生效。_注意:这里的 `RSA` 不是数据库存储加密，数据库存储加密依然使用`org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder`_

### ScheduleConfiguration.java

定时任务配置初始化。  
`ScheduleConfiguration#threadPoolTaskScheduler` 定义定时任务执行线程池。  
`ScheduleConfiguration#scheduledExecutorService` 延时任务线程池。
`SchedulingConfigurerService#configureTasks` 注册定时任务并触发启动时需要执行一次的任务。

### RedissonCacheConfiguration.java
RedissonClient 写入应用信息检查 redis 是否可用

### JPAConfiguration.java
JPA 集成 QueryDSL 配置。  
`@EnableJpaRepositories(basePackages = {"com.ccx.demo.**.dao.jpa"})` 指定扫描包路径。  
`JPAConfiguration#jpaQueryFactory` 初始化 QueryDSL 查询对象
`JPAConfiguration#getQueryFactory` 暴露公共静态方法，用于DAO层查询

JAP 参考配置：https://docs.spring.io/spring-data/jpa/docs/current/reference/html/  
JAP Hibernate 命名策略可自定义，参考配置：https://www.baeldung.com/hibernate-naming-strategy  
JAP QueryDSL 参考配置：http://www.querydsl.com/static/querydsl/latest/reference/html_single/#jpa_integration  
BlazeQueryDSL 参考配置：https://persistence.blazebit.com/documentation/1.5/core/manual/en_US/index.html#querydsl-integration  
QueryDSL SQL Spring: 参考配置：http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html  
Spring Data MongoDB 配置, AbstractMongoConfiguration 已经默认实现了
mongoTemplate和mongoFactoryBean，参考配置：https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference  
Spring Data MongoDB QueryDSL 参考配置： http://www.querydsl.com/static/querydsl/latest/reference/html_single/#mongodb_integration

### DbEncryptConfiguration.java
数据库敏感字段加解密配置初始化，参考：[配置属性](/config/#spring-app-encrypt-db)

### DomainEncryptConfiguration.java
数据实体对象 id 和 数据版本加解密配置，依赖 `DbEncryptConfiguration.java` 的配置属性；参考：[配置属性](/config/#spring-app-encrypt-db)

`DomainEncryptConfiguration#postConstruct` 全局jackson对象和缓存jackson对象注册忽略加解密注解
`DomainEncryptConfiguration#addArgumentResolvers` 注册 Controller 参数解密转换器

### ApiEncryptConfiguration.java
api 请求响应数据包加解密，`spring.app.encrypt.api.enabled=true` 时生效。；参考：[配置属性](/config/#spring-app-encrypt-api)


`ApiEncryptConfiguration#AesApiEncryptResponseBodyAop` 响应结果为 `Result<T>` 包装对象时执行加密逻辑。
`ApiEncryptConfiguration#AesApiDecryptRequestBodyAop` 请求参数解密， 只有 POST PUT DELETE PATCH 请求，且 body 不为空的情况才需要解密。 上传文件也不会走解密逻辑

### NfsConfiguration.java
NAS或本地磁盘存储；参考：[配置属性](/config/#spring-app-nfs-disk)

### NfsOssAliConfiguration.java
阿里 oss 对象存储，`spring.app.nfs.oss.enabled=true` 时生效，依赖`NfsConfiguration.java`，覆盖本地磁盘存储实现逻辑；参考：[配置属性](/config/#spring-app-nfs-oss)

### NfsObsHuaWeiConfiguration.java
华为 obs 对象存储，`spring.app.nfs.obs.enabled=true` 时生效，依赖`NfsConfiguration.java`，覆盖本地磁盘存储实现逻辑；参考：[配置属性](/config/#spring-app-nfs-obs)

### NfsMinioConfiguration.java
minio 对象存储，`spring.app.nfs.minio.enabled=true` 时生效，依赖`NfsConfiguration.java`，覆盖本地磁盘存储实现逻辑；参考：[配置属性](/config/#spring-app-nfs-minio)

### SwaggerConfiguration

Swagger + Knife4j 接口文档配置， `spring.app.swagger.enabled=true` 时生效。  
`@EnableSwagger2` 开启 swagger 文档。  
`@EnableKnife4j` 开启 Knife4j ui。  
Knife4j 官方文档： https://doc.xiaominfo.com   
Knife4j demo： https://gitee.com/xiaoym/swagger-bootstrap-ui-demo.git

### SmsConfiguration.java
短信发送；参考：[配置属性](/config/#spring-app-sms)

### FeignConfiguration.java

### RabbitMqConfiguration.java

## 配置属性

### spring.profiles.active

系统运行环境配置，加载不同环境的配置文件。默认值：local。 application-${spring.profiles.active}.yml。 环境变量`--spring.profiles.active=prod`

| 选项    |说明|
|-------|----|
| local |本地开发环境（默认）|
| dev   |远程开发环境|
| sit   |系统测试环境|
| uat   |验收测试环境|
| prod  |生产环境|

### spring.application.name

应用名。
### spring.app.domain

应用域名，目前没使用。

### spring.app.version

当前运行版本号，取值根 pom -> version。
### spring.app.buildTime

项目编译打包时间，发布完成之后检查打包时间是否是一致的，避免版本更新失败。
### spring.app.gitCommitId

git 提交记录前 8 位，项目打包发布之后可以通过 /version 接口检查是是否同 git 一致。

### jasypt.encryptor.password

配置文件加密密钥。 环境变量`-Djasypt.encryptor.password=6782bc11b94c4c788e4b731471e6c103`。
建议新项目初始化之后立即修改密钥，且为不同环境配置不同的密钥。密钥加解密测试类`JasyptTest.java`

### spring.app.cors.enabled

是否开启跨域开关。默认值：false。

| 选项    |说明|
|-------|----|
| true  |开|
| false |关（默认）|

### spring.app.verifyCodeEnabled

是否开启图片验证码校验，是否开启手机验证码校验。默认值：true。
开发环境关闭该开关可以用任意验证码登录，一般用于自动化测试和开发环境，生产环境请启用该配置 true：是，false：否.。
环境变量`-Dspring.app.verifyCodeEnabled=false`

| 选项    |说明|
|-------|----|
| true  |开（默认）|
| false |关|

### spring.app.swagger.enabled

是否开启 swagger 文档。默认值：false。

| 选项    |说明|
|-------|----|
| true  |开|
| false |关（默认）|

### spring.servlet.multipart

文件上传配置

| 选项                                           | 说明                               |
|----------------------------------------------|----------------------------------|
| spring.servlet.multipart.enabled             | 文件上传开关(true:开，false:关)，默认值：false |
| spring.servlet.multipart.max-file-size       | 最大上传文件大小，参考值： 10MB               |
| spring.servlet.multipart.max-request-size    | 请求数据包大小，参考值： 10MB                |
| spring.servlet.multipart.file-size-threshold | 指定文件大小超过该值之后缓存到临时文件，参考值： 2MB     |

## spring.app.encrypt

### spring.app.encrypt.pwd

登录、注册api接口密码传输加密配置。

| 配置项                               |说明|
|-----------------------------------|----|
| spring.app.encrypt.pwd.enabled    |是否开启用户登录密码加解密，加密方式由`spring.app.encrypt.pwd.type`，目前只支持rsa。默认值：true。环境变量`-Dspring.app.encrypt.pwd.enabled=true`。|
| spring.app.encrypt.pwd.type       |指定用户登录密码加解密方式，目前只支持rsa。默认值：rsa。环境变量`-Dspring.app.encrypt.pwd.type=rsa`。|
| spring.app.encrypt.pwd.privateKey |RSA私钥，`spring.app.encrypt.pwd.type=rsa`时必须指定私钥。建议新项目初始化之后立即修改私钥、公钥，且为不同环境配置不同的私钥、公钥。RSA加解密测试类`Rsa.java` ，该类可生成公钥、私钥，并写入logs目录。环境变量`-Dspring.app.encrypt.pwd.privateKey=''`。|
| spring.app.encrypt.pwd.publicKey  |RSA公钥，`spring.app.encrypt.pwd.type=rsa`时必须指定公钥。建议新项目初始化之后立即修改私钥、公钥，且为不同环境配置不同的私钥、公钥。RSA加解密测试类`Rsa.java` ，该类可生成公钥、私钥，并写入logs目录。环境变量`-Dspring.app.encrypt.pwd.publicKey=''`。|

spring.app.encrypt.pwd.enabled

| 选项    |说明|
|-------|----|
| true  |开（默认）|
| false |关|

spring.app.encrypt.pwd.type

|选项|说明|
|----|----|
|rsa|RSA加密方式|

### spring.app.encrypt.api

api 接口请求参数和响应数据包加解密配置。

| 选项                                     |说明|
|----------------------------------------|----|
| spring.app.encrypt.api.enabled         |是否开启 api 请求数据加解密，加解密方式：AES。默认值：true。 用于前后端数据交互密文传输报文， 目前只有POST、PUT、PATCH、DELETE接口有请求体的情况才会解密， 返回报文只有结构是`Result.java` 包装的类才会加密。环境变量`-Dspring.app.encrypt.api.enabled=true`。|
| spring.app.encrypt.api.secretKey       |api 接口数据加解密密钥。建议新项目初始化之后立即修改加解密密钥，且为不同环境配置不同密钥。RSA加解密测试类`AesApi.java`。 环境变量`-Dspring.app.encrypt.api.secretKey=''`。|
| spring.app.encrypt.api.ivParameterSpec |api 接口数据加解密固定IV，固定长度 16 位字符，长度不是16位会抛异常。使用固定IV时，相同的报文会得到相同的密文。 环境变量`-Dspring.app.encrypt.api.ivParameterSpec=''`。|
| ~~spring.app.encrypt.api.signType~~    |~~数据包签名，功能暂未开放。 环境变量`-Dspring.app.encrypt.api.signType=MD5`。~~|

spring.app.encrypt.api.enabled

| 选项    |说明|
|-------|----|
| true  |开（默认）|
| false |关|

### spring.app.encrypt.db

数据库敏感字段加解密配置。

| 配置项                                   |说明|
|---------------------------------------|----|
| spring.app.encrypt.db.enabled         |数据库敏感字段存储加解密，加解密方式：AES。默认值：true 。环境变量`-Dspring.app.encrypt.api.enabled=true`。|
| spring.app.encrypt.db.secretKey       |数据库敏感字段存储加解密密钥，加解密方式：AES。默认值：true，敏感字段需要在方法头上加注解`@Convert(converter = AesDbConvert.class)`，只有加了该注解的字段才会在入库时加密存储。 建议新项目初始化之后立即修改加解密密钥，且为不同环境配置不同密钥。RSA加解密测试类`AesDbConvert.java`。 环境变量`-Dspring.app.encrypt.db.secretKey=''`。|
| spring.app.encrypt.db.ivParameterSpec |AES加密静态IV，固定长度 16 位字符，如果不使用静态IV，则每次加密密文不一样，无法搜索。 环境变量`-Dspring.app.encrypt.db.secretKey=''`。|

spring.app.encrypt.db.enabled

| 选项    |说明|
|-------|----|
| true  |开（默认）|
| false |关|

## spring.app.autoTask

### spring.app.autoTask.enabled

定时任务总开关，开启后 `spring.app.autoTask.services` 定义的定时任务服务才会注册。 默认值：true。

| 选项    |说明|
|-------|----|
| true  |开（默认）|
| false |关|

### spring.app.autoTask.services

定时任务服务列表，定时任务总开关`spring.app.autoTask.enabled=true`打开时才会注册定时任务服务。 服务注册配置类`com.ccx.demo.config.ScheduleConfiguration`。
定时任务服务必须实现 `com.utils.IAutoTask` 接口。

| 配置项                                                                       |说明|
|---------------------------------------------------------------------------|----|
| spring.app.autoTask.services.${springBeanNameAutoTaskService}.enabled     |${springBeanNameAutoTaskService} 服务定时任务开关，触发时执行 `com.utils.IAutoTask#call()`。 默认值：false。|
| spring.app.autoTask.services.${springBeanNameAutoTaskService}.started     |${springBeanNameAutoTaskService} 服务启动时执行一次 `com.utils.IAutoTask#started()` 方法开关。受分布式锁约束，如果分布式锁开关打开则只有一个服务能触发 `started` 方法。默认值：false。|
| spring.app.autoTask.services.${springBeanNameAutoTaskService}.comment     |${springBeanNameAutoTaskService} 定时任务说明。|
| spring.app.autoTask.services.${springBeanNameAutoTaskService}.cron        |${springBeanNameAutoTaskService} 定时任务执行频率，表达式参考`org.springframework.scheduling.annotation.Scheduled#cron()`。|
| spring.app.autoTask.services.${springBeanNameAutoTaskService}.lockEnabled |${springBeanNameAutoTaskService} 分布式锁开关，基于 redisson 集成分布式锁。打开后只有获取到锁的服务才会执行定时任务逻辑。默认值：false。|
| spring.app.autoTask.services.${springBeanNameAutoTaskService}.lockExpired |${springBeanNameAutoTaskService} 分布式锁锁定时间，基于 redisson 集成分布式锁，`call`方法执行完成后并不会立即释放锁， 必须等到 `lockExpired` 设置的时间到了才会解锁。|

spring.app.autoTask.services.${springBeanNameAutoTaskService}.cron

| 表达式            |说明|
|----------------|----|
| 0/10 * * * * ? |10秒钟执行一次|
| 0 0 * * * ?    |每小时执行一次|
| 0 0 0 * * ?    |每天 00:00:00 执行一次|
| 0 0 0/2 * * ?  |间隔两小时执行一次|
| 0 0 0 1 * ?    |每月 1 日 00:00:00 执行一次|

spring.app.autoTask.services.${springBeanNameAutoTaskService}.lockExpired

| 时间 | 说明  |
|-----|-----|
| 1s  | 1秒  |
| 1m  | 1分钟 |
| 1h  | 1小时 |
| 1d  | 1天  |

## spring.app.nfs

### ~~spring.app.nfs.type~~

~~应用文件存储方式。 环境变量`-Dspring.app.nfs.type=disk`。~~

|选项|说明|
|----|----|
|[disk](./#spring-app-nfs-disk)|<b>默认</b>，本地地盘（nas）存储|
|[alioss](./#spring-app-nfs-alioss)|阿里oss存储|
|[minio](./#spring-app-nfs-minio)|minio分布式文件存储|

### spring.app.nfs.disk

| 配置项                           |说明|
|-------------------------------|----|
| spring.app.nfs.disk.directory |应用文件根目录绝对路径，注意需要 “/” 结尾， 参考值： /app/nas/|
| spring.app.nfs.disk.server    |文件访问根路径，注意需要 “/” 结尾，参考值： https://nas.domain.com/app/nas/|

### spring.app.nfs.oss

| 配置项                              | 说明                                                                                                                                                                                       |
|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| spring.app.nfs.oss.directory  | **使用阿里 oss 时这里必须设置为 '' 空字符传，因为文件存储在 bucketName**                                                                                                                                         |
| spring.app.nfs.oss.server     | 文件访问地址，注意需要 “/” 结尾。图片访问如果变成下载时，注意看这里：https://help.aliyun.com/document_detail/142631.html。 默认值： https://${spring.app.nfs.oss.bucketName}.${spring.app.nfs.oss.region}.aliyuncs.com/ |
| spring.app.nfs.oss.region     | oss区域， 参考值：oss-cn-shanghai                                                                                                                                                               |
| spring.app.nfs.oss.bucketName | 存储桶名称， 注： 阿里云 OSS 的 bucket 必须是全局唯一，不能跟其他用户重叠，最好在阿里云控制台提前建好。 参考值：app-name-oss                                                                                                             |
| spring.app.nfs.oss.external   | 外网节点: https://help.aliyun.com/document_detail/31837.html。 默认值：${spring.app.nfs.oss.region}.aliyuncs.com                                                                               |
| spring.app.nfs.oss.internal   | ecs 内网节点: https://help.aliyun.com/document_detail/31837.html，内网使用该节点访问速度更快。 默认值：${spring.app.nfs.oss.region}-internal.aliyuncs.com                                                    |
| spring.app.nfs.oss.endpoint   | 当前使用节点，默认值： ${spring.app.nfs.oss.external}.                                                                                                                                           |
| spring.app.nfs.oss.sts        | 授权 token，用于生成客户端访问token                                                                                                                                                                  |
| spring.app.nfs.oss.accessKey  | 授权密钥， 阿里云访问控制台申请： https://ram.console.aliyun.com/users                                                                                                                                   |
| spring.app.nfs.oss.secretKey  | 操作密钥， 阿里云访问控制台申请： https://ram.console.aliyun.com/users                                                                                                                                   |

### spring.app.nfs.obs

| 配置项                           | 说明                                                                                                                                    |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| spring.app.nfs.obs.directory  | **使用华为 obs 时这里必须设置为 '' 空字符传，因为文件存储在 bucketName**                                                                                      |
| spring.app.nfs.obs.server     | 文件访问地址，注意需要 “/” 结尾。图片访问如果变成下载时，注意OBS CDN 或 自定义访问域名， 如果不使用自定义域名，图片和 html 访问会变成下载                                                       |
| spring.app.nfs.obs.region     | obs区域， 参考值：https://developer.huaweicloud.com/endpoint?OBS                                                                                                            |
| spring.app.nfs.obs.bucketName | 存储桶名称， 注： 阿里云 OSS 的 bucket 必须是全局唯一，不能跟其他用户重叠，最好在阿里云控制台提前建好。 参考值：app-name-oss                                                          |
| spring.app.nfs.obs.endpoint   | 当前使用节点，https://developer.huaweicloud.com/endpoint?OBS                                                                                        |
| spring.app.nfs.obs.accessKey  | 授权密钥， 我的凭证 => 访问密钥                                                                                |
| spring.app.nfs.obs.secretKey  | 操作密钥， 我的凭证 => 访问密钥                                                                                |

### spring.app.nfs.minio

| 配置项                             | 说明                                                               |
|---------------------------------|------------------------------------------------------------------|
| spring.app.nfs.minio.directory  |**使用 minio 时这里必须设置为 '' 空字符传，因为文件存储在 bucketName**|
| spring.app.nfs.minio.server     | 文件访问地址，注意需要 “/” 结尾。 参考值： https://nas.domain.com/app/nas/         |
| spring.app.nfs.minio.endpoint   | minio 对象存储服务内网地址，参考值：http:///localhost:9001                      |
| spring.app.nfs.minio.accessKey  | minio 账户                                                         |
| spring.app.nfs.minio.secretKey  | minio 密码                                                         |
| spring.app.nfs.minio.bucketName | minio 存储桶名称。 参考值：app-name-oss                                    |

### ~~spring.app.nfs.directory~~

~~当前启用的文件储存应用文件根目录，基于 `spring.app.nfs.type` 动态加载 ，默认值：${spring.app.nfs.${spring.app.nfs.type}.directory}~~

### ~~spring.app.nfs.server~~

~~当前启用的文件储存文件访问地址，基于 `spring.app.nfs.type` 动态加载 ，默认值：${spring.app.nfs.${spring.app.nfs.type}.server}~~

### ~~spring.app.nfs.{folderName}~~

~~定义应用中文件，按文件夹分类~~


## spring.app.cache

缓存配置定义，配置文件定义之后，需要在 [CacheConfiguration.java](./#cacheconfiguration) 中注册缓存，并在对应的 Service + DAO 层启用缓存相关的代码和注解

| 配置项                                        | 说明                                              |
|--------------------------------------------|-------------------------------------------------|
| spring.app.cache.${缓存模块名}.expired          | 过期时间， redis 和 ehcache 共用                        |
| spring.app.cache.${缓存模块名}.nullValueExpired | null 值缓存时间                        |
| spring.app.cache.${缓存模块名}.heap             | 集成 ehcache 缓存参数，堆内数据行数                          |
| spring.app.cache.${缓存模块名}.offheap          | 集成 ehcache 缓存参数，堆外数据所占空间， 参考值： 1KB、1MB、1GB      |
| spring.app.cache.${缓存模块名}.disk             | 集成 ehcache 缓存参数，序列化到磁盘缓存文件最大值， 参考值： 1KB、1MB、1GB |

spring.app.cache.${缓存模块名}.expired

| 时间 | 说明  |
|----|-----|
| 1s | 1秒  |
| 1m | 1分钟 |
| 1h | 1小时 |
| 1d | 1天  |

