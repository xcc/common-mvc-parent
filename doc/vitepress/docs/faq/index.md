# 常见问题

+ 数据变更失败，影响行数:0  
  hibernate 特性，查询对象执行 set 改变值的时候，会自动更新数据库。 所以基于 org.springframework.data.jpa.repository.JpaRepository 接口的查询方法都需要 clone
  之后再执行 set 方法。 或者使用 org.springframework.data.querydsl.QuerydslPredicateExecutor 接口提供的查询方法，不会自动更新。

+ AES 密钥加载异常  
  java.security.InvalidKeyException: Illegal key size， 异常截图：  
  ![AES 密钥加载异常](/faq/aes_invalid_key_exception.png)
  异常引发原因是因为 JDK 版本问题，建议升级到 jdk-8u161 以上版本；或者修改密钥长度为 16 位。 其他解决方案参考： http://www.bewindoweb.com/290.html
+ 启动配置加载失败  
  `active: @env@ # 采坑记录，使用 @@ 获取 pom.xml 环境 ...`  
  ![启动配置加载失败](/faq/3-1.png)
  解决方案
  ![启动配置加载失败解决方案](/faq/3-2.png)
+ 查询投影一定要有主键  
  
