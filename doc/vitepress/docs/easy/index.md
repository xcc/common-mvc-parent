# 快速上手

步骤 1、2、3 用于快速初始化项目，步骤 4、5 用于打包环境集成自动化部署

## 目录

[[toc]]

## 步骤1 ： 初始化新项目
+ 方法1（二选一）：域名环境暂时无法访问，先用方法2
  ```
  # bash + curl 命令直接访问远程脚本，并执行项目初始化步骤；windows 系统可以用 git bash 命令窗口执行 bash 命令
  bash <(curl https://java-doc.cccc6666.com/cli.sh)
  ```
+ 方法2（二选一）：[点击这里下载脚本](https://gitee.com/xcc/common-mvc-parent/blob/spring-boot/doc/vitepress/docs/public/cli.sh)
  ```
  # 执行下载好的脚本，开始初始化项目；windows 系统可以用 git bash 命令窗口执行 bash 命令
  bash cli.sh
  ```

脚本执行依赖环境  
jdk 版本： >= [jdk-8u161](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)  
maven 版本： >= [maven 3](https://maven.apache.org/download.cgi)   
应用名 和 新项目 git 地址必填，其他配置项都可以在后面初始化完成之后修改。 脚本执行完成之后会自动推送代码到新仓库。执行过程截图如下：

![脚本执行截图](/cli.sh.png)

项目结构说明

+ doc：项目相关的文档和材料
+ gulp：代码生成及接口测试脚本
+ app-demo[jar]：单体应用入口
+ app.common.starter.util.Dates 仔细阅读这个日期工具类，使用了比较友好的链式调用，底部有 main 方法参考
+ app.common.starter.util.Util 这个类聚合了一些工具类，有一些方法标记为弃用，因为可以使用第三方的工具类（guava、apache）替代，不需要自己封装，尽量避免自己造轮子，用好现有的框架即可

## 步骤2 ： 数据库初始化

  ```
  -- 建库命令
  DROP DATABASE IF EXISTS app_db;
  CREATE DATABASE app_db CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci';
  ```  

> 项目首次启动时，会执行 `app-demo/src/main/resources/db/liquibase.xml` 自动创建表结构。    
> **注意：**
> + 建表语句参考 `app-demo/src/main/resources/db/db.sql` ，业务表建表语句一定要包含字段 【id,createTime,createUserId,updateTime,updateUserId,deleted】，中间表除外
> + 数据库敏感字段加解密密钥不一样，如果修改密钥，需要同时修改数据库脚本中的手机号密文 `db/liquibase/1.0.0/sql/tab_user-[local|dev|sit|uat|prod].sql`

## 步骤3 ： 启动项目

> 如果 [cli.sh](/cli.sh) 脚本初始化项目时未指定数据库账号密码， 需要手动修改以下配置之后再启动。 更多配置信息参考：[配置](/config/)

  ```
  spring.redis.host: # redis 服务ip
  spring.redis.port: # redis 端口
  spring.redis.database: # redis 索引空间
  spring.redis.password: # redis 密码
  spring.datasource.url: # 数据库连接
  spring.datasource.username: # 数据库账号
  spring.datasource.password: # 数据库密码
  ```

单体应用启动
`AppDemoApplication.java`

## 步骤4 ： 动态环境打包（可选项）

运行时可通过环境变量 --spring.profiles.active=prod 指定启动环境

  ```
  # 开发环境打包
  mvn clean package -Dmaven.test.skip=true -P dev
  # 系统测试环境打包
  mvn clean package -Dmaven.test.skip=true -P sit
  # 验收测试环境打包
  mvn clean package -Dmaven.test.skip=true -P uat
  # 生产环境打包
  mvn clean package -Dmaven.test.skip=true -P prod
  ```

## 步骤5 ： 打 tag 发布 release（可选项）

可以集成自动化部署，打完 tag 自动触发发布流程。

  ```
  # 预发布环境准备,将代码推送到 git 仓库, 失败后可重新执行
  mvn release:prepare -Dresume=false -X
  # 回滚预发布环境
  mvn release:rollback -X
  ```
