// https://www.qiyuandi.com/zhanzhang/zonghe/14601.html
module.exports = {
  title: '教程',// 网站标题
  description: 'java框架使用教程。', //网站描述
  base: '/', //  部署时的路径 默认 /  可以使用二级地址 /base/
  lang: 'zh-CN', //语言
  repo: 'vuejs/vitepress',
  // markdown: { // 代码显示行号
  //   lineNumbers: true
  // },
  head: [
    // 改变title的图标
    [
      'link',
      {
        rel: 'icon',
        href: '/favicon.ico', // 图片放在public文件夹下
      },
    ],
  ],
  // 主题配置
  themeConfig: {
    // 头部导航
    nav: [
      {text: '简介', link: '/'},
      {
        text: '相关链接',
        items: [
          {text: 'Vue3 官方文档', link: 'https://v3.cn.vuejs.org/'},
          {text: 'Vite 中文网', link: 'https://vitejs.cn/'},
          {text: 'VitePress 中文网', link: 'https://vitejs.cn/vitepress/'},
          {text: 'Vue3 非官方文档', link: 'https://vue3js.cn/'}
        ]
      },
      {text: '关于', link: '/about/'},
    ],
    // 侧边导航
    sidebar: [
      {
        text: '基础', children: [
          {text: '快速上手', link: '/easy/'},
          {text: '配置', link: '/config/'},
          {text: '规范', link: '/standard/'},
          {text: '注解', link: '/annotation/'}
        ]
      },
      {
        text: '进阶', children: [
          {text: '注解', link: '/annotation/'}
        ]
      },
      {text: 'FAQ', link: '/faq/'}
    ]
  }
}
