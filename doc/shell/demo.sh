#!/bin/bash
echo date +'%Y-%m-%d %H:%M:%S'
# nohup bash call.sh > call.log 2>&1 &

# 读取 file.log 文件，替换所有数字，替换成递增数字
awk -v RS="[0-9]+" '{n+=1;printf $0n}' file.log

# 默认值
# DATE_NOW=${DATE_NOW-`date +'%Y-%m-%d %H:%M:%S'`}

# 文件路径处理 **********************************************************************************************************
CMD=$0
if [[ "$CMD" == /* ]]; then
  CMD_PATH="${CMD}" # 以 '/' 开头，表示绝对路径，不做处理
elif [[ "$CMD" =~ \.\. ]]; then
  echo '../路径不支持'
  exit
else
  # 执行CMD路径
  CMD_PATH="$PWD/${CMD#./}"
fi
echo "执行路径：$PWD
执行命令：${CMD#./}
脚本文件：$CMD_PATH
脚本目录：$(dirname $CMD_PATH)
脚本名称：$(basename $CMD_PATH)
"

# 从文件提取内容 **********************************************************************************************************
for id in $(grep -Eo '"id":"\w+",' app.log | grep -Eo ':"\w+' | grep -Eo '\w+')
do
  echo "$id"
done

# 遍历指目录指定后缀文件 ***************************************************************************************************
for file in *.sh; do
  echo "$file"
done

# 遍历指目录文件，计算文件 md5 值 *******************************************************************************************
#for file in ./*
#do
#  if test -f "$file"
#  then
#    # cut 剪切文本。 -d 指定与空格和tab键不同的域分隔符。 -f1 表示第一个域。
#    echo "文件：$file => md5: $(md5sum $file | cut -d ' ' -f1)"
#  elif test -d "$file"
#  then
#    echo "目录：$file"
#  else
#    echo "未知 $file"
#  fi
#done
#
#for i in {1..10}
#do
#echo $i
#done

# 按行读取文件内容 ********************************************************************************************************
while read -r row
do
  echo "$row"
done < /home/ccx/git-repository/common-mvc-parent/doc/shell/id.log

# 字符串切割 ********************************************************************************************************
#string="java,JavaScript,mysql,spring"
#array=(`echo $string | tr ',' ' '` )
#for var in ${array[@]}
#do
#   echo $var
#done

# 处理 curl 命令 ： ES 删除日志索引 ****************************************************************************************
RUN_HOST='http://localhost:9200'
curl "$RUN_HOST/_cat/indices?v" > run.log
while read -r row
do
  echo "$row"
  index_name=$(echo "$row" | awk '{print $3}')
  if [[ $index_name == elk-* ]]
  then
    echo -e "\n删除日志索引文件：$index_name" >> run.log
    echo -e "# curl -X DELETE $RUN_HOST/$index_name" >> run.log
    curl -X DELETE "$RUN_HOST/$index_name" >> run.log
  fi
done < run.log

# 按行读取文件内容 ： ES 按 id 删除索引数据 **********************************************************************************
echo '' > run.log
while read -r row
do
  echo "$row"
  RUN_HOST='http://localhost:9200'
  RUN_INDEX_NAME='索引名称'
  echo -e "\nrow：$row" >> run.log
  echo -e "# curl -X DELETE $RUN_HOST/$RUN_INDEX_NAME/_doc/$row" >> run.log
  curl -X DELETE "$RUN_HOST/$RUN_INDEX_NAME/_doc/$row" >> run.log
done < id.log

# 处理 curl 命令 ： 从返回的 json 结果中提取指定字段，字段属性名必须是唯一的 *******************************************************
RUN_HOST='http://localhost:9200'
RUN_INDEX_NAME='索引名称'
result=$(curl -X POST "$RUN_HOST/$RUN_INDEX_NAME/_search" -H 'Content-Type: application/json' -d '{"_source": ["id"],"from":0,"size":1000}')
echo $result | sed 's/,/\n/g' | grep '"_id"' | sed 's/"_id":"\(\w\+\)"/\1/g' > run.log

# docker 镜像备份
docker save alpine | gzip > alpine-latest.tgz
# docker 镜像加载
docker load -i alpine-latest.tgz
# docker 镜像迁移到另外一台主机
#docker save <镜像名> | bzip2 | pv | ssh <用户名>@<主机名> 'cat | docker load'

# 添加远程仓库
git remote add ${仓库别名} ${新仓库地址}
git push ${仓库别名}

# git 获取 commit id
GIT_COMMIT_ID=`git reflog | head -n 1 | awk '{print $1}'`
echo $GIT_COMMIT_ID

# git 提交命令
git commit -m '提交内容'

# git 重置
git reset HEAD --hard # 最近一次提交的代码
git reset origin/develop --hard # 最近一次从服务器上 fetch 的代码

# git 分支
git branch -l # 本地所有分支
git branch -r # 远端所有分支
git branch -a # 本地+远端所有分支
git branch -d $BRANCH_NAME # 删除本地分支
git push origin -d $BRANCH_NAME # 删除远端分支

# git 标签
git tag V1.0.0 # 打标签
git push --tags # 推送标签到仓库

# git 推送
git push --set-upstream origin $BRANCH_NAME # 推送仓库不存在分支

# git 检出
git checkout $BRANCH_NAME # 检出代码，不改变分支名
git checkout $BRANCH_NAME -b $BRANCH_NAME_NEW # 检出代码使用新分支名

# 全局替换文件内容 if [[ "$(uname -s)" =~ 'Darwin' ]]
echo 'a123bc456' | sed 's/\([0-9]\)[0-9]\+/\1.../g'
sed -i 's/被替换的文字/替换后的文字/g' $FILE_PATH # 注意 mac 系统 -i 后面要加 '' ： sed -i ''
perl -pi -e 's/被替换的文字/替换后的文字/g' $FILE_PATH # 兼容 mac 系统

# 只能替换，无法回写文件内容
awk -v RS="(position = )[0-9]+" '{n+=1;printf $0"position = "n}' $FILE_PATH

# 清理 .iml 文件
find ./ -name \*.iml | xargs rm -rf

# 循环生成二维码
clear && for type in ANSI ANSI256 ASCII ASCIIi UTF8 ANSIUTF8;do echo $type ;qrencode -t $type -m 1 -s 1 "app.com";done

# ssh抓取多台服务器日志
#alias glog='call(){ TID=${1:-ERROR}; AFTER=${2:-0}; grep $TID -A $AFTER ~/logs/app.log; ssh -i ~/.ssh/ssh_id_rsa root@192.168.1.111 "grep $TID -A $AFTER ~/logs/app.log" | grep $TID -A $AFTER; }; call'
