#!/usr/bin/env bash
# chmod +x app-demo.sh
# 启动|停止 app-demo.jar 文件
# 注意：在sh文件中=赋值，左右两侧不能有空格

# 应用运行目录
APP_HOME=~/git-repository/common-mvc-parent/apps-prod
# 应用名称(jar包名称)：{APP_NAME}.jar
APP_NAME=app-demo
# 启动端口
SERVER_PORT=3366
# LOG_PATH=$APP_HOME/logs/$APP_NAME.log
#JVM参数
#https://help.aliyun.com/document_detail/148851.html
#https://segmentfault.com/a/1190000038190184
#https://juejin.cn/post/7109260648453767204
# -Xms20m ：设置jvm初始化堆大小为20m，一般与-Xmx相同避免垃圾回收完成后jvm重新分。
# -Xmx20m：设置jvm最大可用内存大小为20m，Xms 和 Xmx设置为老年代存活对象的3-4倍，即Full GC之后的老年代内存占用的3-4倍。。
# -Xmn10m：设置新生代大小为10m，整个JVM内存大小=年轻代大小+年老代大小+持久代大小。持久代一般固定大小为64 MB，所以增大年轻代后，将会减小年老代大小。此值对系统性能影响较大，Sun官方推荐配置为整个堆的3/8。年轻代Xmn的设置为老年代存活对象的1-1.5倍。
# -Xss128k：设置每个线程的栈大小为128k。
# -verbose:gc：可以输出每次GC的一些信息；
# -XX:SurvivorRatio=8：设置新生代中 eden、survivor 分配比例，eden/survivor=8，to survivor 预留空间用于 gc 回收；-XX:SurvivorRatio=4，设置年轻代中Eden区与Survivor区的大小比值。如果设置为4，那么两个Survivor区与一个Eden区的比值为2:4，一个Survivor区占整个年轻代的1/6。
# -XX:NewRatio：设置年轻代（包括Eden和两个Survivor区）与年老代的比值（除去持久代）。如果设置为4，那么年轻代与年老代所占比值为1:4，年轻代占整个堆栈的1/5。
# -XX:MaxPermSize=64MB：设置永久代(持久代)大小，1.8 废弃。
# -XX:MetaspaceSize=512M：永久代(持久代)最小阈值，默认约为21M，空间使用达到阈值，触发FullGC，永久代设置为老年代存活对象的1.2-1.5倍
# -XX:MaxMetaspaceSize=1024M：永久代(持久代) 最大阈值，设置太小会触发FullGC
# -XX:-UseConcMarkSweepGC：使用CMS收集器；
# -XX:-UseParallelGC；
# -XX:-UseSerialGC；
# -XX:CMSInitiatingOccupancyFraction=80 CMS gc，表示在老年代达到80%使用率时马上进行回收；
# -XX:+printGC；
# -XX:+PrintGCTimeStamps：打印时间戳；
# JVM_OPTS="-Dname=$SpringBoot  -Duser.timezone=Asia/Shanghai -Xms512M -Xmx512M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCDateStamps  -XX:+PrintGCDetails -XX:NewRatio=1 -XX:SurvivorRatio=30 -XX:+UseParallelGC -XX:+UseParallelOldGC -XXUseConcMarkSweepGC -XXReservedCodeCacheSize256m"
JVM_OPTS="-Dapp.name=$APP_NAME"
#启动参数
# SPRING_OPTS="--spring.profiles.active=prod"
SPRING_OPTS="--server.port=$SERVER_PORT"
echo $APP_HOME
echo $APP_NAME

if [ "$1" = "" ];
then
    echo -e "未输入操作名 {start|stop|restart|status} "
    exit 1
fi

function start()
{
    count=`ps -ef |grep java|grep $APP_NAME|grep -v grep|wc -l`
    if [ $count != 0 ];then
        echo "$APP_NAME is running..."
    else
        echo "Start $APP_NAME success..."
        nohup java -jar $JVM_OPTS $APP_NAME.jar $SPRING_OPTS > /dev/null 2>&1 &
    fi
    echo "tail -f $APP_HOME/logs/$APP_NAME."
}

function stop()
{
    echo "Stop $APP_NAME"
    echo `ps -ef |grep java|grep $APP_NAME|grep -v grep`
    echo `ps -ef |grep java|grep $APP_NAME|grep -v grep|wc -l`
    boot_id=`ps -ef |grep java|grep $APP_NAME|grep -v grep`
    count=`ps -ef |grep java|grep $APP_NAME|grep -v grep|wc -l`

    if [ $count != 0 ];then
        kill $boot_id
        count=`ps -ef |grep java|grep $APP_NAME|grep -v grep|wc -l`

        boot_id=`ps -ef |grep java|grep $APP_NAME|grep -v grep`
        kill -9 $boot_id
    fi
}

function restart()
{
    stop
    sleep 2
    start
}

function status()
{
    count=`ps -ef |grep java|grep $APP_NAME|grep -v grep|wc -l`
    if [ $count != 0 ];then
        echo "$APP_NAME is running..."
    else
        echo "$APP_NAME is not running..."
    fi
}

case $1 in
    start)
    start;;
    stop)
    stop;;
    restart)
    restart;;
    status)
    status;;
    *)

    echo -e "Usage: sh $APP_NAME.sh {start|stop|restart|status} \nExample: \nsh $APP_NAME.sh start"

esac
